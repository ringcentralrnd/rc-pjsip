Environment configuration:
==========================
1. Install Java JDK
1.1 Download and install JDK: http://www.oracle.com/technetwork/java/javase/downloads/index.html
1.2 Set JAVA_HOME environment variable (example: c:\Program Files\Java\jdk1.6.0_19)
1.3 Add %JAVA_HOME%\bin\ variable to PATH

2. Install Android SDK (r10 or above)
2.1 Download and install: http://ant.apache.org/manual/index.html
2.2 Set ANDROID_HOME environment variable (example: c:\Program Files\android-sdk\) 
2.3 Add %ANDROID_HOME% variable to PATH

Branding:
==========================
- Main development branch is the project root (Android tree)
- For branding support the following mechanism is used:
-- For each brand a customization tree is created (following Android project tree) under "brand" folder where files can be declared that will replace root files
-- For each brand, brand/build properties (application name, version, package, etc) and market key are declared
-- At building time:
--- Main tree will be copied to "brands-builds/<brand-code-name>" folder, 
--- Files from "brand/<brand-code-name>" will override files in "brands-builds/<brand-code-name> "
--- Package and packages in sources/resources will be changed
--- AndroidManifest.xml will be updated
--- BUILD.java file will be generated for conditional compilation (and run-time selection)
--- Common build.xml, build.properties and proguard.cfg will be added to each brand build structure to complete Android project tree 
--- For all brand build trees common Android build procedure will be launched 

Build:
==========================
- Launch build.bat
- The build procedure will create brands trees under "brands-builds" folder
- For each brand in the "brands-builds" common Android build procedure will be called
-- Brand builds (APKs) will be generated in "brands-builds/<brand>/builds" folder:
--- Debug/Engineering build: "engineering", debug, not-obfuscated, signed by debug key 
--- Release/QA build: "qa", debug, not-obfuscated, signed by debug key
--- Production build: "market": no-debug, obfuscated, signed by market key


