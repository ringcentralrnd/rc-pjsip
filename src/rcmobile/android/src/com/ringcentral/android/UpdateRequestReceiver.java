package com.ringcentral.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class UpdateRequestReceiver extends BroadcastReceiver {

    private int requestType;

    private Updatable i;

    public UpdateRequestReceiver(Updatable i, int requestType) {
        super();
        this.i = i;
        this.requestType = requestType;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getIntExtra("type", -1) == this.requestType) {
            i.onUpdateRequest();
        }
    }
}
