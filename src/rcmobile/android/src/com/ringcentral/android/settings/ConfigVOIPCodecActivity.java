/** 
 * Copyright (C) 2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import java.util.Arrays;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.service.CodecInfo;
import com.rcbase.android.sip.service.CodecInfo.Codec;
import com.rcbase.android.sip.service.CodecInfoComparator;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.provider.RCMProviderHelper;

/**
 * Configuration VoIP codec set.
 */
public class ConfigVOIPCodecActivity extends Activity {
	private static final String TAG = "[RC]ConfigVOIPCodecActivity";
	public static final String IS_WIDEBAND_BOOLEAN_EXTRA = "is_wide_band";
	private boolean mWideBandMode = false;
	private CodecInfo[] mCodec;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mWideBandMode = getIntent().getBooleanExtra(IS_WIDEBAND_BOOLEAN_EXTRA, true);
		mCodec = RCMProviderHelper.getCodec(this, mWideBandMode);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.voip_codec_config_layout);
		buildLayout();
	}

	private void buildLayout() {
		if (mCodec == null || mCodec.length == 0) {
			if (LogSettings.MARKET) {
				MktLog.w(TAG, "codec set empty or null");
			}
			finish();
			return;
		}
		Arrays.sort(mCodec, new CodecInfoComparator());
		LinearLayout body = (LinearLayout) findViewById(R.id.codec_config_body);
		body.removeAllViewsInLayout();

		TextView header = new TextView(this);
		if (mWideBandMode) {
			header.setText(R.string.config_screen_voip_codec_wideband_header);
		} else {
			header.setText(R.string.config_screen_voip_codec_narrowband_header);
		}
		header.setTextColor(Color.WHITE);
		header.setTypeface(Typeface.DEFAULT_BOLD);
		header.setBackgroundResource(R.drawable.listheader);
		LayoutParams lparams = new ViewGroup.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		header.setLayoutParams(lparams);
		body.addView(header);

		for (CodecInfo codecInfo : mCodec) {
			View item = View.inflate(this, R.layout.list_row, null);
			item.setEnabled(true);
			((TextView) item.findViewById(R.id.name)).setText(codecInfo.getCodec().getName());
			TextView label = (TextView) item.findViewById(R.id.label);
			label.setEnabled(true);
			if (codecInfo.getPriority() == CodecInfo.Codec.DISABLED) {
				label.setText(R.string.config_screen_voip_codec_disabled_label);
				label.setTextColor(Color.RED);
			} else {
				label.setText(getString(R.string.config_screen_voip_codec_priority_label) + " " + codecInfo.getPriority());
				label.setTextColor(getResources().getColor(R.color.textColorBlue));
			}
			item.setTag(codecInfo);
			item.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						final View dialogView = LayoutInflater.from(ConfigVOIPCodecActivity.this).inflate(R.layout.voip_codec_config_priority_dialog_layout, null);
						final CodecInfo codecInfo = (CodecInfo) v.getTag();

						EditText ed = ((EditText) dialogView.findViewById(R.id.voip_codec_set_priority_edit_text));
						ed.setText("" + codecInfo.getPriority());
						int position = ed.length();
						Editable etext = ed.getText();
						Selection.setSelection(etext, position);

						final AlertDialog dialog = RcAlertDialog.getBuilder(ConfigVOIPCodecActivity.this)
						.setTitle(R.string.config_screen_voip_codec_priority_set_title)
						.setMessage(R.string.config_screen_voip_codec_priority_set_message)
						.setView(dialogView)
						.setIcon(R.drawable.symbol_exclamation)
						.setCancelable(true)
						.create();

						((Button) dialogView.findViewById(R.id.voip_codec_set_priority_button)).setOnClickListener(new OnClickListener() {
							public void onClick(View view) {
								TextView txt = (TextView) dialogView.findViewById(R.id.voip_codec_set_priority_edit_text);
								String priority = txt.getText().toString().trim();
								int newPriority = 0;
								try {
									newPriority = Integer.parseInt(priority);
								} catch (java.lang.Throwable th) {
									Toast.makeText(ConfigVOIPCodecActivity.this, "Invalid value", Toast.LENGTH_LONG).show();
									return;
								}

								if (newPriority > CodecInfo.Codec.MAX_PRIORITY || newPriority < CodecInfo.Codec.MIN_PRIORITY) {
									Toast.makeText(ConfigVOIPCodecActivity.this, "Invalid value", Toast.LENGTH_LONG).show();
									return;
								}
								try {
									dialog.dismiss();
								} catch (java.lang.Throwable thIn) {
								}
								if (codecInfo.getPriority() != newPriority) {
									codecInfo.setPriority(newPriority);
									buildLayout();
									return;
								}
								return;
							}
						});

						dialog.show();

					} catch (java.lang.Throwable th) {
						Toast.makeText(ConfigVOIPCodecActivity.this, "Dialog creation error : " + th.toString(), Toast.LENGTH_LONG).show();
					}
				}
			});

			body.addView(item);
		}

		TextView separator = new TextView(this);
		separator.setText(R.string.config_screen_voip_codec_action_header);
		separator.setTextColor(Color.WHITE);
		separator.setTypeface(Typeface.DEFAULT_BOLD);
		separator.setBackgroundResource(R.drawable.listheader);
		lparams = new ViewGroup.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		separator.setLayoutParams(lparams);
		body.addView(separator);

		View loadDefaultsView = View.inflate(this, R.layout.list_row, null);
		loadDefaultsView.setEnabled(true);
		((TextView) loadDefaultsView.findViewById(R.id.name)).setText(R.string.config_screen_voip_codec_load_defaults_name);
		loadDefaultsView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mWideBandMode) {
					mCodec = Codec.getDefaultsForWideBand();
				} else {
					mCodec = Codec.getDefaultsForNarrowBand();
				}
				Toast.makeText(ConfigVOIPCodecActivity.this, getString(R.string.config_screen_voip_codec_priority_set_loaded_prompt), Toast.LENGTH_SHORT).show();
				buildLayout();
			}
		});
		body.addView(loadDefaultsView);

		View saveView = View.inflate(this, R.layout.list_row, null);
		saveView.setEnabled(true);
		((TextView) saveView.findViewById(R.id.name)).setText(R.string.config_screen_voip_codec_save_name);
		((TextView) saveView.findViewById(R.id.label)).setText(R.string.config_screen_voip_codec_save_label);
		saveView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mCodec == null || mCodec.length == 0) {
					if (LogSettings.MARKET) {
						MktLog.w(TAG, "codec set empty or null");
					}
					finish();
					return;
				}
				for (CodecInfo codecInfo : mCodec) {
					RCMProviderHelper.setPriorityForCodec(ConfigVOIPCodecActivity.this, codecInfo, mWideBandMode);
				}
				Toast.makeText(ConfigVOIPCodecActivity.this, getString(R.string.config_screen_voip_codec_priority_set_saved_prompt), Toast.LENGTH_SHORT).show();
			}
		});
		body.addView(saveView);
	}
}
