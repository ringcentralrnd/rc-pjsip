/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.ringcentral.android.LoggingScreen;
import com.ringcentral.android.R;

public class ConfigSettingsActivity extends Activity {
	
	public static final String CONFIG_LAUNCH_NUMBER = "*3328433284#";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.config_activity_main);
		
		((TextView)findViewById(R.id.config_screen_url_settings).findViewById(R.id.name)).setText(R.string.config_menu_configure_URL_label);		
		((ImageView) findViewById(R.id.config_screen_url_settings).findViewById(R.id.imgright)).setImageResource(R.drawable.ic_list_link);
		findViewById(R.id.config_screen_url_settings).findViewById(R.id.label).setVisibility(View.GONE);
		
		findViewById(R.id.config_screen_url_settings).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(ConfigSettingsActivity.this, ConfigURLActivity.class));				
			}
		});
		
		((TextView)findViewById(R.id.config_screen_voip_settings).findViewById(R.id.name)).setText(R.string.config_menu_configure_VOIP_label);		
		((ImageView) findViewById(R.id.config_screen_voip_settings).findViewById(R.id.imgright)).setImageResource(R.drawable.ic_list_link);
		findViewById(R.id.config_screen_voip_settings).findViewById(R.id.label).setVisibility(View.GONE);
		
		findViewById(R.id.config_screen_voip_settings).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(ConfigSettingsActivity.this, ConfigVOIPActivity.class));
			}
		});
		
		((TextView)findViewById(R.id.config_screen_send_log).findViewById(R.id.name)).setText(R.string.config_menu_configure_send_log_label);        
        ((ImageView) findViewById(R.id.config_screen_send_log).findViewById(R.id.imgright)).setImageResource(R.drawable.ic_list_link);
        findViewById(R.id.config_screen_send_log).findViewById(R.id.label).setVisibility(View.GONE);
        
        findViewById(R.id.config_screen_send_log).setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LoggingScreen.class));
            }
        });
        
		
	}
	
	public static boolean isEnabled(String number){		
		return ((number != null) && (number.equals(CONFIG_LAUNCH_NUMBER)));
		
	}
}
