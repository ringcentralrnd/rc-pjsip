/** 
 * Copyright (C) 2011-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConfig;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.DeviceUtils;
import com.ringcentral.android.utils.RestartAppIntentReceiver;
import com.ringcentral.android.utils.UserInfo;

public class Settings5 extends Activity {
    
    private static final String TAG = "[RC]Settings5";
    
    RestartAppIntentReceiver restartReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        buildLayout(DeviceUtils.getDisplayOrientation(this));
        restartReceiver = RestartAppIntentReceiver.registerRestartEvent(this);
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	if(restartReceiver != null){
    		restartReceiver.destroy();
    		restartReceiver = null;
    	}    	
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	FlurryTypes.onStartSession(this);    	
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	FlurryTypes.onEndSession(this);
    }
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED && hasFocus){
            PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.SETTINGS_OPENING);
        }
    }
    
    /**
     * Build Layout View according to orientation
     */
    private void buildLayout(int orientation) {
        setContentView(orientation == Configuration.ORIENTATION_LANDSCAPE ?
                R.layout.settings5_landscape               //LANDSCAPE
                : R.layout.settings5_portrait);            //PORTRAIT or SQUARE

        
        // RCFax Menu
        // - Account Info (web)
        // - Company (web)
        // - Billing (web)
        // - General
        //   -- Logout
        //   -- Tell A Friend (web)
        //   -- About RingCentral Mobile
        
        // Admin user
        // - Company (web)
        // - Billing (web)
        // - Incoming Calls (web)
        // - Outgoing Calls
        //   -- RingOut Mode
        //   -- Caller ID
        //   -- Fax Settings (web)
        // - General
        //   -- Logout
        //   -- DND Settings
        //   -- Tell A Friend (web)
        //   -- About RingCentral Mobile
        
        // User
        // - Incoming Calls (web)
        // - Outgoing Calls (web)
        //   -- RingOut Mode
        //   -- Caller ID
        //   -- Fax Settings (web)
        // - General
        //   -- Logout
        //   -- DND Settings
        //   -- Tell A Friend (web)
        //   -- About RingCentral Mobile
        
        // Department Manager
        // - Incoming Calls (web)
        // - General
        //   -- Logout
        //   -- Tell A Friend (web)
        //   -- About RingCentral Mobile
        
        //  Take Message Only (TMO) or Announcement Only (AO) 
        // - Incoming Calls (web)
        // - General
        //   -- Logout
        //   -- Tell A Friend (web)
        //   -- About RingCentral Mobile
        
        final int user_type = UserInfo.getUserType(this);
        final int tier_type = UserInfo.getTierServiceType(this);
        
        View item;

        // ---------------------------------------------------------------------------------------------------------------------------------
        // Account Info Menu item
        //    RCFax tier
        // 
        item = findViewById(R.id.item_account_info);
        if (tier_type == UserInfo.TIER_SERVICE_TYPE_RCFAX) {
            item.setVisibility(View.VISIBLE);
            
            ((ImageView) item.findViewById(R.id.image)).setImageResource(R.drawable.settings5_ic_account_info);
            ((TextView) item.findViewById(R.id.text)).setText(R.string.settings5_item_account_info);
            
            item.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: RCFax Account info");
                    }
                    
                    Uri uri = Uri.parse(RCMConfig.get_WEB_SETTINGS_ACCOUNT_INFO_FOR_RCFAX_URL(Settings5.this));
                    if (LogSettings.QA) {
                        QaLog.i(TAG, "URI: RCFax Account Info: " + uri.toString());
                    }
                    
                    startActivity(new Intent(Intent.ACTION_VIEW, uri));
               }
            });
        } else {
            item.setVisibility(View.GONE);
        }
        
        // ---------------------------------------------------------------------------------------------------------------------------------
        // "Company" divider AB-831
        //    Admin
        // 
        item = findViewById(R.id.divider_company);
        if (user_type == UserInfo.ADMIN && tier_type != UserInfo.TIER_SERVICE_TYPE_RCFAX) {
            item.setVisibility(View.VISIBLE);
        } else {
            item.setVisibility(View.GONE);
        }
        
        // ---------------------------------------------------------------------------------------------------------------------------------
        // Company Menu item
        //    RCFax tier
        //    Admin
        // 
        item = findViewById(R.id.item_company);
        if (user_type == UserInfo.ADMIN || tier_type == UserInfo.TIER_SERVICE_TYPE_RCFAX) {
            item.setVisibility(View.VISIBLE);
            
            ((ImageView) item.findViewById(R.id.image)).setImageResource(R.drawable.settings5_ic_company);
            ((TextView) item.findViewById(R.id.text)).setText(R.string.settings5_item_company);
            
            item.setOnClickListener(new OnClickListener() {
    
                @Override
                public void onClick(View v) {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: Company");
                    }
                    Uri uri;
                    if (tier_type == UserInfo.TIER_SERVICE_TYPE_RCFAX) {
                        uri = Uri.parse(RCMConfig.get_WEB_SETTINGS_COMPANY_FOR_RCFAX_URL(Settings5.this));
                    } else {
                        uri = Uri.parse(RCMConfig.get_WEB_SETTINGS_COMPANY_URL(Settings5.this));
                    }
                    if (LogSettings.QA) {
                        QaLog.i(TAG, "URI: Company: " + uri.toString());
                    }
                    
                    startActivity(new Intent(Intent.ACTION_VIEW, uri));
                    FlurryTypes.onEvent(FlurryTypes.SETTINGS_VISITED, FlurryTypes.MENU_ITEM, FlurryTypes.COMPANY);
               }
            });
        } else {
            item.setVisibility(View.GONE);
        }

        // ---------------------------------------------------------------------------------------------------------------------------------
        // Billing Menu item (Enabled/disabled on per-brand basis, refer AB-969) 
        //    RCFax tier
        //    Admin
        // 
        item = findViewById(R.id.item_billing);
        if (BUILD.SETTINGS_BILLING_ENABLED && 
        		(user_type == UserInfo.ADMIN  || tier_type == UserInfo.TIER_SERVICE_TYPE_RCFAX)) {
            item.setVisibility(View.VISIBLE);
            
            ((ImageView) item.findViewById(R.id.image)).setImageResource(R.drawable.settings5_ic_billing);
            ((TextView) item.findViewById(R.id.text)).setText(R.string.settings5_item_billing);

            item.setOnClickListener(new OnClickListener() {
    
                @Override
                public void onClick(View v) {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: Billing");
                    }
                    
                    Uri uri = Uri.parse(RCMConfig.get_WEB_SETTINGS_BILLING_URL(Settings5.this));
                    if (LogSettings.QA) {
                        QaLog.i(TAG, "URI: Billing: " + uri.toString());
                    }
                    
                    startActivity(new Intent(Intent.ACTION_VIEW, uri));
                    FlurryTypes.onEvent(FlurryTypes.SETTINGS_VISITED, FlurryTypes.MENU_ITEM, FlurryTypes.BILLING);
               }
            });
        } else {
            item.setVisibility(View.GONE);
        }
        
        // ---------------------------------------------------------------------------------------------------------------------------------
        // "Personal" divider AB-831
        //    Admin
        // 
        item = findViewById(R.id.divider_personal);
        if (user_type == UserInfo.ADMIN && tier_type != UserInfo.TIER_SERVICE_TYPE_RCFAX) {
            item.setVisibility(View.VISIBLE);
        } else {
            item.setVisibility(View.GONE);
        }
        
        // ---------------------------------------------------------------------------------------------------------------------------------
        // Incoming Calls Menu item
        //    Except RCFax tier
        // 
        item = findViewById(R.id.item_incoming_calls);

        if (tier_type != UserInfo.TIER_SERVICE_TYPE_RCFAX) {
            item.setVisibility(View.VISIBLE);
            ((ImageView) item.findViewById(R.id.image)).setImageResource(R.drawable.settings5_ic_incoming_calls);
            ((TextView) item.findViewById(R.id.text)).setText(R.string.settings5_item_incoming_calls);
            item.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: Incoming Calls");
                    }
                    Uri uri = Uri.parse(RCMConfig.get_WEB_SETTINGS_INCOMING_CALLS_URL(Settings5.this));
                    if (LogSettings.QA) {
                        QaLog.i(TAG, "URI: Incoming Calls: " + uri.toString());
                    }
                    startActivity(new Intent(Intent.ACTION_VIEW, uri));
                    FlurryTypes.onEvent(FlurryTypes.SETTINGS_VISITED, FlurryTypes.MENU_ITEM, FlurryTypes.INCOMING_CALLS);
                }
            });
        } else {
            item.setVisibility(View.GONE);
        }

        // ---------------------------------------------------------------------------------------------------------------------------------
        // Outgoing Calls Menu item
        //    User
        //    Admin
        // 
        item = findViewById(R.id.item_outgoing_calls);
        if ((tier_type != UserInfo.TIER_SERVICE_TYPE_RCFAX) && (user_type == UserInfo.ADMIN || user_type == UserInfo.USER)) {
            item.setVisibility(View.VISIBLE);
            
            ((ImageView) item.findViewById(R.id.image)).setImageResource(R.drawable.settings5_ic_outgoing_calls);
            ((TextView) item.findViewById(R.id.text)).setText(R.string.settings5_item_outgoing_calls);
            
            item.setOnClickListener(new OnClickListener() {
    
                @Override
                public void onClick(View v) {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: Outgoing Calls");
                    }
                    Intent i = new Intent(Settings5.this, Settings.class);
                    i.putExtra(RCMConstants.EXTRA_SETTINGS5, Settings.SETTINGS_OUTGOING_CALLS);
                    startActivity(i);
                    FlurryTypes.onEvent(FlurryTypes.SETTINGS_VISITED, FlurryTypes.MENU_ITEM, FlurryTypes.OUTGOING_CALLS);
                }
            });
        } else {
            item.setVisibility(View.GONE);
        }
        
        
        item = findViewById(R.id.item_general);
        item.setVisibility(View.VISIBLE);

        ((ImageView) item.findViewById(R.id.image)).setImageResource(R.drawable.settings5_ic_general);
        ((TextView) item.findViewById(R.id.text)).setText(R.string.settings5_item_general);
        
        item.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: General");
                }
                Intent i = new Intent(Settings5.this, Settings.class);
                i.putExtra(RCMConstants.EXTRA_SETTINGS5, Settings.SETTINGS_GENERAL);
                startActivity(i);
                FlurryTypes.onEvent(FlurryTypes.SETTINGS_VISITED, FlurryTypes.MENU_ITEM, FlurryTypes.GENERAL);
            }
        });
    }

}
