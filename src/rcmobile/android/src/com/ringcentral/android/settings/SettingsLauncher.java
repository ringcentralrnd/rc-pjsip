/** 
 * Copyright (C) 2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import android.content.Context;
import android.content.Intent;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.AccountInfoTable;
import com.ringcentral.android.statistics.FlurryTypes;

public class SettingsLauncher {

    public static void launch(Context context) {
        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
            PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.SETTINGS_OPENING);
        }
        if (RCMProviderHelper.getServiceVersion(context) >= AccountInfoTable.SERVICE_VERSION_5) {
            context.startActivity(new Intent(context, Settings5.class));
        } else {
            context.startActivity(new Intent(context, Settings.class));
        }
        FlurryTypes.onEvent(FlurryTypes.SETTINGS_VISITED, FlurryTypes.MENU_ITEM, FlurryTypes.MAIN);
    }
}
