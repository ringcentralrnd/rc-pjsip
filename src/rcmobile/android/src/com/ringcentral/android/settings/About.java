/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.utils.AppInfo;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.EmailSender;

public class About extends Activity {
    
    private static final String TAG = "[RC]About";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.about);

        ((TextView) findViewById(R.id.about_app_description).findViewById(R.id.name)).setText(R.string.settings_item_app_description_title);
        findViewById(R.id.about_app_description).findViewById(R.id.label).setVisibility(View.GONE);
        findViewById(R.id.about_app_description).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: App Description");
                }
                FlurryTypes.onEvent(FlurryTypes.EVENT_APP_DESCRIPTION);
                startActivity(new Intent(About.this, AppDescription.class));
            }
        });

        ((TextView) findViewById(R.id.about_version).findViewById(R.id.name)).setText(R.string.settings_item_version_title);
        ((TextView) findViewById(R.id.about_version).findViewById(R.id.label)).setText(getBuildVersion());

        if (BUILD.SETTINGS_LEGACY_ABOUT_TELLFRIEND_ENABLED
                &&  getIntent().getIntExtra(RCMConstants.EXTRA_SETTINGS5, Settings.SETTINGS_DEFAULT) == Settings.SETTINGS_DEFAULT) {
            ((TextView) findViewById(R.id.about_tell_a_friend).findViewById(R.id.name)).setText(R.string.settings_item_tell_a_friend_title);
            findViewById(R.id.about_tell_a_friend).findViewById(R.id.label).setVisibility(View.GONE);
            findViewById(R.id.about_tell_a_friend).setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                	FlurryTypes.onEvent(FlurryTypes.EVENT_TELL_FRIEND);
                    String userName = RCMProviderHelper.getAccountUserName(About.this, RCMProviderHelper.getCurrentMailboxId(About.this));
                    if (userName == null) {
                        userName = getResources().getString(R.string.settings_noname_user_name);
                    }
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: Tell a Friend");
                    }
                    new EmailSender(About.this).sendEmail(
                            null,
                            getString(R.string.settings_tell_a_friend_subject_value, userName),
                            getString(R.string.settings_tell_a_friend_body_value, userName),
                            null);
                }
            });
        } else {
            findViewById(R.id.about_tell_a_friend).setVisibility(View.GONE);
        }

        ((TextView) findViewById(R.id.about_send_feedback).findViewById(R.id.name)).setText(R.string.settings_item_send_feedback_title);
        findViewById(R.id.about_send_feedback).findViewById(R.id.label).setVisibility(View.GONE);
        findViewById(R.id.about_send_feedback).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Send Feedback");
                }
                FlurryTypes.onEvent(FlurryTypes.EVENT_FEEDBACK_EMAIL);
                new EmailSender(About.this).sendEmail(
                        new String[] {getString(R.string.settings_about_feedback_email)},
                        getResources().getString(R.string.settings_feedback_subject_value, BUILD.VERSION_NAME),
                        "",
                        null);
            }
        });

        ((TextView) findViewById(R.id.about_rate_app).findViewById(R.id.name)).setText(R.string.settings_about_rate_app_title);
        ((ImageView) findViewById(R.id.about_rate_app).findViewById(R.id.imgright)).setImageResource(R.drawable.rate_star_big_on);
        findViewById(R.id.about_rate_app).findViewById(R.id.label).setVisibility(View.GONE);
        findViewById(R.id.about_rate_app).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Rate app");
                }
                FlurryTypes.onEvent(FlurryTypes.EVENT_RATE_APP);
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.settings_about_market_link) + BUILD.PACKAGE_NAME));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } catch (Exception e) {
                    if (LogSettings.MARKET){
                        MktLog.e(TAG, "rate.onClick e:"+e);
                    }
                }
            }
        });
    }

    @Override
    protected void onStart() {
    	super.onStart();
    	FlurryTypes.onStartSession(this);
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	FlurryTypes.onEndSession(this);
    }

    private String getBuildVersion() {
        if (LogSettings.QA) {
            return AppInfo.getBuildVersion(this, true);
        } else {
            return AppInfo.getBuildVersion(this, false);
        }
    }

}
