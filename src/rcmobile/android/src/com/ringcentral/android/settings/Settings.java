/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.DialogInterface.OnDismissListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConfig;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.Updatable;
import com.ringcentral.android.UpdateRequestReceiver;
import com.ringcentral.android.api.RequestInfoStorage;
import com.ringcentral.android.api.network.HttpClientFactory;
import com.ringcentral.android.api.network.NetworkManager;
import com.ringcentral.android.api.network.NetworkManagerNotifier;
import com.ringcentral.android.phoneparser.PhoneNumber;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.DndManager;
import com.ringcentral.android.utils.NetworkUtils;
import com.ringcentral.android.utils.PhoneUtils;
import com.ringcentral.android.utils.UserInfo;
import com.ringcentral.android.utils.ui.LoginCredentialsEditor;
import com.ringcentral.android.utils.ui.widget.LoginErrorDialog;
import com.ringcentral.android.utils.ui.widget.LoginWaitingDialog;
import com.ringcentral.android.utils.ui.widget.ToggleButtonWidget;

public class Settings extends Activity implements Updatable, NetworkManagerNotifier {

    private static String TAG = "[RC]Settings";

    static final int SETTINGS_DEFAULT = 0;
    static final int SETTINGS_OUTGOING_CALLS = 1;
    static final int SETTINGS_GENERAL = 2;
    
    private static final int WAITING_DIALOG = 1;
    private static final int ERROR_DIALOG_LOGIN_FAILED = 2;
    private static final int ERROR_DIALOG_AIRPLANE_MODE = 3;
    private static final int ERROR_DIALOG_NO_NETWORK = 4;

    private DNDCallbackIntentReceiver dndIntentReceiver;
    private UpdateRequestReceiver updateIntentReceiver;

    private Handler mNetworkManagerNotifierHandler;
    private Message mNetworkManagerNotifierMessage;

    private boolean mFinished = false;

    private String mPreviousLogin;
    private String mPreviousExt;
    private String mPreviousPassword;

    private boolean mLoginCancelledByUser;
    
    private AlertDialog m_voip_options_dialog = null;
    
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(dndIntentReceiver);
        unregisterReceiver(updateIntentReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlurryTypes.onEventScreen(FlurryTypes.EVENT_SCREEN_OPEN, FlurryTypes.SCR_SETTING);
        dndIntentReceiver = new DNDCallbackIntentReceiver();
        IntentFilter dndIntentFilter = new IntentFilter(RCMConstants.ACTION_DND_STATUS_CHANGED);
        registerReceiver(dndIntentReceiver, dndIntentFilter);

        updateIntentReceiver = new UpdateRequestReceiver(this, RequestInfoStorage.GET_CALLER_IDS);
        IntentFilter intentFilter = new IntentFilter(RCMConstants.ACTION_NETWORK_REQUEST_END);
        registerReceiver(updateIntentReceiver, intentFilter);
        buildSettingsLayout();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);

        buildSettingsLayout();
        
        mFinished = false;
        mLoginCancelledByUser = false;
        mNetworkManagerNotifierHandler = new NetworkManagerNotifierHandler();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mFinished = true;
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	FlurryTypes.onStartSession(this);
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	FlurryTypes.onEndSession(this);
    }
    
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        default:
            return super.onCreateDialog(id);
            
        case WAITING_DIALOG:
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "User: Show waiting dialog: onCreateDialog()");
            }
            
            LoginWaitingDialog lwd = new LoginWaitingDialog(this);
            lwd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    mLoginCancelledByUser = ((LoginWaitingDialog)dialog).cancelledByUser();
                    
                    if (LogSettings.MARKET) {
                        MktLog.i(TAG, "Login progress dialog cancelled " +
                                (mLoginCancelledByUser ? "by user" : "(not by user)"));
                    }
                
                    if (mLoginCancelledByUser) {
                        NetworkManager.getInstance().reclaimLogin(
                                Settings.this, mPreviousLogin, mPreviousExt, mPreviousPassword);
                    }
                }
            });
            
            return lwd;
            
        case ERROR_DIALOG_LOGIN_FAILED:
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "User: Show login failed dialog");
            }
            return new LoginErrorDialog(this);            

        case ERROR_DIALOG_AIRPLANE_MODE:
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "User: Show network not available dialog (airplane mode)");
            }
            return new LoginErrorDialog(this, getString(R.string.network_not_available_airplane_mode));            

        case ERROR_DIALOG_NO_NETWORK:
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "User: Show network not available dialog");
            }
            return new LoginErrorDialog(this, getString(R.string.network_not_available));
        }
    }
  
    @Override
    protected void onPrepareDialog (int id, Dialog dialog) {
        
        super.onPrepareDialog(id, dialog);
        switch (id) {
        case WAITING_DIALOG:
            ((LoginWaitingDialog)dialog).prepare();
            mLoginCancelledByUser = false;
            break;

        case ERROR_DIALOG_LOGIN_FAILED:
            ((LoginErrorDialog)dialog).setMessage(this, mNetworkManagerNotifierMessage);
            break;

        default:
            break;
        }
    }
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED && hasFocus){
            PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.SETTINGS_OPENING);
        }
    }
    
    private void showLoginDialog() {
        if (NetworkUtils.isAirplaneMode(this)) {
            showDialog(ERROR_DIALOG_AIRPLANE_MODE);
            return;
        }

        if (NetworkUtils.getNetworkState(this) == NetworkUtils.NetworkState.NONE) {
            showDialog(ERROR_DIALOG_NO_NETWORK);
            return;
        }
    
        new ReloginDialog().show();
    }

    private void buildSettingsLayout() {
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // See Settings5.buildLayout method where all menus items are reflected in comments
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        
    	
        final int config = getIntent().getIntExtra(RCMConstants.EXTRA_SETTINGS5, SETTINGS_DEFAULT);
        
        View item;
        /*
         * Settings -> Divider
         */
        item = findViewById(R.id.divider_settings);
        if (config == SETTINGS_DEFAULT) {
            item.setVisibility(View.VISIBLE);
        } else {
            item.setVisibility(View.GONE);
        }
        
        /*
         * Settings -> Header
         */
        item = findViewById(R.id.header);
        if (config == SETTINGS_GENERAL) {
            item.setVisibility(View.VISIBLE);
            ((TextView)item).setText(R.string.settings5_header_general);
        } else if (config == SETTINGS_OUTGOING_CALLS) {
            item.setVisibility(View.VISIBLE);
            ((TextView)item).setText(R.string.settings5_header_outgoing_calls);
        } else {
            item.setVisibility(View.GONE);
        }
        
        /*
         * Settings -> Login
         */
        item = findViewById(R.id.settings_login);
        if (config == SETTINGS_DEFAULT) {
            item.setVisibility(View.VISIBLE); 
            
            ((TextView) item.findViewById(R.id.name)).setText(R.string.login_string);
            ((TextView) item.findViewById(R.id.label)).setText(UserInfo.getAccountLabel(this));
            
            item.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    showLoginDialog();
                }
            });
        } else {
            item.setVisibility(View.GONE);
        }

        /*
         * Settings -> Logout
         */
        item = findViewById(R.id.settings_logout);
        if (config == SETTINGS_DEFAULT || config == SETTINGS_GENERAL) {
            item.setVisibility(View.VISIBLE);
            
            ((TextView) item.findViewById(R.id.name)).setText(R.string.logout_string);
            
            TextView textView = (TextView) item.findViewById(R.id.label);
            if (config == SETTINGS_GENERAL) {
                textView.setVisibility(View.VISIBLE);
                textView.setText(UserInfo.getAccountLabel(this));
            } else {
                textView.setVisibility(View.GONE);
            }
            
            item.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: Logout");
                    }
                    FlurryTypes.onEvent(FlurryTypes.EVENT_LOG_OUT);
                  /*  String accountNumber = "";
                    String accountExtension = "";
                    String fAccountNumber = RCMProviderHelper.getLoginNumber(Settings.this);
                    if (!TextUtils.isEmpty(fAccountNumber)) {
                        accountNumber = fAccountNumber;
                        String fAccountExtension = RCMProviderHelper.getLoginExt(Settings.this);
                        if (!TextUtils.isEmpty(fAccountExtension)) {
                            accountExtension = fAccountExtension.trim();
                        }
                    }
                    */
                    //Clear service API version at logout so that it is re-checked from server at next login 
                    RCMProviderHelper.saveServiceApiVersion(Settings.this, "");
                    
                    if (config == SETTINGS_GENERAL) {
                        RingCentralApp.logoutForNewSettings(Settings.this);
                    } else if (config == SETTINGS_DEFAULT){
                        RingCentralApp.logoutForOldSettings(Settings.this);
                    }
                    
                    Settings.this.finish();
                }
           });
        } else {
            item.setVisibility(View.GONE);            
        }

        /*
         * Settings -> Ringout
         */
        item = findViewById(R.id.settings_ringout_mode);
        if (config == SETTINGS_DEFAULT || config == SETTINGS_OUTGOING_CALLS) {
            item.setVisibility(View.VISIBLE);

            ((TextView) item.findViewById(R.id.name)).setText(R.string.settings_ringout_mode_item_title);
            ((View) item.findViewById(R.id.imgright)).setVisibility(View.INVISIBLE); // updating of UI for AB-209
            item.findViewById(R.id.label).setVisibility(View.GONE);
            item.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: RingOut Mode");
                    }
                    FlurryTypes.onEvent(FlurryTypes.EVENT_RINGOUT_OPEN);
                    startActivity(new Intent(Settings.this, RingOutMode.class));
                }
            });
        } else {
            item.setVisibility(View.GONE);
        }

        /*
         * Settings -> VoIP
         */
        item = findViewById(R.id.settings_voip_calling);   
        if (BUILD.VOIP_ENABLED
                && (config == SETTINGS_DEFAULT || config == SETTINGS_OUTGOING_CALLS)
                && RCMProviderHelper.isVoipEnabled_Env_Acc_SipFlagHttpReg(this)) {
        	
            item.setVisibility(View.VISIBLE);
        	
            ((TextView) item.findViewById(R.id.name)).setText(R.string.settings_voip_section_header);
            ((View) item.findViewById(R.id.imgright)).setVisibility(View.INVISIBLE);  // updating of UI for AB-209
            
            boolean turnOffVoipByDefault = BUILD.isTos911Enabled && !RCMProviderHelper.isTos911Accepted(Settings.this);
            
            setVoipStatusLabel( RCMProviderHelper.isVoipSwitchedOn_UserVoip(this) && !turnOffVoipByDefault,
            		RCMProviderHelper.isVoipSwitchedOn_UserWiFi(this), 
            		RCMProviderHelper.isVoipSwitchedOn_User3g4g(this));

            item.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                	if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: VOIP Options");
                    }
                	
                	Intent i = new Intent(Settings.this, VoipSettings.class);
                    startActivity(i);
                }
            });
        } else {
            item.setVisibility(View.GONE);
        }

        /*
         * Settings -> Caller IDs
         */
        item = findViewById(R.id.settings_caller_ids);
        if ((config == SETTINGS_DEFAULT || config == SETTINGS_OUTGOING_CALLS)
                && UserInfo.callerIdsAllowed() == true) {
            item.setVisibility(View.VISIBLE);

            ((TextView) item.findViewById(R.id.name)).setText(R.string.settings_callerid_item_title);
            ((ImageView) item.findViewById(R.id.imgright)).setImageResource(R.drawable.ic_list_link);
    
            String savedPhone = PhoneUtils.getLocalCanonical(RCMProviderHelper.getCallerID(this));
            if (savedPhone != null && !savedPhone.equals("")) {
                item.findViewById(R.id.label).setVisibility(View.VISIBLE);
                ((TextView) item.findViewById(R.id.label)).setText(savedPhone);
            } else {
                item.findViewById(R.id.label).setVisibility(View.GONE);
            }
    
            item.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: CallerIDs");
                    }
                	final CallerIDDialog dialog = new CallerIDDialog(Settings.this);
                    dialog.setOnDismiss(new OnDismissListener() {
                        public void onDismiss(DialogInterface dialogInterface) {
                        	dialog.onDismiss(dialogInterface);
                            buildSettingsLayout();
                        }
                    });
                    dialog.show();
                }
            });
        } else {
            item.setVisibility(View.GONE);            
        }
        
        /*
         * Settigns -> Outgoing Fax
         */
        item = findViewById(R.id.settings_outgoing_fax);
        if (config == SETTINGS_OUTGOING_CALLS) {
            item.setVisibility(View.VISIBLE);

            ((TextView) item.findViewById(R.id.name)).setText(R.string.settings_outgoing_fax);
            //link is needed by Product requirements ;o)
            ((View) item.findViewById(R.id.imgright)).setVisibility(View.INVISIBLE); // updating of UI for AB-209
            item.findViewById(R.id.label).setVisibility(View.GONE);
    
            item.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: Fax Settings");
                    }
                    Uri uri = Uri.parse(RCMConfig.get_WEB_SETTINGS_FAX_URL(Settings.this));
                    if (LogSettings.QA) {
                        QaLog.i(TAG, "URI: Fax Settings: " + uri.toString());
                    }
                    
                    startActivity(new Intent(Intent.ACTION_VIEW, uri));
                }
            });
        } else {
            item.setVisibility(View.GONE);            
        }

        /*
         * Settings DND version 4
         */
        item = findViewById(R.id.settings_dnd_v4);
        if ((config == SETTINGS_DEFAULT) && UserInfo.dndAllowed() ) {
            item.setVisibility(View.VISIBLE);
            
            ((TextView) item.findViewById(R.id.name)).setText(R.string.settings_dnd_on_off_title);
            item.findViewById(R.id.label).setVisibility(View.GONE);
            ((ToggleButton) item.findViewById(R.id.toggle)).setChecked(DndManager.getInstance().getDndStatus(this));

            item.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: Set DND version 4");
                    }
                    if (NetworkUtils.isRCAvaliable(Settings.this)) {
                    	FlurryTypes.onEvent(FlurryTypes.EVENT_DND_CHANGED, ((ToggleButtonWidget) view).isChecked()?FlurryTypes.DND_OPTIONS_ON:FlurryTypes.DND_OPTIONS_OFF);
                        DndManager.getInstance().setDndStatus(Settings.this, ((ToggleButtonWidget) view).isChecked());
                    } else {
                        ((ToggleButtonWidget) view).setChecked(false);
                        DNDialog.showDNDNetworkErrorDialog(Settings.this);
                    }
                }
            });
        } else {
            item.setVisibility(View.GONE);            
        }
        
        /*
         * Settings DND version 5
         * 
         * Don't show DND for RCFax tier, and for DEPARTMENT, TMO, AO extensions
         */
        item = findViewById(R.id.settings_dnd_v5);
        if (config == SETTINGS_GENERAL && UserInfo.dndAllowed()) {
            item.setVisibility(View.VISIBLE);
            
            ((TextView) item.findViewById(R.id.name)).setText(R.string.settings_dnd_on_off_title);
            item.findViewById(R.id.label).setVisibility(View.GONE);
            ((ImageView) item.findViewById(R.id.imgright)).setImageResource(R.drawable.ic_list_link);

            item.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: Set DND version 5");
                    }                    
                    DNDialog.showDNDDialog(Settings.this);                    
                }
            });
        } else {
            item.setVisibility(View.GONE);            
        }

        /*
         * Settings -> Tell-a-Friend
         */
        item = findViewById(R.id.settings_tell_a_friend);
        if (BUILD.SETTINGS_TELLFRIEND_ENABLED
                && config == SETTINGS_GENERAL) {
            item.setVisibility(View.VISIBLE);

            ((TextView) item.findViewById(R.id.name)).setText(R.string.settings_item_tell_a_friend_title);
            item.findViewById(R.id.label).setVisibility(View.GONE);

            item.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: Tell a Friend");
                    }
                    FlurryTypes.onEvent(FlurryTypes.EVENT_TELL_FRIEND);
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: Tell a Friend");
                    }
                    Uri uri = Uri.parse(RCMConfig.get_WEB_SETTINGS_TELL_FRIEND_URL(Settings.this));
                    if (LogSettings.QA) {
                        QaLog.i(TAG, "URI: Tell a Friend: " + uri.toString());
                    }
                    
                    startActivity(new Intent(Intent.ACTION_VIEW, uri));
                }
            });
        } else {
            item.setVisibility(View.GONE);            
        }

        /*
         * Settings -> About
         */
        item = findViewById(R.id.settings_about_ringcentral);
        if (config == SETTINGS_DEFAULT || config == SETTINGS_GENERAL) {
            item.setVisibility(View.VISIBLE);

            if (config == SETTINGS_GENERAL) {
                ((TextView) item.findViewById(R.id.name)).setText(R.string.settings_about_title);
            } else {
                ((TextView) item.findViewById(R.id.name)).setText(R.string.settings_about_title);    
            }
            ((ImageView) item.findViewById(R.id.imgright)).setImageResource(R.drawable.ic_list_link);
            item.findViewById(R.id.label).setVisibility(View.GONE);

            item.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "User: About");
                    }
                    FlurryTypes.onEvent(FlurryTypes.EVENT_ABOUT_VISITED);
                    
                    Intent i = new Intent(Settings.this, About.class);
                    i.putExtra(RCMConstants.EXTRA_SETTINGS5, config);
                    startActivity(i);
                }
            });
        } else {
            item.setVisibility(View.GONE);            
        }
    }

    /**
     * Update VoIP status string
     */
    private void setVoipStatusLabel(boolean voipEnabled, boolean voipOverWifi, boolean voipOver3g) {
        int id = R.string.settings_voip_calling_label_OFF;
        
        if( voipEnabled ){
            if (voipOverWifi && voipOver3g) {
                id = R.string.settings_voip_calling_label_ON_WiFi_3G;
            } else if (voipOverWifi) {
                id = R.string.settings_voip_calling_label_ON_WiFi;
            }
        }

        ((TextView) findViewById(R.id.settings_voip_calling).findViewById(R.id.label)).setText(id);
    }
    
    
    public void onUpdateRequest() {
        buildSettingsLayout();
    }

    @Override
    public NetworkManagerNotifier getNotifier() {
        return this;
    }

    @Override
    public void statusChanged(Bundle status, Object result) {
        if (!isFinishing() && !mFinished
                && status.getInt(NetworkManager.REQUEST_INFO.TYPE) == RequestInfoStorage.LOGIN_CHECK) {

            mNetworkManagerNotifierMessage = mNetworkManagerNotifierHandler.obtainMessage();
            mNetworkManagerNotifierMessage.setData(status);
            mNetworkManagerNotifierHandler.sendMessage(mNetworkManagerNotifierMessage);
        }
    }

    
    private class NetworkManagerNotifierHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            int request_type = msg.getData().getInt(NetworkManager.REQUEST_INFO.TYPE);
            int status = msg.getData().getInt(NetworkManager.REQUEST_INFO.STATUS);
            
            switch (status) {
            case RCMDataStore.SyncStatusEnum.SYNC_STATUS_NOT_LOADED:
                break;
            case RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING:
                break;
            case RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED:
                if (request_type == RequestInfoStorage.LOGIN_CHECK && !mLoginCancelledByUser) {
                    dismissDialog(WAITING_DIALOG);
                    HttpClientFactory.shutdown();
                    RingCentralApp.restartApplication(
                            RCMProviderHelper.getLoginNumber(Settings.this),
                            RCMProviderHelper.getLoginExt(Settings.this),
                            RCMProviderHelper.getLoginPassword(Settings.this),
                            true,
                            false,
                            true,
                            Settings.this);
                    Settings.this.finish();
                }
                break;
            case RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR:
                if (request_type == RequestInfoStorage.LOGIN_CHECK && !mLoginCancelledByUser) {
                    NetworkManager.getInstance().reclaimLogin(
                            Settings.this, mPreviousLogin, mPreviousExt, mPreviousPassword);
                    dismissDialog(WAITING_DIALOG);
                    showDialog(ERROR_DIALOG_LOGIN_FAILED);
                }
                break;
            default:
                if (LogSettings.ENGINEERING) {
                    EngLog.w(TAG, "statusChanged(), status unknown or not processed : " + status);
                }
            }
        }
    }

    
    private class DNDCallbackIntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LogSettings.ENGINEERING) {
                EngLog.d(TAG, "DNDCallbackIntentReceiver(): update dnd in settings");
            }
            buildSettingsLayout();
        }
    }


    private class ReloginDialog extends Dialog {
        
        private EditText mPhoneField;
        private EditText mExtField;
        private EditText mPasswordField;
        
        private LoginCredentialsEditor editor;
        
        ReloginDialog() {
            super(Settings.this);

            setContentView(R.layout.relogin_form);
            setTitle(R.string.account_information);
            
            mPhoneField = (EditText) findViewById(R.id.phone);
            mExtField = (EditText) findViewById(R.id.extension);
            mPasswordField = (EditText) findViewById(R.id.password);
            
            editor = new LoginCredentialsEditor(mPhoneField, mExtField, mPasswordField);
            editor.init(RCMProviderHelper.getLoginNumber(Settings.this),
                    RCMProviderHelper.getLoginExt(Settings.this),
                    RCMProviderHelper.getLoginPassword(Settings.this));
            
            findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {

                public void onClick(View arg0) {
                    PhoneNumber phoneNumber = PhoneUtils.getPhoneNumber(mPhoneField.getText().toString().trim(), new PhoneNumber());
                    if (phoneNumber == null || phoneNumber.numRaw.length() == 0 || mPasswordField.getText() == null || mPasswordField.getText().toString().trim().length() == 0) {
                        Toast.makeText(Settings.this, getString(R.string.relogin_msg_nodata), 3000).show();
                    } else {
                        showDialog(WAITING_DIALOG);
                        dismiss();

                        mPreviousLogin = RCMProviderHelper.getLoginNumber(Settings.this);
                        mPreviousExt = RCMProviderHelper.getLoginExt(Settings.this);
                        mPreviousPassword = RCMProviderHelper.getLoginPassword(Settings.this);
                        
                        NetworkManager.getInstance().loginCheck(
                                Settings.this,
                                phoneNumber.numRaw,
                                mExtField.getText().toString().trim(),
                                mPasswordField.getText().toString().trim(),
                                true);
                    }
                }
            });

            findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {

                public void onClick(View arg0) {
                    dismiss();
                }
            });
        
        }
    
    }

}