/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.api.network.NetworkManager;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.contacts.Projections.CallerID;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.provider.RCMDataStore.CallerIDsTable;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.PhoneUtils;

public class CallerIDDialog implements DialogInterface.OnClickListener, DialogInterface.OnDismissListener {
    private static String TAG = "[RC] CallerIDDialog";
    private static boolean DEBUG = false;

    private Context mContext;
    private AlertDialog mDialog;
    private Cursor mCursor;
    private CallerIDAdapter mCallerIDAdapter;
    private String mCurrentID;
    private String mServerMainNumberTagString;
    private String mReplaceServerMainNumberTagString;

    public CallerIDDialog(Context context) {
        mContext = context;
        mCurrentID = RCMProviderHelper.getCallerID(context);
        Uri uri = UriHelper.getUri(RCMProvider.CALLER_IDS, RCMProviderHelper.getCurrentMailboxId(context));
        mCursor = context.getContentResolver().query(uri, CallerID.SUMMARY_PROJECTION, null, null, null);

        if (mCursor.moveToFirst()) {
            mCallerIDAdapter = new CallerIDAdapter();
            mCallerIDAdapter.changeCursor(mCursor);
            AlertDialog.Builder dialogBuilder = RcAlertDialog.getBuilder(mContext)
                    .setAdapter(mCallerIDAdapter, this)
                    .setTitle(R.string.settings_callerid_dialog_title);

            mDialog = dialogBuilder.create();
            try {
                ListView lv = mDialog.getListView();
                lv.setSelector(R.drawable.rc_settings_list_selector_bg);
            } catch (java.lang.Throwable error) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "setListView failed:", error);
                }
            }
        } else {
            AlertDialog.Builder messageBuilder = RcAlertDialog.getBuilder(mContext)
                    .setTitle(R.string.dialog_title_error)
                    .setMessage(R.string.settings_callerid_dialog_msg_no_caller_ids)
                    .setIcon(R.drawable.symbol_error)
                    .setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int whichButton) {
                    NetworkManager.getInstance().getCallerIds(mContext, true);
                }
            });
            mDialog = messageBuilder.create();
        }
        
        /**
         * Replace "MainNumber" tag by "Main"
         */
        Resources res = context.getResources();
        mServerMainNumberTagString = res.getString(R.string.callerid_tag_server_main_number_string);
        mReplaceServerMainNumberTagString = res.getString(R.string.callerid_tag_replace_server_main_number_string);
    }

    public void show() {
    	FlurryTypes.onEvent(FlurryTypes.EVENT_CALLERID_SETTINGS_OPENED);
        mDialog.show();
    }

    public void setOnDismiss(OnDismissListener onDismissListener) {
        mDialog.setOnDismissListener(onDismissListener);
    }

    public void onClick(DialogInterface dialog, int which) {
        mCursor.moveToPosition(which);
        String phoneNumber = mCursor.getString(mCursor.getColumnIndex(CallerIDsTable.JEDI_NUMBER));
        RCMProviderHelper.saveCallerID(mContext, phoneNumber);
        if(!mCurrentID.equals(phoneNumber))
        	FlurryTypes.onEvent(FlurryTypes.EVENT_CALLERID_CHANGED);
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "User: Selected caller id " + phoneNumber);
        }
    }

	public void onDismiss(DialogInterface dialog) {
		try {
			if (mCursor != null) {
				mCursor.close();
				mCursor = null;
			}
		} catch (java.lang.Throwable error) {
			if (LogSettings.QA) {
				QaLog.e(TAG, "onDismiss() failed: ", error);
			}
		}
	}

    private static final class CallerIDListItemCache {
        public TextView numberText;
        public TextView typeText;
        public RadioButton checkButton;
    }

    private final class CallerIDAdapter extends ResourceCursorAdapter {
        public CallerIDAdapter() {
            super(mContext, R.layout.simple_list_item_single_choice, null);
        }

        @Override
        protected void onContentChanged() {
            if (DEBUG && LogSettings.ENGINEERING) {
                EngLog.d(TAG, "onContentChanged(): ");
            }
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            final View view = super.newView(context, cursor, parent);
            final CallerIDListItemCache cache = new CallerIDListItemCache();
            cache.numberText = (TextView) view.findViewById(android.R.id.text1);
            cache.typeText = (TextView) view.findViewById(android.R.id.text2);
            cache.checkButton = (RadioButton) view.findViewById(R.id.btnSelect);
            view.setTag(cache);
            return view;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            final CallerIDListItemCache cache = (CallerIDListItemCache) view.getTag();
            String number = cursor.getString(CallerID.NUMBER_COLUMN_INDEX);
            cache.numberText.setText(PhoneUtils.getLocalCanonical(number));
            String tag = cursor.getString(CallerID.TYPE_COLUMN_INDEX);
            try {
                if (tag.compareTo(mServerMainNumberTagString) == 0) {
                    tag = mReplaceServerMainNumberTagString;
                }
            } catch (Exception ex) {
            }
            cache.typeText.setText(tag);
            if (mCurrentID.equals(number)) {
                cache.checkButton.setChecked(true);
            } else {
                cache.checkButton.setChecked(false);
            }
        }
    }
}
