/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.contacts.ContactPhoneNumber;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.phoneparser.PhoneNumberFormatter;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.provider.RCMDataStore.AccountInfoTable;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.statistics.GAnalyticsHandler;
import com.ringcentral.android.utils.DeviceUtils;
import com.ringcentral.android.utils.PhoneUtils;

public class RingOutMode extends Activity {
    private static final String TAG = "[RC]RingOutMode";
    
    private boolean mInitTracker = false;

    private OnClickListener anotherPhoneNumberListener = new OnClickListener() {

        public void onClick(View view) {
            LinearLayout phones = (LinearLayout) findViewById(R.id.another_phone_numbers_view);
            for (int counter = 0; counter < phones.getChildCount(); counter++) {
                ((RadioButton) phones.getChildAt(counter).findViewById(R.id.radiobox)).setChecked(false);
            }
            RCMProviderHelper.saveRingoutAnotherPhone(RingOutMode.this, (String) view.getTag());
            ((RadioButton) view.findViewById(R.id.radiobox)).setChecked(true);
            showCurrentPhoneNumber(RCMProviderHelper.getRingoutMode(RingOutMode.this));
        }
    };

    @Override
    protected void onStart() {
    	super.onStart();
    	FlurryTypes.onStartSession(this);
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	FlurryTypes.onEndSession(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.ringout_mode);

        buildLayout();
        setMode(RCMProviderHelper.getRingoutMode(this));
    }

    private void showCurrentPhoneNumber(int mode) {
        ((TextView) findViewById(R.id.my_android_row).findViewById(R.id.label)).setText(PhoneUtils.getLocalCanonical(DeviceUtils.getDeviceNumber(RingOutMode.this)));
        ((TextView) findViewById(R.id.another_phone_row).findViewById(R.id.label)).setText(PhoneUtils.getLocalCanonical(RCMProviderHelper.getRingoutAnotherPhone(RingOutMode.this)));

        if (mode == AccountInfoTable.RINGOUT_MODE_MY_ANDROID) {
            ((RadioButton) findViewById(R.id.my_android_row).findViewById(R.id.radiobox)).setChecked(true);
            ((RadioButton) findViewById(R.id.another_phone_row).findViewById(R.id.radiobox)).setChecked(false);

            ((TextView) findViewById(R.id.another_phone_row).findViewById(R.id.label)).setTextColor(getResources().getColor(R.color.textColorGray));
            ((TextView) findViewById(R.id.another_phone_row).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.textColorGray));
            ((TextView) findViewById(R.id.my_android_row).findViewById(R.id.label)).setTextColor(getResources().getColor(R.color.textColorBlue));
            ((TextView) findViewById(R.id.my_android_row).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.textColorBlack));

            findViewById(R.id.another_phone_layout).setVisibility(View.GONE);
        } else {
            ((RadioButton) findViewById(R.id.my_android_row).findViewById(R.id.radiobox)).setChecked(false);
            ((RadioButton) findViewById(R.id.another_phone_row).findViewById(R.id.radiobox)).setChecked(true);

            ((TextView) findViewById(R.id.my_android_row).findViewById(R.id.label)).setTextColor(getResources().getColor(R.color.textColorGray));
            ((TextView) findViewById(R.id.my_android_row).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.textColorGray));
            ((TextView) findViewById(R.id.another_phone_row).findViewById(R.id.label)).setTextColor(getResources().getColor(R.color.textColorBlue));
            ((TextView) findViewById(R.id.another_phone_row).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.textColorBlack));

            findViewById(R.id.another_phone_layout).setVisibility(View.VISIBLE);
        }
    }

    private void setMode(int mode) {
    	if(RCMProviderHelper.getRingoutMode(this)!=mode)
    		FlurryTypes.onEvent(FlurryTypes.EVENT_RINGOUT_CHANGED, (mode==AccountInfoTable.RINGOUT_MODE_MY_ANDROID)?
    				FlurryTypes.RINGOUT_MY_ANDR:
    				FlurryTypes.RINGOUT_OTHER_PHONE);
        RCMProviderHelper.setRingoutMode(RingOutMode.this, mode);
        showCurrentPhoneNumber(mode);
    }

    private void buildLayout() {
        ((Button) findViewById(R.id.another_phone_add_button)).setText(
                RCMProviderHelper.getCustomPhoneNumber(this) != null && PhoneNumberFormatter.isValidAsMobilePhoneNumber(RCMProviderHelper.getCustomPhoneNumber(this))
                        ? R.string.another_phone_edit_number
                        : R.string.another_phone_add_number
        );

        ((TextView) findViewById(R.id.another_phone_requires_you_to_enter_1).findViewById(R.id.name)).setText(R.string.confirm_connection);

        ((TextView) findViewById(R.id.my_android_row).findViewById(R.id.name)).setText(R.string.my_android);
        findViewById(R.id.my_android_row).setOnClickListener(new OnClickListener() {

            public void onClick(View view) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: My Android");
                }
                setMode(AccountInfoTable.RINGOUT_MODE_MY_ANDROID);
            }
        });

        ((TextView) findViewById(R.id.another_phone_row).findViewById(R.id.name)).setText(R.string.another_phone);
        findViewById(R.id.another_phone_row).setOnClickListener(new OnClickListener() {

            public void onClick(View view) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Another phone");
                }
                setMode(AccountInfoTable.RINGOUT_MODE_ANOTHER_PHONE);
            }
        });

        Cursor cursorForwardingNumbers = RingOutMode.this.getContentResolver().query(UriHelper.getUri(RCMProvider.FW_NUMBERS,
                RCMProviderHelper.getCurrentMailboxId(RingOutMode.this)), null, null, null, null);

        LinearLayout phones = (LinearLayout) findViewById(R.id.another_phone_numbers_view);
        phones.removeAllViewsInLayout();
        String number;
        View phone = null;
        boolean marked = false;

        if (cursorForwardingNumbers != null) {
            cursorForwardingNumbers.moveToPosition(-1);
            while (cursorForwardingNumbers.moveToNext()) {
                phone = View.inflate(this, R.layout.list_row_btn_radiobox, null);
                number = cursorForwardingNumbers.getString(cursorForwardingNumbers.getColumnIndex(RCMDataStore.FwNumbersTable.JEDI_NUMBER));
                ((TextView) phone.findViewById(R.id.name)).setText(PhoneUtils.getLocalCanonical(number));
                ((TextView) phone.findViewById(R.id.label)).setText(cursorForwardingNumbers.getString(cursorForwardingNumbers.getColumnIndex(RCMDataStore.FwNumbersTable.JEDI_TYPE)));
                phone.setTag(number);

                if (!marked && RCMProviderHelper.getRingoutAnotherPhone(RingOutMode.this).equalsIgnoreCase(number)) {
                    ((RadioButton) phone.findViewById(R.id.radiobox)).setChecked(true);
                    marked = true;
                }

                phone.setOnClickListener(anotherPhoneNumberListener);
                phones.addView(phone);
            }
            cursorForwardingNumbers.close();
        }

        // Custom number
        if (RCMProviderHelper.getCustomPhoneNumber(this) != null && PhoneNumberFormatter.isValidAsMobilePhoneNumber(RCMProviderHelper.getCustomPhoneNumber(this))) {
            phone = View.inflate(this, R.layout.list_row_btn_radiobox, null);
            ((TextView) phone.findViewById(R.id.name)).setText(PhoneUtils.getLocalCanonical(RCMProviderHelper.getCustomPhoneNumber(this)));
            ((TextView) phone.findViewById(R.id.label)).setText(R.string.another_phone_custom_number);
            phone.setTag(RCMProviderHelper.getCustomPhoneNumber(this));
            phone.setOnClickListener(anotherPhoneNumberListener);
            ((RadioButton) phone.findViewById(R.id.radiobox)).setChecked(RCMProviderHelper.getRingoutAnotherPhone(RingOutMode.this).equalsIgnoreCase(RCMProviderHelper.getCustomPhoneNumber(this)) && !marked);
            if (RCMProviderHelper.getRingoutAnotherPhone(RingOutMode.this).equalsIgnoreCase(RCMProviderHelper.getCustomPhoneNumber(this)) && !marked) {
                RCMProviderHelper.saveRingoutAnotherPhone(RingOutMode.this, RCMProviderHelper.getCustomPhoneNumber(this));
                marked = true;
            }
            phones.addView(phone);
        }

        // Device number
        phone = View.inflate(this, R.layout.list_row_btn_radiobox, null);
        ((TextView) phone.findViewById(R.id.name)).setText(PhoneUtils.getLocalCanonical(DeviceUtils.getDeviceNumber(RingOutMode.this)));
        ((TextView) phone.findViewById(R.id.label)).setText(R.string.my_android_phone_number);
        phone.setTag(DeviceUtils.getDeviceNumber(RingOutMode.this));
        phone.setOnClickListener(anotherPhoneNumberListener);
        if (RCMProviderHelper.getRingoutAnotherPhone(RingOutMode.this).equalsIgnoreCase(DeviceUtils.getDeviceNumber(RingOutMode.this)) || !marked) {
            RCMProviderHelper.saveRingoutAnotherPhone(RingOutMode.this, DeviceUtils.getDeviceNumber(RingOutMode.this));
            ((RadioButton) phone.findViewById(R.id.radiobox)).setChecked(true);
        } else {
            ((RadioButton) phone.findViewById(R.id.radiobox)).setChecked(false);
        }
        phones.addView(phone);

        findViewById(R.id.another_phone_add_button).setOnClickListener(new OnClickListener() {

            public void onClick(View view) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Add Custom Number");
                }
                showAddCustomNumberDialog(RCMProviderHelper.getCustomPhoneNumber(RingOutMode.this));
            }
        });

        ((ToggleButton) findViewById(R.id.another_phone_requires_you_to_enter_1).findViewById(R.id.toggle)).setChecked(RCMProviderHelper.isConfirmConnection(RingOutMode.this));
        findViewById(R.id.another_phone_requires_you_to_enter_1).setOnClickListener(new OnClickListener() {

            public void onClick(View view) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Confirm Connection");
                }
                FlurryTypes.onEvent(FlurryTypes.EVENT_CONNECTION_CHANGED, ((ToggleButton) view.findViewById(R.id.toggle)).isChecked()? 
            			FlurryTypes.CONNECTION_ON:
            			FlurryTypes.CONNECTION_OFF);
                RCMProviderHelper.setConfirmConnection(RingOutMode.this, ((ToggleButton) view.findViewById(R.id.toggle)).isChecked());
            }
        });

        if (getIntent().getBooleanExtra(RCMConstants.EXTRA_SHOW_DEVICE_NUMBER_DIALOG_AND_FINISH, false)) {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "First launch; show Device Number dialog");
            }
            showEnterDeviceNumberDialog(true);
        } else if (DeviceUtils.checkDeviceIMSI(this) == false || TextUtils.isEmpty(DeviceUtils.getDeviceNumber(this))) {            
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "SIM card changed or no device number in DB; show Device Number dialog");
            }
            GAnalyticsHandler.initTracker(this);
            mInitTracker = true;
            showEnterDeviceNumberDialog(false);
        }
    }
    
    @Override
    protected void onDestroy() {    	
    	super.onDestroy();
    	if(mInitTracker) {
    		GAnalyticsHandler.stopTracker();
    	}
    }

    private void showAddCustomNumberDialog(final String phoneNumber) {
    	FlurryTypes.onEvent(RCMProviderHelper.getCustomPhoneNumber(this) != null && PhoneNumberFormatter.isValidAsMobilePhoneNumber(RCMProviderHelper.getCustomPhoneNumber(this))?
    			FlurryTypes.EVENT_RINGOUT_ANOTHER_PHONE_CHANGED:
    			FlurryTypes.EVENT_RINGOUT_ANOTHER_PHONE_ADDED);
        LayoutInflater factory = LayoutInflater.from(RingOutMode.this);
        final View textEntryView = factory.inflate(R.layout.add_custom_number_dialog, null);
        final TextView txt = (TextView) textEntryView.findViewById(R.id.username_edit);
        txt.setHint(R.string.phone_number);
        txt.setInputType(InputType.TYPE_CLASS_PHONE);
        if (phoneNumber != null) {
            txt.setText(phoneNumber);
        }

        final Dialog dialog = new Dialog(this);
        dialog.setTitle(R.string.add_new_phone);
        dialog.setContentView(textEntryView);
        dialog.setCancelable(false);

        dialog.findViewById(R.id.cancelBtn).setOnClickListener(
                new OnClickListener() {

                    public void onClick(View arg0) {
                        if (LogSettings.MARKET) {
                            MktLog.d(TAG, "User: Add Custom Number: Cancel");
                        }
                        dialog.dismiss();
                    }
                });

        dialog.findViewById(R.id.deleteBtn).setOnClickListener(
                new OnClickListener() {

                    public void onClick(View arg0) {
                        if (LogSettings.MARKET) {
                            MktLog.d(TAG, "User: Add Custom Number: Delete");
                        }
                        RCMProviderHelper.saveCustomPhoneNumber(RingOutMode.this, "");
                        buildLayout();
                        showCurrentPhoneNumber(RCMProviderHelper.getRingoutMode(RingOutMode.this));
                        dialog.dismiss();
                    }
                });

        dialog.findViewById(R.id.saveBtn).setOnClickListener(
                new OnClickListener() {

                    public void onClick(View arg0) {
                        if (LogSettings.MARKET) {
                            MktLog.d(TAG, "User: Add Custom Number: Save");
                        }
                        final String customNumber = txt.getText().toString();
                        
                        ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumber(customNumber);
                        
                        if (cpn != null && cpn.isValid &&
                                PhoneNumberFormatter.isValidAsMobilePhoneNumber(customNumber.trim())) {
                            RCMProviderHelper.saveCustomPhoneNumber(RingOutMode.this, cpn.phoneNumber.numRaw);
                            buildLayout();
                            showCurrentPhoneNumber(RCMProviderHelper.getRingoutMode(RingOutMode.this));
                            dialog.dismiss();
                        } else {
                            if (LogSettings.MARKET) {
                                MktLog.i(TAG, "showAddCustomNumberDialog:warning (empty|invalid) number");
                            }
                            Toast.makeText(RingOutMode.this,
                                    PhoneNumberFormatter.isValidAsMobilePhoneNumber(customNumber.trim())
                                            ? getText(R.string.phone_number_empty)
                                            : getText(R.string.phone_number_invalid),
                                    4000).show();
                        }

                    }
                });
        dialog.show();

    }


    protected void showEnterDeviceNumberDialog(final boolean finishActivityOnExit) {
        final View textEntryView = LayoutInflater.from(RingOutMode.this).inflate(R.layout.alert_dialog_device_number, null);
        if (DeviceUtils.getLine1Number(this) != null) {
            ((TextView) textEntryView.findViewById(R.id.username_edit)).setText(DeviceUtils.getLine1Number(this));
            ((Button) textEntryView.findViewById(R.id.saveBtn)).requestFocus();
            ((Button) textEntryView.findViewById(R.id.saveBtn)).requestFocusFromTouch();
        }

        final AlertDialog dialog = RcAlertDialog.getBuilder(this)
                .setTitle(R.string.phone_number_not_found)
                .setMessage(R.string.enter_phone_number)
                .setView(textEntryView)
                .setIcon(R.drawable.symbol_exclamation)
                .setCancelable(false).create();

        ((Button) textEntryView.findViewById(R.id.saveBtn)).setOnClickListener(new OnClickListener() {

            public void onClick(View view) {
                TextView txt = (TextView) textEntryView.findViewById(R.id.username_edit);
                String deviceNumber = txt.getText().toString().trim();
                if (deviceNumber.length() > 0 && PhoneNumberFormatter.isValidAsMobilePhoneNumber(deviceNumber)) {
                    deviceNumber = deviceNumber.replaceFirst("\\+", "");
                    DeviceUtils.setDeviceNumber(RingOutMode.this, deviceNumber);
                    showCurrentPhoneNumber(RCMProviderHelper.getRingoutMode(RingOutMode.this));
                    dialog.dismiss();
                    if (finishActivityOnExit) {
                        finish();
                    }
                } else {
                    if (LogSettings.MARKET) {
                        MktLog.i(TAG, "showEnterDeviceNumberDialog:warning (empty|invalid) number");
                    }
                    Toast.makeText(RingOutMode.this,
                            PhoneNumberFormatter.isValidAsMobilePhoneNumber(deviceNumber.trim())
                                    ? getText(R.string.phone_number_empty) : getText(R.string.phone_number_invalid),
                            4000).show();
                }
            }
        });

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {

            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    dialog.findViewById(R.id.saveBtn).performClick();
                    return true;
                }
                if(keyCode == KeyEvent.KEYCODE_SEARCH){ // otherwise, dialog can be skipped with search button
					return true;
				}
                return false;
            }
        });

        dialog.show();
    }
}
