package com.ringcentral.android.settings;

import java.lang.reflect.Field;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ringcentral.android.R;
import com.ringcentral.android.provider.RCMDataStore.TSCConfigTable;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;

public class ConfigTSCActivity extends Activity {

	private static final String TAG = "ConfigTSCActivity";
	private ViewGroup tscSettingEnableLL;
	private View tunnelAddressLL;
	private View tunnelPortLL;
	private RadioGroup tunnelModeRG;
	private RadioGroup tscRedundancyRG;
	private CheckBox tscEnableCB;
	private CheckBox tscLoggingCB;
	private CheckBox debugCB;
	private CheckBox tunnelLoadBalanceCB;
	private TextView tunnelAddressTV;
	private TextView tunnelPortTV;

	private final String[] TUNNEL_MODE={
			"TCP",
			"UDP",
			"dtls",
			"AuthDisable"
		};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.config_tsc_activity);

		findView();
		initView();
		setListener();
	}

	private void findView() {
		tscSettingEnableLL = (ViewGroup) findViewById(R.id.tsc_setting_enable_ll);
		tunnelAddressLL = findViewById(R.id.tunnel_address_ll);
		tunnelPortLL = findViewById(R.id.tunnel_port_ll);
		tunnelModeRG = (RadioGroup) findViewById(R.id.tunnel_mode_rg);
		tscRedundancyRG = (RadioGroup) findViewById(R.id.tsc_redundancy_rg);
		tscEnableCB = (CheckBox) findViewById(R.id.tsc_enable_cb);
		tscLoggingCB = (CheckBox) findViewById(R.id.tsc_logging_cb);
		debugCB = (CheckBox) findViewById(R.id.debug_cb);
		tunnelLoadBalanceCB = (CheckBox) findViewById(R.id.tunnel_load_balance_cb);
		tunnelAddressTV = (TextView) findViewById(R.id.tunnel_address_tv);
		tunnelPortTV = (TextView) findViewById(R.id.tunnel_port_tv);
	}

	private void initView(){
		boolean isEnable=RCMProviderHelper.getTSCEnalbe(ConfigTSCActivity.this);
		tscEnableCB.setChecked(isEnable);
		setViewControlEnable(tscSettingEnableLL, isEnable);
		if (isEnable)
			tscSettingEnableLL.setBackgroundColor(Color.WHITE);
		else
			tscSettingEnableLL.setBackgroundColor(Color.LTGRAY);
		
		tunnelAddressTV.setText(RCMProviderHelper.getTSCTunnelAddress(ConfigTSCActivity.this));
		tunnelPortTV.setText(RCMProviderHelper.getTSCTunnelPort(ConfigTSCActivity.this));
		RadioButton selectedTunnelModeRB=(RadioButton)tunnelModeRG.getChildAt(RCMProviderHelper.getTSCTunnelMode(ConfigTSCActivity.this));
		selectedTunnelModeRB.setChecked(true);
		RadioButton selectedRedundancyRB=(RadioButton)tscRedundancyRG.getChildAt(RCMProviderHelper.getTSCRedundancy(ConfigTSCActivity.this));
		selectedRedundancyRB.setChecked(true);
		tscLoggingCB.setChecked(RCMProviderHelper.getTSCLogging(ConfigTSCActivity.this));
		debugCB.setChecked(RCMProviderHelper.getTSCDebug(ConfigTSCActivity.this));
		tunnelLoadBalanceCB.setChecked(RCMProviderHelper.getTSCTunnelLoadBalance(ConfigTSCActivity.this));
	}
	
	private void setListener() {

		tscEnableCB
				.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean arg1) {
						setViewControlEnable(tscSettingEnableLL, arg1);
						RCMProviderHelper.updateSingleValue(ConfigTSCActivity.this, RCMProvider.TSC_CONFIG, TSCConfigTable.TSC_ENABLE, arg1==true? "1":"0", null);
						if (arg1)
							tscSettingEnableLL.setBackgroundColor(Color.WHITE);
						else
							tscSettingEnableLL.setBackgroundColor(Color.LTGRAY);

					}
				});

		tunnelAddressLL.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				inputTunnelAddress();
			}
		});

		tunnelPortLL.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				inputTunnelPort();
			}

		});

		tunnelModeRG
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						Log.i(TAG, ((RadioButton) findViewById(checkedId))
								.getText().toString());
						int index=-1;
						String selectedMode=((RadioButton) findViewById(checkedId)).getText().toString();
						while(!selectedMode.equals(TUNNEL_MODE[++index]));
						RCMProviderHelper.updateSingleValue(ConfigTSCActivity.this, RCMProvider.TSC_CONFIG, TSCConfigTable.TUNNEL_MODE, index+"", null);
					}
				});

		tscRedundancyRG
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						Log.i(TAG, ((RadioButton) findViewById(checkedId))
								.getText().toString());
						int index=-1;
						String selectedRedundancy=((RadioButton) findViewById(checkedId)).getText().toString();
						while(!selectedRedundancy.equals(++index+""));
						RCMProviderHelper.updateSingleValue(ConfigTSCActivity.this, RCMProvider.TSC_CONFIG, TSCConfigTable.TSC_REDUNDANCY, index+"", null);
					}

				});

		tscLoggingCB
				.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean arg1) {
						RCMProviderHelper.updateSingleValue(ConfigTSCActivity.this, RCMProvider.TSC_CONFIG, TSCConfigTable.TSC_LOGGING, arg1==true? "1":"0", null);
					}
				});

		debugCB.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				RCMProviderHelper.updateSingleValue(ConfigTSCActivity.this, RCMProvider.TSC_CONFIG, TSCConfigTable.DEBUG, arg1==true? "1":"0", null);
			}
		});

		tunnelLoadBalanceCB
				.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean arg1) {
						RCMProviderHelper.updateSingleValue(ConfigTSCActivity.this, RCMProvider.TSC_CONFIG, TSCConfigTable.TUNNEL_LOAD_BALANCE, arg1==true? "1":"0", null);
					}
				});
	}

	protected void inputTunnelPort() {
		final EditText tunnelPortET = new EditText(this);
		tunnelPortET.setInputType(EditorInfo.TYPE_CLASS_PHONE);
		new AlertDialog.Builder(this)
				.setTitle(
						getResources().getString(
								R.string.tsc_setting_tunnel_port))
				.setIcon(android.R.drawable.ic_dialog_info)
				.setView(tunnelPortET)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int arg1) {
								String inputPort=tunnelPortET.getText().toString();
								if(isValidTunnelPort(inputPort)){
									RCMProviderHelper.updateSingleValue(ConfigTSCActivity.this, RCMProvider.TSC_CONFIG, TSCConfigTable.TUNNEL_PORT, tunnelPortET.getText().toString().trim(), null);
									tunnelPortTV.setText(inputPort);
									try {
								        Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
								        field.setAccessible(true);
								        field.set(dialog, true);
								    } catch (Exception e) {
								        e.printStackTrace();
								    }
								}else{
									try {
								        Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
								        field.setAccessible(true);
								        field.set(dialog, false);
								    } catch (Exception e) {
								        e.printStackTrace();
								    }
								}
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int arg1) {
						try {
					        Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
					        field.setAccessible(true);
					        field.set(dialog, true);
					    } catch (Exception e) {
					        e.printStackTrace();
					    }
					}
				}).show();
	}

	protected void inputTunnelAddress() {
		final EditText tunnelAddressET = new EditText(this);
		tunnelAddressET.setInputType(EditorInfo.TYPE_CLASS_PHONE);
		new AlertDialog.Builder(this)
				.setTitle(
						getResources().getString(
								R.string.tsc_setting_tunnel_address))
				.setIcon(android.R.drawable.ic_dialog_info)
				.setView(tunnelAddressET)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int arg1) {
								String inputIPAds=tunnelAddressET.getText().toString();
								if(isValidIPAddress(inputIPAds)){
									RCMProviderHelper.updateSingleValue(ConfigTSCActivity.this, RCMProvider.TSC_CONFIG, TSCConfigTable.TUNNEL_ADDRESS, tunnelAddressET.getText().toString().trim(), null);
									tunnelAddressTV.setText(tunnelAddressET.getText());
									try {
								        Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
								        field.setAccessible(true);
								        field.set(dialog, true);
								    } catch (Exception e) {
								        e.printStackTrace();
								    }
								}else{
									Toast.makeText(ConfigTSCActivity.this, "adf", Toast.LENGTH_SHORT).show();
									try {
								        Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
								        field.setAccessible(true);
								        field.set(dialog, false);
								    } catch (Exception e) {
								        e.printStackTrace();
								    }
								}
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int arg1) {
						try {
					        Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
					        field.setAccessible(true);
					        field.set(dialog, true);
					    } catch (Exception e) {
					        e.printStackTrace();
					    }
					}
				}).show();

	}

	protected void setViewControlEnable(View view, boolean arg1) {
		if (view instanceof ViewGroup) {
			int count = ((ViewGroup) view).getChildCount();
			for (int i = 0; i < count; i++) {
				((ViewGroup) view).getChildAt(i).setEnabled(arg1);
				setViewControlEnable(((ViewGroup) view).getChildAt(i), arg1);
			}
		}
	}
	
	protected boolean isValidIPAddress(String ip){
		String   regx="\\d+\\.\\d+\\.\\d+\\.\\d+";
		return ip.trim().matches(regx);
	}
	
	protected boolean isValidTunnelPort(String port){
		String   regx="\\d+";
		return port.trim().matches(regx);
	}
	
}
