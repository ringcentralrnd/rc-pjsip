/** 
 * Copyright (C) 2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.client.SIProxy;
import com.rcbase.android.sip.service.IServicePJSIP;
import com.rcbase.android.sip.service.RCCallInfo;
import com.rcbase.android.sip.service.ServiceState;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.utils.NetworkUtils;

/**
 * <code>ConfigVOIPdumpActivity</code> shows dump of VoIP service
 */
public class ConfigVOIPdumpActivity extends Activity {
    private static final String TAG = "[RC]ConfigVOIPdumpActivity";
    private TextView mLogTextView;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (BUILD.VOIP_ENABLED) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "onCreate...");
            }
            setContentView(R.layout.debug_log_window);
            buildLayout();
            mHandler = new Handler() {
                public void handleMessage(Message msg) {
                    if (null == msg || msg.obj == null || isFinishing() || mLogTextView == null) {
                        return;
                    }
                    mLogTextView.append((String) msg.obj);
                    mLogTextView.append("\n");
                }
            };
            refreshRequest();
        } else {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (BUILD.VOIP_ENABLED) {
            mLogTextView = null;
            mHandler = null;
        }
    }

    /**
     * Builds layout.
     */
    private void buildLayout() {
        if (BUILD.VOIP_ENABLED) {
            TextView header = (TextView) findViewById(R.id.debug_log_header);
            header.setText(R.string.config_screen_voip_service_dump_header);

            mLogTextView = (TextView) findViewById(R.id.debug_log_text_view);

            Button leftButton = (Button) findViewById(R.id.debug_log_left_button);
            leftButton.setText(R.string.config_screen_voip_clear_button_title);
            leftButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isFinishing()) {
                        return;
                    }
                    mLogTextView.setText("");
                }
            });

            Button rightButton = (Button) findViewById(R.id.debug_log_right_button);
            rightButton.setText(R.string.config_screen_voip_refresh_button_title);
            rightButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    refreshRequest();
                }
            });

        }
    }

    /**
     * Logs a line.
     * 
     * @param lineToLog
     *            a line to be added to the log
     */
    private void logLine(String lineToLog) {
        if (BUILD.VOIP_ENABLED) {
            if (isFinishing() || lineToLog == null || mHandler == null) {
                return;
            }

            try {
                Message m = mHandler.obtainMessage();
                m.obj = lineToLog;
                mHandler.sendMessage(m);
            } catch (java.lang.Throwable thIn) {

            }
        }
    }

    /**
     * Refresh request.
     */
    private void refreshRequest() {
        if (BUILD.VOIP_ENABLED) {
            if (isFinishing()) {
                return;
            }
            try {
                logLine("Refreshing...");
                new GetDumpAsyncTask().execute();
            } catch (java.lang.Throwable th) {
                Toast.makeText(this, "Refresh failed : " + th.toString(), Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    private class GetDumpAsyncTask extends AsyncTask<Object, Void, Void> {
        @Override
        protected Void doInBackground(Object... arg0) {
            if (BUILD.VOIP_ENABLED) {
                if (isFinishing()) {
                    return null;
                }

                try {
                    logLine("===============");
                    if (RingCentralApp.numberOfSIPServiceKillsAttempts() > 0) {
                        logLine(" SIP Kills (attempts) : " + RingCentralApp.numberOfSIPServiceKills() + "("
                                + RingCentralApp.numberOfSIPServiceKillsAttempts() + ')');
                        logLine("");
                    }
                    
                    logLine(NetworkUtils.getNetworkStatusAsString(ConfigVOIPdumpActivity.this));
                    logLine("RegisterExpiration:" + NetworkUtils.getForcedExpirationIfRecuired(ConfigVOIPdumpActivity.this));
                    logLine("ForcedSIPport:" + NetworkUtils.getDedicatedSIPTransportLocalPortIfrequired(ConfigVOIPdumpActivity.this));
                    logLine("===============");
                    SIProxy proxy = SIProxy.getInstance();
                    logLine("SIProxy : " + proxy.getServiceConnectionStateAsString() +  " Reconn: " + proxy.getTotalNumberOfReconnects());
                    IServicePJSIP service = proxy.getService();
                    if (service == null) {
                        return null;
                    }
                    logLine("===============");
                    ServiceState state = service.getServiceState();
                    if (state == null) {
                        logLine("Service State: NULL");
                    } else {
                        logLine(state.dump());
                    }

                    if (RCMProviderHelper.isAlwaysCPULockForSIPstack(ConfigVOIPdumpActivity.this)) {
                        logLine("  Always CPU Lock for SIP: TRUE");
                    }
                    
                    /**
                     * Dump current calls.
                     */
                    RCCallInfo[] calls = service.getCurrentCalls();
                    if (calls == null) {
                        logLine(" Current calls : NULL");
                    } else {
                        logLine("> Current calls " + calls.length);
                        StringBuffer sb = new StringBuffer();
                        int activeCalls = 0;
                        int endingCalls = 0;
                        int i = 1;
                        for (RCCallInfo call : calls) {
                            sb.append('\n');
                            sb.append("  Call " + i + "/" + calls.length);
                            sb.append('\n');
                            if (call != null) {
                                if (call.isAlive()) {
                                    activeCalls++;
                                    if (call.isEnding()) {
                                        endingCalls++;
                                    }
                                }
                                sb.append(call.dump());
                            } else {
                                sb.append("  NULL");
                            }
                            i++;
                            sb.append('\n');
                        }

                        logLine("   Active(alive): " + activeCalls + " Ending:" + endingCalls);
                        logLine(sb.toString());
                        logLine("< Current calls");
                    }

                    /**
                     * Dump latest finished calls.
                     */
                    RCCallInfo[] finishedCalls = service.getLatestFinishedCalls();
                    if (finishedCalls == null) {
                        logLine(" Latest finished calls : NULL");
                    } else {
                        logLine("> Latest finished calls " + finishedCalls.length);
                        int i = 1;
                        for (RCCallInfo call : finishedCalls) {
                            logLine("  Call " + i + "/" + finishedCalls.length);
                            if (call != null) {
                                logLine(call.dump());
                            } else {
                                logLine("  NULL");
                            }
                            i++;
                        }
                        logLine("< Latest finished calls");
                    }
                    
                    
                    logLine("PJSIP CALLS... ");
                    int pjsipCalls = service.call_get_pjsip_count();
                    logLine("PJSIP CALLS:  " + pjsipCalls);
                    
                    logLine("===============");
                    logLine("Echo: ");
                    int externalSpkr = RCMProviderHelper.getDeviceExternalSpeakerDelay(ConfigVOIPdumpActivity.this);
                    logLine(" Ext. Speaker: " + externalSpkr + " samples ~ " + externalSpkr / 8 + "ms"); // now hardcode because of we have just 8kHz TODO: remake next time
                    int internalSpkr = RCMProviderHelper.getDeviceInternalSpeakerDelay(ConfigVOIPdumpActivity.this);
                    logLine(" Int. Speaker: " + internalSpkr + " samples ~ " + internalSpkr/8 + "ms"); // now hardcode because of we have just 8kHz TODO: remake next time
                    
                    logLine("Dump finished");
                    logLine("===============");
                } catch (java.lang.Throwable th) {
                    try {
                        logLine("Error refreshing : " + th.toString());
                    } catch (java.lang.Throwable thIn) {

                    }
                }
            }
            return null;
        }
    }
}
