/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.Window;
import android.widget.TextView;

import com.rcbase.android.config.BUILD;
import com.ringcentral.android.R;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.AccountInfoTable;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.ui.ActivityUtils;

public class AppDescription extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.about_app_description);
        int aboutResource;
        if (RCMProviderHelper.getServiceVersion(this) >= AccountInfoTable.SERVICE_VERSION_5){
            aboutResource = R.string.descriptionTextAmarosaUser;
        } else {
            aboutResource = R.string.descriptionTextOldUser;
        }
        String text = String.format(getResources().getString(aboutResource), BUILD.VERSION_NAME);
        ((TextView) findViewById(R.id.app_description_content)).setText(Html.fromHtml(text));
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	FlurryTypes.onStartSession(this);
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	FlurryTypes.onEndSession(this);
    }
}
