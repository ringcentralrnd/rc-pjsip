/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.ui.ActivityUtils;

public class VoipSettings extends Activity {
    private static final String TAG = "[RC]VoipSettings";

    // Buttons
    private ToggleButton mb_outgoingCalls = null;
    private RadioButton  mb_WiFi = null;
    private RadioButton  mb_WiFi3G = null;
    private ToggleButton mb_incomingCalls = null;
    private ToggleButton mb_echoCanceler = null;
    
    // Flags
    private boolean checked_sip = false;
    private boolean checked_sip_incoming = false;
    private boolean checked_sip_WiFi = false;
    private boolean checked_sip_WiFi_3G = false;
    private boolean checked_echo_canceler = false;
    
    private boolean tos_911_accepted = true;
    
    //
    private boolean audioWizardWasRun = false;

    // Dialog's ids
    private static final int SETUP_TOS911_DIALOG = 1;       // Defines "ToS 911" dialog identifier.
    private static final int SETUP_AW_STOP_DIALOG = 2;      // that dialog is necessary for stopping the user to start Audio Wizard - in case if the wired handsfree is connected OR presence of active call
    private static final int SETUP_VOIP_WHATS_NEW_DIALOG = 3;// Voip "Whats new" dialog after the TOS 911 dialog
    
    // Messages for internal handler
    private static final int MSG_SHOW_BDD = 0;
    
    private Handler myHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(null == msg) {
                return;
            }
            
            if(LogSettings.MARKET) { 
                QaLog.i(TAG, " myHandler " + msg.what);
            }
            
            switch(msg.what) {
                case MSG_SHOW_BDD:
                    audioWizardWasRun =  RCMProviderHelper.getDeviceAudioWizard(VoipSettings.this);
                    showDialog(SETUP_VOIP_WHATS_NEW_DIALOG);
                    break;
                default:
                    break;
            }
        };
    };

    private void getSettings() {
        if(LogSettings.MARKET) {
            MktLog.d(TAG, " getSettings ");
        }
        
        checked_sip          = RCMProviderHelper.isVoipSwitchedOn_UserVoip(VoipSettings.this);
        
        if( BUILD.isTos911Enabled ){
        	tos_911_accepted = RCMProviderHelper.isTos911Accepted(VoipSettings.this);
        }
        
        checked_sip_incoming = RCMProviderHelper.isVoipSwitchedOn_UserIncomingCalls(VoipSettings.this);        
        
        checked_sip_WiFi     = RCMProviderHelper.isVoipSwitchedOn_UserWiFi(VoipSettings.this); 
        checked_sip_WiFi_3G  = RCMProviderHelper.isVoipSwitchedOn_User3g4g(VoipSettings.this);
        
        checked_echo_canceler = RCMProviderHelper.getDeviceEchoState(VoipSettings.this);  
        
        traceSettings();
    }

    private final OnClickListener buttonOutgoingCallsListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            //If user have pressed "Decline" on 911 ToS show ToS dialog again
            if (mb_outgoingCalls.isChecked() && BUILD.isTos911Enabled && !RCMProviderHelper.isTos911Accepted(VoipSettings.this)){
                showDialog(SETUP_TOS911_DIALOG);
                return;
            }
            
            turnVoipSettings( mb_outgoingCalls.isChecked() );
        }
    };

    private void turnVoipSettings(boolean on) {
    	
        mb_outgoingCalls.setChecked(on);
        if (BUILD.VOIP_MOBILE_3G_4G_ENABLED) {
            mb_WiFi.setEnabled(on);
            mb_WiFi3G.setEnabled(on);
        }
        if (BUILD.VOIP_INCOMING_ENABLED) {
            mb_incomingCalls.setEnabled(on);
        }
        mb_echoCanceler.setEnabled(on);
        
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Kepp order of method's calls!
        getSettings();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.settings_voip);
        
        buildLayout();
    }
    
    private void buildLayout() {
    	
        if(LogSettings.MARKET) { 
            MktLog.i(TAG, "buildLayout() " );
        }
        // OUTGOING CALLS SETTINGS
        View item = findViewById(R.id.voip_calls_on_off); 
        if (null != item) {
            TextView settingsVoIPToggleItemHeader =(TextView) item.findViewById(R.id.settings_voip_toggle_item_header);
            
            if (BUILD.VOIP_MOBILE_3G_4G_ENABLED) {
                settingsVoIPToggleItemHeader.setText(getString(R.string.settings_voip_calls_on_off_title));
                findViewById(R.id.settings_voip_toggle_list_divider_outer).setVisibility(View.VISIBLE);
                findViewById(R.id.settings_voip_wifi_3g4g_radiobuttons).setVisibility(View.VISIBLE);
                findViewById(R.id.settings_voip_toggle_list_divider_inner).setVisibility(View.GONE);
            } else {
                settingsVoIPToggleItemHeader.setText(getString(R.string.settings_voip_calls_on_off_title_when_3G_4G_disabled));
                findViewById(R.id.settings_voip_toggle_list_divider_inner).setVisibility(View.VISIBLE);
                findViewById(R.id.settings_voip_toggle_list_divider_outer).setVisibility(View.GONE);
                findViewById(R.id.settings_voip_wifi_3g4g_radiobuttons).setVisibility(View.GONE);
            }
            
            mb_outgoingCalls = (ToggleButton) item.findViewById(R.id.settings_voip_toggle_button);
            mb_outgoingCalls.setOnClickListener(buttonOutgoingCallsListener);
            mb_outgoingCalls.setChecked(tos_911_accepted && checked_sip);
        }
        
        
        // NETWORK CONFIGURATION RADIO BUTTONS
        mb_WiFi = (RadioButton)findViewById(R.id.settings_voip_options_dialog_item_WiFi_radiobtn);
        mb_WiFi.setChecked(checked_sip_WiFi);
        mb_WiFi.setEnabled(mb_outgoingCalls.isChecked());
        
        mb_WiFi3G = (RadioButton)findViewById(R.id.settings_voip_options_dialog_item_WiFi_3G_radiobtn);
        mb_WiFi3G.setChecked(checked_sip_WiFi_3G);
        mb_WiFi3G.setEnabled(mb_outgoingCalls.isChecked());
        
        // INCOMING CALLS SETTINGS
        item = findViewById(R.id.voip_incomming_calls_on_off); 
        if (null != item) {
            if (BUILD.VOIP_INCOMING_ENABLED) {
                ((TextView) item.findViewById(R.id.settings_toggle_item_header)).setText(getString(R.string.settings_voip_incoming_calls_on_off_title));
                
                mb_incomingCalls = (ToggleButton) item.findViewById(R.id.settings_toggle_button);
                mb_incomingCalls.setChecked(checked_sip_incoming);
                mb_incomingCalls.setEnabled(mb_outgoingCalls.isChecked());            
                
                TextView desription = (TextView)item.findViewById(R.id.settings_toggle_item_description); 
                desription.setText(R.string.settings_voip_incoming_calls_on_off_label);
                desription.setVisibility(View.VISIBLE);
            } else {
                item.setVisibility(View.GONE);
            }
        }
        
        
        // ECHO CANCELER SETTINGS
        item = findViewById(R.id.voip_echo_cancelation_on_off); 
        if (null != item) {
            TextView tv = (TextView) item.findViewById(R.id.settings_toggle_item_header);
            tv.setText(getString(R.string.settings_voip_echo_canceler_on_off_title));
            
            mb_echoCanceler = (ToggleButton) item.findViewById(R.id.settings_toggle_button);
            mb_echoCanceler.setChecked(checked_echo_canceler);
            mb_echoCanceler.setEnabled(mb_outgoingCalls.isChecked());
            
            TextView desription = (TextView)item.findViewById(R.id.settings_toggle_item_description); 
            desription.setText(R.string.settings_voip_echo_canceler_on_off_label);
            desription.setVisibility(View.VISIBLE);
        }
        
        item = findViewById(R.id.microphone_callibration_wizard);
        if(null != item) {
            ((TextView)item.findViewById(R.id.name)).setVisibility(View.VISIBLE);
            ((TextView)item.findViewById(R.id.name)).setText(R.string.settings_voip_mic_callibration_setup_title);
            item.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean startWizard = true;
                    final RingCentralApp rcApp = (RingCentralApp)VoipSettings.this.getApplication();
                    if( rcApp != null ) {
                        final int sipCallCount = rcApp.getSipActiveCallsCount();
                        // if have at least one call - we should not give possibility to initiate voip call
                        if( sipCallCount > 0){
                            startWizard = false;
                        }
                    }
                    
                    if(startWizard) {   
                        Intent i = new Intent(VoipSettings.this, AudioSetupWizard.class);
                        startActivity(i);
                    } else {
                        showDialog(SETUP_AW_STOP_DIALOG);
                    }
                }
            });
        }
    }
    
    private void saveSettings() {
        if(LogSettings.MARKET) { 
            MktLog.i(TAG, " saveSettings ");
        }
        
        if(mb_echoCanceler.isChecked() != checked_echo_canceler) {
            RCMProviderHelper.saveDeviceEchoState(getApplicationContext(), mb_echoCanceler.isChecked());
            
            Intent iEchoCancelationConfChanged = new Intent(RCMConstants.ACTION_VOIP_ECHO_CANCELATION_STATE_CHANGED);
            iEchoCancelationConfChanged.putExtra(RCMConstants.EXTRA_VOIP_ECHO_CANCELATION_STATE_CHANGED, mb_echoCanceler.isChecked() );
            getApplicationContext().sendBroadcast(iEchoCancelationConfChanged);
            FlurryTypes.onEvent(FlurryTypes.ECHO_CANCELLATION_TOGGLED, FlurryTypes.TOGGLE, mb_echoCanceler.isChecked()? FlurryTypes.ON : FlurryTypes.OFF);
        }
        
        long voipConfigurationStateChanged = 0;
        
        if( mb_outgoingCalls.isChecked() != checked_sip ) {
            RCMProviderHelper.setVoipSwitchedOn_UserVoip(getApplicationContext(), mb_outgoingCalls.isChecked());
            voipConfigurationStateChanged |= RCMConstants.SIP_SETTINGS_USER_VOIP_ENABLED;
            FlurryTypes.onEvent(FlurryTypes.VOIP_TOGGLED, FlurryTypes.TOGGLE, mb_outgoingCalls.isChecked()? FlurryTypes.ON : FlurryTypes.OFF);
        }
        else if( BUILD.isTos911Enabled && 
        		(tos_911_accepted != RCMProviderHelper.isTos911Accepted(VoipSettings.this)) ){
        	
        	/*
        	 *	If TOS 911 flag was changed then 
        	 *	send the same notification as on case SIP Locally state changed 
        	 */
            voipConfigurationStateChanged |= RCMConstants.SIP_SETTINGS_USER_VOIP_ENABLED;
        }
        
        if(mb_WiFi.isChecked() != checked_sip_WiFi) {
            RCMProviderHelper.setVoipSwitchedOn_UserWifi(getApplicationContext(), mb_WiFi.isChecked());
        }
        
        if (BUILD.VOIP_MOBILE_3G_4G_ENABLED) {
            if (mb_WiFi3G.isChecked() != checked_sip_WiFi_3G) {
                RCMProviderHelper.setVoipSwitchedOn_User3g4g(VoipSettings.this, mb_WiFi3G.isChecked());
            }
        }
        
        if (BUILD.VOIP_INCOMING_ENABLED) {
            if (mb_incomingCalls.isChecked() != checked_sip_incoming) {
                RCMProviderHelper.setVoipSwitchedOn_UserIncomingCalls(getApplicationContext(), mb_incomingCalls.isChecked());
                FlurryTypes.onEvent(FlurryTypes.INCOMING_VOIP_TOGGLED, FlurryTypes.TOGGLE, mb_incomingCalls.isChecked()? FlurryTypes.ON : FlurryTypes.OFF);
                // TODO: REPLACE THAT DIRTY HACK
//                if (HttpRegister.SIP_EMULATE_IPHONE) {
//                    RCMProviderHelper.saveHttpRegInstanceId(getApplicationContext(), Integer.toString(0));
//                }
            }
        }
        
        // SERVICE NOTIFICATION CONFIGURATION FLAGS
        if (mb_outgoingCalls.isChecked()) {
        	
            RCMProviderHelper.setVoipSwitchedOn_UserWifi(getApplicationContext(), true);

            if (BUILD.VOIP_MOBILE_3G_4G_ENABLED) {
                if (mb_WiFi3G.isChecked() != checked_sip_WiFi_3G) {
                    voipConfigurationStateChanged |= RCMConstants.SIP_SETTINGS_USER_3G_4G_ENABLED;
                }
            }
            
            if (BUILD.VOIP_INCOMING_ENABLED) {
                if (mb_incomingCalls.isChecked() != checked_sip_incoming) {
                    voipConfigurationStateChanged |= RCMConstants.SIP_SETTINGS_USER_INCOMING_CALLS_ENABLED;
                }
            }
        }
        
        if( voipConfigurationStateChanged > 0 ){
            RCMProviderHelper.voipSettingsChangedNotification(getApplicationContext(), voipConfigurationStateChanged);
        }
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        saveSettings();
        super.onConfigurationChanged(newConfig);
    }
    
    @Override
    protected void onPause() {
        saveSettings();
        super.onPause();
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	FlurryTypes.onStartSession(this);    	
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	FlurryTypes.onEndSession(this);
    }
    
    @Override
    protected Dialog onCreateDialog(int id) {        
        switch (id) {
            case SETUP_TOS911_DIALOG:
                if (BUILD.isTos911Enabled){
                    final Dialog tos911Dialog = ToS911Dialog.getDialog(VoipSettings.this);
                    tos911Dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            public void onDismiss(DialogInterface dialog) {
                                boolean isTOS911Accepted = RCMProviderHelper.isTos911Accepted(VoipSettings.this);
                                turnVoipSettings(isTOS911Accepted);
                                
                                if(isTOS911Accepted) {    
                                    Message msg = myHandler.obtainMessage();
                                    msg.what = MSG_SHOW_BDD;
                                    myHandler.sendMessage(msg);
                                }
                            }
                        });
                    return tos911Dialog;
                } else {
                    return null;
                }
            case SETUP_AW_STOP_DIALOG:
                return RcAlertDialog.getBuilder(this)
                .setTitle(R.string.battery_dialog_warning)
                .setIcon(R.drawable.symbol_exclamation)
                .setMessage(R.string.asw_dialog_active_call_notification)
                .setCancelable(false)
                .setPositiveButton(R.string.battery_dialog_ok, null).create();
                
            case SETUP_VOIP_WHATS_NEW_DIALOG:
                RCMProviderHelper.setAccountVoipWhatsNewDialogShown(VoipSettings.this, true);
                return VoipWhatsNewDialog.getMe(this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(LogSettings.MARKET) {
                            MktLog.w(TAG, "onClick VoipWhatsNewDialog, audioWizardWasRun: " + audioWizardWasRun);
                        }
                        
                        if(!audioWizardWasRun) {
                            Intent i = new Intent(VoipSettings.this, AudioSetupWizard.class); 
                            startActivity(i);
                        }
                    }
                });
            default:
                return null;
        }
    }
    
    @Override
    protected void onPrepareDialog (int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        switch (id) {
        case SETUP_AW_STOP_DIALOG:
            ((AlertDialog)dialog).setMessage(getString(R.string.asw_dialog_active_call_notification));
            break;
        default:
            break;
        }
    }

    /*
     * Put's to the log flags
     */
    private void traceSettings() {
        if(LogSettings.MARKET) {
            MktLog.i(TAG, "getSettings :  " +
                    " sip " + checked_sip + 
                    " incoming " + checked_sip_incoming + 
                    " WiFi " + checked_sip_WiFi + 
                    " WiFi3G " + checked_sip_WiFi_3G +
                    " 3G/4G enabled (cond.comp.) " + BUILD.VOIP_MOBILE_3G_4G_ENABLED + 
                    " Incoming enabled (cond.comp.) " + BUILD.VOIP_INCOMING_ENABLED + 
                    " Echo " + checked_echo_canceler
            );
        }
    }


}
