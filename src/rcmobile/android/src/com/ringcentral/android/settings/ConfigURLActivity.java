/** 
 * Copyright (C) 2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.rcbase.android.config.BUILD;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConfig;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.contacts.RcAlertDialog;

public class ConfigURLActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.config_url_activity);    
		
		findViewById(R.id.config_screen_url_save_button).setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				if (!storeConfiguration(ConfigURLActivity.this)) {
                    return;
                }
                RingCentralApp.restartApplication("", "", "", false, true, true, ConfigURLActivity.this);
                finish();	
			}
		});
		
		buildURLElement(R.id.config_screen_url_jedi, 
				R.string.config_screen_url_jedi, 
				RCMConfig.get_JEDI_URL(this), 
				R.array.config_screen_url_jedi_names, 
				R.array.config_screen_url_jedi_values);
		
		buildURLElement(R.id.config_screen_url_messaging, 
				R.string.config_screen_url_messaging, 
				RCMConfig.get_MSGSYNC_URL(this), 
				R.array.config_screen_url_messaging_names, 
				R.array.config_screen_url_messaging_values);
		
		buildURLElement(R.id.config_screen_url_signup, 
				R.string.config_screen_url_signup, 
				RCMConfig.get_SIGNUP_URL(this, false).toString(), 
				R.array.config_screen_url_signup_names, 
				R.array.config_screen_url_signup_values);
		
		buildCheckBoxExlement(R.id.config_screen_option_url_setup_override,  
				R.string.config_screen_option_setup_url_overriding_enabled_name, 
				RCMConfig.is_SETUP_URL_OVERRIDING_ENABLED(this), 
				R.id.config_screen_url_setup);
		
		buildURLElement(R.id.config_screen_url_setup, 
				R.string.config_screen_url_setup, 
				RCMConfig.get_SETUP_URL(this, false).toString(), 
				R.array.config_screen_url_setup_names, 
				R.array.config_screen_url_setup_values);
		
		buildURLElement(R.id.config_screen_url_setup_signup, 
				R.string.config_screen_url_setup_signup, 
				RCMConfig.get_SETUP_SIGNUP_SETTINGS_URL(this), 
				R.array.config_screen_url_setup_signup_names, 
				R.array.config_screen_url_setup_signup_values);	
		
		buildURLElement(R.id.config_screen_url_mobile_settings_base, 
				R.string.config_screen_url_mobile_settings_base, 
				RCMConfig.get_WEB_SETTINGS_BASE_URL(this), 
				R.array.config_screen_url_mobile_settings_base_names, 
				R.array.config_screen_url_mobile_settings_base_values);	
		
		buildURLElement(R.id.config_screen_url_httpreg, 
				R.string.config_screen_url_httpreg, 
				RCMConfig.get_HTTPREG_URL(this, false ), 
				R.array.config_screen_url_httpreg_names, 
				R.array.config_screen_url_httpreg_values);		
		
		findViewById(R.id.config_screen_url_launch_signup_settup_settings_button).setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
			    try {
			        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(RCMConfig.get_SETUP_SIGNUP_SETTINGS_URL(ConfigURLActivity.this)));
			        ConfigURLActivity.this.startActivity(intent);
			    } catch (java.lang.Throwable th) {
			        Toast.makeText(ConfigURLActivity.this, "Error:" + th.getMessage(), Toast.LENGTH_SHORT);
			    }
			}
		});
		
		findViewById(R.id.config_screen_url_set_default_button).setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				RCMConfig.setDefaultConfiguration(ConfigURLActivity.this);
				RingCentralApp.restartApplication("", "", "", false, true, true, ConfigURLActivity.this);
                finish();			
			}
		});
			
	}
	
	private void buildURLElement(final int resId, final int resName, final String url, final int namesId, final int valuesId){
		final View viewElement = findViewById(resId); 
		viewElement.findViewById(R.id.edit).setVisibility(View.VISIBLE);		
		((TextView)viewElement.findViewById(R.id.label)).setText(resName);
		((TextView)viewElement.findViewById(R.id.edit)).setText(url);
		((ImageView)viewElement.findViewById(R.id.imgright)).setImageResource(R.drawable.ic_list_link);
		((ImageView)viewElement.findViewById(R.id.imgright)).setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				Resources res = getResources();
                SelectionDialog dialog = new SelectionDialog(ConfigURLActivity.this, 
                		(TextView)viewElement.findViewById(R.id.edit), 
                        res.getString(resName), 
                        res.getStringArray(namesId),
                        res.getStringArray(valuesId));
                dialog.show();
			}
		});
	}
	
	private void buildCheckBoxExlement(final int resId, final int nameId, final boolean checked, final int guardedElementId){
		final View viewElement = findViewById(resId);
		((TextView)viewElement.findViewById(R.id.label)).setText(nameId);
					
		((CheckBox)viewElement.findViewById(R.id.checkbox)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				final View guardedView = findViewById(guardedElementId);
				guardedView.setVisibility(isChecked?View.GONE:View.VISIBLE);
			}
		});
		
		((CheckBox)viewElement.findViewById(R.id.checkbox)).setChecked(checked);		
	}
	
    private boolean storeConfiguration(Context context) {    	
        final String jediUrl = getURLText(R.id.config_screen_url_jedi);
        if (jediUrl.length() == 0) {
            Toast.makeText(this, "Invalid JEDI URL", 3000).show();
            return false;
        }
        
        final String msgUrl = getURLText(R.id.config_screen_url_messaging);
        if (msgUrl.length() == 0) {
            Toast.makeText(this, "Invalid Messaging Sync URL", 3000).show();
            return false;
        }
        
        final String signupUrl = getURLText(R.id.config_screen_url_signup);
        if (signupUrl.length() == 0) {
            Toast.makeText(this, "Invalid SignUp URL", 3000).show();
            return false;
        }
        
        final boolean setupUrlOverridingEnabled = getCheckboxValue(R.id.config_screen_option_url_setup_override);
        
        final String setupUrl = getURLText(R.id.config_screen_url_setup);
        if (setupUrl.length() == 0) {
            Toast.makeText(this, "Invalid Setup URL", 3000).show();
            return false;
        }
        
        final String setupSignupUrl = getURLText(R.id.config_screen_url_setup_signup);
        if (setupSignupUrl.length() == 0) {
            Toast.makeText(this, "Invalid Setup Signup Settings URL", 3000).show();
            return false;
        }
        
        final String mobileSettingsBaseUrl = getURLText(R.id.config_screen_url_mobile_settings_base);
        if (mobileSettingsBaseUrl.length() == 0) {
            Toast.makeText(this, "Invalid Mobile Settings URL", 3000).show();
            return false;
        }  

        final String httpregUrl = getURLText(R.id.config_screen_url_httpreg);;
        if (httpregUrl.length() == 0) {
            Toast.makeText(this, "Invalid HTTP Registation URL", 3000).show();
            return false;
        }        
        
        RCMConfig.set_JEDI_URL(context, jediUrl);
        RCMConfig.set_MSGSYNC_URL(context, msgUrl);
        RCMConfig.set_SIGNUP_URL(context, signupUrl);
        RCMConfig.set_SETUP_URL_OVERRIDING_ENABLED(context, setupUrlOverridingEnabled);
        RCMConfig.set_SETUP_URL(context, setupUrl);
        RCMConfig.set_SETUP_SIGNUP_SETTINGS_URL(context, setupSignupUrl);
        RCMConfig.set_WEB_SETTINGS_BASE_URL(context, mobileSettingsBaseUrl);
        RCMConfig.set_HTTPREG_URL(context, httpregUrl);
        
        return true;
    }
    
    private String getURLText(int resId){
    	return ((EditText)findViewById(resId).findViewById(R.id.edit)).getText().toString().trim();
    }
    
    private boolean getCheckboxValue(int resId){
    	return ((CheckBox)findViewById(resId).findViewById(R.id.checkbox)).isChecked();
    }
	
    private class SelectionDialog implements DialogInterface.OnClickListener, DialogInterface.OnDismissListener {
        private Context mContext;
        private AlertDialog mDialog;
        private SimpleAdapter mAdapter;
        private TextView mTextView;
        private List<HashMap<String, String>> mFillMaps;

        public SelectionDialog(Context context, TextView textView, String title, String[] names, String[] values) {
            mContext = context;
            mTextView = textView;
            
            final String[] from = new String[] { URL.NAME, URL.VALUE };
            final int[] to = new int[] { android.R.id.text1, android.R.id.text2 };

            mFillMaps = new ArrayList<HashMap<String, String>>();

            for (int i = 0; i < names.length; i++) {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(URL.NAME, names[i]);
                map.put(URL.VALUE, values[i]);
                mFillMaps.add(map);
            }
            
            mAdapter = new SimpleAdapter(mContext, mFillMaps, R.layout.simple_list_item_2, from, to);            
            mDialog = RcAlertDialog.getBuilder(mContext).setAdapter(mAdapter, this).setTitle(title).create();
        }

        public void show() {
            if (mFillMaps.size() == 0) {
                return;
            }
            mDialog.show();
        }

        public void onClick(DialogInterface dialog, int which) {
            mTextView.setText(mFillMaps.get(which).get(URL.VALUE));
        }

        public void onDismiss(DialogInterface dialog) {
            mContext = null;
            mDialog = null;
            mAdapter = null;
            mTextView = null;
            mFillMaps = null;
        }

        private class URL {
            public static final String NAME = "NAME";
            public static final String VALUE = "VALUE";
        }
    }
    public static boolean isEnabled(String number){		
		return ((number != null) && (number.equals(BUILD.CONFIG_URI.LAUNCH_KEY)));	
	}
}
