/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.client.WizardClient;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.LoginScreen;
import com.ringcentral.android.R;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.utils.DeviceUtils;

import android.app.Activity;
import android.app.Dialog;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Audio wizard setup activity
 * 
 * @author ivan.alyakskin
 */
public class AudioSetupWizard extends Activity implements OnClickListener {
	private static final String TAG = "[RC]AudioSetupWizard";	
	
    public static final String LAUNCH_KEY_LOGIN_SCREEN = "LAUNCH_KEY_LOGIN_SCREEN";    

    public static final int INITIAL        = 0;
    public static final int CONNECTED      = 1;
    public static final int STARTED        = 2;
    public static final int CANCELLED      = 3;
    public static final int FAILED         = 4;
    public static final int COMPLETED      = 5;
     
    private static final int DLG_HEADSET_CONNECTED = 1;    
    
    private Button btnPositive = null;
    private Button btnNegative = null;
    private TextView txtDescription = null;
    private ImageView imgSpeaker = null;
    private ProgressBar progressBar = null;
    private int mDisplayOrientation;
    
    private WizardClient mWizardClient = null;
    private int mCurrentUIState = INITIAL;   
        
    private final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {        	
            if(LogSettings.MARKET) { 
                MktLog.d(TAG, "mHandler.handleMessage(), updating UI state to : " + getStateTag(msg.what));
            }           
            switch (msg.what) {
	            case CONNECTED:	            	
	            case STARTED:
	            case COMPLETED:
	            case FAILED:
	            	if(mCurrentUIState != FAILED){
	            		setNewState(msg.what);
	            	}
	            	break;   	            	
            }
        }
    };
    
    
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DLG_HEADSET_CONNECTED:
            return RcAlertDialog.getBuilder(this)
            .setTitle(R.string.battery_dialog_warning)
            .setIcon(R.drawable.symbol_exclamation)
            .setMessage(R.string.asw_dialog_connected_headset_notification)
            .setCancelable(false)
            .setPositiveButton(R.string.battery_dialog_ok, null).create();
        }
        return null;
    }
         
    private void setNewState(final int newState) {
        if(LogSettings.MARKET) {
            MktLog.d(TAG, "setNewState(), currentUIState : " + getStateTag(mCurrentUIState)  + ", newState : " + getStateTag(newState));
        }
        
        mCurrentUIState = newState;
        
        switch(newState) {
        case INITIAL:
            btnNegative.setVisibility(View.VISIBLE);
            btnNegative.setText(R.string.asw_btn_calibrate_later);
            btnNegative.setEnabled(true);
            
            btnPositive.setVisibility(View.VISIBLE);
            btnPositive.setText(R.string.asw_btn_calibrate_now);
            btnPositive.setEnabled(false);
            
            imgSpeaker.setVisibility(View.GONE);

            txtDescription.setVisibility(View.VISIBLE);
            txtDescription.setText(R.string.asw_calibration_description);
            txtDescription.setTextColor(getResources().getColor(R.color.aws_center_text_color));
            
            progressBar.setVisibility(View.VISIBLE);
            break;            
        case CONNECTED:
            btnNegative.setVisibility(View.VISIBLE);
            btnNegative.setText(R.string.asw_btn_calibrate_later);
            btnNegative.setEnabled(true);
            
            btnPositive.setVisibility(View.VISIBLE);
            btnPositive.setText(R.string.asw_btn_calibrate_now);
            btnPositive.setEnabled(true);
            
            imgSpeaker.setVisibility(View.GONE);

            txtDescription.setVisibility(View.VISIBLE);
            txtDescription.setText(R.string.asw_calibration_description);
            txtDescription.setTextColor(getResources().getColor(R.color.aws_center_text_color));
            
            progressBar.setVisibility(View.GONE);
            break;        
        case STARTED:
            btnNegative.setVisibility(View.VISIBLE);
            btnNegative.setText(R.string.asw_btn_calibrate_cancel);
            btnNegative.setEnabled(true);
            
            btnPositive.setVisibility(View.GONE);
            
            imgSpeaker.setVisibility(View.VISIBLE);

            txtDescription.setVisibility(View.VISIBLE);
            txtDescription.setText(R.string.asw_calibration_in_progress);
            txtDescription.setTextColor(getResources().getColor(R.color.aws_center_text_color));
            
            progressBar.setVisibility(View.VISIBLE);
            break;            
        case COMPLETED:
            btnNegative.setVisibility(View.GONE);
            
            btnPositive.setVisibility(View.VISIBLE);
            btnPositive.setText(R.string.asw_btn_calibrate_done);
            btnPositive.setEnabled(true);
            
            imgSpeaker.setVisibility(View.GONE);

            txtDescription.setVisibility(View.VISIBLE);
            txtDescription.setText(R.string.asw_calibration_completed_ok);
            txtDescription.setTextColor(getResources().getColor(R.color.aws_center_text_color));
            
            progressBar.setVisibility(View.GONE);                                    
            break;                        
        case FAILED:
            btnNegative.setVisibility(View.VISIBLE);
            btnNegative.setText(R.string.asw_btn_calibrate_cancel);
            btnNegative.setEnabled(true);
            
            btnPositive.setVisibility(View.VISIBLE);
            btnPositive.setText(R.string.asw_btn_calibrate_again);
            btnPositive.setEnabled(true);
            
            imgSpeaker.setVisibility(View.GONE);

            txtDescription.setVisibility(View.VISIBLE);
            txtDescription.setText(R.string.asw_calibration_completed_bad);
            txtDescription.setTextColor(getResources().getColor(R.color.aws_center_error_text_color));

            progressBar.setVisibility(View.GONE);
            break;            
        case CANCELLED:    
            if(getIntent().getBooleanExtra(LAUNCH_KEY_LOGIN_SCREEN, false)) {
                Intent i = new Intent(this, LoginScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }            
            finish();            
            break;
        }
    }    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if(LogSettings.ENGINEERING) {
    		EngLog.v(TAG, "onCreate()");
    	}

        mCurrentUIState = INITIAL;

        // setting the flag - wizard was shown, no needs, to show again
        RCMProviderHelper.setDeviceAudioWizard(this, true);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        buildLayout(DeviceUtils.getDisplayOrientation(this));
        
        mWizardClient = new WizardClient(mHandler);        
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	
    	if(LogSettings.ENGINEERING) {
    		EngLog.v(TAG, "onDestroy()");
    	}
    	
    	mWizardClient.releaseTestsCommand();    	
    	mWizardClient = null;
    	
    }
    
	private void buildLayout(int orientation) {
		mDisplayOrientation = orientation;

		int content_view_id = ( (orientation == Configuration.ORIENTATION_LANDSCAPE) ? 
				R.layout.wizard_form_landscape : 
				R.layout.wizard_form);

		setContentView(content_view_id);

		// Description block
		View v = findViewById(R.id.wizard_content);
		txtDescription = (TextView) v.findViewById(R.id.wizard_text_desription);
		progressBar = (ProgressBar) v.findViewById(R.id.wizard_progress);
		imgSpeaker = (ImageView) v.findViewById(R.id.wizard_body_img);

		// Buttons
		v = findViewById(R.id.wizard_buttons);
		btnNegative = (Button) v.findViewById(R.id.btn_wizard_negative);
		btnNegative.setOnClickListener(this);
		btnPositive = (Button) v.findViewById(R.id.btn_wizard_positive);
		btnPositive.setOnClickListener(this);
		
		setNewState(mCurrentUIState);

	}
    
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        
        if(LogSettings.ENGINEERING) {
    		EngLog.v(TAG, "onConfigurationChanged()");
    	}
        
        final int orientation = (newConfig.orientation != Configuration.ORIENTATION_UNDEFINED) 
	        ? newConfig.orientation
	        : DeviceUtils.calcDisplayOrientation(this);

        if (orientation != mDisplayOrientation) {
            buildLayout(orientation);
        }

    }
    
    
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_wizard_positive:
			if (DeviceUtils.isHeadsetConnected()) {
				showDialog(DLG_HEADSET_CONNECTED);
				return;
			}

			switch (mCurrentUIState) {
			case CONNECTED:
			case FAILED:
				mWizardClient.runTestsCommand();
				setNewState(STARTED);
				break;
			case COMPLETED:
				setNewState(CANCELLED);
				break;
			}
			break;

		case R.id.btn_wizard_negative:
			setNewState(CANCELLED);
			break;
		}

	}
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            setNewState(CANCELLED);
        }        
        return super.onKeyDown(keyCode, event);
    }
    
    private String getStateTag(int state){
    	switch (state) {
		case INITIAL:			
			return "INITIAL";
		case CONNECTED:			
			return "CONNECTED";
		case STARTED:			
			return "STARTED";
		case CANCELLED:			
			return "CANCELLED";
		case FAILED:			
			return "FAILED";
		case COMPLETED:			
			return "COMPLETED";
		}
    	return "UNDEFINED";
    }
}