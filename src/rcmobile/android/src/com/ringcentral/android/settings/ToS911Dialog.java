/**
 * Copyright (C) 2010-2012, RingCentral, Inc.
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.provider.RCMProviderHelper;

public class ToS911Dialog extends AlertDialog {
    private static final String TAG = "[RC]ToS911Dialog";
    
    public ToS911Dialog(Context context) {
		super(context);
		init(context);
	}
    
    public ToS911Dialog(Context context, int theme) {
		super(context, theme);
		init(context);
	}

	public ToS911Dialog(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
		init(context);
	}
	
	private void init(final Context context) {
		if (LogSettings.ENGINEERING) {
			EngLog.v(TAG, "init() called...");
		}
		
		setTitle(R.string.tos911_title);		
		setMessage(context.getString(R.string.tos911_text));
		setIcon(R.drawable.symbol_exclamation);
        setCancelable(false);
        setButton(BUTTON_POSITIVE, context.getString(R.string.tos911_accept_button_title), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            	if(LogSettings.MARKET) {
            		EngLog.v(TAG, "911 disclaimer dialog, user : Accept");
            	}
				RCMProviderHelper.set911TosAccepted(RingCentralApp.getContextRC(), true);
            }
        });
        setButton(BUTTON_NEGATIVE, context.getString(R.string.tos911_decline_button_title), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            	if(LogSettings.MARKET) {
            		EngLog.v(TAG, "911 disclaimer dialog, user : Decline");
            	}
				RCMProviderHelper.set911TosAccepted(RingCentralApp.getContextRC(), false);
            }
        });
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final Button btnNegative = (Button) findViewById(android.R.id.button2); // Negative button
		btnNegative.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				if(LogSettings.MARKET) {
            		EngLog.v(TAG, "911 disclaimer dialog, user : Decline");
            	}
				RCMProviderHelper.set911TosAccepted(RingCentralApp.getContextRC(), false);								
				RcAlertDialog.getBuilder(getContext())
					.setTitle(R.string.tos911_dlg_voip_calling_title)
					.setMessage(R.string.tos911_dlg_voip_calling_message)
					.setIcon(R.drawable.symbol_information)
					.setCancelable(false)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if(LogSettings.MARKET) {
			            		EngLog.v(TAG, "911 disclaimer VoIP calling dialog, user : Decline");
			            	}
							ToS911Dialog.this.dismiss();							
						}												
					})
					.create()
					.show();
			}
		});		
	}

	public static final Dialog getDialog(Context context){
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "Launching dialog...");
		}
		return new ToS911Dialog(context);
	}
    
}
 