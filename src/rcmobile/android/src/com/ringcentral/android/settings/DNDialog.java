/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.AccountInfoTable;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.DndManager;
import com.ringcentral.android.utils.NetworkUtils;
import com.ringcentral.android.utils.UserInfo;

public class DNDialog{	
	private static final String TAG = "[RC]DNDialog";	
	
	/**
	 * Main launcher
	 * @param context
	 */
	public static void showDNDDialog(final Context context){
		if(!UserInfo.dndAllowed()){
			if(LogSettings.ENGINEERING){
				MktLog.d(TAG, "DND not enabled for this account/extension, skipping dialog");
			}
			return;
		}		
		
		if (NetworkUtils.isRCAvaliable(context)) {
			final int serviceVersion = RCMProviderHelper.getServiceVersion(context);
			 if(LogSettings.ENGINEERING){
				 EngLog.d(TAG, "Service version : "+serviceVersion + ", Network is OK, launching dialog");
			 }
			if(AccountInfoTable.SERVICE_VERSION_5 <= serviceVersion){
				showDNDDialogV5(context);				
			}else{
				showDNDDialogV4(context);
			}
			
		} else {
			showDNDNetworkErrorDialog(context);
		}		
	}
	
	/**
	 * DND Network Error
	 * @param context
	 */
	public static void showDNDNetworkErrorDialog(Context context){
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "Launching dialog : RC not available(Network), showing error");						
		}		
		RcAlertDialog.getBuilder(context)
			.setTitle(R.string.error_code_alert_title)
			.setMessage(R.string.offline_mode_alerttext_dnd)
			.setIcon(R.drawable.symbol_exclamation)
			.setPositiveButton(R.string.dialog_btn_ok, 
				new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {}
				}
			)
			.create()
			.show();
	}
	
	/**
	 * DND Dialog server version 5
	 * @param context
	 */
	private static void showDNDDialogV5(final Context context) {
		final DNDialog5 dialog = new DNDialog5(context);
		dialog.show();
	}
	
	/**
	 * DND Dialog server version 4, old-school and cool
	 * @param context
	 */
	private static void showDNDDialogV4(final Context context) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dnd_dialog);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.findViewById(R.id.cancelBtn).setOnClickListener(
				new OnClickListener() {
					public void onClick(View v) {
						dialog.dismiss();
					}
				});

		final boolean dndStatus = DndManager.getInstance().getDndStatus(context);

		Button dndModeBtn = (Button) dialog.findViewById(R.id.dndModeBtn);
		dndModeBtn.setText(dndStatus ?  R.string.dnd_off : R.string.dnd_on);		
		dndModeBtn.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				FlurryTypes.onEvent(FlurryTypes.EVENT_DND_CHANGED,
						dndStatus ? FlurryTypes.DND_OPTIONS_OFF : FlurryTypes.DND_OPTIONS_ON);
				DndManager.getInstance().setDndStatus(context, !dndStatus);
				dialog.dismiss();
			}
		});

		dialog.show();
	}
	
	/**
	 * Class for DND Dialog version
	 *
	 */
	private final static class DNDialog5 implements DialogInterface.OnClickListener{
		
		private static final int DND_DEPT_USER = 0;
		private static final int DND_NON_DEPT_USER = DND_DEPT_USER + 1;
		private static final int DND_ONLY_DEPT_CALLS = DND_NON_DEPT_USER + 1;
		
		//mapping user type -> dialog options
		private static final int[][] DND_DIALOG_TYPES = 
												  {//DND_DEPT_USER
												   {DndManager.DND_STATUS_TAKE_ALL_CALLS,
												 	DndManager.DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS,
												 	DndManager.DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS},
												   //DND_NON_DEPT_USER
												   {DndManager.DND_STATUS_TAKE_ALL_CALLS,
													DndManager.DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS},
												   //DND_ONLY_DEPT_CALLS	
												   {DndManager.DND_STATUS_TAKE_ALL_CALLS,
													DndManager.DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS,
													DndManager.DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS,
													DndManager.DND_STATUS_TAKE_DEPARTMENT_CALLS_ONLY} 
												   };				
		
		private Context mContext;
		private AlertDialog mDialog;
		private DNDAdapter mDNDAdapter;
		
		private int dndStatus;
		private int dndDialogType;
		
	
		private DNDialog5(Context context){
			this.mContext = context;
			dndStatus = DndManager.getInstance().getExtendedDndStatus(context);
			dndDialogType = getDialogType(context);
			if(LogSettings.ENGINEERING){
				EngLog.d(TAG, "DNDialogv5 dndStatus : " + dndStatus + " dndDialogType : " + dndDialogType);			
			}
			mDNDAdapter = new DNDAdapter(context);
			mDialog = RcAlertDialog.getBuilder(context)
						.setAdapter(mDNDAdapter, this)					
						.setTitle(R.string.dnd_dialog_title)
						.create();		
			mDialog.getListView().setSelector(R.drawable.rc_settings_list_selector_bg);
			mDialog.setCanceledOnTouchOutside(true);
			
		}
	
		private void show(){
			mDialog.show();
		}	

		@Override
		public void onClick(DialogInterface dialog, int which) {
			int newDndValue = DND_DIALOG_TYPES[dndDialogType][which];
			if(LogSettings.ENGINEERING){
				EngLog.d(TAG, "onClick : oldValue : " + dndStatus + " new value : " + newDndValue + " persisting...");
			}
			if(newDndValue != dndStatus){
				DndManager.getInstance().setExtendedDndStatus(mContext, newDndValue);
				traceFlurry(newDndValue);
			}					
			mDialog.dismiss();
					
			//force clearing up
			mContext = null;
			mDialog = null;
			mDNDAdapter = null;
		}	
		
		/**
		 * Flurry convenience method
		 * @param newDndValue
		 */
		private final void traceFlurry(int newDndValue){
			String flurryEvent = FlurryTypes.DND_PARAM;
			switch (newDndValue) {
			case DndManager.DND_STATUS_TAKE_ALL_CALLS:
				flurryEvent = FlurryTypes.DND_TAKE_ALL_CALLS;
				break;
			case DndManager.DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS:
				flurryEvent = FlurryTypes.DND_TAKE_NONE_CALLS;
				break;
			case DndManager.DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS:
				flurryEvent = FlurryTypes.DND_TAKE_NO_DEPT_CALLS;
				break;				
			}
			FlurryTypes.onEvent(
					(dndDialogType==DND_NON_DEPT_USER)?FlurryTypes.EVENT_DND_USER : FlurryTypes.EVENT_DND_DEPT,									
					FlurryTypes.DND_PARAM, 
					flurryEvent); 								
		}
		
		/**
		 * Prepeares array to be shown in ArrayAdapter
		 * @param context
		 * @param dndOption
		 * @return
		 */
		private String[] getDNDStatusList(Context context, int dndOption){
			String[] result = new String[DND_DIALOG_TYPES[dndOption].length];
			for(int i=0;i<DND_DIALOG_TYPES[dndOption].length;i++){
				result[i] = getDNDStatusAsString(context, DND_DIALOG_TYPES[dndOption][i]);
			}
			return result;		
		}
		
		/**
		 * DND Status to String resource id mapping
		 * @param context
		 * @param dndStatus
		 * @return
		 */
		private String getDNDStatusAsString(Context context, int dndStatus) {
			switch (dndStatus) {
				case DndManager.DND_STATUS_TAKE_ALL_CALLS:
					return context.getString(R.string.dnd_take_all_calls);
				case DndManager.DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS:
					return context.getString(R.string.dnd_not_accept_department_calls);
				case DndManager.DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS:
					return context.getString(R.string.dnd_not_accept_any_calls);
				case DndManager.DND_STATUS_TAKE_DEPARTMENT_CALLS_ONLY:
					return context.getString(R.string.dnd_accept_only_department_calls);
			}
			return "";
		}
		
		/**
		 * By requirements, DND_STATUS_TAKE_DEPARTMENT_CALLS_ONLY override all existing,
		 * prerequisite : 
		 * Client is on Server v5 but with v4 UI. 
		 * DND is on, 
		 * User is department user, 
		 * Status available 
		 */
		private int getDialogType(Context context){
			if(DndManager.getInstance().getExtendedDndStatus(context)==DndManager.DND_STATUS_TAKE_DEPARTMENT_CALLS_ONLY){
				return DND_ONLY_DEPT_CALLS;
			}else{
				if(RCMProviderHelper.isAgent(context)){
					return DND_DEPT_USER;
				}else{
					return DND_NON_DEPT_USER;
				}
			}
		}
		
	
		/**
		 * Adapter for DND Dialog v5
		 *
		 */
		private final class DNDAdapter extends ArrayAdapter<String> {

			public DNDAdapter(Context context) {
				super(context, R.layout.dnd_dialog_list_item, R.id.text, getDNDStatusList(context, dndDialogType));
			}	
			
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {			
				final View view =  super.getView(position, convertView, parent);
				((RadioButton)view.findViewById(R.id.selector)).setChecked(DND_DIALOG_TYPES[dndDialogType][position]==dndStatus);			
				return view;
			}				
		}
	}
	
}

