/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.LoggingManager;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.client.SIProxy;
import com.rcbase.android.sip.client.TimedSIProxyTask;
import com.rcbase.android.sip.service.IServicePJSIP;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.provider.RCMProviderHelper;

public class ConfigVOIPActivity extends Activity {
	private static final String TAG = "[RC] ConfigVOIPActivity";
	private static final long SERVICE_CRASH_EMULATION_DELAY_MS = 2000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (BUILD.VOIP_ENABLED) {
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.config_voip_activity);

			((Button) findViewById(R.id.config_screen_url_save_button))
					.setEnabled(false);

			/**
			 * SIP Service dump
			 */
			View dumpServiceViewItem = findViewById(R.id.sip_service_dump);
			dumpServiceViewItem.setEnabled(true);
			((TextView) dumpServiceViewItem.findViewById(R.id.name))
					.setText(R.string.config_screen_voip_service_dump_header);
			dumpServiceViewItem.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					startActivity(new Intent(ConfigVOIPActivity.this,
							ConfigVOIPdumpActivity.class));
				}
			});

			/**
			 * 3G/4G Test mode
			 */
			((TextView) findViewById(R.id.config_screen_voip_3g).findViewById(
					R.id.name)).setText(R.string.config_screen_voip_3g);
			((ToggleButton) findViewById(R.id.config_screen_voip_3g)
					.findViewById(R.id.toggle)).setChecked(RCMProviderHelper
					.isVoipSwitchedOn_User3g4g(ConfigVOIPActivity.this));

			if (RCMProviderHelper
					.isVoipSwitchedOn_UserVoip(ConfigVOIPActivity.this)) {
				findViewById(R.id.config_screen_voip_3g).setEnabled(true);
			} else {
				findViewById(R.id.config_screen_voip_3g).setEnabled(false);
			}

			if (com.rcbase.android.config.BUILD.VOIP_MOBILE_3G_4G_ENABLED) {
				((TextView) findViewById(R.id.config_screen_voip_3g)
						.findViewById(R.id.label))
						.setText(R.string.config_screen_voip_3g_enabled);
			} else {
				((TextView) findViewById(R.id.config_screen_voip_3g)
						.findViewById(R.id.label))
						.setText(R.string.config_screen_voip_3g_disabled);
			}
			findViewById(R.id.config_screen_voip_3g).setOnClickListener(
					new OnClickListener() {
						@Override
						public void onClick(View v) {
							boolean isChecked = ((ToggleButton) findViewById(
									R.id.config_screen_voip_3g).findViewById(
									R.id.toggle)).isChecked();
							if (LogSettings.MARKET) {
								MktLog.w("RCConfig", "3G/4G Test mode: "
										+ isChecked);
							}
							RCMProviderHelper.setVoipSwitchedOn_User3g4g(
									ConfigVOIPActivity.this, isChecked);
							RCMProviderHelper
									.voipSettingsChangedNotification(
											getApplicationContext(),
											RCMConstants.SIP_SETTINGS_USER_3G_4G_ENABLED);
						}
					});

			((TextView) findViewById(R.id.config_screen_voip_wifi)
					.findViewById(R.id.name))
					.setText(R.string.config_screen_voip_wifi);
			((ToggleButton) findViewById(R.id.config_screen_voip_wifi)
					.findViewById(R.id.toggle)).setChecked(RCMProviderHelper
					.isVoipSwitchedOn_UserWiFi(ConfigVOIPActivity.this));
			findViewById(R.id.config_screen_voip_wifi).setEnabled(false);
			findViewById(R.id.config_screen_voip_wifi).findViewById(R.id.label)
					.setVisibility(View.GONE);

			if (com.rcbase.android.config.BUILD.VOIP_MOBILE_3G_4G_ENABLED) {
				View edgeView = findViewById(R.id.config_screen_voip_edge);
				edgeView.setVisibility(View.VISIBLE);
				((TextView) edgeView.findViewById(R.id.name))
						.setText(R.string.config_screen_voip_include_edge_name);
				edgeView.findViewById(R.id.label).setVisibility(View.VISIBLE);
				((TextView) edgeView.findViewById(R.id.label))
						.setText(R.string.config_screen_voip_include_edge_label);
				((ToggleButton) edgeView.findViewById(R.id.toggle))
						.setChecked(RCMProviderHelper
								.isNeedToIncludeEDGEintoMobVoIP(ConfigVOIPActivity.this));
				findViewById(R.id.config_screen_voip_edge).setOnClickListener(
						new OnClickListener() {
							@Override
							public void onClick(View v) {
								boolean isChecked = ((ToggleButton) findViewById(
										R.id.config_screen_voip_edge)
										.findViewById(R.id.toggle)).isChecked();
								if (LogSettings.MARKET) {
									MktLog.w("RCConfig", "EDGE: " + isChecked);
								}
								RCMProviderHelper.includeEDGEintoMobVoIP(
										ConfigVOIPActivity.this, isChecked);
							}
						});
			} else {
				findViewById(R.id.config_screen_voip_edge).setVisibility(
						View.GONE);
			}

			View edgeView = findViewById(R.id.config_screen_voip_always_cpu_lock);
			edgeView.setVisibility(View.VISIBLE);
			((TextView) edgeView.findViewById(R.id.name))
					.setText(R.string.config_screen_voip_always_use_cpu_lock_title);
			edgeView.findViewById(R.id.label).setVisibility(View.VISIBLE);
			((TextView) edgeView.findViewById(R.id.label))
					.setText(R.string.config_screen_voip_always_use_cpu_lock_label);
			((ToggleButton) edgeView.findViewById(R.id.toggle))
					.setChecked(RCMProviderHelper
							.isAlwaysCPULockForSIPstack(ConfigVOIPActivity.this));
			findViewById(R.id.config_screen_voip_always_cpu_lock)
					.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							boolean isChecked = ((ToggleButton) findViewById(
									R.id.config_screen_voip_always_cpu_lock)
									.findViewById(R.id.toggle)).isChecked();
							if (LogSettings.MARKET) {
								MktLog.w("RCConfig",
										"AlwaysCPULockForSIPstack: "
												+ isChecked);
							}
							RCMProviderHelper.setAlwaysCPULockForSIPstack(
									ConfigVOIPActivity.this, isChecked);
						}
					});

			/**
			 * Codec priorities set
			 */
			View wideBandViewItem = findViewById(R.id.config_screen_voip_wideband_codec);
			wideBandViewItem.setEnabled(true);
			((TextView) wideBandViewItem.findViewById(R.id.name))
					.setText(R.string.config_screen_voip_wideband_codec_title);
			((TextView) wideBandViewItem.findViewById(R.id.label))
					.setText(R.string.config_screen_voip_codec_configuration_label);
			wideBandViewItem.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						Intent i = new Intent(ConfigVOIPActivity.this,
								ConfigVOIPCodecActivity.class);
						i.putExtra(
								ConfigVOIPCodecActivity.IS_WIDEBAND_BOOLEAN_EXTRA,
								true);
						ConfigVOIPActivity.this.startActivity(i);
					} catch (java.lang.Throwable th) {
						if (LogSettings.MARKET) {
							MktLog.w(
									TAG,
									"On wide-band click error: "
											+ th.toString());
						}
					}
				}
			});

			View narrowBandViewItem = findViewById(R.id.config_screen_voip_narrowband_codec);
			narrowBandViewItem.setEnabled(true);
			((TextView) narrowBandViewItem.findViewById(R.id.name))
					.setText(R.string.config_screen_voip_narrowband_codec_title);
			((TextView) narrowBandViewItem.findViewById(R.id.label))
					.setText(R.string.config_screen_voip_codec_configuration_label);
			narrowBandViewItem.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						Intent i = new Intent(ConfigVOIPActivity.this,
								ConfigVOIPCodecActivity.class);
						i.putExtra(
								ConfigVOIPCodecActivity.IS_WIDEBAND_BOOLEAN_EXTRA,
								false);
						ConfigVOIPActivity.this.startActivity(i);
					} catch (java.lang.Throwable th) {
						if (LogSettings.MARKET) {
							MktLog.w(
									TAG,
									"On wide-band click error: "
											+ th.toString());
						}
					}
				}
			});

			/**
			 * Kill SIP service option
			 */
			View killServiceViewItem = findViewById(R.id.config_screen_voip_kill_sip_service_process);
			killServiceViewItem.setEnabled(true);
			((TextView) killServiceViewItem.findViewById(R.id.name))
					.setText(R.string.config_screen_voip_kill_service_process_title);
			((TextView) killServiceViewItem.findViewById(R.id.label))
					.setText(R.string.config_screen_voip_kill_service_procees_label);
			killServiceViewItem.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						IServicePJSIP service = SIProxy.getInstance()
								.getService();
						if (service == null) {
							Toast.makeText(ConfigVOIPActivity.this,
									"Error: service is null", Toast.LENGTH_LONG)
									.show();
							return;
						}

						int servicePID = service.getSipServicePID();
						if (servicePID == android.os.Process.myPid()) {
							Toast.makeText(
									ConfigVOIPActivity.this,
									"Error: service is launched in the same process",
									Toast.LENGTH_LONG).show();
							return;
						}
						Toast.makeText(ConfigVOIPActivity.this,
								"Killing SIP Service process " + servicePID,
								Toast.LENGTH_LONG).show();
						android.os.Process.killProcess(servicePID);
					} catch (java.lang.Throwable th) {
						Toast.makeText(ConfigVOIPActivity.this,
								"Killing SIP Service error:" + th.toString(),
								Toast.LENGTH_LONG).show();
					}
				}
			});

			/**
			 * Sends QUIT signal to SIP service process option
			 */
			View quitSignalServiceViewItem = findViewById(R.id.config_screen_voip_quit_signal_sip_service_process);
			quitSignalServiceViewItem.setEnabled(true);
			((TextView) quitSignalServiceViewItem.findViewById(R.id.name))
					.setText(R.string.config_screen_voip_quit_service_process_title);
			((TextView) quitSignalServiceViewItem.findViewById(R.id.label))
					.setText(R.string.config_screen_voip_quit_service_process_label);
			quitSignalServiceViewItem.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						IServicePJSIP service = SIProxy.getInstance()
								.getService();
						if (service == null) {
							Toast.makeText(ConfigVOIPActivity.this,
									"Error: service is null", Toast.LENGTH_LONG)
									.show();
							return;
						}

						int servicePID = service.getSipServicePID();
						if (servicePID == android.os.Process.myPid()) {
							Toast.makeText(
									ConfigVOIPActivity.this,
									"Error: service is launched in the same process",
									Toast.LENGTH_LONG).show();
							return;
						}
						Toast.makeText(
								ConfigVOIPActivity.this,
								"Send QUIT signal SIP Service process "
										+ servicePID, Toast.LENGTH_LONG).show();
						android.os.Process.sendSignal(servicePID,
								android.os.Process.SIGNAL_QUIT);
					} catch (java.lang.Throwable th) {
						Toast.makeText(
								ConfigVOIPActivity.this,
								"Sending QUIT signal to SIP Service process failed:"
										+ th.toString(), Toast.LENGTH_LONG)
								.show();
					}
				}
			});

			/**
			 * Emulate SIP service process crash option
			 */
			View emulateServiceCrashViewItem = findViewById(R.id.config_screen_voip_emulate_sip_service_process_crash);
			emulateServiceCrashViewItem.setEnabled(true);
			((TextView) emulateServiceCrashViewItem.findViewById(R.id.name))
					.setText(R.string.config_screen_voip_emulate_service_process_crash_title);
			((TextView) emulateServiceCrashViewItem.findViewById(R.id.label))
					.setText(R.string.config_screen_voip_emulate_service_process_crash_label);
			emulateServiceCrashViewItem
					.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							try {
								IServicePJSIP service = SIProxy.getInstance()
										.getService();
								if (service == null) {
									Toast.makeText(ConfigVOIPActivity.this,
											"Error: service is null",
											Toast.LENGTH_LONG).show();
									return;
								}

								int servicePID = service.getSipServicePID();
								if (servicePID == android.os.Process.myPid()) {
									Toast.makeText(
											ConfigVOIPActivity.this,
											"Error: service is launched in the same process",
											Toast.LENGTH_LONG).show();
									return;
								}
								Toast.makeText(
										ConfigVOIPActivity.this,
										"Emulating crash in SIP Service process in "
												+ SERVICE_CRASH_EMULATION_DELAY_MS
												+ "ms", Toast.LENGTH_LONG)
										.show();

								service.testMode_emualateCrash(SERVICE_CRASH_EMULATION_DELAY_MS);
							} catch (java.lang.Throwable th) {
								Toast.makeText(
										ConfigVOIPActivity.this,
										"Sending QUIT signal to SIP Service process failed:"
												+ th.toString(),
										Toast.LENGTH_LONG).show();
							}
						}
					});

			/**
			 * Clean-up Application and Service log, clean-up latest finished
			 * calls
			 */
			View cleanupViewItem = findViewById(R.id.config_screen_voip_cleanup_logs);
			cleanupViewItem.setEnabled(true);
			((TextView) cleanupViewItem.findViewById(R.id.name))
					.setText(R.string.config_screen_voip_cleanup_title);
			((TextView) cleanupViewItem.findViewById(R.id.label))
					.setText(R.string.config_screen_voip_cleanup_label);
			cleanupViewItem.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						new CleanUpAsyncTask().execute();
					} catch (java.lang.Throwable th) {
						Toast.makeText(ConfigVOIPActivity.this,
								"Cleanup failed:" + th.toString(),
								Toast.LENGTH_LONG).show();
					}
				}
			});

			/**
			 * Emulate SIP restart when commands queue is exceeded and
			 * normalized.
			 */
			View commandsConcistencyCheckNumbersViewItem = findViewById(R.id.config_screen_voip_test_commands_consistency_check_numbers);
			commandsConcistencyCheckNumbersViewItem.setEnabled(true);
			((TextView) commandsConcistencyCheckNumbersViewItem
					.findViewById(R.id.name))
					.setText(R.string.config_screen_voip_commands_consistensy_check_numbers_title);
			((TextView) commandsConcistencyCheckNumbersViewItem
					.findViewById(R.id.label))
					.setText(R.string.config_screen_voip_commands_consistensy_check_numbers_label);
			commandsConcistencyCheckNumbersViewItem
					.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							try {
								IServicePJSIP service = SIProxy.getInstance()
										.getService();
								if (service == null) {
									Toast.makeText(ConfigVOIPActivity.this,
											"Error: service is null",
											Toast.LENGTH_LONG).show();
									return;
								}

								int servicePID = service.getSipServicePID();
								if (servicePID == android.os.Process.myPid()) {
									Toast.makeText(
											ConfigVOIPActivity.this,
											"Error: service is launched in the same process",
											Toast.LENGTH_LONG).show();
									return;
								}
								service.testEnqueedCommandsMaxNumberExceeded();
							} catch (java.lang.Throwable th) {
							}
						}
					});

			/**
			 * Emulate SIP restart when commands is executed for long time.
			 */
			View commandsConcistencyCheckExecutionViewItem = findViewById(R.id.config_screen_voip_test_commands_consistency_check_execution);
			commandsConcistencyCheckExecutionViewItem.setEnabled(true);
			((TextView) commandsConcistencyCheckExecutionViewItem
					.findViewById(R.id.name))
					.setText(R.string.config_screen_voip_commands_consistensy_check_execution_title);
			((TextView) commandsConcistencyCheckExecutionViewItem
					.findViewById(R.id.label))
					.setText(R.string.config_screen_voip_commands_consistensy_check_execution_label);
			commandsConcistencyCheckExecutionViewItem
					.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							try {
								IServicePJSIP service = SIProxy.getInstance()
										.getService();
								if (service == null) {
									Toast.makeText(ConfigVOIPActivity.this,
											"Error: service is null",
											Toast.LENGTH_LONG).show();
									return;
								}

								int servicePID = service.getSipServicePID();
								if (servicePID == android.os.Process.myPid()) {
									Toast.makeText(
											ConfigVOIPActivity.this,
											"Error: service is launched in the same process",
											Toast.LENGTH_LONG).show();
									return;
								}
								service.testCommandExecutionTimeExceeded();
							} catch (java.lang.Throwable th) {
							}
						}
					});

			/**
			 * TSC Setting
			 */
			View tscSettingViewItem = findViewById(R.id.config_tsc);
			((TextView) tscSettingViewItem
					.findViewById(R.id.name))
					.setText(R.string.config_tsc);
			tscSettingViewItem.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(ConfigVOIPActivity.this,ConfigTSCActivity.class);
					startActivity(intent);
				}
			});
		}
	}

	private class CleanUpAsyncTask extends AsyncTask<Object, Void, Integer> {
		private static final int SUCCESS = 0;
		private static final int TIMEOUT = 1;
		private static final int FAILED = 2;
		private static final int EXCEPTION = 3;
		private String exception = "";

		@Override
		protected Integer doInBackground(Object... arg0) {
			try {
				LoggingManager.cleanup();
			} catch (java.lang.Throwable th) {
			}
			try {
				VoIPCfgCleanUpCommand task = new VoIPCfgCleanUpCommand();
				int state = task.execute(3000, true);
				if (state == TimedSIProxyTask.TIMEOUT) {
					return TIMEOUT;
				} else if (state == TimedSIProxyTask.FAILED) {
					return FAILED;
				}
				return SUCCESS;
			} catch (java.lang.Throwable th) {
				exception = th.toString();
			}
			return EXCEPTION;
		}

		@Override
		public void onPostExecute(Integer result) {
			int res = result.intValue();
			switch (res) {
			case (SUCCESS):
				Toast.makeText(ConfigVOIPActivity.this, "Clean-up completed",
						Toast.LENGTH_LONG).show();
				return;
			case (TIMEOUT):
				Toast.makeText(ConfigVOIPActivity.this,
						"Clean-up failed: TIMEOUT", Toast.LENGTH_LONG).show();
				return;
			case (FAILED):
				Toast.makeText(ConfigVOIPActivity.this, "Clean-up: FAILED",
						Toast.LENGTH_LONG).show();
				return;
			case (EXCEPTION):
				Toast.makeText(ConfigVOIPActivity.this,
						"Clean-up failed (async):" + exception,
						Toast.LENGTH_LONG).show();
				return;
			}
		}
	}

	private static class VoIPCfgCleanUpCommand extends TimedSIProxyTask {
		public VoIPCfgCleanUpCommand() {
			super("VoIPCfgCleanUpCommand");
		}

		@Override
		public void doServiceRequest(IServicePJSIP service) {
			try {
				service.cleanupLogAndLatestFinishedCalls();
				if (LogSettings.MARKET) {
					MktLog.e(TAG, "cleanupLogAndLatestFinishedCalls");
				}
			} catch (java.lang.Throwable th) {
				if (LogSettings.MARKET) {
					MktLog.e(TAG, "doServiceRequest:error:", th);
				}
			}
		}
	}
}
