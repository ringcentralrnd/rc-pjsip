/** 
 * Copyright (C) 2010-2011 RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.settings;

import com.ringcentral.android.R;
import com.ringcentral.android.contacts.RcAlertDialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class VoipWhatsNewDialog {
    
    /**
     * return's VoIP whats new dialog to be shown  
     * NB: with specified listener, for login screen & VoIP settings it should be different (!)
     * @param context 
     * @param listener - special listener, 
     * @return
     */
    public static AlertDialog getMe(Context context, DialogInterface.OnClickListener listener) {
        return RcAlertDialog.getBuilder(context)
        .setTitle(R.string.voip_whats_new_caption)
        .setIcon(R.drawable.symbol_exclamation)
        .setMessage(R.string.voip_whats_new_description)
        .setCancelable(false)
        .setPositiveButton(R.string.voip_whats_new_ok, listener)
        .create(); 
    }
}
