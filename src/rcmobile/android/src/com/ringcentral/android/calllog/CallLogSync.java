/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.calllog;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.api.AbstractRequestObject;
import com.ringcentral.android.api.RequestInfoStorage;
import com.ringcentral.android.api.Utils;
import com.ringcentral.android.api.pojo.CallLogPOJO;
import com.ringcentral.android.api.pojo.CallLogsListPOJO;
import com.ringcentral.android.contacts.Cont;
import com.ringcentral.android.contacts.ContactPhoneNumber;
import com.ringcentral.android.contacts.LocalSyncService;
import com.ringcentral.android.contacts.Cont.acts.BindSync;
import com.ringcentral.android.contacts.Cont.acts.ContactBinding;
import com.ringcentral.android.contacts.LocalSyncService.LocalSyncControl;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.provider.RCMDataStore.CallLogTable;
import com.ringcentral.android.utils.PhoneUtils;

public class CallLogSync {
    
    /**
     * Defines logging tag for sync operation.
     */
    private static final String SYNC_LOG_TAG = "[RC]CallLogSync";
    
    /**
     * Get More records flag mask.
     */
    private static final int DONT_SHOW_GET_MORE_RECORDS_MASK = 0x00000002;
    
    /**
     * Clean-up flag mask.
     */
    private static final int CLEAN_UP_MASK = 0x00000001;
    
    /**
     * Call Log maximum number of records (local storage) 
     */
    private static final int CALL_LOG_SIZE_LIMIT = 100;
    
    /**
     * Call Log records projection.
     */
    private static final String[] PROJECTION_HAS_MORE_RECORDS = new String[]{
            CallLogTable.RCM_FOLDER,
            CallLogTable.HAS_MORE_RECORD_ITEM
    };
    
    /**
     * Remove "Get More Records" special record from DB.
     * 
     * @param context the execution context
     * @param mailboxId the mailboxId
     * @param type the call log type (missed/all)
     */
    private static void removeGMRItem(Context context, long mailboxId, int type) {
        try {
            context.getContentResolver().delete(
                    UriHelper.getUri(RCMProvider.CALL_LOG, mailboxId),
                    "(" + CallLogTable.RCM_FOLDER + '=' + type + ") AND (" + CallLogTable.HAS_MORE_RECORD_ITEM
                            + " = 1)", null);
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                MktLog.w(SYNC_LOG_TAG, "removeGMRItem error: " + error.getMessage());
            }
        }
    }
    
    /**
     * Forms content values for creation GMR record in DB.
     * 
     * @param context
     *            the execution context
     * @param mailboxId
     *            the mailboxId
     * @param type
     *            the call log type (missed/all)
     * @return the values
     */
    private static ContentValues formGMRPutValue(Context context, long mailboxId, int type) {
        ContentValues getMoreRecordsValue = new ContentValues();
        getMoreRecordsValue.put(RCMDataStore.CallLogTable.MAILBOX_ID, mailboxId);
        getMoreRecordsValue.put(RCMDataStore.CallLogTable.RCM_FOLDER, type);
        getMoreRecordsValue.put(RCMDataStore.CallLogTable.JEDI_START, 0);
        getMoreRecordsValue.put(RCMDataStore.CallLogTable.HAS_MORE_RECORD_ITEM, 1);
        return getMoreRecordsValue;
    }
    
    /**
     * Deletes all records from Call Log DB.
     * 
     * @param context
     *            the execution context
     * @param mailboxId
     *            the mailboxId
     * @param type
     *            the call log type (missed/all)
     */
    private static void deleteAllCallLogStored(Context context, long mailboxId, int type) {
        try {
            context.getContentResolver().delete(UriHelper.getUri(RCMProvider.CALL_LOG, mailboxId),
                    RCMDataStore.CallLogTable.RCM_FOLDER + '=' + type, null);
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                MktLog.w(SYNC_LOG_TAG, "deleteAllCallLogStored error: " + error.getMessage());
            }
        }
    }
    
    /**
     * Update Call Log DB based on <code>list</code> jobs.
     * 
     * @param context
     *            the execution context
     * @param mailboxId
     *            the mailboxId
     * @param type
     *            the call log type (missed/all)
     * @param list
     *            list with jobs (if any)
     */
    private static boolean updateCallLogContent(Context context, long mailboxId, int type, List<CallLogPOJO> list) {
        if (list == null) {
            return false;
        }
        boolean hasChanges = false;
        for (CallLogPOJO item: list) {
            if (item.recordStatus == CallLogPOJO.Status.UPDATE) {
                if (item.cv != null) {
                    if (item.recordDbId != -1) {
                        try {
                            if (LogSettings.ENGINEERING) {
                                EngLog.w(SYNC_LOG_TAG, "updateCallLogContent (update) record " + item.recordDbId);
                            }
                            context.getContentResolver().update(UriHelper.getUri(RCMProvider.CALL_LOG, mailboxId), 
                                    item.cv, RCMDataStore.CallLogTable.RCM_FOLDER + '=' + type + " AND "
                                    + RCMDataStore.CallLogTable._ID + "=" + item.recordDbId, null);
                            hasChanges = true;
                        } catch (java.lang.Throwable error) {
                            if (LogSettings.MARKET) {
                                MktLog.w(SYNC_LOG_TAG, "updateCallLogContent (update) error: " + error.getMessage());
                            }
                        }
                    }
                    item.cv = null;
                }
                item.recordStatus = CallLogPOJO.Status.KEEP;
            } else if (item.recordStatus == CallLogPOJO.Status.DELETE) {
                if (item.recordDbId != -1) {
                    try {
                        if (LogSettings.QA) {
                            QaLog.w(SYNC_LOG_TAG, "updateCallLogContent (delete) record " + item.recordDbId);
                        }
                        context.getContentResolver().delete(UriHelper.getUri(RCMProvider.CALL_LOG, mailboxId),
                                RCMDataStore.CallLogTable.RCM_FOLDER + '=' + type + " AND " + RCMDataStore.CallLogTable._ID + "=" + item.recordDbId,
                                null);
                        hasChanges = true;
                    } catch (java.lang.Throwable error) {
                        if (LogSettings.MARKET) {
                            MktLog.w(SYNC_LOG_TAG, "updateCallLogContent (delete) error: " + error.getMessage());
                        }
                    }
                }
                item.cv = null;
                item.recordStatus = CallLogPOJO.Status.IGNORE;
            }
        }
        
        return hasChanges;
    }
    
    /**
     * Process/Stores Call Log data.
     * 
     * @param context
     *            the execution context
     * @param object
     *            the call log request
     * @param result
     *            the response
     * @return
     */
    public static void storeCallLog(Context context, AbstractRequestObject object, Object result) {
        CallLogsListPOJO logs = (CallLogsListPOJO) result;

        if (logs == null || object == null) {
            if (LogSettings.MARKET) {
                MktLog.w(SYNC_LOG_TAG, "storeCallLog: invalid parameters");
            }
            return;
        }

        boolean hasDbChanges = false;
        
        boolean serverCleanUp = (CLEAN_UP_MASK & logs.getBreakFlag()) != 0;
        boolean getMoreRecordsItemExists = false;
        long mailboxId = RCMProviderHelper.getCurrentMailboxId(context);
        Uri callLogURI = UriHelper.getUri(RCMProvider.CALL_LOG);
        int type = (object.getRequestInfo().getType() == RequestInfoStorage.GET_ALL_CALL_LOGS ? RCMDataStore.CallLogTable.LOGS_ALL
                : RCMDataStore.CallLogTable.LOGS_MISSED);

        String FLOG_TAG = null;
        if (type == RCMDataStore.CallLogTable.LOGS_MISSED) {
            FLOG_TAG = "> CallLog(Missed): ";
        } else {
            FLOG_TAG = "> CallLog(All): ";
        }
            
        boolean serverShowGMR = (logs.getBreakFlag() & DONT_SHOW_GET_MORE_RECORDS_MASK) == 0;

        if (LogSettings.MARKET) {
            MktLog.i(SYNC_LOG_TAG, FLOG_TAG + "Server(Clean-up bit) is " + serverCleanUp);
            MktLog.i(SYNC_LOG_TAG, FLOG_TAG + "Server(GMR bit) is " + serverShowGMR);
        }
        
        
        if (serverCleanUp) {
            if (LogSettings.MARKET) {
                MktLog.w(SYNC_LOG_TAG, FLOG_TAG + " Server -> Clean-up previous data");
            }
            deleteAllCallLogStored(context, mailboxId, type);
            hasDbChanges = true;
        } else {
            Cursor c = null;
            try {
                c = context.getContentResolver().query(
                        UriHelper.getUri(RCMProvider.CALL_LOG, mailboxId),
                        PROJECTION_HAS_MORE_RECORDS,
                        "(" + CallLogTable.RCM_FOLDER + '=' + type + ") AND (" + CallLogTable.HAS_MORE_RECORD_ITEM
                                + " = 1)", null, null);
                if (c != null && c.getCount() > 0) {
                    getMoreRecordsItemExists = true;
                }
                
                /**
                 * Workaround - if GMR is in Loading state and server return
                 * nothing to be updated and GMR true the value progress bar can
                 * be still shown
                 */
                if (getMoreRecordsItemExists) {
                    getMoreRecordsItemExists = false;
                    removeGMRItem(context, mailboxId, type);
                }
            } catch (java.lang.Throwable error) {
                if (LogSettings.MARKET) {
                    MktLog.w(SYNC_LOG_TAG, FLOG_TAG + "Retrive GMR error: " + error.getMessage());
                }
            } finally {
                if (c != null) {
                    try {
                        c.close();
                    } catch (Exception ex) {
                    }
                    c = null;
                }
            }
        }

        List<CallLogPOJO> newLogs = logs.getLogs();
        List<CallLogPOJO> storedLogs = RCMProviderHelper.getCallLogs(context, mailboxId, type);
        if (LogSettings.MARKET) {
            MktLog.d(SYNC_LOG_TAG, FLOG_TAG + "Records number from server:" + newLogs.size());
            MktLog.d(SYNC_LOG_TAG, FLOG_TAG + "Records number stored:" + storedLogs.size());
        }
        List<CallLogPOJO> records = new ArrayList<CallLogPOJO>();
        boolean needUpdateStored = false;
        for (CallLogPOJO item : newLogs) {
            boolean found = false;
            for (CallLogPOJO stored : storedLogs) {
                if (stored.equals(item)) {
                    found = true;

                    ContentValues cv = new ContentValues();

                    if (stored.getStart() != item.getStart()) {
                        stored.setStart(item.getStart());
                        cv.put(CallLogTable.JEDI_START, stored.getStart());
                        stored.cv = cv;
                        stored.recordStatus = CallLogPOJO.Status.UPDATE;
                        needUpdateStored = true;
                    }

                    if (stored.getStatus() != item.getStatus()) {
                        stored.setStatus(item.getStatus());
                        cv.put(CallLogTable.JEDI_STATUS, stored.getStatus());
                        stored.cv = cv;
                        stored.recordStatus = CallLogPOJO.Status.UPDATE;
                        needUpdateStored = true;
                    }

                    if (stored.getLength() != item.getLength()) {
                        stored.setLength(item.getLength());
                        cv.put(CallLogTable.JEDI_LENGTH, stored.getLength());
                        stored.cv = cv;
                        stored.recordStatus = CallLogPOJO.Status.UPDATE;
                        needUpdateStored = true;
                    }

                    break;
                }
            }
            if (!found) {
                records.add(item);
            }
        }

        newLogs = records;

        if (LogSettings.MARKET) {
            MktLog.d(SYNC_LOG_TAG, FLOG_TAG + "New records number:" + newLogs.size());
        }
        
        if (newLogs.size() == 0) {
            if (LogSettings.MARKET) {
                MktLog.d(SYNC_LOG_TAG, FLOG_TAG + "No new records (update stored = " + needUpdateStored
                        + ")");
            }
            if (needUpdateStored) {
                if (LogSettings.MARKET) {
                    MktLog.d(SYNC_LOG_TAG, FLOG_TAG + "Previous call log records updating.");
                }
                if (updateCallLogContent(context, mailboxId, type, storedLogs)) {
                    hasDbChanges = true;
                }
            }

            if (serverShowGMR && (storedLogs.size() < CALL_LOG_SIZE_LIMIT)) {
                if (LogSettings.MARKET) {
                    MktLog.d(SYNC_LOG_TAG, FLOG_TAG + "Show Get More Records...");
                }
                if (!getMoreRecordsItemExists) {
                    ContentValues getMoreRecordsValue = formGMRPutValue(context, mailboxId, type);
                    try {
                        context.getContentResolver().insert(callLogURI, getMoreRecordsValue);
                    } catch (java.lang.Throwable error) {
                        if (LogSettings.MARKET) {
                            MktLog.w(SYNC_LOG_TAG, FLOG_TAG + "Set GMR error: " + error.getMessage());
                        }
                    }
                }
            } else {
                if (LogSettings.MARKET) {
                    QaLog.i(SYNC_LOG_TAG, FLOG_TAG + "Don't show Get More Records...");
                }
                if (getMoreRecordsItemExists) {
                    removeGMRItem(context, mailboxId, type);
                }
            }
            if (hasDbChanges) {
                LocalSyncService.syncCallLog(context);
            }
            return;
        }

        List<CallLogPOJO> finalLogs = new ArrayList<CallLogPOJO>();
        List<CallLogPOJO> processFinalLogs = null;
        for (CallLogPOJO item : newLogs) {
            finalLogs.add(item);
        }
        for (CallLogPOJO item : storedLogs) {
            finalLogs.add(item);
        }

        Utils.sortLogs(finalLogs);
        processFinalLogs = finalLogs;
        if (finalLogs.size() > CALL_LOG_SIZE_LIMIT) {
            if (LogSettings.MARKET) {
                MktLog.i(SYNC_LOG_TAG, FLOG_TAG + "Size limit (Clean-up)");
            }
            records = new ArrayList<CallLogPOJO>();
            int number = 0;
            for (CallLogPOJO item : finalLogs) {
                number++;
                if (number > CALL_LOG_SIZE_LIMIT) {
                    if (item.recordStatus == CallLogPOJO.Status.KEEP || item.recordStatus == CallLogPOJO.Status.UPDATE) {
                        item.recordStatus = CallLogPOJO.Status.DELETE;
                    }
                    
                    if (item.recordStatus == CallLogPOJO.Status.NEW) {
                        item.recordStatus = CallLogPOJO.Status.IGNORE;
                    }
                } else {
                    records.add(item);
                }
            }
            finalLogs = records;
        }

        if (updateCallLogContent(context, mailboxId, type, processFinalLogs)) {
            if (LogSettings.MARKET) {
                MktLog.d(SYNC_LOG_TAG, FLOG_TAG + "DB updated");
            }
            hasDbChanges = true;
        }

        if (LogSettings.MARKET) {
            int newRecords = 0;
            int keepRecords = 0;
            for (CallLogPOJO item : finalLogs) {
                if (item.recordStatus == CallLogPOJO.Status.NEW) {
                    newRecords++;
                } else if (item.recordStatus == CallLogPOJO.Status.KEEP) {
                    keepRecords++;
                } 
            }
            
            MktLog.i(SYNC_LOG_TAG, FLOG_TAG + "Records new=" + newRecords + "; stored="+ keepRecords);
        }
        
        List<ContentValues> logValues = new ArrayList<ContentValues>();
        for (CallLogPOJO item : newLogs) {
            if (item.recordStatus != CallLogPOJO.Status.NEW) {
                continue;
            }
            ContentValues initialValues = new ContentValues();
            initialValues.put(RCMDataStore.CallLogTable.MAILBOX_ID, mailboxId);
            initialValues.put(RCMDataStore.CallLogTable.JEDI_FROM_NAME, item.getFromName());
            initialValues.put(RCMDataStore.CallLogTable.JEDI_FROM_PHONE, item.getFromPhone());
            initialValues.put(RCMDataStore.CallLogTable.JEDI_LENGTH, item.getLength());
            initialValues.put(RCMDataStore.CallLogTable.JEDI_LOCATION, item.getLocation());
            initialValues.put(RCMDataStore.CallLogTable.JEDI_CALL_DIRECTION, item.getCallDirection());
            initialValues.put(RCMDataStore.CallLogTable.JEDI_TO_NAME, item.getToName());
            initialValues.put(RCMDataStore.CallLogTable.JEDI_TO_PHONE, item.getToPhone());
            initialValues.put(RCMDataStore.CallLogTable.JEDI_STATUS, item.getStatus());
            initialValues.put(RCMDataStore.CallLogTable.JEDI_CALL_TYPE, item.getCallType());
            initialValues.put(RCMDataStore.CallLogTable.JEDI_PIN, item.getPin());
            initialValues.put(RCMDataStore.CallLogTable.JEDI_RECORD_ID, item.getRecordId());
            initialValues.put(RCMDataStore.CallLogTable.JEDI_START, item.getStart());
            initialValues.put(RCMDataStore.CallLogTable.RCM_FOLDER, type);
            String number;
            if (item.getCallDirection() == CallLogPOJO.DIRECTION_OUTGOING_INT) {
                number = item.getToPhone();
            } else {
                number = item.getFromPhone();
            }

            ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumberWithoutTrim(number);
            
            if (!cpn.isValid) {
                cpn = PhoneUtils.getContactPhoneNumber(number);    
            }

            if (cpn != null && cpn.isValid) {
                initialValues.put(RCMDataStore.CallLogTable.NORMALIZED_NUMBER, cpn.normalizedNumber);
                initialValues.put(RCMDataStore.CallLogTable.IS_VALID_NUMBER, 1);
            } else {
                initialValues.put(RCMDataStore.CallLogTable.NORMALIZED_NUMBER, number);
                initialValues.put(RCMDataStore.CallLogTable.IS_VALID_NUMBER, 0);
            }

            initialValues.put(RCMDataStore.CallLogTable.BIND_HAS_CONTACT, 0);
            initialValues.put(RCMDataStore.CallLogTable.BIND_ID, -1);
            initialValues.put(RCMDataStore.CallLogTable.BIND_DISPLAY_NAME, (String) null);
            initialValues.put(RCMDataStore.CallLogTable.BIND_IS_PERSONAL_CONTACT, 0);
            initialValues.put(RCMDataStore.CallLogTable.BIND_ORIGINAL_NUMBER, (String) null);
            initialValues.put(RCMDataStore.CallLogTable.HAS_MORE_RECORD_ITEM, 0);
            logValues.add(initialValues);
        }
        
        try {
            hasDbChanges = true;
            context.getContentResolver().bulkInsert(UriHelper.getUri(RCMProvider.CALL_LOG),
                    logValues.toArray(new ContentValues[0]));
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                MktLog.w(SYNC_LOG_TAG, FLOG_TAG +  "Store delta error: " + error.getMessage());
            }
        }
        
        if (LogSettings.MARKET) {
            if (logValues.size() > 0) {
                MktLog.i(SYNC_LOG_TAG, FLOG_TAG + "Added new records: " + logValues.size());
            }
        }
        
        if (serverShowGMR && (finalLogs.size() < CALL_LOG_SIZE_LIMIT)) {
            if (LogSettings.MARKET) {
                MktLog.d(SYNC_LOG_TAG, FLOG_TAG + "Show Get More Records...");
            }
            if (!getMoreRecordsItemExists) {
                ContentValues getMoreRecordsValue = formGMRPutValue(context, mailboxId, type);
                try {
                    context.getContentResolver().insert(callLogURI, getMoreRecordsValue);
                } catch (java.lang.Throwable error) {
                    if (LogSettings.MARKET) {
                        MktLog.w(SYNC_LOG_TAG, FLOG_TAG + "Set GMR error: " + error.getMessage());
                    }
                }
            }
        } else {
            if (LogSettings.MARKET) {
                MktLog.d(SYNC_LOG_TAG, FLOG_TAG + "Don't show Get More Records...");
            }
            if (getMoreRecordsItemExists) {
                removeGMRItem(context, mailboxId, type);
            }
        }

        if (hasDbChanges) {
            LocalSyncService.syncCallLog(context);
        }
        return;
    }

    
    /**
     * Synchronize Call Log records.
     * 
     * @param sync the sync control
     * 
     * @return the status
     */
    public static final boolean sync(LocalSyncControl sync) {
        Uri uri = UriHelper.getUri(RCMProvider.CALL_LOG, sync.getMailboxId());
        Cursor c = null;
        ArrayList<SyncItem> updates = new ArrayList<SyncItem>();
        try {
            c = sync.getContext().getContentResolver().query(uri, SYNC_PROJECTION,
                    CallLogTable.HAS_MORE_RECORD_ITEM + " = 0", null, null);
            if (c != null) {
                c.moveToPosition(-1);
                while (c.moveToNext()) {
                    try {
                        if (sync.isTerminated()) {
                            return false;
                        }
                        
                        SyncItem item = new SyncItem();
                        item.id = c.getLong(SYNC_ID_INDX);
                        ContactBinding bind = readSyncBind(sync.getContext(), c);
                        BindSync bsync = Cont.acts.syncBind(sync.getContext(), sync.getMailboxId(), bind);
                        
                        if (bsync.syncState == BindSync.State.INVALID_RECORD) {
                            Cont.acts.bindSyncTrace(sync.getContext(), bsync, SYNC_LOG_TAG, "", item.id);
                        } else if (bsync.syncState == BindSync.State.NOT_CHANGED) {
                            continue;
                        } else {
                            Cont.acts.bindSyncTrace(sync.getContext(), bsync, SYNC_LOG_TAG, "", item.id);
                            item.sync = bsync;
                            updates.add(item);
                        }
                        
                    } catch (java.lang.Throwable error) {
                        if (LogSettings.MARKET) {
                            MktLog.e(SYNC_LOG_TAG, "sync (read): ", error);
                        }
                    }
                }
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                MktLog.e(SYNC_LOG_TAG, "sync: ", error);
            }
            return false;
        } finally {
            if (c != null) {
                c.close();
            }
        }
        if (sync.isTerminated()) {
            return false;
        }
        
        if (updates.size() > 0) {
            for (SyncItem item: updates) {
                updateCallLogBind(sync.getContext(), sync.getMailboxId(), item.id, item.sync);
                if (sync.isTerminated()) {
                    return true;
                }
            }
            return true;
        }
        return false;
    }

    public static void updateCallLogBind(Context context, long mailboxId, long recordId, BindSync sync) {
        try {
            Uri uri = UriHelper.getUri(RCMProvider.CALL_LOG, mailboxId, recordId);
            context.getContentResolver().update(uri, getCvForUpdate(sync), null, null);
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                MktLog.e(SYNC_LOG_TAG, "updateCallLogBind error: " + error.toString());
            }
        }
    }
    
    private static final String[] SYNC_PROJECTION = { 
            CallLogTable._ID,
            CallLogTable.IS_VALID_NUMBER,
            CallLogTable.NORMALIZED_NUMBER,
            CallLogTable.BIND_HAS_CONTACT,
            CallLogTable.BIND_ID,
            CallLogTable.BIND_DISPLAY_NAME,
            CallLogTable.BIND_ORIGINAL_NUMBER,
            CallLogTable.BIND_IS_PERSONAL_CONTACT
    };
    private static final int SYNC_ID_INDX = 0;
    private static final int SYNC_IS_VALID_NUMBER_INDX = 1;
    private static final int SYNC_NORMILIZED_NUMBER_INDX = 2;
    private static final int SYNC_BIND_HAS_CONTACT_INDX = 3;
    private static final int SYNC_BIND_ID_INDX = 4;
    private static final int SYNC_BIND_DISPLAY_NAME_INDX = 5;
    private static final int SYNC_BIND_ORIGINAL_NUMBER_INDX = 6;
    private static final int SYNC_BIND_IS_PERSONAL_CONTACT_INDX = 7;
    
    private static class SyncItem {
        private long id;
        private BindSync sync;
    }
    
    private static ContactBinding readSyncBind(Context context, Cursor c) {
        ContactBinding bind = new ContactBinding();
        if (c.getLong(SYNC_IS_VALID_NUMBER_INDX) == 0) {
            bind.isValid = false;
            return bind;
        }
                
        bind.originalNumber = c.getString(SYNC_NORMILIZED_NUMBER_INDX);
        bind.cpn = PhoneUtils.getContactPhoneNumber(bind.originalNumber);
        bind.isValid = bind.cpn.isValid;
        
        if (!bind.isValid) {
            return bind;
        }
        
        bind.hasContact = (c.getLong(SYNC_BIND_HAS_CONTACT_INDX) > 0);
        
        if (bind.hasContact) {
            bind.phoneId = c.getLong(SYNC_BIND_ID_INDX);
            bind.displayName = c.getString(SYNC_BIND_DISPLAY_NAME_INDX);
            bind.isPersonalContact = (c.getLong(SYNC_BIND_IS_PERSONAL_CONTACT_INDX) > 0);
            bind.phoneNumber = c.getString(SYNC_BIND_ORIGINAL_NUMBER_INDX);
        }
        
        return bind;
    }
    
    private static ContentValues getCvForUpdate(BindSync sync) {
        ContentValues cv = new ContentValues();
        cv.put(RCMDataStore.CallLogTable.BIND_HAS_CONTACT, sync.bind.hasContact ? 1:0);
        cv.put(RCMDataStore.CallLogTable.BIND_ID, sync.bind.phoneId);
        cv.put(RCMDataStore.CallLogTable.BIND_DISPLAY_NAME, sync.bind.displayName);
        cv.put(RCMDataStore.CallLogTable.BIND_IS_PERSONAL_CONTACT, sync.bind.isPersonalContact ? 1:0);
        cv.put(RCMDataStore.CallLogTable.BIND_ORIGINAL_NUMBER, sync.bind.phoneNumber);
        return cv;
    }
}
