/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.calllog;

import android.content.Context;
import android.database.Cursor;
import com.ringcentral.android.R;
import com.ringcentral.android.api.pojo.CallLogPOJO;
import com.ringcentral.android.contacts.Cont.acts.ContactBinding;
import com.ringcentral.android.contacts.Cont;
import com.ringcentral.android.contacts.Cont.acts.PhoneContact;
import com.ringcentral.android.contacts.ContactPhoneNumber;
import com.ringcentral.android.provider.RCMDataStore.CallLogTable;
import com.ringcentral.android.utils.PhoneUtils;

public class CallLogQuery {
    public static final String[] CALLLOG_SUMMARY_PROJECTION = new String[] { 
            CallLogTable._ID, 
            CallLogTable.JEDI_START,
            CallLogTable.JEDI_CALL_DIRECTION, 
            CallLogTable.JEDI_CALL_TYPE, 
            CallLogTable.JEDI_STATUS,
            CallLogTable.JEDI_FROM_NAME, 
            CallLogTable.JEDI_TO_NAME, 
            CallLogTable.HAS_MORE_RECORD_ITEM,
            CallLogTable.NORMALIZED_NUMBER,
            CallLogTable.BIND_HAS_CONTACT,
            CallLogTable.BIND_IS_PERSONAL_CONTACT,
            CallLogTable.BIND_ID, 
            CallLogTable.BIND_DISPLAY_NAME, 
            CallLogTable.BIND_ORIGINAL_NUMBER,
            CallLogTable.IS_VALID_NUMBER, 
            CallLogTable.JEDI_LENGTH,
            CallLogTable.MAILBOX_ID,
            CallLogTable.JEDI_LOCATION

    };
    public static final int CALLLOG_ID_INDX = 0;
    public static final int CALLLOG_START_INDX = 1;
    public static final int CALLLOG_CALL_DIRECTION_INDX = 2;
    public static final int CALLLOG_CALL_TYPE_INDX = 3;
    public static final int CALLLOG_STATUS_INDX = 4;
    public static final int CALLLOG_NAME_FROM_INDX = 5;
    public static final int CALLLOG_NAME_TO_INDX = 6;
    public static final int CALLLOG_HAS_MORE_RECORD_ITEM_INDX = 7;
    public static final int CALLLOG_NORMALIZED_NUMBER_INDX = 8;
    public static final int CALLLOG_BIND_HAS_CONTACT_INDX = 9;
    public static final int CALLLOG_BIND_IS_PERSONAL_CONTACT_INDX = 10;
    public static final int CALLLOG_BIND_ID_INDX = 11;
    public static final int CALLLOG_BIND_DISPLAY_NAME_INDX = 12;
    public static final int CALLLOG_BIND_ORIGINAL_NUMBER_INDX = 13;
    public static final int CALLLOG_IS_VALID_NUMBER_INDX = 14;
    public static final int CALLLOG_LENGTH_INDX = 15;
    public static final int CALLLOG_MAILBOX_INDX = 16;
    public static final int CALLLOG_JEDI_LOCATION_INDX = 17;
    
    /**
     * Read BindSync data (push) from a cursor with
     * {@link #CALLLOG_SUMMARY_PROJECTION} projection.
     * 
     * @param context
     *            the execution context
     * @param c
     *            the cursor (retrieved with {@link #CALLLOG_SUMMARY_PROJECTION}
     *            projection)
     * @return the bind sync with date from the cursor
     */
    public static ContactBinding readSyncBind(Context context, Cursor c) {
        ContactBinding bind = new ContactBinding();
        if (c.getLong(CALLLOG_IS_VALID_NUMBER_INDX) == 0) {
            bind.isValid = false;
            return bind;
        }
                
        bind.originalNumber = c.getString(CALLLOG_NORMALIZED_NUMBER_INDX);
        bind.cpn = PhoneUtils.getContactPhoneNumber(bind.originalNumber);
        bind.isValid = bind.cpn.isValid;
        
        if (!bind.isValid) {
            return bind;
        }
        
        bind.hasContact = (c.getLong(CALLLOG_BIND_HAS_CONTACT_INDX) > 0);
        
        if (bind.hasContact) {
            bind.phoneId = c.getLong(CALLLOG_BIND_ID_INDX);
            bind.displayName = c.getString(CALLLOG_BIND_DISPLAY_NAME_INDX);
            bind.isPersonalContact = (c.getLong(CALLLOG_BIND_IS_PERSONAL_CONTACT_INDX) > 0);
            bind.phoneNumber = c.getString(CALLLOG_BIND_ORIGINAL_NUMBER_INDX);
        }
        
        return bind;
    }
    
    public static final class CallLogItem {
        public String displayName;
        public boolean isValidNumber;
        public ContactPhoneNumber cpn;
        public int callStatus;
        public int callType;
        public int direction;
        public long date;
        public float length;
        public String location;
        public String phoneType;
        public String getPhoneTypeOrLocation(){
        	if(phoneType==null||"".equals(phoneType)){
        		return location;
        	}
        	return phoneType;
        }
    }
    
    /**
     * Reads a Call Log item from cursor with
     * {@link #CALLLOG_SUMMARY_PROJECTION} projection.
     * 
     * @param context
     *            the execution context
     * @param c
     *            the cursor (retrieved with {@link #CALLLOG_SUMMARY_PROJECTION}
     *            projection)
     * @return the item
     */
    public static final CallLogItem readCallLogItem(Context context, Cursor c) {
        CallLogItem i = new CallLogItem();
        boolean isValidNumber = (c.getLong(CallLogQuery.CALLLOG_IS_VALID_NUMBER_INDX) > 0);

        if (isValidNumber) {
            i.cpn = PhoneUtils.getContactPhoneNumber(c.getString(CallLogQuery.CALLLOG_NORMALIZED_NUMBER_INDX));
            if (i.cpn == null || !i.cpn.isValid) {
                isValidNumber = false;
            }
        }
        
        i.isValidNumber = isValidNumber;

        if (c.getLong(CallLogQuery.CALLLOG_BIND_HAS_CONTACT_INDX) > 0) {
            i.displayName = c.getString(CallLogQuery.CALLLOG_BIND_DISPLAY_NAME_INDX);
        }

        i.direction = c.getInt(CallLogQuery.CALLLOG_CALL_DIRECTION_INDX);
        if (i.displayName == null || i.displayName.trim().length() == 0) {
            if (i.direction == CallLogPOJO.DIRECTION_INCOMING_INT) {
                i.displayName = c.getString(CallLogQuery.CALLLOG_NAME_FROM_INDX);
            } else {
                i.displayName = c.getString(CallLogQuery.CALLLOG_NAME_TO_INDX);
            }

            if (i.displayName == null || i.displayName.trim().length() == 0) {
                if (isValidNumber) {
                    i.displayName = i.cpn.phoneNumber.localCanonical;
                } else {
                    if (i.cpn != null && i.cpn.normalizedNumber != null && (i.cpn.normalizedNumber.trim().length() > 0)) {
                        i.displayName = i.cpn.normalizedNumber;
                    } else {
                        i.displayName = context.getString(R.string.calllog_display_name_unknown);
                    }
                }
            }
        }
        
        if(c.getInt(CallLogQuery.CALLLOG_BIND_IS_PERSONAL_CONTACT_INDX)!=0){
        	PhoneContact phoneContact = Cont.acts().lookUpPhoneContactById(context,c.getInt(CallLogQuery.CALLLOG_BIND_ID_INDX));
        	i.phoneType= Cont.acts().getPhoneNumberTag(context, phoneContact.type);
        }
        if(c.getString(CallLogQuery.CALLLOG_JEDI_LOCATION_INDX) != null) {
        	i.location = c.getString(CallLogQuery.CALLLOG_JEDI_LOCATION_INDX);
        } else {
        	i.location = context.getString(R.string.calllog_calltype_159_unknown);
        }

        i.callStatus = c.getInt(CallLogQuery.CALLLOG_STATUS_INDX);
        i.callType = c.getInt(CallLogQuery.CALLLOG_CALL_TYPE_INDX);
        i.date = c.getLong(CallLogQuery.CALLLOG_START_INDX);
        i.length = c.getFloat(CallLogQuery.CALLLOG_LENGTH_INDX);
        
        return i;
    }
}
