/**
 * 
 */
package com.ringcentral.android.api.parsers;

import com.ringcentral.android.api.XmlParsingObjectAbstract;

/**
 * @author Denis Kudja
 *
 */
public class MessageResponseHandler extends XmlParsingObjectAbstract {

	private final static String sf_tag_name 				= "RESPONSE";
	
	private final static String sf_sessionIdentity 			= "SesIdentity";
	private final static String sf_SessionID				= "SessionID";
	private final static String sf_MailboxId				= "MailboxId";
	private final static String sf_modificationCounterMsg	= "ModificationCounter";
	private final static String sf_modificationCounterExt	= "ExtensionsModId";
	private final static String sf_modificationCounterPNum	= "PNumListModificationCounter";
	private final static String sf_requestInterval 			= "REQINTERVAL";
	
	private String 	m_sessionIdentity;
	private String 	m_sessionID;
	private String 	m_mailboxID;
	
	private long 	m_modificationCounterMsg 	= 0;
	private long 	m_modificationCounterExt	= 0;
	private long 	m_modificationCounterPNum	= 0;
	private long    m_request_interval 			= 0;
	
	private MessageResponseResultHandler m_result = null;
	
	/*
	 * 
	 */
	public MessageResponseHandler( XmlParsingObjectAbstract parent ){
		super( parent );
		m_result = new MessageResponseResultHandler( this );
		addChild( m_result.getTagName(), m_result ); //RESULT object
	}
	
	public MessageResponseResultHandler getResult() {
		return m_result;
	}

	public String getSf_tag_name() {
		return sf_tag_name;
	}

	public String getSessionIdentity() {
		return m_sessionIdentity;
	}

	public String getSessionID() {
		return m_sessionID;
	}

	public String getMailboxID() {
		return m_mailboxID;
	}

	public long getModificationCounterMsg() {
		return m_modificationCounterMsg;
	}

	public long getModificationCounterExt() {
		return m_modificationCounterExt;
	}

	public long getModificationCounterPNum() {
		return m_modificationCounterPNum;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.IXmlParsingObject#getTagName()
	 */
	@Override
	public String getTagName() {
		return sf_tag_name;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void  endElement  (String uri, String localName, String qName){
		super.endElement(uri, localName, qName);
	
		String elementName = getElementName( localName, qName );
		
		if( elementName.equalsIgnoreCase(sf_sessionIdentity) ){
			m_sessionIdentity = getValue();
		}
		else if( elementName.equalsIgnoreCase(sf_SessionID) ){
			m_sessionID = getValue();
		}
		else if( elementName.equalsIgnoreCase(sf_MailboxId) ){
			m_mailboxID = getValue();
		}
		else if( elementName.equalsIgnoreCase(sf_requestInterval) ){
			m_request_interval = getLongValue();
		}
		else if( elementName.equalsIgnoreCase(sf_modificationCounterMsg) ){
			m_modificationCounterMsg = getLongValue();
		}
		else if( elementName.equalsIgnoreCase(sf_modificationCounterExt) ){
			m_modificationCounterExt = getLongValue();
		}
		else if( elementName.equalsIgnoreCase(sf_modificationCounterPNum) ){
			m_modificationCounterPNum = getLongValue();
		}
	}
}
