package com.ringcentral.android.api.pojo;

import android.content.ContentValues;
import android.text.format.DateUtils;

public class CallLogPOJO  extends BasePOJO {

	private int callDirection;
	private int callType;
	private String fromName;
	private String fromPhone;
	private float length;
	private String location;
	private long mailboxId;
	private String pin;
	private long recordId;
	private String computedLabel;
	private String computedTimeLabel;
	private String callStartTimeText="";
	public long start;
	private int status;
	private String toName;
	private String toPhone;

	public static enum Status {
	    NEW,
	    KEEP,
	    UPDATE,
	    DELETE,
	    IGNORE
	}
	public Status recordStatus = Status.NEW;
	public long recordDbId = -1;
	public ContentValues cv;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (recordId ^ (recordId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CallLogPOJO other = (CallLogPOJO) obj;
		if (recordId != other.recordId)
			return false;
		return true;
	}

	

	public int getCallDirection() {
		return callDirection;
	}

	public void setCallDirection(int callDirection) {
		this.callDirection = callDirection;
	}

	public int getCallType() {
		return callType;
	}

	public void setCallType(int callType) {
		this.callType = callType;
	}


	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String getFromPhone() {
		return fromPhone;
	}

	public void setFromPhone(String fromPhone) {
		this.fromPhone = fromPhone;
	}

	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}
	

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public long getMailboxId() {
		return mailboxId;
	}

	public void setMailboxId(long mailboxId) {
		this.mailboxId = mailboxId;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}


	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}

	public String getToName() {
		return toName;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	public String getToPhone() {
		return toPhone;
	}

	public void setToPhone(String toPhone) {
		this.toPhone = toPhone;
	}
	
	public String getComputedLabel() {
		return computedLabel;
	}

	public void setComputedLabel(String computedLabel) {
		this.computedLabel = computedLabel;
	}
	
	public String getComputedTimeLabel() {
		return computedTimeLabel;
	}

	public void setComputedTimeLabel(String computedTimeLabel) {
		this.computedTimeLabel = computedTimeLabel;
	}
	

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}
	
	public String getCallStartTimeText() {
		if(callStartTimeText==""){
			//if (getStart() != null) {
				// Set the date/time field by mixing relative and absolute times.
	            int flags = DateUtils.FORMAT_ABBREV_RELATIVE;
	            long time = getStart();
	           
	            callStartTimeText = DateUtils.getRelativeTimeSpanString(time,
	                    System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS, flags).toString();
			//}
		}
		return callStartTimeText;
	}
	
	
	
	// Values for CallType from RC_Mob_CallLog_Specification.pdf
	public final static int CALLTYPE_UNKNOWN 		= 159;
	public final static int CALLTYPE_PHONECALL 		= 160;
	public final static int CALLTYPE_PHONELOGIN 	= 161;
	public final static int CALLTYPE_FAXRECEIVE		= 162;
	public final static int CALLTYPE_ACCEPTCALL		= 163;
	public final static int CALLTYPE_FINDME 		= 164;
	public final static int CALLTYPE_FOLLOWME 		= 165;
	public final static int CALLTYPE_OUTGOINGFAX 	= 166;
	public final static int CALLTYPE_CALLRETURN 	= 167;
	public final static int CALLTYPE_CALLINGCARD 	= 168;
	public final static int CALLTYPE_RINGDIRECTLY 	= 169;
	public final static int CALLTYPE_RINGOUTWEB 	= 170;
	public final static int CALLTYPE_SIPCALL 		= 171;
	public final static int CALLTYPE_RINGOUTPC 		= 172;
	public final static int CALLTYPE_RINGME 		= 173;
	public final static int CALLTYPE_TRANSFERCALL 	= 174;
	public final static int CALLTYPE_RINGOUTMOBILE	= 265;
	
	// Values for Status
	public final static int CALLSTATUS_UNKNOWN					= 175;
	public final static int CALLSTATUS_INPROGRESS				= 176;
	public final static int CALLSTATUS_MISSED					= 177;
	public final static int CALLSTATUS_ACCEPT					= 178;
	public final static int CALLSTATUS_VOICEMAIL				= 179;
	public final static int CALLSTATUS_REJECT					= 180;
	public final static int CALLSTATUS_REPLY					= 182;
	public final static int CALLSTATUS_FAX_RECEIVE				= 183;
	public final static int CALLSTATUS_FAX_RECEIVE_ERROR		= 184;
	public final static int CALLSTATUS_FAX_ONDEMAND				= 185;
	public final static int CALLSTATUS_FAX_PARTIAL_RECEIVE		= 186;
	public final static int CALLSTATUS_BLOCKED					= 187;
	public final static int CALLSTATUS_CONNECTED				= 188;
	public final static int CALLSTATUS_NOANSWER					= 189;
	public final static int CALLSTATUS_INTERNATIONAL_DISABLED	= 190;
	public final static int CALLSTATUS_BUSY						= 191;
	public final static int CALLSTATUS_FAX_SEND_ERROR			= 192;
	public final static int CALLSTATUS_FAX_SENT					= 193;
	public final static int CALLSTATUS_NO_FAX_MACHINE			= 194;
	public final static int CALLSTATUS_SUSPENDED_ACOUNT			= 203;
	public final static int CALLSTATUS_CALL_FAILED				= 204;
	public final static int CALLSTATUS_CALL_FAILURE				= 205;
	public final static int CALLSTATUS_INTERNAL_ERROR			= 206;
	public final static int CALLSTATUS_IP_PHONE_OFFLINE			= 207;
	public final static int CALLSTATUS_NO_CALLING_CREDIT		= 208;
	public final static int CALLSTATUS_RESTRICTED_NUMBER		= 209;
	public final static int CALLSTATUS_WRONG_NUMBER				= 210;
	public final static int CALLSTATUS_ANSWERED_NOT_ACCEPTED	= 211;
	public final static int CALLSTATUS_FORCE_STOPPED			= 237;
	public final static int CALLSTATUS_ABANDONED				= 252;
	public final static int CALLSTATUS_DECLINED					= 253;
	
	// Direction
	public final static String DIRECTION_INCOMING 	= "Incoming";
	public final static String DIRECTION_OUTGOING	= "Outgoing";
	
	public final static int DIRECTION_INCOMING_INT 	= 1;
	public final static int DIRECTION_OUTGOING_INT	= 0;
}
