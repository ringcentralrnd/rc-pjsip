package com.ringcentral.android.api;

public class RequestBody {

    protected static final String PLACEHOLDER_PATTERN = "{%s}";
    protected String body;

    public RequestBody(String bodyTemplate) {
        this.body = bodyTemplate;
    }

    public String getBody() {
        return this.body;
    }

    public RequestBody setProperty(String name, Object value) {
        body = body.replace(String.format(PLACEHOLDER_PATTERN, name), value.toString());
        return this;
    }
    
	public String toLogString() {
		return body;
	}
}

