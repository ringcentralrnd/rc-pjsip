package com.ringcentral.android.api.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.ringcentral.android.api.pojo.LongPOJO;

/**
 * Session response handler
 * @author user
 *
 */
public class LongPOJOHandler extends Handler<LongPOJO> {

	private static final String RESULT = "result";

	public LongPOJOHandler() {
		super();
		LongPOJO longPOJO = new LongPOJO();
		setPOJO(longPOJO);
	}

	public void startElement(String uri, String name, String qName,
			Attributes atts) {
		super.startElement(uri, name, qName, atts);
	}

	public void endElement(String uri, String name, String qName)
			throws SAXException {
		super.endElement(uri, name, qName);
		if (getElementName(name, qName).equals(RESULT)) {
			getPOJO().setValue(Long.parseLong(getBuffer().toString()));
		}
	}

	public void characters(char chars[], int start, int length) {
		getBuffer().append(chars, start, length);
	}

	@Override
	public Object getData() {
		return getPOJO();
	}

}
