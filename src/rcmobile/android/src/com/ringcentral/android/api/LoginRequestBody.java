package com.ringcentral.android.api;

public class LoginRequestBody extends RequestBody {
	private String bodyToLog;

	public LoginRequestBody(String bodyTemplate) {
		super(bodyTemplate);
		bodyToLog = bodyTemplate;
	}

	@Override
	public RequestBody setProperty(String name, Object value) {
		body = body.replace(String.format(PLACEHOLDER_PATTERN, name),value.toString());
		if (name.equals("password")) {
			bodyToLog = bodyToLog.replace(String.format(PLACEHOLDER_PATTERN, name), "***");
		} else {
			bodyToLog = bodyToLog.replace(String.format(PLACEHOLDER_PATTERN, name), value.toString());
		}
		return this;
	}

	@Override
	public String toLogString() {
		return bodyToLog;
	}
}