/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ringcentral.android.RCMConfig;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.api.RequestInfo.AUTH_TYPE;
import com.ringcentral.android.api.parsers.AccountInfoPOJOHandler;
import com.ringcentral.android.api.parsers.ApiVersionPOJOHandler;
import com.ringcentral.android.api.parsers.BaseHandler;
import com.ringcentral.android.api.parsers.CallLogsHandler;
import com.ringcentral.android.api.parsers.CallerIdPOJOListHandler;
import com.ringcentral.android.api.parsers.ExtensionsPOJOListHandler;
import com.ringcentral.android.api.parsers.LoginPOJOHandler;
import com.ringcentral.android.api.parsers.SessionStatusPOJOHandler;

public class RequestInfoStorage {

    public static final int LOGIN = 10;
    public static final int RELOGIN_INTERNALLY = 11;
    public static final int LOGIN_CHECK = 12;
    public static final int GET_API_VERSION = 20;
    //public static final int GET_USER_INFO = 25;
    public static final int GET_ACCOUNT_INFO = 30;
    public static final int GET_ACCOUNT_INFO_CHECK_COUNTERS = 31;
    public static final int GET_ACCOUNT_INFO_CHECK_EXT_COUNTER = 32;
    public static final int GET_ACCOUNT_INFO_CHECK_MSG_COUNTER = 33;
    public static final int SET_DND_STATUS = 40;
    public static final int SET_EXTENDED_DND_STATUS = 45;
    public static final int GET_CALLER_IDS = 50;
    //public static final int GET_MESSAGES = 60;
    //public static final int GET_MESSAGE = 70;
    public static final int LIST_EXTENSIONS = 90;
    //public static final int SET_FORWARDING_NUMBERS = 100;
    public static final int RINGOUT_CALL = 110;
    public static final int CALL_STATUS = 120;
    public static final int RINGOUT_CANCEL = 130;
    public static final int GET_ALL_CALL_LOGS = 140;
    public static final int GET_MISSED_CALL_LOGS = 150;
    public static final int DIRECT_RINGOUT = 160;
    public static final int SET_SETUP_WIZARD_STATE = 170;

    private static List<RequestInfo> list = new ArrayList<RequestInfo>();

    private static HashMap<Integer, String> urls = new HashMap<Integer, String>() {

        private static final long serialVersionUID = 1L;

        {
            put(LOGIN, "Authenticator");
            put(RELOGIN_INTERNALLY, "Authenticator");
            put(LOGIN_CHECK, "Authenticator");
            put(GET_API_VERSION, "Authenticator");
//            put(GET_USER_INFO, "Account");
            put(GET_ACCOUNT_INFO, "iPhone");
            put(GET_ACCOUNT_INFO_CHECK_COUNTERS, "iPhone");
            put(GET_ACCOUNT_INFO_CHECK_EXT_COUNTER, "iPhone");
            put(GET_ACCOUNT_INFO_CHECK_MSG_COUNTER, "iPhone");
            put(SET_DND_STATUS, "iPhone");
            put(SET_EXTENDED_DND_STATUS, "iPhone");
            put(GET_CALLER_IDS, "Account");
//            put(GET_MESSAGES, "Messages");
//            put(GET_MESSAGE, "Messages");
            put(LIST_EXTENSIONS, "iPhone");
//            put(SET_FORWARDING_NUMBERS, "iPhone");
            put(RINGOUT_CALL, "RingOut");
            put(CALL_STATUS, "RingOut");
            put(RINGOUT_CANCEL, "RingOut");
            put(GET_ALL_CALL_LOGS, "iPhone");
            put(GET_MISSED_CALL_LOGS, "iPhone");
            put(DIRECT_RINGOUT, "RingOut");
            put(SET_SETUP_WIZARD_STATE, "Account");
        }
    };

    private static synchronized void initRequestHash(){
        if (list.isEmpty()) {
            addItem("Login", RequestTemplate.LOGIN, LOGIN, LoginPOJOHandler.class, AUTH_TYPE.GET_COOKIE, false, true);
            addItem("Re-Login", RequestTemplate.LOGIN, RELOGIN_INTERNALLY, LoginPOJOHandler.class, AUTH_TYPE.GET_COOKIE, false, true);
            addItem("LoginCheck", RequestTemplate.LOGIN, LOGIN_CHECK, LoginPOJOHandler.class, AUTH_TYPE.GET_COOKIE, false, true);
            addItem("GetAPIVersion", RequestTemplate.GET_API_VERSION, GET_API_VERSION, ApiVersionPOJOHandler.class, AUTH_TYPE.SET_COOKIE, false, true);
//            addItem(RequestTemplate.GET_USER_INFO, GET_USER_INFO, UserInfoPOJOHandler.class, AUTH_TYPE.SET_COOKIE, false, true);
            addItem("GetAccountInfo", RequestTemplate.ACCOUNT_INFO, GET_ACCOUNT_INFO, AccountInfoPOJOHandler.class, AUTH_TYPE.SET_COOKIE, false, true);
            addItem("GetAccountInfoAndCheckCounters", RequestTemplate.ACCOUNT_INFO, GET_ACCOUNT_INFO_CHECK_COUNTERS, AccountInfoPOJOHandler.class, AUTH_TYPE.SET_COOKIE, false, true);
            addItem("GetAccountInfoAndCheckExtCounter", RequestTemplate.ACCOUNT_INFO, GET_ACCOUNT_INFO_CHECK_EXT_COUNTER, AccountInfoPOJOHandler.class, AUTH_TYPE.SET_COOKIE, false, true);
            addItem("GetAccountInfoAndCheckMsgCounter", RequestTemplate.ACCOUNT_INFO, GET_ACCOUNT_INFO_CHECK_MSG_COUNTER, AccountInfoPOJOHandler.class, AUTH_TYPE.SET_COOKIE, false, true);
            addItem("SetDND", RequestTemplate.SET_DND_STATUS, SET_DND_STATUS, BaseHandler.class, AUTH_TYPE.SET_COOKIE, false, true);
            addItem("SetExtendedDND", RequestTemplate.SET_DND_STATUS, SET_EXTENDED_DND_STATUS, BaseHandler.class, AUTH_TYPE.SET_COOKIE, false, true);
            addItem("GetCallerIDs", RequestTemplate.GET_CALLERD_ID, GET_CALLER_IDS, CallerIdPOJOListHandler.class, AUTH_TYPE.SET_COOKIE, false, false);
            addItem("ListExtensions", RequestTemplate.LIST_EXTENSIONS, LIST_EXTENSIONS, ExtensionsPOJOListHandler.class, AUTH_TYPE.SET_COOKIE, false, false);
            addItem("ClassicRingOut", RequestTemplate.RINGOUT_CALL, RINGOUT_CALL, SessionStatusPOJOHandler.class, AUTH_TYPE.SET_COOKIE, false, false);
            addItem("ClassicRingOutStatus", RequestTemplate.CALL_STATUS, CALL_STATUS, SessionStatusPOJOHandler.class, AUTH_TYPE.SET_COOKIE, false, false);
            addItem("ClassicRingOutCancel", RequestTemplate.RINGOUT_CANCEL, RINGOUT_CANCEL, BaseHandler.class, AUTH_TYPE.SET_COOKIE, true, false);
            addItem("GetCallLogAll", RequestTemplate.GET_CALL_LOGS, GET_ALL_CALL_LOGS, CallLogsHandler.class, AUTH_TYPE.SET_COOKIE, false, false);
            addItem("GetCallLogMissed", RequestTemplate.GET_CALL_LOGS, GET_MISSED_CALL_LOGS, CallLogsHandler.class, AUTH_TYPE.SET_COOKIE, false, false);
            addItem("DirectRingOut", RequestTemplate.DIRECT_RINGOUT, DIRECT_RINGOUT, BaseHandler.class, AUTH_TYPE.SET_COOKIE, false, false);
            addItem("SetSetupWizardState", RequestTemplate.SET_SETUP_WIZARD_STATE, SET_SETUP_WIZARD_STATE, BaseHandler.class, AUTH_TYPE.SET_COOKIE, true, false);
        }
    }
    
    public static synchronized void clearRequestHash(){
    	if(!list.isEmpty()){
    		list.clear();
    	}
    }

    @SuppressWarnings("unchecked")
    private static void addItem(final String shortName, final String template, final int type, final Class handler, final AUTH_TYPE authType,
                                final boolean force, final boolean useCache) {

        list.add(new RequestInfo() {
            {
                setTemplate(template);
                setType(type);
                setUrl(RCMConfig.get_JEDI_URL(RingCentralApp.getContextRC()) + urls.get(type));
                setHandler(handler);
                setAuthType(authType);
                setForce(force);
                setUseCache(useCache);
                requestShortName = shortName;
            }

        });
    }

    public static RequestInfo findByType(int type) {
    	initRequestHash();
        for (RequestInfo item : list) {
            if (item.getType() == type) {
                return item;
            }
        }
        return null;
    }


}
