package com.ringcentral.android.api.pojo;

import java.util.ArrayList;
import java.util.List;


public class CallerIdListPOJO extends BasePOJO {

    private List<CallerIdPOJO> callerIdsList = new ArrayList<CallerIdPOJO>();

    public List<CallerIdPOJO> getCallerIdsList() {
        return callerIdsList;
    }

    public void setCallerIdsList(List<CallerIdPOJO> callerIdsList) {
        this.callerIdsList = callerIdsList;
    }
}
