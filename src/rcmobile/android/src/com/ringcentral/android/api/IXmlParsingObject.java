/**
 * 
 */
package com.ringcentral.android.api;

/**
 * @author Denis Kudja
 *
 */
public interface IXmlParsingObject {

	public String getTagName();
}
