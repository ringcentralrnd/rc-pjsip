/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.api.Utils;
import com.ringcentral.android.api.XmlParsingObjectAbstract;
import com.ringcentral.android.api.pojo.MessagePOJO;
import com.ringcentral.android.api.pojo.MessagePOJO.CompType;
import com.ringcentral.android.api.pojo.MessagePOJO.MessageType;
import com.ringcentral.android.messages.MessagesListHandler;

/**
 * @author Denis Kudja
 *
 */
public class MessageHeaderHandler extends XmlParsingObjectAbstract {

	private final static String sf_sf_tag_name		= "MESSAGEHDR";
	
	private final static String sf_msg_ID 			= "MsgID";
	private final static String sf_create_date 		= "CreateDate";
	private final static String sf_from_name 		= "FromName";
	private final static String sf_from_phone 		= "FromPhone";
	private final static String sf_to_name 			= "ToName";
	private final static String sf_to_phone 		= "ToPhone";
	private final static String sf_extension 		= "FileExt";
	private final static String sf_status 			= "Status";
	private final static String sf_msg_type 		= "MsgType";
	private final static String sf_cmpression_type	= "CompType";
	private final static String sf_duration 		= "Duration";
	private final static String sf_body_size		= "BodySize";
	
	
	private MessagePOJO m_msg = null;
	
	public MessageHeaderHandler(MessagesListHandler parent) {
		super(parent);
	}

	@Override
	public String getTagName() {
		return sf_sf_tag_name;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	public void  startElement  (String uri, String localName, String qName, Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		
		String elementName = getElementName( localName, qName );
		if( isItself(elementName)){
			m_msg = new MessagePOJO();
		}
	}	

	public void  endElement  (String uri, String localName, String qName){
		super.endElement(uri, localName, qName);
	
		String elementName = getElementName( localName, qName );
		
		if( elementName.equalsIgnoreCase(sf_msg_ID) ){
			m_msg.setId( getLongValue() );
		}
		else if( elementName.equalsIgnoreCase(sf_create_date) ){
			m_msg.setTimeInMillis(Utils.setCreateDate( getValue() ));
		}
		else if( elementName.equalsIgnoreCase(sf_from_name) ){
			m_msg.setFromName( getValue() );
		}
		else if( elementName.equalsIgnoreCase(sf_from_phone) ){
			m_msg.setFromPhone( getValue() );
		}
		else if( elementName.equalsIgnoreCase(sf_to_name) ){
			m_msg.setToName( getValue() );
		}
		else if( elementName.equalsIgnoreCase(sf_to_phone) ){
			m_msg.setToPhone( getValue() );
		}
		else if( elementName.equalsIgnoreCase(sf_extension) ){
			m_msg.setFileExtension( getValue() );
		}
		else if( elementName.equalsIgnoreCase(sf_status) ){
			int status = getIntValue();
			m_msg.setReadStatus( status == 0 ? MessagePOJO.UNREAD : MessagePOJO.READ );
		}
		else if( elementName.equalsIgnoreCase(sf_duration) ){
			m_msg.setDuration( getIntValue() );
		}
		else if( elementName.equalsIgnoreCase(sf_body_size) ){
			m_msg.setBody_size( getLongValue() );
		}
		else if( elementName.equalsIgnoreCase(sf_msg_type) ){
			final int msg_type = getIntValue();
			MessageType msgType = MessageType.ANY;

			switch (msg_type) {
			case 0:
				msgType = MessageType.ANY;
			case 1:
				msgType = MessageType.VOICE;
				break;
			case 2:
				msgType = MessageType.FAX;
				break;
			case 3:
				msgType = MessageType.GENERIC;
				break;
			case 4:
				msgType = MessageType.CALL;
				break;
			case 5:
				msgType = MessageType.EXT;
				break;
			}
			m_msg.setMessageType(msgType);
		}
		else if( elementName.equalsIgnoreCase(sf_cmpression_type) ){
			final int comp_type = getIntValue();
			CompType compType = CompType.UNDEF;

			switch (comp_type) {
			case 0:
				compType = CompType.UNDEF;
				break;
			case 1:
				compType = CompType.GSM;
				break;
			case 2:
				compType = CompType.WAVE;
				break;
			case 3:
				compType = CompType.AIFF;
				break;
			case 4:
				compType = CompType.GIF;
				break;
			case 5:
				compType = CompType.TIFF;
				break;
			case 6:
				compType = CompType.R0Z;
				break;
			}
			
			m_msg.setCompType( compType );
		}		
		
		if( isItself(elementName)){
			
            if( LogSettings.MARKET && m_msg != null ) {
                MktLog.d("MessageHeaderHandler", "Loaded message id:" + m_msg.getId());
            }
			
			if( m_msg.isValid()){
				((MessagesListHandler)getParent()).addMessage( m_msg );
			}
			
			m_msg = null;
		}
		
	}	
}
