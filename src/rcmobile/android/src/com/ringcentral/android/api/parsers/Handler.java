package com.ringcentral.android.api.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.ringcentral.android.api.pojo.BasePOJO;

/**
 * Basic response handler
 * @author user
 *
 * @param <T>
 */
public abstract class Handler<T extends BasePOJO> extends DefaultHandler {

	private static final String TRUE = "true";
	private static final String  SUCCESS= "success";
	private static final String CODE = "code";
	private static final String MESSAGE = "message";
	
	private T pojo;

	private StringBuffer buf;

	
	public Handler(){
		super();
	}
	
	protected void setPOJO(T pojo){
		this.pojo = pojo;
	}
	
	public T getPOJO() {
		return this.pojo;
	}

	@Override
	public void characters(char chars[], int start, int length) {
		getBuffer().append(chars, start, length);
	}

	@Override
	public void endElement(String uri, String name, String qName) throws SAXException {
		
		String elementName = getElementName(name, qName);
		if (elementName.equals(SUCCESS)) {
			getPOJO().setSuccess(getBuffer().toString().equals(TRUE));
		} else if (elementName.equals(CODE)) {
			getPOJO().setErrorCode(Integer.parseInt(getBuffer().toString()));
		} else if (elementName.equals(MESSAGE)) {
			getPOJO().setMessage(getBuffer().toString());
		}
	}

	@Override
	public void startElement(String uri, String name, String qName,
			Attributes atts) {	
		buf = new StringBuffer();
	}

	
	protected void clearBuffer() {
		buf = new StringBuffer();
	}

	protected StringBuffer getBuffer() {
		return this.buf;
	}
	
	protected String getElementName(String name, String qName) {
		if ("".equals(name)) {
			return qName;
		} else {
			return name;
		}
	}


	public abstract Object getData();
	
	

}

