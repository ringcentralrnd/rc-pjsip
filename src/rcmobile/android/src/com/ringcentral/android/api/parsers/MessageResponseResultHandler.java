/**
 * 
 */
package com.ringcentral.android.api.parsers;

import com.ringcentral.android.api.XmlParsingObjectAbstract;

/**
 * @author Denis Kudja
 *
 */
public class MessageResponseResultHandler extends XmlParsingObjectAbstract {

	private final static String sf_tag_name 				= "RESULT";
	
	private final static String sf_ErrorCode				= "ErrorCode";
	private final static String sf_ErrorName				= "ErrorName"; 
	
	private String 	m_error = new String();
	private int 	m_code;

	public String getError() {
		return m_error;
	}

	public int getCode() {
		return m_code;
	}
	
	public MessageResponseResultHandler( XmlParsingObjectAbstract parent ){
		super( parent );
	}
	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.IXmlParsingObject#getTagName()
	 */
	@Override
	public String getTagName() {
		return sf_tag_name;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void  endElement  (String uri, String localName, String qName){
		super.endElement(uri, localName, qName);
		
		String elementName = getElementName( localName, qName );
		
		if( elementName.equalsIgnoreCase(sf_ErrorName) ){
			m_error = getValue();
		}
		else if( elementName.equalsIgnoreCase(sf_ErrorCode) ){
			m_code = getIntValue();
		}
	}	
}
