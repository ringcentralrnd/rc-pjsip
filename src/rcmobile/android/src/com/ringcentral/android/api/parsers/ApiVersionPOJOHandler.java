/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.ringcentral.android.api.pojo.ApiVersionPOJO;

public class ApiVersionPOJOHandler extends Handler<ApiVersionPOJO> {

    private static final String RETURN = "return";

    public ApiVersionPOJOHandler() {
        super();
        setPOJO(new ApiVersionPOJO());
    }


    @Override
    public void startElement(String uri, String name, String qName, Attributes atts) {
        super.startElement(uri, name, qName, atts);
    }

    @Override
    public void endElement(String uri, String name, String qName) throws SAXException {
        super.endElement(uri, name, qName);

        if (getElementName(name, qName).equals(RETURN)) {
            getPOJO().setApiVersion(getBuffer().toString());
        }
    }
    
    @Override
    public Object getData() {
        return getPOJO();
    }

}
