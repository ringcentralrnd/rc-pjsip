/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api.parsers;


import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.ringcentral.android.api.pojo.AccountInfoPOJO;
import com.ringcentral.android.api.pojo.ForwardingNumberPOJO;

/**
 * Account info response parser
 */
public class AccountInfoPOJOHandler extends Handler<AccountInfoPOJO> {

	private static final String ACCOUNT_NUMBER = "accountNumber";
	private static final String AGENT = "agent";
	private static final String AGENT_STATUS = "agentStatus";
	private static final String DND = "dnd";
	private static final String DND_STATUS = "dndStatus";
	private static final String FORWARDING_NUMBERS = "forwardingNumbers";
	private static final String FREE = "free";
	private static final String GLOBAL_EXT_MOD_COUNTER = "globalExtModCounter";
	private static final String MSG_MOD_COUNTER = "msgModCounter";
	private static final String PIN = "pin";
	private static final String ACCESS_LEVEL = "accessLevel";
	private static final String EXTENSION_TYPE = "extensionType";
	private static final String TIER_SERVICE_TYPE = "tierServiceType";
	private static final String SYSTEM_EXTENSION = "systemExtension";
	private static final String SERVICE_VERSION = "serviceVersion";
	private static final String TIER_SETTINGS = "tierSettings";
	private static final String USER_ID = "userId";
	private static final String FORWARDING = "forwarding";
	private static final String NAME = "name";
	private static final String NUMBER = "number";
	private static final String ORDER_BY = "orderBy";
	private static final String TYPE = "type";
	private static final String FIRST_NAME = "firstName";
	private static final String LAST_NAME = "lastName";
	private static final String EMAIL = "email";
	private static final String SETUP_WIZARD_STATE = "setupWizardState";
	private static final String SETUP_URL = "expressSetupMobileUrl";
	private static final String TRIAL_EXPIRATION_STATE = "trialExpirationState";
	private static final String DAYS_TO_EXPIRE = "daysToExpire";
	private static final String BRAND_ID = "brandId";

	private ForwardingNumberPOJO forwardingNumberPOJO;

	public AccountInfoPOJOHandler() {
		super();
		AccountInfoPOJO sessionStatusPOJO = new AccountInfoPOJO();
		setPOJO(sessionStatusPOJO);
	}

	public void startElement(String uri, String name, String qName,
			Attributes atts) {
		super.startElement(uri, name, qName, atts);
		if (getElementName(name, qName).equals(FORWARDING_NUMBERS)) {
			forwardingNumberPOJO = new ForwardingNumberPOJO();
		}
	}

	public void endElement(String uri, String name, String qName)
			throws SAXException {
		super.endElement(uri, name, qName);
		if (getElementName(name, qName).equals(ACCOUNT_NUMBER)) {
			getPOJO().setAccountNumber(getBuffer().toString());
        } else if (getElementName(name, qName).equals(AGENT)) {
            getPOJO().setAgent(Boolean.parseBoolean(getBuffer().toString()));
        } else if (getElementName(name, qName).equals(AGENT_STATUS)) {
            getPOJO().setAgentStatus(getBuffer().toString());
		} else if (getElementName(name, qName).equals(DND)) {
			getPOJO().setDnd(Boolean.parseBoolean(getBuffer().toString()));
        } else if (getElementName(name, qName).equals(DND_STATUS)) {
            getPOJO().setDndStatus(getBuffer().toString());
		} else if (getElementName(name, qName).equals(FORWARDING_NUMBERS)) {
			getPOJO().getForwardingNumbers().add(forwardingNumberPOJO);
		} else if (getElementName(name, qName).equals(FREE)) {
			getPOJO().setFree(Boolean.parseBoolean(getBuffer().toString()));
		} else if (getElementName(name, qName).equals(GLOBAL_EXT_MOD_COUNTER)) {
			getPOJO().setGlobalExtModCounter(Long.parseLong(getBuffer().toString()));
		} else if (getElementName(name, qName).equals(MSG_MOD_COUNTER)) {
			getPOJO().setMsgModCounter(Long.parseLong(getBuffer().toString()));
		} else if (getElementName(name, qName).equals(PIN)) {
			getPOJO().setPin(getBuffer().toString());
        } else if (getElementName(name, qName).equals(ACCESS_LEVEL)) {
            getPOJO().setAccessLevel(getBuffer().toString());
        } else if (getElementName(name, qName).equals(EXTENSION_TYPE)) {
            getPOJO().setExtensionType(getBuffer().toString());
        } else if (getElementName(name, qName).equals(TIER_SERVICE_TYPE)) {
            getPOJO().setTierServiceType(getBuffer().toString());
        } else if (getElementName(name, qName).equals(SYSTEM_EXTENSION)){
        	getPOJO().setSystemExtension(Boolean.parseBoolean(getBuffer().toString()));
        } else if (getElementName(name, qName).equals(SERVICE_VERSION)) {
            getPOJO().setServiceVersion(Integer.parseInt(getBuffer().toString()));
		} else if (getElementName(name, qName).equals(TIER_SETTINGS)) {
			getPOJO().setTierSettings(Long.parseLong(getBuffer().toString()));
		} else if (getElementName(name, qName).equals(USER_ID)) {
			getPOJO().setUserId(Long.parseLong(getBuffer().toString()));
		} else if (getElementName(name, qName).equals(FORWARDING)) {
			forwardingNumberPOJO.setForwarding(Boolean.parseBoolean(getBuffer().toString()));
		} else if (getElementName(name, qName).equals(NAME)) {
			forwardingNumberPOJO.setName(getBuffer().toString());
		} else if (getElementName(name, qName).equals(NUMBER)) {
			forwardingNumberPOJO.setNumber(getBuffer().toString());
		} else if (getElementName(name, qName).equals(ORDER_BY)) {
			forwardingNumberPOJO.setOrderBy(Integer.parseInt(getBuffer().toString()));
		} else if (getElementName(name, qName).equals(TYPE)) {
			forwardingNumberPOJO.setType(getBuffer().toString());
		} else if (getElementName(name, qName).equals(FIRST_NAME)) {
		    getPOJO().setFirstName(getBuffer().toString());
        } else if (getElementName(name, qName).equals(LAST_NAME)) {
            getPOJO().setLastName(getBuffer().toString());
        } else if (getElementName(name, qName).equals(EMAIL)) {
            getPOJO().setEmail(getBuffer().toString());
        } else if (getElementName(name, qName).equals(SETUP_WIZARD_STATE)) {
        	getPOJO().setSetupWizardState(getBuffer().toString());
        } else if (getElementName(name, qName).equals(SETUP_URL)){
        	getPOJO().setExpressSetupMobileUrl(getBuffer().toString());
        } else if (getElementName(name, qName).equals(TRIAL_EXPIRATION_STATE)){
        	getPOJO().setTrialExpirationState(getBuffer().toString());
        } else if (getElementName(name, qName).equals(DAYS_TO_EXPIRE)){
        	getPOJO().setDaysToExpire(Integer.parseInt(getBuffer().toString()));
        } else if (getElementName(name, qName).equals(BRAND_ID)){
        	getPOJO().setBrandId(getBuffer().toString());
        }

	}

	@Override
	public Object getData() {
		return getPOJO();
	}



}
