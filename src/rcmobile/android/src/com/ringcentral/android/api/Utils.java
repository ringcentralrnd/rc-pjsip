package com.ringcentral.android.api;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.api.pojo.CallLogPOJO;

public class Utils {
    private static final String rgch = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+*";
    private static final Pattern datePattern = Pattern.compile("^([0-9]+),([0-9]+),([0-9]+),([0-9]+),([0-9]+),");
    private static final TimeZone GMT = TimeZone.getTimeZone("GMT+0");
    private static final ByteBuffer mpwchb;
    private static final int bBad = 0xff;
    private static final String TAG = "[RC] Utils";

    static String encode(ByteBuffer digest) {
        int bufLimit = digest.limit();
        StringBuilder sb = new StringBuilder();
        {
            int indx = 0;
            while (bufLimit >= 3) {
                byte ba = (byte) ((digest.get(indx) >> 0x02) & 0x3F);
                byte bb = (byte) (((digest.get(indx) & 0x03) << 0x4)
                        | ((digest.get(indx + 0x01) >> 0x04) & 0x0F));
                byte bc = (byte) (((digest.get(indx + 0x01) & 0x0F) << 0x02)
                        | ((digest.get(indx + 0x02) >> 0x06) & 0x03));
                byte bd = (byte) ((digest.get(indx + 0x02) & 0x3F));
                sb.append(rgch.charAt(ba));
                sb.append(rgch.charAt(bb));
                sb.append(rgch.charAt(bc));
                sb.append(rgch.charAt(bd));
                indx += 3;
                bufLimit -= 3;
            }
            if (bufLimit == 1) {
                byte ba = (byte) ((digest.get(indx) >> 0x02) & 0x3F);
                byte bb = (byte) (((digest.get(indx) & 0x03) << 0x04) | 0x00);
                sb.append(rgch.charAt(ba));
                sb.append(rgch.charAt(bb));
                sb.append('=');
                sb.append('=');
            } else if (bufLimit == 2) {
                byte ba = (byte) ((digest.get(indx) >> 0x02) & 0x3F);
                byte bb = (byte) (((digest.get(indx) & 0x03) << 0x04) 
                        | ((digest.get(indx + 0x01) >> 0x04) & 0x0F));
                byte bc = (byte) (((digest.get(indx) & 0x0F) << 0x2) 
                        | 0x00);
                sb.append(rgch.charAt(ba));
                sb.append(rgch.charAt(bb));
                sb.append(rgch.charAt(bc));
                sb.append('=');
            }
        }
        return sb.toString();
    }

    static {
        mpwchb = ByteBuffer.allocate(256);
        int i;
        for (i = 0; i < 256; i++) {
            mpwchb.put(i, (byte) bBad);
        }
        for (i = 0; i < 64; i++) {
            mpwchb.put(rgch.charAt(i), (byte) i);
        }
    }

    public static String md5Wchar2(String s) {
        String ret = null;
        try {
            ByteBuffer str = ByteBuffer.wrap(s.getBytes());
            int len = str.limit();
            ByteBuffer buf = ByteBuffer.allocate(len * 2);
            for (int i = 0; i < len; i++) {
                buf.put(str.get());
                buf.put((byte) 0);
            }
            MessageDigest m = MessageDigest.getInstance("MD5");
            buf.rewind();
            m.update(buf);
            ByteBuffer d = ByteBuffer.wrap(m.digest());
            ret = encode(d);
        } catch (NoSuchAlgorithmException e) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "md5Wchar2()", e);
            }
        }

        return ret;
    }

    public static String computeDigitalSignature(String phoneNumber, String sessionID, String password) {
        if (phoneNumber == null || sessionID == null || password == null || phoneNumber.length() == 0
                || sessionID.length() == 0 || password.length() == 0) {
            return "";
        }

        String s = phoneNumber + ":" + getPasswordHash(password) + ":" + sessionID + "l";
        String digitalSignature = Utils.md5Wchar2(s);

        return digitalSignature;
    }

    public static long getPasswordHash(String password) {
        long pswHash = 0;
        if (password == null) {
            return 0;
        }

        int limit = password.length();
        for (int i = 0; i < limit; i++) {
            int c = (int) password.charAt(i) & 0xff;
            pswHash = (pswHash * 33 + c);
        }

        pswHash &= 0xffffffffL;
        return pswHash;
    }

    public static long setCreateDate(String createDate) {
        long timeInMillis = -1;
        Matcher m = datePattern.matcher(createDate);
        if (m.find()) {
            GregorianCalendar calendar = new GregorianCalendar(GMT);
            calendar.clear();
            calendar.set(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)) - 1, Integer.parseInt(m.group(3)),
                    Integer.parseInt(m.group(4)), Integer.parseInt(m.group(5)));
            timeInMillis = calendar.getTimeInMillis();
        }
        return timeInMillis;
    }

    public static void sortLogs(List<CallLogPOJO> logs) {
        Collections.sort(logs, new Comparator<CallLogPOJO>() {

            public int compare(CallLogPOJO firstItem, CallLogPOJO secondItem) {
                if (firstItem.getStart() < secondItem.getStart()) {
                    return 1;
                } else if (firstItem.getStart() > secondItem.getStart()) {
                    return -1;
                }
                return 0;
            }
        });
    }

    public static String buildDate(long date) {
        return toISO8601(new Date(date));
    }

    public static String toISO8601(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String result = sdf.format(date);
        return result;
    }

    // public static void setUpdateInterval(boolean isActive, RingCentralApp
    // rApp) {
    // if (isActive) {
    // Log.d(TAG, "short update interval: " + RCMConfig.SHORT_UPDATE_INTERVAL +
    // "ms");
    // rApp.setUpdateInterval(RCMConfig.SHORT_UPDATE_INTERVAL);
    // } else {
    // Log.d(TAG, "long update interval: " + RCMConfig.LONG_UPDATE_INTERVAL +
    // "ms");
    // rApp.setUpdateInterval(RCMConfig.LONG_UPDATE_INTERVAL);
    // }
    // }
}
