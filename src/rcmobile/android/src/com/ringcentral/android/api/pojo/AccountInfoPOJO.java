/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api.pojo;

import java.util.ArrayList;
import java.util.List;

import com.ringcentral.android.provider.RCMDataStore.AccountInfoTable;


public class AccountInfoPOJO extends BasePOJO {
	private String accountNumber;
	private boolean agent;
	private String agentStatus;
	private boolean dnd = false;
	private String dndStatus;
    private List<ForwardingNumberPOJO> forwardingNumbers = new ArrayList<ForwardingNumberPOJO>();
	private boolean free;
	private long globalExtModCounter;
	private long msgModCounter;
	private String pin;
	private String accessLevel;
	private String extensionType;
	private String tierServiceType;
	private boolean systemExtension;
 	private int serviceVersion = AccountInfoTable.SERVICE_VERSION_4;  //Default value; overwritten by value received from JEDI
 	private long tierSettings;
	private long userId;
	private String firstName;
	private String lastName;
	private String email;
	private String setupWizardState;
	private String expressSetupMobileUrl;
	private String trialExpirationState;
	private int daysToExpire;
	private String brandId;
	
	
    public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

    public boolean isAgent() {
        return agent;
    }
    public void setAgent(boolean agent) {
        this.agent = agent;
    }
    
    public String getAgentStatus() {
        return agentStatus;
    }
    public void setAgentStatus(String agentStatus) {
        this.agentStatus = agentStatus;
    }
    
	public boolean isDnd() {
		return dnd;
	}
	public void setDnd(boolean dnd) {
		this.dnd = dnd;
	}
	
    public String getDndStatus() {
        return dndStatus;
    }
    public void setDndStatus(String dndStatus) {
        this.dndStatus = dndStatus;
    }
    
	public boolean isFree() {
		return free;
	}
	public void setFree(boolean free) {
		this.free = free;
	}

	public long getGlobalExtModCounter() {
		return globalExtModCounter;
	}
	public void setGlobalExtModCounter(long globalExtModCounter) {
		this.globalExtModCounter = globalExtModCounter;
	}

	public long getMsgModCounter() {
		return msgModCounter;
	}
	public void setMsgModCounter(long msgModCounter) {
		this.msgModCounter = msgModCounter;
	}

	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getAccessLevel() {
        return accessLevel;
    }
    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getExtensionType() {
        return extensionType;
    }
    public void setExtensionType(String extensionType) {
        this.extensionType = extensionType;
    }
    
    /**
     * Returns account tier type (Tier Service type, e.g. "RCMobile",
     * "RCOffice", "RCVoice", "RCFax"). Valid from 5.0.x
     * 
     * @return tier service type
     */
    public String getTierServiceType() {
        return tierServiceType;
    }
    
    /**
     * Sets account tier type (Tier Service type, e.g. "RCMobile", "RCOffice",
     * "RCVoice", "RCFax"). Valid from 5.0.x
     * 
     * @param tierServiceType
     *            tier service type
     */
    public void setTierServiceType(String tierServiceType) {
        this.tierServiceType = tierServiceType;
    }
    
    public boolean isSystemExtension() {
        return systemExtension;
    }
    public void setSystemExtension(boolean  systemExtension) {
        this.systemExtension = systemExtension;
    }

    public int getServiceVersion() {
        return serviceVersion;
    }
    public void setServiceVersion(int serviceVersion) {
        this.serviceVersion = serviceVersion;
    }

    public long getTierSettings() {
		return tierSettings;
	}
	public void setTierSettings(long tierSettings) {
		this.tierSettings = tierSettings;
	}

	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}

    public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}		
	
	public String getSetupWizardState() {
		return setupWizardState;
	}
	public void setSetupWizardState(String setupWizardState) {
		this.setupWizardState = setupWizardState;
	}		
		
	public String getExpressSetupMobileUrl() {
		return expressSetupMobileUrl;
	}
	public void setExpressSetupMobileUrl(String expressSetupMobileUrl) {
		this.expressSetupMobileUrl = expressSetupMobileUrl;
	}
	
	public List<ForwardingNumberPOJO> getForwardingNumbers() {
        return forwardingNumbers;
    }
    public void setForwardingNumbers(List<ForwardingNumberPOJO> forwardingNumbers) {
        this.forwardingNumbers = forwardingNumbers;
    }
	
	public String getTrialExpirationState() {
		return trialExpirationState;
	}
	public void setTrialExpirationState(String trialExpirationState) {
		this.trialExpirationState = trialExpirationState;
	}
	
	public int getDaysToExpire() {
		return daysToExpire;
	}
	public void setDaysToExpire(int daysToExpire) {
		this.daysToExpire = daysToExpire;
	}
	
	public String getBrandId() {
		return brandId;
	}
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}
	
	
}

