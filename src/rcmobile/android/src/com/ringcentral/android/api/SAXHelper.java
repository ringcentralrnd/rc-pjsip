/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.SocketException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.api.parsers.Handler;

public class SAXHelper {

    private static final String TAG = "[RC]SAXHelper";

    public static Object parseResponse(InputStream is, Handler handler, String shortRequestName, boolean traceResponse) throws ParserConfigurationException,
            SAXException, IOException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser sp = spf.newSAXParser();
        XMLReader xmlReader = sp.getXMLReader();
        xmlReader.setContentHandler(handler);

        if (traceResponse && LogSettings.MARKET) {
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));

            try {
                for (String line; (line = reader.readLine()) != null;) {
                    builder.append(line).append('\n');
                }
            } finally {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
            
            
            if (shortRequestName == null) {
                shortRequestName = "Unknown";
            }
            
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "< JEDI response(" + shortRequestName + ") " + builder.toString());
            }

            InputSource input = new InputSource(new StringReader(builder.toString()));
            xmlReader.parse(input);
        } else {
            try {
                xmlReader.parse(new InputSource(is));
            } catch (SocketException e) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, e.getMessage(), e);
                }
                throw new SocketException();
            }
        }

        return handler.getData();
    }

}
