package com.ringcentral.android.api.parsers;


import com.ringcentral.android.api.pojo.BasePOJO;


/**
 * Base class for xml response parsers
 * @author user
 *
 */
public   class BaseHandler extends Handler<BasePOJO> {

	public BaseHandler(){
		super();
		setPOJO(new BasePOJO());
	}
	
	
	public  Object getData(){
		return getPOJO();
	}
	
	

}
