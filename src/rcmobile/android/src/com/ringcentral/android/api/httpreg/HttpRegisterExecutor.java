/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api.httpreg;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.api.httpreg.HttpRegister;
import com.rcbase.api.httpreg.IHttpRegisterCallback;
import com.rcbase.parsers.httpreg.HttpRegisterRequest;
import com.rcbase.parsers.httpreg.HttpRegisterResponse;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.LoginScreen;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.provider.RCMProviderHelper;

/**
 * @author Denis Kudja
 *
 */
public class HttpRegisterExecutor extends AsyncTask<Object, Void, Void>
		implements IHttpRegisterCallback {
	
    private static final String TAG = "[RC] HttpRegisterExecutor";
	
	private Context m_context;
	// handler to notificate login thread
    private Handler m_handler;

    protected void sendNotifications(){
        /*
         * Send broadcast notification that 
         * login operation is finished successfully (used for SIP registration)
         */
        if (LogSettings.ENGINEERING) {
        	EngLog.i(TAG, "onFinished: Send notification ACTION_LOGIN_FINISHED");
        }
        Intent loginFinishedIntent = new Intent(RCMConstants.ACTION_LOGIN_FINISHED);
        m_context.sendBroadcast(loginFinishedIntent);
		
        notifyLoginScreen();
    }
    
	/* (non-Javadoc)
	 * @see com.rcbase.api.httpreg.IHttpRegisterCallback#onFinished(com.rcbase.parsers.httpreg.HttpRegisterResponse)
	 */
	@Override
	public void onFinished(HttpRegisterResponse response) {
		
		if( response.isValid() ){
			
			final long sipFlags = response.getBody().getSipFlgs();
			
			if (LogSettings.MARKET) {
	            MktLog.i(TAG, "HTTP-REG:SIP Flags: 0x" + Long.toHexString(sipFlags));
	        }
			RCMProviderHelper.setHttpRegSipFlags(m_context, sipFlags);
			
			final boolean isVoipOnServiceEnabled = (( sipFlags & RCMConstants.SIPFLAGS_VOIP_ENABLED) != 0 );
			
			if (LogSettings.MARKET) {
                MktLog.i(TAG, "HTTP-REG:SIP Flag VoIP Enabled : " + isVoipOnServiceEnabled);
            }
			
			RCMProviderHelper.setVoipEnabled_SipFlagHttpReg( m_context, isVoipOnServiceEnabled );
			
        	String instanceId 	= RCMProviderHelper.getHttpRegInstanceId(m_context);
        	String instanceIdNew = response.getBody().getInstanceId();

			/*
			 * Check and update Instance ID
			 */
        	if( instanceId == null || 
        			instanceId.length() == 0 || 
        			instanceId != instanceIdNew )
        	{
        		RCMProviderHelper.saveHttpRegInstanceId( m_context, instanceIdNew );
        	}
			
		}
		
		sendNotifications();		
	}

	/* (non-Javadoc)
	 * @see com.rcbase.api.httpreg.IHttpRegisterCallback#onFailed(int)
	 */
	@Override
	public void onFailed(int http_error) {
        /*
         * Send broadcast notification that 
         * login operation failed send notification on anyway 
         * SIP Service will check it and if need to stop SIP stack
         */
		sendNotifications();		
	}

	@Override
	protected Void doInBackground(Object... context) {
    	HttpRegister httpReg = new HttpRegister(this, false);
    	HttpRegisterRequest request;

    	m_context = (Context)context[0];

        // if we have 2 objects, second should be handler, to notify LoginScreen that reg completed
        if(context.length > 1) { 
            m_handler = (Handler)context[1];
        }
    	
        /*
         * Do nothing if SIP_SETTINGS_SIP_FLAG_HTTPREG_ENABLED flag is true already
         */
        if( RCMProviderHelper.isVoipEnabled_SipFlagHttpReg( m_context )){
			if (LogSettings.MARKET) {
                MktLog.i(TAG, "HTTP-REG:SIP Flag VoIP Enabled enabled already. Do not call HTTP registration again");
            }
        	
    		sendNotifications();		
    		return null;
        }
        
        if(LogSettings.MARKET) {
            MktLog.i(TAG, "HttpRegisterExecutor Instance ID: notification now will be sent");
        }
        
        final String instanceId = RCMProviderHelper.getHttpRegInstanceId(m_context);
        
   		request = HttpRegister.makeRequest( HttpRegister.AgentRole.agent_Android );
    	
		request.getTag_2_Bdy().setExt(RCMProviderHelper.getLoginNumber(m_context).trim());
        request.getTag_2_Bdy().setPn(RCMProviderHelper.getLoginExt(m_context).trim());
        request.getTag_2_Bdy().setSP(RCMProviderHelper.getLoginPassword(m_context).trim());
        request.getTag_2_Bdy().setInst(instanceId);
        request.getTag_2_Bdy().setCln(instanceId);
		
		/*
		 * Invalid user info. HTTP registration could not be executed
		 */
		if( request.isValid() ){
			httpReg.register(request);
		}
    	
		return null;
	}

	/**
     * that method notifies login process about completed httpReg
     * * if handler was specified during procedure call
     */
    private void notifyLoginScreen() {
        if(null != m_handler) {
            if(LogSettings.MARKET) {
                MktLog.i(TAG, "notifyLoginScreen: notification now will be sent");
            }
            Message msg = m_handler.obtainMessage();
            msg.what = LoginScreen.MESSAGE_HTTP_REG_COMPLETED;
            m_handler.sendMessage(msg);
        } else {
            if(LogSettings.MARKET) {
                MktLog.w(TAG, "notificate no need to notify");
            }
        }
    }

}
