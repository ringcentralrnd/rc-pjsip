package com.ringcentral.android.api.network;

import android.os.Bundle;

public interface NetworkManagerNotifier {

    abstract void statusChanged(Bundle status, Object result);
    abstract NetworkManagerNotifier getNotifier();
}
