package com.ringcentral.android.api;

import com.ringcentral.android.api.parsers.Handler;

public class RequestInfo {

    public enum AUTH_TYPE {
        NONE, GET_COOKIE, SET_COOKIE
    };

    public String requestShortName = "Unknown";
    
    private String url;

    private int type;

    private String template;

    private AUTH_TYPE authType;

    private boolean force;

    private boolean useCache;

    public boolean isUseCache() {
        return useCache;
    }

    public void setUseCache(boolean useCache) {
        this.useCache = useCache;
    }

    public AUTH_TYPE getAuthType() {
        return authType;
    }

    public void setAuthType(AUTH_TYPE authType) {
        this.authType = authType;
    }

    private Class<? extends Handler<?>> handler;

    public Class<? extends Handler<?>> getHandler() {
        return handler;
    }

    public void setHandler(Class<? extends Handler<?>> handler) {
        this.handler = handler;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public boolean isForce() {
        return force;
    }

    public void setForce(boolean force) {
        this.force = force;
    }
}
