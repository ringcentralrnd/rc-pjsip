package com.ringcentral.android.api.pojo;

public class LongPOJO extends BasePOJO {
	
	private long value;
	
	public void setValue(long value){
		this.value = value;	
	}
	
	public long getValue(){
		return this.value;
	}
	

}
