/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.api.pojo.ExtensionPOJO;
import com.ringcentral.android.api.pojo.ExtensionsListPOJO;

/**
 * Extensions list response parser
 * @author user
 *
 */
public class ExtensionsPOJOListHandler extends Handler<ExtensionsListPOJO> {
	
	private static final String EXTENSIONS = "extensions";
	private static final String GLOBAL_EXT_MOD_COUNTER = "globalExtModCounter";

	private static final String TAG = "[RC] ExtensionsPOJOListHandler";
	
	private boolean enableDebug = false;

	private static final String EMAIL =  "email";						// <email>
	private static final String FIRST_NAME = "firstName";				// <firstName>
	private static final String LAST_NAME =  "lastName";				// <lastName>
	private static final String MIDDLE_NAME = "middleName";				// <middleName>
	private static final String MAILBOX_ID = "mailboxId";				// <mailboxId>
	private static final String PIN =  "pin";							// <pin>
	private static final String ADDRESS_LINE1 = "addressLine1";			// <addressLine1/>
	private static final String ADDRESS_LINE2 ="addressLine2";			// <addressLine2/>
	private static final String CITY = "city";							// <city/>
	private static final String STATE =  "state";						// <state/>
	private static final String ZIP_CODE =  "zipCode";					//  <zipCode/>
	private static final String COUNTRY =   "country";					//  <country/>
	
	private ExtensionPOJO extPOJO;
	
	public ExtensionsPOJOListHandler() {
		super();
		setPOJO(new ExtensionsListPOJO());
	}
	
	public void startElement(String uri, String name, String qName, Attributes atts) {
		super.startElement(uri, name, qName, atts);
		
		if(getElementName(name, qName).equals(EXTENSIONS)){
			extPOJO = new ExtensionPOJO();
			getPOJO().getList().add(extPOJO);
		}
	
	}

	public void endElement(String uri, String name, String qName) throws SAXException {
		super.endElement(uri, name, qName);

		if (enableDebug && LogSettings.ENGINEERING) {
			EngLog.i(TAG, "endElement: name=" + name + " qName=" + qName
                    + " | buffer=" + getBuffer().toString());
		}
		
		String elementName = getElementName(name, qName);
		
		if( elementName.equals(GLOBAL_EXT_MOD_COUNTER)){
			long extModCounter = Long.valueOf(getBuffer().toString());
			getPOJO().setExtModCounter(extModCounter);
		}
		
		if (elementName.equals(EMAIL)) {
			extPOJO.setEmail(getBuffer().toString());
		} else if (elementName.equals(FIRST_NAME)) {
			extPOJO.setFirstName(getBuffer().toString());
		} else if (elementName.equals(LAST_NAME)) {
			extPOJO.setLastName(getBuffer().toString());
		} else if (elementName.equals(MAILBOX_ID)) {
			extPOJO.setMailboxId(Long.valueOf(getBuffer().toString()));
		} else if (elementName.equals(PIN)) {
			extPOJO.setPin(getBuffer().toString());
		} else if (elementName.equals(MIDDLE_NAME)) {
			extPOJO.setMiddleName(getBuffer().toString());
		} else if (elementName.equals(ADDRESS_LINE1)) { // address info
			extPOJO.setAddressLine1(getBuffer().toString());
		} else if (elementName.equals(ADDRESS_LINE2)) {
			extPOJO.setAddressLine2(getBuffer().toString());
		} else if (elementName.equals(CITY)) {
			extPOJO.setCity(getBuffer().toString());
		} else if (elementName.equals(STATE)) {
			extPOJO.setState(getBuffer().toString());
		} else if (elementName.equals(ZIP_CODE)) {
			extPOJO.setZipCode(getBuffer().toString());
		} else if (elementName.equals(COUNTRY)) {
			extPOJO.setCountry(getBuffer().toString());
		}
	}

	@Override
	public Object getData() {
		return getPOJO();
	}
	
	
}
