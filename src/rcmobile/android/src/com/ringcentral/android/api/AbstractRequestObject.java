/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api;

public class AbstractRequestObject {

    private RequestInfo requestInfo;

    private RequestBody requestBody;

    public AbstractRequestObject(RequestInfo requestInfo) {
        this.requestInfo = requestInfo;
        if (requestInfo.getType() == RequestInfoStorage.LOGIN
                || requestInfo.getType() == RequestInfoStorage.RELOGIN_INTERNALLY
                || requestInfo.getType() == RequestInfoStorage.LOGIN_CHECK) {
            this.requestBody = new LoginRequestBody(requestInfo.getTemplate());            
        } else{
            this.requestBody = new RequestBody(requestInfo.getTemplate());
        }
    }

    public RequestInfo getRequestInfo() {
        return this.requestInfo;
    }

    public RequestBody getRequestBody() {
        return this.requestBody;
    }
    
    /**
     * Defines if JEDI request shall be traced (MARKET Log)
     */
    public boolean traceRequestSOAP = false;
    
    /**
     * Defines if JEDI response shall be traced (MARKET Log)
     */
    public boolean traceResponseSOAP = false;
}
