/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api.pojo;

public class LoginPOJO extends BasePOJO {

    private long mailboxId;
    private long userId;
    private String ipAddress;
    private long requestId;
    private long startTime;
    private String loginHash;
    
	public long getMailboxId() {
        return mailboxId;
    }

    public void setMailboxId(long mailboxId) {
        this.mailboxId = mailboxId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public long getRequestId() {
        return requestId;
    }

    public void setRequestId(long requestId) {
        this.requestId = requestId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getLoginHash() {
        return loginHash;
    }

    public void setLoginHash(String loginHash) {
        this.loginHash = loginHash;
    }


    @Override
    public String toString() {
        return "LoginPOJO{" +
                "mailboxId=" + mailboxId +
                ", userId=" + userId +
                ", ipAddress='" + ipAddress + '\'' +
                ", requestId=" + requestId +
                ", startTime=" + startTime +
                ", loginHash=" + loginHash +
                '}';
    }
}
