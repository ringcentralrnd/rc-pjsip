package com.ringcentral.android.api.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.ringcentral.android.api.pojo.CallerIdListPOJO;
import com.ringcentral.android.api.pojo.CallerIdPOJO;

/**
 * Caller ids list response handler
 * @author user
 *
 */
public class CallerIdPOJOListHandler extends Handler<CallerIdListPOJO> {
	
	private static final String NUMBER = "number";
	
	private static final String USAGE_TYPE = "usageType";
	
	private CallerIdPOJO callerIdPOJO;
	
	public CallerIdPOJOListHandler() {
		super();
		setPOJO(new CallerIdListPOJO());
	}
	
	public void startElement(String uri, String name, String qName,
			Attributes atts) {
        super.startElement(uri, name, qName, atts);
		if(getElementName(name, qName).equals(NUMBER)){
			callerIdPOJO = new CallerIdPOJO();
			getPOJO().getCallerIdsList().add(callerIdPOJO);
		}
	}

	public void endElement(String uri, String name, String qName)
			throws SAXException {
		super.endElement(uri, name, qName);
		if (getElementName(name, qName).equals(NUMBER)) {
			callerIdPOJO.setNumber(getBuffer().toString());;
		} else if (getElementName(name, qName).equals(USAGE_TYPE)) {
			callerIdPOJO.setUsageType(getBuffer().toString());
		}
	}

	@Override
	public Object getData() {
		return getPOJO();
	}


}
