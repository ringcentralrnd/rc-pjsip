package com.ringcentral.android.api.pojo;

import java.util.ArrayList;
import java.util.List;


public class CallLogsListPOJO extends BasePOJO {

	private int breakFlag;
	
	public int getBreakFlag() {
		return breakFlag;
	}

	public void setBreakFlag(int breakFlag) {
		this.breakFlag = breakFlag;
	}

	private List<CallLogPOJO> logs = new ArrayList<CallLogPOJO>();

	public List<CallLogPOJO> getLogs() {
		return logs;
	}
	
	public void setLogs(List<CallLogPOJO> logs) {
		this.logs = logs;
	}
	
	public CallLogPOJO getById(int id){
		for(CallLogPOJO item: logs){
			if(item.getMailboxId()==id){
				return item;
			}
		}
		return null;
	}
	
	
}
