/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api.network;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConfig;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.api.AbstractRequestObject;
import com.ringcentral.android.api.RequestInfo;
import com.ringcentral.android.api.RequestInfoStorage;
import com.ringcentral.android.api.Utils;
import com.ringcentral.android.api.pojo.AccountInfoPOJO;
import com.ringcentral.android.api.pojo.ApiVersionPOJO;
import com.ringcentral.android.api.pojo.BasePOJO;
import com.ringcentral.android.api.pojo.CallLogPOJO;
import com.ringcentral.android.api.pojo.CallerIdListPOJO;
import com.ringcentral.android.api.pojo.CallerIdPOJO;
import com.ringcentral.android.api.pojo.ExtensionPOJO;
import com.ringcentral.android.api.pojo.ExtensionsListPOJO;
import com.ringcentral.android.api.pojo.ForwardingNumberPOJO;
import com.ringcentral.android.api.pojo.LoginPOJO;
import com.ringcentral.android.api.pojo.SessionStatusPOJO;
import com.ringcentral.android.calllog.CallLogSync;
import com.ringcentral.android.contacts.ContactPhoneNumber;
import com.ringcentral.android.messages.MessagesHandler;
import com.ringcentral.android.phoneparser.PhoneNumber;
import com.ringcentral.android.phoneparser.PhoneNumberFormatter;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.provider.RCMDataStore.AccountInfoTable;
import com.ringcentral.android.provider.RCMDataStore.ExtensionsTable;
import com.ringcentral.android.ringout.CallStatusStrings;
import com.ringcentral.android.utils.DeviceUtils;
import com.ringcentral.android.utils.DndManager;
import com.ringcentral.android.utils.NetworkUtils;
import com.ringcentral.android.utils.PhoneUtils;
import com.ringcentral.android.utils.UserInfo;

/**
 * Class encapsulate application network activity and try to make all connections safe.
 * Options:
 * - Singleton pattern via private constructor, ThreadSafe also;
 * - Server error handling and store;
 * - Requests statuses stored at DB for restricted access;
 * - Encapsulate direct access to http connections and inputstream from them;
 */

public class NetworkManager {

    public static final String TAG = "[RC]NetworkManager";
    public static final boolean DEBUG = false;

    /**
     * Error codes for JEDI and HTTP errors.
     */
    public interface ERROR_CODES {
        public static final int NOT_SET = 0;
        public static final int PARTNER_NOT_LOGGED_IN = 101;
        public static final int MAILBOX_NOT_LOGGED_IN = 102;
        public static final int MAILBOX_USERNAME_OR_PASSWORD_INCORRECT = 702;
        public static final int JEDI_ERROR_INACTIVE_USER = 712;
        public static final int CALL_SESSION_ID_IS_NULL = 902;
        public static final int CALLER_ID_IS_NOT_ALLOWED = 907;
        public static final int NETWORK_NOT_AVAILABLE = -1;
        public static final int NETWORK_NOT_AVAILABLE_AIRPLANE_ON = -2;
    }

    /**
     * Fields for request status that sends to the caller
     */
    public interface REQUEST_INFO {
        public static final String TYPE = "object_type";
        public static final String STATUS = "object_status";
        public static final String ERROR_CODE = "error_code";
        public static final String MESSAGE_SYSTEM = "message_system";
        public static final String MESSAGE_USER = "message_user";
        public static final String SERVER_TYPE = "server_type";
    }

    /**
     * Fields for detect server type that generate error or answer
     */
    public interface SERVER_TYPE {
        public static final int NOT_SET = 0;
        public static final int HTTP = 1;
        public static final int JEDI = 2;
    }

    /**
     * Constants for requests interval
     */
    private static final long CALLLOG_REFRESH_TIMEOUT = 3 * 60 * 1000; // 3 minutes
    private static final long EXTENSIONS_LIST_REFRESH_TIMEOUT = 5 * 60 * 1000; // 5 minutes;
    private static final long MESSAGES_REFRESH_TIMEOUT = 5 * 60 * 1000; // 5 minutes
    
    private static long sCalllogLastRefresh = 0;
    private static long sExtensionsListLastRefresh = 0;
    private static long sMessagesLastRefresh = 0;

    /**
     * Constants for requested objects
     */
    private static final String LOGIN = "login";
    private static final String EXTENSION = "extension";
    private static final String PASSWORD = "password";
    private static final String SESSION_ID = "sessionId";
    private static final String MAILBOX_ID = "mailboxId";
    private static final String BLOCKLIST = "blocklist";
    private static final String FROM_TYPE = "fromType";
    private static final String FROM = "from";
    private static final String CLID = "clid";
    private static final String TYPE = "type";
    private static final String TO_TYPE = "toType";
    private static final String TO = "to";
    private static final String PROMPT = "prompt";
    private static final String DEVICE_CALLER_ID = "deviceCallerID";
    private static final String CALLED_NUMBER = "calledNumber";
    private static final String S_CALLER_ID = "SCallerID";
    private static final String DND = "dnd";
    private static final String DND_STATUS = "dndStatus";
    private static final String USER_ID = "userId";
    private static final String START_TIME = "startTime";
    private static final String REQUEST_ID = "requestId";
    private static final String IP_FOURTH = "ip.fourth";
    private static final String IP_SECOND = "ip.second";
    private static final String IP_THIRD = "ip.third";
    private static final String IP_FIRST = "ip.first";
    private static final String DOT_PATTERN = "\\.";
    private static final String ALL = "All";
    private static final String MISSED = "MISSED";
    private static final String REC_COUNT = "recCount";
    private static final String PERIOD_VALUE = "30";
    private static final String PERIOD = "period";
    private static final String DIRECTION = "direction";
    private static final String CACHE_START = "cacheStart";
    private static final String CACHE_END = "cacheEnd";
    private static final String CALL_TYPE = "callType";
    private static final String COOKIE = "signup.cookie";
    private static final String SETUP_WIZARD_STATE = "setupWizardState";

    private String mCookie = "";
    private boolean mLoopCallStatusCheck = false;
    private long mLastSessionId;
    private boolean mCurrentDNDStatus;
    private String mCurrentExtendedDNDStatus;

    private static NetworkManager sInstance;

    private NetworkManager() {
    }

    public static synchronized NetworkManager getInstance() {
        if (sInstance == null) {
            sInstance = new NetworkManager();
        }
        return sInstance;
    }

    /**
     * Request login procedure to server
     *
     * @param context
     * @param login
     * @param ext
     * @param password
     * @return request status as int
     */
    public int login(Context context, String login, String ext, String password, boolean backgroundTask) {
        return login(context, login, ext, password, RequestInfoStorage.LOGIN, backgroundTask);
    }

    public int loginCheck(Context context, String login, String ext, String password, boolean backgroundTask) {
        return login(context, login, ext, password, RequestInfoStorage.LOGIN_CHECK, backgroundTask);
    }

    private int login(Context context, String login, String ext, String password, int loginType, boolean backgroundTask) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "login(login=" + login + ", ext=" + ext + ", pwd=  , loginType=" + loginType + ", bckgrnd=" + backgroundTask + ")...");
        }
        
        AbstractRequestObject requestObject = getRequestObject(context, loginType, false);
        
        if (LogSettings.MARKET) {
            requestObject.traceRequestSOAP = true;
            requestObject.traceResponseSOAP = true;
        }
        
        requestObject.getRequestBody().setProperty(LOGIN, login).setProperty(PASSWORD, password).setProperty(EXTENSION, ext);
        // INFO : check that application don't try to RELOGIN and if it try - send new request
//        if ((!RCMProviderHelper.getLoginNumber(context).equals("") && !RCMProviderHelper.getLoginPassword(context).equals(""))
//                && (!login.equals(RCMProviderHelper.getLoginNumber(context))
//                || !ext.equals(RCMProviderHelper.getLoginExt(context))
//                || !password.equals(RCMProviderHelper.getLoginPassword(context)))) {
//            if (LogSettings.ENGINEERING) {
//                Log.w(TAG, "Try to relogin or login credentials changed.");
//            }
        setRequestStatus(context, requestObject, null, RCMDataStore.SyncStatusTable.SYNC_STATUS_NOT_LOADED);
        saveCredentials(context, login, ext, password);
//        }
        if (backgroundTask) {
            new BackgroundTask().execute(context, requestObject);
            return RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED;
        } else {
            return send(context, requestObject);
        }
    }


    public void reclaimLogin(Context context, String login, String ext, String password) {
        saveCredentials(context, login, ext, password);
        RCMProviderHelper.setRequestStatus(context,
                RCMDataStore.SyncStatusTable.LOGIN_STATUS,
                RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED); 
    }


    public void getApiVersion(Context context, boolean backgroundTask) {
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "getApiVersion(bckgrnd=" + backgroundTask + ")...");
        }

        AbstractRequestObject requestObject = getRequestObject(context, RequestInfoStorage.GET_API_VERSION, true);
        setRequestStatus(context, requestObject, null, RCMDataStore.SyncStatusTable.SYNC_STATUS_NOT_LOADED);

        if (LogSettings.MARKET) {
            requestObject.traceRequestSOAP = true;
            requestObject.traceResponseSOAP = true;
        }
        if (backgroundTask) {
            new BackgroundTask().execute(context, requestObject);
        } else {
            send(context, requestObject);
        }
    }

    
    /**
     * Request ringout call status from server (async supported).
     * If loop param is true then ringout status cheking in loop (every on second) till server said that request inProgress then loop will be canceled.
     *
     * @param context
     * @param sessionId
     * @param loop           - boolean, should we retrieve status at loop or only once
     * @param backgroundTask - boolean, async param
     */
    public void getRingOutCallStatus(Context context, long sessionId, Boolean loop, boolean backgroundTask) {
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "getRingOutCallStatus(sessionId=" + sessionId + ", loop=" + loop + ", bckgrnd=" + backgroundTask + ")...");
        }
        
        AbstractRequestObject requestObject = getRequestObject(context, RequestInfoStorage.CALL_STATUS, true);
        requestObject.getRequestBody().setProperty(SESSION_ID, sessionId);

        if (LogSettings.MARKET) {
            requestObject.traceRequestSOAP = true;
            requestObject.traceResponseSOAP = true;
        }
        
        // TODO : move to properties
        setRequestStatus(context, requestObject, null, RCMDataStore.SyncStatusTable.SYNC_STATUS_NOT_LOADED);

        if (LogSettings.MARKET) {
            MktLog.d(TAG, "getRingOutCallStatus(): sending request to server: status:\n"
                    + "\t\t sessionId: " + sessionId);
        }
        
        if (backgroundTask) {
            new BackgroundTask().execute(context, requestObject, loop);
        } else {
            send(context, requestObject);
        }
    }

    /**
     * Classing ringout procedure (async supported via backgroundTask param)
     *
     * @param context
     * @param blocklist
     * @param clid
     * @param from
     * @param fromType
     * @param prompt
     * @param to
     * @param toType
     * @param type
     * @param backgroundTask - async
     */
    public void callRingOut(Context context,
                            String blocklist, String clid,
                            String from, String fromType,
                            boolean prompt,
                            String to, String toType,
                            String type, boolean backgroundTask) {
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "callRingOut()...");
        }
        
        AbstractRequestObject requestObject = getRequestObject(context, RequestInfoStorage.RINGOUT_CALL, true);

        if (LogSettings.MARKET) {
            requestObject.traceRequestSOAP = true;
            requestObject.traceResponseSOAP = true;
        }
        
        requestObject.getRequestBody().
                setProperty(MAILBOX_ID, RCMProviderHelper.getCurrentMailboxId(context)).
                setProperty(BLOCKLIST, blocklist).
                setProperty(CLID, clid).
                setProperty(FROM, from).
                setProperty(FROM_TYPE, fromType).
                setProperty(PROMPT, prompt).
                setProperty(TO, to).
                setProperty(TO_TYPE, toType).
                setProperty(TYPE, type);

        // TODO : move to properties
        setRequestStatus(context, requestObject, null, RCMDataStore.SyncStatusTable.SYNC_STATUS_NOT_LOADED);
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "callRingOut(): sending request to server: callAndGetFirstStatus:\n"
                    + "\t\t blocklist: " + blocklist + "\n"
                    + "\t\t clid: " + clid + "\n"
                    + "\t\t from: " + from + "\n"
                    + "\t\t fromType: " + fromType + "\n"
                    + "\t\t mailboxId: " + RCMProviderHelper.getCurrentMailboxId(context) + "\n"
                    + "\t\t prompt: " + prompt + "\n"
                    + "\t\t to: " + to + "\n"
                    + "\t\t toType: " + toType + "\n"
                    + "\t\t type: " + type);
        }
        
        if (backgroundTask) {
            new BackgroundTask().execute(context, requestObject);
        } else {
            send(context, requestObject);
        }
    }

    /**
     * One leg ringout procedure (async supported).
     *
     * @param context
     * @param calledNumber
     * @param deviceCallerId
     * @param fromType
     * @param SCallerID
     * @param toType
     * @param backgroundTask - async
     */
    public void callDirectRingOut(Context context,
                                  String calledNumber, String deviceCallerId,
                                  String fromType, String SCallerID, String toType,
                                  boolean backgroundTask) {
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "callDirectRingOut()...");
        }

        AbstractRequestObject requestObject = getRequestObject(context, RequestInfoStorage.DIRECT_RINGOUT, true);
        
        if (LogSettings.MARKET) {
            requestObject.traceRequestSOAP = true;
            requestObject.traceResponseSOAP = true;
        }
        
        // UK device number - PP-2112
        PhoneNumber pn = PhoneUtils.getParser().parse(DeviceUtils.getDeviceNumber(context), new PhoneNumber());
        if (!pn.isUSAOrCanada) {
            if (LogSettings.MARKET) {
                EngLog.d(TAG, "deviceNumber not in USAOrCanada, try to parse if UK");
            }
            ContactPhoneNumber parsedViaCurrentAccount = PhoneUtils.getContactPhoneNumber(pn.original);
            if (parsedViaCurrentAccount.phoneNumber.countryCode.compareTo("44") == 0) {
                deviceCallerId = PhoneNumberFormatter.trimNonDigitSymbols(parsedViaCurrentAccount.phoneNumber.localCanonical);
            } else {
                deviceCallerId = pn.original;
            }
        } else {
            deviceCallerId = pn.numRaw;
        }

        if (LogSettings.MARKET) {
            EngLog.d(TAG, "formatted deviceCallerId = " + deviceCallerId);
        }

        requestObject.getRequestBody().
                setProperty(CALLED_NUMBER, calledNumber).
                setProperty(DEVICE_CALLER_ID, deviceCallerId).
                setProperty(FROM_TYPE, fromType).
                setProperty(S_CALLER_ID, SCallerID).
                setProperty(TO_TYPE, toType);

        // TODO : move to properties
        setRequestStatus(context, requestObject, null, RCMDataStore.SyncStatusTable.SYNC_STATUS_NOT_LOADED);

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "callDirectRingOut(): sending request to server: startOneLegRingOutSession:\n"
                    + "\t\t calledNumber: " + calledNumber + "\n"
                    + "\t\t deviceCallerId: " + deviceCallerId + "\n"
                    + "\t\t fromType: " + fromType + "\n"
                    + "\t\t SCallerID: " + SCallerID + "\n"
                    + "\t\t toType: " + toType);
        }
        
        if (backgroundTask) {
            new BackgroundTask().execute(context, requestObject);
        } else {
            send(context, requestObject);
        }
    }

    /**
     * Cancel current ringout procedure.
     * If sessionId is -1, NetworkManager try to find current ringout request and get sessionId from it and then cancel procedure.
     *
     * @param context
     * @param sessionId, if equals "-1" - then try to detect sessionId automatically
     */
    public void cancelRingOut(Context context, long sessionId, boolean backgroundTask) {
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "cancelRingOut()...");
        }

        AbstractRequestObject requestObject = getRequestObject(context, RequestInfoStorage.RINGOUT_CANCEL, true);

        if (sessionId == -1) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "cancelRingOut(): sessionId=-1");
            }
            if (mLastSessionId != 0) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "cancelRingOut(): sessionId=mLastSessionId=" + mLastSessionId);
                }
                sessionId = mLastSessionId;
            } else {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "cancelRingOut(): sessionId=..., return");
                }
                return;
            }
        }

        if (LogSettings.MARKET) {
            requestObject.traceRequestSOAP = true;
            requestObject.traceResponseSOAP = true;
        }
        
        requestObject.getRequestBody().setProperty(SESSION_ID, sessionId);

        // TODO : move to properties
        setRequestStatus(context, requestObject, null, RCMDataStore.SyncStatusTable.SYNC_STATUS_NOT_LOADED);
        mLoopCallStatusCheck = false;
        if (backgroundTask) {
            new BackgroundTask().execute(context, requestObject);
        } else {
            send(context, requestObject);
        }
    }

    // TODO : move to AlarmManager. discuss.

    /**
     * Refresh call log (async).
     * 
     * @param context
     */
    public void refreshCallLog(Context context, long mailboxId) {
        if (SystemClock.elapsedRealtime() > sCalllogLastRefresh + CALLLOG_REFRESH_TIMEOUT) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "refreshCallLog initiating...");
            }
            sCalllogLastRefresh = SystemClock.elapsedRealtime();
            getCallLogByType(context,
                    mailboxId,
                    RCMConstants.CALL_LOG_PAGE_SIZE,
                    RCMDataStore.CallLogTable.LOGS_ALL,
                    "Up",
                    true);
            getCallLogByType(context,
                    mailboxId,
                    RCMConstants.CALL_LOG_PAGE_SIZE,
                    RCMDataStore.CallLogTable.LOGS_MISSED,
                    "Up",
                    true);
        } else {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "refreshCallLog request: omitted, timeout has not been expiered");
            }
        }
    }

    public void getCallLogByType(Context context, long mailboxId, int recordCount, int type, String direction, boolean backgroundTask) {
    	if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
            PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.CALLLOG_NET_DOWNLOAD);
    	}
        String cacheStart = "", cacheEnd = "";
        List<CallLogPOJO> logs = RCMProviderHelper.getCallLogs(context, mailboxId, type);
        int logSize = logs.size();
        if (logSize > 0) {
            cacheStart = Utils.buildDate(logs.get(0).getStart());
            if (logSize == 1) {
                cacheEnd = cacheStart;
            } else {
                cacheEnd = Utils.buildDate(logs.get(logSize - 1).getStart());
            }
            if (LogSettings.ENGINEERING) {
                EngLog.d(TAG, "getCallLogByType(...), " + "cacheStart = " + cacheStart + ", " + "cacheEnd = " + cacheEnd
                        + ", " + "logs.size = " + logSize + " direction = " + direction);
            }
        } else {
            if (direction.equalsIgnoreCase("Up")) {
                direction = "Down";
                if (LogSettings.ENGINEERING) {
                    EngLog.d(TAG, "Refresh (empty list):  Up->Down");
                }
            } else {
                if (LogSettings.ENGINEERING) {
                    EngLog.d(TAG, "Refresh (empty list):  Direction: " + direction);
                }
            }
        }

        AbstractRequestObject requestObject = fillLogsParams(context, recordCount, type, direction, cacheStart, cacheEnd);
        
        if (LogSettings.QA) {
            requestObject.traceResponseSOAP = true;
        }
        if (LogSettings.MARKET) {
            requestObject.traceRequestSOAP = true;
        }
        setRequestStatus(context, requestObject, null, RCMDataStore.SyncStatusTable.SYNC_STATUS_NOT_LOADED);
        if (backgroundTask) {
            new BackgroundTask().execute(context, requestObject);
        } else {
            send(context, requestObject);
        }
    }

    /**
     * Refresh extensions list (async).
     *
     * @param context
     */
    public void refreshExtensionsList(Context context) {
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "refreshExtensionsList()...");
        }

        refreshCounters(context, true, false, false);
    }

    public void getExtensions(Context context, boolean backgroundTask) {
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "getExtensions(bckgrnd=" + backgroundTask + ")...");
        }

        if (!UserInfo.companyExtensionsAllowed()) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "getExtensions(): Extensions not allowed for this account/ext; return");
            }
            return;
        }

        AbstractRequestObject requestObject = getRequestObject(context, RequestInfoStorage.LIST_EXTENSIONS, true);
        if (LogSettings.MARKET) {
            requestObject.traceRequestSOAP = true;
        }
        requestObject.getRequestBody().setProperty(MAILBOX_ID, RCMProviderHelper.getCurrentMailboxId(context));
        setRequestStatus(context, requestObject, null, RCMDataStore.SyncStatusTable.SYNC_STATUS_NOT_LOADED);

        if (backgroundTask) {
            new BackgroundTask().execute(context, requestObject);
        } else {
            send(context, requestObject);
        }
    }

    /*
     * For use by DnDManager.
     * Don't use this method directly.
     * Use DnDManager.setDndStatus() instead.
     */
    public void setDndStatus(Context context, boolean dnd, boolean backgroundTask) {
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "setDndStatus(dnd=" + dnd + ", bckgrnd=" + backgroundTask + ")...");
        }

        setDndStatus(context, false, dnd, "", backgroundTask);
    }

    /*
     * For use by DnDManager.
     * Don't use this method directly.
     * Use DnDManager.setExtendedDndStatus() instead.
     */
    public void setExtendedDndStatus(Context context, String extendedDndStatus, boolean backgroundTask) {
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "setExtendedDndStatus(extendedDndStatus = " + extendedDndStatus + ", bckgrnd=" + backgroundTask + ")...");
        }
        
        setDndStatus(context, true, false, extendedDndStatus, backgroundTask);
    }
    
    private void setDndStatus(Context context,
            boolean useExtendedDnd, boolean dnd, String extendedDndStatus, boolean backgroundTask) {

        int request_type = useExtendedDnd ? RequestInfoStorage.SET_EXTENDED_DND_STATUS : RequestInfoStorage.SET_DND_STATUS;
        AbstractRequestObject requestObject = getRequestObject(context, request_type, true);
        requestObject.getRequestBody().setProperty(MAILBOX_ID, RCMProviderHelper.getCurrentMailboxId(context));
        
        if (useExtendedDnd) {
            requestObject.getRequestBody().setProperty(DND, "");
            requestObject.getRequestBody().setProperty(DND_STATUS, extendedDndStatus);
            mCurrentExtendedDNDStatus = extendedDndStatus;
        } else {
            requestObject.getRequestBody().setProperty(DND, dnd);
            requestObject.getRequestBody().setProperty(DND_STATUS, "");
            mCurrentDNDStatus = dnd;
        }
        
        if (LogSettings.MARKET) {
            requestObject.traceRequestSOAP = true;
            requestObject.traceResponseSOAP = true;
        }

        // TODO : move to properties
        setRequestStatus(context, requestObject, null, RCMDataStore.SyncStatusTable.SYNC_STATUS_NOT_LOADED);
        if (backgroundTask) {
            new BackgroundTask().execute(context, requestObject);
        } else {
            send(context, requestObject);
        }
    }

    public int getAccountInfo(Context context, boolean checkExtCounter, boolean checkMsgCounter, boolean backgroundTask) {
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "getAccountInfo(checkExtCounter=" + checkExtCounter + ", checkMsgCounter=" + checkMsgCounter + ", bckgrnd=" + backgroundTask + ")...");
        }

        int request_type;
        if (checkExtCounter && checkMsgCounter) {
            request_type = RequestInfoStorage.GET_ACCOUNT_INFO_CHECK_COUNTERS;
        } else if (checkExtCounter) {
            request_type = RequestInfoStorage.GET_ACCOUNT_INFO_CHECK_EXT_COUNTER; 
        } else if (checkMsgCounter) {
            request_type = RequestInfoStorage.GET_ACCOUNT_INFO_CHECK_MSG_COUNTER; 
        } else {
            request_type = RequestInfoStorage.GET_ACCOUNT_INFO;
        }
        
        AbstractRequestObject requestObject = getRequestObject(context, request_type, true);
        requestObject.getRequestBody().setProperty(MAILBOX_ID, RCMProviderHelper.getCurrentMailboxId(context));
        setRequestStatus(context, requestObject, null, RCMDataStore.SyncStatusTable.SYNC_STATUS_NOT_LOADED);
        if (LogSettings.MARKET) {
            requestObject.traceRequestSOAP = true;
            requestObject.traceResponseSOAP = true;
        }
        if (backgroundTask) {
            new BackgroundTask().execute(context, requestObject);
            return RCMDataStore.SyncStatusEnum.SYNC_STATUS_NOT_LOADED;
        } else {
            return send(context, requestObject);
        }
    }

    public void checkExtCounter(Context context, boolean backgroundTask) {
        if (RCMProviderHelper.getExtModCounter(context) != RCMProviderHelper.getExtModCounterTemp(context)) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "AccountInfo said: " + RCMConstants.ACTION_EXTENSIONS_MOD_COUNTER_CHANGED);
            }
            getExtensions(context, backgroundTask);
        }
    }

    public void checkMsgCounter(Context context, boolean backgroundTask) {
        if (RCMProviderHelper.getMsgModCounter(context) != RCMProviderHelper.getMsgModCounterTemp(context)) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "AccountInfo said: " + RCMConstants.ACTION_MESSAGES_MOD_COUNTER_CHANGED);
            }
            MessagesHandler.updateList(backgroundTask);
        } else {
            //TODO : check this behavior. [DK] check if some bodies are still not loaded
            MessagesHandler.downloadAllMessages(backgroundTask);
        }
    }
    
    public void getCallerIds(Context context, boolean backgroundTask) {
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "getCallerIds(bckgrnd=" + backgroundTask + ")...");
        }

        AbstractRequestObject requestObject = getRequestObject(context, RequestInfoStorage.GET_CALLER_IDS, true);
        if (LogSettings.MARKET) {
            requestObject.traceRequestSOAP = true;
        }
        setRequestStatus(context, requestObject, null, RCMDataStore.SyncStatusTable.SYNC_STATUS_NOT_LOADED);
        if (backgroundTask) {
            new BackgroundTask().execute(context, requestObject);
        } else {
            send(context, requestObject);
        }
    }

    /**
     * Refresh messages list (async).
     *
     * @param context
     */
    public void refreshMessages(Context context, boolean force) {
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "refreshMessages(force=" + force + ")...");
        }

        refreshCounters(context, false, true, force);
    }

    
    
    public void refreshCounters(Context context) {
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "refreshCounters()...");
        }
        refreshCounters(context, true, true, false);
    }
    
    private void refreshCounters(Context context, boolean checkExt, boolean checkMsg, boolean forceMsgCheck) {
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "refreshCounters(checkExt=" + checkExt + ", checkMsg=" + checkMsg + ", forceMsgCheck=" + forceMsgCheck +")...");
        }

        if (!checkExt && !checkMsg) {
            return;
        }
        
        boolean check_msg = false;
        boolean check_ext = false;
        
        long cur_time = SystemClock.elapsedRealtime();
        
        if (checkMsg && (forceMsgCheck || cur_time > sMessagesLastRefresh + MESSAGES_REFRESH_TIMEOUT)) {
            sMessagesLastRefresh = cur_time;
            check_msg = true;
            
            if (LogSettings.MARKET) {
                EngLog.d(TAG, "refreshCounters(): MsgCounter will be checked");
            }
        }
    
        if (checkExt && (cur_time > sExtensionsListLastRefresh + EXTENSIONS_LIST_REFRESH_TIMEOUT)) {
            sExtensionsListLastRefresh = cur_time;
            check_ext = true;

            if (LogSettings.MARKET) {
                EngLog.d(TAG, "refreshCounters(): ExtCounter will be checked");
            }
        }

        if (check_ext || check_msg) {
            getAccountInfo(context, check_ext, check_msg, true);
        } else {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "refreshCounters(). Omitted(timeout)");
            }
        }
    }
    
    

//    public void getMessages(Context context, boolean backgroundTask) {
//        AbstractRequestObject requestObject = getRequestObject(context, RequestInfoStorage.GET_MESSAGES, true);
//        requestObject.getRequestBody().setProperty(MAILBOX_ID, RCMProviderHelper.getCurrentMailboxId(context));
////        setRequestStatus(context, requestObject, null, RCMDataStore.SyncStatusTable.SYNC_STATUS_NOT_LOADED);
//        if (backgroundTask) {
//            new BackgroundTask().execute(context, getRequestObject(context, RequestInfoStorage.GET_MESSAGES, true));
//        } else {
//            send(context, getRequestObject(context, RequestInfoStorage.GET_MESSAGES, true));
//        }
//    }
    
    /**
     * Set setupWizardState on the server to the specified state
     *
     * @param context
     * @param newState
     */
    public void setSetupWizardState(Context context, String newState, boolean backgroundTask) {
        AbstractRequestObject requestObject = getRequestObject(context, RequestInfoStorage.SET_SETUP_WIZARD_STATE, true);
        requestObject.getRequestBody().setProperty(SETUP_WIZARD_STATE, newState);

        // TODO : move to properties
        setRequestStatus(context, requestObject, null, RCMDataStore.SyncStatusTable.SYNC_STATUS_NOT_LOADED);

        if (backgroundTask) {
            new BackgroundTask().execute(context, requestObject);
        } else {
            send(context, requestObject);
        }
    }
    

    /**
     * Temporary method for back compatibility
     * TODO : do we need it?
     *
     * @return AbstractRequestObject formed from DB
     */
    @Deprecated
    public AbstractRequestObject getPOJO(AbstractRequestObject object) {
        return new AbstractRequestObject(new RequestInfo());
    }

    /**
     * send preformed query
     *
     * @param context
     * @param object
     * @return query status as int
     */
    public int sendQuery(Context context, AbstractRequestObject object) {
        return send(context, object);
    }

    
    private int send(Context context, AbstractRequestObject object) {
        return send0(context, object, false);
    }
    
    private synchronized int send0(Context context, AbstractRequestObject object, final boolean secondAttempt) {
        // if already loading - don't load again
        // if loaded - check for EXPIRED and don't load again
        if (LogSettings.ENGINEERING && DEBUG) {
            EngLog.i(TAG, "send() object type: " + object.getRequestInfo().getType()
                    + ", status: " + getRequestStatus(context, object.getRequestInfo().getType())
                    + ", secondAttempt: " + secondAttempt);
        }
        if (getRequestStatus(context, object.getRequestInfo().getType()) == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING) {
            if (LogSettings.MARKET) {
                try {
                    MktLog.i(TAG, "return Status: RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING:" + object.getRequestInfo().requestShortName);
                } catch (java.lang.Throwable th) {
                    
                }
            }
            return RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING;
        } else if (getRequestStatus(context, object.getRequestInfo().getType()) == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED) {
            // TODO : checking for EXPIRED content
            if (LogSettings.MARKET) {
                try {
                    MktLog.i(TAG, "return Status: RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED:" + object.getRequestInfo().requestShortName);
                } catch (java.lang.Throwable th) {
                    
                }
            }
            return RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED;
        } else if (getRequestStatus(context, object.getRequestInfo().getType()) == RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR) {
            if (LogSettings.MARKET) {
                try {
                    MktLog.i(TAG, "return Status: RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR:" + object.getRequestInfo().requestShortName);
                } catch (java.lang.Throwable th) {
                    
                }
            }
            // TODO : checking for ERROR processing, now time - send request
        }

        if (LogSettings.ENGINEERING) {
            object.traceRequestSOAP = true;
            object.traceResponseSOAP = true;
        }

        boolean airoplanMode = NetworkUtils.isAirplaneMode(context);

        if (airoplanMode && LogSettings.MARKET) {
            MktLog.w(TAG, "send(): Airoplan mode");
        }
        if (airoplanMode || NetworkUtils.getNetworkState(context) == NetworkUtils.NetworkState.NONE) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "send(): STATUS_ERROR: Airoplan mode or NetworkState is NONE");
            }
            BasePOJO pojo = new BasePOJO();
            if (airoplanMode) {
                pojo.setErrorCode(ERROR_CODES.NETWORK_NOT_AVAILABLE_AIRPLANE_ON);
                pojo.setMessage(context.getString(R.string.airplane_network_error));
            } else {
                pojo.setErrorCode(ERROR_CODES.NETWORK_NOT_AVAILABLE);
                pojo.setMessage(context.getString(R.string.generic_network_error));
            }
            pojo.setSuccess(false);
            setRequestStatus(context, object, pojo, RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR);
            return RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR;
        }

        // TODO : check sequence
        setRequestStatus(context, object, null, RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING);
        
        if (LogSettings.MARKET) {
            try {
                MktLog.i(TAG, "send(): > JEDI Request(" + object.getRequestInfo().requestShortName + ")");
                if (object.traceRequestSOAP) {
                    MktLog.d(TAG, object.getRequestBody().toLogString());
                }
            } catch (java.lang.Throwable th) {
            }
        }

        Object result = ServerConnector.makeRequest(object);
        if (result == null) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "send(): ServerConnector.makeRequest returned null");
            }
            setRequestStatus(context, object, null, RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR);
            return RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR;
        }

        // INFO : catch HTTP exceptions
        if (result instanceof Exception) {
            // TODO : add to db ERROR_CODE and SERVER_TYPE
            setRequestStatus(context, object, result, RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR);
            return RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR;
        }

        BasePOJO basePOJO = (BasePOJO) result;
        int errorCode = basePOJO.getErrorCode();
        boolean successState = basePOJO.getSuccess();

        if (object.getRequestInfo().getType() == RequestInfoStorage.GET_API_VERSION) {
            //JEDI Authenticator:getAPIVersionResponse does not return <success> and <code> fields,
            //so we set them here to default values (no error) for further error checking 
            
            errorCode = ERROR_CODES.NOT_SET;
            successState = true;
        }
        
        if (LogSettings.MARKET) {
            try {
                MktLog.i(TAG, "send(): < JEDI Response(" + object.getRequestInfo().requestShortName + "): errorCode = " + errorCode
                        + ", msg: " + basePOJO.getMessage() + ", success: " + successState);
            } catch (java.lang.Throwable th) {
            }
        }

        if ((errorCode == ERROR_CODES.MAILBOX_NOT_LOGGED_IN || errorCode == ERROR_CODES.PARTNER_NOT_LOGGED_IN)
                && !secondAttempt
                && object.getRequestInfo().getType() != RequestInfoStorage.LOGIN
                && object.getRequestInfo().getType() != RequestInfoStorage.RELOGIN_INTERNALLY
                && object.getRequestInfo().getType() != RequestInfoStorage.LOGIN_CHECK) {
            // TODO : move credentials to NetworkManager
            //  TODO : process error handling while request executing
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "send(): Attempt re-login");
            }
            if (login(context,
                    RCMProviderHelper.getLoginNumber(context),
                    RCMProviderHelper.getLoginExt(context),
                    RCMProviderHelper.getLoginPassword(context),
                    RequestInfoStorage.RELOGIN_INTERNALLY,
                    false) == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED) {
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "send(): Re-login success");
                }

                if (object.getRequestInfo().getType() != RequestInfoStorage.GET_ACCOUNT_INFO
                        && object.getRequestInfo().getType() != RequestInfoStorage.GET_ACCOUNT_INFO_CHECK_COUNTERS
                        && object.getRequestInfo().getType() != RequestInfoStorage.GET_ACCOUNT_INFO_CHECK_EXT_COUNTER
                        && object.getRequestInfo().getType() != RequestInfoStorage.GET_ACCOUNT_INFO_CHECK_MSG_COUNTER
                        && object.getRequestInfo().getType() != RequestInfoStorage.SET_DND_STATUS
                        && object.getRequestInfo().getType() != RequestInfoStorage.SET_EXTENDED_DND_STATUS) {
                    if (LogSettings.MARKET) {
                        MktLog.i(TAG, "send(): Re-new account info after re-login");
                    }
                    getAccountInfo(context, false, false, true);
                }
                
                setRequestStatus(context, object, null, RCMDataStore.SyncStatusTable.SYNC_STATUS_NOT_LOADED);
                return send0(context, object, true);
            } else {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "send(): Re-login failed");
                }
                setRequestStatus(context, object, result, RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR);
                return RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR;
            }
        }
        
        if (errorCode == ERROR_CODES.CALLER_ID_IS_NOT_ALLOWED || errorCode == ERROR_CODES.CALL_SESSION_ID_IS_NULL) {
            if (LogSettings.MARKET) {
               MktLog.i(TAG, "send(): CallerID not allowed (error code " + errorCode + "). Re-new CallerIDs.");
            }
            getCallerIds(context, true);
        } else if (!successState || errorCode != ERROR_CODES.NOT_SET) { 
            setRequestStatus(context, object, result, RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR);
            return RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR;
        }

        // TODO : add result to db
        // TODO : add check for success DB operations
        storeData(context, object, result);
        setRequestStatus(context, object, result, RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED);
        return RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED;
    }

    private void storeData(Context context, AbstractRequestObject object, Object result) {
        if (LogSettings.ENGINEERING && DEBUG) {
            EngLog.d(NetworkManager.TAG, "storeData() type: " + object.getRequestInfo().getType());
        }
        switch (object.getRequestInfo().getType()) {
            case RequestInfoStorage.LOGIN_CHECK:
                break;      //LOGIN_CHECK doesn't store login data in DB 
            case RequestInfoStorage.LOGIN:
            case RequestInfoStorage.RELOGIN_INTERNALLY:
                storeLogin(context, object, result);
                break;
            case RequestInfoStorage.GET_API_VERSION:
                storeApiVersion(context, object, result);
                break;
            case RequestInfoStorage.GET_ALL_CALL_LOGS:
            case RequestInfoStorage.GET_MISSED_CALL_LOGS:
                CallLogSync.storeCallLog(context, object, result);
                break;
            case RequestInfoStorage.CALL_STATUS:
                break;
            case RequestInfoStorage.RINGOUT_CALL:
                break;
            case RequestInfoStorage.DIRECT_RINGOUT:
                break;
            case RequestInfoStorage.RINGOUT_CANCEL:
                break;
            case RequestInfoStorage.LIST_EXTENSIONS:
                storeExtensionsList(context, object, result);
                break;
            case RequestInfoStorage.SET_DND_STATUS:
                DndManager.getInstance().saveDndStatus(context, mCurrentDNDStatus);
                break;
            case RequestInfoStorage.SET_EXTENDED_DND_STATUS:
                DndManager.getInstance().saveExtendedDndStatus(context, mCurrentExtendedDNDStatus);
                break;
            case RequestInfoStorage.GET_ACCOUNT_INFO:
                storeAccountInfo(context, object, result, false, false);
                break;
            case RequestInfoStorage.GET_ACCOUNT_INFO_CHECK_COUNTERS:
                storeAccountInfo(context, object, result, true, true);
                break;
            case RequestInfoStorage.GET_ACCOUNT_INFO_CHECK_EXT_COUNTER:
                storeAccountInfo(context, object, result, true, false);
                break;
            case RequestInfoStorage.GET_ACCOUNT_INFO_CHECK_MSG_COUNTER:
                storeAccountInfo(context, object, result, false, true);
                break;
            case RequestInfoStorage.GET_CALLER_IDS:
                storeCallerIds(context, object, result);
                break;
            case RequestInfoStorage.SET_SETUP_WIZARD_STATE:            	
                break;
        }
    }

    /**
     * store status of request at database
     *
     * @param context
     * @param object
     * @param status
     */
    private void setRequestStatus(final Context context, AbstractRequestObject object, Object result, int status) {
        if (LogSettings.ENGINEERING && DEBUG) {
            EngLog.w(TAG, "setRequestStatus to object with type: " + object.getRequestInfo().getType() + ", status: " + status);
        }
        // TODO : how to?
        sendNotify(context, object.getRequestInfo().getType(), status, result);
        // TODO refactor SWITCH to dynamic detection
        switch (object.getRequestInfo().getType()) {
            case RequestInfoStorage.LOGIN:
            case RequestInfoStorage.RELOGIN_INTERNALLY:
            case RequestInfoStorage.LOGIN_CHECK:
                RCMProviderHelper.setRequestStatus(context, RCMDataStore.SyncStatusTable.LOGIN_STATUS, status);
                break;
            case RequestInfoStorage.GET_API_VERSION:
                RCMProviderHelper.setRequestStatus(context, RCMDataStore.SyncStatusTable.API_VERSION_STATUS, status);
                break;
            case RequestInfoStorage.GET_ALL_CALL_LOGS:
                RCMProviderHelper.setRequestStatus(context, RCMDataStore.SyncStatusTable.ALL_CALL_LOGS_STATUS, status);
                break;
            case RequestInfoStorage.GET_MISSED_CALL_LOGS:
                RCMProviderHelper.setRequestStatus(context, RCMDataStore.SyncStatusTable.MISSES_CALL_LOGS_STATUS, status);
                break;
            case RequestInfoStorage.CALL_STATUS:
                if (status != RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING
                        && result != null
                        && (result instanceof SessionStatusPOJO)
                        && !((SessionStatusPOJO) result).getCallState().equals("")
                        && !((SessionStatusPOJO) result).getCallState().equals(CallStatusStrings.JEDI_RINGOUT_CALL_STATE_IN_PROGRESS)) {
                    mLoopCallStatusCheck = false;
                } else if (status == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED
                        && result != null
                        && (result instanceof SessionStatusPOJO)
                        && ((SessionStatusPOJO) result).getSessionId() != 0) {
                    mLastSessionId = ((SessionStatusPOJO) result).getSessionId();
                }
                RCMProviderHelper.setRequestStatus(context, RCMDataStore.SyncStatusTable.CALL_STATUS, status);
                break;
            case RequestInfoStorage.RINGOUT_CALL:
                if (status == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED
                        && result != null
                        && (result instanceof SessionStatusPOJO)
                        && ((SessionStatusPOJO) result).getSessionId() != 0) {
                    mLastSessionId = ((SessionStatusPOJO) result).getSessionId();
                }
                RCMProviderHelper.setRequestStatus(context, RCMDataStore.SyncStatusTable.RINGOUT_CALL_STATUS, status);
                break;
            case RequestInfoStorage.DIRECT_RINGOUT:
                RCMProviderHelper.setRequestStatus(context, RCMDataStore.SyncStatusTable.DIRECT_RINGOUT_STATUS, status);
                break;
            case RequestInfoStorage.RINGOUT_CANCEL:
                RCMProviderHelper.setRequestStatus(context, RCMDataStore.SyncStatusTable.RINGOUT_CANCEL_STATUS, status);
                break;
            case RequestInfoStorage.LIST_EXTENSIONS:
                RCMProviderHelper.setRequestStatus(context, RCMDataStore.SyncStatusTable.LIST_EXTENSIONS_STATUS, status);
                break;
            case RequestInfoStorage.SET_DND_STATUS:
            case RequestInfoStorage.SET_EXTENDED_DND_STATUS:
                RCMProviderHelper.setRequestStatus(context, RCMDataStore.SyncStatusTable.DND_STATUS, status);
                break;
            case RequestInfoStorage.GET_ACCOUNT_INFO:
            case RequestInfoStorage.GET_ACCOUNT_INFO_CHECK_COUNTERS:
            case RequestInfoStorage.GET_ACCOUNT_INFO_CHECK_EXT_COUNTER:
            case RequestInfoStorage.GET_ACCOUNT_INFO_CHECK_MSG_COUNTER:
                RCMProviderHelper.setRequestStatus(context, RCMDataStore.SyncStatusTable.ACCOUNT_INFO_STATUS, status);
                break;
            case RequestInfoStorage.GET_CALLER_IDS:
                RCMProviderHelper.setRequestStatus(context, RCMDataStore.SyncStatusTable.CALLER_IDS_STATUS, status);
                break;
            case RequestInfoStorage.SET_SETUP_WIZARD_STATE:
                RCMProviderHelper.setRequestStatus(context, RCMDataStore.SyncStatusTable.SETUP_WIZARD_STATE_STATUS, status);
                break;
            default:
                if (LogSettings.ENGINEERING && DEBUG) {
                    EngLog.w(TAG, "setRequestStatus UNKNOWN object with ID : " + object.getRequestInfo().getType());
                }
        }
    }

    /**
     * returns status of request from database via request type
     *
     * @param context
     * @param requestType
     * @return int as status
     * @default SYNC_STATUS_NOT_LOADED
     */
    public int getRequestStatus(Context context, int requestType) {
        if (LogSettings.ENGINEERING && DEBUG) {
            EngLog.w(TAG, "getRequestStatus type: " + requestType);
        }
        switch (requestType) {
            case RequestInfoStorage.LOGIN:
            case RequestInfoStorage.RELOGIN_INTERNALLY:
            case RequestInfoStorage.LOGIN_CHECK:
                return RCMProviderHelper.getRequestStatus(context, RCMDataStore.SyncStatusTable.LOGIN_STATUS);
            case RequestInfoStorage.GET_API_VERSION:
                return RCMProviderHelper.getRequestStatus(context, RCMDataStore.SyncStatusTable.API_VERSION_STATUS);
            case RequestInfoStorage.GET_ALL_CALL_LOGS:
                if (LogSettings.ENGINEERING && DEBUG) {
                    EngLog.w(TAG, "getRequestStatus GET_ALL_CALL_LOGS: " + RCMProviderHelper.getRequestStatus(context, RCMDataStore.SyncStatusTable.ALL_CALL_LOGS_STATUS));
                }
                return RCMProviderHelper.getRequestStatus(context, RCMDataStore.SyncStatusTable.ALL_CALL_LOGS_STATUS);
            case RequestInfoStorage.GET_MISSED_CALL_LOGS:
                return RCMProviderHelper.getRequestStatus(context, RCMDataStore.SyncStatusTable.MISSES_CALL_LOGS_STATUS);
            case RequestInfoStorage.CALL_STATUS:
                return RCMProviderHelper.getRequestStatus(context, RCMDataStore.SyncStatusTable.CALL_STATUS);
            case RequestInfoStorage.RINGOUT_CALL:
                return RCMProviderHelper.getRequestStatus(context, RCMDataStore.SyncStatusTable.RINGOUT_CALL_STATUS);
            case RequestInfoStorage.DIRECT_RINGOUT:
                return RCMProviderHelper.getRequestStatus(context, RCMDataStore.SyncStatusTable.DIRECT_RINGOUT_STATUS);
            case RequestInfoStorage.RINGOUT_CANCEL:
                return RCMProviderHelper.getRequestStatus(context, RCMDataStore.SyncStatusTable.RINGOUT_CANCEL_STATUS);
            case RequestInfoStorage.LIST_EXTENSIONS:
                return RCMProviderHelper.getRequestStatus(context, RCMDataStore.SyncStatusTable.LIST_EXTENSIONS_STATUS);
            case RequestInfoStorage.SET_DND_STATUS:
            case RequestInfoStorage.SET_EXTENDED_DND_STATUS:
                return RCMProviderHelper.getRequestStatus(context, RCMDataStore.SyncStatusTable.DND_STATUS);
            case RequestInfoStorage.GET_ACCOUNT_INFO:
            case RequestInfoStorage.GET_ACCOUNT_INFO_CHECK_COUNTERS:
            case RequestInfoStorage.GET_ACCOUNT_INFO_CHECK_EXT_COUNTER:
            case RequestInfoStorage.GET_ACCOUNT_INFO_CHECK_MSG_COUNTER:
                return RCMProviderHelper.getRequestStatus(context, RCMDataStore.SyncStatusTable.ACCOUNT_INFO_STATUS);
            case RequestInfoStorage.GET_CALLER_IDS:
                return RCMProviderHelper.getRequestStatus(context, RCMDataStore.SyncStatusTable.CALLER_IDS_STATUS);
            case RequestInfoStorage.SET_SETUP_WIZARD_STATE:
                return RCMProviderHelper.getRequestStatus(context, RCMDataStore.SyncStatusTable.SETUP_WIZARD_STATE_STATUS);
            default:
                if (LogSettings.ENGINEERING && DEBUG) {
                    EngLog.w(TAG, "getRequestStatus UNKNOWN request type: " + requestType);
                }
                return RCMDataStore.SyncStatusEnum.SYNC_STATUS_NOT_LOADED;
        }
    }

    /**
     * Create AbstractRequestObject with properties, fill it with base properties if needed and return it to caller
     *
     * @param context
     * @param infoStorageType
     * @param setBaseProperties
     * @return wellformed AbstractRequestObject
     */
    private AbstractRequestObject getRequestObject(Context context, int infoStorageType, boolean setBaseProperties) {
        AbstractRequestObject object = new AbstractRequestObject(RequestInfoStorage.findByType(infoStorageType));

        if (LogSettings.ENGINEERING && DEBUG) {
            EngLog.w(TAG, "getRequestObject() type : " + infoStorageType + ", setBaseProps? " + setBaseProperties + ", Is returned object NULL? " + (object == null));
        }

        if (setBaseProperties) {
            // TODO : check for valid IP Address after RELOGIN
            String ip[] = RCMProviderHelper.getLoginIPAddress(context).split(DOT_PATTERN);
            if (LogSettings.ENGINEERING && NetworkManager.DEBUG) {
                EngLog.w(TAG, "IP length: " + (ip == null ? "null" : ip.length) + ", IP: " + (ip.length >= 4 ? ip[0] + ":" + ip[1] + ":" + ip[2] + ":" + ip[3] : "EMPTY"));
                EngLog.w(TAG, "AUTH_TYPE: " + object.getRequestInfo().getAuthType());
            }
            if (ip.length > 1) {
                object.getRequestBody()
                        .setProperty(IP_FIRST, ip[0])
                        .setProperty(IP_SECOND, ip[1])
                        .setProperty(IP_THIRD, ip[2])
                        .setProperty(IP_FOURTH, ip[3])
                        .setProperty(REQUEST_ID, RCMProviderHelper.getLoginRequestID(context))
                        .setProperty(START_TIME, RCMProviderHelper.getLoginStartTime(context))
                        .setProperty(USER_ID, RCMProviderHelper.getUserId(context));
            }
//            if (object.getRequestInfo().getAuthType() == RequestInfo.AUTH_TYPE.SET_COOKIE && getCookie() != null) {
//                object.getRequestBody().setProperty(COOKIE, getCookie());
//            }
        }
        return object;
    }

    private AbstractRequestObject fillLogsParams(Context context, int recordCount, int type, String direction, String cacheStart, String cacheEnd) {
        AbstractRequestObject requestObject = getRequestObject(context,
                type == RCMDataStore.CallLogTable.LOGS_ALL
                        ? RequestInfoStorage.GET_ALL_CALL_LOGS
                        : RequestInfoStorage.GET_MISSED_CALL_LOGS,
                true);

        String callType = type == RCMDataStore.CallLogTable.LOGS_ALL ? ALL : MISSED;

        requestObject.getRequestBody().
                setProperty(MAILBOX_ID, RCMProviderHelper.getCurrentMailboxId(context)).
                setProperty(CACHE_END, cacheEnd).
                setProperty(CACHE_START, cacheStart).
                setProperty(DIRECTION, direction).
                setProperty(PERIOD, PERIOD_VALUE).
                setProperty(REC_COUNT, recordCount).
                setProperty(CALL_TYPE, callType);

        return requestObject;
    }

    private synchronized void saveCredentials(Context context, String phone, String ext, String password) {
        RCMProviderHelper.saveLoginNumber(context, phone);
        RCMProviderHelper.saveLoginExt(context, ext);
        RCMProviderHelper.saveLoginPassword(context, password);
    }

    public synchronized void removeCredentials(Context context) {
        RCMProviderHelper.saveLoginNumber(context, "");
        RCMProviderHelper.saveLoginExt(context, "");
        RCMProviderHelper.saveLoginPassword(context, "");
    }

    public synchronized void setCookie(String cookie) {
        if (LogSettings.MARKET) {
            EngLog.w(TAG, "setCookie() = " + cookie);
        }
        this.mCookie = cookie;
    }

    public String getCookie() {
        if (LogSettings.ENGINEERING && DEBUG) {
            EngLog.w(TAG, "getCookie() = " + mCookie);
        }
        return mCookie == null ? "" : mCookie;
    }

    private void sendNotify(Context context, int objectType, int status, Object result) {
        for (Class cls : context.getClass().getInterfaces()) {
            if (cls.equals(NetworkManagerNotifier.class)) {
                if (LogSettings.ENGINEERING) {
                    EngLog.d(TAG, "send notify to " + context.getClass().getCanonicalName() + " that status changed to : " + status);
                }
                if (LogSettings.ENGINEERING && DEBUG) {
                    EngLog.w(TAG, "Bundle type = " + objectType + ", status = " + status + ", result null? " + (result == null));
                }
                if (((NetworkManagerNotifier) context).getNotifier() != null) {
                    ((NetworkManagerNotifier) context).getNotifier().statusChanged(prepareNotifyBundle(context, objectType, status, result), result);
                } else {
                    ((NetworkManagerNotifier) context).statusChanged(prepareNotifyBundle(context, objectType, status, result), result);
                }
                return;
            }
        }
    }

    private Bundle prepareNotifyBundle(Context context, int objectType, int status, Object result) {
        Bundle bundle = new Bundle();
        bundle.putInt(REQUEST_INFO.TYPE, objectType);
        bundle.putInt(REQUEST_INFO.STATUS, status);
        if (status != RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR
                && status != RCMDataStore.SyncStatusEnum.SYNC_STATUS_NOT_LOADED) {
            bundle.putInt(REQUEST_INFO.SERVER_TYPE, SERVER_TYPE.JEDI);
        } else if (status == RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR) {
            bundle.putInt(REQUEST_INFO.SERVER_TYPE, SERVER_TYPE.HTTP);
        } else {
            bundle.putInt(REQUEST_INFO.SERVER_TYPE, SERVER_TYPE.NOT_SET);
        }
        if (result != null) {
            if (result instanceof Exception) {
                // TODO : check for real error code from HTTP. a u sure?
                bundle.putInt(REQUEST_INFO.ERROR_CODE, -1);
                bundle.putString(REQUEST_INFO.MESSAGE_SYSTEM, ((Exception) result).getMessage());
                bundle.putString(REQUEST_INFO.MESSAGE_USER, context.getString(R.string.generic_network_error));
                bundle.putInt(REQUEST_INFO.SERVER_TYPE, SERVER_TYPE.HTTP);
                if (LogSettings.MARKET) {
                    try {
                        MktLog.w(TAG, "Bundle via EXCEPTION error_code = " + "-1" + ", message = " + ((Exception) result).getMessage());
                    } catch (java.lang.Throwable th) {
                    }
                }
            } else {
                bundle.putInt(REQUEST_INFO.ERROR_CODE, ((BasePOJO) result).getErrorCode());
                bundle.putString(REQUEST_INFO.MESSAGE_SYSTEM, ((BasePOJO) result).getMessage());
                
                int errorCode = ((BasePOJO) result).getErrorCode();
                
                if (errorCode == ERROR_CODES.MAILBOX_USERNAME_OR_PASSWORD_INCORRECT) {
                    bundle.putString(REQUEST_INFO.MESSAGE_USER, context.getString(R.string.accountinfo_failed));
                } else if (errorCode == ERROR_CODES.JEDI_ERROR_INACTIVE_USER) {
                    bundle.putString(REQUEST_INFO.MESSAGE_USER, context.getString(R.string.jedi_error_712_inactive_user_prompt));
                } else if (errorCode == ERROR_CODES.NETWORK_NOT_AVAILABLE_AIRPLANE_ON) {
                    bundle.putString(REQUEST_INFO.MESSAGE_USER, ((BasePOJO) result).getMessage());
                } else {
                    bundle.putString(REQUEST_INFO.MESSAGE_USER, context.getString(R.string.generic_network_error));
                }
                bundle.putInt(REQUEST_INFO.SERVER_TYPE, SERVER_TYPE.JEDI);
                if (LogSettings.MARKET) {
                    try {
                        QaLog.w(TAG, "Bundle via POJO. error_code = " + errorCode + ", message = " + ((BasePOJO) result).getMessage());
                    } catch (java.lang.Throwable th) {
                    }

                }
            }
        }
        return bundle;
    }

    private class BackgroundTask extends AsyncTask<Object, Integer, Integer> {

        @Override
        protected Integer doInBackground(Object... objects) {
            if (objects[1] instanceof AbstractRequestObject
                    && ((AbstractRequestObject) objects[1]).getRequestInfo().getType() == RequestInfoStorage.CALL_STATUS) {
                if (objects[2] instanceof Boolean) {
                    if (((Boolean) objects[2]).booleanValue()) {
                        mLoopCallStatusCheck = true;
                        while (mLoopCallStatusCheck) {
                            setRequestStatus((Context) objects[0], (AbstractRequestObject) objects[1], null, RCMDataStore.SyncStatusTable.SYNC_STATUS_NOT_LOADED);
                            send((Context) objects[0], (AbstractRequestObject) objects[1]);
                            // TODO : change thread sleep to managed thread
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                            }
                        }
                        return null;
                    } else {
                        send((Context) objects[0], (AbstractRequestObject) objects[1]);
                    }
                }
            } else if (objects[0] instanceof Context
                    && objects[1] instanceof AbstractRequestObject) {
                send((Context) objects[0], (AbstractRequestObject) objects[1]);
            }
            return null;
        }
    }


    private boolean storeLogin(Context context, AbstractRequestObject object, Object result) {
        // TODO : refactor == main info saved at method login(...) call
        if (LogSettings.MARKET) {
            QaLog.d(TAG, "storeLogin()");
        }
        RCMProviderHelper.setCurrentMailboxId(context, ((LoginPOJO) result).getMailboxId());
        RCMProviderHelper.saveLoginIPAddress(context, ((LoginPOJO) result).getIpAddress());
        RCMProviderHelper.saveLoginRequestID(context, String.valueOf(((LoginPOJO) result).getRequestId()));
        RCMProviderHelper.saveLoginStartTime(context, String.valueOf(((LoginPOJO) result).getStartTime()));
        RCMProviderHelper.saveUserId(context, String.valueOf(((LoginPOJO) result).getUserId()));
        RCMProviderHelper.saveLoginHash(context, String.valueOf(((LoginPOJO) result).getLoginHash()));
        return true;
    }

    private void storeApiVersion(Context context, AbstractRequestObject object, Object result) {
        if (LogSettings.MARKET) {
            QaLog.d(TAG, "storeApiVersion()");
        }
        RCMProviderHelper.saveServiceApiVersion(context, ((ApiVersionPOJO) result).getApiVersion());        
    }

    private boolean storeCallerIds(Context context, AbstractRequestObject object, Object result) {
        if (LogSettings.MARKET) {
            QaLog.d(TAG, "storeCallerIds()");
        }

        ContentResolver resolver = context.getContentResolver();
        List<CallerIdPOJO> callerIds = ((CallerIdListPOJO) result).getCallerIdsList();
        if (callerIds == null || callerIds.size() <= 0) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "storeCallerIds(): Empty list");
            }
            
            return false;
        }

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "storeCallerIds(): size: " + callerIds.size());
        }
        

        ContentValues[] callerIds_values = new ContentValues[callerIds.size()];

        int i = 0;
        ContentValues values_i;
        for (CallerIdPOJO pojo : callerIds) {
            values_i = new ContentValues();

            values_i.put(RCMDataStore.CallerIDsTable.MAILBOX_ID, RCMProviderHelper.getCurrentMailboxId(context));
            values_i.put(RCMDataStore.CallerIDsTable.JEDI_NUMBER, pojo.getNumber());
            values_i.put(RCMDataStore.CallerIDsTable.JEDI_USAGE_TYPE, pojo.getUsageType());

            callerIds_values[i++] = values_i;
        }

        resolver.delete(
                UriHelper.getUri(RCMProvider.CALLER_IDS, RCMProviderHelper.getCurrentMailboxId(context)), 
                null, null);

        resolver.bulkInsert(UriHelper.getUri(RCMProvider.CALLER_IDS), callerIds_values);
        
        // said that now we have CallerID main number and set it to default if TIERS_PHS_CHANGE_CALLERID is 0 and CalledIDs settings blocked
        setCallerIdsTierSettings(context, RCMProviderHelper.getTierSettings(context));

        return true;
    }

    private boolean storeAccountInfo(Context context, AbstractRequestObject object, Object result,
            boolean checkExtCounter, boolean checkMsgCounter) {
        if (LogSettings.MARKET) {
            QaLog.d(TAG, "storeAccountInfo()");
        }

        ContentValues values = new ContentValues();
        values.put(RCMDataStore.AccountInfoTable.MAILBOX_ID, RCMProviderHelper.getCurrentMailboxId(context));
        values.put(RCMDataStore.AccountInfoTable.RCM_LOGIN_NUMBER, RCMProviderHelper.getLoginNumber(context));
        values.put(RCMDataStore.AccountInfoTable.RCM_LOGIN_EXT, RCMProviderHelper.getLoginExt(context));
        values.put(RCMDataStore.AccountInfoTable.RCM_PASSWORD, RCMProviderHelper.getLoginPassword(context));
        values.put(RCMDataStore.AccountInfoTable.JEDI_ACCOUNT_NUMBER, ((AccountInfoPOJO) result).getAccountNumber());
        values.put(RCMDataStore.AccountInfoTable.JEDI_FIRST_NAME, ((AccountInfoPOJO) result).getFirstName());
        values.put(RCMDataStore.AccountInfoTable.JEDI_LAST_NAME, ((AccountInfoPOJO) result).getLastName());
        values.put(RCMDataStore.AccountInfoTable.JEDI_EMAIL, ((AccountInfoPOJO) result).getEmail());
        values.put(RCMDataStore.AccountInfoTable.JEDI_AGENT, ((AccountInfoPOJO) result).isAgent() ? 1 : 0);
        values.put(RCMDataStore.AccountInfoTable.JEDI_AGENT_STATUS, ((AccountInfoPOJO) result).getAgentStatus());
        values.put(RCMDataStore.AccountInfoTable.JEDI_DND, ((AccountInfoPOJO) result).isDnd() ? 1 : 0);
        values.put(RCMDataStore.AccountInfoTable.JEDI_DND_STATUS, ((AccountInfoPOJO) result).getDndStatus());
        values.put(RCMDataStore.AccountInfoTable.JEDI_FREE, ((AccountInfoPOJO) result).isFree() ? 1 : 0);
        values.put(RCMDataStore.AccountInfoTable.JEDI_PIN, ((AccountInfoPOJO) result).getPin());
        values.put(RCMDataStore.AccountInfoTable.JEDI_USER_ID, ((AccountInfoPOJO) result).getUserId());
        values.put(RCMDataStore.AccountInfoTable.JEDI_ACCESS_LEVEL, ((AccountInfoPOJO) result).getAccessLevel());
        values.put(RCMDataStore.AccountInfoTable.JEDI_EXTENSION_TYPE, ((AccountInfoPOJO) result).getExtensionType());
        values.put(RCMDataStore.AccountInfoTable.JEDI_TIER_SERVICE_TYPE, ((AccountInfoPOJO) result).getTierServiceType());
        values.put(RCMDataStore.AccountInfoTable.JEDI_SYSTEM_EXTENSION, ((AccountInfoPOJO) result).isSystemExtension()? 1 : 0);
        values.put(RCMDataStore.AccountInfoTable.JEDI_SERVICE_VERSION, ((AccountInfoPOJO) result).getServiceVersion());
        values.put(RCMDataStore.AccountInfoTable.JEDI_TIER_SETTINGS, ((AccountInfoPOJO) result).getTierSettings());
        values.put(RCMDataStore.AccountInfoTable.JEDI_EXT_MOD_COUNTER_TEMP, ((AccountInfoPOJO) result).getGlobalExtModCounter());
        values.put(RCMDataStore.AccountInfoTable.JEDI_MSG_MOD_COUNTER_TEMP, ((AccountInfoPOJO) result).getMsgModCounter());
        values.put(RCMDataStore.AccountInfoTable.JEDI_SETUP_WIZARD_STATE, ((AccountInfoPOJO) result).getSetupWizardState());
        values.put(RCMDataStore.AccountInfoTable.JEDI_EXPRESS_SETUP_MOBILE_URL, ((AccountInfoPOJO) result).getExpressSetupMobileUrl());
        values.put(RCMDataStore.AccountInfoTable.JEDI_TRIAL_EXPIRATION_STATE, ((AccountInfoPOJO) result).getTrialExpirationState());
        values.put(RCMDataStore.AccountInfoTable.JEDI_DAYS_TO_EXPIRE, ((AccountInfoPOJO) result).getDaysToExpire());
        values.put(RCMDataStore.AccountInfoTable.JEDI_BRAND_ID, ((AccountInfoPOJO) result).getBrandId());
        
        
        final String expressSetupUrl = ((AccountInfoPOJO) result).getExpressSetupMobileUrl();
        if(BUILD.URI.SETUP_URL_OVERRIDING_ENABLE && !TextUtils.isEmpty(expressSetupUrl)){
        	RCMConfig.set_SETUP_URL(context, expressSetupUrl);
        }
        
        ContentResolver resolver = context.getContentResolver();
        Cursor cursor = resolver.query(
                UriHelper.getUri(RCMProvider.ACCOUNT_INFO, RCMProviderHelper.getCurrentMailboxId(context)),
                null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            resolver.update(UriHelper.getUri(RCMProvider.ACCOUNT_INFO, RCMProviderHelper.getCurrentMailboxId(context)),
                    values, null, null);
        } else {
            resolver.insert(UriHelper.getUri(RCMProvider.ACCOUNT_INFO), values);
        }

        if (cursor != null) {
            cursor.close();
        }

        resolver.delete(UriHelper.getUri(RCMProvider.FW_NUMBERS, RCMProviderHelper.getCurrentMailboxId(context)), null, null);
        List<ForwardingNumberPOJO> fw_numbers = ((AccountInfoPOJO) result).getForwardingNumbers();
        if (fw_numbers != null && fw_numbers.size() > 0) {
            ContentValues[] fwn_values = new ContentValues[fw_numbers.size()];
            int i = 0;
            ContentValues values_i;
            for (ForwardingNumberPOJO fwn_pojo : fw_numbers) {
                values_i = new ContentValues();
                values_i.put(RCMDataStore.FwNumbersTable.MAILBOX_ID, RCMProviderHelper.getCurrentMailboxId(context));
                values_i.put(RCMDataStore.FwNumbersTable.JEDI_NAME, fwn_pojo.getName());
                values_i.put(RCMDataStore.FwNumbersTable.JEDI_NUMBER, fwn_pojo.getNumber());
                values_i.put(RCMDataStore.FwNumbersTable.JEDI_ORDER_BY, fwn_pojo.getOrderBy());
                values_i.put(RCMDataStore.FwNumbersTable.JEDI_TYPE, fwn_pojo.getType());
                fwn_values[i++] = values_i;
            }
            resolver.bulkInsert(UriHelper.getUri(RCMProvider.FW_NUMBERS), fwn_values);
        }

        setTierSettings(context,
                ((AccountInfoPOJO) result).getTierSettings(),
                UserInfo.getTierServiceTypeByString(context, ((AccountInfoPOJO) result).getTierServiceType()),
                UserInfo.getUserType(context));
        
        if (checkExtCounter) {
            checkExtCounter(context, true);
        }
        if (checkMsgCounter) {
            checkMsgCounter(context, true);
        }
        
        //!!!Don't remove! Needed to validate the DND cache and send DND notification
        if (((AccountInfoPOJO) result).getServiceVersion() >= AccountInfoTable.SERVICE_VERSION_5) {
            DndManager.getInstance().saveExtendedDndStatus(context, ((AccountInfoPOJO) result).getDndStatus());
        } else {
            DndManager.getInstance().saveDndStatus(context, ((AccountInfoPOJO) result).isDnd());
        }
        return true;
    }

    private boolean storeExtensionsList(Context context, AbstractRequestObject object, Object result) {
        if (LogSettings.MARKET) {
            QaLog.d(TAG, "storeExtensionsList():");
        }
        long mailbox_id = RCMProviderHelper.getCurrentMailboxId(context);
        List <ExtensionPOJO> new_ext_list = ((ExtensionsListPOJO)result).getList();

        if (LogSettings.MARKET) {
            QaLog.i(TAG, "storeExtensionsList(): " + new_ext_list.size() + " entries received from server");
        }
        
        /* Retrieve old extensions' list from the DB */ 
        HashMap<Long, ExtensionPOJO> old_ext_map = null;
        
        Cursor c = context.getContentResolver().query(
                UriHelper.getUri(RCMProvider.EXTENSIONS, mailbox_id), null, null, null, null);

        if (c != null && c.getCount() > 0) {
            old_ext_map = new HashMap<Long, ExtensionPOJO>();

            ExtensionPOJO pojo;
            long ext_mailbox_id;
            Integer starred;
            
            c.moveToFirst();
            do {
                pojo = new ExtensionPOJO();
                
                ext_mailbox_id = c.getLong(c.getColumnIndex(RCMDataStore.ExtensionsTable.JEDI_MAILBOX_ID_EXT));
                pojo.setMailboxId(ext_mailbox_id);
                
                pojo.setEmail(c.getString(c.getColumnIndex(RCMDataStore.ExtensionsTable.JEDI_EMAIL)));
                pojo.setFirstName(c.getString(c.getColumnIndex(RCMDataStore.ExtensionsTable.JEDI_FIRST_NAME)));
                pojo.setMiddleName(c.getString(c.getColumnIndex(RCMDataStore.ExtensionsTable.JEDI_MIDDLE_NAME)));
                pojo.setLastName(c.getString(c.getColumnIndex(RCMDataStore.ExtensionsTable.JEDI_LAST_NAME)));
                pojo.setPin(c.getString(c.getColumnIndex(RCMDataStore.ExtensionsTable.JEDI_PIN)));
                pojo.setAddressLine1(c.getString(c.getColumnIndex(RCMDataStore.ExtensionsTable.JEDI_ADDRESS_LINE_1)));
                pojo.setAddressLine2(c.getString(c.getColumnIndex(RCMDataStore.ExtensionsTable.JEDI_ADDRESS_LINE_2)));
                pojo.setCity(c.getString(c.getColumnIndex(RCMDataStore.ExtensionsTable.JEDI_CITY)));
                pojo.setCountry(c.getString(c.getColumnIndex(RCMDataStore.ExtensionsTable.JEDI_COUNTRY)));
                pojo.setState(c.getString(c.getColumnIndex(RCMDataStore.ExtensionsTable.JEDI_STATE)));
                pojo.setZipCode(c.getString(c.getColumnIndex(RCMDataStore.ExtensionsTable.JEDI_ZIPCODE)));
                
                starred = c.getInt(c.getColumnIndex(RCMDataStore.ExtensionsTable.RCM_STARRED));
                pojo.setStarred(starred != null ? starred != 0 : false);

                old_ext_map.put(ext_mailbox_id, pojo);
            } while (c.moveToNext());
        } else {
            if (LogSettings.MARKET) {
                QaLog.w(TAG, "storeExtensionsList(): empty old_ext list");
            }
        }

        if (c != null) {
            c.close();
        }
        
        if (old_ext_map != null) {
            if (LogSettings.MARKET) {
                QaLog.i(TAG, "storeExtensionsList(): " + old_ext_map.size() + " entries retrieved from DB");
            }
            
            /* Copy favorites status from the old list to the new list */
            for (ExtensionPOJO ext : new_ext_list) {
                ExtensionPOJO old = old_ext_map.get(ext.getMailboxId());
                boolean starred = (old != null ? old.isStarred() : false);
                ext.setStarred(starred);
            }
            
            /* Remove obsolete entries from the DB */
            Collection<ExtensionPOJO> old_ext = old_ext_map.values();
            ContentResolver resolver = context.getContentResolver();
            String[] selectionArgs = new String[1];
            int count = 0;
            for (ExtensionPOJO ext : old_ext) {
                if (!new_ext_list.contains(ext)) {
                    selectionArgs[0] = Long.toString(ext.getMailboxId());
                    count += resolver.delete(UriHelper.getUri(RCMProvider.EXTENSIONS, mailbox_id),
                            RCMDataStore.ExtensionsTable.JEDI_MAILBOX_ID_EXT + " = ?",
                            selectionArgs);
                }
            }

            if (LogSettings.MARKET) {
                QaLog.i(TAG, "storeExtensionsList(): " + count + " obsolete entries removed from DB");
            }
            
            
            /* Remove already existing entries from the new extensions' list */
            new_ext_list.removeAll(old_ext);
            if (LogSettings.MARKET) {
                QaLog.i(TAG, "storeExtensionsList(): " + new_ext_list.size() + " new/modified entries");
            }
        }        
        
        if (!new_ext_list.isEmpty()) {
            /* Store new extensions list to the database */
            ContentValues item;
            Vector<ContentValues> values = new Vector<ContentValues>();

            for (ExtensionPOJO pojo : new_ext_list){
                item = new ContentValues();
                item.put(ExtensionsTable.MAILBOX_ID, mailbox_id);
    
                String first_name = pojo.getFirstName();
                String middle_name = pojo.getMiddleName();
                String last_name = pojo.getLastName();
                
                StringBuilder display_name = new StringBuilder(first_name);
                if (!TextUtils.isEmpty(middle_name)) {
                    if (display_name.length() != 0) {
                        display_name.append(' ');
                    }
                    display_name.append(middle_name); 
                }
                if (!TextUtils.isEmpty(last_name)) {
                    if (display_name.length() != 0) {
                        display_name.append(' ');
                    }
                    display_name.append(last_name); 
                }
    
                item.put(ExtensionsTable.JEDI_FIRST_NAME, first_name);
                item.put(ExtensionsTable.JEDI_LAST_NAME, last_name);
                item.put(ExtensionsTable.JEDI_MIDDLE_NAME, middle_name);
                item.put(ExtensionsTable.RCM_DISPLAY_NAME, display_name.toString());
    
                item.put(ExtensionsTable.JEDI_MAILBOX_ID_EXT, pojo.getMailboxId());
                item.put(ExtensionsTable.JEDI_PIN, pojo.getPin());
                item.put(ExtensionsTable.JEDI_EMAIL, pojo.getEmail());
                item.put(ExtensionsTable.JEDI_ADDRESS_LINE_1, pojo.getAddressLine1());
                item.put(ExtensionsTable.JEDI_ADDRESS_LINE_2, pojo.getAddressLine2());
                item.put(ExtensionsTable.JEDI_CITY, pojo.getCity());
                item.put(ExtensionsTable.JEDI_COUNTRY, pojo.getCountry());
                item.put(ExtensionsTable.JEDI_STATE, pojo.getState());
                item.put(ExtensionsTable.JEDI_ZIPCODE, pojo.getZipCode());
                item.put(ExtensionsTable.RCM_STARRED, pojo.isStarred()? 1 : 0);
                
                values.addElement(item);
            }
            
            
            if (values.size() > 0) {
                if (LogSettings.MARKET) {
                    QaLog.i(TAG, "storeExtensionsList() put size: " + values.size());
                }
                context.getContentResolver().bulkInsert(
                        UriHelper.getUri(RCMProvider.EXTENSIONS), values.toArray(new ContentValues[values.size()]));
            }
        }
        
        long ext_mod_counter = ((ExtensionsListPOJO)result).getExtModCounter();
        if (ext_mod_counter > 0) {
            long cur_ext_mod_counter = RCMProviderHelper.getExtModCounter(context);
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "storeExtensionsList(): Extension mod counter current: " + cur_ext_mod_counter + " from server: " + ext_mod_counter);
            }
            if (ext_mod_counter != cur_ext_mod_counter) {
                RCMProviderHelper.saveExtModCounter(context, ext_mod_counter);
            }
        }
        return true;
    }


    private void setTierSettings(Context context, final long tierSettings, final int tierServiceType, final int userType) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "setTierSettings(): tierSettings=0x" + Long.toHexString(tierSettings) + "; tierServiceType=" + tierServiceType + "; userType=" + userType);
        }

        if ((tierSettings & RCMConstants.TIERS_PHS_CAN_VIEW_EXTENSIONS) == 0) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "setTierSettings(): TIERS_PHS_CAN_VIEW_EXTENSIONS _NOT_ SET, so we couldn't load extensions and now erase existing.");
            }
            context.getContentResolver().delete(UriHelper.getUri(RCMProvider.EXTENSIONS, RCMProviderHelper.getCurrentMailboxId(context)), null, null);
            UserInfo.setCompanyExtensionsAllowed(false);
        } else {
            UserInfo.setCompanyExtensionsAllowed(true);
        }


        if (tierServiceType == UserInfo.TIER_SERVICE_TYPE_RCFAX
                || userType == UserInfo.DEPARTMENT
                || userType == UserInfo.TAKE_MESSAGES_ONLY
                || userType == UserInfo.ANNOUNCEMENTS_ONLY) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "setTierSettings(): DND status will not be shown for this account/ext: tierServiceType=" + tierServiceType + "; userType=" + userType);
            }
            UserInfo.setDndAllowed(false);
        } else if ((tierSettings & RCMConstants.TIERS_PHS_DO_NOT_DISTURB) == 0) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "setTierSettings(): TIERS_PHS_DO_NOT_DISTURB not set, so we will not show DND status.");
            }
            UserInfo.setDndAllowed(false);
        } else {
            UserInfo.setDndAllowed(true);
        }


        setCallerIdsTierSettings(context, tierSettings);
    }

    private void setCallerIdsTierSettings(Context context, final long tierSettings) {
        if ((tierSettings & RCMConstants.TIERS_PHS_CHANGE_CALLERID) == 0) {
            UserInfo.setCallerIdsAllowed(false);
            String main_number = RCMProviderHelper.getMainNumber(context);
            
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "setCallerIdsTierSettings(): TIERS_PHS_CHANGE_CALLERID _NOT_ SET. Set CallerID to: "
                        + (TextUtils.isEmpty(main_number) ? "not available, will be set later." : main_number));
            }
            
            RCMProviderHelper.saveCallerID(context, main_number);
        } else {
            UserInfo.setCallerIdsAllowed(true);
            String currentCallerId = RCMProviderHelper.getCallerID(context);
            if (!TextUtils.isEmpty(currentCallerId) && !RCMProviderHelper.isNumberInCallerIds(context, currentCallerId)) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "Cuerrent callerId " + currentCallerId + " is not in CallerIds, set main as default");
                }
                RCMProviderHelper.saveCallerID(context, RCMProviderHelper.getMainNumber(context));
            }
        }
    }

}
