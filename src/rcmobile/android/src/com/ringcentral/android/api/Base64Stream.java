package com.ringcentral.android.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class Base64Stream {
	
	private static final String sf_rgchBase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+*";
	private static final byte[] sf_mpwchb = new byte[256];
	private static final int sf_bBad = 0xff;
	
	static { // only fill this array once
		int i;

		// Initialize our decoding array
		for (i = 0; i < 256; i++) {
			sf_mpwchb[i] = (byte)sf_bBad;
		}

		for (i = 0; i < 64; i++) {
			sf_mpwchb[sf_rgchBase64.charAt(i)] = (byte) i;
		}
	}

	/*
	 * 
	 */
	private static int read( InputStream is, int[] result )  {
		int read = 0;
		for( int i = 0; i < 3; i++ ){
			try{
				int next_value = is.read();
				if( next_value == -1 )
					break;
				
				result[read++] = ( next_value & 0x00FF );		
			}
			catch( IOException e_io ){
				break;
			}
		}
		return read;
	}

	/*
	 * 
	 */
	private static int read( InputStream is )  {
		int result = -1;
		try{
			result = is.read();		
		}
		catch( IOException e_io ){
		}
	return result;
	}
	
	/*
	 * 
	 */
	public static int getInitialSizeEncode( InputStream is ){
		try{
			return ( is.available() * 4 ) / 3;
		}
		catch( IOException e_io ){
			
		}
		
		return 512;
	}

	/*
	 * 
	 */
	public static int getInitialSizeDecode( InputStream is ){
		try{
			return ( ( is.available() * 3 ) / 4 );
		}
		catch( IOException e_io ){
			
		}
		
		return 512;
	}
	
	/*
	 * 
	 */
	 public static int encodeBase64( InputStream is, OutputStream os ) {
		
		int output_size = 0;
		int buffer[] = new int[3];
		int read = 0; 
		
		try{
			//         
			// Main encoding loop
			//         
			while( (read = read( is, buffer)) == 3) {
				
				byte b0 = (byte) ((  buffer[0] >> 2) & 0x3F );
				byte b1 = (byte) ((( buffer[0] & 0x03) << 4) | ((buffer[1] >> 4) & 0x0F ));
				byte b2 = (byte) ((( buffer[1] & 0x0F) << 2) | ((buffer[2] >> 6) & 0x03 ));
				byte b3 = (byte) ((  buffer[2] & 0x3F));
				
				os.write(sf_rgchBase64.charAt(b0));
				os.write(sf_rgchBase64.charAt(b1));
				os.write(sf_rgchBase64.charAt(b2));
				os.write(sf_rgchBase64.charAt(b3));
				
				output_size += 4;
			}

			if (read == 0) { // nothing to do
			} 
			else if (read == 1) {
				byte b0 = (byte) ( ( buffer[0] >> 2 ) & 0x3F);
				byte b1 = (byte) (((buffer[0] & 0x03) << 4) | 0);
				
				os.write(sf_rgchBase64.charAt(b0));
				os.write(sf_rgchBase64.charAt(b1));
				os.write('=');
				os.write('=');

				output_size += 4;
			} 
			else if (read == 2) {
				byte b0 = (byte) ((buffer[0] >> 2) & 0x3F);
				byte b1 = (byte) (((buffer[0] & 0x03) << 4) | ((buffer[1] >> 4) & 0x0F));
				byte b2 = (byte) (((buffer[1] & 0x0F) << 2) | 0);
				
				os.write(sf_rgchBase64.charAt(b0));
				os.write(sf_rgchBase64.charAt(b1));
				os.write(sf_rgchBase64.charAt(b2));
				os.write('=');

				output_size += 4;
			}
			
		}
		catch( IOException e_io ){
			output_size = 0;
		}

		try {
			os.close();
		} catch (IOException e) {
			
		}
		
		return output_size;
	} 
	 
	 /*
	  *  Note: that the size will be less than the original
	  */
	 public static int decodeBase64(InputStream is, OutputStream os)
		{
			// Loop over the entire input buffer
			long bCurrent = 0;
			// what we're in the process of filling up
			int cbitFilled = 0;

			// current destination (not filled)
			int output_size = 0;
			int next_char = 0;

			try{
				while( (next_char = read(is)) != -1 ){
					next_char &= 0xff;

					// Ignore white space
					if (next_char == 0x0A || next_char == 0x0D || next_char == 0x20 || next_char == 0x09) {
						continue; // Have we reached the end?
					}
					if (next_char == '=') {
						break;
					}
					int bDigit = sf_mpwchb[next_char] & 0xff;

					if ( bDigit == sf_bBad) {
						output_size =  0; // Add in its contribution
						break;
					}
					
					bCurrent <<= 6;
					bCurrent |= bDigit;
					cbitFilled += 6;

					// If we've got enough, output a byte
					if( cbitFilled >= 8 ) {
						long b = (bCurrent >> (cbitFilled - 8));
						// get's top eight valid bits
						os.write(((int) ( b & 0xFF)) );
						output_size++;
						// store the byte away
						cbitFilled -= 8;
					}
				}
			}
			catch( IOException e_io ){
				output_size = 0;
			}
			
			try {
				os.close();
			} catch (IOException e) {
				
			}
			
			return output_size;
		}
	
}
