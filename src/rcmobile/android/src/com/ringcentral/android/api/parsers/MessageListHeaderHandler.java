/**
 * 
 */
package com.ringcentral.android.api.parsers;

import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.api.XmlParsingObjectAbstract;

/**
 * @author Denis Kudja
 *
 */
public class MessageListHeaderHandler extends XmlParsingObjectAbstract {

	private final static String sf_sf_tag_name		= "MESSAGESLISTHDR";
	private final static String sf_msg_number 		= "MsgNumber";

	private long m_msg_number = 0;
	
	public long getMsg_number() {
		return m_msg_number;
	}

	public MessageListHeaderHandler(XmlParsingObjectAbstract parent) {
		super(parent);
	}

	@Override
	public String getTagName() {
		return sf_sf_tag_name;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void  endElement  (String uri, String localName, String qName){
		super.endElement(uri, localName, qName);
		
		String elementName = getElementName( localName, qName );
		
		if( elementName.equalsIgnoreCase(sf_msg_number) ){
			m_msg_number = getLongValue();

            if( LogSettings.MARKET  ) {
                QaLog.i("MessageListHeader", "Number messages on list " + m_msg_number);
            }
			
		}
	}	
	
}
