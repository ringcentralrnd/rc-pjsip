/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api.parsers;

import org.xml.sax.SAXException;

import com.ringcentral.android.api.pojo.LoginPOJO;

/**
 * Login response handler
 * @author user
 *
 */
public class LoginPOJOHandler extends Handler<LoginPOJO> {

	private static final String MAILBOX_ID = "mailboxId";
    private static final String USER_ID =  "userId";
    private static final String IP_FIRST = "first";
    private static final String IP_SECOND = "second";
    private static final String IP_THIRD = "third";
    private static final String IP_FOURTH = "fourth";
    private static final String REQUEST_ID = "requestId";
    private static final String START_TIME = "startTime";
    private static final String LOGIN_HASH = "loginHash";

    private String ipAddress = IP_FIRST+"."+IP_SECOND+"."+IP_THIRD+"."+IP_FOURTH;
    
    public LoginPOJOHandler() {
		super();
		LoginPOJO loginPOJO = new LoginPOJO();
		setPOJO(loginPOJO);
	}



	public void endElement(String uri, String name, String qName)
			throws SAXException {
		super.endElement(uri, name, qName);
		String elementName = getElementName(name, qName);
		if (elementName.equals(MAILBOX_ID)) {
			getPOJO().setMailboxId(Long.parseLong(getBuffer().toString()));
		} else if (elementName.equals(USER_ID)) {
			getPOJO().setUserId(Long.parseLong(getBuffer().toString()));
		} else if (elementName.equals(REQUEST_ID)) {
			getPOJO().setRequestId(Long.parseLong(getBuffer().toString()));
		} else if (elementName.equals(START_TIME)) {
			getPOJO().setStartTime(Long.parseLong(getBuffer().toString()));
        } else if (elementName.equals(LOGIN_HASH)) {
            getPOJO().setLoginHash(getBuffer().toString());
		} else if (elementName.equals(IP_FIRST) || elementName.equals(IP_SECOND)
				|| elementName.equals(IP_THIRD) || elementName.equals(IP_FOURTH)) {
			ipAddress = ipAddress.replaceFirst(elementName, getBuffer()
					.toString());
			getPOJO().setIpAddress(ipAddress);
		}
	}

	@Override
	public Object getData() {
		return getPOJO();
	}
    
}
