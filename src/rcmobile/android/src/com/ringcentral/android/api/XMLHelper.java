package com.ringcentral.android.api;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;

public class XMLHelper {

	private static final String NOT_SUPPORTED_YET = "Not supported yet.";
    private static final String TAG = "[RC] XMLHelper";

    public static Node findchildByElement(Node parent, String name) {
		if (parent == null || name == null || name.length() <= 0) {
			return null;
		}

		NodeList children = parent.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);
			if (name.equalsIgnoreCase(child.getNodeName())) {
				return child;
			}
		}
		return null;
	}

	public static String getNodeValue(Node node) {
		switch (node.getNodeType()) {
		case Node.TEXT_NODE:
			return node.getNodeValue();
		case Node.DOCUMENT_NODE:
		case Node.ELEMENT_NODE:
			NodeList children = node.getChildNodes();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < children.getLength(); i++) {
				Node child = children.item(i);
				sb.append(getNodeValue(child));
			}
			return sb.toString();
		}
		return null;
	}

	

	public static DocumentBuilder getDocumentBuilder() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
			builder.setErrorHandler(new ErrorHandler() {

				public void warning(SAXParseException exception)
						throws SAXException {
					throw new UnsupportedOperationException(
							NOT_SUPPORTED_YET);
				}

				public void error(SAXParseException exception)
						throws SAXException {
					throw new UnsupportedOperationException(
							NOT_SUPPORTED_YET);
				}

				public void fatalError(SAXParseException exception)
						throws SAXException {
					throw new UnsupportedOperationException(
							NOT_SUPPORTED_YET);
				}
			});

		}

		catch (ParserConfigurationException e) {
            if (LogSettings.MARKET) {
                EngLog.e(TAG, "getDocumentBuilder()", e);
            }
		}

		return builder;

	}

}
