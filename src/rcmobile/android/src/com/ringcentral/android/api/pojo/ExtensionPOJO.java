/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api.pojo;

import android.text.TextUtils;


public class ExtensionPOJO extends BasePOJO{
	
    private long mailboxId;
	private String email;
	private String firstName;
    private String middleName;
	private String lastName;
    private String pin;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String country;
    private String state;
    private String zipCode;
	private boolean isStarred;
	

	@Override
	public boolean equals(Object object) {
        if (object == null || !(object instanceof ExtensionPOJO)) {
            return false;
        }
	    
        if (object == this) {
            return true;
        }
        
        ExtensionPOJO pojo = (ExtensionPOJO)object;
        
        if (mailboxId == pojo.mailboxId
                && TextUtils.equals(email, pojo.email)
                && TextUtils.equals(firstName, pojo.firstName)
                && TextUtils.equals(middleName, pojo.middleName)
                && TextUtils.equals(lastName, pojo.lastName)
                && TextUtils.equals(pin, pojo.pin)
                && TextUtils.equals(addressLine1, pojo.addressLine1)
                && TextUtils.equals(addressLine2, pojo.addressLine2)
                && TextUtils.equals(city, pojo.city)
                && TextUtils.equals(country, pojo.country)
                && TextUtils.equals(state, pojo.state)
                && TextUtils.equals(zipCode, pojo.zipCode)) {
            return true;
        } else {
            return false;
        }
    }
    
	@Override
	public int hashCode() {
	    return (int)mailboxId;
	}
	
	
    public long getMailboxId() {
        return mailboxId;
    }
    public void setMailboxId(long mailboxId) {
        this.mailboxId = mailboxId;
    }

	public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPin() {
        return pin;
    }
    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getAddressLine1() {
        return addressLine1;
    }
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }
    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public boolean isStarred() {
		return isStarred;
	}
	public void setStarred(boolean isStarred) {
		this.isStarred = isStarred;
	}

}
