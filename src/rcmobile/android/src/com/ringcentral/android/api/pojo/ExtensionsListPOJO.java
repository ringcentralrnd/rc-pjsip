/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api.pojo;

import java.util.ArrayList;
import java.util.List;

public class ExtensionsListPOJO  extends BasePOJO{

	private long mExtModCounter;
	private List<ExtensionPOJO> mList = new ArrayList<ExtensionPOJO>();

	
	public long getExtModCounter() {
		return mExtModCounter;
	}

	public void setExtModCounter(long counter) {
		mExtModCounter = counter;
	}

	public List<ExtensionPOJO> getList() {
		return mList;
	}
	
	public void setList(List<ExtensionPOJO> list) {
		mList = list;
	}
	
	public ExtensionPOJO getById(int id){
		for(ExtensionPOJO item: mList){
			if(item.getMailboxId()==id){
				return item;
			}
		}
		return null;
	}

}
