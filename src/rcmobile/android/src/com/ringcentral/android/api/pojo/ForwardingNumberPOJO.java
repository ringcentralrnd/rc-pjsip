package com.ringcentral.android.api.pojo;

public class ForwardingNumberPOJO  extends BasePOJO {

    private boolean forwarding;

    private String name;

    private String number;

    private int orderBy;

    private String type;
    
    public boolean isForwarding() {
		return forwarding;
	}

	public void setForwarding(boolean forwarding) {
		this.forwarding = forwarding;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public int getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(int orderBy) {
		this.orderBy = orderBy;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}



}
