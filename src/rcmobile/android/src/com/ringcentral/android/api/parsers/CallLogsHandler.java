/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.api.pojo.CallLogPOJO;
import com.ringcentral.android.api.pojo.CallLogsListPOJO;
import com.ringcentral.android.utils.DateUtils;

public class CallLogsHandler extends Handler<CallLogsListPOJO> {

    private static final String TAG = "[RC] CallLogsHandler";
    private boolean enableDebug = false;
    private static final String CALL_RECORDS = "callRecords";
    private static final String BREAK_FLAG = "breakFlag";
    private static final String CALL_DIRECTION = "callDirection";
    private static final String CALL_TYPE = "callType";
    // private static final String DELETED = "deleted";
    private static final String FROM_NAME = "fromName";
    private static final String FROM_PHONE = "fromPhone";
    private static final String TO_NAME = "toName";
    private static final String TO_PHONE = "toPhone";
    private static final String LENGTH = "length";
    private static final String LOCATION = "location";
    // private static final String MAILBOX_ID = "mailboxId";
    private static final String PIN = "pin";
    private static final String START = "start";
    private static final String STATUS = "status";
    private static final String RECORD_ID = "recordId";
    private CallLogPOJO callLogPOJO;
    private boolean inCallRecord = false;

    public CallLogsHandler() {
        super();
        setPOJO(new CallLogsListPOJO());
    }

    public void startElement(String uri, String name, String qName, Attributes atts) {
        super.startElement(uri, name, qName, atts);
        if (getElementName(name, qName).equals(CALL_RECORDS)) {
            callLogPOJO = new CallLogPOJO();
            getPOJO().getLogs().add(callLogPOJO);
            inCallRecord = true;
        }

    }

    public void endElement(String uri, String name, String qName) throws SAXException {
        super.endElement(uri, name, qName);

        if (enableDebug && LogSettings.ENGINEERING) {
            EngLog.i(TAG, "endElement: name=" + name + " qName=" + qName + " buffer=" + getBuffer().toString());
        }

        if (getElementName(name, qName).equals(CALL_RECORDS)) {
            inCallRecord = false;
        }

        if (getElementName(name, qName).equals(BREAK_FLAG)) {
            getPOJO().setBreakFlag(Integer.parseInt(getBuffer().toString()));
        }
        if (getElementName(name, qName).equals(CALL_DIRECTION)) {
            if (getBuffer().toString().equals(CallLogPOJO.DIRECTION_INCOMING)) {
                callLogPOJO.setCallDirection(CallLogPOJO.DIRECTION_INCOMING_INT);
            } else {
                callLogPOJO.setCallDirection(CallLogPOJO.DIRECTION_OUTGOING_INT);
            }
        }
        if (getElementName(name, qName).equals(CALL_TYPE)) {
            callLogPOJO.setCallType(Integer.parseInt(getBuffer().toString()));
        }
        if (getElementName(name, qName).equals(FROM_NAME)) {
            callLogPOJO.setFromName(getBuffer().toString());
        }
        if (getElementName(name, qName).equals(FROM_PHONE)) {
            callLogPOJO.setFromPhone(getBuffer().toString());
        }
        if (getElementName(name, qName).equals(TO_NAME)) {
            callLogPOJO.setToName(getBuffer().toString());
        }
        if (getElementName(name, qName).equals(TO_PHONE)) {
            callLogPOJO.setToPhone(getBuffer().toString());
        }
        if (getElementName(name, qName).equals(LENGTH)) {
            callLogPOJO.setLength(Float.parseFloat(getBuffer().toString()));
        }
        if (getElementName(name, qName).equals(LOCATION)) {
            callLogPOJO.setLocation(getBuffer().toString());
        }
        if (getElementName(name, qName).equals(PIN)) {
            callLogPOJO.setPin(getBuffer().toString());
        }
        if (getElementName(name, qName).equals(RECORD_ID)) {
            callLogPOJO.setRecordId(Long.parseLong(getBuffer().toString()));
        }
        if (getElementName(name, qName).equals(START)) {
            callLogPOJO.setStart(DateUtils.parseISO8601Date(getBuffer().toString()).getTime());
        }
        if (inCallRecord && getElementName(name, qName).equals(STATUS)) {
            int status = Integer.parseInt(getBuffer().toString());
            callLogPOJO.setStatus(status);
        }
    }

    @Override
    public Object getData() {
        return getPOJO();
    }
}
