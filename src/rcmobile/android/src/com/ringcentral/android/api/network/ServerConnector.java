/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api.network;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.api.AbstractRequestObject;
import com.ringcentral.android.api.SAXHelper;
import com.ringcentral.android.api.RequestInfo.AUTH_TYPE;
import com.ringcentral.android.api.parsers.Handler;

/**
 * NOTE: Re-use of connections shall not be used as first required certain Android approach and Android code had bugs so the re-use shall not be used at all.
 */
public class ServerConnector {
    // TODO : add checking for network state and go to offline mode if needed (aloenko) 
    private static final String KEEP_ALIVE = "Keep-Alive";
    private static final String CONNECTION = "Connection";
    private static final String SET_COOKIE = "set-cookie";
    private static final String TEXT_XML_CHARSET_UTF_8 = "text/xml; charset=utf-8";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String COOKIE = "Cookie";

    private static final String TAG = "[RC] ServerConnector";
    private static final boolean DEBUG = false;

    /**
     * Execute request with predefined parameters.
     *
     * @param url  Request URL
     * @param body Request body
     * @return HttpResponse
     */
    public static HttpResponse makeRequest(String url, String body) {
        if (url == null || body == null || url.length() <= 0) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "wrong params at makeRequest: url = " + (url == null ? "null" : url)
                        + ", body = " + (body == null ? "null" : body));
            }
            return null;
        }
        try {
            return executeHttpRequest(body, url, AUTH_TYPE.NONE);
        } catch (Exception e) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "makeRequest()", e);
            } else if (LogSettings.MARKET) {
                MktLog.e(TAG, "makeRequest() " + e.getMessage());
            }
            return null;
        }
    }

    /**
     * Execute request with predefined and tuned AbstractRequestObject.
     * Then parse server response via SAXHelper.
     *
     * @param reqObject AbstractRequestObject with needed parameters
     * @return Object (as SOAP server answer)
     */
    public static Object makeRequest(AbstractRequestObject reqObject) {
        Object result = null;
        InputStream responseIS = null;
        HttpResponse response = null;
        try {
            response = executeHttpRequest(
                    reqObject.getRequestBody().getBody(),
                    reqObject.getRequestInfo().getUrl(),
                    reqObject.getRequestInfo().getAuthType());
            
            if( response == null )
            	return result;
            
            responseIS = response.getEntity().getContent();

            try {
                Class handler = reqObject.getRequestInfo().getHandler();
                Handler handlerInstance;

                if (handler != null) {
                    handlerInstance = (Handler) handler.newInstance();
                    try {
                        result = SAXHelper.parseResponse(responseIS, handlerInstance, reqObject.getRequestInfo().requestShortName, reqObject.traceResponseSOAP);
                    } catch (Exception e) {
                        result = e;
                        if (LogSettings.MARKET) {
                            MktLog.w(TAG, "makeRequest()", e);
                        } else if (LogSettings.MARKET) {
                            MktLog.e(TAG, "makeRequest() " + e.getMessage());
                        }
                    }
                } else {
                    result = null;
                }

            } catch (Exception e) {
                result = e;
                if (LogSettings.ENGINEERING) {
                    EngLog.e(TAG, "makeRequest()", e);
                } else if (LogSettings.MARKET) {
                    MktLog.e(TAG, "makeRequest() " + e.getMessage());
                }
            }
        } catch (IOException e) {
            result = e;
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "makeRequest()", e);
            } else if (LogSettings.MARKET) {
                MktLog.e(TAG, "makeRequest() " + e.getMessage());
            }
        } catch (Exception e) {
            result = e;
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "makeRequest()", e);
            } else if (LogSettings.MARKET) {
                MktLog.e(TAG, "makeRequest() " + e.getMessage());
            }
        } finally {
            if (responseIS != null) {
                try {
                    responseIS.close();
                    responseIS = null;
                } catch (java.lang.Throwable tr1) {
                }
            }
            
            if (response != null) {
                try {
                    response.getEntity().consumeContent();
                    response = null;
                } catch (java.lang.Throwable tr1) {
                }
            }
        }
        return result;
    }

    /**
     * Execute request via Apache HttpClient with ThreadSafeClientConnManager.
     *
     * @param body      Request body
     * @param urlString Server URL
     * @param authType  (may be NONE, GET_COOKIE, SET_COOKIE)
     * @return HttpResponse
     * @throws IOException If Cookies not found.
     */
    private static HttpResponse executeHttpRequest(final String body, final String urlString, final AUTH_TYPE authType)
            throws IOException {

        HttpClient client = HttpClientFactory.getHttpClient();
        HttpEntity entity = new StringEntity(body);

        final HttpPost post = new HttpPost(urlString);
        post.addHeader(CONTENT_TYPE, TEXT_XML_CHARSET_UTF_8);
        post.addHeader(CONNECTION, KEEP_ALIVE);

        if (NetworkManager.getInstance().getCookie() != null && authType == AUTH_TYPE.SET_COOKIE) {
            if (LogSettings.ENGINEERING && DEBUG) {
                EngLog.w(TAG, "executeHttpRequest with Cookie = " + NetworkManager.getInstance().getCookie());
            }
            post.addHeader(COOKIE, NetworkManager.getInstance().getCookie());
        }
        post.setEntity(entity);

        HttpResponse response;
        try {
            response = client.execute(post);
            if (LogSettings.ENGINEERING) {
                EngLog.d(TAG, response == null ? "HttpResponse after request = null" : "HttpResponse after request: " + String.valueOf(response.getStatusLine().getStatusCode()));
            }
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                if (authType == AUTH_TYPE.GET_COOKIE) {
                    Header[] cookieHeader = response.getAllHeaders();
                    boolean cookies = false;
                    if (cookieHeader != null) {
                        for (Header header : cookieHeader) {
                            if (DEBUG && LogSettings.ENGINEERING) {
                                EngLog.d(TAG, "headers : " + header.getName() + " : " + header.getValue());
                            }
                            if (header.getName().equalsIgnoreCase(SET_COOKIE)) {
                                NetworkManager.getInstance().setCookie(header.getValue());
                                cookies = true;
                            }
                        }
                    }
                    if (!cookies && authType == AUTH_TYPE.SET_COOKIE) {
                        if (client != null) {
                            try {
                                client.getConnectionManager().shutdown();
                            } catch (Exception e) {
                                if (LogSettings.ENGINEERING) {
                                    EngLog.e(TAG, "executeHttpRequest(...) shutdown exception", e);
                                }
                            }
                        }
                        throw new IOException("Cookie not found!");
                    }
                }
            } else {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "HttpResponse code: " + response.getStatusLine().getStatusCode());
                }
            }
        } catch (Exception e) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "HttpClient Exception", e);
            }
            return null;
        }
        return response;
    }

}
