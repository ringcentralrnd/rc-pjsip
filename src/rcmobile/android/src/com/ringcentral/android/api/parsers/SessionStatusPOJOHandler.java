/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.ringcentral.android.api.pojo.SessionStatusPOJO;

public class SessionStatusPOJOHandler extends Handler<SessionStatusPOJO> {

	private static final String CALL_STATE= "callState";
	
	private static final String PARTY_STATUS = "partyStatus";
	
	private static final String USER_STATUS ="userStatus";
	
    private static final String SESSION_ID = "sessionId";
	
	
	public SessionStatusPOJOHandler() {
		super();
		SessionStatusPOJO sessionStatusPOJO = new SessionStatusPOJO();
		setPOJO(sessionStatusPOJO);
	}

	
	public void startElement(String uri, String name, String qName,
			Attributes atts) {
		super.startElement(uri, name, qName, atts);
	}

	public void endElement(String uri, String name, String qName)
			throws SAXException {
		super.endElement(uri, name, qName);
		if (getElementName(name, qName).equals(CALL_STATE)) {
			getPOJO().setCallState(getBuffer().toString());
		} else if (getElementName(name, qName).equals(PARTY_STATUS)) {
			getPOJO().setPartyStatus(getBuffer().toString());
		} else if (getElementName(name, qName).equals(USER_STATUS)) {
			getPOJO().setUserStatus(getBuffer().toString());
		} else if (getElementName(name, qName).equals(SESSION_ID)) {
			getPOJO().setSessionId(Long.parseLong(getBuffer().toString()));
		}
    }

	public void characters(char chars[], int start, int length) {
		getBuffer().append(chars, start, length);
	}


	@Override
	public Object getData() {
		return getPOJO();
	}


	
}
