/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.api.pojo;

import java.util.Date;

import android.text.TextUtils;
import android.text.format.DateUtils;

import com.ringcentral.android.contacts.ContactPhoneNumber;
import com.ringcentral.android.utils.PhoneUtils;

public class MessagePOJO {

    private static final String TAG = "[RC]MessagePOJO";

    private static final String EMPTY = "";
	private static final String NAME_UNKNOWN = "Unknown";
	
	public static final String VOICEMAIL = "Voicemail";
	public static final String FAX = "Fax";
	public static final String MESSAGE = "Message";	

	public static final int UNREAD = 0;
	public static final int READ = 1;

	public static final int DELETED_LOCALLY_FALSE = 0;
    public static final int DELETED_LOCALLY_TRUE  = 1;
	
	public enum MessageType {
		ANY(0), VOICE(1), FAX(2), GENERIC(3), CALL(4), EXT(5);

		public static MessageType findByOrdinal(int ordinal) {
			for (MessageType item : values()) {
				if (item.ordinal() == ordinal) {
					return item;
				}
			}
			return ANY;
		}
		
		public String getLabel() {
			switch (this) {
			case VOICE:
				return MessagePOJO.VOICEMAIL;
			case FAX:
				return MessagePOJO.FAX;
			}
			return MessagePOJO.MESSAGE;
		}

		
		private final int id;

		MessageType(int id) {
			this.id = id;
		}

		public int getValue() {
			return id;
		}

	}

	public enum CompType {
		UNDEF(0), GSM(1), WAVE(2), AIFF(3), GIF(4), TIFF(5), R0Z(6);

		private final int id;

		CompType(int id) {
			this.id = id;
		}

		public int getValue() {
			return id;
		}

		public static CompType findByOrdinal(int ordinal) {
			for (CompType item : values()) {
				if (item.ordinal() == ordinal) {
					return item;
				}
			}
			return UNDEF;
		}
	}

	/*
	 * 
	 */
	private String textDate = EMPTY;
	private int deletedLocally;
	private int readStatusChangedLocally;
	
	private String computedLabel;
	private int readStatus;
	private int syncStatus;
	private CompType 	compType;
	private String 		address;
	private Date 		dateTime;
	private long 		id = 0;
	private MessageType messageType = MessageType.VOICE;
	private String 		text;

	private byte[] body;

	private String 		fromName = "";
	private String 		fromPhone;
	private boolean     isFromPhoneValidNumber;
	private String      fromPhoneNormalized;
	private String      fromPhoneCanonic;
	private String      fromPhoneLocalCanonic;
	private String 		toName;
	private String 		toPhone;
	
	
	private long 		timeInMillis;
	private String 		fileExtension;
	private int 		duration;
	private long 		m_body_size = 0;
	private boolean		mIsBinded;
	private boolean 	mIsPersonalContact;
	private long 		mBindId;
	private String		mBindOriginalNumber;
	private String		mBindDisplayName;
	private int     	mDurationSyncState = 0;

	/**
	 * @return the body_size
	 */
	public long getBody_size() {
		return m_body_size;
	}

	/**
	 * @param bodySize the body_size to set
	 */
	public void setBody_size(long bodySize) {
		m_body_size = bodySize;
	}

	/*
	 * Message ID MUST present always.
	 */
	public boolean isValid(){
		return ( id > 0 );
	}
	
	public int getReadStatusChangedLocally() {
		return readStatusChangedLocally;
	}

	public void setReadStatusChangedLocally(int readStatusChangedLocally) {
		this.readStatusChangedLocally = readStatusChangedLocally;
	}

	public int getSyncStatus() {
		return syncStatus;
	}

	public void setSyncStatus(int syncStatus) {
		this.syncStatus = syncStatus;
	}
	
	public CompType getCompType() {
		return compType;
	}

	public void setCompType(CompType compType) {
		this.compType = compType;
	}

	
	public int getDeletedLocally() {
		return deletedLocally;
	}

	public void setDeletedLocally(int deletedLocally) {
		this.deletedLocally = deletedLocally;
	}

	public int getReadStatus() {
		return readStatus;
	}

	public void setReadStatus(int readStatus) {
		this.readStatus = readStatus;		
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public String getFromName() {
	    if (fromName == null) {
	        return "";
	    }
		return fromName;
	}
	
    /**
     * Returns from name label for the record. 
     * If the from name is not valid, trying to check personal contacts, if no phone number will be returned
     * 
     * @param localCanonic
     *            defines what kind of number form shall be return in case of
     *            not valid name
     * 
     * @return from name label
     */
	public String getFromNameOrNumber(boolean localCanonic) {

	    if (isBinded()) {
	    	return getBindDisplayName();
	    }
	    
	    if (isFromNameValid()) {
	        return getFromName();
	    }	    
	    
	    String name = null;
	    name = (localCanonic) ? getFromPhoneLocalCanonic() : getFromPhoneCanonic(); 	    
	    if(TextUtils.isEmpty(name)){
	    	name = NAME_UNKNOWN;	    	
	    }
	    
	    return name;
    }
	
	public boolean isFromNameValid() {
	    if (fromName == null) {
	        return false;
	    }
	    
	    if (fromName.trim().length() > 0) {
	        return true;
	    }
	    return false;
    }
	

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

    public String getFromPhone() {
        return fromPhone;
    }

    public String getFromPhoneNormalized() {
        return fromPhoneNormalized;
    }

    public String getFromPhoneCanonic() {
        return fromPhoneCanonic;
    }

    public String getFromPhoneLocalCanonic() {
        return fromPhoneLocalCanonic;
    }

	public void setFromPhone(String fromPhone) {
	    this.fromPhone = fromPhone;
	    ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumberWithoutTrim(fromPhone);
        if (!cpn.isValid) {
            cpn = PhoneUtils.getContactPhoneNumber(fromPhone);    
        }
        if (cpn != null && cpn.isValid) {
            fromPhoneNormalized = cpn.normalizedNumber;
            fromPhoneCanonic = cpn.phoneNumber.canonical;
            fromPhoneLocalCanonic = cpn.phoneNumber.localCanonical;
        } else {
            fromPhoneNormalized = fromPhone;
            fromPhoneCanonic = fromPhone;
            fromPhoneLocalCanonic = fromPhone;
        }
	}

	public String getToName() {
		return toName;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	public String getToPhone() {
		return toPhone;
	}

	public void setToPhone(String toPhone) {
		this.toPhone = toPhone;
	}

	public long getTimeInMillis() {
		return timeInMillis;
	}

	public void setTimeInMillis(long timeInMillis) {
		setDateTime(new Date(timeInMillis));
		this.timeInMillis = timeInMillis;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public byte[] getBody() {
		return body;
	}

	public void setBody(byte[] body) {
		this.body = body;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if ((obj == null) || (obj.getClass() != this.getClass())) {
			return false;
		}
		MessagePOJO msg = (MessagePOJO) obj;
		return this.getId() == msg.getId();

	}

	// TODO implement this method! hashCode should return custom value
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	public String getTextDate() {
		if (textDate == EMPTY) {
			if (dateTime != null) {
				// Set the date/time field by mixing relative and absolute
				// times.
				int flags = DateUtils.FORMAT_ABBREV_RELATIVE;
				long date = dateTime.getTime();

				textDate = DateUtils.getRelativeTimeSpanString(date,
						System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS,
						flags).toString();
				// textDate = DateFormat.getInstance().format(dateTime); // old
				// style
			}
		}
		return this.textDate;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getDuration() {
		return duration;
	}
	
	public String getComputedLabel() {
		return computedLabel;
	}

	public void setComputedLabel(String computedLabel) {
		this.computedLabel = computedLabel;
	}
	
	public void setBinded(boolean binded){
		this.mIsBinded = binded;
	}
	
	public boolean isBinded(){
		return mIsBinded;
	}
	
	public void setBindDisplayName(String bindDisplayName){
		this.mBindDisplayName = bindDisplayName;
	}
	
	public String getBindDisplayName(){
		return mBindDisplayName;
	}
		
    public boolean isBindPersonalContact() {
		return mIsPersonalContact;
	}

	public void setBindPersonalContact(boolean isPersonalContact) {
		this.mIsPersonalContact = isPersonalContact;
	}

	public long getBindId() {
		return mBindId;
	}

	public void setBindId(long bindId) {
		this.mBindId = bindId;
	}

	public String getBindOriginalNumber() {
		return mBindOriginalNumber;
	}

	public void setmBindOriginalNumber(String bindOriginalNumber) {
		this.mBindOriginalNumber = bindOriginalNumber;
	}

	public int getDurationSyncState() {
		return mDurationSyncState;
	}
	
	public void setDurationSyncState(int state){
		mDurationSyncState = state;
	}
}
