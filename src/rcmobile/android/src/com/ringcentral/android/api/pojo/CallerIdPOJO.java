package com.ringcentral.android.api.pojo;


public class CallerIdPOJO extends BasePOJO{

    private String number;

    private String usageType;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }
}
