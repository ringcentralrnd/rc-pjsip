/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.search;

import java.util.SortedSet;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;

import com.ringcentral.android.contacts.Cont;
import com.ringcentral.android.phoneparser.PhoneNumberFormatter;

@SuppressWarnings("deprecation")
public class ContactsSearchAgent1x extends ContactsSearchAgent {

    private PhoneNumberFormatter mFormatter = new PhoneNumberFormatter();
    
    private ContactsSearchAgent1x() {}
    private static class SingletonHolder { 
        public static final ContactsSearchAgent1x sInstance = new ContactsSearchAgent1x();
    }
    public static ContactsSearchAgent getInstance() {
        return SingletonHolder.sInstance;
    }
    
//-----------------------------------------------------------------------------
    
    @Override
    void searchAndStorePersonalContacts(Context context, String what, SortedSet<SearchItem> results) {
        searchAndStoreByName(context, what, results);
        searchAndStoreByNumber(context, what, results);
        if (PhoneNumberUtils.isISODigit(what.charAt(0))) {
            searchAndStoreByNumber(context, '+' + what, results);
        }
    }  

//-----------------------------------------------------------------------------

    private static final String[] SEARCH_CONTACT_NAME_PROJECTION = new String[] {
        android.provider.Contacts.People._ID,
        android.provider.Contacts.People.DISPLAY_NAME };
    
    private void searchAndStoreByName(Context context, String what, SortedSet<SearchItem> results) {
        Cursor cursor = context.getContentResolver().query(
                Uri.withAppendedPath(android.provider.Contacts.People.CONTENT_FILTER_URI, Uri.encode(what)),
                SEARCH_CONTACT_NAME_PROJECTION,
                null, null, null);
        
        if (cursor == null) {
            return;
        }
        if (cursor.getCount() <= 0) {
            cursor.close();
            return;
        }

        int column_id = cursor.getColumnIndex(android.provider.Contacts.People._ID);
        int column_name = cursor.getColumnIndex(android.provider.Contacts.People.DISPLAY_NAME);
        
        cursor.moveToFirst();
        do {
            getAndStorePhoneNumbersForContact(context,
                    cursor.getLong(column_id), cursor.getString(column_name), results);
        } while (cursor.moveToNext());
        
        cursor.close();
    }

//-----------------------------------------------------------------------------
    
    private static final String[] GET_CONTACT_PHONES_PROJECTION = new String[] {
        android.provider.Contacts.People.Phones.NUMBER,
        android.provider.Contacts.People.Phones.TYPE };

    private void getAndStorePhoneNumbersForContact(Context context,
            long contactID, String contactName, SortedSet<SearchItem> results) {
        Cursor cursor = context.getContentResolver().query(
                Uri.withAppendedPath(
                        ContentUris.withAppendedId(android.provider.Contacts.People.CONTENT_URI, contactID),
                        android.provider.Contacts.People.Phones.CONTENT_DIRECTORY),
                GET_CONTACT_PHONES_PROJECTION,
                null, null, null);
        
        if (cursor == null) {
            return;
        }
        if (cursor.getCount() <= 0) {
            cursor.close();
            return;
        }

        int column_number = cursor.getColumnIndex(android.provider.Contacts.People.Phones.NUMBER);
        int column_type = cursor.getColumnIndex(android.provider.Contacts.People.Phones.TYPE);

        String number;
        int type;

        cursor.moveToFirst();
        do {
            number = mFormatter.getFormatted(cursor.getString(column_number));
            if (TextUtils.isEmpty(number)) {
                continue;
            }
            type = cursor.getInt(column_type);

            results.add(new SearchItem (
                    SearchItem.ANDROID_CONTACT,
                    contactName,
                    number,
                    type,
                    Cont.acts().getPhoneNumberTag(context, type) + ": "));
        } while (cursor.moveToNext());
        
        cursor.close();
   }

//-----------------------------------------------------------------------------
    
    private static final String[] SEARCH_NUMBER_PROJECTION = new String[] {
        android.provider.Contacts.Phones.NUMBER,
        android.provider.Contacts.Phones.TYPE,
        android.provider.Contacts.Phones.DISPLAY_NAME };

    private static final String SEARCH_NUMBER_SELECTION_1 =
        android.provider.Contacts.Phones.NUMBER + " LIKE ? || '%'";         //NUMBER: "The phone number as the user entered it"

    private static final String SEARCH_NUMBER_SELECTION_2 =
                   android.provider.Contacts.Phones.NUMBER + " LIKE ? || '%'" +         //NUMBER: "The phone number as the user entered it"
          " OR " + android.provider.Contacts.Phones.NUMBER_KEY + " LIKE '%' || ?";      //NUMBER_KEY: "The normalized phone number": stripped (only digits and + sign) and REVERSED


    private void searchAndStoreByNumber(Context context, String what, SortedSet<SearchItem> results) {
        String selection;
        String[] selectionArgs;

        String what_stripped = PhoneNumberUtils.getStrippedReversed(what);
        if (!TextUtils.isEmpty(what_stripped)) {
            selection = SEARCH_NUMBER_SELECTION_2;
            selectionArgs = new String[] {what, what_stripped};
        } else {
            selection = SEARCH_NUMBER_SELECTION_1;
            selectionArgs = new String[] {what};
        }

        Cursor cursor = context.getContentResolver().query(
                android.provider.Contacts.Phones.CONTENT_URI,
                SEARCH_NUMBER_PROJECTION,
                selection,
                selectionArgs,
                null);

        if (cursor == null) {
            return;
        }
        if (cursor.getCount() <= 0) {
            cursor.close();
            return;
        }
    
        int column_name = cursor.getColumnIndex(android.provider.Contacts.Phones.DISPLAY_NAME);
        int column_number = cursor.getColumnIndex(android.provider.Contacts.Phones.NUMBER);
        int column_type = cursor.getColumnIndex(android.provider.Contacts.Phones.TYPE);
        
        String name;
        String number;
        int type;

        cursor.moveToFirst();
        do {
            number = mFormatter.getFormatted(cursor.getString(column_number));
            if (TextUtils.isEmpty(number)) {
                continue;
            }

            type = cursor.getInt(column_type);
            name = cursor.getString(column_name);
                
            results.add(new SearchItem (
                    SearchItem.ANDROID_CONTACT,
                    name,
                    number,
                    type,
                    Cont.acts().getPhoneNumberTag(context, type) + ": "));
        } while (cursor.moveToNext());
        
        cursor.close();
    }
        
}
