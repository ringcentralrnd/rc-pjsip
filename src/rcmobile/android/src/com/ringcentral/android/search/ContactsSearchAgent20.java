/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.search;

import java.util.SortedSet;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;

import com.ringcentral.android.contacts.Cont;
import com.ringcentral.android.phoneparser.PhoneNumberFormatter;

public class ContactsSearchAgent20 extends ContactsSearchAgent {

    private PhoneNumberFormatter mFormatter = new PhoneNumberFormatter();
    
    private ContactsSearchAgent20() {}
    private static class SingletonHolder { 
        public static final ContactsSearchAgent20 sInstance = new ContactsSearchAgent20();
    }
    public static ContactsSearchAgent getInstance() {
        return SingletonHolder.sInstance;
    }


    private static final String[] PROJECTION = new String[] {
        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
        ContactsContract.CommonDataKinds.Phone.NUMBER,
        ContactsContract.CommonDataKinds.Phone.TYPE };

    
    @Override
    void searchAndStorePersonalContacts(Context context, String what, SortedSet<SearchItem> results) {
        searchAndStore(context, what, results);
        if (PhoneNumberUtils.isISODigit(what.charAt(0))) {
            searchAndStore(context, '+' + what, results);
        }
    }  

        
    private void searchAndStore(Context context, String what, SortedSet<SearchItem> results) {
        Cursor cursor = context.getContentResolver().query(
                Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI, Uri.encode(what)),
                PROJECTION,
                null, null, null);

        if (cursor == null) {
            return;
        }
        if (cursor.getCount() <= 0) {
            cursor.close();
            return;
        }

        int column_name = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int column_number = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        int column_type = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);
        
        String name; 
        String number;
        int type;
        
        cursor.moveToFirst();
        do {
            number = mFormatter.getFormatted(cursor.getString(column_number));
            if (TextUtils.isEmpty(number)) {
                continue;
            }
            
            name = cursor.getString(column_name);
            type = cursor.getInt(column_type);

            results.add (new SearchItem(
                    SearchItem.ANDROID_CONTACT,
                    name,
                    number,
                    type,
                    Cont.acts().getPhoneNumberTag(context, type) + ": "));
        } while (cursor.moveToNext());
        
        cursor.close();
    }

}
