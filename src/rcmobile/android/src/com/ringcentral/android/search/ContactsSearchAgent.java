/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.search;

import java.util.SortedSet;

import android.content.Context;

public abstract class ContactsSearchAgent {

    abstract void searchAndStorePersonalContacts(Context context, String what, SortedSet<SearchItem> results);

}
