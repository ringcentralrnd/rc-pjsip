/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.search;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.contacts.Cont;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.provider.RCMDataStore.ExtensionsTable;

public class SearchProvider extends ContentProvider {

    private static final String TAG = "[RC]SearchProvider";
    
    private static final String AUTHORITY = "com.ringcentral.android.search.searchprovider";
    static final Uri SEARCH_URI = Uri.parse("content://"+ AUTHORITY);

    private ContactsSearchAgent mContactsSearchAgent;
    private TreeSet<SearchItem> mResultsSet;

    
    @Override
    public boolean onCreate() {
        mContactsSearchAgent = Cont.acts().getContactsSearchAgent();
        mResultsSet = new TreeSet<SearchItem>(new SearchItem.ItemComparator());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String what = uri.getLastPathSegment();

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "Search for \"" + what + "\"...");
        }
        
        mResultsSet.clear();
        searchAndStoreExtensions(what, mResultsSet);
        mContactsSearchAgent.searchAndStorePersonalContacts(getContext(), what, mResultsSet);

        MatrixCursor cursor = new MatrixCursor(SearchItem.COLUMN_NAMES);
        Iterator<SearchItem> iterator = mResultsSet.iterator();
        SearchItem item;
        int id = 0;
        while (iterator.hasNext()) {
            item = iterator.next();
            item.setID(id++);
            cursor.addRow(item.getColumns());
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

//-----------------------------------------------------------------------------
    
    private static final String[] EXT_QUERY_PROJECTION = new String[] {
        ExtensionsTable.RCM_DISPLAY_NAME,
        ExtensionsTable.JEDI_PIN };
    
    private static final String EXT_QUERY_SELECTION =
                 ExtensionsTable.JEDI_FIRST_NAME  + " LIKE ? || '%'" +
        " OR " + ExtensionsTable.JEDI_LAST_NAME   + " LIKE ? || '%'" +
        " OR " + ExtensionsTable.JEDI_MIDDLE_NAME + " LIKE ? || '%'" +
        " OR " + ExtensionsTable.RCM_DISPLAY_NAME + " LIKE ? || '%'" +
        " OR " + ExtensionsTable.JEDI_PIN         + " LIKE ? || '%'";

    
    private void searchAndStoreExtensions(String what, SortedSet<SearchItem> results) {
        Context context = getContext();
        
        Cursor cursor = context.getContentResolver().query(
                UriHelper.getUri(RCMProvider.EXTENSIONS, RCMProviderHelper.getCurrentMailboxId(context)),
                EXT_QUERY_PROJECTION,
                EXT_QUERY_SELECTION,
                new String[] {what, what, what, what, what},
                null);
        
        if (cursor == null) {
            return;
        }
        
        if (cursor.getCount() <= 0) {
            cursor.close();
            return;
        }
        
        int column_name = cursor.getColumnIndex(ExtensionsTable.RCM_DISPLAY_NAME);
        int column_pin  = cursor.getColumnIndex(ExtensionsTable.JEDI_PIN);

        cursor.moveToFirst();
        do {
            results.add (new SearchItem(
                    SearchItem.RC_EXTENSION,
                    cursor.getString(column_name),
                    cursor.getString(column_pin),
                    0,      //dummy value: phone type is not applicable for extensions
                    context.getString(R.string.phone_tag_extension) + ": "));
        } while (cursor.moveToNext());
        
        cursor.close();
    }

}
