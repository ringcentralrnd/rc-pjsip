/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.search;

import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.ringout.RingOutAgent;
import com.ringcentral.android.statistics.FlurryTypes;

public class SearchActivity extends ListActivity {

    private static final String TAG = "[RC]SearchActivity";
    
    private static final int MENU_ITEM_NEW_SEARCH = 1;

    private SearchResultsAdapter adapter;
    private RingOutAgent mRingOutAgent; 
    
    @Override
    protected void onStart() {
    	super.onStart();
    	FlurryTypes.onStartSession(this);
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	FlurryTypes.onEndSession(this);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRingOutAgent = new RingOutAgent(this);

        Intent intent = getIntent();
        if (intent.getAction().equals(RCMConstants.ACTION_RINGOUT)) {
            String number = intent.getDataString();
            
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "onCreate(): ACTION_RINGOUT: Start RO Agent: " + number);
            }
            mRingOutAgent.call(number, null);

            finish();
            return;
        }
        
        if (!intent.getAction().equals(Intent.ACTION_SEARCH)) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "onCreate(): Invalid ACTION: " + intent.getAction() + "; finish");
            }
            finish();
            return;
        }

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onCreate(): ACTION_SEARCH");
        }
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);           
        setContentView(R.layout.search_list);
        ((TextView) findViewById(R.id.emptyListText)).setText(R.string.noMatchingContacts);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String number = ((TextView) view.findViewById(R.id.data)).getText().toString();
                String name = ((TextView) view.findViewById(R.id.name)).getText().toString();

                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "User pressed item at position " + position + "; start RO Agent: " + number);
                }
                mRingOutAgent.call(number, name);
            }
        });

        adapter = new SearchResultsAdapter(this, null);
        handleSearchIntent(intent);
        setListAdapter(adapter);
    }
    
    @Override
    protected void onNewIntent(Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SEARCH)) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "onNewIntent(): ACTION_SEARCH");
            }

            setIntent(intent);
            handleSearchIntent(intent);
        } else if (intent.getAction().equals(RCMConstants.ACTION_RINGOUT)) {
            String number = intent.getDataString();

            if (LogSettings.MARKET) {
                MktLog.i(TAG, "onNewIntent(): ACTION_RINGOUT: Start RO Agent: " + number);
            }
            mRingOutAgent.call(number, null);
        }
    }
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        
        menu.add(Menu.NONE, MENU_ITEM_NEW_SEARCH, Menu.FIRST, R.string.menu_item_new_search)
            .setIcon(android.R.drawable.ic_menu_search);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
        case MENU_ITEM_NEW_SEARCH:
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "User pressed 'New Search' menu btn");
            }
            onSearchRequested();
            return true;
        default:
            return true;
        }
    }

    
    private void handleSearchIntent(Intent intent) {
        String what = intent.getStringExtra(SearchManager.QUERY);
        if (TextUtils.isEmpty(what)) {
            finish();
        }
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "handleSearchIntent(): Search for \"" + what + "\"...");
        }

        Cursor cursor = getContentResolver().query(
                Uri.withAppendedPath(SearchProvider.SEARCH_URI, Uri.encode(what)), null, null, null, null);
        adapter.changeCursor(cursor);
    }

    
    private class SearchResultsAdapter extends CursorAdapter {

        private SearchResultsAdapter(Context context, Cursor cursor) {
            super(context, cursor);
        }
        
        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View view = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.contacts_list_item_photo_search, null);
            view.findViewById(R.id.photo).setVisibility(View.GONE);
            view.findViewById(R.id.presence).setVisibility(View.GONE);
            
            bindView(view, context, cursor);
            return view;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            ((TextView) view.findViewById(R.id.name)).setText(cursor.getString(SearchItem.COLUMN_NAME));
            ((TextView) view.findViewById(R.id.label)).setText(cursor.getString(SearchItem.COLUMN_LABEL));
            ((TextView) view.findViewById(R.id.data)).setText(cursor.getString(SearchItem.COLUMN_NUMBER));
        }
    }



}
