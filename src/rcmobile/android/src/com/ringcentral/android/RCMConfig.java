/** 
 * Copyright (C) 2010-2012 RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.utils.RCClientInfoCollector;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.AppConfigTable;

/**
 * Configuration/settings class. This class holds information about all the parameters which affect RCM for Android.
 */
@SuppressWarnings("deprecation")
public final class RCMConfig {
	private static final String TAG = "[RC]RCMConfig";

	public static final String URI_PARAM_HASH = "hash";
	public static final String URI_PARAM_OUTBOUND_LINK = "ol";
	
	/*
	 * According to URI standards fragment part(after #sign) of URI should follow parameters part
	 * Uri.Builder works as expected(as described), but mobile web side works expect fragment part to 
	 * prevent parameters part and in this case forward user either to error page or to web login scree,
	 * so will use concatenation instead of builder.
	 * 
	 * */
	private static final String WEB_SETTIONS_COMMON_PARAMS = "appUrlScheme=%s&hash=%s";
	
	private static final String WEB_SETTINGS_COMPANY = 					"#main?screen=%22phoneSystem%22&";
	private static final String WEB_SETTINGS_BILLING = 					"#billing/main?";
	private static final String WEB_SETTINGS_UPGRADE = 					"#billing/upgrade?";
	private static final String WEB_SETTINGS_INCOMING_CALL = 			"#main?screen=%22myInbound%22&";
	private static final String WEB_SETTINGS_TELL_FRIEND_URL = 			"#tellAFriend/main?";
	private static final String WEB_SETTINGS_FAX = 						"#faxSettings?";
	private static final String WEB_SETTINGS_COMPANY_FOR_RCFAX = 		"#main?screen=%22phoneSystemFaxAccount%22&";
	private static final String WEB_SETTINGS_ACCOUNT_INFO_FOR_RCFAX = 	"#mailboxInfo?";	
	
	private static String  cfg_JEDI_URL;
	private static String  cfg_MSGSYNC_URL;
	private static String  cfg_SIGNUP_URL;
	private static Boolean cfg_SETUP_URL_OVERRIDING_ENABLED;
	private static String  cfg_SETUP_URL;
	private static String  cfg_SETUP_SIGNUP_SETTINGS_URL;
	private static String  cfg_WEB_SETTINGS_BASE_URL;
	private static String  cfg_HTTPREG_URL;

    /**
     * Default VoIP settings
     * 
     * SIP is enabled
     * 
     * WiFi is enabled
     * 3G/4G is disabled
     * Incoming calls is disabled
     */

	/*
	 * #DK: Temporally added SIP_SETTINGS_SIP_FLAG_HTTPREG_ENABLED & SIP_SETTINGS_ON_ENVIRONMENT_ENABLED
	 * This values really should be dedicated through HTTP registration and GetAccountInfo call 
	 */
    /*
     * #MB, 2011-11-15, AB-202/AB-258: Removed SIP_SETTINGS_ON_ENVIRONMENT_ENABLED
     * It is now set at login according to JEDI Authenticator:getAPIVersion value 
     */


//    public static final long VOIP_DEFAULT_SETTINGS = (RCMConstants.SIP_SETTINGS_USER_VOIP_ENABLED | RCMConstants.SIP_SETTINGS_USER_WIFI_ENABLED
//    // | RCMConstants.SIP_SETTINGS_SIP_FLAG_HTTPREG_ENABLED
//    // | RCMConstants.SIP_SETTINGS_USER_INCOMING_CALLS_ENABLED
//    // | RCMConstants.SIP_SETTINGS_ON_ENVIRONMENT_ENABLED
//    // | RCMConstants.SIP_SETTINGS_ON_ACCOUNT_ENABLED
//    );

	public static final long VOIP_DEFAULT_SETTINGS;
	static {       //calculate VOIP_DEFAULT_SETTINGS per brand properties
        long voip_default_settings = 0;
        if (BUILD.VOIP_ENABLED) {
            voip_default_settings = RCMConstants.SIP_SETTINGS_USER_WIFI_ENABLED;

            if (BUILD.VOIP_ENABLED_BY_DEFAULT) {
                voip_default_settings |= RCMConstants.SIP_SETTINGS_USER_VOIP_ENABLED;
            }
            
            if (BUILD.VOIP_MOBILE_3G_4G_ENABLED && BUILD.VOIP_MOBILE_3G_4G_ENABLED_BY_DEFAULT) {
                voip_default_settings |= RCMConstants.SIP_SETTINGS_USER_3G_4G_ENABLED;
            }

            if (BUILD.VOIP_INCOMING_ENABLED && BUILD.VOIP_INCOMING_ENABLED_BY_DEFAULT) {
                voip_default_settings |= RCMConstants.SIP_SETTINGS_USER_INCOMING_CALLS_ENABLED;
            }
        }
        
        VOIP_DEFAULT_SETTINGS = voip_default_settings;
    }
    
    
    
	/*
	 * JEDI_URL
	 */
	public static String get_JEDI_URL(Context context) {
		if (cfg_JEDI_URL == null) {
			final String value = RCMProviderHelper.simpleQuery(context, RCMProvider.APP_CONFIG, AppConfigTable.URL_JEDI);
			if (TextUtils.isEmpty(value)) {
				set_JEDI_URL(context, BUILD.URI.JEDI);
			} else {
				cfg_JEDI_URL = value;
			}
		}
		return cfg_JEDI_URL;
	}
    
    public static boolean set_JEDI_URL(Context context, String value){
    	if(LogSettings.ENGINEERING){
    		EngLog.d(TAG, "set_JEDI_URL() current : " + cfg_JEDI_URL + " new : " + value);
    	}
    	if((value != null)&&(!value.equals(cfg_JEDI_URL))){
    		cfg_JEDI_URL = value;
    		return RCMProviderHelper.updateSingleValue(context, RCMProvider.APP_CONFIG, AppConfigTable.URL_JEDI, value, null) > 0;
    	}else{
    		return false;
    	}    	    	    	        
    }
    
    
    /*
     * MSGSYNC_URL
     */
    public static String get_MSGSYNC_URL(Context context){
    	if(cfg_MSGSYNC_URL == null){
    		final String value = RCMProviderHelper.simpleQuery(context, RCMProvider.APP_CONFIG, AppConfigTable.URL_MSGSYNC);
			if (TextUtils.isEmpty(value)) {
				set_MSGSYNC_URL(context, BUILD.URI.MSGSYNC);
			} else {
				cfg_MSGSYNC_URL = value;
			}
    	}
    	return cfg_MSGSYNC_URL;
    }
    
    public static boolean set_MSGSYNC_URL(Context context, String value){
    	if(LogSettings.ENGINEERING){
    		EngLog.d(TAG, "set_MSGSYNC_URL() current : " + cfg_MSGSYNC_URL + " new : " + value);
    	}
    	if((value != null)&&(!value.equals(cfg_MSGSYNC_URL))){
    		cfg_MSGSYNC_URL = value;
    		return RCMProviderHelper.updateSingleValue(context, RCMProvider.APP_CONFIG, AppConfigTable.URL_MSGSYNC, value, null) > 0;    	
    	}else{
    		return false;
    	}
    }
    
    
    /*
     * SIGNUP_URL
     */
    public static Uri get_SIGNUP_URL(Context context, boolean appendParameters){
    	if(cfg_SIGNUP_URL == null){
    		final String value = RCMProviderHelper.simpleQuery(context, RCMProvider.APP_CONFIG, AppConfigTable.URL_SIGNUP);
			if (TextUtils.isEmpty(value)) {
				set_SIGNUP_URL(context, BUILD.URI.SIGNUP);
			} else {
				cfg_SIGNUP_URL = value;
			}
    	}    	    	    	    
    	Uri uri = Uri.parse(cfg_SIGNUP_URL);
    	
    	if(appendParameters && BUILD.isOutboundLinkWithEncryptedDataTurnedOn){
    		return uri.buildUpon()
    				  .appendQueryParameter(URI_PARAM_OUTBOUND_LINK, RCClientInfoCollector.getEncryptedPhoneData(context))
    				  .build();
    	}
    	return uri;
    }
    
    public static boolean set_SIGNUP_URL(Context context, String value){
    	if(LogSettings.ENGINEERING){
    		EngLog.d(TAG, "set_SIGNUP_URL() current : " + cfg_SIGNUP_URL + " new : " + value);
    	}
    	if((value != null)&&(!value.equals(cfg_SIGNUP_URL))){
    		cfg_SIGNUP_URL = value;
    		return RCMProviderHelper.updateSingleValue(context, RCMProvider.APP_CONFIG, AppConfigTable.URL_SIGNUP, value, null) > 0;
    	}else{
    		return false;
    	}
    }
    
    
    /*
     * SETUP_URL        
     */    
    public static Uri get_SETUP_URL(Context context, boolean appendParameters){
    	if(cfg_SETUP_URL == null){
    		final String value = RCMProviderHelper.simpleQuery(context, RCMProvider.APP_CONFIG, AppConfigTable.URL_SETUP);
			if (TextUtils.isEmpty(value)) {
				set_SETUP_URL(context, BUILD.URI.SETUP);
			} else {
				cfg_SETUP_URL = value;
			}
    	}
    	Uri uri = Uri.parse(cfg_SETUP_URL);
    	
    	if(appendParameters){
    		Uri.Builder builder = uri.buildUpon().appendQueryParameter(URI_PARAM_HASH, RCMProviderHelper.getLoginHash(context));
    		if(BUILD.isOutboundLinkWithEncryptedDataTurnedOn) {
    			builder.appendQueryParameter(URI_PARAM_OUTBOUND_LINK, RCClientInfoCollector.getEncryptedPhoneData(context));
    		}
    		return builder.build();
    	}    	    	
    	return uri;
    } 
    
    public static boolean set_SETUP_URL(Context context, String value){
    	if(LogSettings.ENGINEERING){
    		EngLog.d(TAG, "set_SETUP_URL() current : " + cfg_SETUP_URL + " new : " + value);
    	}
    	if((value != null)&&(!value.equals(cfg_SETUP_URL))){
    		cfg_SETUP_URL = value;
    		return RCMProviderHelper.updateSingleValue(context, RCMProvider.APP_CONFIG, AppConfigTable.URL_SETUP, value, null) > 0;
    	}else{
    		return false;
    	}    		
    }
    
    
    /*
     * SETUP_URL_OVERRIDING_ENABLED
     */    
    public static boolean is_SETUP_URL_OVERRIDING_ENABLED(Context context){
    	if(cfg_SETUP_URL_OVERRIDING_ENABLED == null){
    		final String value = RCMProviderHelper.simpleQuery(context, RCMProvider.APP_CONFIG, AppConfigTable.IS_SETUP_OVERLOAD_ENABLED);   
    		if (TextUtils.isEmpty(value)) {
    			set_SETUP_URL_OVERRIDING_ENABLED(context, BUILD.URI.SETUP_URL_OVERRIDING_ENABLE);
			} else {
				cfg_SETUP_URL_OVERRIDING_ENABLED = (Integer.valueOf(value) > 0);
			}
    	}
    	return cfg_SETUP_URL_OVERRIDING_ENABLED;
    }
    
    public static boolean set_SETUP_URL_OVERRIDING_ENABLED(Context context, boolean value){
    	if(LogSettings.ENGINEERING){
    		EngLog.d(TAG, "set_SETUP_URL_OVERRIDING_ENABLED() current : " + cfg_SETUP_URL_OVERRIDING_ENABLED + " new : " + value);
    	}
    	if(!Boolean.valueOf(value).equals(cfg_SETUP_URL_OVERRIDING_ENABLED)){
    		cfg_SETUP_URL_OVERRIDING_ENABLED = value;
    		return RCMProviderHelper.updateSingleValue(context, RCMProvider.APP_CONFIG, AppConfigTable.IS_SETUP_OVERLOAD_ENABLED, String.valueOf(value ? 1 : 0), null) > 0;
    	}else{
    		return false;
    	}
    }
    
    
    /*
     * SETUP_SIGNUP_SETTINGS_URL
     */
    public static String get_SETUP_SIGNUP_SETTINGS_URL(Context context){
    	if(cfg_SETUP_SIGNUP_SETTINGS_URL == null){    		
    		final String value = RCMProviderHelper.simpleQuery(context, RCMProvider.APP_CONFIG, AppConfigTable.URL_SETUP_SIGNUP);   
    		if (TextUtils.isEmpty(value)) {
    			set_SETUP_SIGNUP_SETTINGS_URL(context, BUILD.URI.SETUP_SIGNUP_SETTINGS);
			} else {
				cfg_SETUP_SIGNUP_SETTINGS_URL = value;
			}
    	}
    	return cfg_SETUP_SIGNUP_SETTINGS_URL;
    }
    
    public static boolean set_SETUP_SIGNUP_SETTINGS_URL(Context context, String value){
    	if(LogSettings.ENGINEERING){
    		EngLog.d(TAG, "set_SETUP_SIGNUP_SETTINGS_URL() current : " + cfg_SETUP_SIGNUP_SETTINGS_URL + " new : " + value);
    	}
    	if((value != null)&&(!value.equals(cfg_SETUP_SIGNUP_SETTINGS_URL))){
    		cfg_SETUP_SIGNUP_SETTINGS_URL = value;
    		return RCMProviderHelper.updateSingleValue(context, RCMProvider.APP_CONFIG, AppConfigTable.URL_SETUP_SIGNUP, value, null) > 0;
    	}else{
    		return false;
    	}
    }       
    
    
    /*
     * WEB_SETTINGS_BASE_URL
     */    
    public static String get_WEB_SETTINGS_BASE_URL(Context context){
    	if(cfg_WEB_SETTINGS_BASE_URL == null){
    		final String value = RCMProviderHelper.simpleQuery(context, RCMProvider.APP_CONFIG, AppConfigTable.URL_WEBSETTING_BASE);
    		if (TextUtils.isEmpty(value)) {
    			set_WEB_SETTINGS_BASE_URL(context, BUILD.URI.WEB_SETTINGS_BASE);
			} else {
				cfg_WEB_SETTINGS_BASE_URL = value;
			}
    	}
    	return cfg_WEB_SETTINGS_BASE_URL;
    }
    
    public static boolean set_WEB_SETTINGS_BASE_URL(Context context, String value){
    	if(LogSettings.ENGINEERING){
    		EngLog.d(TAG, "set_WEB_SETTINGS_BASE_URL() current : " + cfg_WEB_SETTINGS_BASE_URL + " new : " + value);
    	}
    	if((value != null)&&(!value.equals(cfg_WEB_SETTINGS_BASE_URL))){
    		cfg_WEB_SETTINGS_BASE_URL = value;
    		return RCMProviderHelper.updateSingleValue(context, RCMProvider.APP_CONFIG, AppConfigTable.URL_WEBSETTING_BASE, value, null) > 0;
    	}else{
    		return false;
    	}
    }
    
    public static String get_WEB_SETTINGS_UPGRADE_URL(Context context){    	
    	return get_WEB_SETTINGS_BASE_URL(context) + WEB_SETTINGS_UPGRADE + getUrlParameters(context);
    }
    
    public static String get_WEB_SETTINGS_COMPANY_URL(Context context){
    	return get_WEB_SETTINGS_BASE_URL(context) + WEB_SETTINGS_COMPANY + getUrlParameters(context);
    }
    
    public static String get_WEB_SETTINGS_BILLING_URL(Context context){
    	return get_WEB_SETTINGS_BASE_URL(context) + WEB_SETTINGS_BILLING + getUrlParameters(context);
    }
    
    public static String get_WEB_SETTINGS_INCOMING_CALLS_URL(Context context){
    	return get_WEB_SETTINGS_BASE_URL(context) + WEB_SETTINGS_INCOMING_CALL + getUrlParameters(context);
    }
    
    public static String get_WEB_SETTINGS_TELL_FRIEND_URL(Context context){
    	return get_WEB_SETTINGS_BASE_URL(context) + WEB_SETTINGS_TELL_FRIEND_URL + getUrlParameters(context);
    }
    
    public static String get_WEB_SETTINGS_FAX_URL(Context context){
    	return get_WEB_SETTINGS_BASE_URL(context) + WEB_SETTINGS_FAX + getUrlParameters(context);
    }
    
    public static String get_WEB_SETTINGS_COMPANY_FOR_RCFAX_URL(Context context){
        return get_WEB_SETTINGS_BASE_URL(context) + WEB_SETTINGS_COMPANY_FOR_RCFAX + getUrlParameters(context);
    }
    
    public static String get_WEB_SETTINGS_ACCOUNT_INFO_FOR_RCFAX_URL(Context context){
        return get_WEB_SETTINGS_BASE_URL(context) + WEB_SETTINGS_ACCOUNT_INFO_FOR_RCFAX + getUrlParameters(context);
    }
    
    private static String getUrlParameters(Context context){
    	return String.format(WEB_SETTIONS_COMMON_PARAMS, BUILD.APP_URL_SCHEME, RCMProviderHelper.getLoginHash(context));
    }
   
    
   /*
    * HTTPREG_URL 
    */
    public static String get_HTTPREG_URL(Context context, boolean alwaysFromDB ){
    	if(cfg_HTTPREG_URL == null || alwaysFromDB ){    		
    		final String value = RCMProviderHelper.simpleQuery(context, RCMProvider.APP_CONFIG, AppConfigTable.URL_HTTPREG);   
    		if (TextUtils.isEmpty(value)) {
    			set_HTTPREG_URL(context, BUILD.URI.HTTPREG);
			} else {
				cfg_HTTPREG_URL = value;
			}
    	}

    	if(LogSettings.ENGINEERING){
    		EngLog.d(TAG, "get_HTTPREG_URL() Value: " + cfg_HTTPREG_URL );
    	}
    	return cfg_HTTPREG_URL;
    }
    
    public static boolean set_HTTPREG_URL(Context context, String value){
    	if(LogSettings.ENGINEERING){
    		EngLog.d(TAG, "set_HTTPREG_URL() current : " + cfg_HTTPREG_URL + " new : " + value);
    	}
    	if((value != null)&&(!value.equals(cfg_HTTPREG_URL))){
    		cfg_HTTPREG_URL = value;
    		return RCMProviderHelper.updateSingleValue(context, RCMProvider.APP_CONFIG, AppConfigTable.URL_HTTPREG, value, null) > 0;
    	}else{
    		return false;
    	}
    }
      
     
    
    public static final void setDefaultConfiguration(Context context){
    	if (LogSettings.ENGINEERING) {
            EngLog.i(TAG, "setDefaultConfiguration(), reverting to default");
        }
    	set_JEDI_URL(context, BUILD.URI.JEDI);
    	set_MSGSYNC_URL(context, BUILD.URI.MSGSYNC);
    	set_SIGNUP_URL(context, BUILD.URI.SIGNUP);
    	set_SETUP_URL(context, BUILD.URI.SETUP);
    	set_SETUP_URL_OVERRIDING_ENABLED(context, BUILD.URI.SETUP_URL_OVERRIDING_ENABLE);
    	set_SETUP_SIGNUP_SETTINGS_URL(context, BUILD.URI.SETUP_SIGNUP_SETTINGS);
    	set_WEB_SETTINGS_BASE_URL(context, BUILD.URI.WEB_SETTINGS_BASE);
    	set_HTTPREG_URL(context, BUILD.URI.HTTPREG);
    } 

      /**
       * checkConfigRevision
       * 
       * @param context
       * @return true when changed(need to rollback to default)
       */
      public static boolean checkConfigRevision(Context context){    	
      	final String value = RCMProviderHelper.simpleQuery(context, RCMProvider.APP_CONFIG, AppConfigTable.CONFIG_REVISION);
      	final int dbRevision = TextUtils.isEmpty(value)? -1 : Integer.valueOf(value);
      	
      	if(LogSettings.MARKET){
      		MktLog.d(TAG, "Current revision : " + BUILD.CFG_REVISION + ", DB revision : " + dbRevision);
      	}
      	    	
      	if (BUILD.CFG_REVISION > dbRevision) { // revert to default
      		RCMProviderHelper.updateSingleValue(context, RCMProvider.APP_CONFIG, AppConfigTable.CONFIG_REVISION, String.valueOf(BUILD.CFG_REVISION), null);
      		setDefaultConfiguration(context);
      		return true;
      	}
      	return false;
      }   

    
    public static void dumpConfig(){
    	if(LogSettings.MARKET){
    		MktLog.i(TAG, "JEDI URL : " 				      + cfg_JEDI_URL);
    		MktLog.i(TAG, "MSGSYNC URL : " 				      + cfg_MSGSYNC_URL);
    		MktLog.i(TAG, "SIGNUP URL : " 				      + cfg_SIGNUP_URL);
    		MktLog.i(TAG, "SETUP_URL_OVERRIDING_ENABLED : "   + cfg_SETUP_URL_OVERRIDING_ENABLED); 
    		MktLog.i(TAG, "SETUP URL : " 				      + cfg_SETUP_URL);
    		MktLog.i(TAG, "SETUP_SIGNUP_SETTINGS URL : "      + cfg_SETUP_SIGNUP_SETTINGS_URL);
    		MktLog.i(TAG, "WEB_SETTINGS_BASE URL : "          + cfg_WEB_SETTINGS_BASE_URL);
    		MktLog.i(TAG, "HTTPREG URL : " 				      + cfg_HTTPREG_URL);
    	}
    }
}
