/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android;

/**
 * Global RCM constants and definitions.
 */

public final class RCMConstants {

    /**
     * /* RingOut constants
     */
    public static final int RINGOUT_MODE_MY_ANDROID = 0;
    public static final int RINGOUT_MODE_ANOTHER_PHONE = 1;

    public static final int RINGOUT_RESULT_OK = 1;
    public static final int RINGOUT_RESULT_FAIL = 2;
    public static final int RINGOUT_RESULT_CANCELLED = 3;

    /**
     * Internal permission name. See Android Manifest.
     */
    public static final String INTERNAL_PERMISSION_NAME = "com.ringcentral.android.INTERNAL_PERMISSION";

    /**
     * Action constants for RCM Intents
     */
    public static final String ACTION_RESTART_APP = "com.ringcentral.android.intent.action.RESTART_APP";
    public static final String ACTION_LOGIN_FINISHED = "com.ringcentral.android.intent.action.LOGIN_FINISHED";
    public static final String ACTION_RINGOUT = "com.ringcentral.android.intent.action.RINGOUT";
    public static final String ACTION_RINGOUT_VIEW = "com.ringcentral.android.intent.action.RINGOUT_VIEW";
    public static final String ACTION_NETWORK_REQUEST_END = "com.ringcentral.android.intent.action.NETWORK_REQUEST_END";
    public static final String ACTION_NETWORK_STATE_CHANGED = "com.ringcentral.android.intent.action.NETWORK_STATE_CHANGED";
    public static final String ACTION_DND_STATUS_CHANGED = "com.ringcentral.android.intent.action.DND_STATUS_CHANGED";
    public static final String ACTION_UPDATE_MESSAGES_LIST = "com.ringcentral.android.intent.action.UPDATE_MESSAGES_LIST";
    public static final String ACTION_UPDATE_MESSAGE_DETAIL_VIEW = "com.ringcentral.android.intent.action.UPDATE_MESSAGE_DETAIL_VIEW";
    public static final String ACTION_MESSAGES_MOD_COUNTER_CHANGED = "com.ringcentral.android.intent.action.MESSAGES_MOD_COUNTER_CHANGED";
    public static final String ACTION_EXTENSIONS_MOD_COUNTER_CHANGED = "com.ringcentral.android.intent.action.EXTENSIONS_MOD_COUNTER_CHANGED";
    public static final String ACTION_MESSAGES_BODY_IS_DOWNLOADED = "com.ringcentral.android.intent.action.MESSAGES_BODY_IS_DOWNLOADED";
    public static final String ACTION_UPDATE_LOGS_LIST = "com.ringcentral.android.intent.action.UPDATE_LOGS_LIST";
    public static final String ACTION_CURRENT_TAB_CHANGED = "com.ringcentral.android.intent.action.CURRENT_TAB_CHANGED";
    public static final String ACTION_LIST_RECENT_CALL = "com.ringcentral.android.intent.action.RECENT_CALLS";
    public static final String ACTION_LIST_CONTACTS = "com.ringcentral.android.intent.action.LIST_CONTACTS";
    public static final String ACTION_LIST_FAVORITES = "com.ringcentral.android.intent.action.LIST_FAVORITES";

    public static final String ACTION_LIST_EXTENSIONS = "com.ringcentral.android.intent.action.LIST_EXTENSIONS";
    public static final String ACTION_LIST_EXTENSIONS_FAV = "com.ringcentral.android.intent.action.LIST_EXTENSIONS_FAV";
    public static final String ACTION_LIST_MESSAGES = "com.ringcentral.android.intent.action.LIST_MESSAGES";
    public static final String ACTION_LIST_SEARCH = "com.ringcentral.android.intent.action.LIST_SEARCH";
    public static final String ACTION_CONFIGURATION_CHANGED = "com.ringcentral.android.intent.action.ACTION_CONFIGURATION_CHANGED";
    public static final String ACTION_HTTP_REGISTERED = "com.ringcentral.android.intent.action.HTTP_REGISTERED";
    public static final String ACTION_RC_APP_STARTED = "com.ringcentral.android.intent.action.RC_APP_STARTED";
    public static final String ACTION_VOIP_CONFIGURATION_CHANGED = "com.ringcentral.android.intent.action.VOIP_CONFIGURATION_CHANGED";
    public static final String ACTION_VOIP_STATE_CHANGED = "com.ringcentral.android.intent.action.VOIP_STATE_CHANGED";
    public static final String ACTION_UPDATE_MESSAGE_INDICATOR = "com.ringcentral.android.intent.action.UPDATE_MESSAGE_INDICATOR";
    public static final String ACTION_VOIP_CALLS_NUMBER_CHANGED = "com.ringcentral.android.intent.action.VOIP_CALLS_NUMBER_CHANGED";
    public static final String ACTION_VOIP_NOTIFY_INCOMING_CALL = "com.ringcentral.android.intent.action.VOIP_NOTIFY_INCOMING_CALL";
    public static final String ACTION_VOIP_STOP_NOTIFY_INCOMING_CALL = "com.ringcentral.android.intent.action.VOIP_STOP_NOTIFY_INCOMING_CALL";
    public static final String ACTION_VOIP_ECHO_CANCELATION_STATE_CHANGED = "com.ringcentral.android.intent.action.VOIP_ECHO_CANCELATION_STATE_CHANGED";
    public static final String ACTION_VOIP_RING_STARTED = "com.ringcentral.android.intent.action.VOIP_RING_STARTED";
    public static final String ACTION_INVOIP_RINGTONE_STARTED = "com.ringcentral.android.intent.action.INVOIP_RINGTONE_STARTED";
    public static final String ACTION_INVOIP_ACTIVITY_STARTED = "com.ringcentral.android.intent.action.INVOIP_ACTIVITY_STARTED";
    public static final String ACTION_INVOIP_ANSWER = "com.ringcentral.android.intent.action.INVOIP_ANSWER";
    public static final String ACTION_INVOIP_ANSWER_AND_HOLD = "com.ringcentral.android.intent.action.INVOIP_ANSWER_AND_HOLD";
    public static final String ACTION_CALL_STATE_ACTIVITY_STARTED = "com.ringcentral.android.intent.action.CALL_STATE_ACTIVITY_STARTED";
    public static final String ACTION_CLOSE_RINGOUT = "com.ringcentral.android.intent.action.CLOSE_RINGOUT_SCREEN";
    
    /**
     * On phone parser station location change, see
     * {@link #INTERNAL_PERMISSION_NAME}.
     * 
     * Parameters are declared in PhoneUtils.
     * 
     */
    public static final String ACTION_STATION_LOCATION_CHANGE = "com.ringcentral.android.intent.action.STATION_LOCATION_CHANGE";

    /**
     * Extra data keys for RCM Intents
     */
    public static final String EXTRA_NEW_CONFIG = "com.ringcentral.android.intent.extra.NEW_CONFIG";
    public static final String EXTRA_RESTART_APP = "com.ringcentral.android.intent.extra.RESTART_APPLICATION";
    public static final String EXTRA_CALL_FROM_NUMBER = "com.ringcentral.android.intent.extra.CALL_FROM_NUMBER";
    public static final String EXTRA_CALL_TO_NUMBER = "com.ringcentral.android.intent.extra.CALL_TO_NUMBER";
    public static final String EXTRA_CALL_TO_NAME = "com.ringcentral.android.intent.extra.CALL_TO_NAME";
    public static final String EXTRA_MESSAGE_ID = "com.ringcentral.android.intent.extra.MESSAGE_ID";
    public static final String EXTRA_MESSAGE_POSITION = "com.ringcentral.android.intent.extra.MESSAGE_POSITION";
    public static final String EXTRA_MESSAGES_COUNT = "com.ringcentral.android.intent.extra.MESSAGES_COUNT";
    public static final String EXTRA_MESSAGES_SORT_ORDER = "com.ringcentral.android.intent.extra.MESSAGES_SORT_ORDER";
    public static final String EXTRA_MESSAGES_LIST_MODE = "com.ringcentral.android.intent.extra.MESSAGES_LIST_MODE";
    public static final String EXTRA_SETTINGS5 = "com.ringcentral.android.intent.extra.SETTINGS5";
    public static final String EXTRA_SHOW_DEVICE_NUMBER_DIALOG_AND_FINISH = "com.ringcentral.android.intent.extra.EXTRA_SHOW_DEVICE_NUMBER_DIALOG_AND_FINISH";
    public static final String EXTRA_VOIP_INCOMING_CALL_INFO = "com.ringcentral.android.intent.extra.VOIP_INCOMING_CALL_INFO";
    public static final String EXTRA_VOIP_INCOMING_CALL_INFO_COUNT = "com.ringcentral.android.intent.extra.VOIP_INCOMING_CALL_INFO_CALL_COUNT";
    public static final String EXTRA_VOIP_INCOMING_CALL_START_TIME = "com.ringcentral.android.intent.extra.VOIP_INCOMING_CALL_START_TIME";
    public static final String EXTRA_VOIP_CONFIGURATION_STATE_CHANGED = "com.ringcentral.android.intent.extra.VOIP_CONF_STATE_CHANGED";
    public static final String EXTRA_VOIP_STATE_CHANGED = "com.ringcentral.android.intent.extra.VOIP_STATE_CHANGED";
    public static final String EXTRA_VOIP_CALLS_NUMBER_CHANGED_TOTAL = "com.ringcentral.android.intent.extra.VOIP_CALLS_NUMBER_CHANGED_TOTAL";
    public static final String EXTRA_VOIP_CALLS_NUMBER_CHANGED_ON_HOLD = "com.ringcentral.android.intent.extra.VOIP_CALLS_NUMBER_CHANGED_ON_HOLD";
    public static final String EXTRA_VOIP_ECHO_CANCELATION_STATE_CHANGED = "com.ringcentral.android.intent.extra.VOIP_CONF_ECHO_STATE_CHANGED";
    public static final String EXTRA_VOIP_OUTGOING_CALL_ID = "com.ringcentral.android.intent.extra.VOIP_OUTGOING_CALL_ID";
    public static final String EXTRA_VOIP_CALL_TYPE = "com.ringcentral.android.intent.extra.VOIP_CALL_TYPE";
    public static final String EXTRA_VOIP_CALL_ACTION = "com.ringcentral.android.intent.extra.VOIP_CALL_ACTION";
    public static final String EXTRA_VOIP_CALL_INFO = "com.ringcentral.android.intent.extra.VOIP_CALL_INFO";
    public static final String EXTRA_VOIP_RING_STARTED = "com.ringcentral.android.intent.extra.VOIP_RING_STARTED";
    public static final String EXTRA_INVOIP_ACTIVITY_STARTED = "com.ringcentral.android.intent.extra.INVOIP_ACTIVITY_STARTED";
    public static final String EXTRA_INVOIP_ANSWER_TIME = "com.ringcentral.android.intent.extra.INVOIP_ANSWER_TIME";
    public static final String EXTRA_CALL_STATE_ACTIVITY_STARTED = "com.ringcentral.android.intent.extra.CALL_STATE_ACTIVITY_STARTED";
    public static final String EXTRA_INVOIP_ANSWER_AND_HOLD_TIME = "com.ringcentral.android.intent.extra.INVOIP_ANSWER_AND_HOLD_TIME";

    /**
     * Request codes for startActivityForResult(Intent intent, int requestCode)
     */
    public static final int ACTIVITY_REQUEST_CODE_RINGOUT_CALL = 100;

    /**
     * Notification identifiers
     */
    public static final int NOTIFICATION_ID_RCM_STATUS = 1;
    public static final int NOTIFICATION_ID_UNREAD_MESSAGES = 2;

    /**
     * User account tier settings
     */
    public static final long TIERS_PHS_VER_AUTO_UPGRDADE = 0x00000000001L;
    public static final long TIERS_PHS_SHOW_ADS = 0x00000000002L;
    public static final long TIERS_PHS_SENDVOICEMAIL = 0x00000000004L;
    public static final long TIERS_PHS_REPLY = 0x00000000008L;
    public static final long TIERS_PHS_TAKECALL = 0x00000000010L;
    public static final long TIERS_PHS_REJECT = 0x00000000020L;
    public static final long TIERS_PHS_SHOW0MINLEFT = 0x00000000040L;
    public static final long TIERS_PHS_SHOWBUYMINUTES = 0x00000000080L;
    public static final long TIERS_PHS_SHOWUPGRADENOW = 0x00000000100L;
    public static final long TIERS_PHS_ALLOW_HIDE_T0 = 0x00000000200L;
    public static final long TIERS_PHS_NOT_ENHANCED = 0x00000000400L;
    public static final long TIERS_PHS_CALL_BACK = 0x00000000800L;
    public static final long TIERS_PHS_NTFY_TRIAL_END = 0x00000001000L;
    public static final long TIERS_PHS_SHOWVERIFICATION = 0x00000002000L;
    public static final long TIERS_PHS_PRIVATE_ACCESS_NUMBER = 0x00000004000L;
    public static final long TIERS_PHS_NOTIFY_MOBILE = 0x00000008000L;
    public static final long TIERS_PHS_CUSTOM_REPLY = 0x00000010000L;
    public static final long TIERS_PHS_SHOW_CALL_LOG = 0x00000020000L;
    public static final long TIERS_PHS_MC2_PERIOD_CHNG_ALLOWED = 0x00000040000L;
    public static final long TIERS_PHS_SHOW_FAQ = 0x00000080000L;
    public static final long TIERS_PHS_SHOW_SEND_FAX = 0x00000100000L;
    public static final long TIERS_PHS_HIDE_TAKECALL = 0x00000200000L;
    public static final long TIERS_PHS_HIDE_SENDVOICEMAIL = 0x00000400000L;
    public static final long TIERS_PHS_NOT_NOTIFIED_ON_NEW_CALL = 0x00000800000L;
    public static final long TIERS_PHS_CALLER_PREVIEW = 0x00001000000L;
    public static final long TIERS_PHS_DIAL_FROM_CLIENT = 0x00002000000L;
    public static final long TIERS_PHS_SHOW_WHATS_NEW = 0x00004000000L;
    public static final long TIERS_PHS_DISABLE_AB_SYNC = 0x00008000000L;
    public static final long TIERS_PHS_USE_CDW = 0x00010000000L;
    public static final long TIERS_PHS_ENABLE_IE_TOOLBAR = 0x00020000000L;
    public static final long TIERS_PHS_HOTKEY_DIAL = 0x00040000000L;
    public static final long TIERS_PHS_HOTKEY_FAX = 0x00080000000L;
    public static final long TIERS_PHS_DISABLE_LOCAL_STORAGE = 0x00100000000L;
    public static final long TIERS_PHS_MULTIPLE_EXTENSIONS = 0x00200000000L;
    public static final long TIERS_PHS_CAN_VIEW_EXTENSIONS = 0x00400000000L;
    public static final long TIERS_PHS_CAN_VIEW_QUEUEAGENT = 0x00800000000L;
    public static final long TIERS_PHS_INTERNATIONAL_CALLING = 0x01000000000L;
    public static final long TIERS_PHS_SYSTEM_EXTENSION = 0x02000000000L;
    public static final long TIERS_PHS_OPERATOR_EXTENSION = 0x04000000000L;
    public static final long TIERS_PHS_DO_NOT_DISTURB = 0x08000000000L;
    public static final long TIERS_PHS_CHANGE_CALLERID = 0x10000000000L;

    /*
     * SIP flags. Received during HTTP registration procedure.
     */
    public static final int SIPFLAGS_SHOW_ANSWER = 0x00000001;
    public static final int SIPFLAGS_SHOW_DIAL = 0x00000002;
    public static final int SIPFLAGS_VOIP_ENABLED = 0x00000004;
    public static final int SIPFLAGS_LINE_ASSIGNED = 0x00000008;
    public static final int SIPFLAGS_LINE_DISABLED = 0x00000010;
    public static final int SIPFLAGS_THIRDPARTYCALLS_ENABLED = 0x00000020;

    /**
     * Service
     */
    public static final int SERVICE_FIRST_RUN_AFTER_CREATING = 2 * 60 * 1000; // 2
									      // minutes
    public static final int SERVICE_INTERVAL = 10 * 60; // 10 minutes

    /**
     * Others
     */
    public static final int CALL_LOG_PAGE_SIZE = 25;
    public static final String FILE_PROTOCOL = "file://";

    /*
     * SIP settings
     */

    /**
     * Defines if VoIP enabled in Application VoIP Settings
     */
    public static final long SIP_SETTINGS_USER_VOIP_ENABLED = 0x00000001;

    /**
     * Defines if VoIP via WiFi is enabled in Application VoIP Settings
     */
    public static final long SIP_SETTINGS_USER_WIFI_ENABLED = 0x00000002;

    /**
     * Defines if VoIP via 3G/4G is enabled in Application VoIP Settings
     */
    public static final long SIP_SETTINGS_USER_3G_4G_ENABLED = 0x00000004;

    /**
     * Defines if VoIP incoming calls is enabled in Application VoIP Settings
     */
    public static final long SIP_SETTINGS_USER_INCOMING_CALLS_ENABLED = 0x00000008;

    /**
     * Defines if VoIP (SIP Flag in HTTPREG) is enabled for the account
     */
    public static final long SIP_SETTINGS_SIP_FLAG_HTTPREG_ENABLED = 0x00000010;

    /**
     * Defines if VoIP is enabled for the account per environment (disabled <
     * JEDI 5.x.x)
     */
    public static final long SIP_SETTINGS_ON_ENVIRONMENT_ENABLED = 0x00000020;

    /**
     * Defines if VoIP is enabled for the account AccountInfo.ServiceMask
     * TIERS_PHS_DIAL_FROM_CLIENT, later can be brand/tier.
     */
    public static final long SIP_SETTINGS_ON_ACCOUNT_ENABLED = 0x00000040;

    /*
     * VoIP status
     */
    public static final long VOIP_STATUS_VOIP_AVAILABLE = 0x00000001;
    public static final long VOIP_STATUS_VOIP_WIFI_AVAILABLE = 0x00000002;
    public static final long VOIP_STATUS_VOIP_3G_4G_AVAILABLE = 0x00000004;
    public static final long VOIP_STATUS_VOIP_NEW_AVAILABLE = 0x00000008;
    public static final long VOIP_STATUS_VOIP_INCOMING_AVAILABLE = 0x00000008;

    /*
     * VoIP call type (incoming/outgoing)
     */
    public static final int VOIP_CALL_TYPE_NONE = -1;
    public static final int VOIP_CALL_TYPE_OUTGOING = 1;
    public static final int VOIP_CALL_TYPE_INCOMING = 2;

    /*
     * VoIP user action for incoming call
     */
    public static final int VOIP_CALL_ACTION_NONE = -1;
    public static final int VOIP_CALL_ACTION_ANSWER = 1;
    public static final int VOIP_CALL_ACTION_ANSWER_AND_HOLD = 2;
    public static final int VOIP_CALL_ACTION_ANSWER_AND_HANGUP = 3;

    /*
     * VoIP calls handling delays (AB-445: workaround for UI performance
     * bottlenecks on slow phones)
     */
    public static final long VOIP_ANSWER_DELAY = 2000; // msec
    public static final long VOIP_START_CALL_DELAY = 2000; // msec

    /*
     * AEC default delay value
     */
    public static final int DEFAULT_AEC_DELAY_VALUE = 2560; // samples

    
    /*
     * for eventDetail
     */
    public static final String ISKNOWNCONTACT="isknowncontact";
    public static final String CALLLOGID="calllogid";
}
