/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.LoggingManager;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.api.RequestInfoStorage;
import com.ringcentral.android.api.httpreg.HttpRegisterExecutor;
import com.ringcentral.android.api.network.HttpClientFactory;
import com.ringcentral.android.api.network.NetworkManager;
import com.ringcentral.android.api.network.NetworkManagerNotifier;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.messages.Messages;
import com.ringcentral.android.messages.MessagesHandler;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.AccountInfoTable;
import com.ringcentral.android.provider.RCMDataStore.SyncStatusEnum;
import com.ringcentral.android.ringout.RingOut;
import com.ringcentral.android.ringout.RingOutAgent;
import com.ringcentral.android.settings.AudioSetupWizard;
import com.ringcentral.android.settings.ConfigSettingsActivity;
import com.ringcentral.android.settings.ToS911Dialog;
import com.ringcentral.android.settings.VoipWhatsNewDialog;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.DeviceUtils;
import com.ringcentral.android.utils.LoginInfo;
import com.ringcentral.android.utils.PhoneUtils;
import com.ringcentral.android.utils.UserInfo;
import com.ringcentral.android.utils.ui.LoginCredentialsEditor;
import com.ringcentral.android.utils.ui.widget.LoginErrorDialog;
import com.ringcentral.android.utils.ui.widget.LoginWaitingDialog;


public class LoginScreen extends Activity implements NetworkManagerNotifier, TextWatcher {

    private static final String TAG = "[RC]LoginScreen";
    
    private static final int ID_SPLASH_REQUEST = 1;
    
    private static final int WAITING_DIALOG = 1;
    private static final int ERROR_DIALOG = 2;
    private static final int SETUP_INCOMPLETE_DIALOG = 3;
    private static final int TRIAL_ADMIN_EXPIRED_DIALOG = 4;
    private static final int TRIAL_ADMIN_EXPIRING_DIALOG = 5;
    private static final int TRIAL_USER_EXPIRED_DIALOG = 6;
    private static final int TRIAL_NONE_DIALOG = 7; // not an actual dialog type, just looks like )   
    /**
     * Defines "ToS" dialog identifier.
     */
    private static final int SETUP_TOS_DIALOG = 8;
    /**
     * Defines "ToS 911" dialog identifier.
     */
    private static final int SETUP_TOS911_DIALOG = 9;
    
    private static final int VOIP_WHATS_NEW_DIALOG = 0xA; // ten! :)

    
    private boolean mLoginCancelledByUser;

    private NetworkManagerNotifier mNetworkManagerNotifier;
    private Message mHandlerMessage = null;
    
    private boolean mAudioSetupWizard = false;

    private boolean mIsTosEulaDialogActive = false;
    private boolean mIsTos911DialogActive = false;
    private boolean mHttpRegCompleted = false;
    
    private LoginCredentialsEditor mCredentialsEditor;

    private LoginInfo mLoginInfo;
    
    private EditText phoneField;
    private EditText extField;
    private EditText passwordField;
	private Button mLoginBtn;
    private Button mSignupBtn;
    
    /**
     *  HTTP REG STUFF, id for handler's messages & handler it self
     */
    public static final int MESSAGE_HTTP_REG_COMPLETED = 0x0;
    
    private Handler m_httpRegHandler = new Handler() {
        public void handleMessage(Message msg) {
            if(null == msg) {
                return;
            }
            
            if(LogSettings.MARKET) {
                MktLog.i(TAG, "m_httpRegHandler what: " + msg.what);
            }
            
            switch (msg.what) {
            case MESSAGE_HTTP_REG_COMPLETED:
                mHttpRegCompleted = true;
                checkAccountNotification(getApplicationContext(), NOTIFICATION_VOIP_WHATS_NEW);
                break;                
            default:
                break;
            }
        }
    };

    
    private static final int STATE_SPLASH = 0;
    private static final int STATE_TOS_EULA = 1;
    private static final int STATE_TOS_911 = 2;
    private static final int STATE_LOGIN = 3;
    //
    private void preLoginChecks(int state) {
    	
    	if (state == STATE_SPLASH) {
    		if(BUILD.SPLASH_SCREEN_ENABLED) {
    			if(!RingCentral.isAlive()) {
    				startActivityForResult(new Intent(getApplicationContext(), SplashScreenActivity.class), ID_SPLASH_REQUEST);    			
    				return;
    			}
    		}
    		state = STATE_TOS_EULA;
    	}
    	
    	if (state == STATE_TOS_EULA) {
    		if(BUILD.isTosEULAEnabled && !RCMProviderHelper.isTosAccepted(this) && (!mIsTosEulaDialogActive)) {    			
    			mIsTosEulaDialogActive = true;
    			showDialog(SETUP_TOS_DIALOG);
    			return;
    		}
    		state = STATE_TOS_911;
    	}
    	
    	if (state == STATE_TOS_911) {
    		if(BUILD.isTos911Enabled && !RCMProviderHelper.isTos911Executed(this) && (!mIsTos911DialogActive)) {
    			mIsTos911DialogActive = true;
    			showDialog(SETUP_TOS911_DIALOG);
    			return;
    		}
    		state = STATE_LOGIN;
    	}
    	
    	if (state == STATE_LOGIN) {
    		removeDialog(SETUP_TOS911_DIALOG);
    		if (!RingCentral.isAlive() || mLoginInfo.restart) {
            	autoLogin(mLoginInfo);
            }else{
            	Launcher.launch(this, getIntent());
            }
    	}    	    	    	        	
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                        
        if( null != savedInstanceState) {
        	mIsTosEulaDialogActive = savedInstanceState.getBoolean("isEULAActive");
        	mIsTos911DialogActive = savedInstanceState.getBoolean("is911Active");
        }
        
        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "onCreate() called with intent : " + getIntent());
        }
        
        mLoginCancelledByUser = false;        
        mNetworkManagerNotifier = this;
        
        mLoginInfo = LoginInfo.fillLoginInfo(this, getIntent());
        showLoginScreen(mLoginInfo.phone, mLoginInfo.ext, mLoginInfo.password);
                
        preLoginChecks(STATE_SPLASH);        
    }
    

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    	if (LogSettings.ENGINEERING) {
              EngLog.i(TAG, "onNewIntent() called with intent : " + intent);
        }    	
    	LoginInfo loginInfo = LoginInfo.fillLoginInfo(this, intent);

        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "onNewIntent : " + loginInfo);
        }
        showLoginScreen(loginInfo.phone, loginInfo.ext, loginInfo.password);
        
        if (mIsTosEulaDialogActive || mIsTos911DialogActive){
        	return;
        }
        //at this point TABs can not be present in backstack otherwise call through onCreate would be initiated
        autoLogin(loginInfo);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	preLoginChecks(STATE_TOS_EULA);
    }

	private void autoLogin(LoginInfo loginInfo) {
		if (loginInfo.autologin) {
    		if (LogSettings.MARKET) {
    			MktLog.d(TAG, "autoLogin(): Show waiting dialog");
    		}
    		showDialog(WAITING_DIALOG);
    		login(loginInfo.phone, loginInfo.ext, loginInfo.password);                
    	}
	}
    @Override
    protected void onDestroy() {
    	super.onDestroy();
        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "onDestroy()...");
        }        
        leakCleanUpRootView();
    }
    
    @Override
    protected void onStart() {
    	super.onStart();    	
    	FlurryTypes.onStartSession(this);
    	setEditFieldTypeAndLoginButton();
    }
    
    @Override
    protected void onStop() {
    	super.onStop();    	
    	FlurryTypes.onEndSession(this);
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	outState.putBoolean("isEULAActive", mIsTosEulaDialogActive);
    	outState.putBoolean("is911Active", mIsTos911DialogActive);
    }

    @Override
    public boolean onSearchRequested() {
        return false;   //disable search button
    }


    private void login(final String number, final String extension, final String password) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "Login initiating...");
        }
        NetworkManager.getInstance().login(LoginScreen.this, PhoneUtils.parsePhoneNum(number, false), extension, password, true);
    }

    private void getAccountInfo() {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "Getting account info ...");
        }
        NetworkManager.getInstance().getAccountInfo(LoginScreen.this, false, false, true);
    }

    private void getServiceApiVersion() {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "Getting service API version ...");
        }
        NetworkManager.getInstance().getApiVersion(LoginScreen.this, true);
    }

    private void showLoginScreen(final String phone, final String ext, final String password) {
        setContentView(R.layout.mainlogin);

        phoneField = (EditText) findViewById(R.id.phone);
        extField = (EditText) findViewById(R.id.extension);
        passwordField = (EditText) findViewById(R.id.password);
        mLoginBtn = (Button) findViewById(R.id.btnSignIn);
        mSignupBtn = (Button) findViewById(R.id.btnNew);
        
        phoneField.addTextChangedListener(this);
        extField.addTextChangedListener(this);
        passwordField.addTextChangedListener(this);
        
        mCredentialsEditor = new LoginCredentialsEditor(phoneField, extField, passwordField);
        if (mLoginInfo.autofill){
            mCredentialsEditor.init(phone, ext, password);
        } else {
            mCredentialsEditor.init("", "", "");
        }
        
        mLoginBtn.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                if (LogSettings.MARKET) {
                    QaLog.d(TAG, "User: Login button");
                }
                final String phone = phoneField.getText().toString();                
                
                if (LogSettings.ENGINEERING && LoggingManager.isItTestCase(phone)) {
				    if (LogSettings.MARKET) {
	                    QaLog.d(TAG, "User: Logging test case (panic)");
	                }
					LoggingManager.loggingTest();
				} else if (LoggingManager.itIsLoggerCase(phone)){
					if (LogSettings.MARKET) {
	                    QaLog.d(TAG, "User: Logging feature");
	                }
					startActivity(new Intent(getApplicationContext(), LoggingScreen.class));
				} else if (ConfigSettingsActivity.isEnabled(phone)){
					if (LogSettings.MARKET) {
	                    QaLog.d(TAG, "Configuration feature");
	                }
	            	startActivity(new Intent(LoginScreen.this, ConfigSettingsActivity.class));
	            } else  {
					final String password = passwordField.getText().toString();
					final String ext = extField.getText().toString();
					if (phone.length() == 0 || password.length() == 0) {
					    if (LogSettings.MARKET) {
		                    MktLog.d(TAG, "Phone number or password empty - warning dialog.");
		                }
						Toast.makeText(LoginScreen.this, getString(R.string.relogin_msg_nodata), 3000).show();
					} else {
					    try {
					        if (LogSettings.MARKET) {
	                            MktLog.i(TAG, "User entered: number " + phone + ", ext " + ext);
	                        }
					    } catch (java.lang.Throwable th) {
					    }
					    if (LogSettings.MARKET) {
				            QaLog.i(TAG, "Show waiting dialog...");
				        }
						showDialog(WAITING_DIALOG);
						login(phone, ext, password);
					}
				}
            }
        });

        mSignupBtn.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: I'm a new user");
                }
                launchSignup(LoginScreen.this);               
            }

        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case WAITING_DIALOG:
            LoginWaitingDialog lwd = new LoginWaitingDialog(this);
            lwd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    mLoginCancelledByUser = ((LoginWaitingDialog)dialog).cancelledByUser();
                    
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "Login progress dialog cancelled " +
                                (mLoginCancelledByUser ? "by user; restart app" : "(not by user)"));
                    }
                    
                    if (!mLoginCancelledByUser) {
                        return;
                    }

                    RingCentralApp.restartApplication(
                            RCMProviderHelper.getLoginNumber(LoginScreen.this),
                            RCMProviderHelper.getLoginExt(LoginScreen.this),
                            RCMProviderHelper.getLoginPassword(LoginScreen.this),
                            false,
                            false,
                            true,
                            LoginScreen.this);
                }
            });
            
            return lwd;

        case ERROR_DIALOG:
            return new LoginErrorDialog(this);
        case SETUP_INCOMPLETE_DIALOG:              	
        	return RcAlertDialog.getBuilder(this)		
				.setTitle(R.string.loginscreen_setup_dialog_title)
 		    	.setMessage(R.string.loginscreen_setup_dialog_msg)
 		    	.setCancelable(false)
 		    	.setPositiveButton(R.string.loginscreen_setup_dialog_yes, new DialogInterface.OnClickListener() {					
 		    		@Override
 		    		public void onClick(DialogInterface dialog, int which) {
 		    			if(LogSettings.MARKET){
 		    				MktLog.d(TAG, "SETUP_INCOMPLETE_DIALOG, User : Yes");
 		    			}
 		    			startSetupWizard(LoginScreen.this);
 		    		}
 		    	})
 		    	.setNeutralButton(R.string.loginscreen_setup_dialog_later, new DialogInterface.OnClickListener() {					
 		    		@Override
 		    		public void onClick(DialogInterface dialog, int which) {
 		    			if(LogSettings.MARKET){
 		    				MktLog.d(TAG, "SETUP_INCOMPLETE_DIALOG, User : Later : " + System.currentTimeMillis());
 		    			}
 		    			RCMProviderHelper.saveLastCompleteSetupRequest(LoginScreen.this, System.currentTimeMillis());
 		    			showDialog(WAITING_DIALOG);
 		    			checkAccountNotification(LoginScreen.this, NOTIFICATION_VOIP_WHATS_NEW);
 		    		}
 		    	})
 		    	.setNegativeButton(R.string.loginscreen_setup_dialog_no, new DialogInterface.OnClickListener() {					
 		    		@Override
 		    		public void onClick(DialogInterface dialog, int which) {
 		    			if(LogSettings.MARKET){
 		    				MktLog.d(TAG, "SETUP_INCOMPLETE_DIALOG, User : No");
 		    			}
 		    			NetworkManager.getInstance().setSetupWizardState(LoginScreen.this, RCMDataStore.AccountInfoTable.SETUP_WIZARD_STATE_COMPLETED, true);
 		    			showDialog(WAITING_DIALOG);
 		    			checkAccountNotification(LoginScreen.this, NOTIFICATION_VOIP_WHATS_NEW);
 		    		}
 		    	})
 		    	.create();
        case TRIAL_ADMIN_EXPIRED_DIALOG : 
        	return RcAlertDialog.getBuilder(this)
        		.setTitle(R.string.loginscreen_trial_dlg_title)
        		.setMessage(R.string.loginscreen_trial_dlg_msg_admin_expired)
        		.setCancelable(false)
        		.setNegativeButton(R.string.loginscreen_trial_dlg_cancel, new DialogInterface.OnClickListener() {					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(LogSettings.MARKET){
 		    				MktLog.d(TAG, "TRIAL_ADMIN_EXPIRED_DIALOG, User : Cancel");
 		    			}
						dialog.dismiss();						
					}
				})
				.setPositiveButton(R.string.loginscreen_trial_dlg_upgrade, new DialogInterface.OnClickListener() {					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(LogSettings.MARKET){
 		    				MktLog.d(TAG, "TRIAL_ADMIN_EXPIRED_DIALOG, User : startUpdateWizard");
 		    			}
						startUpdadeWizard(LoginScreen.this);						
					}
				})
				.create();
        case TRIAL_ADMIN_EXPIRING_DIALOG : 
        	return RcAlertDialog.getBuilder(this)
        		.setTitle(R.string.loginscreen_trial_dlg_title)
        		.setMessage(getExpiringMessage(LoginScreen.this))
        		.setCancelable(false)
        		.setNegativeButton(R.string.loginscreen_trial_dlg_cancel, new DialogInterface.OnClickListener() {					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(LogSettings.MARKET){
 		    				MktLog.d(TAG, "TRIAL_ADMIN_EXPIRING_DIALOG, User : Cancel");
 		    			}
						RCMProviderHelper.saveLastExpirationReminder(LoginScreen.this, System.currentTimeMillis());
						checkAccountNotification(LoginScreen.this, NOTIFICATION_SETUP);
					}
				})
				.setPositiveButton(R.string.loginscreen_trial_dlg_upgrade, new DialogInterface.OnClickListener() {					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(LogSettings.MARKET){
 		    				MktLog.d(TAG, "TRIAL_ADMIN_EXPIRING_DIALOG, User : startUpdateWizard");
 		    			}
						RCMProviderHelper.saveLastExpirationReminder(LoginScreen.this, System.currentTimeMillis());
						startUpdadeWizard(LoginScreen.this);						
					}
				})
				.create();
        case TRIAL_USER_EXPIRED_DIALOG : 
        	return RcAlertDialog.getBuilder(this)
	    		.setTitle(R.string.loginscreen_trial_dlg_title)
	    		.setMessage(R.string.loginscreen_trial_dlg_msg_user_expired)
	    		.setCancelable(false)
				.setPositiveButton(R.string.loginscreen_trial_dlg_ok, new DialogInterface.OnClickListener() {					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(LogSettings.MARKET){
 		    				MktLog.d(TAG, "TRIAL_USER_EXPIRED_DIALOG, User : ok");
 		    			}
						dialog.dismiss();						
					}
				})
				.create();
        case TRIAL_NONE_DIALOG: // if will be called for some reason sometimes
        	return null;
        case SETUP_TOS_DIALOG:
            if (BUILD.isTosEULAEnabled){
                return RcAlertDialog.getBuilder(this)
                .setTitle(R.string.tos_title)
                .setMessage(R.string.tos_text)
                .setIcon(R.drawable.symbol_exclamation)
                .setCancelable(false)
                .setPositiveButton(R.string.tos_accept_button_title, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {                        
						RCMProviderHelper.setTosAccepted(LoginScreen.this, true);
                        mIsTosEulaDialogActive = false;
                        preLoginChecks(STATE_TOS_911);
                    }
                })
                .setNegativeButton(R.string.tos_decline_button_title, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        
						RCMProviderHelper.setTosAccepted(LoginScreen.this, false);
                        mIsTosEulaDialogActive = false;
                        if (LogSettings.MARKET) {
                            MktLog.w(TAG, "TOS: The user selected Decline");
                        }
                        finish();
                    }
                })
                .create();
            } else {
                return null;
            }
        case SETUP_TOS911_DIALOG:
            if (BUILD.isTos911Enabled){
                Dialog tos911Dialog = ToS911Dialog.getDialog(LoginScreen.this);
                tos911Dialog.setOnDismissListener(new OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						mIsTos911DialogActive = false;
						preLoginChecks(STATE_LOGIN);
					}
				});
                return tos911Dialog;
            } else {
                return null;
            }
        case VOIP_WHATS_NEW_DIALOG:
            mAudioSetupWizard = RCMProviderHelper.getDeviceAudioWizard(LoginScreen.this); 
            RCMProviderHelper.setAccountVoipWhatsNewDialogShown(LoginScreen.this, true);
            return VoipWhatsNewDialog.getMe(this, new DialogInterface.OnClickListener() {                   
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(!mAudioSetupWizard) {
                        Intent i = new Intent(LoginScreen.this, AudioSetupWizard.class);
                        i.putExtra(AudioSetupWizard.LAUNCH_KEY_LOGIN_SCREEN, true);
                        startActivity(i);     
                    } else {
                        checkAccountNotification(LoginScreen.this, NOTIFICATION_LOGIN);
                    }
                }
            });
        default:
            return null;
        }
    }

    @Override
    protected void onPrepareDialog (int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        switch (id) {
        case WAITING_DIALOG:
            ((LoginWaitingDialog)dialog).prepare();
            mLoginCancelledByUser = false;
            break;
        case ERROR_DIALOG:
            ((LoginErrorDialog)dialog).setMessage(this, mHandlerMessage);
            break;
        case TRIAL_ADMIN_EXPIRING_DIALOG : // need as dialog get cached in onCreateDialog
        	((AlertDialog)dialog).setMessage(getExpiringMessage(LoginScreen.this));
        	break;
        default:
            break;
        }
    }


    Handler notifierHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            int request_type = msg.getData().getInt(NetworkManager.REQUEST_INFO.TYPE);
            int status = msg.getData().getInt(NetworkManager.REQUEST_INFO.STATUS);
            
            switch (status) {
                case RCMDataStore.SyncStatusEnum.SYNC_STATUS_NOT_LOADED:
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "SYNC_STATUS_NOT_LOADED");
                    }
                    break;
                case RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING:
                    //getDialog(WAITING_DIALOG);
                    break;
                case RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED:
                    if (mLoginCancelledByUser) {
                        break;
                    }
                    
                    if (request_type == RequestInfoStorage.LOGIN) {
                        if (LogSettings.MARKET) {
                            MktLog.i(TAG, "Login: SYNC_STATUS_LOADED");
                        }
                        
                        PhoneUtils.setStationLocation(DeviceUtils.getDeviceNumber(LoginScreen.this), RCMProviderHelper.getLoginNumber(LoginScreen.this), true, LoginScreen.this);
                        
                        if (UserInfo.getServiceApiMainVersion(LoginScreen.this) < 5) {
                            //This is "old" (4.70) user, or API version is unknown yet.
                            //Check API version at every login
                            //to see if the user has been migrated to the new (5.x.x User Partitioned) environment.
                            getServiceApiVersion();
                        } else {
                            getAccountInfo();
                        }
                    } else if (request_type == RequestInfoStorage.GET_API_VERSION) {
                        if (LogSettings.MARKET) {
                            MktLog.i(TAG, "GET_API_VERSION: SYNC_STATUS_LOADED");
                        }
                        getAccountInfo();
                    } else if (request_type == RequestInfoStorage.GET_ACCOUNT_INFO) {
                        if (LogSettings.MARKET) {
                            MktLog.i(TAG, "AccountInfo: SYNC_STATUS_LOADED, checking if there are account notifications ");
                        }
                        
                        Context ctx = LoginScreen.this.getApplicationContext();
                        RCMProviderHelper.setVoipEnabled_Env_Acc(ctx, UserInfo.isVoipOnEnvironmentEnabled(ctx), UserInfo.isVoIPForAccountEnabled(ctx), true);
                        
                        checkAccountNotification(LoginScreen.this, NOTIFICATION_TRIAL);                        
                    } else if (request_type == RequestInfoStorage.GET_CALLER_IDS) {
                        if (LogSettings.MARKET) {
                            MktLog.i(TAG, "CallerIDs: SYNC_STATUS_LOADED");
                        }
                        completeLogin(false);
                    }
                    break;
                case RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR:
                    if (LogSettings.MARKET) {
                        MktLog.i(TAG, "SYNC_STATUS_ERROR");
                    }

                    if (mLoginCancelledByUser) {
                        if (LogSettings.MARKET) {
                            MktLog.i(TAG, "Login already cancelled by user; error dialog will not be shown");
                        }
                        break;
                    }

                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "Dismiss waiting dialog");
                    }

                    safeDismissDialog(WAITING_DIALOG);

                    if (LogSettings.MARKET) {
                        MktLog.i(TAG, "Show error dialog");
                    }
                    showDialog(ERROR_DIALOG);
                    break;
                default:
                    if (LogSettings.MARKET) {
                        MktLog.i(TAG, "statusChanged(), status unknown or not processed : " + status);
                    }
            }
        }
    };

    @Override   //NetworkManagerNotifier.statusChanged()
    public void statusChanged(Bundle status, Object result) {
        if (this.isFinishing()) {
            return;
        }
        
        try {
			int request_type = status.getInt(NetworkManager.REQUEST_INFO.TYPE);

			if (LogSettings.ENGINEERING) {
			    EngLog.d(TAG, "statusChanged(): request_type = " + request_type);
			}
			
			if  (request_type == RequestInfoStorage.LOGIN
			        || request_type == RequestInfoStorage.GET_ACCOUNT_INFO
			        || request_type == RequestInfoStorage.GET_CALLER_IDS
			        || request_type == RequestInfoStorage.GET_API_VERSION) {
			    mHandlerMessage = notifierHandler.obtainMessage();
			    mHandlerMessage.setData(status);
			    notifierHandler.sendMessage(mHandlerMessage);
			}
		} catch (Exception e) {
			if (LogSettings.MARKET) {
			    MktLog.w(TAG, "statusChanged():ex:" + e);
			}
		}
    }

    @Override   //NetworkManagerNotifier.getNotifier()
    public NetworkManagerNotifier getNotifier() {
        return mNetworkManagerNotifier;
    }

    /*
     * Performs mandatory network operations behind the ProgressDialog;
     * if no such operations - starts completeLogin() 
     */
    private void extendedLogin() {
        if (NetworkManager.getInstance()
                .getRequestStatus(LoginScreen.this, RequestInfoStorage.GET_CALLER_IDS) == SyncStatusEnum.SYNC_STATUS_LOADED) {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "extendedLogin(): Caller IDs already loaded; complete login...");
            }
            
            //CallerIDs list is already in the DB; will be updated by completeLogin() in "background" (after LoginScreen has been closed) 
            completeLogin(true);
        } else {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "extendedLogin(): Load Caller IDs...");
            }
            
            //CallerIDs list not loaded yet; loading CallerIDs in the LoginScreen
            NetworkManager.getInstance().getCallerIds(LoginScreen.this, true);
        }
    }
    
    /*
     *  Closes LoginScreen;
     *  launches main RingCentral Activity;
     *  performs remaining network operations in "background"  
     */
    private void completeLogin(boolean updateCallerIds) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "completeLogin(updateCallerIds = " + updateCallerIds + "); dismiss waiting dialog");
        }
        
        safeDismissDialog(WAITING_DIALOG);

        mNetworkManagerNotifier = null; //disable NetworkManager notifications
        
        if (RCMProviderHelper.isVoipEnabled_Env_Acc(this) && !mHttpRegCompleted) {
            // Execute HTTP registration for SIP flags receiving
            if(LogSettings.MARKET) {
                MktLog.w(TAG, "completeLogin : HTTP Registration");
            }
            new HttpRegisterExecutor().execute(getApplicationContext());
        }

        if (updateCallerIds) {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "completeLogin(): Load Caller IDs...");
            }
            NetworkManager.getInstance().getCallerIds(getApplicationContext(), true);
        }
        
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "completeLogin(): check counters");
        }
        NetworkManager.getInstance().checkExtCounter(getApplicationContext(), true);
        NetworkManager.getInstance().checkMsgCounter(getApplicationContext(), true);

        if (LogSettings.MARKET) {
            MktLog.d(TAG, "completeLogin(): retrieve CallLog");
        }
        NetworkManager.getInstance().getCallLogByType(getApplicationContext(),
                // TODO: pass mailboxId from the caller
                RCMProviderHelper.getCurrentMailboxId(this),
                RCMConstants.CALL_LOG_PAGE_SIZE,
                RCMDataStore.CallLogTable.LOGS_ALL, "Up", true);
        NetworkManager.getInstance().getCallLogByType(getApplicationContext(),
                // TODO: pass mailboxId from the caller
                RCMProviderHelper.getCurrentMailboxId(this),
                RCMConstants.CALL_LOG_PAGE_SIZE,
                RCMDataStore.CallLogTable.LOGS_MISSED, "Up", true);
          
        Launcher.launch(this, getIntent());
    }

    /**
     * Class to act as dispatcher when passing user through LoginScreen or call restore apploication
     *
     */
    public static class Launcher {
    	
    	public static void launch(Activity baseActivity,  Intent launcher) {
    		final boolean tabsAlive = RingCentral.isAlive();    		
    		if(LogSettings.ENGINEERING){
    			EngLog.d(TAG, "Launcher:launch() isAlive: " + tabsAlive);
    		}
    		if(hasExtra(launcher, RingOut.PHONE_NUMBER)){
    			final String phoneNumber = launcher.getStringExtra(RingOut.PHONE_NUMBER);
    			if(LogSettings.ENGINEERING){
        			EngLog.d(TAG, "Launching tabs, found phone number :  " + phoneNumber);
        		}
    			RingOutAgent roAgent = new RingOutAgent(baseActivity);
    			roAgent.call(phoneNumber, null, true);
    			
    		} else if (hasExtra(launcher, RingCentral.TAB) && tabsAlive) {
    			if(LogSettings.ENGINEERING){
        			EngLog.d(TAG, "Launching tabs, found messages update");
        		}
    			if (TextUtils.isEmpty(DeviceUtils.getDeviceNumber(baseActivity))){
    			    if (LogSettings.MARKET){
    			        MktLog.d(TAG, "device number is empty. canceling switch to Messages");
    			    }
    			} else {
        			launchTabs(baseActivity, launcher);
        			RingCentral.fireSwitchTabEvent(baseActivity, RingCentral.TAB_MESSAGES_ID);
    				Messages.fireSwitchMessageTabEvent(baseActivity, Messages.MODE_RECENT);
    			}
    		} else {
    			if(LogSettings.ENGINEERING){
        			EngLog.d(TAG, "Launching tabs, no interesting action");
        		}
	    		if(tabsAlive){
	    			restoreTabs(baseActivity, launcher);
	    		} else {
	    			launchTabs(baseActivity, launcher);
	    		}
    		}
    		baseActivity.finish();
    	}
    	
    	public static void launchLogin(Context context, Intent launcher){
    		final Intent intent = new Intent(context, LoginScreen.class);
    		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    		if((launcher.getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) == 0)
    			intent.putExtras(launcher);
    		context.startActivity(intent); 
    	}
    	
    	public static void restoreTabs(Context context, Intent launcher) {
    		final Intent intent = new Intent(context, LoginScreen.class);
    		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY );
    		intent.addCategory(Intent.CATEGORY_LAUNCHER);
    		intent.setAction(Intent.ACTION_MAIN);
    		context.startActivity(intent);
    	}
    	
    	public static void launchTabs(Context context, Intent launcher){
    		final Intent intent = new Intent(context, RingCentral.class);
    		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    		if((launcher.getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) == 0)
    			intent.putExtras(launcher);
    		//RingCentral.setRestart(false);
    		context.startActivity(intent);    		    		
    	}    	
    	
    	
    	private static boolean hasExtra(Intent launcher, String extra){
    		return (launcher != null) &&
    			   ((launcher.getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) == 0 ) &&
    			   (launcher.hasExtra(extra));
    	}
    }
    
    
	public static void launchSignup(Context ctx) {
		Uri uri = RCMConfig.get_SIGNUP_URL(ctx, true);

		if (LogSettings.MARKET) {
			MktLog.d(TAG, "Launching signup : " + uri.toString());
		}

		Intent marketIntent = new Intent(Intent.ACTION_VIEW, uri);
		ctx.startActivity(marketIntent);
    }
	
    /**
     * Safe dismiss dialog as there is no consistency in dialogs
     * showing/dismissing
     * 
     * @param dialog
     *            The id of the managed dialog.
     */
    private void safeDismissDialog(int dialog) {
        try {
            dismissDialog(dialog);
        } catch (java.lang.Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "onDismissDialog " + dialog + " error occured " + th.toString());
            }
        }
    }
	
    /**
     * 
     * Handling User Notification cycle
     * 
     * 
     */
    private static final int NOTIFICATION_TRIAL = 1;
    private static final int NOTIFICATION_SETUP = 2;
    private static final int NOTIFICATION_VOIP_WHATS_NEW = 3;
    private static final int NOTIFICATION_LOGIN = 4;
    private void checkAccountNotification(Context context, int step){
    	if(LogSettings.MARKET){
    		MktLog.d(TAG, "checkAccountNotification, step :" + step);
    	}
    	if(step == NOTIFICATION_TRIAL){
    		int trialDialogType = getTrialDialogType(context);
    		if(LogSettings.ENGINEERING){
        		EngLog.i(TAG, "checkAccountNotification(): Trial reminder, status : " + (trialDialogType != TRIAL_NONE_DIALOG));
        	}
    		if(trialDialogType != TRIAL_NONE_DIALOG){
    			if (LogSettings.MARKET) {
    				MktLog.d(TAG, "checkAccountNotification(): trial dialog type = " + trialDialogType);
    			}
    			safeDismissDialog(WAITING_DIALOG);
    			showDialog(trialDialogType);
    			return;
    		}
    		step = NOTIFICATION_SETUP;    		
    	}
    	if(step == NOTIFICATION_SETUP){
    		boolean setupWizardEnabled = isSetupWizardEnabled(context);
    		if(LogSettings.MARKET){
        		MktLog.i(TAG, "checkAccountNotification(): Setup Wizard status : " + setupWizardEnabled);
        	}
    		if(setupWizardEnabled){
    			String setupWizardState = RCMProviderHelper.getSetupWizardState(context);
    			if (LogSettings.MARKET) {
    				MktLog.i(TAG, "checkAccountNotification(): setup_wizard_state = " + setupWizardState);
    			}
    			if (AccountInfoTable.SETUP_WIZARD_STATE_NOT_STARTED.equals(setupWizardState)) {
    				safeDismissDialog(WAITING_DIALOG);
    				startSetupWizard(context);
    			} else if ( AccountInfoTable.SETUP_WIZARD_STATE_INCOMPLETE.equals(setupWizardState)) {
    				safeDismissDialog(WAITING_DIALOG);
    				showDialog(SETUP_INCOMPLETE_DIALOG);
    			}
    			return;
    		}
        	step = NOTIFICATION_VOIP_WHATS_NEW;    		
    	}
    	/** comment is deprecated 
         * Battery drain & wizard part, short description:
         * 
         * BDD ~ battery drain dialog 
         * HttpReg ~ http registration(getting info like: is voip enabled on that tier & etc..)
         * 
         * EXECUTE_BATTERY_DRAIN_DIALOG_STEP:
         * (BDD not shown & HttpReg not completed) ? [EXECUTE_HTTP_REGISTRATION(handler) & RETURN] : [CONTINUE]   
         * (BDD not shown & VoIP is enabled) ? [SHOW_DIALOG] : [CONTINUE]
         *                         
         * EXECUTE_HTTP_REGISTRATION PROC
         *     [handler accumulating & http registration execution]
         *     ....
         *     on registration finished => notify handler 
         * EXECUTE_HTTP_REGISTRATION ENDP
         *  
         * HANDLER:
         *     on http registration finished message will call checkAccountNotification function
         */
        if(step == NOTIFICATION_VOIP_WHATS_NEW) {
            boolean voipWhatsNewDialogShown = RCMProviderHelper.isVoipWhatsNewDialogShown(context);
            boolean isVoipEnabled = RCMProviderHelper.isVoipEnabled_Env_Acc_SipFlagHttpReg_ToS_UserVoIP(context);
            
            if(!mHttpRegCompleted && !voipWhatsNewDialogShown) {
                if(LogSettings.MARKET) {
                    MktLog.d(TAG, " firstly http registration will be completed ");
                }
                startHttpReg(false);
                return;
            }
            
            if(!voipWhatsNewDialogShown && isVoipEnabled) {
                safeDismissDialog(WAITING_DIALOG);
                RCMProviderHelper.setAccountVoipWhatsNewDialogShown(context, true);
                showDialog(VOIP_WHATS_NEW_DIALOG);
                return;
            } 
            
            step = NOTIFICATION_LOGIN;
        }
    	if(step == NOTIFICATION_LOGIN){
    		if(LogSettings.ENGINEERING){
        		EngLog.d(TAG, "checkAccountNotification(): proceed with login");
        	}
    		extendedLogin();
    	}    	
    }
    /**
     * ##########################################################################
     * 
     * Account expiration methods part
     * 
     * ##########################################################################
     */                
    private int getTrialDialogType(Context context){
    	final int rcServiceVersion = RCMProviderHelper.getServiceVersion(context);    	
    	final int userType = UserInfo.getUserType(context);
    	final int trialDaysLeft = RCMProviderHelper.getTrialDaysLeft(context);
    	final String trialExpirationState = RCMProviderHelper.getTrialState(context);
    	int dialogType = TRIAL_NONE_DIALOG;
    	if(rcServiceVersion >= AccountInfoTable.SERVICE_VERSION_5){
    		if(AccountInfoTable.TRIAL_STATUS_EXPIRED.equals(trialExpirationState)){
    			dialogType = (userType == UserInfo.ADMIN) ?  
    							TRIAL_ADMIN_EXPIRED_DIALOG : 
    							TRIAL_USER_EXPIRED_DIALOG;
    		}else if(AccountInfoTable.TRIAL_STATUS_EXPIREDINXDAYS.equals(trialExpirationState) &&
    				(userType == UserInfo.ADMIN) &&
    				(trialDaysLeft <= BUILD.ACCOUNT_EXPIRE_REMINDER_START_DAYS) &&
    				(System.currentTimeMillis()-RCMProviderHelper.getLastExpirationReminder(context)>BUILD.ACCOUNT_EXPIRATION_REMINDER_INTERVAL)){
    			dialogType = TRIAL_ADMIN_EXPIRING_DIALOG;    			
    		}
    	}
    	if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "TrialDialog : serviceVersion : "      + rcServiceVersion + 
            						  " userType : "  			 + userType + 
            						  " trialDaysLeft : "        + trialDaysLeft +
            						  " BUILD.ACCOUNT_EXPIRE_REMINDER_START_DAYS :" + BUILD.ACCOUNT_EXPIRE_REMINDER_START_DAYS + 
            						  " trialExpirationState : " + trialExpirationState + 
            						  " dialogType: "            + dialogType);
        }
    	return dialogType;
    }    
    private void startUpdadeWizard(Context context){
    	Uri uri = Uri.parse(RCMConfig.get_WEB_SETTINGS_UPGRADE_URL(context));
    	if (LogSettings.MARKET) {
            MktLog.i(TAG, "URI: Upgrade Url: " + uri.toString());
        }   
    	context.startActivity(new Intent(Intent.ACTION_VIEW, uri));    	
    }
    private String getExpiringMessage(Context context){
    	final int daysLeft = RCMProviderHelper.getTrialDaysLeft(this);
    	if (daysLeft == 1) {    		
    		return context.getString(R.string.loginscreen_trial_dlg_msg_admin_expirein1day);
    	} else if (daysLeft == 0 ) {
    		return context.getString(R.string.loginscreen_trial_dlg_msg_admin_expiretoday);
    	} else if (daysLeft > 1){
    		return context.getString(R.string.loginscreen_trial_dlg_msg_admin_expireinxdays, daysLeft);
    	}
    	return "";    	
    }
    /**
     * ##########################################################################
     * 
     * SetupWizard methods part
     * 
     * ##########################################################################
     */    
    private boolean isSetupWizardEnabled(Context context){    	
    	final int rcServiceVersion = RCMProviderHelper.getServiceVersion(context);
    	final boolean rcSystemExtension = RCMProviderHelper.isSystemExtension(context);
    	final String rcSetupWizardState = RCMProviderHelper.getSetupWizardState(context);
    	final long timeDelta = System.currentTimeMillis()-RCMProviderHelper.getLastCompleteSetupRequest(context);
    	
		boolean setupWizardEnabled = (rcServiceVersion >= AccountInfoTable.SERVICE_VERSION_5)&&
								  	  rcSystemExtension&&
								  	  (AccountInfoTable.SETUP_WIZARD_STATE_NOT_STARTED.equals(rcSetupWizardState)||								  	  
								  	  (AccountInfoTable.SETUP_WIZARD_STATE_INCOMPLETE.equals(rcSetupWizardState)&&
								  	  (timeDelta > BUILD.SETUP_WIZARD_REMINDER_INTERVAL)));
		if (LogSettings.MARKET) {
            MktLog.i(TAG, "SetupWizard : serviceVersion : "   + rcServiceVersion + 
            						  " SystemExtension : "  + rcSystemExtension + 
            						  " SetupWizardState : " + rcSetupWizardState + 
            						  " isEnabled : " + setupWizardEnabled + 
            						  " timeDelta : " + timeDelta);
        }
		return setupWizardEnabled;
	}

	private boolean startSetupWizard(Context context) {
		if (LogSettings.MARKET) {
            MktLog.d(TAG, "Starting SetupWizard, clearing DB");
        }            
		//clearing credentials and related stuff
		MessagesHandler.clearBodyLoaderTask();
        HttpClientFactory.shutdown();
        RequestInfoStorage.clearRequestHash(); 
        RCMProviderHelper.clearSyncStatusTable(context);
        Uri uri = RCMConfig.get_SETUP_URL(context, true);
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "URI: Setup Url: " + uri.toString());
        } 
        context.startActivity(new Intent(Intent.ACTION_VIEW, uri));                       
        return true;
    }

	private void showTos911Dialog() {
		mIsTos911DialogActive = true;
		showDialog(SETUP_TOS911_DIALOG);
	}
	
	
	 // ====================== Workaround Section (BN) ==========================
    // == Reduces memory leaks. The section can be just added to any Activity ==
    // == Call leakCleanUpRootView onDestroy and before setting new root view ==
    
    @Override
    public void setContentView(int layoutResID) {
        ViewGroup rootView = (ViewGroup) 
            LayoutInflater.from(this).inflate(layoutResID, null);
        setContentView(rootView);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        leakCleanUpRootView();
        mLeakContentView = (ViewGroup) view;
    }

    @Override
    public void setContentView(View view, LayoutParams params) {
        super.setContentView(view, params);
        leakCleanUpRootView();
        mLeakContentView = (ViewGroup) view;
    }
    
    /**
     * Cleanup root view to reduce memory leaks.
     */
    private void leakCleanUpRootView() {
        if (mLeakContentView != null) {
            ViewGroup v = mLeakContentView;
            mLeakContentView = null;
            mStackRecursions = 0;
            leakCleanUpChildsDrawables(v);
            System.gc();
        }
    }
    
    /**
     * Clean-up Drawables in the view including child.
     * 
     * @param v
     */
    private void leakCleanUpChildsDrawables(View v) {
        if (v != null) {
            try {
                ViewGroup group = (ViewGroup) v;
                int childs = group.getChildCount();
                for (int i = 0; i < childs; i++) {
                    if (LEAKS_CLEAN_UP_STACK_RECURSIONS_LIMIT > mStackRecursions) {
                        mStackRecursions++;
                        leakCleanUpChildsDrawables(group.getChildAt(i));
                        mStackRecursions--;
                    } else {
                        break;
                    }
                }
            } catch (java.lang.Throwable th) {
            }
            leakCleanUpDrawables(v);
        }
    }

    /**
     * Keeps recursions number for memory clean-ups.
     */
    private int mStackRecursions;
    
    /**
     * Limit of leaks clean-ups stack recursions.
     */
    private static final int LEAKS_CLEAN_UP_STACK_RECURSIONS_LIMIT = 256;
    
    /**
     * Cleans drawables of in the view.
     * 
     * @param v the view to clean-up
     */
    private void leakCleanUpDrawables(View v) {
        if (v == null) {
            return;
        }
            
        try {
            v.setBackgroundDrawable(null);
        } catch (java.lang.Throwable th) {
        }

        try {
            ImageView imageView = (ImageView) v;
            imageView.setImageDrawable(null);
            imageView.setBackgroundDrawable(null);
        } catch (java.lang.Throwable th) {
        }
    }
    
    /**
     * Keeps current root view.
     */
    private ViewGroup mLeakContentView = null;
    
    /**
     * Makes http depending setting and already completed actions 
     * (look where it's called, done especially for BatteryDrainDialog)
     * @param inBackground - true if no need to notify anyone
     *                     - false if necessary notify login screen using m_httpRegHandler regarding completed httpReg(Battery drain dialog) 
     */
    private void startHttpReg(boolean inBackground) {
        if(LogSettings.MARKET) {
            MktLog.d(TAG, "startHttpReg, inBackground: " + inBackground);
        }
        
        // if registration is not completed
        if(!mHttpRegCompleted) {
            if (RCMProviderHelper.isVoipEnabled_Env_Acc(this)) {
                // Execute HTTP registration for SIP flags receiving
                if(LogSettings.MARKET) {
                    MktLog.w(TAG, "startHttpReg : HTTP Registration");
                }
                if(!inBackground) { 
                    new HttpRegisterExecutor().execute(getApplicationContext(), m_httpRegHandler);
                } else {
                    new HttpRegisterExecutor().execute(getApplicationContext());
                }
            } else {
                // notificate UI that it completed
                mHttpRegCompleted = true;
                
                Message msg = m_httpRegHandler.obtainMessage();
                msg.what = MESSAGE_HTTP_REG_COMPLETED;
                m_httpRegHandler.sendMessage(msg);
            }
        } else {
            if(LogSettings.MARKET) {
                MktLog.i(TAG, "http registration already completed");
            }
        }
    }
    
    /**
     * Set 'TypeFace' of edit field and set 'ClickAble' of Login button
     **/
    private void setEditFieldTypeAndLoginButton(){
    	if(phoneField.length() > 0){
			phoneField.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
		}else{
			phoneField.setTypeface(Typeface.DEFAULT, Typeface.ITALIC);
		}
		
		if(extField.length() > 0){
			extField.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
		}else{
			extField.setTypeface(Typeface.DEFAULT, Typeface.ITALIC);
		}
		
		if(passwordField.length() > 0){
			passwordField.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
		}else{
			passwordField.setTypeface(Typeface.DEFAULT, Typeface.ITALIC);
		}
		
		if(phoneField.length() > 0 && passwordField.length() > 0){
			mLoginBtn.setEnabled(true);
			mLoginBtn.setBackgroundResource(R.drawable.bg_login_button_background);
		}else{
			mLoginBtn.setEnabled(false);
			mLoginBtn.setBackgroundResource(R.drawable.btn_login_bg_disable);
		}
    }
    
	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		setEditFieldTypeAndLoginButton();
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		
	}
    
}