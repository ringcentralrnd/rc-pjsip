/** 
 * Copyright (C) 2010-2012 RingCentral, Inc. 
 * All Rights Reserved.
 */

package com.ringcentral.android;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.utils.DeviceUtils;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class SplashScreenActivity extends Activity {
	private static final String TAG = "[RC]SplashScreenActivity";
	
	private static final long SPLASH_LIFETIME = BUILD.SPLASH_SCREEN_TIMEOUT;	
	private static final int MSG_SPLASH_EXPIRE = 0;
	
	private int mDisplayOrientation;
	
	final Handler mHandler = new Handler() {		
		public void handleMessage(Message msg) {			
			if (msg.what == MSG_SPLASH_EXPIRE) {
				if(LogSettings.MARKET) {
					EngLog.d(TAG, "Splash timer expired, closing...");
				}
				finish();
			}			
		}		
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (!BUILD.SPLASH_SCREEN_ENABLED) {
			finish();
			return;
		}
		
		buildLayout(DeviceUtils.getDisplayOrientation(this));
		mHandler.sendEmptyMessageDelayed(MSG_SPLASH_EXPIRE, SPLASH_LIFETIME);
		
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		
		if (!BUILD.SPLASH_SCREEN_ENABLED) {
			finish();
			return;
		}
		
		int orientation = newConfig.orientation;
        
        if (orientation == Configuration.ORIENTATION_UNDEFINED) {
            orientation = DeviceUtils.calcDisplayOrientation(this);
        }

        if (orientation != mDisplayOrientation) {
            buildLayout(orientation);
        }
	}
	
	private void buildLayout(int orientation) {
		if (!BUILD.SPLASH_SCREEN_ENABLED) {
			finish();
			return;
		}
		
		mDisplayOrientation = orientation;
		if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
			setContentView(R.layout.login_splash_landscape); 			
		} else {
			setContentView(R.layout.login_splash_portrait);
		}		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		setResult(RESULT_OK);
	}

}
