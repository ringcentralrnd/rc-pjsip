/** 
 * Copyright (C) 2011-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.sip.service.CallInfo;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.MailboxCurrentTable;
import com.ringcentral.android.voip.VoipCallStatusActivity;
import com.ringcentral.android.voip.VoipInCall;


public class RcmStatusNotification {

    private static final String TAG = "[RC]RcmStatusNotification";

    private static RcmStatusNotification sInstance;

    public static synchronized RcmStatusNotification getInstance() {
        if (sInstance == null) {
            sInstance = new RcmStatusNotification();
        }
        return sInstance;
    }
    
    public static synchronized void onAppContextChanged(){
		if (sInstance != null) {
			sInstance.destroy();
		} 
		sInstance = new RcmStatusNotification();
    }


    private Notification mNotification;
    private final String mContentTitle;
    
    private Context mAppContext = null;
    
    private Intent mNotificationIntentCalls;
    private Intent mNotificationIntentIncomingCall;
    
    private int mCallsTotal;
    private int mCallsOnHold;

    private boolean mIsIncomingCall;

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "onReceive(): action: " + action);
            }
            
            if (!checkContext()) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "onReceive(): Application context changed; return");
                }
                return;
            }
            
            if (RCMConstants.ACTION_VOIP_CALLS_NUMBER_CHANGED.equals(action)) {
                int calls_total = intent.getIntExtra(RCMConstants.EXTRA_VOIP_CALLS_NUMBER_CHANGED_TOTAL, -1);
                int calls_on_hold = intent.getIntExtra(RCMConstants.EXTRA_VOIP_CALLS_NUMBER_CHANGED_ON_HOLD, -1);
            
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "onReceive(): calls_total=" + calls_total + ", calls_on_hold=" + calls_on_hold);
                }
                
                if (calls_total < 0 || calls_on_hold < 0) {
                    return;         //invalid calls count
                }
                
                if (calls_total == 0) {
                    mIsIncomingCall = false;
                }
                
                updateCallsCount(calls_total, calls_on_hold);

                if (calls_total > 0 || calls_on_hold > 0) {
                    show();
                } else {
                    cancel();
                }

            } else if (RCMConstants.ACTION_VOIP_NOTIFY_INCOMING_CALL.equals(action)) {
                if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
                    long startTime = intent.getLongExtra(RCMConstants.EXTRA_VOIP_INCOMING_CALL_START_TIME, 0);
                    PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.INCOMING_VOIP_RINGTONE, startTime);
                    PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.INCOMING_TO_INVOIP_ACTIVITY, startTime);
                }
                
                CallInfo call_info = intent.getParcelableExtra(RCMConstants.EXTRA_VOIP_INCOMING_CALL_INFO);
                int calls_number = intent.getIntExtra(RCMConstants.EXTRA_VOIP_INCOMING_CALL_INFO_COUNT, -1);
                
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "onReceive(): calls_number=" + calls_number + (call_info == null ? "; call_info==null!!!" : ""));
                }
                
				if (call_info != null && calls_number >= 0) {
		            mIsIncomingCall = true;
		            notifyIncomingCall(call_info, calls_number);
					show();
				}
            
            } else if (RCMConstants.ACTION_VOIP_STOP_NOTIFY_INCOMING_CALL.equals(action)) {
				if (LogSettings.MARKET) {
					MktLog.i(TAG, "onReceive(): ACTION_VOIP_STOP_NOTIFY_INCOMING_CALL");
				}
		        
				mIsIncomingCall = false;
		        if (mCallsTotal <= 0) {
		            cancel();
		        } else {
		            updateCallsCount(mCallsTotal, mCallsOnHold);
		            show();
		        }
            }
        }
    };
    
    private RcmStatusNotification() {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "RcmStatusNotification(): constructor");
        }
        
        mAppContext = RingCentralApp.getContextRC();
        
        mNotification = new Notification();
        mNotification.flags |= Notification.FLAG_ONGOING_EVENT; 

        mContentTitle = mAppContext.getString(R.string.app_name);

        mNotificationIntentCalls = new Intent(mAppContext, VoipCallStatusActivity.class);
        mNotificationIntentCalls.setFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        
        mNotificationIntentIncomingCall = new Intent(mAppContext, VoipInCall.class);
        mNotificationIntentIncomingCall.setFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

        mAppContext.registerReceiver(mBroadcastReceiver, new IntentFilter(RCMConstants.ACTION_VOIP_CALLS_NUMBER_CHANGED));        
        mAppContext.registerReceiver(mBroadcastReceiver, new IntentFilter(RCMConstants.ACTION_VOIP_NOTIFY_INCOMING_CALL));        
        mAppContext.registerReceiver(mBroadcastReceiver, new IntentFilter(RCMConstants.ACTION_VOIP_STOP_NOTIFY_INCOMING_CALL));
        
        mIsIncomingCall = false;
        mCallsTotal = 0;
        mCallsOnHold = 0;
    }

    public void show() {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "show()...");
        }

        if (!checkContext()) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "show(): Application context changed; return");
            }
            return;
        }
        
        if (isMailboxValid()) {
	        try {
				((NotificationManager) mAppContext
				        .getSystemService(Context.NOTIFICATION_SERVICE))
				        .notify(RCMConstants.NOTIFICATION_ID_RCM_STATUS, mNotification);
			} catch (Exception e) {
				if (LogSettings.MARKET){
					MktLog.e(TAG, "show e:"+e);
				}
			}
        }
    }

	public void cancel() {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "cancel()...");
        }
        try {
			((NotificationManager) mAppContext
			        .getSystemService(Context.NOTIFICATION_SERVICE))
			        .cancel(RCMConstants.NOTIFICATION_ID_RCM_STATUS);
		} catch (Exception e) {
			if (LogSettings.MARKET){
				MktLog.e(TAG, "cancel e:"+e);
			}
		}
    }

    private synchronized void updateCallsCount(int callsTotal, int callsOnHold) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "updateCallsCount(callsTotal=" + callsTotal +", callsOnHold=" + callsOnHold + "); mIsIncomingCall=" + mIsIncomingCall);
        }

        mCallsTotal = callsTotal;
        mCallsOnHold = callsOnHold;
        
        if (!mIsIncomingCall && callsTotal <= 0) {
            return;         //No active calls; notification will not be shown 
        }
        
        Intent notification_intent = mIsIncomingCall ? mNotificationIntentIncomingCall : mNotificationIntentCalls;
        
        mNotification.icon = R.drawable.ic_stat_notify_call;
        mNotification.setLatestEventInfo(mAppContext,
                mContentTitle,
                getContentText(callsTotal, callsOnHold),
                PendingIntent.getActivity(mAppContext, 0, notification_intent, PendingIntent.FLAG_UPDATE_CURRENT));
    }

    private synchronized void notifyIncomingCall(CallInfo callInfo, int callsNumber) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "notifyIncomingCall(callsNumber=" + callsNumber + ")");
        }

        mNotificationIntentIncomingCall.putExtra(RCMConstants.EXTRA_VOIP_INCOMING_CALL_INFO, callInfo);
        mNotificationIntentIncomingCall.putExtra(RCMConstants.EXTRA_VOIP_INCOMING_CALL_INFO_COUNT, callsNumber);

        mNotification.contentIntent = PendingIntent.getActivity(mAppContext, 0,
                mNotificationIntentIncomingCall, PendingIntent.FLAG_UPDATE_CURRENT);

        mNotification.icon = R.drawable.ic_stat_notify_call;
    }

    private String getContentText(int callsTotal, int callsOnHold) {
        if (LogSettings.MARKET) {
        	MktLog.i(TAG, "getContentText(callsTotal=" + callsTotal + ", callsOnHold=" + callsOnHold + ")");
        }

        if (callsTotal <= 0 && callsOnHold <= 0) {
            return "";
        }

        String active_calls_label = null;
        String calls_on_hold_label = null;
        String result = null;
        
        int active_calls = callsTotal - callsOnHold;
        
        if (active_calls == 1) {
            active_calls_label = mAppContext.getString(R.string.status_notification_one_active_call);
        } else if (active_calls > 1) {
            active_calls_label = mAppContext.getString(R.string.status_notification_multiple_active_calls, active_calls);
        }
        
        if (callsOnHold == 1) {
            calls_on_hold_label = mAppContext.getString(R.string.status_notification_one_call_on_hold);
        } else if (callsOnHold > 1) {
            calls_on_hold_label = mAppContext.getString(R.string.status_notification_multiple_calls_on_hold, callsOnHold);
        }
        
        if (active_calls > 0 && callsOnHold > 0) {
            result = mAppContext.getString(R.string.status_notification_calls_all, active_calls_label, calls_on_hold_label);
        } else if (active_calls > 0) {
            result = active_calls_label;
        } else if (callsOnHold > 0) {
            result = calls_on_hold_label;
        } else {
            result = null;
        }
        
        return result;
    }


    private boolean isMailboxValid() {
        try {
            return RCMProviderHelper.getCurrentMailboxId(mAppContext) != MailboxCurrentTable.MAILBOX_ID_NONE;
        } catch (Exception e) {
            if (LogSettings.MARKET){
                MktLog.e(TAG, "isMailboxValid e:"+e);
            }
        }
        return false;
    }
    
    private synchronized boolean checkContext() {
        boolean valid = mAppContext != null && mAppContext == RingCentralApp.getContextRC();
        
        if (!valid){
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "checkContext(): context is invalid");
            }
            destroy();
        }
        
        return valid;
    }

    private void destroy() {
        try {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "destroy...");
            }
            cancel();
            mAppContext.unregisterReceiver(mBroadcastReceiver);
            mBroadcastReceiver = null;
            mAppContext = null;
            mNotification = null;
            mNotificationIntentCalls = null;
            mNotificationIntentIncomingCall = null;
        } catch (Exception e) {
            if (LogSettings.MARKET){
                MktLog.e(TAG, "destroy e:"+e);
            }
        }
    }

}
