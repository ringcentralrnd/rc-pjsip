/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.ringcentral.android.contacts.CallLogListActivity;
import com.ringcentral.android.contacts.ContactsListActivity;
import com.ringcentral.android.contacts.FavoritesListActivity;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.messages.Messages;
import com.ringcentral.android.settings.RingOutMode;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.DeviceUtils;
import com.ringcentral.android.utils.NetworkUtils;
import com.ringcentral.android.utils.OnBootReceiver;
import com.ringcentral.android.utils.RestartAppIntentReceiver;
import com.ringcentral.android.utils.SettingsFormatUtils;
import com.ringcentral.android.utils.ui.widget.ActivityManager;

/*
 *  TODO When Android 1.5 support will be dropped out, TabManagerActivity can be removed with TabActivity and 
 *  other stub classes can be removed
 */
public class RingCentral extends TabManagerActivity implements ActivityManager.OnTabChangeListener {

    private static final String TAG = "[RC]RingCentral";        
    
    public static final String TAB = "TAB";
    public static final String TAB_TAG = "TAB_TAG";
    public static final String TAB_RINGOUT = "RingOut";        
    public static final String TAB_CALL_LOG = "CallLog";
    public static final String TAB_CONTACTS = "Contacts";
    public static final String TAB_FAVORITES = "Favorites";
    public static final String TAB_MESSAGES = "Messages";
    
    @Deprecated
    public static final int TAB_RINGOUT_ID = 0;      
    public static final int TAB_MESSAGES_ID		= 0;
    public static final int TAB_CONTACTS_ID		= 1;
    public static final int TAB_FAVORITES_ID	= 2;
    public static final int TAB_CALL_LOG_ID		= 3;

    private SwitchTabReceiver mSwitchTabReceiver;
    private RestartAppIntentReceiver restartAppIntentReceiver;  
    private VoipRingStartReceiver mRingStartedReceiver;
    private IncomingVoipRingtoneStartedReceiver mInVoipRingtoneStartedReceiver;
    private IncomingVoipActivityOpenedReceiver mInVoipActivityOpenedReceiver;
    private IncomingVoipActivityAnswerReceiver mInVoipActivityAnswerReceiver;
    private VoipCallStatusActivityOpenedReceiver mCallStatusOpened;
    private IncomingVoipActivityAnswerAndHoldReceiver mInVoipActivityAnswerAndHoldReceiver;
    
    private SettingsFormatUtils mSettingsFormatUtils;

    
    //dirty workaround to check if activity has been killed to avoid multiple activity creation
    private static Object mSync = new Object();
    private static boolean isAlive = false;
    private static boolean hasRestart = false;
    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "onCreate RC intent : " + getIntent().toString() + ", hasRestart : " + hasRestart);
        }                         
        
        synchronized (mSync) {
        	if(hasRestart) {        		
        		if(LogSettings.MARKET) {
        			MktLog.w(TAG, "Found that process was killed, restarting application for consistency");
        		}
        		hasRestart = false;
        		isAlive = false;
        		RingCentralApp.restartApplication(RingCentral.this);
        		finish();
        	} else {
        		isAlive = true;
        	}
        	
		}        
        buildView(DeviceUtils.getDisplayOrientation(this));
        
        mSettingsFormatUtils = new SettingsFormatUtils(getBaseContext());
        
        restartAppIntentReceiver = RestartAppIntentReceiver.registerRestartEvent(this);
        
        mSwitchTabReceiver = new SwitchTabReceiver();
        IntentFilter intentFilter = new IntentFilter(RCMConstants.ACTION_CURRENT_TAB_CHANGED);
		registerReceiver(mSwitchTabReceiver, intentFilter);
		
		if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
		    mRingStartedReceiver = new VoipRingStartReceiver();
		    registerReceiver(mRingStartedReceiver, new IntentFilter(RCMConstants.ACTION_VOIP_RING_STARTED));
		    
		    mInVoipRingtoneStartedReceiver = new IncomingVoipRingtoneStartedReceiver();
		    registerReceiver(mInVoipRingtoneStartedReceiver, new IntentFilter(RCMConstants.ACTION_INVOIP_RINGTONE_STARTED));
		    
		    mInVoipActivityOpenedReceiver = new IncomingVoipActivityOpenedReceiver();
		    registerReceiver(mInVoipActivityOpenedReceiver, new IntentFilter(RCMConstants.ACTION_INVOIP_ACTIVITY_STARTED));
		    
		    mInVoipActivityAnswerReceiver = new IncomingVoipActivityAnswerReceiver();
		    registerReceiver(mInVoipActivityAnswerReceiver, new IntentFilter(RCMConstants.ACTION_INVOIP_ANSWER));
		    
		    mCallStatusOpened = new VoipCallStatusActivityOpenedReceiver();
            registerReceiver(mCallStatusOpened, new IntentFilter(RCMConstants.ACTION_CALL_STATE_ACTIVITY_STARTED));
            
            mInVoipActivityAnswerAndHoldReceiver = new IncomingVoipActivityAnswerAndHoldReceiver();
            registerReceiver(mInVoipActivityAnswerAndHoldReceiver, new IntentFilter(RCMConstants.ACTION_INVOIP_ANSWER_AND_HOLD));
		}

        ((RingCentralApp) getApplicationContext()).setNetworkState(NetworkUtils.getNetworkState(this));
        
        if (DeviceUtils.checkDeviceIMSI(this) == false || TextUtils.isEmpty(DeviceUtils.getDeviceNumber(this))) {            
        	if (LogSettings.MARKET) {
                MktLog.i(TAG, "First launch detected or SIM card changed");
            }            
            Intent intent = new Intent(this, RingOutMode.class);
        	intent.putExtra(RCMConstants.EXTRA_SHOW_DEVICE_NUMBER_DIALOG_AND_FINISH, true);
            startActivity(intent);
        }
        
        OnBootReceiver.scheduleTimer(this, false);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onDestroy()... system destroying tab activity");
        }
        
        synchronized (mSync) {
        	isAlive = false;
		}        
        
        if(restartAppIntentReceiver != null){
        	restartAppIntentReceiver.destroy();        
        	restartAppIntentReceiver = null;
        }                
        if(mSwitchTabReceiver != null){
        	unregisterReceiver(mSwitchTabReceiver);
        	mSwitchTabReceiver = null;
        }
        if(mRingStartedReceiver != null){
            unregisterReceiver(mRingStartedReceiver);
            mRingStartedReceiver = null;
        }
        if(mInVoipRingtoneStartedReceiver != null){
            unregisterReceiver(mInVoipRingtoneStartedReceiver);
            mInVoipRingtoneStartedReceiver = null;
        }
        if(mInVoipActivityOpenedReceiver != null){
            unregisterReceiver(mInVoipActivityOpenedReceiver);
            mInVoipActivityOpenedReceiver = null;
        }
        if(mInVoipActivityAnswerReceiver != null){
            unregisterReceiver(mInVoipActivityAnswerReceiver);
            mInVoipActivityAnswerReceiver = null;
        }
        if(mCallStatusOpened != null){
            unregisterReceiver(mCallStatusOpened);
            mCallStatusOpened = null;
        }
        if(mInVoipActivityAnswerAndHoldReceiver != null){
            unregisterReceiver(mInVoipActivityAnswerAndHoldReceiver);
            mInVoipActivityAnswerAndHoldReceiver = null;
        }
        
       
        leakCleanUpRootView();
        leakCleanUpChildsDrawables(getActivityManager());
        
        try {
            if (mSettingsFormatUtils != null) {
                mSettingsFormatUtils.destroy();
            }
            mSettingsFormatUtils = null;
        } catch (java.lang.Throwable th) {
        }
                 
    }
    
    @Override
    protected void onResume() {
        super.onResume(); 
        ((RingCentralApp) getApplicationContext()).setNetworkState(NetworkUtils.getNetworkState(this));        
        ((RingCentralApp) getApplication()).sendBroadcast(new Intent(RCMConstants.ACTION_DND_STATUS_CHANGED));
        
    }
        
    
    @Override
    protected void onStart() {
        super.onStart();
        FlurryTypes.onStartSession(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
            PerformanceMeasurementLogger.getInstance().printResults();
        }
        FlurryTypes.onEndSession(this);
    }
    
    public static boolean isAlive(){
    	synchronized (mSync) {
    		return isAlive;
		}    	
    }
    
    public static void setRestart(boolean hasRestart) {
    	synchronized (mSync) {
    		RingCentral.hasRestart = hasRestart;
		}
    }
    

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if(LogSettings.ENGINEERING){
        	EngLog.d(TAG, "onNewIntent : " + intent.toString() + ", action" + intent.getAction());
        }
         	
        if ((intent.getAction() != null) && intent.getAction().equals(RCMConstants.ACTION_CURRENT_TAB_CHANGED)) {
        	if(intent.hasExtra(TAB)){
        		final int newTab = intent.getIntExtra(TAB, TAB_RINGOUT_ID);
        		if(LogSettings.ENGINEERING){
                	EngLog.d(TAG, "onNewIntent, setting TAB : " + newTab);
                }
        		getActivityManager().setCurrentScreen(newTab);            		
        	}
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (LogSettings.ENGINEERING){
            EngLog.i(TAG, "onConfigurationChanged(): orientation = " + newConfig.orientation);
        }

        // Workaround for Android bug #6893
        // (https://code.google.com/p/android/issues/detail?id=6893):
        // sometimes onConfigurationChanged() in the RingOut tab activity is not
        // invoked at screen orientation change
        // As a workaround, ACTION_CONFIGURATION_CHANGED broadcast is sent from
        // here
        
        sendBroadcast(new Intent(RCMConstants.ACTION_CONFIGURATION_CHANGED).putExtra(RCMConstants.EXTRA_NEW_CONFIG, newConfig));

        int orientation = (newConfig.orientation != Configuration.ORIENTATION_UNDEFINED) 
        		? newConfig.orientation
                : DeviceUtils.calcDisplayOrientation(this);

        int curTab = getActivityManager().getCurrentTab();
        getActivityManager().clearAllTabs(); 
        leakCleanUpChildsDrawables(getActivityManager());
        
        prepareContentView(orientation);

        getActivityManager().setOnTabChangedListener(this);
        setTabs(orientation, curTab);
    }
    
    private void buildView(int orientation) {
        prepareContentView(orientation);
        setDefaultKeyMode(DEFAULT_KEYS_DISABLE);        
        getActivityManager().setOnTabChangedListener(this);
        
        final int activeTab = getIntent().getIntExtra(TAB, TAB_RINGOUT_ID);
        setTabs(orientation, activeTab);
    }
    	
    /*
     * Set orientation is needed as TabActivity is horizontal by default
     */
    private void prepareContentView(int orientation){    	
		setContentView(R.layout.tab_content);
    }
    
//    private void setTabs(int orientation, int activeTab) {
//    	final ActivityManager tabHost = getActivityManager();
//        
//    	Intent intent = new Intent(RCMConstants.ACTION_LIST_MESSAGES);
//        intent.setClass(this, Messages.class);
//        tabHost.addScreen(tabHost.newTabSpec(TAB_MESSAGES, intent));
//        
//        intent = new Intent(RCMConstants.ACTION_LIST_CONTACTS);
//        intent.setClass(this, ContactsListActivity.class);        
//        tabHost.addScreen(tabHost.newTabSpec(TAB_CONTACTS, intent));  
//        
//        intent = new Intent(RCMConstants.ACTION_LIST_FAVORITES);
//        intent.setClass(this, FavoritesListActivity.class);
//        tabHost.addTab(tabHost
//        		.newTabSpec(TAB_FAVORITES)
//        		.setIndicator(createIndicatorView(orientation, R.string.tab_name_favorites, R.drawable.tab_favorites))
//        		.setContent(intent));
//        intent = new Intent(RCMConstants.ACTION_LIST_MESSAGES);
//        intent.setClass(this, Messages.class);
//        tabHost.addTab(tabHost
//        		.newTabSpec(TAB_MESSAGES)
//        		.setIndicator(createMessagesIndicatorView(orientation))
//        		.setContent(intent));        
//
////        tabHost.setCurrentTab(activeTab);
//        
//        intent.setClass(this, ContactsListActivity.class);        
//        tabHost.addScreen(tabHost.newTabSpec(TAB_FAVORITES, intent));
//    
//        intent = new Intent(RCMConstants.ACTION_LIST_RECENT_CALL);
//        intent.setClass(this, CallLogListActivity.class);
//        tabHost.addScreen(tabHost.newTabSpec(TAB_CALL_LOG, intent));
//
//        tabHost.setCurrentScreen(activeTab);
//    }
    
    private void setTabs(int orientation, int activeTab) {
    	final ActivityManager tabHost = getActivityManager();
    	
    	Intent intent = new Intent(RCMConstants.ACTION_LIST_MESSAGES);
        intent.setClass(this, Messages.class);
        tabHost.addScreen(tabHost.newTabSpec(TAB_MESSAGES, intent));
        
        intent = new Intent(RCMConstants.ACTION_LIST_CONTACTS);
        intent.setClass(this, ContactsListActivity.class);        
        tabHost.addScreen(tabHost.newTabSpec(TAB_CONTACTS, intent));  
        
        intent = new Intent(RCMConstants.ACTION_LIST_FAVORITES);
        intent.setClass(this, FavoritesListActivity.class);        
        tabHost.addScreen(tabHost.newTabSpec(TAB_FAVORITES, intent));
        
        intent = new Intent(RCMConstants.ACTION_LIST_RECENT_CALL);
        intent.setClass(this, CallLogListActivity.class);
        tabHost.addScreen(tabHost.newTabSpec(TAB_CALL_LOG, intent));

        tabHost.setCurrentScreen(activeTab);
    }
   

    public void onTabChanged(String tabId) {
        Activity activity = getLocalActivityManager().getActivity(tabId);
        if (activity != null) {
            activity.onWindowFocusChanged(true);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
        case KeyEvent.KEYCODE_CALL: {
            RcAlertDialog.getBuilder(RingCentral.this)
            	.setTitle(R.string.app_name)
            	.setMessage(R.string.switch_to_dialer)
            	.setIcon(R.drawable.symbol_help)
            	.setPositiveButton(R.string.dialog_btn_ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            setResult(RESULT_OK);
                            if (LogSettings.ENGINEERING) {
                                if (getActivityManager().getCurrentTab() == 0) {
                                    EngLog.e(TAG, "TODO: If DIALER page is opened and we press the CALL button -> make RingCentral Call");
                                }
                            }
                        }
                    })
                .setNegativeButton(R.string.dialog_btn_cancel, 
                	new DialogInterface.OnClickListener() {
                		public void onClick(DialogInterface dialog, int whichButton) {
                		}
                	})
                .show();
            return true;
        }
        }
        return super.onKeyDown(keyCode, event);
    }
    
    
	private class SwitchTabReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (LogSettings.ENGINEERING) {
				EngLog.i(TAG, "receiverd tab change : " + intent.getIntExtra(RingCentral.TAB, -1) + " " + intent.getStringExtra(RingCentral.TAB));
			}

			if (intent.hasExtra(RingCentral.TAB) && getActivityManager().getCurrentTab() != intent.getIntExtra(RingCentral.TAB, RingCentral.TAB_RINGOUT_ID)) {
				getActivityManager().setCurrentScreen(intent.getIntExtra(RingCentral.TAB, RingCentral.TAB_RINGOUT_ID));
			} else if(intent.hasExtra(RingCentral.TAB_TAG) && !getActivityManager().getCurrentTabTag().equals(intent.getStringExtra(RingCentral.TAB))) {
				getActivityManager().setCurrentTabByTag(intent.getStringExtra(RingCentral.TAB_TAG));
			}
		}
	}
    
    private class VoipRingStartReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LogSettings.ENGINEERING){
                EngLog.i(TAG, "received ring started");
            }
            long time = intent.getLongExtra(RCMConstants.EXTRA_VOIP_RING_STARTED, 0);
            PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.CALL_INITIZED, time);
        }
    }
    
    private class IncomingVoipRingtoneStartedReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LogSettings.ENGINEERING){
                EngLog.i(TAG, "Incoming voip ringtone started");
            }
            long time = intent.getLongExtra(RCMConstants.EXTRA_VOIP_RING_STARTED, 0);
            PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.INCOMING_VOIP_RINGTONE, time);
        }
    }
    
    private class IncomingVoipActivityOpenedReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LogSettings.ENGINEERING){
                EngLog.i(TAG, "Incoming VoIP activity started");
            }
            long time = intent.getLongExtra(RCMConstants.EXTRA_INVOIP_ACTIVITY_STARTED, 0);
            PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.INCOMING_TO_INVOIP_ACTIVITY, time);
        }
    }
    
    private class IncomingVoipActivityAnswerReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LogSettings.ENGINEERING){
                EngLog.i(TAG, "Incoming VoIP answer taped");
            }
            long time = intent.getLongExtra(RCMConstants.EXTRA_INVOIP_ANSWER_TIME, 0);
            PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.INCOMING_ANSWER, time);
        }
    }
    
    private class VoipCallStatusActivityOpenedReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LogSettings.ENGINEERING){
                EngLog.i(TAG, "Voip Call status activity opened");
            }
            long time = intent.getLongExtra(RCMConstants.EXTRA_CALL_STATE_ACTIVITY_STARTED, 0);
            PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.CALL_FROM_CALLLOG, time);
            PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.DIALER_CALL, time);
            PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.INCOMING_ANSWER, time);
            PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.BACK_TO_ACTIVITY, time);
            PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.CALL_FROM_CONTACTS);
        }
    }
    
    private class IncomingVoipActivityAnswerAndHoldReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LogSettings.ENGINEERING){
                EngLog.i(TAG, "Voip Call status activity opened");
            }
            long time = intent.getLongExtra(RCMConstants.EXTRA_INVOIP_ANSWER_AND_HOLD_TIME, 0);
            PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.BACK_TO_ACTIVITY, time);
        }
    }
    
    public static void fireSwitchTabEvent(Context context, int newTab){
    	if (LogSettings.ENGINEERING){
            EngLog.i(TAG, "fireSwitchTabEvent, new Tab : " + newTab);
        }
    	
    	final Intent intent = new Intent(RCMConstants.ACTION_CURRENT_TAB_CHANGED);
    	intent.putExtra(RingCentral.TAB, newTab);
    	context.sendBroadcast(intent);    	    	
    }

    // ====================== Workaround Section (BN) ==========================
    // == Reduces memory leaks. The section can be just added to any Activity ==
    // == Call leakCleanUpRootView onDestroy and before setting new root view ==
    
    @Override
    public void setContentView(int layoutResID) {
        ViewGroup rootView = (ViewGroup) 
            LayoutInflater.from(this).inflate(layoutResID, null);
        setContentView(rootView);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        leakCleanUpRootView();
        mLeakContentView = (ViewGroup) view;
    }

    @Override
    public void setContentView(View view, LayoutParams params) {
        super.setContentView(view, params);
        leakCleanUpRootView();
        mLeakContentView = (ViewGroup) view;
    }
    
    /**
     * Cleanup root view to reduce memory leaks.
     */
    private void leakCleanUpRootView() {
        if (mLeakContentView != null) {
            ViewGroup v = mLeakContentView;
            mLeakContentView = null;
            mStackRecursions = 0;
            leakCleanUpChildsDrawables(v);
            System.gc();
        }
    }
    
    /**
     * Clean-up Drawables in the view including child.
     * 
     * @param v
     */
    private void leakCleanUpChildsDrawables(View v) {
        if (v != null) {
            try {
                ViewGroup group = (ViewGroup) v;
                int childs = group.getChildCount();
                for (int i = 0; i < childs; i++) {
                    if (LEAKS_CLEAN_UP_STACK_RECURSIONS_LIMIT > mStackRecursions) {
                        mStackRecursions++;
                        leakCleanUpChildsDrawables(group.getChildAt(i));
                        mStackRecursions--;
                    } else {
                        break;
                    }
                }
            } catch (java.lang.Throwable th) {
            }
            leakCleanUpDrawables(v);
        }
    }

    /**
     * Keeps recursions number for memory clean-ups.
     */
    private int mStackRecursions;
    
    /**
     * Limit of leaks clean-ups stack recursions.
     */
    private static final int LEAKS_CLEAN_UP_STACK_RECURSIONS_LIMIT = 256;
    
    /**
     * Cleans drawables of in the view.
     * 
     * @param v the view to clean-up
     */
    private void leakCleanUpDrawables(View v) {
        if (v == null) {
            return;
        }
            
        try {
            v.setBackgroundDrawable(null);
        } catch (java.lang.Throwable th) {
        }

        try {
            ImageView imageView = (ImageView) v;
            imageView.setImageDrawable(null);
            imageView.setBackgroundDrawable(null);
        } catch (java.lang.Throwable th) {
        }
    }
    
   
    
    /**
     * Keeps current root view.
     */
    private ViewGroup mLeakContentView = null;
}