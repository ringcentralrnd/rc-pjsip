/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser.impl;

import java.util.Vector;


/**
 * Auto-generated. 
 * Phone data version: 1
 * Phone data revision: 12
 */
final class PhoneData {
    private static Vector countries;

    public static Vector getCountries() {
        if (countries == null) {
            countries = new Vector();
            init();
        }
        return countries;
    }

    private static void init0() {
        opt1(opt0(256, PhoneDataConstantPool0.cnst0,
                PhoneDataConstantPool0.cnst1));
        opt1(opt0(215, PhoneDataConstantPool0.cnst2,
                PhoneDataConstantPool0.cnst3));
        opt1(opt0(220, PhoneDataConstantPool0.cnst4,
                PhoneDataConstantPool0.cnst5));
        opt1(opt0(127, PhoneDataConstantPool0.cnst6,
                PhoneDataConstantPool0.cnst7));
        opt1(opt0(0, PhoneDataConstantPool0.cnst8, 
                PhoneDataConstantPool0.cnst9));
        opt1(opt0(153, PhoneDataConstantPool0.cnst10,
                PhoneDataConstantPool0.cnst11));
        opt1(opt0(177, PhoneDataConstantPool0.cnst12,
                PhoneDataConstantPool0.cnst13));
        opt1(opt0(145, PhoneDataConstantPool0.cnst14,
                PhoneDataConstantPool0.cnst15));
        opt1(opt0(208, PhoneDataConstantPool0.cnst16,
                PhoneDataConstantPool0.cnst17));
        opt1(opt0(199, PhoneDataConstantPool0.cnst18,
                PhoneDataConstantPool0.cnst19));
        opt1(opt0(32, PhoneDataConstantPool0.cnst20,
                PhoneDataConstantPool0.cnst21));
    }


    private static void init2() {
        opt1(opt0(187, PhoneDataConstantPool4.cnst1359,
                PhoneDataConstantPool4.cnst1360));
        opt1(opt0(136, PhoneDataConstantPool4.cnst1361,
                PhoneDataConstantPool4.cnst1362));
        opt1(opt0(3, PhoneDataConstantPool4.cnst1363,
                PhoneDataConstantPool4.cnst1364));
        opt1(opt0(77, PhoneDataConstantPool4.cnst1365,
                PhoneDataConstantPool4.cnst1366));
        opt1(opt0(31, PhoneDataConstantPool4.cnst1367,
                PhoneDataConstantPool4.cnst1368));
        opt1(opt0(218, PhoneDataConstantPool4.cnst1369,
                PhoneDataConstantPool4.cnst1370));
        opt1(opt0(89, PhoneDataConstantPool4.cnst1371,
                PhoneDataConstantPool4.cnst1372));
        opt1(opt0(152, PhoneDataConstantPool4.cnst1373,
                PhoneDataConstantPool4.cnst1374));
        opt1(opt0(128, PhoneDataConstantPool4.cnst1375,
                PhoneDataConstantPool4.cnst1376));
        opt1(opt0(140, PhoneDataConstantPool4.cnst1377,
                PhoneDataConstantPool4.cnst1378));
        opt1(opt0(18, PhoneDataConstantPool4.cnst1379,
                PhoneDataConstantPool4.cnst1380));
        opt1(opt0(102, PhoneDataConstantPool4.cnst1381,
                PhoneDataConstantPool4.cnst1382));
        opt1(opt0(115, PhoneDataConstantPool4.cnst1383,
                PhoneDataConstantPool4.cnst1384));
        opt1(opt0(0, PhoneDataConstantPool4.cnst1385,
                PhoneDataConstantPool4.cnst1386));
        opt1(opt0(110, PhoneDataConstantPool4.cnst1387,
                PhoneDataConstantPool4.cnst1388));
        opt1(opt0(164, PhoneDataConstantPool4.cnst1389,
                PhoneDataConstantPool4.cnst1390));

    }

    private static void init3() {
        opt1(opt0(224, PhoneDataConstantPool4.cnst1391,
                PhoneDataConstantPool4.cnst1392));
        opt1(opt0(45, PhoneDataConstantPool4.cnst1393,
                PhoneDataConstantPool0.cnst31));
        opt1(opt0(60, PhoneDataConstantPool4.cnst1394,
                PhoneDataConstantPool4.cnst1395));
        opt1(opt0(123, PhoneDataConstantPool4.cnst1396,
                PhoneDataConstantPool4.cnst1397));
        opt1(opt0(178, PhoneDataConstantPool4.cnst1398,
                PhoneDataConstantPool4.cnst1399));
        opt1(opt0(129, PhoneDataConstantPool4.cnst1400,
                PhoneDataConstantPool4.cnst1401));
        opt1(opt0(194, PhoneDataConstantPool4.cnst1402,
                PhoneDataConstantPool4.cnst1403));
        opt1(opt0(200, PhoneDataConstantPool4.cnst1404,
                PhoneDataConstantPool4.cnst1405));
        opt1(opt0(36, PhoneDataConstantPool4.cnst1406,
                PhoneDataConstantPool4.cnst1407));
        opt1(opt0(237, PhoneDataConstantPool4.cnst1408,
                PhoneDataConstantPool4.cnst1409));
        opt1(opt0(44, PhoneDataConstantPool4.cnst1410,
                PhoneDataConstantPool4.cnst1411));
        opt1(opt0(88, PhoneDataConstantPool4.cnst1412,
                PhoneDataConstantPool4.cnst1413));
        opt1(opt0(4, PhoneDataConstantPool4.cnst1414,
                PhoneDataConstantPool4.cnst1415));
        opt1(opt0(80, PhoneDataConstantPool4.cnst1416,
                PhoneDataConstantPool4.cnst1417));
        opt1(opt0(20, PhoneDataConstantPool4.cnst1418,
                PhoneDataConstantPool4.cnst1419));
        opt1(opt0(107, PhoneDataConstantPool4.cnst1420,
                PhoneDataConstantPool4.cnst1421));

    }

    private static void init4() {
        opt1(opt0(0, PhoneDataConstantPool4.cnst1422,
                PhoneDataConstantPool4.cnst1423));
        opt1(opt0(93, PhoneDataConstantPool4.cnst1424,
                PhoneDataConstantPool4.cnst1425));
        opt1(opt0(238, PhoneDataConstantPool4.cnst1426,
                PhoneDataConstantPool4.cnst1427));
        opt1(opt0(211, PhoneDataConstantPool4.cnst1428,
                PhoneDataConstantPool4.cnst1429));
        opt1(opt0(147, PhoneDataConstantPool4.cnst1430,
                PhoneDataConstantPool4.cnst1431));
        opt1(opt0(48, PhoneDataConstantPool4.cnst1432,
                PhoneDataConstantPool4.cnst1433));
        opt1(opt0(239, PhoneDataConstantPool4.cnst1434,
                PhoneDataConstantPool0.cnst57));
        opt1(opt0(201, PhoneDataConstantPool4.cnst1435,
                PhoneDataConstantPool4.cnst1436));
        opt1(opt0(119, PhoneDataConstantPool4.cnst1437,
                PhoneDataConstantPool4.cnst1438));
        opt1(opt0(227, PhoneDataConstantPool4.cnst1439,
                PhoneDataConstantPool4.cnst1440));
        opt1(opt0(58, PhoneDataConstantPool4.cnst1441,
                PhoneDataConstantPool4.cnst1442));
    }

    private static void init5() {
        c = new Country();
        c.localLength = -1;
        c.flagIndex = 240;
        c.name = PhoneDataConstantPool4.cnst1443;
        c.code = PhoneDataConstantPool4.cnst1444;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool4.cnst1445;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool4.cnst1447;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool4.cnst1448;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool0.cnst17;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool4.cnst1449;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool4.cnst1450;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool4.cnst1451;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool4.cnst1452;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool4.cnst1453;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool4.cnst1454;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init6() {
        opt1(opt0(50, PhoneDataConstantPool4.cnst1455,
                PhoneDataConstantPool4.cnst1444));
        opt1(opt0(141, PhoneDataConstantPool4.cnst1456,
                PhoneDataConstantPool4.cnst1457));
        opt1(opt0(241, PhoneDataConstantPool4.cnst1458,
                PhoneDataConstantPool4.cnst1459));
        opt1(opt0(43, PhoneDataConstantPool4.cnst1460,
                PhoneDataConstantPool4.cnst1461));
        opt1(opt0(55, PhoneDataConstantPool4.cnst1462,
                PhoneDataConstantPool4.cnst1463));
        opt1(opt0(112, PhoneDataConstantPool4.cnst1464,
                PhoneDataConstantPool4.cnst1465));
        opt1(opt0(171, PhoneDataConstantPool4.cnst1466,
                PhoneDataConstantPool4.cnst1467));
        opt1(opt0(203, PhoneDataConstantPool4.cnst1468,
                PhoneDataConstantPool4.cnst1469));
        opt1(opt0(205, PhoneDataConstantPool4.cnst1470,
                PhoneDataConstantPool4.cnst1471));
        opt1(opt0(122, PhoneDataConstantPool4.cnst1472,
                PhoneDataConstantPool4.cnst1473));
        opt1(opt0(217, PhoneDataConstantPool4.cnst1474,
                PhoneDataConstantPool4.cnst1475));
        opt1(opt0(189, PhoneDataConstantPool4.cnst1476,
                PhoneDataConstantPool4.cnst1477));
        opt1(opt0(228, PhoneDataConstantPool4.cnst1478,
                PhoneDataConstantPool4.cnst1479));

    }

    private static void init7() {
        opt1(opt0(25, PhoneDataConstantPool4.cnst1480,
                PhoneDataConstantPool4.cnst1481));
        opt1(opt0(156, PhoneDataConstantPool4.cnst1482,
                PhoneDataConstantPool4.cnst1483));
        opt1(opt0(94, PhoneDataConstantPool4.cnst1484,
                PhoneDataConstantPool4.cnst1485));
        opt1(opt0(76, PhoneDataConstantPool4.cnst1486,
                PhoneDataConstantPool4.cnst1487));
        opt1(opt0(184, PhoneDataConstantPool4.cnst1488,
                PhoneDataConstantPool4.cnst1489));
        opt1(opt0(38, PhoneDataConstantPool4.cnst1490,
                PhoneDataConstantPool4.cnst1491));
        opt1(opt0(35, PhoneDataConstantPool4.cnst1492,
                PhoneDataConstantPool4.cnst1493));
        opt1(opt0(28, PhoneDataConstantPool4.cnst1494,
                PhoneDataConstantPool4.cnst1495));
        opt1(opt0(99, PhoneDataConstantPool4.cnst1496,
                PhoneDataConstantPool4.cnst1497));
    }

    private static void init8() {
        c = new Country();
        c.localLength = -1;
        c.flagIndex = 108;
        c.name = PhoneDataConstantPool4.cnst1498;
        c.code = PhoneDataConstantPool4.cnst1499;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool0.cnst59;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1500;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1501;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1502;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool4.cnst1366;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1503;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1504;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1505;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init9() {
        Country c = opt0(174, PhoneDataConstantPool5.cnst1506,
                PhoneDataConstantPool4.cnst1499);
        c.areaCodeLength = 3;
        c.localLength = 7;
        opt1(c);
    }

    private static void init10() {
        opt1(opt0(242, PhoneDataConstantPool5.cnst1507,
                PhoneDataConstantPool5.cnst1508));
        opt1(opt0(226, PhoneDataConstantPool5.cnst1509,
                PhoneDataConstantPool5.cnst1510));
        opt1(opt0(86, PhoneDataConstantPool5.cnst1511,
                PhoneDataConstantPool5.cnst1512));
        opt1(opt0(168, PhoneDataConstantPool5.cnst1513,
                PhoneDataConstantPool5.cnst1514));
        opt1(opt0(34, PhoneDataConstantPool5.cnst1515,
                PhoneDataConstantPool5.cnst1516));
        opt1(opt0(198, PhoneDataConstantPool5.cnst1517,
                PhoneDataConstantPool5.cnst1518));
        opt1(opt0(40, PhoneDataConstantPool5.cnst1519,
                PhoneDataConstantPool5.cnst1520));
        opt1(opt0(151, PhoneDataConstantPool5.cnst1521,
                PhoneDataConstantPool5.cnst1522));
        opt1(opt0(213, PhoneDataConstantPool5.cnst1523,
                PhoneDataConstantPool5.cnst1524));
        opt1(opt0(225, PhoneDataConstantPool5.cnst1525,
                PhoneDataConstantPool5.cnst1526));
        opt1(opt0(137, PhoneDataConstantPool5.cnst1527,
                PhoneDataConstantPool5.cnst1528));
        opt1(opt0(111, PhoneDataConstantPool5.cnst1529,
                PhoneDataConstantPool5.cnst1530));
        opt1(opt0(37, PhoneDataConstantPool5.cnst1531,
                PhoneDataConstantPool5.cnst1532));

    }

    private static void init11() {
        opt1(opt0(75, PhoneDataConstantPool5.cnst1533,
                PhoneDataConstantPool5.cnst1534));
        opt1(opt0(67, PhoneDataConstantPool5.cnst1535,
                PhoneDataConstantPool5.cnst1536));
        opt1(opt0(81, PhoneDataConstantPool5.cnst1537,
                PhoneDataConstantPool5.cnst1538));
        opt1(opt0(192, PhoneDataConstantPool5.cnst1539,
                PhoneDataConstantPool5.cnst1540));
        opt1(opt0(179, PhoneDataConstantPool5.cnst1541,
                PhoneDataConstantPool5.cnst1542));
        opt1(opt0(210, PhoneDataConstantPool5.cnst1543,
                PhoneDataConstantPool5.cnst1544));
        opt1(opt0(146, PhoneDataConstantPool5.cnst1545,
                PhoneDataConstantPool5.cnst1546));
        opt1(opt0(180, PhoneDataConstantPool5.cnst1547,
                PhoneDataConstantPool5.cnst1548));
        opt1(opt0(113, PhoneDataConstantPool5.cnst1549,
                PhoneDataConstantPool5.cnst1550));
        opt1(opt0(92, PhoneDataConstantPool5.cnst1551,
                PhoneDataConstantPool5.cnst1552));
        opt1(opt0(103, PhoneDataConstantPool5.cnst1553,
                PhoneDataConstantPool5.cnst1554));
        opt1(opt0(26, PhoneDataConstantPool5.cnst1555,
                PhoneDataConstantPool5.cnst1556));
        opt1(opt0(118, PhoneDataConstantPool5.cnst1557,
                PhoneDataConstantPool5.cnst1558));
        opt1(opt0(234, PhoneDataConstantPool5.cnst1559,
                PhoneDataConstantPool5.cnst1560));

    }

    private static void init12() {
        opt1(opt0(150, PhoneDataConstantPool5.cnst1561,
                PhoneDataConstantPool5.cnst1562));
        opt1(opt0(243, PhoneDataConstantPool5.cnst1563,
                PhoneDataConstantPool5.cnst1564));
        opt1(opt0(158, PhoneDataConstantPool5.cnst1565,
                PhoneDataConstantPool5.cnst1566));
        opt1(opt0(214, PhoneDataConstantPool5.cnst1567,
                PhoneDataConstantPool5.cnst1568));
        opt1(opt0(54, PhoneDataConstantPool5.cnst1569,
                PhoneDataConstantPool5.cnst1570));
        opt1(opt0(70, PhoneDataConstantPool5.cnst1571,
                PhoneDataConstantPool5.cnst1572));
        opt1(opt0(96, PhoneDataConstantPool5.cnst1573,
                PhoneDataConstantPool5.cnst1574));
        opt1(opt0(163, PhoneDataConstantPool5.cnst1575,
                PhoneDataConstantPool5.cnst1576));
        opt1(opt0(219, PhoneDataConstantPool5.cnst1577,
                PhoneDataConstantPool5.cnst1578));
        opt1(opt0(160, PhoneDataConstantPool5.cnst1579,
                PhoneDataConstantPool5.cnst1580));
        opt1(opt0(105, PhoneDataConstantPool5.cnst1581,
                PhoneDataConstantPool5.cnst1582));
        opt1(opt0(244, PhoneDataConstantPool5.cnst1583,
                PhoneDataConstantPool5.cnst1584));
        opt1(opt0(245, PhoneDataConstantPool5.cnst1585,
                PhoneDataConstantPool5.cnst1586));
        opt1(opt0(183, PhoneDataConstantPool5.cnst1587,
                PhoneDataConstantPool5.cnst1588));
        opt1(opt0(120, PhoneDataConstantPool5.cnst1589,
                PhoneDataConstantPool5.cnst1590));

    }

    private static void init13() {
        opt1(opt0(235, PhoneDataConstantPool5.cnst1591,
                PhoneDataConstantPool5.cnst1592));
        opt1(opt0(1, PhoneDataConstantPool5.cnst1593,
                PhoneDataConstantPool5.cnst1594));
        opt1(opt0(193, PhoneDataConstantPool5.cnst1595,
                PhoneDataConstantPool5.cnst1596));
        opt1(opt0(246, PhoneDataConstantPool5.cnst1597,
                PhoneDataConstantPool5.cnst1598));
        opt1(opt0(126, PhoneDataConstantPool5.cnst1599,
                PhoneDataConstantPool4.cnst1454));
        opt1(opt0(29, PhoneDataConstantPool5.cnst1600,
                PhoneDataConstantPool5.cnst1601));
        opt1(opt0(91, PhoneDataConstantPool5.cnst1602,
                PhoneDataConstantPool5.cnst1603));
        opt1(opt0(114, PhoneDataConstantPool5.cnst1604,
                PhoneDataConstantPool5.cnst1605));
        opt1(opt0(124, PhoneDataConstantPool5.cnst1606,
                PhoneDataConstantPool5.cnst1607));
        opt1(opt0(175, PhoneDataConstantPool5.cnst1608,
                PhoneDataConstantPool5.cnst1609));
        opt1(opt0(247, PhoneDataConstantPool5.cnst1610,
                PhoneDataConstantPool5.cnst1611));
        opt1(opt0(134, PhoneDataConstantPool5.cnst1612,
                PhoneDataConstantPool5.cnst1613));
        opt1(opt0(161, PhoneDataConstantPool5.cnst1614,
                PhoneDataConstantPool5.cnst1615));
    }

    private static void init14() {
        opt1(opt0(16, PhoneDataConstantPool5.cnst1616,
                PhoneDataConstantPool4.cnst1453));
        opt1(opt0(90, PhoneDataConstantPool5.cnst1617,
                PhoneDataConstantPool5.cnst1618));
        opt1(opt0(71, PhoneDataConstantPool5.cnst1619,
                PhoneDataConstantPool5.cnst1620));
        opt1(opt0(83, PhoneDataConstantPool5.cnst1621,
                PhoneDataConstantPool5.cnst1622));
        opt1(opt0(248, PhoneDataConstantPool5.cnst1623,
                PhoneDataConstantPool5.cnst1624));
        opt1(opt0(72, PhoneDataConstantPool5.cnst1625,
                PhoneDataConstantPool5.cnst1626));
        opt1(opt0(69, PhoneDataConstantPool5.cnst1627,
                PhoneDataConstantPool5.cnst1628));
        opt1(opt0(142, PhoneDataConstantPool5.cnst1629,
                PhoneDataConstantPool5.cnst1630));
        opt1(opt0(229, PhoneDataConstantPool5.cnst1631,
                PhoneDataConstantPool5.cnst1632));
        opt1(opt0(130, PhoneDataConstantPool5.cnst1633,
                PhoneDataConstantPool5.cnst1634));
        opt1(opt0(249, PhoneDataConstantPool5.cnst1635,
                PhoneDataConstantPool5.cnst1636));
        opt1(opt0(64, PhoneDataConstantPool5.cnst1637,
                PhoneDataConstantPool5.cnst1638));
        opt1(opt0(97, PhoneDataConstantPool5.cnst1639,
                PhoneDataConstantPool4.cnst1452));
        opt1(opt0(65, PhoneDataConstantPool5.cnst1640,
                PhoneDataConstantPool5.cnst1641));
        opt1(opt0(166, PhoneDataConstantPool5.cnst1642,
                PhoneDataConstantPool5.cnst1643));
        opt1(opt0(173, PhoneDataConstantPool5.cnst1644,
                PhoneDataConstantPool5.cnst1645));
        opt1(opt0(135, PhoneDataConstantPool5.cnst1646,
                PhoneDataConstantPool5.cnst1647));
    }

    private static void init15() {
        opt1(opt0(169, PhoneDataConstantPool5.cnst1648,
                PhoneDataConstantPool5.cnst1649));
        opt1(opt0(236, PhoneDataConstantPool5.cnst1650,
                PhoneDataConstantPool5.cnst1651));
        opt1(opt0(190, PhoneDataConstantPool5.cnst1652,
                PhoneDataConstantPool5.cnst1653));
        opt1(opt0(78, PhoneDataConstantPool5.cnst1654,
                PhoneDataConstantPool5.cnst1655));
        opt1(opt0(132, PhoneDataConstantPool5.cnst1656,
                PhoneDataConstantPool5.cnst1657));
        opt1(opt0(21, PhoneDataConstantPool5.cnst1658,
                PhoneDataConstantPool5.cnst1659));
        opt1(opt0(53, PhoneDataConstantPool5.cnst1660,
                PhoneDataConstantPool5.cnst1661));
        opt1(opt0(167, PhoneDataConstantPool5.cnst1662,
                PhoneDataConstantPool4.cnst1451));
        opt1(opt0(250, PhoneDataConstantPool5.cnst1663,
                PhoneDataConstantPool5.cnst1664));
        opt1(opt0(133, PhoneDataConstantPool5.cnst1665,
                PhoneDataConstantPool5.cnst1666));
        opt1(opt0(202, PhoneDataConstantPool5.cnst1667,
                PhoneDataConstantPool5.cnst1668));
        opt1(opt0(56, PhoneDataConstantPool5.cnst1669,
                PhoneDataConstantPool5.cnst1670));
        opt1(opt0(13, PhoneDataConstantPool5.cnst1671,
                PhoneDataConstantPool5.cnst1672));
        opt1(opt0(121, PhoneDataConstantPool5.cnst1673,
                PhoneDataConstantPool5.cnst1674));
        opt1(opt0(84, PhoneDataConstantPool5.cnst1675,
                PhoneDataConstantPool5.cnst1505));
        opt1(opt0(14, PhoneDataConstantPool5.cnst1676,
                PhoneDataConstantPool5.cnst1677));

    }

    private static void init16() {
        opt1(opt0(143, PhoneDataConstantPool5.cnst1678,
                PhoneDataConstantPool5.cnst1679));
        opt1(opt0(61, PhoneDataConstantPool5.cnst1680,
                PhoneDataConstantPool5.cnst1681));
        opt1(opt0(51, PhoneDataConstantPool5.cnst1682,
                PhoneDataConstantPool5.cnst1683));
        opt1(opt0(117, PhoneDataConstantPool5.cnst1684,
                PhoneDataConstantPool5.cnst1685));
        opt1(opt0(79, PhoneDataConstantPool5.cnst1686,
                PhoneDataConstantPool5.cnst1687));
        opt1(opt0(0, PhoneDataConstantPool5.cnst1688,
                PhoneDataConstantPool5.cnst1689));
        opt1(opt0(0, PhoneDataConstantPool5.cnst1690,
                PhoneDataConstantPool0.cnst68));
        opt1(opt0(46, PhoneDataConstantPool5.cnst1691,
                PhoneDataConstantPool5.cnst1692));
        opt1(opt0(251, PhoneDataConstantPool5.cnst1693,
                PhoneDataConstantPool5.cnst1694));
        opt1(opt0(9, PhoneDataConstantPool5.cnst1695,
                PhoneDataConstantPool5.cnst1696));
        opt1(opt0(149, PhoneDataConstantPool5.cnst1697,
                PhoneDataConstantPool4.cnst1450));
        opt1(opt0(165, PhoneDataConstantPool5.cnst1698,
                PhoneDataConstantPool5.cnst1699));
        opt1(opt0(57, PhoneDataConstantPool5.cnst1700,
                PhoneDataConstantPool5.cnst1701));
        opt1(opt0(188, PhoneDataConstantPool5.cnst1702,
                PhoneDataConstantPool5.cnst1703));
        opt1(opt0(23, PhoneDataConstantPool5.cnst1704,
                PhoneDataConstantPool5.cnst1705));
        opt1(opt0(100, PhoneDataConstantPool5.cnst1706,
                PhoneDataConstantPool5.cnst1707));
        opt1(opt0(148, PhoneDataConstantPool5.cnst1708,
                PhoneDataConstantPool5.cnst1504));
        opt1(opt0(74, PhoneDataConstantPool5.cnst1709,
                PhoneDataConstantPool5.cnst1710));
    }

    private static void init17() {
        opt1(opt0(125, PhoneDataConstantPool5.cnst1711,
                PhoneDataConstantPool5.cnst1712));
        opt1(opt0(109, PhoneDataConstantPool5.cnst1713,
                PhoneDataConstantPool5.cnst1714));
        opt1(opt0(52, PhoneDataConstantPool5.cnst1715,
                PhoneDataConstantPool5.cnst1716));
        opt1(opt0(185, PhoneDataConstantPool5.cnst1717,
                PhoneDataConstantPool5.cnst1718));
        opt1(opt0(182, PhoneDataConstantPool5.cnst1719,
                PhoneDataConstantPool5.cnst1720));
        opt1(opt0(66, PhoneDataConstantPool5.cnst1721,
                PhoneDataConstantPool0.cnst26));
    }

    private static void init18() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 170;
        c.name = PhoneDataConstantPool5.cnst1722;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1725;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1726;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init19() {
        // Canada
        c = new Country();
        c.areaCodeLength = 3;
        c.localLength = 7;
        c.flagIndex = 39;
        c.name = PhoneDataConstantPool5.cnst1727;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1728;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool4.cnst1493;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1609;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1729;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1730;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1731;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1732;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1733;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1734;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1735;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1570;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1736;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1737;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1738;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1739;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1740;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1741;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1742;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1743;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1744;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1745;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1746;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1747;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1748;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1749;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1750;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1751;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        
        sPhns = new Vector();
        spPhone = new SpecialPhone();
        spPhone.code = PhoneDataConstantPool5.cnst1794;
        spPhone.type = SpecialPhone.SpecialPhoneType.getByCode(SpecialPhone.SpecialPhoneType.enPDSPTEmergency);
        sPhns.addElement(spPhone);
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        f.format = PhoneDataConstantPool5.cnst1798;
        fmtg.internationalFormat = f;
        f = new Format();
        f.format = PhoneDataConstantPool5.cnst1799;
        fmtg.localFormat = f;
        f = new Format();
        f.format = PhoneDataConstantPool5.cnst1800;
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init20() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 104;
        c.name = PhoneDataConstantPool5.cnst1752;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1753;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init21() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 196;
        c.name = PhoneDataConstantPool5.cnst1754;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1755;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init22() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 212;
        c.name = PhoneDataConstantPool5.cnst1756;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1757;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init23() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 63;
        c.name = PhoneDataConstantPool5.cnst1758;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1759;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init24() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 197;
        c.name = PhoneDataConstantPool5.cnst1760;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1761;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init25() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 62;
        c.name = PhoneDataConstantPool5.cnst1762;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1763;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init26() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 252;
        c.name = PhoneDataConstantPool5.cnst1764;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1765;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init27() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 5;
        c.name = PhoneDataConstantPool5.cnst1766;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1767;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init28() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 87;
        c.name = PhoneDataConstantPool5.cnst1768;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1769;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init29() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 253;
        c.name = PhoneDataConstantPool5.cnst1770;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1638;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init30() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 139;
        c.name = PhoneDataConstantPool5.cnst1771;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1772;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init31() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 216;
        c.name = PhoneDataConstantPool5.cnst1773;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1774;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init32() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 85;
        c.name = PhoneDataConstantPool5.cnst1775;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1776;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init33() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 27;
        c.name = PhoneDataConstantPool5.cnst1777;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1778;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init34() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 42;
        c.name = PhoneDataConstantPool5.cnst1779;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1780;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init35() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 231;
        c.name = PhoneDataConstantPool5.cnst1781;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1782;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init36() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 230;
        c.name = PhoneDataConstantPool5.cnst1783;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1784;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init37() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 10;
        c.name = PhoneDataConstantPool5.cnst1785;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool4.cnst1405;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init38() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 8;
        c.name = PhoneDataConstantPool5.cnst1786;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1679;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init39() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 22;
        c.name = PhoneDataConstantPool5.cnst1787;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool4.cnst1409;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init40() {
        c = new Country();
        c.localLength = 7;
        c.flagIndex = 19;
        c.name = PhoneDataConstantPool5.cnst1788;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        area = new Area();
        area.code = PhoneDataConstantPool5.cnst1683;
        area.name = PhoneDataConstantPool4.cnst1446;
        ars.addElement(area);
        c.areas = ars;
        srvFtrs = new Vector();
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        fmtg.internationalFormat = f;
        f = new Format();
        fmtg.localFormat = f;
        f = new Format();
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init41() {
        // USA
        c = new Country();
        c.areaCodeLength = 3;
        c.localLength = 7;
        c.flagIndex = 223;
        c.name = PhoneDataConstantPool5.cnst1789;
        c.code = PhoneDataConstantPool5.cnst1723;
        c.internationalPrefix = PhoneDataConstantPool5.cnst1724;
        c.nationalPrefix = PhoneDataConstantPool5.cnst1723;
        ars = new Vector();
        c.areas = ars;
        srvFtrs = new Vector();
        sf = new ServiceFeature();
        sf.sequence = PhoneDataConstantPool5.cnst1790;
        sf.type = ServiceFeature.ServiceFeatureType.getByCode(ServiceFeature.ServiceFeatureType.enPDSFTBlockCLID);
        srvFtrs.addElement(sf);
        sf = new ServiceFeature();
        sf.sequence = PhoneDataConstantPool5.cnst1791;
        sf.type = ServiceFeature.ServiceFeatureType.getByCode(ServiceFeature.ServiceFeatureType.enPDSFTCallback);
        srvFtrs.addElement(sf);
        sf = new ServiceFeature();
        sf.sequence = PhoneDataConstantPool5.cnst1792;
        sf.type = ServiceFeature.ServiceFeatureType.getByCode(ServiceFeature.ServiceFeatureType.enPDSFTVoiceMail);
        srvFtrs.addElement(sf);
        sf = new ServiceFeature();
        sf.sequence = PhoneDataConstantPool5.cnst1793;
        sf.type = ServiceFeature.ServiceFeatureType.getByCode(ServiceFeature.ServiceFeatureType.enPDSFTUnknown);
        srvFtrs.addElement(sf);
        c.serviceFeatures = srvFtrs;
        sPhns = new Vector();
        spPhone = new SpecialPhone();
        spPhone.code = PhoneDataConstantPool5.cnst1794;
        spPhone.type = SpecialPhone.SpecialPhoneType.getByCode(SpecialPhone.SpecialPhoneType.enPDSPTEmergency);
        sPhns.addElement(spPhone);
        c.specialPhones = sPhns;
        fmtg = new Formatting();
        f = new Format();
        f.format = PhoneDataConstantPool5.cnst1798;
        fmtg.internationalFormat = f;
        f = new Format();
        f.format = PhoneDataConstantPool5.cnst1799;
        fmtg.localFormat = f;
        f = new Format();
        f.format = PhoneDataConstantPool5.cnst1800;
        fmtg.nationalFormat = f;
        lLfs = new Vector();
        fmtg.localLengthFormats = lLfs;
        c.formatting = fmtg;
        countries.addElement(c);
    }

    private static void init42() {
        opt1(opt0(206, PhoneDataConstantPool5.cnst1801,
                PhoneDataConstantPool5.cnst1802));
        opt1(opt0(98, PhoneDataConstantPool5.cnst1803,
                PhoneDataConstantPool5.cnst1804));
        opt1(opt0(162, PhoneDataConstantPool5.cnst1805,
                PhoneDataConstantPool5.cnst1806));
        opt1(opt0(0, PhoneDataConstantPool5.cnst1807,
                PhoneDataConstantPool5.cnst1808));
        opt1(opt0(0, PhoneDataConstantPool5.cnst1809,
                PhoneDataConstantPool0.cnst70));
        opt1(opt0(33, PhoneDataConstantPool5.cnst1810,
                PhoneDataConstantPool5.cnst1811));
        opt1(opt0(186, PhoneDataConstantPool5.cnst1812,
                PhoneDataConstantPool4.cnst1449));
        opt1(opt0(77, PhoneDataConstantPool5.cnst1813,
                PhoneDataConstantPool5.cnst1814));
        opt1(opt0(12, PhoneDataConstantPool5.cnst1815,
                PhoneDataConstantPool5.cnst1816));
        opt1(opt0(17, PhoneDataConstantPool5.cnst1817,
                PhoneDataConstantPool5.cnst1818));
        opt1(opt0(59, PhoneDataConstantPool5.cnst1819,
                PhoneDataConstantPool5.cnst1820));
        opt1(opt0(30, PhoneDataConstantPool5.cnst1821,
                PhoneDataConstantPool5.cnst1822));

    }

    private static void init43() {
        opt1(opt0(6, PhoneDataConstantPool5.cnst1823,
                PhoneDataConstantPool5.cnst1824));
        opt1(opt0(95, PhoneDataConstantPool5.cnst1825,
                PhoneDataConstantPool5.cnst1826));
        opt1(opt0(24, PhoneDataConstantPool5.cnst1827,
                PhoneDataConstantPool5.cnst1503));
        opt1(opt0(255, PhoneDataConstantPool5.cnst1828,
                PhoneDataConstantPool5.cnst1829));
        opt1(opt0(116, PhoneDataConstantPool5.cnst1830,
                PhoneDataConstantPool5.cnst1831));
        opt1(opt0(207, PhoneDataConstantPool5.cnst1832,
                PhoneDataConstantPool5.cnst1833));
        opt1(opt0(7, PhoneDataConstantPool5.cnst1834,
                PhoneDataConstantPool5.cnst1835));
        opt1(opt0(82, PhoneDataConstantPool5.cnst1836,
                PhoneDataConstantPool5.cnst1837));
        opt1(opt0(131, PhoneDataConstantPool5.cnst1838,
                PhoneDataConstantPool5.cnst1839));

    }

    private static void opt1(Country c) {
        c.areas = stubAreas;
        c.serviceFeatures = stubSFeatures;
        c.specialPhones = stubSPhones;
        c.formatting = stubFormatting;
        countries.addElement(c);
    }

    private static Country opt0(int flagIndex, String name, String code) {
        Country c = new Country();
        c.areaCodeLength = -1;
        c.localLength = -1;
        c.flagIndex = flagIndex;
        c.name = name;
        c.code = code;
        return c;
    }

    private static Country c;
    private static Vector ars;
    private static Area area;
    private static Vector srvFtrs;
    private static ServiceFeature sf;
    private static Vector sPhns;
    private static SpecialPhone spPhone;
    private static Formatting fmtg;
    private static Format f;
    private static Vector lLfs;
    private static Formatting stubFormatting;
    private static Vector stubAreas;
    private static Vector stubSFeatures;
    private static Vector stubSPhones;
    
    private static void init() {
        stubAreas = new Vector();
        stubSFeatures = new Vector();
        stubSPhones = new Vector();
        stubFormatting = new Formatting();
        Format stubFormat = new Format();
        stubFormatting.internationalFormat = stubFormat;
        stubFormatting.localFormat = stubFormat;
        stubFormatting.nationalFormat = stubFormat;
        stubFormatting.localLengthFormats = new Vector();
        init0();
        PhoneDataMethods0.init1(countries);
        init2();
        init3();
        init4();
        init5();
        init6();
        init7();
        init8();
        init9();
        init10();
        init11();
        init12();
        init13();
        init14();
        init15();
        init16();
        init17();
        init18();
        init19();
        init20();
        init21();
        init22();
        init23();
        init24();
        init25();
        init26();
        init27();
        init28();
        init29();
        init30();
        init31();
        init32();
        init33();
        init34();
        init35();
        init36();
        init37();
        init38();
        init39();
        init40();
        init41();
        init42();
        init43();

        c = null;
        ars = null;
        area = null;
        srvFtrs = null;
        sf = null;
        sPhns = null;
        spPhone = null;
        fmtg = null;
        f = null;
        lLfs = null;
        stubAreas = null;
        stubSFeatures = null;
        stubSPhones = null;
        stubFormatting = null;
    }
}
