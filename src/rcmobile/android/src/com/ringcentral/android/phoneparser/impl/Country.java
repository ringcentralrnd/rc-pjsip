/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser.impl;

import java.util.Vector;

class Country extends NodeItemBase {

    long id = 0;
    int areaCodeLength = -1;
    int localLength = -1;
    int flagIndex = 0;
    String name;
    String code;
    String internationalPrefix;
    String nationalPrefix;
    String flagURL;
    Vector areas;
    Vector specialPhones;
    Vector serviceFeatures;
    Formatting formatting;

    public Country() {
    }

    public long getId() {
        return id;
    }

    public int getAreaCodeLength() {
        return areaCodeLength;
    }

    public int getLocalLength() {
        return localLength;
    }

    public int getFlagIndex() {
        return flagIndex;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public String getInternationalPrefix() {
        return internationalPrefix;
    }

    public String getNationalPrefix() {
        return nationalPrefix;
    }

    public String getFlagURL() {
        return flagURL;
    }

    public Vector getAreas() {
        return areas;
    }

    public Vector getSpecialPhones() {
        return specialPhones;
    }

    public Vector getServiceFeatures() {
        return serviceFeatures;
    }

    public Formatting getFormatting() {
        return formatting;
    }

    public boolean isRemoveNPrefixAfterCC() {
        return code.equals("44");
    }

    public boolean isFixedDialing() {
        return ((areaCodeLength > 0) && (localLength > 0));
    }

    public boolean isSpecialNumber(CharArrayExt num) {
        for (int i=0; i<specialPhones.size(); i++)
        {
            SpecialPhone specialPhone = (SpecialPhone)specialPhones.elementAt(i);
            if (num.equals(specialPhone.getCode()))
                return true;
        }
        return false;
    }

    public boolean isSpecialNumber(String num) {
        for (int i=0; i<specialPhones.size(); i++)
        {
            SpecialPhone specialPhone = (SpecialPhone)specialPhones.elementAt(i);
            if (specialPhone.getCode().equals(num))
                return true;
        }
        return false;
    }

    public boolean isCountry() {
        return true;
    }
}
