/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser.impl;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.phoneparser.PhoneNumber;
import com.ringcentral.android.phoneparser.PhoneNumberParser;

import java.lang.ref.WeakReference;
import java.util.Vector;

/**
 * Implementation of RC Phone parser.
 */
public class PhoneNumberParserImpl implements PhoneNumberParser {
    /**
     * Keeps global data.
     */
    private static volatile RootNode sRootNode;

    /**
     * Keeps synchronization primitive for global data access.
     */
    private static volatile Object sLock = new Object();

    /**
     * Defines if global data is loading.
     */
    private static volatile boolean sLoadingData;
    private static final String TAG = "[RC] PhoneNumberParserImpl";

    /**
     * Determines if parser data is loaded.
     * 
     * @return <code>true</code> if loaded, <code>false</code> otherwise
     */
    public static boolean isRootDataLoaded() {
        synchronized (sLock) {
            if (sRootNode == null) {
                return false;
            }
            return true;
        }
    }

    /**
     * Determines if parser data is loading.
     * 
     * @return <code>true</code> if the loading is in progress,
     *         <code>false</code> otherwise
     */
    public static boolean isRootDataLoading() {
        synchronized (sLock) {
            return sLoadingData;
        }
    }

    /**
     * Loads phone data. Shall be executed asynchronously.
     */
    public static void loadData() {
        synchronized (sLock) {
            if (sRootNode != null) {
                return;
            } else {
                if (sLoadingData) {
                    try {
                        sLock.wait();
                    } catch (InterruptedException e) {
                        if (LogSettings.ENGINEERING) {
                            EngLog.w(TAG, "loadData(): " + e.getMessage());
                        }
                    }
                    return;
                }
                sLoadingData = true;
            }
        }

        RootNode rootNode = new RootNode();
        rootNode.addData(PhoneData.getCountries());

        synchronized (sLock) {
            sRootNode = rootNode;
            sLoadingData = false;
            sLock.notifyAll();
        }
    }

    private RootNode rootNode;

    private String stationCountryCode = "";
    private String stationAreaCode = "";
    private String stationSettingsKey = "";
    private boolean stationDataLoaded = false;
    private String stationInternationalDialPrefix = "011";

    private String countryCode;
    private String areaCode;
    private String number;
    private String subAddress;
    private String original;
    private String sipPostfix;
    private String feature;
    private String curSettingsKey = "";
    private boolean nationalAsPriority;
    private boolean checkForCCWhenNaP;
    private boolean ignoreFixedDialWhenInter;
    private boolean dnProcSubAddress;
    private boolean allowStars;
    private boolean noShortNumbers;
    private boolean localDialing;

    private static String[] ext = {
    // PT! In order of check!
            "EXT", "X", null };

    private static char[] symbols2digits = { '2', '2', '2', // ABC
            '3', '3', '3', // DEF
            '4', '4', '4', // GHI
            '5', '5', '5', // JKL
            '6', '6', '6', // MNO
            '7', '7', '7', '7', // PQRS
            '8', '8', '8', // TUV
            '9', '9', '9', '9' // WXYZ
    };

    private static String separators = "+-(). ";

    private static String separatorSIP = "@";

    private static boolean isSeparator(char chr) {
        return (separators.indexOf(chr) != -1);
    }

    private static boolean isSeparatorOrDigit(char chr) {
        return ((Utils.isDigit(chr)) || isSeparator(chr));
    }

    /**
     * Default parser.
     */
    public PhoneNumberParserImpl() {
        synchronized (sLock) {
            if (sRootNode == null) {
                throw new IllegalArgumentException(
                        "Phone parser data is not loaded.");
            }
            this.rootNode = sRootNode;
        }
        localDialing = false;
        nationalAsPriority = false;
        checkForCCWhenNaP = false;
        ignoreFixedDialWhenInter = false;
        dnProcSubAddress = false;
        noShortNumbers = false;
        allowStars = false;
        empty();
    }

    /**
     * Parser constructor with phone number.
     */
    public PhoneNumberParserImpl(String number) throws Exception {
        synchronized (sLock) {
            if (sRootNode == null) {
                throw new IllegalArgumentException(
                        "Phone parser data is not loaded.");
            }
            this.rootNode = sRootNode;
        }
        localDialing = false;
        nationalAsPriority = false;
        checkForCCWhenNaP = false;
        ignoreFixedDialWhenInter = false;
        dnProcSubAddress = false;
        noShortNumbers = false;
        allowStars = false;
        doParse(number);
    }

    /**
     * Create parser.
     * 
     * @param rootNode
     * @param number
     * @param settingsKey
     * @param nationalAsPriority
     * @param checkForCCWhenNaP
     * @param ignoreFixedDialWhenInter
     * @throws Exception
     */
    private PhoneNumberParserImpl(RootNode rootNode, String number,
            String settingsKey, boolean nationalAsPriority,
            boolean checkForCCWhenNaP, boolean ignoreFixedDialWhenInter)
            throws Exception {
        this.rootNode = rootNode;
        localDialing = false;
        this.nationalAsPriority = nationalAsPriority;
        this.checkForCCWhenNaP = checkForCCWhenNaP;
        this.ignoreFixedDialWhenInter = ignoreFixedDialWhenInter;
        dnProcSubAddress = false;
        noShortNumbers = false;
        allowStars = false;

        if (number == null)
            number = "";

        setCurSettingsKey(settingsKey);
        doParse(number);
    }

    // private PhoneNumberParserImpl(String country,
    // String area,
    // String number,
    // String subAddress,
    // String sipPostfix,
    // String settingsKey,
    // boolean nationalAsPriority,
    // boolean checkForCCWhenNaP,
    // boolean ignoreFixedDialWhenInter) {
    // countryCode = getDigitsOnlyTranslateLiterals(country != null ? country :
    // "", 0, false);
    // areaCode = getDigitsOnlyTranslateLiterals(area != null ? area : "", 0,
    // false);
    // this.number = getDigitsOnlyTranslateLiterals(number != null ? number :
    // "", 0, true);
    // this.subAddress = getDigitsOnlyTranslateLiterals(subAddress != null ?
    // subAddress : "", 0, false);
    // this.sipPostfix = sipPostfix != null ? sipPostfix : "";
    // localDialing = false;
    // this.nationalAsPriority = nationalAsPriority;
    // this.checkForCCWhenNaP = checkForCCWhenNaP;
    // this.ignoreFixedDialWhenInter = ignoreFixedDialWhenInter;
    // dnProcSubAddress = false;
    // noShortNumbers = false;
    // allowStars = false;
    // setCurSettingsKey(settingsKey);
    // original = getCanonical();
    // }

    private boolean isEmpty() {
        return (Utils.isEmpty(countryCode) && Utils.isEmpty(areaCode) && Utils
                .isEmpty(number));
    }

    /**
     * Clear values.
     */
    private void empty() {
        countryCode = "";
        areaCode = "";
        number = "";
        subAddress = "";
        sipPostfix = "";
        original = "";
        feature = "";
    }

//    private int getCountryCodeAsNum() { // todo unused method
//        return Integer.parseInt(getCountryCode());
//    }

    private void setCountryCode(String code) {
        countryCode = code;
    }

    private void setAreaCode(String area) {
        areaCode = area;
    }

    private String getNumberFormated(Country country) {
        if (country == null)
            country = getCurCountrySettings();

        if (country == null) {
            return "";
        }

        String format = null;

        int len = number.length();

        int count = country.getFormatting().localLengthFormats.size();

        for (int i = 0; i < count; i++) {
            LengthFormat xFormat = (LengthFormat)country.getFormatting()
                    .localLengthFormats.elementAt(i);

            if (xFormat.length == len) {
                format = xFormat.format;
                break;
            }
        }

        if (Utils.isEmpty(format) && (country.getLocalLength() == len)) {
            format = country.getFormatting().localFormat.format;
        }

        if (Utils.isEmpty(format)) {
            format = "%N";
        }

        return formatLocal(format);
    }

    private void setSubAddress(String SubAddress) {
        subAddress = SubAddress;
    }

//    private String getSipPostfix() { // todo unused method
//        return sipPostfix;
//    }

    private String getCanonical(boolean proceedInsideCountry) {
        if (isSpecialNumber())
            return number;

        String format;
        Country curCountry = null;

        if (Utils.isEmpty(number) && Utils.isEmpty(areaCode)) {
            if (proceedInsideCountry || Utils.isEmpty(countryCode)) {
                return "";
            }
            return "+" + countryCode;
        }

        if (Utils.isEmpty(countryCode))
            format = "%N";
        else {
            curCountry = getCurCountrySettings();

            if (curCountry != null) {
                if (proceedInsideCountry
                        && (curCountry.getCode().equals(countryCode))) {
                    format = curCountry.getFormatting().nationalFormat
                            .format;
                } else {
                    if (Utils.isEmpty(countryCode))
                        format = "%A%N";
                    else {
                        if (Utils.isEmpty(areaCode))
                            format = "+%C %N";
                        else
                            format = curCountry.getFormatting()
                                    .internationalFormat.format;
                    }
                }
            } else {
                throw new RuntimeException("getCanonical exception");
            }
        }

        if (Utils.isEmpty(format))
            format = "+%C %A%N";

        return format(curCountry, format, true);
    }

    private String getAsNumRaw() {
        if (isSpecialNumber())
            return number;

        return countryCode + areaCode + number;
    }

//    private String getFullNumRow() {
//        String ret = getAsNumRow();
//
//        if (!Utils.isEmpty(subAddress))
//            ret += "*" + subAddress;
//
//        return ret;
//    }

//    private String getFullDialable() { // todo unused
//        String szNumRow = getFullNumRow();
//
//        if (Utils.isEmpty(szNumRow) || Utils.isEmpty(countryCode)
//                || isInsideCountry())
//            return szNumRow;
//
//        return getStationInternationalPrefix() + szNumRow;
//    }

//    private String getTAMFormat() {
//        if (isSpecialNumber())
//            return number;
//
//        if (!isInsideCountry())
//            return getStationInternationalPrefix() + getAsNumRow();
//
//        return areaCode + number;
//    }

//    private String getFullTAMFormat() { // todo unused
//        String ret = getTAMFormat();
//
//        if (isSpecialNumber())
//            return ret;
//
//        if (!Utils.isEmpty(subAddress))
//            ret += "*" + subAddress;
//
//        if (!Utils.isEmpty(sipPostfix)) {
//            ret += separatorSIP;
//            ret += sipPostfix;
//        }
//
//        return ret;
//    }

//    private String getVanityNumber(boolean local) {
//        char[] buff;
//        if (local) {
//            buff = getLocalCanonical().toCharArray();
//        } else {
//            buff = getCanonical().toCharArray();
//        }
//
//        int len = buff.length;
//
//        if (len >= 1) {
//            char[] szOriginal = original.toCharArray();
//
//            for (int i = (len - 1); i >= 0; i--) {
//                char cur = buff[i];
//
//                if ((cur >= '0') && (cur <= '9')) {
//                    char orCur = getLastVanityAndClear(szOriginal);
//
//                    if (orCur == 0) { // No more...
//                        break;
//                    }
//
//                    if ((orCur >= 'A') && (orCur <= 'Z')) {
//                        if (symbols2digits[(orCur - 'A')] != cur) { // Incorrect
//                                                                    // order.
//                            break;
//                        }
//
//                        buff[i] = orCur;
//                    }
//                }
//            }
//        }
//        return new String(buff);
//    }

//    private char getLastVanityAndClear(char[] number) {
//        int len = number.length;
//        if (len >= 1) {
//            for (int i = (len - 1); i >= 0; i--) {
//                char cur = number[i];
//                if ((cur >= 'a') && (cur <= 'z')) {
//                    cur = String.valueOf(cur).toUpperCase().charAt(0);
//                }
//                if (((cur >= 'A') && (cur <= 'Z'))
//                        || ((cur >= '0') && (cur <= '9'))) {
//                    /*
//                     * char[] newArray = new char[i]; System.arraycopy(szNumber,
//                     * 0, newArray, );
//                     */
//                    number[i] = 0; // todo cut char array instead of this line
//                    return cur;
//                }
//            }
//        }
//        return 0;
//    }

    private String getDigitsOnlyTranslateLiteralsInline(String number,
            boolean allowStar) {
        StringBuffer dest = new StringBuffer();
        for (int i = 0; i < number.length(); i++) {
            char cur = number.charAt(i);
            if ((cur >= 'A') && (cur <= 'Z')) {
                cur = symbols2digits[cur - 'A'];
            } else {
                if ((cur >= 'a') && (cur <= 'z')) {
                    cur = symbols2digits[cur - 'a'];
                } else {
                    if (cur == '*') {
                        if (!allowStar) {
                            cur = 0;
                        }
                    } else if ((cur < '0') || (cur > '9')) {
                        cur = 0;
                    }
                }
            }
            if (cur != 0) {
                dest.append(cur);
            }
        }
        return dest.toString();
    }

    private String getDigitsOnlyTranslateLiterals(String numberStr,
            int startIndex, boolean alowStar) {
        StringBuffer result = new StringBuffer();

        for (int i = startIndex; i < numberStr.length(); i++) {
            char cur = numberStr.charAt(i);

            if ((cur == ' ') || (cur == '\t')) {
                continue;
            }

            if ((cur >= 'A') && (cur <= 'Z')) {
                cur = symbols2digits[cur - 'A'];
            } else {
                if ((cur >= 'a') && (cur <= 'z')) {
                    cur = symbols2digits[cur - 'a'];
                }
            }

            if ((cur > 0)
                    && (Utils.isDigit(cur) || (alowStar && (cur == '*')))) {
                result.append(cur);
            }
        }
        return result.toString();
    }

    /*
     * todo not used boolean IsDigitsOnlyPhoneNumber( String szNumber ) { if(
     * _tcslen( szNumber ) == 0 ) return false;
     * 
     * char szFirst =szNumber; if( szFirst == '@' ) return false; //empty sip
     * number
     * 
     * while(szNumber != 0 ) { char szCur =szNumber ++; if( isSeparator(szCur)
     * || szCur == '+' ) continue;
     * 
     * if( szCur == '@' ) return true; //digits only sip number
     * 
     * if( !isDigit( szCur ) ) return false; }
     * 
     * return true; }
     * 
     * String TranslateAccountNumber( String szNumber ) { String szRet =
     * getDigitsOnlyTranslateLiterals( szNumber , false);
     * 
     * if( ( szRet.length() == 11 ) && ( szRet[ 0 ] == '1' ) ) szRet =
     * szRet.Mid( 1 );
     * 
     * return szRet; }
     * 
     * String CorrectNickName( String szNickName ) { ASSERT( szNickName != null
     * );
     * 
     * char szRet[ 1024 ]; ZeroMemory( szRet, sizeof( szRet ) );
     * 
     * int iR = 0; int iS = 0;
     * 
     * while( szNickName[ iS ] != 0 ) { if( _istalnum ( szNickName[ iS ] ) || (
     * szNickName[ iS ] == '_' ) ) szRet[ iR++ ] = szNickName[ iS ];
     * 
     * iS++; ASSERT( iR < 1022 ); }
     * 
     * return szRet; }
     */

    private boolean isValid() {
        return !Utils.isEmpty(number);
    }

    private boolean isFullValid() {
        return !(Utils.isEmpty(getCountryCode()) || Utils.isEmpty(number));
    }

    private boolean isShortNumber() {
        if (Utils.isEmpty(countryCode) && Utils.isEmpty(areaCode)
                && !Utils.isEmpty(number) && (number.length() <= 5))
            return true;

        return false;
    }

//    private boolean isCountryValid() {
//        return !Utils.isEmpty(countryCode)
//                && rootNode.isLoaded()
//                && rootNode.getCountryArea(countryCode,
//                        new CountryAreaContainer());
//    }

//    private boolean haveToBeValidated() {
//        return (!isCountryValid() || (areaCode.length() > 5));
//    }

//    private void doValidation() { // todo unused
//        if (haveToBeValidated()) {
//            number = countryCode + areaCode + number;
//            countryCode = "";
//            areaCode = "";
//        }
//    }

    private boolean isUSANumberAndValid() {
        if (checkUSAValid() && isFullValid() && getCountryCode().equals("1"))
            return true;
        return false;
    }

    private boolean checkUSAValid() {
        if (isEmpty())
            return false;

        if (!getCountryCode().equals("1"))
            return true;

        if (isSpecialNumber())
            return true;

        if ((getAreaCode().length() != 3) || (getNumber().length() != 7))
            return false;

        char cN = getAreaCode().charAt(0);

        if (cN < '2')
            return false;

        cN = getNumber().charAt(0);

        return cN >= '2';
    }

    private boolean isSpecialNumber(CharArrayExt num, Country country) {
        if (country == null)
            country = getCurCountrySettings();

        return country != null && country.isSpecialNumber(num);
    }

    private boolean isSpecialNumber(String num, Country country) {
        if (country == null)
            country = getCurCountrySettings();

        return country != null && country.isSpecialNumber(num);
    }

    /**
     * Defines if it is a special number.
     */
    private boolean isSpecialNumber() {
        return !Utils.isEmpty(number)
                && (is911() || is112() || is999() || (!allowStars && isStarSequence()) || isSpecialNumber(number, null));
    }

    /**
     * Defines if it is USA 911.
     */
    private boolean is911() {
        return getCountryCode().equals("1") && getNumber().equals("911");

    }

    /**
     * Defines if it is UK 112.
     */
    private boolean is112() {
        return getCountryCode().equals("44") && getNumber().equals("112");

    }

    /**
     * Defines if it is UK 999.
     */
    private boolean is999() {
        return getCountryCode().equals("44") && getNumber().equals("999");

    }

    private boolean isStarSequence() {
        return number.length() > 0 && number.charAt(0) == '*';

    }

    /**
     * Defines if this is USA toll free number.
     */
    private boolean isTollFree() {
        if (!"1".equals(getCountryCode())) {
            return false;
        }
        
        if (("800".equals(getAreaCode())
                        || "866".equals(getAreaCode())
                        || "855".equals(getAreaCode())
                        || "877".equals(getAreaCode()) || "888"
                        .equals(getAreaCode()))) {
            return true;
        }
        
        return false;
    }

//    private boolean isLocalDialing() { // todo unused
//        return localDialing;
//    }

//    private boolean isSipPhone() {
//        return (!Utils.isEmpty(sipPostfix));
//    }

    /**
     * Original method name is "CheckSecondPlus". Returns index of the second
     * "+" symbol or -1
     * 
     * @param num
     * @return
     */
    private int getSecondPlusIndx(CharArrayExt num) {
        // Check leading space:
        char nextChar;
        int i = 0;
        for (; i < num.length(); i++) {
            nextChar = num.charAt(i);
            if ((nextChar == ' ') || (nextChar == '\t')) {
                continue;
            }
            break;
        }

        // Ignore leading pluses.
        for (; i < num.length(); i++) {
            nextChar = num.charAt(i);
            if (nextChar == '+') {
                continue;
            }
            break;
        }

        // Checking for digit or char:
        for (; i < num.length(); i++) {
            nextChar = num.charAt(i);
            if ((((nextChar >= 'A') && (nextChar <= 'Z'))
                    || ((nextChar >= 'a') && (nextChar <= 'z')) || Utils
                    .isDigit(nextChar))
                    && (i + 1) < num.length()) {
                return num.indexOf('+', i + 1);
            }
        }
        return -1;
    }

    private void doParse(String numberStr) throws Exception {
        if (numberStr == null) {
            throw new Exception("Phone number is empty");
        }

        // class fields cleanup
        empty();
        original = numberStr;

        CharArrayExt numArray = new CharArrayExt(numberStr);

        int secondPlusIndex = -1;
        if (!dnProcSubAddress) {
            secondPlusIndex = getSecondPlusIndx(numArray);
        }

        if (secondPlusIndex > -1) {
            subAddress = getDigitsOnlyTranslateLiterals(numArray.toString(),
                    secondPlusIndex + 1, false);
            numArray.cut(0, secondPlusIndex);
        }

        if (numArray.isEmpty()) {
            throw new Exception("Phone number is empty");
        }

        numArray.toUpperCase();

        // Getting SIP postfix
        int sipPostfixIndex = numArray.indexOf(separatorSIP);
        if (sipPostfixIndex > -1) {
            sipPostfix = numArray.substring(sipPostfixIndex
                    + separatorSIP.length());
            numArray.cut(0, sipPostfixIndex);
        }

        Country currentCountry = getCurCountrySettings(); // todo

        if (currentCountry != null) {
            Vector serviceFeatures = (Vector) currentCountry.getServiceFeatures();
            for (int i=0; i<serviceFeatures.size(); i++){
                ServiceFeature serviceFeature = (ServiceFeature)serviceFeatures.elementAt(i);
                int iLen = serviceFeature.sequence.length();
                if (numArray.startsWith(serviceFeature.sequence)) {
                    number = serviceFeature.sequence
                            + new PhoneNumberParserImpl(rootNode, numArray
                                    .substring(iLen), curSettingsKey,
                                    nationalAsPriority, checkForCCWhenNaP,
                                    false).getAsNumRaw();
                    feature = serviceFeature.sequence;
                    return;
                }
            }
        }

        if (numArray.isEmpty()) {
            throw new Exception("Phone number is empty");
        }

        if (numArray.charAt(0) == '*') {
            // star sequence
            number = numArray.toString();
            return;
        }

        if (!dnProcSubAddress && secondPlusIndex == -1) {
            int lpStar = numArray.indexOf("*");

            // Getting extension:
            if (lpStar != -1) {
                subAddress = getDigitsOnlyTranslateLiterals(numArray
                        .toString(), lpStar + 1, false);
                numArray.cut(0, lpStar); // instead of <*lpStar = 0;>
            } else {
                for (int i = 0; ext[i] != null; i++) {
                    int extIndex = numArray.indexOf(ext[i]);

                    if (extIndex > -1) {
                        if (extIndex == 0) { // At begin number...
                            continue;
                        }

                        if (!isSeparatorOrDigit(numArray.charAt(extIndex - 1))) {
                            continue; // letter before...
                        }

                        int extLen = PhoneNumberParserImpl.ext[i].length();

                        if (!isSeparatorOrDigit(numArray.charAt(extIndex
                                + extLen))) {
                            continue; // letter after...
                        }

                        if (isSeparator(numArray.charAt(extIndex + extLen)))
                            extLen++;

                        subAddress = getDigitsOnlyTranslateLiterals(numArray
                                .toString(), extIndex + extLen, false);
                        numArray.cut(0, extIndex); // instead of <*extIndex =
                                                   // 0;>
                        break;
                    }
                }
            }
        }

        boolean international = false;

        numArray.trim();

        if (numArray.charAt(0) == '+') {
            numArray.cut(1);
            international = true;
        }

        numArray = new CharArrayExt(getDigitsOnlyTranslateLiteralsInline(
                numArray.toString(), allowStars));

        // Removing international prefix...
        String szCurInterPrefix = (currentCountry == null) ? null
                : currentCountry.getInternationalPrefix();

        int intPrefixLen = (szCurInterPrefix != null) ? szCurInterPrefix
                .length() : 0;

        if ((intPrefixLen != 0) && numArray.startsWith(szCurInterPrefix)) {
            numArray.cut(intPrefixLen);
            international = true;
        }

        Country country;
        Area area;

        if (!international) {
            if (isSpecialNumber(numArray, null)) {
                countryCode = currentCountry.getCode();
                number = numArray.toString();
                return;
            }

            if (!noShortNumbers && (numArray.length() <= 5)) {
                number = numArray.toString();
                return;
            }

            // Not international number....
            if (currentCountry != null) {
                boolean national = false;
                CharArrayExt tempNumberArray = new CharArrayExt(numArray
                        .toCharArray());
                // todo avoid of tempNumberArray, to use only num numArray
                if (!Utils.isEmpty(currentCountry.getNationalPrefix())
                        && tempNumberArray.startsWith(currentCountry
                                .getNationalPrefix())) {
                    tempNumberArray.cut(currentCountry.getNationalPrefix()
                            .length());
                    national = true;
                }

                intPrefixLen = tempNumberArray.length();
                if (national) {
                    // National.
                    if (currentCountry.isFixedDialing()) {
                        if (intPrefixLen <= currentCountry
                                .getAreaCodeLength()) {
                            countryCode = currentCountry.getCode();
                            areaCode = tempNumberArray.toString();
                            return;
                        }

                        countryCode = currentCountry.getCode();
                        areaCode = tempNumberArray.substring(0,
                                currentCountry.getAreaCodeLength());
                        number = tempNumberArray.substring(currentCountry
                                .getAreaCodeLength());
                        return;
                    } else {
                        // National. Not fixed dialing...
                        CountryAreaContainer container = new CountryAreaContainer();
                        boolean result = rootNode.getCountryArea(
                                currentCountry.getCode()
                                        + tempNumberArray.toString(),
                                container);
                        country = container.country; // todo
                        area = container.area; // todo
                        if (result) {
                            countryCode = currentCountry.getCode();
                            tempNumberArray.cut(container.areaOffset); // todo
                            if (area != null) {
                                areaCode = area.code;
                            } else {
                                if (country.getAreaCodeLength() > 0) {
                                    areaCode = tempNumberArray.substring(0,
                                            country.getAreaCodeLength());
                                }
                            }
                            tempNumberArray.cut(areaCode.length());
                            number = tempNumberArray.toString();
                            return;
                        }
                    }
                } else {
                    // Not national.
                    if (currentCountry.isFixedDialing()) {
                        if (nationalAsPriority && intPrefixLen <= (currentCountry.getLocalLength() + currentCountry.getAreaCodeLength()) ) {
                            // Not national. Fixed Dialing. National As Priority
                            if (intPrefixLen <= currentCountry
                                    .getLocalLength()) {
                                countryCode = currentCountry.getCode();
                                areaCode = getStationAreaCode();
                                number = tempNumberArray.toString();
                                localDialing = true;
                                return;
                            }

                            countryCode = currentCountry.getCode();
                            areaCode = tempNumberArray.substring(0,
                                    currentCountry.getAreaCodeLength());
                            number = tempNumberArray.substring(currentCountry
                                    .getAreaCodeLength());
                            return;
                        } else {
                            // Not national. Fixed Dialing. International As
                            // Priority
                            if (!ignoreFixedDialWhenInter) {
                                if (intPrefixLen == currentCountry
                                        .getLocalLength()) {
                                    countryCode = currentCountry.getCode();
                                    areaCode = getStationAreaCode();
                                    number = tempNumberArray.toString();
                                    localDialing = true;
                                    return;
                                }
                                if (intPrefixLen == (currentCountry
                                        .getLocalLength() + currentCountry
                                        .getAreaCodeLength())) {
                                    countryCode = currentCountry.getCode();
                                    areaCode = tempNumberArray.substring(0,
                                            currentCountry
                                                    .getAreaCodeLength());
                                    number = tempNumberArray
                                            .substring(currentCountry
                                                    .getAreaCodeLength());
                                    return;
                                }
                            }
                        }
                    } else {
                        // Not national. Floating Dialing.
                        if (nationalAsPriority) {
                            int countryCodeLenght = 0;
                            String numberToCheck;
                            if (checkForCCWhenNaP) {
                                if (tempNumberArray.startsWith(currentCountry
                                        .getCode())) {
                                    numberToCheck = tempNumberArray
                                            .toString();
                                    countryCodeLenght = currentCountry
                                            .getCode().length();
                                } else {
                                    numberToCheck = currentCountry.getCode()
                                            + tempNumberArray.toString();
                                }
                            } else {
                                numberToCheck = currentCountry.getCode()
                                        + tempNumberArray.toString();
                            }
                            // Not national. Floating Dialing. National As
                            // Priority
                            CountryAreaContainer container = new CountryAreaContainer();
                            boolean result = rootNode.getCountryArea(
                                    numberToCheck, container);
                            country = container.country; // todo
                            area = container.area; // todo
                            if (result) {
                                tempNumberArray.cut(countryCodeLenght);
                                countryCode = currentCountry.getCode();
                                tempNumberArray.cut(container.areaOffset); // todo
                                if (area != null) {
                                    areaCode = area.code;
                                } else {
                                    if (country.getAreaCodeLength() > 0) {
                                        areaCode = tempNumberArray.substring(
                                                0, country
                                                        .getAreaCodeLength());
                                    }
                                }
                                tempNumberArray.cut(areaCode.length());
                                number = tempNumberArray.toString();
                                return;
                            }
                        }
                    }
                }
            }
        }

        CountryAreaContainer container = new CountryAreaContainer();
        boolean result = rootNode.getCountryArea(numArray.toString(),
                container);
        country = container.country; // todo
        area = container.area; // todo
        if (result) {
            countryCode = country.getCode();
            numArray.cut(countryCode.length());
            numArray.cut(container.areaOffset); // todo

            if (area != null) {
                areaCode = area.code;
            } else {
                if ((country.getAreaCodeLength() > 0)
                        && numArray.length() >= country.getAreaCodeLength()) {
                    areaCode = numArray.substring(0, country
                            .getAreaCodeLength());
                }
            }
            if (!Utils.isEmpty(areaCode)) {
                numArray.cut(areaCode.length());
            }

            number = numArray.toString();

            String testNumber = areaCode + number;

            if (country.isSpecialNumber(testNumber)) {
                number = testNumber;
                areaCode = null;
            }
        } else {
            number = numArray.toString();
        }
    }

    /**
     * Sets station location.
     * 
     * @param country
     *            country code
     * @param area
     *            area code
     * @param internationalPrefix
     *            international prefix (e.g. 011)
     * 
     * @return <code>true</code> on success, <code>false</code> otherwise
     */
    public boolean setStationLocation(String country, String area,
            String internationalPrefix) {
        stationInternationalDialPrefix = internationalPrefix;

        if (Utils.isEmpty(country)) {
            return false;
        } else {
            setStationLocation(country, area);
        }
        return (!Utils.isEmpty(stationCountryCode) && !Utils
                .isEmpty(stationAreaCode));
    }

    /**
     * Defines if current station location values are set according to parameters.
     * 
     * @param country
     *            country code
     * @param area
     *            area code
     * @param internationalPrefix
     *            international prefix (e.g. 011)
     * 
     * @return <code>true</code> if the phone parser is set to the parameters, <code>false</code> otherwise
     */
    public boolean isStationLocationSetTo(String country, String area,
            String internationalPrefix) {
        if (equalValues(country, stationCountryCode) && 
                equalValues(area, stationAreaCode) &&
                equalValues(internationalPrefix, stationInternationalDialPrefix)) {
            return true;
        }
        return false;
    }
    
    /**
     * Compares String values.
     * 
     * @param s1 first string
     * @param s2 second string
     * @return <code>true</code> if s1 value equals s2 value, otherwise <code>false</code>
     */
    private static final boolean equalValues(String s1, String s2) {
        if (s1 == null) {
            if (s2 != null) {
                return false;
            } else {
                return true;
            }
        } else {
            if (s2 == null) {
                return false;
            }
            return s1.equals(s2);
        }
    }
    
    /**
     * Returns parser settings configuration as string for logging.
     * 
     * @return parser settings configuration as string for logging
     */
    public String getParserSettingsDescription() {
        StringBuffer sb = new StringBuffer("Country[");
        sb.append(stationCountryCode == null ? "null" : stationCountryCode);
        
        sb.append("] Area[");
        sb.append(stationAreaCode == null ? "null" : stationAreaCode);
        
        sb.append("] IntPref[");
        sb.append(stationInternationalDialPrefix == null ? "null" : stationInternationalDialPrefix);
        
        sb.append("] National Is Priority[");
        sb.append(nationalAsPriority);
        
        sb.append("] Check for CC[");
        sb.append(checkForCCWhenNaP);
        
        sb.append("] DnProcSubAddress[");
        sb.append(dnProcSubAddress);
        
        sb.append("] Ignore Fixed Dial[");
        sb.append(ignoreFixedDialWhenInter);
        
        
        sb.append("] No Short Numbers[");
        sb.append(noShortNumbers);
        sb.append(']');
        
        return sb.toString();
    }
    
    /**
     * Sets station location.
     * 
     * @param countryCode
     *            country code
     * @param areaCode
     *            area code
     * 
     * @return <code>true</code> on success, <code.false</code> otherwise
     */
    private void setStationLocation(String countryCode, String areaCode) {

        if ((countryCode == null) || (areaCode == null)) {
            stationDataLoaded = false;
            stationCountryCode = null;
            stationAreaCode = null;
            stationSettingsKey = null;
        } else {
            stationDataLoaded = true;
            stationCountryCode = !Utils.isEmpty(countryCode) ? countryCode
                    : "";
            stationAreaCode = !Utils.isEmpty(areaCode) ? areaCode : "";
            stationSettingsKey = stationCountryCode + stationAreaCode;
        }
    }

    /**
     * Clears station location.
     */
//    private void clearStationLocation() {
//        setStationLocation(null, null);
//    }

    private boolean checkCurrent() {
        if (!stationDataLoaded) {
            stationDataLoaded = true;

            stationSettingsKey = stationCountryCode + stationAreaCode;
        }

        return (!Utils.isEmpty(stationCountryCode) && !Utils
                .isEmpty(stationAreaCode));

    }

//    private boolean isStationDataLoaded() { // todo unused
//        return stationDataLoaded;
//    }

//    private int getStationCountryCodeAsNum() { // todo unused
//        return Integer.parseInt(getStationCountryCode());
//    }

    private String getStationInternationalPrefix() {
        checkCurrent();
        return stationInternationalDialPrefix;
    }

//    private boolean compareAsNumRow(PhoneNumberParserImpl refer) {
//        return (getAsNumRow().equals(refer.getAsNumRow()));
//    }

//    private boolean compareAsFullNumRow(PhoneNumberParserImpl refer) {
//        return (getFullNumRow().equals(refer.getFullNumRow()));
//    }

    private boolean isInsideCountry() {
        checkCurrent();
        return (countryCode.equals(stationCountryCode) || Utils
                .isEmpty(countryCode));
    }

    private String getCurSettingsKey() {
        if (!Utils.isEmpty(curSettingsKey)) {
            return curSettingsKey;
        }

        if (!Utils.isEmpty(stationSettingsKey)) {
            return stationSettingsKey;
        }

        return "1650";
    }

    private void setCurSettingsKey(String key) {
        if (key != null) {
            curSettingsKey = key;
        } else {
            curSettingsKey = null;
        }
    }

    private Country getCountrySettingsByKey(String key) {

        if (Utils.isEmpty(key)) {
            return null;
        }

        Country country;
        CountryAreaContainer container = new CountryAreaContainer();
        if (rootNode.isLoaded()) {
            rootNode.getCountryArea(key, container);
        }
        country = container.country;

        // ASSERT( lpCountry != null );
        return country;
    }

//    private int getCountryFlagIndexByKey(String key) {
//        Country country = getCountrySettingsByKey(key);
//
//        if (country == null) {
//            return 0;
//        }
//
//        return country.getFlagIndex();
//    }

    private String getCountryNameByKey(String key) {
        Country country = getCountrySettingsByKey(key);

        if (country == null) {
            return "";
        }

        return country.getName();
    }

//    private boolean isStationCanada() {
//        return (getCountryNameByKey("1905")
//                .equals(getCountryNameByKey(stationSettingsKey)));
//    }

//    private int getCountryFlagIndex() { // todo unused
//        if (Utils.isEmpty(countryCode)) {
//            return 0;
//        }
//
//        if (isSpecialNumber()) {
//            return getCountryFlagIndexByKey(countryCode);
//        }
//
//        return getCountryFlagIndexByKey(getAsNumRow());
//    }

    /**
     * Defines if country USA or Canada.
     */
    private boolean isUSAorCanada() {
        String szName = getCountryName();

        if (szName.equals(getUSACountryName())
                || szName.equals(getCanadaCountryName()))
            return true;
        return false;
    }

    /**
     * Returns USA country name.
     */
    private String getUSACountryName() {
        return getCountryNameByKey("1650");
    }

    /**
     * Returns Canada country name.
     */
    private String getCanadaCountryName() {
        return getCountryNameByKey("1204");
    }

    /**
     * Returns current country name.
     */
//    private String getCurCountryName() {
//        return getCountryNameByKey(getCurSettingsKey());
//    }

    /**
     * Returns current country settings.
     */
    private Country getCurCountrySettings() {
        checkCurrent();
        return getCountrySettingsByKey(getCurSettingsKey());
    }

//    private String getCurInternationalPrefix() { // todo unused
//        Country lpCountry = getCurCountrySettings();
//        return (lpCountry != null) ? lpCountry.getInternationalPrefix() : "";
//    }

//    private boolean isCurrentCountry(Country lpCountry) { // todo unused
//        if (Utils.isEmpty(countryCode)) {
//            return false;
//        }
//        if (lpCountry == null) {
//            lpCountry = getCurCountrySettings();
//        }
//        return (lpCountry.getCode().equals(countryCode));
//    }

//    private Country getCountrySettings() { // todo unused
//        // ASSERT( g_xRootNode.isLoaded() );
//
//        String szKey = countryCode + areaCode;
//        CountryAreaContainer container = new CountryAreaContainer();
//
//        if (!Utils.isEmpty(szKey) && rootNode.isLoaded()) {
//            rootNode.getCountryArea(szKey, container);
//        }
//        return container.country;
//    }

    private String format(Country curCountry, String format, boolean addExt) {

        StringBuffer ret = new StringBuffer();
        boolean percent = false;
        for (int i = 0; i < format.length(); i++) {
            if (format.charAt(i) == '%') {
                if (percent) {
                    ret.append("%");
                } else {
                    percent = true;
                }
            } else {
                if (percent) {
                    switch (format.charAt(i)) {
                    case 'C':
                        ret.append(countryCode);
                        break;

                    case 'A':
                        ret.append(areaCode);
                        break;

                    case 'L':
                        if (curCountry != null)
                            ret.append(curCountry.getNationalPrefix());
                        else {
                            // ASSERT( FALSE );
                        }
                        break;

                    case 'I':
                        if (curCountry != null)
                            ret.append(curCountry.getInternationalPrefix());
                        else {
                            // ASSERT( FALSE );
                        }
                        break;

                    case 'N':
                        ret.append(number);
                        break;

                    case 'P':
                        ret.append(getNumberFormated(curCountry));
                        break;

                    default: {
                        ret.append(format.charAt(i));
                    }
                        break;
                    }

                    percent = false;
                } else
                    ret.append(format.charAt(i));
            }
        }

        if (addExt) {
            if (!Utils.isEmpty(subAddress)) {
                ret.append(" * ");
                ret.append(subAddress);
            }
        }

        return ret.toString();
    }

    private String formatLocal(String format) {
        StringBuffer ret = new StringBuffer();
        boolean percent = false;
        int num = -1;

        for (int i = 0; i < format.length(); i++) {
            if (format.charAt(i) == '%') {
                if (percent)
                    ret.append("%");
                else
                    percent = true;
            } else {
                if (percent) {
                    if (Utils.isDigit(format.charAt(i)))
                        num = format.charAt(i) - '0' - 1;
                    else {
                        switch (format.charAt(i)) {
                        case 'n': {
                            int len = number.length();

                            if ((num >= 0) && (num < len))
                                ret.append(number.charAt(num));
                        }
                            break;

                        case 'N':
                            ret.append(number);
                            break;

                        default: {
                            ret.append(format.charAt(i));
                        }
                            break;
                        }

                        percent = false;
                        num = -1;
                    }
                } else
                    ret.append(format.charAt(i));
            }
        }
        return ret.toString();
    }

//    private boolean getNationalAsPriority() { // todo unused
//        return nationalAsPriority;
//    }

//    private boolean getCheckForCCWhenNaP() { // todo unused
//        return checkForCCWhenNaP;
//    }

    public void setCheckForCCWhenNaP(boolean checkForCCWhenNaP) {
        this.checkForCCWhenNaP = checkForCCWhenNaP;
    }

//    private boolean getIgnoreFixedDialWhenInter() { // todo unused
//        return ignoreFixedDialWhenInter;
//    }

    public void setIgnoreFixedDialWhenInter(boolean ignoreFixedDialWhenInter) {
        this.ignoreFixedDialWhenInter = ignoreFixedDialWhenInter;
    }

//    private boolean getDnProcSubAddress() { // todo unused
//        return dnProcSubAddress;
//    }

    public void setDnProcSubAddress(boolean dnProcSubAddress) {
        this.dnProcSubAddress = dnProcSubAddress;
    }

//    private boolean getAllowStars() { // todo unused
//        return allowStars;
//    }

    public void setAllowStars(boolean allowStars) {
        this.allowStars = allowStars;
    }

//    private boolean getNoShortNumbers() { // todo unused
//        return noShortNumbers;
//    }

    public void setNoShortNumbers(boolean noShortNumbers) {
        this.noShortNumbers = noShortNumbers;
    }

//    private int getCountriesCount() { // todo unused
//        return rootNode.getCountriesCount();
//    }

//    private String getCountryNameAt(int iPos) { // todo unused
//        Country lpCountry = rootNode.getCountryAt(iPos);
//        if (lpCountry != null) {
//            return lpCountry.getName();
//        }
//        return "";
//    }

//    private String getCountryCodeAt(int iPos) { // todo unused
//        Country lpCountry = rootNode.getCountryAt(iPos);
//        if (lpCountry != null) {
//            return lpCountry.getCode();
//        }
//        return "";
//    }

//    private int getCountryFlagIndexAt(int iPos) { // todo unused
//        Country lpCountry = rootNode.getCountryAt(iPos);
//        if (lpCountry != null) {
//            return lpCountry.getFlagIndex();
//        }
//        return 0;
//    }

//    private int getCountryIndexByKey(String szKey) { // todo unused
//        Country lpCountry = getCountrySettingsByKey(szKey);
//        if (lpCountry != null) {
//            return rootNode.findCountry(lpCountry);
//        }
//        return -1;
//    }

//    private int getCountryIndexByName(String szName) { // todo unused
//        return rootNode.findCountry(szName);
//    }

//    private int getCountryIndex() { // todo unused
//        if (Utils.isEmpty(countryCode))
//            return -1;
//
//        if (isSpecialNumber())
//            return getCountryIndexByKey(countryCode);
//
//        return getCountryIndexByKey(getAsNumRow());
//    }

    /**
     * Sets country code.
     * 
     * @param countryCode
     *            country code
     */
    public void setSettingsKey(String countryCode) {
        setCurSettingsKey(countryCode);
    }

    /**
     * Set national format as priority.
     * 
     * @param nationalAsPriority
     *            the national format priority value
     */
    public void setNationalAsPriority(boolean nationalAsPriority) {
        this.nationalAsPriority = nationalAsPriority;
    }

//    private String getStationCountryCode() {
//        checkCurrent();
//        return stationCountryCode;
//    }

    private String getStationAreaCode() {
        checkCurrent();
        return stationAreaCode;
    }

    private String getCountryCode() {
        return countryCode;
    }

    private String getAreaCode() {
        return areaCode;
    }

    private String getNumber() {
        return number;
    }

    private String getFormattedNumber() {
        return getNumberFormated(null);
    }

    private String getSubAddress() {
        return subAddress;
    }

    private String getCanonical() {
        return getCanonical(false);
    }

    private String getLocalCanonical() {
        return getCanonical(true);
    }

    private String getOriginal() {
        return original;
    }

//    private String getNumRow() {
//        return getAsNumRow();
//    }

    private String getDialable() {
        checkCurrent();
        String szNumRow = getAsNumRaw();

        if (Utils.isEmpty(szNumRow) || Utils.isEmpty(countryCode)
                || isInsideCountry())
            return szNumRow;

        return getStationInternationalPrefix() + szNumRow;
    }

//    private String getTTS() {
//        String szNumRow = getAsNumRow();
//        StringBuffer szRes = new StringBuffer();
//        int iLen = szNumRow.length();
//        for (int i = 0; i < iLen; i++) {
//            szRes.append(szNumRow.charAt(i));
//            if (i < (iLen - 1))
//                szRes.append(" ");
//        }
//        return szRes.toString();
//    }

//    private String getSMS() {
//        return getCanonical(true);
//    }

    private String getCountryName() {
        if (Utils.isEmpty(countryCode)) {
            return "";
        }

        if (isSpecialNumber()) {
            return getCountryNameByKey(countryCode);
        }

        return getCountryNameByKey(getAsNumRaw());
    }

    private String getE164() {
        if (isStarSequence())
            return number;

        if (!isSpecialNumber()) {
            if (Utils.isEmpty(countryCode)) {
                String ret = areaCode + number;
                return ret.length() > 16 ? ret.substring(0, 16) : ret;
            }
        }

        String ret = "+" + countryCode + areaCode + number;
        return ret.length() > 16 ? ret.substring(0, 16) : ret;
    }

    private String getFullNumber() {
        if (Utils.isEmpty(subAddress)) {
            return getE164();
        }
        return getE164() + "x" + subAddress;
    }

//    private String getFeature() {
//        return feature;
//    }

//    private String getFeaturePhoneNumber() {
//        int iFLen = feature.length();
//        // int iNLen = number.length();
//        // ASSERT( iNLen>= iFLen ); todo ???
//        // ASSERT( m_szNumber.Left( iFLen ) == m_szFeature ); todo ???
//        return number.substring(iFLen);
//    }

//    private String getVanity(boolean local) {
//        return getVanityNumber(local);
//    }

    /**
     * Keeps phone number container.
     */
    private WeakReference wRef = null;

    /**
     * Get container.
     * 
     * @see #returnPhoneNumberContainer(PhoneNumber)
     */
    public PhoneNumber getPhoneNumberContainer() {
        PhoneNumber p;
        if (wRef != null) {
            p = (PhoneNumber)wRef.get();
            if (p == null) {
                p = new PhoneNumber();
            }
            wRef = null;
        } else {
            p = new PhoneNumber();
        }
        return p;
    }

    /**
     * Retrieve container.
     * 
     * @see #getPhoneNumberContainer()
     */
    public void returnPhoneNumberContainer(PhoneNumber container) {
        if (wRef != null) {
            if (wRef.get() == null) {
                PhoneNumber phoneNumber = new PhoneNumber(container);
                wRef = new WeakReference(phoneNumber);
            }
        } else {
            PhoneNumber phoneNumber = new PhoneNumber(container);
            wRef = new WeakReference(phoneNumber);
        }
    }

    /**
     * Keeps sync-primitive.
     */
    private final Object m_lock = new Object();
    
    /**
     * Parse <code>phoneNumber</code> and put result into <code>container</code>
     */
    public synchronized PhoneNumber parse(String phoneNumber, PhoneNumber container){
        String errorMessage = null;
        synchronized (m_lock) {
            try {
                doParse(phoneNumber);
                if (!isValid()) {
                    throw new Exception("Phone number is invalid");
                }
                container.isValid = isValid();
                container.isFullValid = isFullValid();
                container.isUSAValid = checkUSAValid();
                container.isUSANumberAndValid = isUSANumberAndValid();
                container.isEmpty = isEmpty();
                container.isSpecialNumber = isSpecialNumber();
                container.isShortNumber = isShortNumber();
                container.is911 = is911();
                container.is112 = is112();
                container.is999 = is999();
                container.isTollFree = isTollFree();
                container.isUSAOrCanada = isUSAorCanada();
                container.countryName = getCountryName();
                container.original = getOriginal();
                container.canonical = getCanonical();
                container.localCanonical = getLocalCanonical();
                container.countryCode = getCountryCode();
                container.areaCode = getAreaCode();
                container.number = getNumber();
                container.subAddress = getSubAddress();
                container.dialable = getDialable();
                container.countryName = getCountryName();
                container.e164 = getE164();
                container.formattedNumber = getFormattedNumber();
                container.numRaw = getAsNumRaw();
                container.fullNumber = getFullNumber();
                return container;
            } catch (Exception e) {
                if (LogSettings.ENGINEERING) {
                    errorMessage = e.getMessage();
                }
            }
        }
        if (LogSettings.ENGINEERING) {
            EngLog.w(TAG, "parse(): " + errorMessage);
        }
        return null;
    }

    /**
     * Parse <code>phoneNumber</code>.
     */
    public synchronized PhoneNumber parse(String phoneNumber){
        PhoneNumber c = null;
        try {
            c = getPhoneNumberContainer();
            parse(phoneNumber, c);
            if (!isValid()) {
                throw new Exception("Phone number is invalid");
            }
            return c;
        } catch (Exception e) {
            if (LogSettings.ENGINEERING) {
                EngLog.w(TAG, "parse(): " + e.getMessage());
            }
        }
        return null;
    }

    public void set(String countryCode, String areaCode, String number,
            String subAddress) {
        setCountryCode(countryCode);
        setAreaCode(areaCode);
        this.number = number;
        setSubAddress(subAddress);
    }

    /**
     * To string.
     */
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("PhoneNumberParserImpl[");
        sb.append("stationSettingsKey='").append(stationSettingsKey).append(
                '\'');
        sb.append("settingsKey='").append(curSettingsKey).append('\'');
        sb.append(", nationalAsPriority=").append(nationalAsPriority);
        sb.append(", ignoreFixedDialWhenInter=").append(
                ignoreFixedDialWhenInter);
        sb.append(", noShortNumbers=").append(noShortNumbers);
        sb.append(", checkForCCWhenNaP=").append(checkForCCWhenNaP);
        sb.append(", dnProcSubAddress=").append(dnProcSubAddress);
        sb.append(", allowStars=").append(allowStars);
        sb.append(']');
        return sb.toString();
    }
}
