/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser.impl;

class Area extends NodeItemBase {
    String code;
    String name;

    boolean isArea() {
        return true;
    }
}
