/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser.impl;

import java.util.Vector;

/**
 * Auto-generated. 
 * Phone data version: 1
 * Phone data revision: 12
 */
final class PhoneDataMethods3 {
    final static void init0(Vector ars) {
        area = new Area();
        area.code = PhoneDataConstantPool1.cnst596;
        area.name = PhoneDataConstantPool1.cnst597;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool1.cnst598;
        area.name = PhoneDataConstantPool1.cnst599;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst600;
        area.name = PhoneDataConstantPool2.cnst601;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst602;
        area.name = PhoneDataConstantPool2.cnst603;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst604;
        area.name = PhoneDataConstantPool2.cnst605;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst606;
        area.name = PhoneDataConstantPool2.cnst607;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst608;
        area.name = PhoneDataConstantPool2.cnst609;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst610;
        area.name = PhoneDataConstantPool2.cnst611;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst612;
        area.name = PhoneDataConstantPool2.cnst613;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst614;
        area.name = PhoneDataConstantPool2.cnst615;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst616;
        area.name = PhoneDataConstantPool2.cnst617;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst618;
        area.name = PhoneDataConstantPool2.cnst619;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst620;
        area.name = PhoneDataConstantPool2.cnst621;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst622;
        area.name = PhoneDataConstantPool2.cnst623;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst624;
        area.name = PhoneDataConstantPool2.cnst625;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst626;
        area.name = PhoneDataConstantPool2.cnst627;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst628;
        area.name = PhoneDataConstantPool2.cnst629;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst630;
        area.name = PhoneDataConstantPool2.cnst631;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst632;
        area.name = PhoneDataConstantPool2.cnst633;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst634;
        area.name = PhoneDataConstantPool2.cnst635;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst636;
        area.name = PhoneDataConstantPool2.cnst637;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst638;
        area.name = PhoneDataConstantPool2.cnst639;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst640;
        area.name = PhoneDataConstantPool2.cnst641;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst642;
        area.name = PhoneDataConstantPool2.cnst643;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst644;
        area.name = PhoneDataConstantPool2.cnst645;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst646;
        area.name = PhoneDataConstantPool2.cnst647;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst648;
        area.name = PhoneDataConstantPool2.cnst649;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst650;
        area.name = PhoneDataConstantPool2.cnst651;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst652;
        area.name = PhoneDataConstantPool2.cnst653;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst654;
        area.name = PhoneDataConstantPool2.cnst655;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst656;
        area.name = PhoneDataConstantPool2.cnst657;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst658;
        area.name = PhoneDataConstantPool2.cnst659;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst660;
        area.name = PhoneDataConstantPool2.cnst661;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst662;
        area.name = PhoneDataConstantPool2.cnst663;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst664;
        area.name = PhoneDataConstantPool2.cnst665;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst666;
        area.name = PhoneDataConstantPool2.cnst667;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst668;
        area.name = PhoneDataConstantPool2.cnst669;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst670;
        area.name = PhoneDataConstantPool2.cnst671;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst672;
        area.name = PhoneDataConstantPool2.cnst673;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst674;
        area.name = PhoneDataConstantPool2.cnst675;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst676;
        area.name = PhoneDataConstantPool2.cnst677;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst678;
        area.name = PhoneDataConstantPool2.cnst679;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst680;
        area.name = PhoneDataConstantPool2.cnst681;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst682;
        area.name = PhoneDataConstantPool2.cnst683;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst684;
        area.name = PhoneDataConstantPool2.cnst685;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst686;
        area.name = PhoneDataConstantPool2.cnst687;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst688;
        area.name = PhoneDataConstantPool2.cnst689;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst690;
        area.name = PhoneDataConstantPool2.cnst691;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst692;
        area.name = PhoneDataConstantPool2.cnst693;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst694;
        area.name = PhoneDataConstantPool2.cnst695;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst696;
        area.name = PhoneDataConstantPool2.cnst697;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst698;
        area.name = PhoneDataConstantPool2.cnst699;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst700;
        area.name = PhoneDataConstantPool2.cnst701;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst702;
        area.name = PhoneDataConstantPool2.cnst703;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst704;
        area.name = PhoneDataConstantPool2.cnst705;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst706;
        area.name = PhoneDataConstantPool2.cnst707;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst708;
        area.name = PhoneDataConstantPool2.cnst709;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst710;
        area.name = PhoneDataConstantPool2.cnst711;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst712;
        area.name = PhoneDataConstantPool2.cnst713;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst714;
        area.name = PhoneDataConstantPool2.cnst715;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst716;
        area.name = PhoneDataConstantPool2.cnst717;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst718;
        area.name = PhoneDataConstantPool2.cnst719;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst720;
        area.name = PhoneDataConstantPool2.cnst721;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst722;
        area.name = PhoneDataConstantPool2.cnst723;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst724;
        area.name = PhoneDataConstantPool2.cnst725;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst726;
        area.name = PhoneDataConstantPool2.cnst727;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst728;
        area.name = PhoneDataConstantPool2.cnst729;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst730;
        area.name = PhoneDataConstantPool2.cnst731;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst732;
        area.name = PhoneDataConstantPool2.cnst733;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst734;
        area.name = PhoneDataConstantPool2.cnst735;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst736;
        area.name = PhoneDataConstantPool2.cnst737;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst738;
        area.name = PhoneDataConstantPool2.cnst739;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst740;
        area.name = PhoneDataConstantPool2.cnst741;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst742;
        area.name = PhoneDataConstantPool2.cnst743;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst744;
        area.name = PhoneDataConstantPool2.cnst745;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst746;
        area.name = PhoneDataConstantPool2.cnst747;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst748;
        area.name = PhoneDataConstantPool2.cnst749;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst750;
        area.name = PhoneDataConstantPool2.cnst751;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst752;
        area.name = PhoneDataConstantPool2.cnst753;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst754;
        area.name = PhoneDataConstantPool2.cnst755;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst756;
        area.name = PhoneDataConstantPool2.cnst757;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst758;
        area.name = PhoneDataConstantPool2.cnst759;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst760;
        area.name = PhoneDataConstantPool2.cnst761;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst762;
        area.name = PhoneDataConstantPool2.cnst763;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst764;
        area.name = PhoneDataConstantPool2.cnst765;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst766;
        area.name = PhoneDataConstantPool2.cnst767;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst768;
        area.name = PhoneDataConstantPool2.cnst769;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst770;
        area.name = PhoneDataConstantPool2.cnst771;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst772;
        area.name = PhoneDataConstantPool2.cnst773;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst774;
        area.name = PhoneDataConstantPool2.cnst775;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst776;
        area.name = PhoneDataConstantPool2.cnst777;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst778;
        area.name = PhoneDataConstantPool2.cnst779;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst780;
        area.name = PhoneDataConstantPool2.cnst781;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst782;
        area.name = PhoneDataConstantPool2.cnst783;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst784;
        area.name = PhoneDataConstantPool2.cnst785;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst786;
        area.name = PhoneDataConstantPool2.cnst787;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst788;
        area.name = PhoneDataConstantPool2.cnst789;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst790;
        area.name = PhoneDataConstantPool2.cnst791;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst792;
        area.name = PhoneDataConstantPool2.cnst793;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst794;
        area.name = PhoneDataConstantPool2.cnst795;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst796;
        area.name = PhoneDataConstantPool2.cnst797;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst798;
        area.name = PhoneDataConstantPool2.cnst799;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst800;
        area.name = PhoneDataConstantPool2.cnst801;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst802;
        area.name = PhoneDataConstantPool2.cnst803;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst804;
        area.name = PhoneDataConstantPool2.cnst805;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst806;
        area.name = PhoneDataConstantPool2.cnst807;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst808;
        area.name = PhoneDataConstantPool2.cnst809;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst810;
        area.name = PhoneDataConstantPool2.cnst811;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst812;
        area.name = PhoneDataConstantPool2.cnst813;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst814;
        area.name = PhoneDataConstantPool2.cnst815;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst816;
        area.name = PhoneDataConstantPool2.cnst817;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst818;
        area.name = PhoneDataConstantPool2.cnst819;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst820;
        area.name = PhoneDataConstantPool2.cnst821;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst822;
        area.name = PhoneDataConstantPool2.cnst823;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst824;
        area.name = PhoneDataConstantPool2.cnst825;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst826;
        area.name = PhoneDataConstantPool2.cnst827;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst828;
        area.name = PhoneDataConstantPool2.cnst829;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst830;
        area.name = PhoneDataConstantPool2.cnst831;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst832;
        area.name = PhoneDataConstantPool2.cnst833;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst834;
        area.name = PhoneDataConstantPool2.cnst835;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst836;
        area.name = PhoneDataConstantPool2.cnst837;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst838;
        area.name = PhoneDataConstantPool2.cnst839;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst840;
        area.name = PhoneDataConstantPool2.cnst841;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst842;
        area.name = PhoneDataConstantPool2.cnst843;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst844;
        area.name = PhoneDataConstantPool2.cnst845;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst846;
        area.name = PhoneDataConstantPool2.cnst847;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst848;
        area.name = PhoneDataConstantPool2.cnst849;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst850;
        area.name = PhoneDataConstantPool2.cnst851;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst852;
        area.name = PhoneDataConstantPool2.cnst853;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst854;
        area.name = PhoneDataConstantPool2.cnst855;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst856;
        area.name = PhoneDataConstantPool2.cnst857;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst858;
        area.name = PhoneDataConstantPool2.cnst859;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst860;
        area.name = PhoneDataConstantPool2.cnst861;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst862;
        area.name = PhoneDataConstantPool2.cnst863;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst864;
        area.name = PhoneDataConstantPool2.cnst865;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst866;
        area.name = PhoneDataConstantPool2.cnst867;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst868;
        area.name = PhoneDataConstantPool2.cnst869;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst870;
        area.name = PhoneDataConstantPool2.cnst871;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst872;
        area.name = PhoneDataConstantPool2.cnst873;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst874;
        area.name = PhoneDataConstantPool2.cnst875;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst876;
        area.name = PhoneDataConstantPool2.cnst877;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst878;
        area.name = PhoneDataConstantPool2.cnst879;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst880;
        area.name = PhoneDataConstantPool2.cnst881;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst882;
        area.name = PhoneDataConstantPool2.cnst883;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst884;
        area.name = PhoneDataConstantPool2.cnst885;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst886;
        area.name = PhoneDataConstantPool2.cnst887;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst888;
        area.name = PhoneDataConstantPool2.cnst889;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst890;
        area.name = PhoneDataConstantPool2.cnst891;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst892;
        area.name = PhoneDataConstantPool2.cnst893;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst894;
        area.name = PhoneDataConstantPool2.cnst895;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst896;
        area.name = PhoneDataConstantPool2.cnst897;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool2.cnst898;
        area.name = PhoneDataConstantPool2.cnst899;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst900;
        area.name = PhoneDataConstantPool3.cnst901;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst902;
        area.name = PhoneDataConstantPool3.cnst903;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst904;
        area.name = PhoneDataConstantPool3.cnst905;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst906;
        area.name = PhoneDataConstantPool3.cnst907;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst908;
        area.name = PhoneDataConstantPool3.cnst909;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst910;
        area.name = PhoneDataConstantPool3.cnst911;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst912;
        area.name = PhoneDataConstantPool3.cnst913;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst914;
        area.name = PhoneDataConstantPool3.cnst915;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst916;
        area.name = PhoneDataConstantPool3.cnst917;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst918;
        area.name = PhoneDataConstantPool3.cnst919;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst920;
        area.name = PhoneDataConstantPool3.cnst921;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst922;
        area.name = PhoneDataConstantPool3.cnst923;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst924;
        area.name = PhoneDataConstantPool3.cnst925;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst926;
        area.name = PhoneDataConstantPool3.cnst927;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst928;
        area.name = PhoneDataConstantPool3.cnst929;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst930;
        area.name = PhoneDataConstantPool3.cnst931;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst932;
        area.name = PhoneDataConstantPool3.cnst933;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst934;
        area.name = PhoneDataConstantPool3.cnst935;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst936;
        area.name = PhoneDataConstantPool3.cnst937;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst938;
        area.name = PhoneDataConstantPool3.cnst939;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst940;
        area.name = PhoneDataConstantPool3.cnst941;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst942;
        area.name = PhoneDataConstantPool3.cnst943;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst944;
        area.name = PhoneDataConstantPool3.cnst945;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst946;
        area.name = PhoneDataConstantPool3.cnst947;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst948;
        area.name = PhoneDataConstantPool3.cnst949;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst950;
        area.name = PhoneDataConstantPool3.cnst951;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst952;
        area.name = PhoneDataConstantPool3.cnst953;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst954;
        area.name = PhoneDataConstantPool3.cnst955;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst956;
        area.name = PhoneDataConstantPool3.cnst957;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst958;
        area.name = PhoneDataConstantPool3.cnst959;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst960;
        area.name = PhoneDataConstantPool3.cnst961;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst962;
        area.name = PhoneDataConstantPool3.cnst963;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst964;
        area.name = PhoneDataConstantPool3.cnst965;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst966;
        area.name = PhoneDataConstantPool3.cnst967;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst968;
        area.name = PhoneDataConstantPool3.cnst969;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst970;
        area.name = PhoneDataConstantPool3.cnst971;
        ars.addElement(area);
        area = new Area();
        area.code = PhoneDataConstantPool3.cnst972;
        area.name = PhoneDataConstantPool3.cnst973;
        ars.addElement(area);

    }
    private static Area area;
}
