/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser;


/**
 * Phone number parser interface.
 */
public interface PhoneNumberParser {
    /**
     * Sets country code.
     * 
     * @param countryCode
     *            country code
     */
    void setSettingsKey(String countryCode);

    /**
     * Set national format as priority.
     * 
     * @param nationalAsPriority
     *            the national format priority value
     */
    void setNationalAsPriority(boolean nationalAsPriority);

    /**
     * Parse <code>phoneNumber</code>.
     */
    PhoneNumber parse(String phoneNumber);

    /**
     * Parse <code>phoneNumber</code> and put result into <code>container</code>
     */
    PhoneNumber parse(String phoneNumber, PhoneNumber container);

    /**
     * Defines if current station location values are set according to parameters.
     * 
     * @param country
     *            country code
     * @param area
     *            area code
     * @param internationalPrefix
     *            international prefix (e.g. 011)
     * 
     * @return <code>true</code> if the phone parser is set to the parameters, <code>false</code> otherwise
     */
    boolean isStationLocationSetTo(String country, String area,
            String internationalPrefix);

    /**
     * Sets station location.
     * 
     * @param country
     *            country code
     * @param area
     *            area code
     * @param internationalPrefix
     *            international prefix (e.g. 011)
     * 
     * @return <code>true</code> on success, <code>false</code> otherwise
     */
    boolean setStationLocation(String country, String area,
            String internationalPrefix);
    
    /**
     * Returns parser settings configuration as string for logging.
     * 
     * @return parser settings configuration as string for logging
     */
    String getParserSettingsDescription();
    
    /**
     * Get phone number container.
     * 
     * @see #returnPhoneNumberContainer(PhoneNumber)
     * @see #parse(String, PhoneNumber)
     */
    PhoneNumber getPhoneNumberContainer();

    /**
     * Retrieve container.
     * 
     * @see #getPhoneNumberContainer()
     */
    void returnPhoneNumberContainer(PhoneNumber container);

    void setIgnoreFixedDialWhenInter(boolean ignoreFixedDialWhenInter);
    void setNoShortNumbers(boolean noShortNumbers);
    void setCheckForCCWhenNaP(boolean checkForCCWhenNaP);
    void setDnProcSubAddress(boolean dnProcSubAddress);
    void setAllowStars(boolean allowStar);
}
