/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser.impl;

import java.util.Vector;

/**
 * Phone parser utilities.
 */
public class Utils {

    private static final String EMPTY = "";

    public static boolean isEmpty(String str) {
        return str == null || EMPTY.equals(str);
    }

    public static boolean isEmpty(Integer in) {
        return in == null || in.intValue() == 0;
    }

    public static boolean isEmpty(Long in) {
        return in == null || in.longValue() == 0L;
    }

    public static boolean isEmptyTrimmed(String str) {
        return str == null || EMPTY.equals(str.trim());
    }

    public static boolean isEmpty(byte[] array) {
        return array == null || array.length == 0;
    }

    public static boolean isEmpty(Object[] array) {
        return array == null || array.length == 0;
    }

    public static boolean isEmpty(Vector c) {
        return c == null || c.isEmpty();
    }

    public static boolean isDigit(char chr) {
        return chr >= '0' && chr <= '9';
    }
}
