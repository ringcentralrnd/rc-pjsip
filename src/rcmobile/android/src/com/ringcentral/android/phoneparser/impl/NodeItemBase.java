/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser.impl;

class NodeItemBase {
    private Country country;

    NodeItemBase() {
    }

    boolean isCountry() {
        return false;
    }

    boolean isUKCountry() {
        return false;
    }

    boolean isArea() {
        return false;
    }

    void setCountry(Country country) {
        this.country = country;
    }

    Country getCountry() {
        if (isCountry()) {
            return (Country) this;
        }
        return country;
    }
}
