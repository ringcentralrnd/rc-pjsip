/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.utils.PhoneUtils;

/**
 * RingCentral phone number entering formatter.
 * 
 * The code is inherited from RCM iPhone client v. 1.7 - USA oriented
 * 
 * TODO: Re-factor if required for other countries.
 */
public class PhoneNumberFormatter {
    /**
     * Phone number valid characters set.
     */
    private static final String NUMBER_SYMBOLS_SET = "+0123456789#*";

    /**
     * Extra number delimiters.
     */
    private static final String EXTRA_CHARS_SET = "*#";

    /**
     * Keeps current international separator.
     */
    private String currentInternationalSeparator;

    /**
     * Keeps current international code.
     */
    private String currentInternationalCode;

    /**
     * Keeps current maximum international number.
     */
    private int currentMaximumInternalNumber;
    
    /**
     * Keeps current area code length.
     */
    private int currentAreaCodeLength;
    
    /**
     * Keeps length of current number (first part).
     */
    private int currentLocalNumber1stPartLength;
    
    /**
     * Keeps length of current number (second part).
     */
    private int currentLocalNumber2dPartLength;
    
    static final String TAG = "[RC] PhoneNumberFormatter";
    /**
     * Default constructor.
     */
    public PhoneNumberFormatter() {
        currentInternationalSeparator = "+";
        currentInternationalCode = "011";
        currentMaximumInternalNumber = 10;
        currentAreaCodeLength = 3;
        currentLocalNumber1stPartLength = 3;
        currentLocalNumber2dPartLength = 4;
    }
    
    /**
     * Constructs with certain parameters.
     * 
     * @param internationalSeparator international separator
     * @param internationalCode international code
     * @param maximumInternalNumber maximum internal number
     * @param areaCodeLength area code length
     * @param localNumber1stPartLength local number 1st part length
     * @param localNumber2dPartLength local number 1d part length
     */
    public PhoneNumberFormatter(String internationalSeparator,
            String internationalCode,
            int maximumInternalNumber,
            int areaCodeLength,
            int localNumber1stPartLength,
            int localNumber2dPartLength) {
        currentInternationalSeparator = internationalSeparator;
        currentInternationalCode = internationalCode;
        currentMaximumInternalNumber = maximumInternalNumber;
        currentAreaCodeLength = areaCodeLength;
        currentLocalNumber1stPartLength = localNumber1stPartLength;
        currentLocalNumber2dPartLength = localNumber2dPartLength;
    }
    
    /**
     * Trims all non <code>+0123456789#*</code> characters.
     * 
     * @param phoneNumber
     *            the phone number to be trimmed
     * 
     * @return the trimmed result
     */
    public static final String trimNonNumberSymbols(String phoneNumber) {
        StringBuffer sb = new StringBuffer();
        int len = phoneNumber.length();
        char ch;
        for (int i = 0; i < len; i++) {
            ch = phoneNumber.charAt(i);
            if (NUMBER_SYMBOLS_SET.indexOf(ch) != -1) {
                sb.append(ch);
            }
        }
        return sb.toString();
    }
    
    /**
     * Trims all no <code>0123456789</code> characters.
     * 
     * @param phoneNumber 
     *                the phone number to be trimmed
     *                
     * @return the trimmed result
     */
    public static final String trimNonDigitSymbols( String phoneNumber )
    {
        if (phoneNumber == null)
            return new String();
            
        StringBuffer result = new StringBuffer();
        for(int charIndex = 0; charIndex < phoneNumber.length(); charIndex ++)
        {
            char symbol = phoneNumber.charAt(charIndex);
            if((symbol >= '0') && (symbol <= '9'))
                result.append(symbol);
        }
        return result.toString();
    }
    
    /**
     * Defines if <code>phoneNumber</code> keeps valid symbols numbers.
     * 
     * @param phoneNumber
     *            the phone number
     * 
     * @return <code>true</code> if <code>phoneNumber</code> keeps valid symbols
     *         numbers, <code>false</code> otherwise.
     */
    public static final boolean hasPhoneNumberSymbols(String phoneNumber) {
        int len = phoneNumber.length();
        char ch;
        for (int i = 0; i < len; i++) {
            ch = phoneNumber.charAt(i);
            if (NUMBER_SYMBOLS_SET.indexOf(ch) == -1) {
                return false;
            }
        }
        return true;
    }

    /**
     * Defines if <code>symbol</code> is a valid number symbol.
     * 
     * @param symbol
     *            the symbol
     * 
     * @return <code>true</code> if <code>symbol</code> is a valid number
     *         symbol, <code>false</code> otherwise.
     */
    public static final boolean isPhoneNumberSymbol(char symbol) {
        if (NUMBER_SYMBOLS_SET.indexOf(symbol) != -1) {
            return true;
        }
        return false;
    }
    
    /**
     * Returns formatted phone number.
     * 
     * @param phoneNumber the original (entered) phone number.
     *  
     * @return formatted phone number
     */
    public String getFormatted(String phoneNumber) {
        String sourceString = trimNonNumberSymbols(phoneNumber);
        String phone;
        String extra;
        String formattedNumber = null;

        int extraIndex = -1;
        int len = EXTRA_CHARS_SET.length();
        for (int i = 0; i < len; i++) {
            int index = sourceString.indexOf(EXTRA_CHARS_SET.charAt(i));
            if (index != -1) {
                if (extraIndex == -1) {
                    extraIndex = index;
                } else {
                    if (index < extraIndex) {
                        extraIndex = index;
                    }
                }
            }
        }

        if (extraIndex == -1) {
            phone = sourceString;
            extra = null;
        } else {
            phone = sourceString.substring(0, extraIndex);
            extra = sourceString.substring(extraIndex);
        }

    	if (phone.length() <= 5) {
            formattedNumber = phone;
    	}
        if (phone.startsWith(currentInternationalCode)
                || phone.startsWith("+")) {
            formattedNumber = formatInternationalNumber(phone);
        } else if (phone.length() <= 11 && phone.startsWith("1")) {
            formattedNumber = "1 "
                    + formatCurrentAreaCodeNumber(iSubstring(phone, 1));
        } else if (phone.length() <= 7) {
            formattedNumber = formatLocalNumber(phone);
        } else if (phone.length() <= 10) {
            formattedNumber = formatCurrentAreaCodeNumber(phone);
        } else {
            formattedNumber = phone;
        }

        return ((extra != null && extra.length() > 0) ? 
                (formattedNumber + " " + extra)
                : formattedNumber).trim();
    }

    /**
     * Returns phone number in the international format.
     * 
     * @param phoneNumber
     *            the phone number
     * 
     * @return phone number in the international format
     */
    private String formatInternationalNumber(String phoneNumber) {
        String formattedNumber;

        String internetionalCode;
        String countryCodeNumber;

        if (phoneNumber.startsWith(currentInternationalCode)) {
            internetionalCode = currentInternationalCode + " ";
            countryCodeNumber = iSubstring(phoneNumber, 
                    currentInternationalCode.length());
        } else if (phoneNumber.startsWith(currentInternationalSeparator)) {
            internetionalCode = currentInternationalSeparator;
            countryCodeNumber = iSubstring(phoneNumber, 
                    currentInternationalSeparator.length());
        } else {
            return phoneNumber;
        }

        if (countryCodeNumber.startsWith("1")
                && (countryCodeNumber.length() > 1)) {
            if (countryCodeNumber.length() <= 
                (currentMaximumInternalNumber + 1)) {
                formattedNumber = internetionalCode
                        + "1 "
                        + formatCurrentAreaCodeNumber(
                                iSubstring(countryCodeNumber, 1));
            } else {
                formattedNumber = internetionalCode + "1 "
                        + iSubstring(countryCodeNumber, 1);
            }
        } else
            formattedNumber = internetionalCode + countryCodeNumber;

        return formattedNumber;
    }

    /**
     * Returns formatted code area.
     * 
     * @param areaCodePhoneNumber
     *            the code area
     * 
     * @return formatted code area
     */
    private String formatCurrentAreaCodeNumber(String areaCodePhoneNumber) {
        String areaCode = iSubstring(areaCodePhoneNumber, 
                0, currentAreaCodeLength);

        if (3 == areaCode.length()) {
            areaCode = "(" + areaCode + ")";
        } else if (areaCode.length() > 0) {
            areaCode = "(" + areaCode + " )";
        }

        String formattedLocalNumber = null;
        if (areaCodePhoneNumber.length() > currentAreaCodeLength)
            formattedLocalNumber = formatLocalNumber(
                    iSubstring(areaCodePhoneNumber, currentAreaCodeLength));

        return (formattedLocalNumber != null) ? 
                (areaCode + " " + formattedLocalNumber)
                : areaCode;
    }

    /**
     * Returns formatted local number.
     * 
     * @param localNumber
     *            the phone number.
     * 
     * @return formatted local number.
     */
    private String formatLocalNumber(String localNumber) {
        String phone1 = iSubstring(localNumber, 0,
                currentLocalNumber1stPartLength);
        String phone2 = iSubstring(localNumber,
                currentLocalNumber1stPartLength,
                currentLocalNumber2dPartLength);
            
        if (phone2.length() > 0) {
            phone2 = "-" + phone2;
        }

        return phone1 + phone2;
    }
    
    /**
     * Substring the string.
     * 
     * @param str the string
     * @param bIndex the beginning index, inclusive
     * @param len the length from the bIndex
     * @return substring or empty string if out of bounds
     */
    private static final String iSubstring(String str, int bIndex, int len) {
        int strLen = str.length();
        if (bIndex < strLen) {
            int endIndex = bIndex + len;
            if (endIndex < strLen) {
                return str.substring(bIndex, endIndex);
            } else {
                return str.substring(bIndex);
            }
        }
        return "";
    }
    
    /**
     * Substring the string.
     * 
     * @param str the string
     * @param bIndex the beginning index, inclusive
     * @return substring or empty string if out of bounds
     */
    private static final String iSubstring(String str, int bIndex) {
        int strLen = str.length();
        if (bIndex < strLen) {
            return str.substring(bIndex);
        }
        return "";
    }

    /**
     * Defines if the <code>phoneNumber</code> is valid as RC Mobile Number for
     * RingOut. Used when the end-user enters new RC Mobile Phone Number.
     *
     * @param phoneNumber the phone number to check
     * @return <code>true</code> if the <code>phoneNumber</code> can be set as
     * RC Mobile Number for RingOut.
     */
    public static boolean isValidAsMobilePhoneNumber(String phoneNumber) {
        try {
            if (phoneNumber == null) {
                return false;
            }
            String num = PhoneNumberFormatter.trimNonNumberSymbols(phoneNumber);
            PhoneNumberParser parser = PhoneUtils.getParser();
            PhoneNumber pn = parser.parse(num, new PhoneNumber());
//            if ((pn != null) && pn.isFullValid && !pn.isShortNumber && !pn.isTollFree && !pn.isSpecialNumber) {
//            if ((pn != null) && pn.isFullValid && !pn.isShortNumber && !pn.isSpecialNumber) {
//            Conditions changes via PP-2112
            if ((pn != null) && pn.isValid && !pn.isShortNumber && !pn.isTollFree && !pn.isSpecialNumber) {
                return true;
            }
        } catch (Exception ex) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "isValidAsMobilePhoneNumber()", ex);
            }
        }
        return false;
    }
}
