/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser;

/**
 * Phone number parser exception.
 */
public class PhoneParserException extends Exception {

    private static final long serialVersionUID = -4014910189150255851L;

    public PhoneParserException() {
    }

    public PhoneParserException(String message) {
        super(message);
    }

    public PhoneParserException(String number, String parserConfigAsString) {
        super("Unable to parse number: " + number
                + ", with PhoneParser settings: " + parserConfigAsString);
    }
}
