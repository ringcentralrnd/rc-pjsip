/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser.impl;

import java.util.Vector;

class Node {

    private Node[] nodes = new Node[10];
    private Node parentNode;
    private Vector items;

    public Node(Node lpParent) {
        parentNode = lpParent;
        items = null;
        zeroNodes();
    }

    public Node() {
    }

    private int getItemsCount() {
        return Utils.isEmpty(items) ? 0 : items.size();
    }

    private Vector getItems() {
        if (Utils.isEmpty(items)) {
            items = new Vector();
        }
        return items;
    }

    private void zeroNodes() {
    }

    public boolean isLoaded() {
        for (int i = 0; i < 10; i++)
            if (isNodeExist(i)) {
                return true;
            }
        return false;
    }

    private int getNodeIndex(char cDigit) {
        int a = cDigit - '0';
        return (a);
    }

    private boolean isNodeExist(int iIndex) {
        return (nodes[iIndex] != null);
    }

    private boolean isNodeExist(char cDigit) {
        return isNodeExist(getNodeIndex(cDigit));
    }

    private Node getNode(int iIndex) {
        if (!isNodeExist(iIndex))
            nodes[iIndex] = new Node(this);

        return nodes[iIndex];
    }

    private Node getNode(char cDigit) {
        return getNode(getNodeIndex(cDigit));
    }

    private void addArea(String szNum, Country lpCountry, Area xArea) {
        if (Utils.isEmpty(szNum)) {
            Area lpArea = xArea;
            lpArea.setCountry(lpCountry);
            getItems().addElement(lpArea);
        } else {
            getNode(szNum.charAt(0)).addArea(szNum.substring(1), lpCountry,
                    xArea);
        }
    }

    protected void onCountryAdded(Country lpCountry) {
        if (parentNode != null) {
            parentNode.onCountryAdded(lpCountry);
        }
    }

    private void addCountry(String szNum, Country xCountry) {
        if (Utils.isEmpty(szNum)) {
            Country lpCountry = xCountry;
            getItems().addElement(lpCountry);
            onCountryAdded(lpCountry);

            Vector areas = lpCountry.getAreas();
            for (int i=0; i<areas.size(); i++) {
                Area xArea = (Area)areas.elementAt(i);
                addArea(xArea.code, lpCountry, xArea);
            }
        } else {
            getNode(szNum.charAt(0)).addCountry(szNum.substring(1), xCountry);
        }
    }

    public void addCountry(Country xCountry) {
        addCountry(xCountry.getCode(), xCountry);
    }

    public void addData(Vector xData) {
        for (int i=0; i<xData.size(); i++) {
            Country country = (Country)xData.elementAt(i);
            addCountry(country);
        }
    }

    // private boolean isCountryNode() {
    // int iCount = getItemsCount();
    // for (int i = 0; i < iCount; i++) {
    // NodeItemBase lpItem = getItems().get(i);
    // if (lpItem.isCountry())
    // return true;
    // }
    // return false;
    // }

    private Country isRemoveNPrefixAfterCC() {
        Vector items = getItems();
        for (int i=0; i<items.size(); i++) {
            NodeItemBase lpItem = (NodeItemBase)items.elementAt(i);
            if (lpItem.isCountry()) {
                Country lpCountry = (Country) lpItem;
                if (lpCountry.isRemoveNPrefixAfterCC()) {
                    return lpCountry;
                }
            }
        }

        return null;
    }

    private boolean get(CharArrayExt szNum, CountryAreaContainer container) {
        char cCur = 0;
        if (!szNum.isEmpty()) {
            cCur = szNum.charAt(0);
        }

        Country lpSettingsCountry = isRemoveNPrefixAfterCC();

        if ((cCur != 0) && lpSettingsCountry != null) {
            // Special processing for numbers like +44(0)204445555

            int iLen = lpSettingsCountry.getNationalPrefix().length();

            if ((iLen > 0)
                    && szNum
                            .startsWith(lpSettingsCountry.getNationalPrefix())) {
                szNum.cut(iLen);
                cCur = szNum.charAt(0);
                container.areaOffset = iLen;
            }
        }

        if ((cCur != 0) && isNodeExist(cCur)) {
            if (getNode(cCur).get(szNum.cut(1), container)) {
                return true;
            }
        }

        int iCount = getItemsCount();

        if (iCount == 1) { // Normally
            NodeItemBase lpItem = (NodeItemBase)getItems().elementAt(0);
            if (lpItem.isArea()) {
                container.area = (Area) lpItem;
            }
            container.country = lpItem.getCountry();
            return true;
        } else {
            if (iCount > 1) {
                for (int i = 0; i < iCount; i++) {
                    NodeItemBase lpItem = (NodeItemBase)getItems().elementAt(i);

                    if (lpItem.isCountry()) {
                        Country lpCountry = (Country) lpItem;

                        if (lpCountry.getAreas().size() == 0) {
                            container.country = lpCountry;
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public boolean getCountryArea(String num, CountryAreaContainer container) {
        char cCur = 0;
        if (!Utils.isEmpty(num)) {
            cCur = num.charAt(0);
        }
        CharArrayExt numArray = new CharArrayExt(num);
        return (cCur != 0) && isNodeExist(cCur)
                && getNode(cCur).get(numArray.cut(1), container);
    }
}
