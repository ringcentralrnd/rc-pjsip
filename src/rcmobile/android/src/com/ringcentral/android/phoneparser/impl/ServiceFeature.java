/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser.impl;

public class ServiceFeature {

    String sequence;
    ServiceFeatureType type;

    public ServiceFeatureType getType() {
        return type;
    }

    static public class ServiceFeatureType
    {
        static public final int enPDSFTUnknown = 0;            //Unspecified
        static public final int enPDSFTBlockCLID = 1;          //Block CLID
        static public final int enPDSFTCallback = 2;           //Callback
        static public final int enPDSFTVoiceMail = 3;          //Voice mail
        
        private int m_code;
        
        ServiceFeatureType(int code){
            m_code = code;
        }
        
        public static ServiceFeatureType getByCode(int code)
        {
            switch(code)
            {
                case enPDSFTUnknown:
                case enPDSFTBlockCLID:
                case enPDSFTCallback:
                case enPDSFTVoiceMail:
                    return new ServiceFeatureType(code);
                default:
                    return null;
            }
        }
    }

}

