package com.ringcentral.android.phoneparser;

import java.util.Comparator;



public class NamedPhoneNumber extends PhoneNumber
{
    private static final String LOG_TAG = "RCNamPhNum:";
//    private static final boolean DEBUG_ENABLED = Logger.DEBUG_ENABLED;
    
    
    protected String m_name = "";
    
    public NamedPhoneNumber()
    {}
    
    public NamedPhoneNumber(String name)
    {
        m_name = name;
    }
    
    public NamedPhoneNumber(PhoneNumber basePhoneNumber)
    {
        super(basePhoneNumber);
    }
    
    public NamedPhoneNumber(String name, PhoneNumber basePhNum)
    {
        super(basePhNum);
        m_name = name;
    }
    
    // copy constructor
    public NamedPhoneNumber(NamedPhoneNumber base)
    {
        super(base);
        m_name = base.m_name;
    }
    
    public String toString()
    {
        StringBuffer sb = new StringBuffer(m_name);
        if (!m_name.equals("")) {
            sb.append(" ");
        }
        sb.append(localCanonical);
        
        return sb.toString();
    }
    
    public boolean equals(NamedPhoneNumber another)
    {
        if (m_name.equals(another.m_name) && super.equals(another))
            return true;
        return false;
    }
    
    public boolean equals(PhoneNumber another)
    {
        boolean result = super.equals(another); 
        return result;
    }

    /////////////////////////////// Serializable interface implementation
    
       
    
    static public Comparator getComparator() {
        NamedPhNumComparator comparator = new NamedPhNumComparator();
        return comparator;
    }
    
    
    /**
     * comparator sorting at first by name and then by local canonical number
     */
    static private class NamedPhNumComparator implements Comparator{
        
        private static final String LOG_TAG = NamedPhoneNumber.LOG_TAG + "NPNComp:";
        
        public int compare(Object o1, Object o2) {
            if (o1==null || o2==null)
                throw new NullPointerException(LOG_TAG + "compare:" + "parameter is null");
            
            NamedPhoneNumber npn1 = (NamedPhoneNumber)o1;
            NamedPhoneNumber npn2 = (NamedPhoneNumber)o2;
            
            int nameCompResult = npn1.m_name.compareTo(npn2.m_name);
            if (nameCompResult != 0)
                return nameCompResult;
            
            // names are equal here
            return npn1.localCanonical.compareTo(npn2.localCanonical);
        }
    }
    
}
