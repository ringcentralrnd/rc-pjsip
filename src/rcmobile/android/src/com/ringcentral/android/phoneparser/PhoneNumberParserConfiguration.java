/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser;

import com.ringcentral.android.phoneparser.impl.PhoneNumberParserImpl;

/**
 * RC Phone parser factory class.
 */
public final class PhoneNumberParserConfiguration {
    /**
     * Determines if the parser data has been loaded.
     * 
     * @return <code>true</code> if loaded, <code>false</code> otherwise
     */
    public static final boolean isDataLoaded() {
        return PhoneNumberParserImpl.isRootDataLoaded();
    }

    /**
     * Determines if the parser data is loading.
     * 
     * @return <code>true</code> if the loading is in progress,
     *         <code>false</code> otherwise
     */
    public static boolean isDataLoading() {
        return PhoneNumberParserImpl.isRootDataLoading();
    }

    /**
     * Loads phone data. Shall be executed asynchronously.
     */
    public static void loadData() {
        PhoneNumberParserImpl.loadData();
    }

    /**
     * Obtain new instance of default phone parser.
     * 
     * @return phone parser new instance
     * 
     * @throws IllegalStateException
     *             if the phone data was not loaded, see
     *             <code>isLoaded, isDataLoading, loadData </code>
     */
    public final static PhoneNumberParser getNewParser()
            throws IllegalStateException {
        return new PhoneNumberParserImpl();
    }

    /**
     * Obtain new instance of a phone parser with phone number.
     * 
     * @return phone parser new instance
     * 
     * @throws IllegalStateException
     *             if the phone data was not loaded, see
     *             <code>isLoaded, isDataLoading, loadData </code>
     * @throws Exception
     *             if the phoneNumber is not valid>
     */
    public final static PhoneNumberParser getNewParser(String phoneNumber)
            throws IllegalStateException, Exception {
        return new PhoneNumberParserImpl(phoneNumber);
    }
}
