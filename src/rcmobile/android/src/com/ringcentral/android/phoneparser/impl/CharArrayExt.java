/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser.impl;

class CharArrayExt {

    private char[] charArray;
    private int currentStartPos;
    private int currentEndPos;
    private int currentLength;

    CharArrayExt(char[] charArray) {
        this.charArray = charArray;
        currentLength = charArray.length;
        currentEndPos = currentLength;
    }

    CharArrayExt(String str) {
        this(str.toCharArray());
    }

    int length() {
        return currentLength;
    }

    CharArrayExt cut(int newStartPos, int newEndPos) {
        if (newStartPos < 0 || newEndPos < 0 || newStartPos > newEndPos
                || newEndPos > length()) {
            throw new RuntimeException(
                    "Incorrect array indexes. Current array length = "
                            + length() + " requested indexes are "
                            + newStartPos + ", " + newEndPos);
        }
        currentLength = newEndPos - newStartPos;
        currentStartPos += newStartPos;
        currentEndPos = currentStartPos + currentLength;
        return this;
    }

    CharArrayExt cut(int newStartPos) {
        if (newStartPos < 0 || newStartPos > length()) {
            throw new RuntimeException(
                    "Incorrect array index. Current array length = "
                            + length() + " requested index is " + newStartPos);
        }
        currentLength = length() - newStartPos;
        currentStartPos += newStartPos;
        currentEndPos = currentStartPos + currentLength;
        return this;
    }

    boolean charExists(int index) {
        return index >= 0 && index < length();
    }

    char charAt(int index) {
        if (!charExists(index)) {
            throw new RuntimeException(
                    "Incorrect array index. Current array length = "
                            + length() + " requested index is " + index);
        }
        return charArray[currentStartPos + index];
    }

    int indexOf(String str) {
        return indexOf(str, 0);
    }

    int indexOf(String str, int fromIndex) {
        if (!charExists(fromIndex)) {
            throw new RuntimeException(
                    "Incorrect array index. Current array length = "
                            + length() + " requested index is " + fromIndex);
        }
        if (Utils.isEmpty(str)) {
            throw new RuntimeException("Searching string is null or empty");
        }

        int prevIndx = 0;
        int nextIndx;
        for (int i = fromIndex; i < currentEndPos - currentStartPos;) { // "i"
            // will
            // only
            // increment
            // in
            // nested
            // cycle
            for (int j = 0; j < str.length(); j++) { // searching string char by
                // char
                nextIndx = indexOf(str.charAt(j), i);
                if (nextIndx < 0) {
                    return -1;
                }
                if (j > 0 && (nextIndx != prevIndx + 1)) {
                    i = nextIndx - j; // the case then tho neighbour symbols of
                    // "str" are found, but they are not
                    // neighbour in original char array
                    break;
                }
                if (j == str.length() - 1) {
                    return nextIndx - j; // the last symbol of searching string
                    // has been found
                }
                i = nextIndx + 1; // increment global index
                if (i >= currentEndPos) {
                    return -1;
                }
                prevIndx = nextIndx;
            }
        }
        return -1;
    }

    int indexOf(char chr) {
        return indexOf(chr, 0);
    }

    int indexOf(char chr, int fromIndex) {
        if (!charExists(fromIndex)) {
            throw new RuntimeException(
                    "Incorrect array index. Current array length = "
                            + length() + " requested index is " + fromIndex);
        }
        for (int i = currentStartPos + fromIndex; i < currentEndPos; i++) {
            if (charArray[i] == chr) {
                return i - currentStartPos;
            }
        }
        return -1;
    }

    boolean startsWith(String str) {
        if (Utils.isEmpty(str)) {
            throw new RuntimeException("Searching string is null or empty");
        }
        for (int i = 0; i < str.length(); i++) {
            if (!charExists(i) || charAt(i) != str.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    boolean startsWith(char chr) {
        return length() > 0 && charArray[currentStartPos] == chr;
    }

    boolean isEmpty() {
        if (length() == 0) {
            return true;
        }
        for (int i = currentStartPos; i < currentEndPos; i++) {
            if (charArray[i] != ' ') {
                return false;
            }
        }
        return true;
    }

    void toUpperCase() {
        for (int i = currentStartPos; i < currentEndPos; i++) {
            if (charArray[i] >= 'a' && charArray[i] <= 'z') {
                charArray[i] = (char) ((int) charArray[i] - 0x20);
            }
        }
    }

    public String toString() {
        return new String(charArray, currentStartPos, currentLength);
    }

    char[] toCharArray() {
        char[] tmp = new char[currentLength];
        System.arraycopy(charArray, currentStartPos, tmp, 0, currentLength);
        return tmp;
    }

    String substring(int beginIndex, int endIndex) {
        if (beginIndex < 0 || endIndex < 0 || beginIndex > endIndex
                || endIndex > length()) {
            throw new RuntimeException(
                    "Incorrect array indexes. Current array length = "
                            + length() + " requested indexes are "
                            + beginIndex + ", " + endIndex);
        }

        return new String(charArray, currentStartPos + beginIndex, endIndex
                - beginIndex);
    }

    String substring(int beginIndex) {
        return substring(beginIndex, currentLength);
    }

    void skipSpaceLeft() {
        // cutting of spaces & tabs from right
        int charsToSkip = 0;
        for (int i = 0; i < length(); i++) {
            char nextChar = charAt(i);
            if ((nextChar != ' ') && (nextChar != '\t')) {
                break;
            }
            charsToSkip++;
        }
        if (charsToSkip > 0) {
            cut(charsToSkip);
        }
    }

    void skipSpaceRight() {
        // cutting of spaces & tabs from right
        int charsToCutFromRight = 0;
        for (int i = length() - 1; i >= 0; i--) {
            char nextChar = charAt(i);
            if ((nextChar != ' ') && (nextChar != '\t')) {
                break;
            }
            charsToCutFromRight++;
        }
        if (charsToCutFromRight > 0) {
            cut(0, length() - charsToCutFromRight);
        }
    }

    void trim() {
        skipSpaceLeft();
        skipSpaceRight();
    }

    boolean equals(String str) {
        if (str == null) {
            throw new RuntimeException("Searching string is null");
        }
        if (length() != str.length()) {
            return false;
        }
        for (int i = 0; i < str.length(); i++) {
            if (charAt(i) != str.charAt(i)) {
                return false;
            }
        }
        return true;
    }
}
