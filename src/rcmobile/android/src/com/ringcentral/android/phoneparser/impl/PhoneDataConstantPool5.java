/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser.impl;

/**
 * Auto-generated. 
 * Phone data version: 1
 * Phone data revision: 12
 */
final class PhoneDataConstantPool5 {
    static final String cnst1500 = "573";
    static final String cnst1501 = "571";
    static final String cnst1502 = "570";
    static final String cnst1503 = "32";
    static final String cnst1504 = "31";
    static final String cnst1505 = "30";
    static final String cnst1506 = "Russia";
    static final String cnst1507 = "French Polynesia";
    static final String cnst1508 = "689";
    static final String cnst1509 = "Vanuatu";
    static final String cnst1510 = "678";
    static final String cnst1511 = "Guatemala";
    static final String cnst1512 = "502";
    static final String cnst1513 = "Poland";
    static final String cnst1514 = "48";
    static final String cnst1515 = "Bulgaria";
    static final String cnst1516 = "359";
    static final String cnst1517 = "Sudan";
    static final String cnst1518 = "249";
    static final String cnst1519 = "Cape Verde Islands";
    static final String cnst1520 = "238";
    static final String cnst1521 = "Niger";
    static final String cnst1522 = "227";
    static final String cnst1523 = "Tunisia";
    static final String cnst1524 = "216";
    static final String cnst1525 = "Uzbekistan";
    static final String cnst1526 = "998";
    static final String cnst1527 = "Mongolia";
    static final String cnst1528 = "976";
    static final String cnst1529 = "Kuwait";
    static final String cnst1530 = "965";
    static final String cnst1531 = "Cambodia";
    static final String cnst1532 = "855";
    static final String cnst1533 = "Fiji";
    static final String cnst1534 = "679";
    static final String cnst1535 = "El Salvador";
    static final String cnst1536 = "503";
    static final String cnst1537 = "Germany";
    static final String cnst1538 = "49";
    static final String cnst1539 = "South Africa";
    static final String cnst1540 = "27";
    static final String cnst1541 = "Sao Tome and Principe";
    static final String cnst1542 = "239";
    static final String cnst1543 = "Togo";
    static final String cnst1544 = "228";
    static final String cnst1545 = "Nepal";
    static final String cnst1546 = "977";
    static final String cnst1547 = "Saudi Arabia";
    static final String cnst1548 = "966";
    static final String cnst1549 = "Laos";
    static final String cnst1550 = "856";
    static final String cnst1551 = "Honduras";
    static final String cnst1552 = "504";
    static final String cnst1553 = "Italy";
    static final String cnst1554 = "39";
    static final String cnst1555 = "Benin";
    static final String cnst1556 = "229";
    static final String cnst1557 = "Libya";
    static final String cnst1558 = "218";
    static final String cnst1559 = "Yemen";
    static final String cnst1560 = "967";
    static final String cnst1561 = "Nicaragua";
    static final String cnst1562 = "505";
    static final String cnst1563 = "St. Helena";
    static final String cnst1564 = "290";
    static final String cnst1565 = "Oman";
    static final String cnst1566 = "968";
    static final String cnst1567 = "Turkey";
    static final String cnst1568 = "90";
    static final String cnst1569 = "Costa Rica";
    static final String cnst1570 = "506";
    static final String cnst1571 = "Eritrea";
    static final String cnst1572 = "291";
    static final String cnst1573 = "India";
    static final String cnst1574 = "91";
    static final String cnst1575 = "Panama";
    static final String cnst1576 = "507";
    static final String cnst1577 = "Ukraine";
    static final String cnst1578 = "380";
    static final String cnst1579 = "Pakistan";
    static final String cnst1580 = "92";
    static final String cnst1581 = "Japan";
    static final String cnst1582 = "81";
    static final String cnst1583 = "Guadeloupe";
    static final String cnst1584 = "590";
    static final String cnst1585 = "St Pierre et Miquelon";
    static final String cnst1586 = "508";
    static final String cnst1587 = "Serbia";
    static final String cnst1588 = "381";
    static final String cnst1589 = "Lithuania";
    static final String cnst1590 = "370";
    static final String cnst1591 = "Zambia";
    static final String cnst1592 = "260";
    static final String cnst1593 = "Afghanistan";
    static final String cnst1594 = "93";
    static final String cnst1595 = "South Korea";
    static final String cnst1596 = "82";
    static final String cnst1597 = "Tokelau";
    static final String cnst1598 = "690";
    static final String cnst1599 = "Malaysia";
    static final String cnst1600 = "Bolivia";
    static final String cnst1601 = "591";
    static final String cnst1602 = "Haiti";
    static final String cnst1603 = "509";
    static final String cnst1604 = "Latvia";
    static final String cnst1605 = "371";
    static final String cnst1606 = "Madagascar";
    static final String cnst1607 = "261";
    static final String cnst1608 = "Rwanda";
    static final String cnst1609 = "250";
    static final String cnst1610 = "Sri Lanka";
    static final String cnst1611 = "94";
    static final String cnst1612 = "Micronesia";
    static final String cnst1613 = "691";
    static final String cnst1614 = "Palau";
    static final String cnst1615 = "680";
    static final String cnst1616 = "Australia";
    static final String cnst1617 = "Guyana";
    static final String cnst1618 = "592";
    static final String cnst1619 = "Estonia";
    static final String cnst1620 = "372";
    static final String cnst1621 = "Gibraltar";
    static final String cnst1622 = "350";
    static final String cnst1623 = "Reunion Island";
    static final String cnst1624 = "262";
    static final String cnst1625 = "Ethiopia";
    static final String cnst1626 = "251";
    static final String cnst1627 = "Equatorial Guinea";
    static final String cnst1628 = "240";
    static final String cnst1629 = "Myanmar";
    static final String cnst1630 = "95";
    static final String cnst1631 = "Vietnam";
    static final String cnst1632 = "84";
    static final String cnst1633 = "Marshall Islands";
    static final String cnst1634 = "692";
    static final String cnst1635 = "Wallis & Futuna Islands";
    static final String cnst1636 = "681";
    static final String cnst1637 = "East Timor";
    static final String cnst1638 = "670";
    static final String cnst1639 = "Indonesia";
    static final String cnst1640 = "Ecuador";
    static final String cnst1641 = "593";
    static final String cnst1642 = "Peru";
    static final String cnst1643 = "51";
    static final String cnst1644 = "Romania";
    static final String cnst1645 = "40";
    static final String cnst1646 = "Moldova";
    static final String cnst1647 = "373";
    static final String cnst1648 = "Portugal";
    static final String cnst1649 = "351";
    static final String cnst1650 = "Zimbabwe";
    static final String cnst1651 = "263";
    static final String cnst1652 = "Somalia";
    static final String cnst1653 = "252";
    static final String cnst1654 = "Gabon";
    static final String cnst1655 = "241";
    static final String cnst1656 = "Mauritius";
    static final String cnst1657 = "230";
    static final String cnst1658 = "Bangladesh";
    static final String cnst1659 = "880";
    static final String cnst1660 = "Cook Islands";
    static final String cnst1661 = "682";
    static final String cnst1662 = "Philippines";
    static final String cnst1663 = "French Guyana";
    static final String cnst1664 = "594";
    static final String cnst1665 = "Mexico";
    static final String cnst1666 = "52";
    static final String cnst1667 = "Switzerland";
    static final String cnst1668 = "41";
    static final String cnst1669 = "Croatia";
    static final String cnst1670 = "385";
    static final String cnst1671 = "Armenia";
    static final String cnst1672 = "374";
    static final String cnst1673 = "Luxembourg";
    static final String cnst1674 = "352";
    static final String cnst1675 = "Greece";
    static final String cnst1676 = "Aruba";
    static final String cnst1677 = "297";
    static final String cnst1678 = "Namibia";
    static final String cnst1679 = "264";
    static final String cnst1680 = "Djibouti";
    static final String cnst1681 = "253";
    static final String cnst1682 = "Congo";
    static final String cnst1683 = "242";
    static final String cnst1684 = "Liberia";
    static final String cnst1685 = "231";
    static final String cnst1686 = "Gambia";
    static final String cnst1687 = "220";
    static final String cnst1688 = "Iridium";
    static final String cnst1689 = "881";
    static final String cnst1690 = "INMARSAT - 870";
    static final String cnst1691 = "China";
    static final String cnst1692 = "86";
    static final String cnst1693 = "Niue";
    static final String cnst1694 = "683";
    static final String cnst1695 = "Antarctica";
    static final String cnst1696 = "672";
    static final String cnst1697 = "New Zealand";
    static final String cnst1698 = "Paraguay";
    static final String cnst1699 = "595";
    static final String cnst1700 = "Cuba";
    static final String cnst1701 = "53";
    static final String cnst1702 = "Slovenia";
    static final String cnst1703 = "386";
    static final String cnst1704 = "Belarus";
    static final String cnst1705 = "375";
    static final String cnst1706 = "Ireland";
    static final String cnst1707 = "353";
    static final String cnst1708 = "Netherlands";
    static final String cnst1709 = "Faroe Islands";
    static final String cnst1710 = "298";
    static final String cnst1711 = "Malawi";
    static final String cnst1712 = "265";
    static final String cnst1713 = "Kenya";
    static final String cnst1714 = "254";
    static final String cnst1715 = "Democratic Republic of the Congo";
    static final String cnst1716 = "243";
    static final String cnst1717 = "Sierra Leone";
    static final String cnst1718 = "232";
    static final String cnst1719 = "Senegal";
    static final String cnst1720 = "221";
    static final String cnst1721 = "Egypt";
    static final String cnst1722 = "Puerto Rico";
    static final String cnst1723 = "1";
    static final String cnst1724 = "011";
    static final String cnst1725 = "939";
    static final String cnst1726 = "787";
    static final String cnst1727 = "Canada";
    static final String cnst1728 = "204";
    static final String cnst1729 = "289";
    static final String cnst1730 = "306";
    static final String cnst1731 = "403";
    static final String cnst1732 = "416";
    static final String cnst1733 = "418";
    static final String cnst1734 = "438";
    static final String cnst1735 = "450";
    static final String cnst1736 = "514";
    static final String cnst1737 = "519";
    static final String cnst1738 = "581";
    static final String cnst1739 = "587";
    static final String cnst1740 = "604";
    static final String cnst1741 = "613";
    static final String cnst1742 = "647";
    static final String cnst1743 = "705";
    static final String cnst1744 = "709";
    static final String cnst1745 = "778";
    static final String cnst1746 = "780";
    static final String cnst1747 = "807";
    static final String cnst1748 = "819";
    static final String cnst1749 = "867";
    static final String cnst1750 = "902";
    static final String cnst1751 = "905";
    static final String cnst1752 = "Jamaica";
    static final String cnst1753 = "876";
    static final String cnst1754 = "St. Kitts/Nevis";
    static final String cnst1755 = "869";
    static final String cnst1756 = "Trinidad and Tobago";
    static final String cnst1757 = "868";
    static final String cnst1758 = "Dominican Republic";
    static final String cnst1759 = "809";
    static final String cnst1760 = "St. Vincent";
    static final String cnst1761 = "784";
    static final String cnst1762 = "Dominica";
    static final String cnst1763 = "767";
    static final String cnst1764 = "St. Lucia";
    static final String cnst1765 = "758";
    static final String cnst1766 = "American Samoa";
    static final String cnst1767 = "684";
    static final String cnst1768 = "Guam";
    static final String cnst1769 = "671";
    static final String cnst1770 = "Northern Mariana Islands";
    static final String cnst1771 = "Montserrat";
    static final String cnst1772 = "664";
    static final String cnst1773 = "Turks and Caicos Islands";
    static final String cnst1774 = "649";
    static final String cnst1775 = "Grenada and Carriacuou";
    static final String cnst1776 = "473";
    static final String cnst1777 = "Bermuda";
    static final String cnst1778 = "441";
    static final String cnst1779 = "Cayman Islands";
    static final String cnst1780 = "345";
    static final String cnst1781 = "U.S. Virgin Islands";
    static final String cnst1782 = "340";
    static final String cnst1783 = "British Virgin Islands";
    static final String cnst1784 = "284";
    static final String cnst1785 = "Antigua and Barbuda";
    static final String cnst1786 = "Anguilla";
    static final String cnst1787 = "Barbados";
    static final String cnst1788 = "Bahamas";
    static final String cnst1789 = "United States";
    static final String cnst1790 = "*67";
    static final String cnst1791 = "*69";
    static final String cnst1792 = "*86";
    static final String cnst1793 = "*37";
    static final String cnst1794 = "911";
    static final String cnst1795 = "112";
    static final String cnst1796 = "999";
    static final String cnst1798 = "+%C (%A) %P";
    static final String cnst1799 = "%1n%2n%3n-%4n%5n%6n%7n";
    static final String cnst1800 = "(%A) %P";
    static final String cnst1801 = "Tajikistan";
    static final String cnst1802 = "992";
    static final String cnst1803 = "Iran";
    static final String cnst1804 = "98";
    static final String cnst1805 = "Palestinian Settlements";
    static final String cnst1806 = "970";
    static final String cnst1807 = "International Networks";
    static final String cnst1808 = "882";
    static final String cnst1809 = "INMARSAT - 871";
    static final String cnst1810 = "Brunei";
    static final String cnst1811 = "673";
    static final String cnst1812 = "Singapore";
    static final String cnst1813 = "Martinique";
    static final String cnst1814 = "596";
    static final String cnst1815 = "Argentina";
    static final String cnst1816 = "54";
    static final String cnst1817 = "Austria";
    static final String cnst1818 = "43";
    static final String cnst1819 = "Czech Republic";
    static final String cnst1820 = "420";
    static final String cnst1821 = "Bosnia and Herzegovina";
    static final String cnst1822 = "387";
    static final String cnst1823 = "Andorra";
    static final String cnst1824 = "376";
    static final String cnst1825 = "Iceland";
    static final String cnst1826 = "354";
    static final String cnst1827 = "Belgium";
    static final String cnst1828 = "Greenland";
    static final String cnst1829 = "299";
    static final String cnst1830 = "Lesotho";
    static final String cnst1831 = "266";
    static final String cnst1832 = "Tanzania";
    static final String cnst1833 = "255";
    static final String cnst1834 = "Angola";
    static final String cnst1835 = "244";
    static final String cnst1836 = "Ghana";
    static final String cnst1837 = "233";
    static final String cnst1838 = "Mauritania";
    static final String cnst1839 = "222";
}

