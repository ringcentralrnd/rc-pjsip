/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser.impl;

import java.util.Vector;

class Formatting {
        Format nationalFormat;
        Format internationalFormat;
        Format localFormat;
        Vector localLengthFormats;
}
