/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser.impl;

import java.util.Vector;

class RootNode extends Node {

        private Vector countries = new Vector();

        public RootNode() {
        }

        protected void onCountryAdded(Country lpCountry) {
                if (lpCountry != null)
                        countries.addElement(lpCountry);
        }

        public int getCountriesCount() {
                return countries.size();
        }

        public Country getCountryAt(int iPos) {
                if ((iPos >= 0) && (iPos < getCountriesCount())) {
                        return (Country)countries.elementAt(iPos);
                }
                return null;
        }

        public int findCountry(Country lpToFind) {
                int iCount = getCountriesCount();

                for (int i = 0; i < iCount; i++) {
                        Country lpCur = getCountryAt(i);
                        if ((lpCur.getCode().equals(lpToFind.getCode()))
                                        && (lpCur.getName().equals(lpToFind.getName()))) {
                                return i;
                        }
                }
                return -1;
        }

        public int findCountry(String szName) {
                if (szName == null || (szName.length() == 0)) {
                        return -1;
                }
                int iCount = getCountriesCount();
                for (int i = 0; i < iCount; i++) {
                        Country lpCur = getCountryAt(i);
                        if (lpCur.getName().equals(szName)) {
                                return i;
                        }
                }
                return -1;
        }
}
