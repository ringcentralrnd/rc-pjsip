/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser.impl;

public class SpecialPhone {

    String code;
    SpecialPhoneType type;

    public String getCode() {
        return code;
    }

    public SpecialPhoneType getType() {
        return type;
    }
    
    static public class SpecialPhoneType
    {
        static public final int enPDSPTUnknown = 0;         //Unspecified
        static public final int enPDSPTEmergency = 1;       //Emergency
        static public final int enPDSPTDirectory = 2;       //Directory
        static public final int enPDSPTUpdateEmAddr = 3;    //Update emergency address (933 in US)
        
        private int m_code;
        
        SpecialPhoneType(int code){
            m_code = code;
        }
        
        public static SpecialPhoneType getByCode(int code)
        {
            switch(code)
            {
                case enPDSPTUnknown:
                case enPDSPTEmergency:
                case enPDSPTDirectory:
                case enPDSPTUpdateEmAddr:
                    return new SpecialPhoneType(code);
                default:
                    return null;
            }
        }
    }
}

