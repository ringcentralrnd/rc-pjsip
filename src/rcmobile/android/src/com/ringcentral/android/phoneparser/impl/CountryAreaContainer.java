/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser.impl;

class CountryAreaContainer {
        public Country country;
        public Area area;
        public int areaOffset;
}
