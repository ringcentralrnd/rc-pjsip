/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.phoneparser;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;

import java.util.Comparator;



/**
 * Phone number parse output.
 */
public class PhoneNumber
{
        
        private static final String NUMBER_SYMBOLS_SET = "+0123456789#*";
        private static final String LOG_TAG = "PhNum:";
        
     //   private static boolean ENABLE_DEBUG = true && Logger.DEBUG_ENABLED;
        
        public static int counter = 0;
        
    /**
     * Defines if number is valid (not empty).
     */
    public boolean isValid = false;
    
    /**
     * Defines if number is full valid (country code and number are not empty).
     */
    public boolean isFullValid;
    
    /**
     * Defines if this is a valid USA number.
     */
    public boolean isUSAValid;
    
    /**
     * Defines if this is a valid USA number (isUSAValid, isFullValid, and
     * country code is 1
     */
    public boolean isUSANumberAndValid;
    
    /**
     * Defines if country code, area code and number are empty.
     */
    public boolean isEmpty = true;
    
    /**
     * Defines if this is a special number.
     */
    public boolean isSpecialNumber;
    
    /**
     * Defines if it is short number (country and area codes are empty and
     * number length <= 5).
     */
    public boolean isShortNumber;
    
    /**
     * Defines if it is USA 911.
     */
    public boolean is911;

    /**
     * Defines if it is UK 112.
     */
    public boolean is112;

    /**
     * Defines if it is UK 999.
     */
    public boolean is999;

    /**
     * Defines if this is USA toll free number.
     */
    public boolean isTollFree;
    
    /**
     * Defines if country USA or Canada.
     */
    public boolean isUSAOrCanada;
     
    /**
     * Keeps country name.
     */
    public String countryName;
    
    
    public PhoneNumber()
    {}
    
    
    public PhoneNumber(PhoneNumber base)
    {
    	if (base==null)
    		return;
        isValid         = base.isValid;
        isFullValid     = base.isFullValid;
        isUSAValid      = base.isUSAValid;
        isUSANumberAndValid = base.isUSANumberAndValid;
        isEmpty         = base.isEmpty;
        isSpecialNumber = base.isSpecialNumber;
        isShortNumber   = base.isShortNumber;
        is911           = base.is911;
        is112           = base.is112;
        is999           = base.is999;
        isTollFree      = base.isTollFree;
        isUSAOrCanada   = base.isUSAOrCanada;
        countryName     = base.countryName      == null ? null : new String(base.countryName);
        original        = base.original         == null ? null : new String(base.original);
        canonical       = base.canonical        == null ? null : new String(base.canonical);
        localCanonical  = base.localCanonical   == null ? null : new String(base.localCanonical);
        countryCode     = base.countryCode      == null ? null : new String(base.countryCode);
        areaCode        = base.areaCode         == null ? null : new String(base.areaCode);
        number          = base.number           == null ? null : new String(base.number);
        subAddress      = base.subAddress       == null ? null : new String(base.subAddress);
        dialable        = base.dialable         == null ? null : new String(base.dialable);
        e164            = base.e164             == null ? null : new String(base.e164);
        formattedNumber = base.formattedNumber  == null ? null : new String(base.formattedNumber);
        numRaw          = base.numRaw           == null ? null : new String(base.numRaw);
        fullNumber      = base.fullNumber       == null ? null : new String(base.fullNumber);
    }

    
    /**
     * Defines if this is international call.
     * 
     * @param dest destination phone number
     */
    public boolean isInternationalCallTo(PhoneNumber dest) {
        if (isUSAOrCanada && dest.isUSAOrCanada)
            return false;

        if (countryName.equals(dest.countryName))
            return false;

        return true;
    }
    
    /**
     * Use device for dialing (special numbers).
     */
    public boolean useDeviceForDial() {
        return (is911 || is112 || is999);
    }
    
    /**
     * Keeps original number.
     */
        public String original;
        
        /**
     * Canonical form number with country and area code formatted according to
     * national standard.
     */
        public String canonical;
        
        /**
     * <code>localCanonical</code></b> number without country code formatted
     * according to national standard
     */
        public String localCanonical;
        
        /**
         * Keeps country code.
         */
        public String countryCode;
        
        /**
         * Keeps area code.
         */
        public String areaCode;
        
        /**
     * Keeps number: number inside location i.e. without country and area codes
     */
        public String number;
        
        /**
         * Keeps extension number
         */
        public String subAddress;
        
        /**
         * Number to dial from a stationary phone
         */
        public String dialable;
        
        /**
         * Keeps E164
         */
        public String e164;

        /**
         * Formatted number.
         */
        public String formattedNumber;
        
        /**
         * countryCode + areaCode + number
         */
        public String numRaw;
        
        /**
         * "+" + countryCode + areaCode + number + "x" +
         */
        public String fullNumber;
        
        /**
         * compares numRow to determine if two numbers are equal
         * @param phoneNumber
         *          compare with 
         * @return
         *          true if string representation of numRow is equal to current, false - othrewise. If any numRow == null returns false
         */
        public boolean equals(PhoneNumber phoneNumber)
        {
            if (phoneNumber == null)
                return false;
            
            if (numRaw==null && phoneNumber.numRaw==null)
                return true;
            
            if ((phoneNumber.numRaw == null && numRaw != null)
                    || (phoneNumber.numRaw != null && numRaw == null))
                return false;
            
            return numRaw.compareTo(phoneNumber.numRaw)==0;
        }

        /**
         * To string.
         */
        public String toString() {
                final StringBuffer sb = new StringBuffer();
                sb.append("PhoneNumber[");
                sb.append("original='").append(original==null?"null":original).append('\'');
                sb.append(", canonical='").append(canonical==null?"null":canonical).append('\'');
                sb.append(", localCanonical='").append(localCanonical==null?"null":localCanonical).append('\'');
                sb.append(", countryCode='").append(countryCode==null?"null":countryCode).append('\'');
                sb.append(", areaCode='").append(areaCode==null?"null":areaCode).append('\'');
                sb.append(", number='").append(number==null?"null":number).append('\'');
                sb.append(", subAddress='").append(subAddress==null?"null":subAddress).append('\'');
                sb.append(", dialable='").append(dialable==null?"null":dialable).append('\'');
                sb.append(", countryName='").append(countryName==null?"null":countryName).append('\'');
                sb.append(", e164='").append(e164==null?"null":e164).append('\'');
                sb.append(", formattedNumber='").append(formattedNumber==null?"null":formattedNumber).append('\'');
                sb.append(", numRow='").append(numRaw==null?"null":numRaw).append('\'');
                sb.append(", fullNumber='").append(fullNumber==null?"null":fullNumber).append('\'');
                sb.append(']');
                return sb.toString();
        }
        
        
        
        
        /**
         * defines is this string a number.
         *
         * @param string <description>
         * @return <description>
         */
        static public boolean isPhoneNumber(String string) 
        {
            if (string == null || string.equals("")) 
                return false;
              
            String nonFormattedNumber = getDigitsFromPhone(string);
            if(nonFormattedNumber == null || nonFormattedNumber.equals(""))
                return false;
                
            for (int i = 0; i < nonFormattedNumber.length(); i++) 
            {
                if (nonFormattedNumber.charAt(i) >= '0' && nonFormattedNumber.charAt(i) <= '9') {

                } else 
                    return false;
            }
            return true;
        }
        
        static public boolean arePhoneNumbersEqual(String number1, String number2)
        {
            String nonFormattedNumber1 = getDigitsFromPhone(number1);
            String nonFormattedNumber2 = getDigitsFromPhone(number2);
            return nonFormattedNumber1.equals(nonFormattedNumber2);
        }
        
        static private String getDigitsFromPhone(String phone)
        {
            String result = "";
            for (int i = 0; i < phone.length(); i++) 
            {
                if (phone.charAt(i) != '(' && phone.charAt(i) != ')' && phone.charAt(i) != '#' && phone.charAt(i) != '+' && phone.charAt(i) != (' ') && phone.charAt(i) != ('-')) 
                {
                    result += "" + phone.charAt(i);
                }
            }
            return result;
        }

        static public Comparator getComparator() {
            return new PhoneNumberComparator();
        }
        

        /**
         * comparator sorting by the local canonical number
         */
        static private class PhoneNumberComparator implements Comparator {
            private static final String PHNUM_COMPARATOR__LOG_TAG = "PNComp:";
            private static final String TAG = "[RC] PhoneNumberComparator (inner class)";

            public int compare(Object o1, Object o2) {
                try {
                    if (o1==null || o2==null)
                        throw new NullPointerException(PHNUM_COMPARATOR__LOG_TAG + "compare:parameter is null");
                    
                    PhoneNumber pn1 = (PhoneNumber)o1;
                    PhoneNumber pn2 = (PhoneNumber)o2;
                    
                    int result = pn1.localCanonical.compareTo(pn2.localCanonical);
                    return result;
                } catch (Exception e) {
                    if (LogSettings.ENGINEERING) {
                        EngLog.w(TAG, "compare(): " + e.getMessage());
                    }
                    throw new RuntimeException(e.toString());
                }
            }
        }

}




