package com.ringcentral.android.statistics;

import getjar.android.sdk.Utility;
import getjar.android.sdk.Utility.EventTypes;

import org.apache.http.HttpStatus;

import android.content.Context;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;

public class GetJarHandler {
	private static final String TAG = "[RC] GetJarHandler";
	

	public static void configureGetJar(final Context context){
		if(!BUILD.GETJAR.ENABLE)
			return;
    	try{
    		if(context!=null){    			
    			if(Utility.hasOpenEventBeenPushed(context)) { 
    				if(LogSettings.MARKET){
    					MktLog.w(TAG, "[GETJAR] OPEN event already sent, returning");
    				}
    				return;
    			}
    			final String referrer = Utility.getReferrer(context);
    			if((referrer == null) || (referrer.length() <= 0)) {
    				if(LogSettings.MARKET){
    					MktLog.w(TAG, "[GETJAR] 'referrer' value not yet persisted, returning");
    				}
    				return;
    			}
    			new Thread( 
    					new Runnable() { 
    						public void run() { 
    							try {
    								synchronized(GetJarHandler.class){
    									if(Utility.hasOpenEventBeenPushed(context)) { 
    										return;
    									}
    									int httpResponseCode = Utility.logEvent(context, referrer, EventTypes.OPEN);
    									if(httpResponseCode == HttpStatus.SC_OK) {
    										Utility.setOpenEventAsPushed(context);
    									}
    								}
    							} catch(Throwable t) {
    								if(LogSettings.MARKET){
    									MktLog.e(TAG, Utility.getExceptionText(t));
    								}
    							}
    						} 
    					}, "configureGetJar"
    			).start();
    		}else{
    			if(LogSettings.ENGINEERING){
    				EngLog.i(TAG, "[GetJar] No Application context");
    			}
    		}
    	} catch (Throwable t){
    		if(LogSettings.MARKET){
    			MktLog.e(TAG, Utility.getExceptionText(t));
			}
    	}
    }


}
