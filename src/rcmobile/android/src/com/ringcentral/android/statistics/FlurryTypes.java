package com.ringcentral.android.statistics;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.os.SystemClock;

import com.flurry.android.FlurryAgent;
import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;

public class FlurryTypes {
	private static final String TAG = "[RC] FlurryTypes";
	private static final String LOG_MS = " ms";
	private static final String LOG_ON_EVENT_TAKES_TOO_MUCH_TIME = "onEvent takes too much time: ";
	private static final int LOGGING_MAX_TIME = 1000;

	//Screen open events
	public static final String EVENT_SCREEN_OPEN = "Screen open";
	public static final String SCREEN = "Screen";
	public static final String SCR_KEYPAD = "Keypad";		
	public static final String SCR_CALL_LOG_ALL = "CallLog-All";
	public static final String SCR_CALL_LOG_ALL_CONTEXT = "CallLog-All-ContextMenu";
	public static final String SCR_CALL_LOG_MISSED = "CallLog-Missed";
	public static final String SCR_CALL_LOG_MISSED_CONTEXT = "CallLog-Missed-ContextMenu";
	public static final String SCR_CALL_LOG_DETAILS = "CallLog-Details";		
	public static final String SCR_EVENT_DETAILS = "Event-Details";		
	public static final String SCR_CONTACTS_PERSONAL = "Contacts-Personal";
	public static final String SCR_CONTACTS_PERSONAL_CONTEXT = "Contacts-Personal-ContextMenu";
	public static final String SCR_CONTACTS_PERSONAL_OPTIONS = "Contacts-Personal-OptionsMenu";
	public static final String SCR_CONTACTS_EXTENSION = "Contacts-Extension";
	public static final String SCR_CONTACTS_EXTENSION_CONTEXT = "Contacts-Extension-ContextMenu";
	public static final String SCR_CONTACTS_EXTENSION_OPTIONS = "Contacts-Extension-OptionsMenu";
	public static final String SCR_CONTACTS_EXTENSION_DETAILS = "Contacts-Extension-Details";	
	public static final String SCR_FAVORITES_PERSONAL = "Favorites-Personal";
	public static final String SCR_FAVORITES_PERSONAL_CONTEXT = "Favorites-Personal-ContextMenu";
	public static final String SCR_FAVORITES_PERSONAL_OPTIONS = "Favorites-Personal-OptionsMenu";
	public static final String SCR_FAVORITES_EXTENSION = "Favorites-Company";
	public static final String SCR_FAVORITES_EXTENSION_CONTEXT = "Favorites-Company-ContextMenu";
	public static final String SCR_FAVORITES_EXTENSION_OPTIONS = "Favorites-Company-OptionsMenu";
	public static final String SCR_FAVORITES_EXTENSION_DETAILS = "Favorites-Company-Details";	
	public static final String SCR_MESSAGES_RECENT = "Messages-Recent";
	public static final String SCR_MESSAGES_RECENT_CONTEXT = "Messages-Recent-ContextMenu";
	public static final String SCR_MESSAGES_DELETED = "Messages-Deleted";
	public static final String SCR_MESSAGES_DELETED_CONTEXT = "Messages-Deleted-ContextMenu";
	public static final String SCR_MESSAGES_VOICEMAIL = "Messages-Details";
	public static final String SCR_SETTING = "Settings";
	
	public static final String EVENT_APP_DESCRIPTION = "App Description Visited";	
	public static final String EVENT_ABOUT_VISITED = "About Visited";
	public static final String EVENT_FEEDBACK_EMAIL = "Feedback Email";
	public static final String EVENT_RATE_APP = "Feedback Rate App";	
	public static final String EVENT_TELL_FRIEND = "Tell a friend";
		
	public static final String EVENT_SORT_BY = "Messages Sort By";
	public static final String SORT_BY_DATE = "Date";
	public static final String SORT_BY_SENDER = "Sender";
	
	public static final String EVENT_CALL_FROM = "Call from";
	
	public static final String EVENT_FAVORITES_OPEN = "Favorites. User Activate View";	
	public static final String EVENT_ADD_FAVORITES = "Add to Favorites";	
	public static final String EVENT_REMOVE_FAVORITES = "Remove from Favorites";
	
	public static final String EVENT_CREATE_NEW_CONTACT = "Create New Contact";	
	public static final String EVENT_ADD_EXISTING_CONTACT = "Add to Existing Contact";
	
	public static final String EVENT_DND_CHANGED = "DND Settings";	
	public static final String DND_SETTINGS_ON = "Settings ON";
	public static final String DND_SETTINGS_OFF = "Settings OFF";
	public static final String DND_OPTIONS_ON = "Options ON";
	public static final String DND_OPTIONS_OFF = "Options OFF";
	
	public static final String EVENT_MARK_UNOPENED = "Mark unopened";
	public static final String MARK_UNOPEN_CONTEXT = "Context-menu";
	public static final String MARK_UNOPEN_VOICEMAIL = "Voicemail";
	
	public static final String EVENT_CALLERID_CHANGED = "CallerID Changed";	
	public static final String EVENT_CALLERID_SETTINGS_OPENED = "CallerID Settings Opened";
	
	public static final String EVENT_SPEAKER = "Speaker";
	public static final String SPEAKER_ON = "On";
	public static final String SPEAKER_OFF = "Off";
	
	public static final String EVENT_DELETE_MSG = "Delete message";	
	
	public static final String EVENT_VIEW_MESSAGE = "View Message Info";
	
	public static final String EVENT_LOG_OUT = "Log out";			
	
	public static final String EVENT_RINGOUT_OPEN = "Ringout Mode Settings Opened";	
	
	public static final String EVENT_RINGOUT_CHANGED = "RingOut Settings. Phone Changed";
	public static final String RINGOUT_MY_ANDR = "My Android";
	public static final String RINGOUT_OTHER_PHONE = "Other phone";	
	
	public static final String EVENT_RINGOUT_CALL = "User made RingOut call";
	public static final String EVENT_RINGOUT_CALL_TYPE = "Type";
	public static final String RINGOUT_CLASSIC = "Classic"; // Classic - 2 leg - old
	public static final String RINGOUT_DIRECT = "Direct";   // New - Direct - 1 leg
	
	
	public static final String EVENT_CONNECTION_CHANGED = "RingOut Settings. Confirm Connection Changed";
	public static final String CONNECTION_ON = "ON";
	public static final String CONNECTION_OFF = "OFF";
	
	public static final String EVENT_RINGOUT_ANOTHER_PHONE_ADDED = "RingOut Settings. Custom Phone Added";
	
	public static final String EVENT_RINGOUT_ANOTHER_PHONE_CHANGED = "RingOut Settings. Custom Phone Changed";
	
	public static final String EVENT_CALL_LOG_OPEN = "Call Log.Opened";
	public static final String EVENT_CALL_LOG_DETAILS_OPEN = "Call Log.Details Opened";
	public static final String EVENT_CONTACTS_DETAILS_OPEN = "Contacts.Details Opened";
	
	public static final String EVENT_PASTE = "Paste";
	public static final String PASTE_KEYPADPHONENUMBER = "KeypadPhoneNumber";
	
	public static final String EVENT_FORWARD = "Forward";
	public static final String EVENT_FORWARD_PARAM = "Message Type";
	public static final String FORWARD_VOICEMAIL = "Voicemail";
	public static final String FORWARD_FAX = "Fax";
	public static final String FORWARD_MESSAGE = "Message";
	
	public static final String EVENT_DND_USER = "User turned ON/OFF DND for self";
	public static final String EVENT_DND_DEPT = "User turned ON/OFF DND for department calls";
	public static final String DND_PARAM = "Param";
	public static final String DND_TAKE_ALL_CALLS = "Take All Calls";
	public static final String DND_TAKE_NONE_CALLS = "Do Not Take Any Calls";		
	public static final String DND_TAKE_NO_DEPT_CALLS = "Do Not Take Department Calls";

	public static final String SETTINGS_VISITED = "Settings Visited";
	public static final String MENU_ITEM = "Menu Item";
	public static final String MAIN = "Main";
	public static final String COMPANY = "Company";
	public static final String BILLING = "Billing";
	public static final String INCOMING_CALLS = "Incoming calls";
	public static final String OUTGOING_CALLS = "Outgoing calls";
	public static final String GENERAL = "General";

	public static final String VOIP_IN_WIFI = "VoIP Incoming Wifi";
	public static final String ACTION = "Action";
	public static final String ANSWER = "Answer";
	public static final String ANSWER_AND_HOLD = "Hold + Answer";
	public static final String ANSWER_AND_HANGUP = "Hang Up + Answer";
	public static final String REJECT = "Reject";
	
	public static final String VOIP_OUT_WIFI = "VoIP Outgoing Wifi";
	public static final String ORIGIN = "Origin";
	public static final String RCM = "RCM";
	public static final String NATIVE = "Native";

	public static final String VOICEMAIL_PLAYED = "Voicemail Played";
	public static final String FAX_VIEWED = "Fax Viewed";
	public static final String VOIP_TOGGLED = "VoIP toggled ON/OFF";
	public static final String TOGGLE = "Toggle";
	public static final String ON = "on";
	public static final String OFF = "off";
	public static final String INCOMING_VOIP_TOGGLED = "Incoming VoIP ON/OFF";
	public static final String ECHO_CANCELLATION_TOGGLED = "Echo cancellation toggled";
	
	
	
	public static final void onStartSession(Context context){
		if(BUILD.FLURRY.ENABLE){
			if(LogSettings.ENGINEERING){
				EngLog.i(TAG, "onStartSession");
			}
			try{
				FlurryAgent.onStartSession(context, BUILD.FLURRY.APP_KEY);
				FlurryAgent.setLogEvents(true);
			}catch(java.lang.Throwable t){
				if(LogSettings.QA){
					QaLog.e(TAG, "Exception on Starting Session", t);
				}
			}
		}
	}
	
	public static final void onEndSession(Context context){
		if(BUILD.FLURRY.ENABLE){
			if(LogSettings.ENGINEERING){
				EngLog.i(TAG, "onEndSession");
			}
			try{
				FlurryAgent.onEndSession(context);
			}catch(java.lang.Throwable t){
				if(LogSettings.QA){
					QaLog.e(TAG, "Exception on Closing Session", t);
				}
			}
		}
	}
	
	public static final void onEvent(String eventId){
		if(BUILD.FLURRY.ENABLE){
			long currentTime = SystemClock.elapsedRealtime();
			if(LogSettings.ENGINEERING){
				EngLog.i(TAG, "onEvent : " + eventId);
			}
			try {
				FlurryAgent.onEvent(eventId);
			} catch (java.lang.Throwable t) {
				if(LogSettings.QA){
					QaLog.e(TAG, "onEvent(string)", t);
				}
			}		
			checkFlurryLoggingSpeed(currentTime);
		}
	}
	
	public static final void onEvent(String eventId, String paramValue){
		if(BUILD.FLURRY.ENABLE){
			long currentTime =SystemClock.elapsedRealtime();
			if(LogSettings.ENGINEERING){
				EngLog.i(TAG, "onEvent : " + eventId + " parameter : "+paramValue);
			}
			try {
				Map<String, String> params = new HashMap<String, String>();
				params.put(eventId, paramValue);			
				FlurryAgent.onEvent(eventId, params);
			} catch (java.lang.Throwable t) {
				if(LogSettings.QA){
					QaLog.e(TAG, "onEvent(string, string)", t);
				}
			}
			checkFlurryLoggingSpeed(currentTime);
		}
	}	
	
	public static final void onEventScreen(String eventId, String paramValue){
		if(BUILD.FLURRY.ENABLE){
			long currentTime = SystemClock.elapsedRealtime();
			if(LogSettings.ENGINEERING){
				EngLog.i(TAG, "onEventScreen : " + eventId + " parameter : "+paramValue);
			}
			try {
				Map<String, String> params = new HashMap<String, String>();
				params.put(FlurryTypes.SCREEN, paramValue);			
				FlurryAgent.onEvent(eventId, params);
			} catch (java.lang.Throwable t) {
				if(LogSettings.QA){
					QaLog.e(TAG, "onEventScreen(string, string)", t);
				}
			}
			checkFlurryLoggingSpeed(currentTime);
		}
	}
	
	public static final void onEvent(String eventId, String paramKey, String paramValue){
		if(BUILD.FLURRY.ENABLE){
			long currentTime =SystemClock.elapsedRealtime();
			if(LogSettings.ENGINEERING){
				EngLog.i(TAG, "onEventScreen : " + eventId + " parameterName : " + paramKey + " parameterValue : " + paramValue);
			}
			try {
				Map<String, String> params = new HashMap<String, String>();
				params.put(paramKey, paramValue);			
				FlurryAgent.onEvent(eventId, params);
			} catch (java.lang.Throwable t) {
				if(LogSettings.QA){
					QaLog.e(TAG, "onEvent(string, string, string)", t);
				}
			}
			checkFlurryLoggingSpeed(currentTime);
		}
	}

	private static void checkFlurryLoggingSpeed(long currentTime) {
		long duration = SystemClock.elapsedRealtime() - currentTime;
		if (duration >= LOGGING_MAX_TIME && LogSettings.MARKET){
			MktLog.w(TAG, LOG_ON_EVENT_TAKES_TOO_MUCH_TIME+duration+LOG_MS);
		}
	}	
	
}
