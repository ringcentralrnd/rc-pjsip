package com.ringcentral.android.statistics;

import android.content.Context;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.provider.RCMProviderHelper;
	
public class GAnalyticsHandler {
	
	private static final String INSTALL = "/Install";
	private static GoogleAnalyticsTracker tracker;
	private static final String TAG = "GAHdlr";
	
	public static void initTracker(Context context){
        if (BUILD.GANALYT.ENABLE) {
            // should be called on first launch only
            if (RCMProviderHelper.getDeviceIMSI(context) == null || RCMProviderHelper.getDeviceIMSI(context).equals("")) {
                if (tracker == null) {
                    try {
                        tracker = GoogleAnalyticsTracker.getInstance();
                    } catch (java.lang.Throwable ex1) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "initTracker(): GoogleAnalyticsTracker.getInstance(): exception:" + ex1.toString());
                        }
                    }
                } else {
                    if (LogSettings.MARKET) {
                        QaLog.w(TAG, "initTracker(): Tracker not null");
                    }
                    return;
                }
                
                if (tracker == null) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "initTracker(): tracker is null return");
                    }
                    return;
                }
                
                try {
                    tracker.start(BUILD.GANALYT.APP_KEY, context);
                } catch (java.lang.Throwable ex2) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "initTracker(): tracker.start: exception:" + ex2.toString());
                    }
                    return;
                }

                try {
                    tracker.trackPageView(INSTALL);
                } catch (java.lang.Throwable ex3) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "initTracker(): tracker.trackPageView: exception:" + ex3.toString());
                    }
                    return;
                }
                
                try {
                    tracker.dispatch();
                } catch (java.lang.Throwable ex4) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "initTracker(): tracker.dispatch(): exception:" + ex4.toString());
                    }
                    return;
                }
            }
        }
	}
	
	public static void stopTracker(){
        if (!BUILD.GANALYT.ENABLE) {
            try {
                if (tracker != null) {
                    tracker.stop();
                    tracker = null;
                }
            } catch (java.lang.Throwable ex) {
                if (LogSettings.MARKET) {
                    QaLog.w(TAG, "stopTracker():exception:" + ex.toString());
                }
            }
        }
	}
}
