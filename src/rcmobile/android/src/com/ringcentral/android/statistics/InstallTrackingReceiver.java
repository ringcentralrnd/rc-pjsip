package com.ringcentral.android.statistics;

import getjar.android.sdk.GetJarTrackingReceiver;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.ads.InstallReceiver;
import com.google.android.apps.analytics.AnalyticsReceiver;
import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;


/*
 * Will be handling multiple installation sources
 */
public class InstallTrackingReceiver extends BroadcastReceiver {
	private static String TAG = "[RC]InstallTrackingReceiver";

	public void onReceive(Context context, Intent intent) {		
		if(LogSettings.MARKET){
			MktLog.i(TAG, "onReceive referrer = " + intent.getStringExtra("referrer"));
		}
		if(BUILD.ADMOB.ENABLE){
			if(LogSettings.MARKET){
				MktLog.i(TAG, "AdMob agent enabled");
			}
			try {
				InstallReceiver adMobReceiver = new InstallReceiver();
				adMobReceiver.onReceive(context, intent);
			} catch(Throwable t) {
				if (LogSettings.MARKET) {
					MktLog.e(TAG, "Exception when processing adMob install tracking.", t);
				}
			}
		}
		if(BUILD.GETJAR.ENABLE){
			if(LogSettings.MARKET){
				MktLog.i(TAG, "GetJar agent enabled");
			}
			try {
				GetJarTrackingReceiver getJarReceiver = new GetJarTrackingReceiver();
				getJarReceiver.onReceive(context, intent);
			} catch (Throwable t) {
				if (LogSettings.MARKET) {
					MktLog.e(TAG, "Exception when processing getJar install tracking.", t);
				}
			}
		}
		if(BUILD.GANALYT.ENABLE){
			if(LogSettings.MARKET){
				MktLog.i(TAG, "Google Analytics agent enabled");
			}
			try {
				AnalyticsReceiver analyticsReceiver = new AnalyticsReceiver();
				analyticsReceiver.onReceive(context, intent);
			} catch (Throwable t) { 
				if (LogSettings.MARKET) {
                	MktLog.e(TAG, "Exception when processing GoogleAnalyrics install tracking.", t);
            	}
			}		
		}
	}

}
