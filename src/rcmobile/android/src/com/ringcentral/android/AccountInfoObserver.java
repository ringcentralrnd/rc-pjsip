/** 
 * Copyright (C) 2010-2011 RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.UriHelper;

public class AccountInfoObserver {
    private Context ctx;
    private static final String TAG = "[RC] AccountInfoObserver";

    public AccountInfoObserver(Context ctx) {
        this.ctx = ctx;
        registerObservers();
    }

    private void registerObservers() {
        ctx.getContentResolver().registerContentObserver(UriHelper.getUri(RCMProvider.TIER_SETTINGS), true,
                new ContentObserver(new Handler()) {

                    @Override
                    public boolean deliverSelfNotifications() {
                        return false;
                    }

                    @Override
                    public void onChange(boolean selfChange) {
                        if (LogSettings.MARKET) {
                            MktLog.d(TAG, "registerObservers() New TIER settings value.");
                        }
                        super.onChange(selfChange);
                    }

                });
    }
}
