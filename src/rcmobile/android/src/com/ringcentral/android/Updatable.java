package com.ringcentral.android;

public interface Updatable {

    public void onUpdateRequest();

}
