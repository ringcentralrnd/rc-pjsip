/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android;

import java.lang.reflect.Field;
import java.util.List;

import android.app.ActivityManager;
import android.app.Application;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.SystemClock;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.LoggingManager;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.logging.LoggingManager.LOGGING_TYPE;
import com.rcbase.android.sip.client.SIProxy;
import com.rcbase.android.sip.service.IServicePJSIP;
import com.ringcentral.android.api.RequestInfoStorage;
import com.ringcentral.android.api.network.HttpClientFactory;
import com.ringcentral.android.messages.MessagesHandler;
import com.ringcentral.android.messages.MessagesNotification;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.MailboxCurrentTable;
import com.ringcentral.android.statistics.GetJarHandler;
import com.ringcentral.android.utils.FileUtils;
import com.ringcentral.android.utils.LoginInfo;
import com.ringcentral.android.utils.NetworkUtils;
import com.ringcentral.android.utils.PhoneUtils;
import com.ringcentral.android.utils.NetworkUtils.NetworkState;

public class RingCentralApp extends Application {
    /**
     * Defines logging tag.
     */
	private static final String TAG = "[RC]RingCentralApp";

	/**
	 * Keeps application execution context.
	 */
	private static Context sApplicationContext = null;

	private long mLatestShutdownToConnectingAttempt = 0;
	private static final long SHUTDOWN_TO_CONNECTING_TIMEOUT = 1000 * 60 * 5;
	private static long sShutdownsDetected = 0;
	private static long sShutdownToConnectingAttempts = 0;
	
    /**
     * Defines if SIP service is available.
     * 
     * @return service availability
     */
    public boolean isSipServiceOnForVoipCalls() {
        int state = SIProxy.getInstance().getServiceConnectionState();
        if (state == SIProxy.SHUTDOWN) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "isSipServiceOnForVoipCalls:SHUTDOWN");
            }
            sShutdownsDetected++;
            if ((SystemClock.elapsedRealtime() - mLatestShutdownToConnectingAttempt) > SHUTDOWN_TO_CONNECTING_TIMEOUT) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "isSipServiceOnForVoipCalls:SHUTDOWN_TO_CONNECT_ATTEMPT");
                }
                SIProxy.ensureBindingStarted(sProcessType);
                sShutdownToConnectingAttempts++;
            }
        } else if (state == SIProxy.CONNECTED) {
            return true;
        }
    	return false;
    }
    
//    /**
//     * Makes a VoIP call
//     */
//    public void makeCall(final String number, final String name) {
//        try {
//            SIProxy.getInstance().getService().call_make_call(0, number, name);
//        } catch (java.lang.Throwable th) {
//            if (LogSettings.MARKET) {
//                MktLog.e(TAG, "makeCall:" + th.toString());
//            }
//        }
//    }
    
    public void makeCallInit( final String number, final String name ){
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "makeCallInit(number = " + number + ", name = " + name + ")");
        }
        
        try {
            SIProxy.getInstance().getService().call_make_call_init(0, number, name);
        } catch (java.lang.Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "makeCallInit:" + th.toString());
            }
        }
    }
    
    public void makeCallComplete( final long rcCallId ){
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "makeCallComplete(rcCallId = " + rcCallId + ")");
        }
        
        try {
            SIProxy.getInstance().getService().call_make_call_complete(rcCallId);
        } catch (java.lang.Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "makeCallComplete:" + th.toString());
            }
        }
    }
    
    public void callAnswer(final long rcCallId, final long delayMillys) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "callAnswer(rcCallId = " + rcCallId + ", delayMillys = " + delayMillys + ")");
        }
        
        try {
            SIProxy.getInstance().getService().call_answer(rcCallId, delayMillys);
        } catch (java.lang.Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "callAnswer:" + th.toString());
            }
        }
    }
    
    public void callAnswerAndHold(final long rcCallId, final long delayMillys) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "callAnswerAndHold(rcCallId = " + rcCallId + ", delayMillys = " + delayMillys + ")");
        }
        try {
            SIProxy.getInstance().getService().call_answer_and_hold(rcCallId, delayMillys);
        } catch (java.lang.Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "callAnswerAndHold:" + th.toString());
            }
        }
    }
    
    public void callAnswerAndHangup(final long rcCallId, final long delayMillys) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "callAnswerAndHangup(rcCallId = " + rcCallId + ", delayMillys = " + delayMillys + ")");
        }

        try {
            SIProxy.getInstance().getService().call_answer_and_hangup(rcCallId, delayMillys);
        } catch (java.lang.Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "callAnswerAndHold:" + th.toString());
            }
        }
    }
    
    /**
     * Get active calls number
     */
    public int getSipActiveCallsCount() {

        int sipCallCount = 0;
        try {
            if (SIProxy.getInstance().getService().isStartedSipStack()) {
                sipCallCount = SIProxy.getInstance().getService().call_get_count();
            }
        } catch (java.lang.Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "getSipActiveCallsCount:" + th.toString());
            }
        }

        return sipCallCount;
    }
    
    /**
     * Check SIP stack state.
     * Returned value means if SIP stack is ready to make call. 
     */
    public boolean canMakeVoipOutboundCall() {
        IServicePJSIP m_service = SIProxy.getInstance().getService();
        if (m_service == null) {

            if (LogSettings.MARKET) {
                MktLog.i(TAG, "checkSipState Failed. No connection with SIP stack");
            }
            return false;
        }

        boolean result = true;

        try {
            result = m_service.canMakeVoIPOutboundCall();
        } catch (Throwable e) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "checkSipState Failed", e);
            }
            result = false;
        }

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "checkSipState Result:" + result);
        }

        return result;
    }

    /**
     * Define that process has not been identified.
     */
    public final static int UNKNOWN_PROCESS = 0;
    
    /**
     * Defines that process is the main application
     */
    public final static int APPLICATION_PROCESS = 1;
    
    /**
     * Defines that process executes SIP service.
     */
    public final static int SIP_SERVICE_PROCESS = 2;
    
    /**
     * Keeps process type.
     */
    private static int sProcessType = UNKNOWN_PROCESS;

    /**
     * Returns the process type {@link #UNKNOWN_PROCESS}, {@link #APPLICATION_PROCESS} or {@link #SIP_SERVICE_PROCESS}
     * 
     * @return the process type
     */
    public final static int getProcessType() {
        return sProcessType;
    }
    
    /**
     * Returns if the process is application
     * 
     * @return if the process is application
     */
    public final static boolean isApplicationProcess() {
        return sProcessType == APPLICATION_PROCESS;
    }

    /**
     * Returns if the process is ServiceSIP
     * 
     * @return if the process is ServiceSIP
     */
    public final static boolean isSIPProcess() {
        return sProcessType == SIP_SERVICE_PROCESS;
    }
    
    /**
     * Returns process name for logging.
     * 
     * @return  process name for logging
     */
    public final static String getProcessTypeAsString() {
        switch (sProcessType) {
        case (APPLICATION_PROCESS) :
            return "Application process";
        case (SIP_SERVICE_PROCESS) :
            return "SIP Service process";
        default:
            return "Unknow process";
        }
    }

    /**
     * Returns the number of attempts to kill SIP service.
     * 
     * @return the number of attempts to kill SIP service.
     */
    public static long numberOfSIPServiceKillsAttempts() {
        return mNumberOfSIPServiceKillsAttempts;
    }
    
    /**
     * Returns the number SIP service kills.
     * 
     * @return the number SIP service kills.
     */
    public static long numberOfSIPServiceKills() {
        return mNumberOfSIPServiceKills;
    }
    
    /**
     * Keeps the number of attempts to kill SIP service.
     */
    private static long mNumberOfSIPServiceKillsAttempts = 0;
    
    /**
     * Keeps the number SIP service kills.
     */
    private static long mNumberOfSIPServiceKills = 0;
    
    /**
     * Tries to kill SIP service from application context.
     */
    public static final void tryToKillSIPService() {
        if (sProcessType != APPLICATION_PROCESS) {
            return;
        }
        mNumberOfSIPServiceKillsAttempts++;
        if (LogSettings.MARKET) {
            MktLog.e(TAG, "Kill ServiceSIP ...");
        }
        try {
            int myUID = android.os.Process.myUid();
            ActivityManager actmgr = (ActivityManager) sApplicationContext.getSystemService(Context.ACTIVITY_SERVICE);
            List<RunningAppProcessInfo> appList = actmgr.getRunningAppProcesses();
            for (int i = 0; i < appList.size(); i++) {
                RunningAppProcessInfo rti = (RunningAppProcessInfo) appList.get(i);
                Field uidField = rti.getClass().getDeclaredField("uid");                
                if ((uidField != null)&&(myUID == ((Number)uidField.get(rti)).intValue()) && rti.processName.contains(":comm")) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "Kill SIP Service with PID:" + rti.pid);
                    }
                    android.os.Process.killProcess(rti.pid);
                    mNumberOfSIPServiceKills++;
                    break;
                }
            }
        } catch (java.lang.Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "tryToKillSIPService:", th);
            }
        }
    }
    
    @Override
    public void onCreate() {
        super.onCreate();        
        sApplicationContext = getApplicationContext();
        sProcessType = APPLICATION_PROCESS;
        LOGGING_TYPE myLoggingSide = null;
        ActivityManager actmgr = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningAppProcessInfo> appList = actmgr.getRunningAppProcesses();
        for (int i = 0; i < appList.size(); i++) {
            RunningAppProcessInfo rti = (RunningAppProcessInfo) appList.get(i);
            /**
             * Important note: service process post-fix shall be synchronized
             * with the android:process value of ServicePJSIP declaration of
             * AndroidManifest.xml
             */
            if(rti.pid == android.os.Process.myPid()&& rti.processName.contains(":comm") ) {
                sProcessType = SIP_SERVICE_PROCESS;
                myLoggingSide = LOGGING_TYPE.SERVICE;
                break;
            }
        }
        
        SIProxy.ensureBindingStarted(sProcessType);
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onCreate");
        }
        RCMConfig.checkConfigRevision(this);
        FileUtils.createNomediaFileIfNotExist();
        GetJarHandler.configureGetJar(getApplicationContext());
        PhoneUtils.getParser();
        setNetworkState(NetworkUtils.getNetworkState(RingCentralApp.this));
        if (sProcessType == APPLICATION_PROCESS) {
        	RcmStatusNotification.onAppContextChanged();
        	MessagesNotification.cancelAll(getContextRC());
        	RingCentral.setRestart(true);
            sendBroadcast(new Intent(RCMConstants.ACTION_RC_APP_STARTED));   
        }
        LoggingManager.initializeWork((null == myLoggingSide) ? LOGGING_TYPE.APPLICATION : myLoggingSide);
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "onCreate completed");
        }
    }

    public static Context getContextRC(){    	
        return sApplicationContext;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "onTerminate: " + getProcessTypeAsString());
        }        
        
        /*
         * Stop RC PJSIP service
         */
        unBindSipService();
        sApplicationContext = null;
    }

    public void setNetworkState(NetworkState networkState) {
        // this.networkState = networkState;
        Intent i = new Intent(RCMConstants.ACTION_NETWORK_STATE_CHANGED);
        sendBroadcast(i);
    }


    /*
     * Unbind RC PJSIP service
     */
    private void unBindSipService(){
    	if (LogSettings.MARKET) {
            MktLog.d(TAG, "unBindSipService is starting...");
        }
    	stopSipService();
    	SIProxy.getInstance().destroy(true);
    }
    
    /*
     *  Stop RC PJSIP service
     */
    private void stopSipService(){
        IServicePJSIP m_service = SIProxy.getInstance().getService();
        try {
            if (m_service != null) {
                m_service.stopSipStack();
            }
        } catch (Throwable e) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "unBindSipServiceRoutine():stopSipStack:" + e.toString());
            }
        }
    }  

    public static String getBuildVersion(Context context, boolean showSvnRevision) {
        String version = null;
        try {
            version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            if (LogSettings.ENGINEERING) {
                EngLog.w(TAG, "getBuildVersion(): " + e.getMessage());
            }
        }
        
        if (version == null) {
            version = "1.7";    
        }

        if (showSvnRevision) {
            StringBuffer versionWithRevision = new StringBuffer(version);
            versionWithRevision.append(" (");
            versionWithRevision.append(context.getString(R.string.svn_revision));
            versionWithRevision.append(")");
            version = versionWithRevision.toString();
        }
            
        return version;
    }
	
    public static void logoutForNewSettings(Context context) {
        RingCentralApp.restartApplication(RCMProviderHelper.getLoginNumber(context), RCMProviderHelper
                .getLoginExt(context), "", false, true, true, context);
    }

    public static void logoutForOldSettings(Context context) {
        RingCentralApp.restartApplication(RCMProviderHelper.getLoginNumber(context), RCMProviderHelper
                .getLoginExt(context), RCMProviderHelper.getLoginPassword(context), false, true, false, context);
    }

    public static void restartApplication(Context context) {
    	 RingCentralApp.restartApplication(
                 RCMProviderHelper.getLoginNumber(context),
                 RCMProviderHelper.getLoginExt(context),
                 RCMProviderHelper.getLoginPassword(context),
                 true,
                 false,
                 true,
                 context);
    }
   
    
    public static void restartApplication(String phone, String ext, String password, boolean autologin, boolean clearCredentials, boolean fillCredentialsFields, Context context) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "restartApplication called..." + getProcessTypeAsString());
        }
        //RCMProviderPOJOHelper.deleteCurrentMailboxId(context);
        
        MessagesHandler.clearBodyLoaderTask();
        HttpClientFactory.shutdown();
        RequestInfoStorage.clearRequestHash(); // clear hashes as it have some links embedded
        RCMProviderHelper.clearSyncStatusTable(context);
        MessagesNotification.cancelAll(context);
        
        RCMProviderHelper.setCurrentMailboxId(context, MailboxCurrentTable.MAILBOX_ID_NONE);
        if (clearCredentials) {
            RCMProviderHelper.clearUserCredentialsTable(context);
        }       
        RcmStatusNotification.getInstance().cancel();
        
        Intent restartAppIntent = new Intent(RCMConstants.ACTION_RESTART_APP);
        context.sendBroadcast(restartAppIntent);

        Intent i = new Intent();
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra(LoginInfo.PHONE, phone);
        i.putExtra(LoginInfo.EXT, ext);
        i.putExtra(LoginInfo.PASSWORD, password);
        i.putExtra(LoginInfo.AUTOLOGIN, autologin);
        i.putExtra(LoginInfo.RESTART, true);
        i.putExtra(LoginInfo.AUTOFILL, fillCredentialsFields);
        i.setClass(context, LoginScreen.class);        
        context.startActivity(i);        
    }	
}
