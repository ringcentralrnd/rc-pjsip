/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.ringout;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.provider.Settings;
import android.text.ClipboardManager;
import android.text.Editable;
import android.text.InputType;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.NumberKeyListener;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.LoggingManager;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.LoggingScreen;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.contacts.CompanyFavorites;
import com.ringcentral.android.contacts.Cont;
import com.ringcentral.android.contacts.Cont.acts.ContactBinding;
import com.ringcentral.android.contacts.ContactPhoneNumber;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.phoneparser.PhoneNumberFormatter;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.settings.ConfigSettingsActivity;
import com.ringcentral.android.settings.DNDialog;
import com.ringcentral.android.settings.SettingsLauncher;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.DeviceUtils;
import com.ringcentral.android.utils.PhoneUtils;
import com.ringcentral.android.utils.ToneGeneratorUtils;
import com.ringcentral.android.utils.ui.widget.MenuCustomDialog;
import com.ringcentral.android.utils.ui.widget.RMenuWindow;
import com.ringcentral.android.utils.ui.widget.RMenuWindow.MainMenu;

public class RingOut extends Activity implements View.OnClickListener, View.OnLongClickListener, TextWatcher {

	private static final String TAG = "[RC]RingOut";
	private static final boolean DEBUG = false;
    
	private class FinishRingOutActivityReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (LogSettings.ENGINEERING) {
				EngLog.d(TAG, " FinishRingOutActivityReceiver ");
			}
    
			RingOut.this.finish();
		}
	}
    
    public static final String PHONE_NUMBER = "PhoneNumber";
    
    //!!! Don't change the characters order in this array!!! 
    //Changing it may lead to issues on Motorola Milestone and possibly certain other handsets
    //due to Android/Motorola platform bug (see PP-2491 for details)
    private static final char[] DIAL_CHARS = {'+', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '#'};
	
	private EditText mDigits;
	private View mBtnBackspace;
	private View mBtnCall;
	private Button mBtnUser;
	private Button mBtnBack;
	
	private Drawable mDigitsBackground;
	private Drawable mDigitsEmptyBackground;

	private BroadcastReceiver mBroadcastReceiver;
	private int mDisplayOrientation;

	private boolean mKeyTonesEnabled;
	private ToneGeneratorUtils mToneGenerator;

	private String mLastSuccessNumber;
	private boolean mCallActive;

	private ClipboardManager mClipboardManager;

	private PhoneNumberFormatter mFormatter;

	private RingOutAgent mRingOutAgent;

	private MenuCustomDialog mMenuCustomDialog;

	private FinishRingOutActivityReceiver mFinishRingout;

    
    
//-------------------------------------------------------------------
	
	/* Overridden Activity class methods */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (LogSettings.ENGINEERING) {
			EngLog.i(TAG, "onCreate()...");
		}
		
		super.onCreate(savedInstanceState);
		
		mCallActive = false;
		mLastSuccessNumber = RCMProviderHelper.getLastCallNumber(this);

        // Load up the resources for the text field.
        Resources r = getResources();
        mDigitsBackground = r.getDrawable(R.drawable.bg_dialpad_entryarea_active);
        mDigitsEmptyBackground = r.getDrawable(R.drawable.bg_dialpad_entryarea_normal);
        
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Build Layout according to orientation
        buildLayout(DeviceUtils.getDisplayOrientation(this));
        
        mBroadcastReceiver = new RingoutBroadcastReceiver();
        registerReceiver(mBroadcastReceiver, new IntentFilter(RCMConstants.ACTION_CONFIGURATION_CHANGED));        
    
        mClipboardManager = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
        mFormatter = new PhoneNumberFormatter();
        mRingOutAgent = new RingOutAgent(this);
        
        mFinishRingout = new FinishRingOutActivityReceiver();
		registerReceiver(mFinishRingout, new IntentFilter(RCMConstants.ACTION_CLOSE_RINGOUT));
        
        mFinishRingout = new FinishRingOutActivityReceiver();
		registerReceiver(mFinishRingout, new IntentFilter(RCMConstants.ACTION_CLOSE_RINGOUT));
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (LogSettings.ENGINEERING) {
			EngLog.i(TAG, "onResume()...");
		}

		FlurryTypes.onEventScreen(FlurryTypes.EVENT_SCREEN_OPEN, FlurryTypes.SCR_KEYPAD);

		// Last success number might be changed by RingOut call from this or another Activity (Contacts, CallLog, Messages etc.)
		mLastSuccessNumber = RCMProviderHelper.getLastCallNumber(this);
		mBtnCall.setClickable(isCallBtnClickable());

		// ToneGenerator is created in onResume() and destroyed in onPause()
		// to avoid occupying audio resources when the Activity is in background
		mKeyTonesEnabled = (Settings.System.getInt(getContentResolver(), Settings.System.DTMF_TONE_WHEN_DIALING, 1) != 0);
		if (mKeyTonesEnabled) {
			mToneGenerator = new ToneGeneratorUtils(this, AudioManager.STREAM_SYSTEM, ToneGeneratorUtils.KEY_TONE_RELATIVE_VOLUME_30);
		}
	}

	@Override
	protected void onPause() {
		if (LogSettings.ENGINEERING) {
			EngLog.i(TAG, "onPause()...");
		}

		super.onPause();

		if (mToneGenerator != null) {
			mToneGenerator.destroy();
			mToneGenerator = null;
		}
	}

	@Override
	protected void onRestart() {
		if (LogSettings.ENGINEERING) {
			EngLog.i(TAG, "onRestart()...");
		}

		super.onRestart();

		// onConfigurationChanged() is not always called when the Activity becomes visible (Android bug?)
		// That's why display orientation is checked here
		int orientation = DeviceUtils.getDisplayOrientation(this);
		if (orientation != mDisplayOrientation) {
			if (LogSettings.MARKET) {
				MktLog.i(TAG, "onRestart(): orientation changed: " + orientation);
			}
			buildLayout(orientation);
		}
	}

	@Override
    protected void onDestroy() {
        if (LogSettings.ENGINEERING) {
            EngLog.i(TAG, "onDestroy()...");
        }
        
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
        mBroadcastReceiver = null;
        
        unregisterReceiver(mFinishRingout);
        mFinishRingout = null;
        
        leakCleanUpRootView();
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	FlurryTypes.onStartSession(this);
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	FlurryTypes.onEndSession(this);
    }
    

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		if (LogSettings.MARKET) {
			MktLog.i(TAG, "onConfigurationChanged(): orientation = " + newConfig.orientation);
		}
		configurationChanged(newConfig);
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.ringout_search:
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "User pressed Search menu btn");
                }
                onSearchRequested();
                return true;
            case R.id.ringout_settings:
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "User pressed Settings menu btn");
                }
                SettingsLauncher.launch(this);
                return true;
            case R.id.ringout_dnd:
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "User pressed DND menu btn");
                }
                DNDialog.showDNDDialog(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		
		switch (v.getId()) {
		case R.id.digits:
			menu.clear(); // Remove Android's default menu items

			getMenuInflater().inflate(R.menu.ringout_digits_context_menu, menu);
			menu.setHeaderTitle(R.string.ringout_context_menu_title);

			if (mDigits.length() != 0) {
				menu.setGroupVisible(R.id.copy_group, true);
			} else {
				menu.setGroupVisible(R.id.copy_group, false);
			}

			if (mClipboardManager.hasText() 
					&& PhoneNumberFormatter.hasPhoneNumberSymbols(mClipboardManager.getText().toString())) {
				menu.setGroupVisible(R.id.paste_group, true);
			} else {
				menu.setGroupVisible(R.id.paste_group, false);
			}

			return;
		default:
			return;
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case R.id.copy:
			if (LogSettings.MARKET) {
				MktLog.i(TAG, "User pressed Copy context menu");
			}
			mClipboardManager.setText(PhoneNumberFormatter.trimNonNumberSymbols(mDigits.getText().toString()));
			return true;
		case R.id.cut:
			if (LogSettings.MARKET) {
				MktLog.i(TAG, "User pressed Cut context menu");
			}
			mClipboardManager.setText(PhoneNumberFormatter.trimNonNumberSymbols(mDigits.getText().toString()));
			mDigits.getEditableText().clear();
			return true;
		case R.id.paste:
			if (LogSettings.MARKET) {
				MktLog.i(TAG, "User pressed Paste context menu");
			}
			Editable number = mDigits.getEditableText();
			number.append(PhoneNumberFormatter.trimNonNumberSymbols(mClipboardManager.getText().toString()));
			mDigits.setText(mFormatter.getFormatted(number.toString()));
			mDigits.setSelection(mDigits.length()); // move cursor to the end
			FlurryTypes.onEvent(FlurryTypes.EVENT_PASTE, FlurryTypes.PASTE_KEYPADPHONENUMBER);
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onActivityResult(requestCode=" + requestCode + ", resultCode=" + resultCode + ",...)");
        }
        
        super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RCMConstants.ACTIVITY_REQUEST_CODE_RINGOUT_CALL) {
			mCallActive = false;

			if (resultCode != RCMConstants.RINGOUT_RESULT_FAIL) {
				if (LogSettings.MARKET) {
					MktLog.i(TAG, "onActivityResult():  Call Failed");
				}

				mLastSuccessNumber = RCMProviderHelper.getLastCallNumber(this);
				mDigits.getText().clear();
			}
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED && hasFocus) {
			PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.DIALER_OPEN);
		}
	}

	// -------------------------------------------------------------------

	/* View.OnClickListener interface implementation */

	public void onClick(View view) {
		if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
			PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.DIALER_KEYTONE);
		}
		switch (view.getId()) {
		case R.id.one:
			view.performHapticFeedback(0);
			playTone(view, ToneGenerator.TONE_DTMF_1, ToneGeneratorUtils.SHORT_KEY_TONE_LENGTH_MS);
			injectKeyEvent(KeyEvent.KEYCODE_1);
			return;
		case R.id.two:
			view.performHapticFeedback(0);
			playTone(view, ToneGenerator.TONE_DTMF_2, ToneGeneratorUtils.SHORT_KEY_TONE_LENGTH_MS);
			injectKeyEvent(KeyEvent.KEYCODE_2);
			return;
		case R.id.three:
			view.performHapticFeedback(0);
			playTone(view, ToneGenerator.TONE_DTMF_3, ToneGeneratorUtils.SHORT_KEY_TONE_LENGTH_MS);
			injectKeyEvent(KeyEvent.KEYCODE_3);
			return;
		case R.id.four:
			view.performHapticFeedback(0);
			playTone(view, ToneGenerator.TONE_DTMF_4, ToneGeneratorUtils.SHORT_KEY_TONE_LENGTH_MS);
			injectKeyEvent(KeyEvent.KEYCODE_4);
			return;
		case R.id.five:
			view.performHapticFeedback(0);
			playTone(view, ToneGenerator.TONE_DTMF_5, ToneGeneratorUtils.SHORT_KEY_TONE_LENGTH_MS);
			injectKeyEvent(KeyEvent.KEYCODE_5);
			return;
		case R.id.six:
			view.performHapticFeedback(0);
			playTone(view, ToneGenerator.TONE_DTMF_6, ToneGeneratorUtils.SHORT_KEY_TONE_LENGTH_MS);
			injectKeyEvent(KeyEvent.KEYCODE_6);
			return;
		case R.id.seven:
			view.performHapticFeedback(0);
			playTone(view, ToneGenerator.TONE_DTMF_7, ToneGeneratorUtils.SHORT_KEY_TONE_LENGTH_MS);
			injectKeyEvent(KeyEvent.KEYCODE_7);
			return;
		case R.id.eight:
			view.performHapticFeedback(0);
			playTone(view, ToneGenerator.TONE_DTMF_8, ToneGeneratorUtils.SHORT_KEY_TONE_LENGTH_MS);
			injectKeyEvent(KeyEvent.KEYCODE_8);
			return;
		case R.id.nine:
			view.performHapticFeedback(0);
			playTone(view, ToneGenerator.TONE_DTMF_9, ToneGeneratorUtils.SHORT_KEY_TONE_LENGTH_MS);
			injectKeyEvent(KeyEvent.KEYCODE_9);
			return;
		case R.id.zero:
			view.performHapticFeedback(0);
			playTone(view, ToneGenerator.TONE_DTMF_0, ToneGeneratorUtils.SHORT_KEY_TONE_LENGTH_MS);
			injectKeyEvent(KeyEvent.KEYCODE_0);
			return;
		case R.id.pound:
			view.performHapticFeedback(0);
			playTone(view, ToneGenerator.TONE_DTMF_P, ToneGeneratorUtils.SHORT_KEY_TONE_LENGTH_MS);
			injectKeyEvent(KeyEvent.KEYCODE_POUND);
			return;
		case R.id.star:
			view.performHapticFeedback(0);
			playTone(view, ToneGenerator.TONE_DTMF_S, ToneGeneratorUtils.SHORT_KEY_TONE_LENGTH_MS);
			injectKeyEvent(KeyEvent.KEYCODE_STAR);
			return;
		case R.id.btnTopLeft:
			this.finish();
			return;
		case R.id.backspace:
			view.performHapticFeedback(0);
			injectKeyEvent(KeyEvent.KEYCODE_DEL);
			return;
		case R.id.btnCall:
			if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
				PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.DIALER_CALL);
				PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.CALL_INITIZED);
			}
			view.performHapticFeedback(0);

			String phone = mDigits.getText().toString();

			if (LogSettings.MARKET) {
				MktLog.i(TAG, "User pressed Call soft btn; phone num: " + phone);
			}

			if (LogSettings.ENGINEERING) {
				if (LoggingManager.isItTestCase(phone)) { // Magic number for logging test (forced crash)
					LoggingManager.loggingTest();
					return;
				}
			}

			if (LoggingManager.itIsLoggerCase(phone)) { // Magic number for sending logs
				startActivity(new Intent(this, LoggingScreen.class));
				return;
			}

			if (ConfigSettingsActivity.isEnabled(phone)) {
				startActivity(new Intent(this, ConfigSettingsActivity.class));
				return;
			}

			injectKeyEvent(KeyEvent.KEYCODE_CALL);
			return;

		case R.id.btnTopRight:
			view.performHapticFeedback(0);

			if (LogSettings.MARKET) {
				MktLog.i(TAG, "User pressed User soft btn; phone num: " + mDigits.getText().toString());
			}

			try {
				onUserButtonPress();
			} catch (java.lang.Throwable error) {
				if (LogSettings.MARKET) {
					MktLog.e(TAG, "onUserButtonPress() error: " + error.getMessage(), error);
				}
			}
			return;
		}
	}

	/**
	 * On "User" button press.
	 */
	private void onUserButtonPress() {
		String number = mDigits.getText().toString();
		ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumberWithoutTrim(PhoneNumberFormatter.trimNonDigitSymbols(number));
		if (!cpn.isValid) {
			if (LogSettings.MARKET) {
				MktLog.w(TAG, "onUserButtonPress(): invalid number: " + number);
			}
			return;
		}

		final Context ctx = this;
		final long mailboxId = RCMProviderHelper.getCurrentMailboxId(ctx);
		final ContactBinding cb = Cont.acts.bindContactByNumber(ctx, mailboxId, cpn.phoneNumber.numRaw);
		if (!cb.isValid) {
			if (LogSettings.MARKET) {
				MktLog.w(TAG, "onUserButtonPress(): invalid binding: " + number);
			}
			return;
		}

		if (cb.cpn.phoneNumber.isSpecialNumber) {
			if (LogSettings.MARKET) {
				MktLog.w(TAG, "onUserButtonPress(): special number: " + number);
			}
			return;
		}

		if (cb.hasContact && cb.isPhoneNumberFavorite) {
			if (LogSettings.MARKET) {
				MktLog.w(TAG, "onUserButtonPress(): number is in contacts and favorites: " + number);
			}
			Toast.makeText(ctx, ctx.getResources().getString(R.string.ringout_alert_number_contact_and_favorite), Toast.LENGTH_LONG).show();
			return;
		}

		AlertDialog.Builder builder = RcAlertDialog.getBuilder(ctx);

		if (cb.hasContact) {
			builder.setTitle(cb.displayName + ": " + cb.cpn.phoneNumber.localCanonical);
		} else {
			builder.setTitle(cb.cpn.phoneNumber.localCanonical);
		}
		builder.setCancelable(true);
		NumberActionSelectionListAdapter adapter = new NumberActionSelectionListAdapter(ctx, cb, mailboxId);
		builder.setView(adapter);

		AlertDialog alert = builder.create();
		adapter.setDialog(alert);
		alert.show();
	}

	/**
	 * "Create New Contact", "Add no Existing Contact", "Add to Favorites" selection dialog.
	 */
	private class NumberActionSelectionListAdapter extends LinearLayout {
		/**
		 * Keeps a contact bind.
		 */
		private ContactBinding mCb;

		/**
		 * Keeps dialog owner.
		 */
		private AlertDialog mAlert;

		/**
		 * Keeps execution context.
		 */
		private Context mContext;

		/**
		 * Keeps mailbox identifier.
		 */
		private long mMailboxid;

		/**
		 * Default constructor.
		 * 
		 * @param context
		 *            the execution context
		 * 
		 * @param cb
		 *            the contact bind
		 */
		public NumberActionSelectionListAdapter(Context context, ContactBinding cb, long mailboxId) {
			super(context);
			mContext = context;
			mCb = cb;
			mMailboxid = mailboxId;
			this.setOrientation(VERTICAL);
			if (mCb.hasContact) {
				setButton(context, mCreateButtonListener, false, R.string.calllog_menu_add_to_contacts_new);
				setButton(context, mAddToExistingButtonListener, false, R.string.calllog_menu_add_to_contacts_existing);
				setButton(context, mAddTofavoritesButtonListener, true, R.string.calllog_menu_add_to_faworites);
			} else {
				setButton(context, mCreateButtonListener, true, R.string.calllog_menu_add_to_contacts_new);
				setButton(context, mAddToExistingButtonListener, true, R.string.calllog_menu_add_to_contacts_existing);
				setButton(context, mAddTofavoritesButtonListener, false, R.string.calllog_menu_add_to_faworites);
			}
		}

		/**
		 * Adds a button to the parent view.
		 * 
		 * @param context
		 *            the execution context
		 * @param listener
		 *            the button click listener
		 * @param state
		 *            the button state
		 * @param resid
		 *            the title resource id
		 */
		private void setButton(Context context, OnClickListener listener, boolean state, int resid) {
			Button btn = new Button(context);
			btn.setText(resid);
			btn.setClickable(state);
			btn.setFocusable(state);
			btn.setEnabled(state);
			addView(btn, new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			btn.setOnClickListener(listener);
		}

		/**
		 * Keeps "Create New Contact" button click listener.
		 */
		private OnClickListener mCreateButtonListener = new OnClickListener() {
			public void onClick(View v) {
				if (LogSettings.MARKET) {
					MktLog.i(TAG, "User pressed 'Create New Contact'");
				}

				if (mCb.hasContact) {
					if (LogSettings.MARKET) {
						MktLog.w(TAG, "Create New Contact: Contact exists; do nothing");
					}
					return;
				}
				FlurryTypes.onEventScreen(FlurryTypes.EVENT_CREATE_NEW_CONTACT, FlurryTypes.SCR_KEYPAD);
				try {
					startActivity(Cont.acts().getCreateNewContactIntent(mContext, mCb.cpn.phoneNumber.localCanonical, mCb.cpn.phoneNumber.numRaw));
				} catch (ActivityNotFoundException e) {
					if (LogSettings.MARKET) {
						MktLog.e(TAG, "create new contact start activity error: " + e.toString());
					}
				}
				dismiss();
			}
		};

		/**
		 * Keeps "Add To Existing Contact" button click listener.
		 */
		private OnClickListener mAddToExistingButtonListener = new OnClickListener() {
			public void onClick(View v) {
				if (LogSettings.MARKET) {
					MktLog.i(TAG, "User pressed 'Add To Existing Contact'");
				}

				if (mCb.hasContact) {
					if (LogSettings.MARKET) {
						MktLog.w(TAG, "Add To Existing Contact: number already binded; do nothing");
					}
					return;
				}
				FlurryTypes.onEventScreen(FlurryTypes.EVENT_ADD_EXISTING_CONTACT, FlurryTypes.SCR_KEYPAD);
				try {
					startActivity(Cont.acts().getAddToContactIntent(mContext, mCb.cpn.phoneNumber.numRaw));
				} catch (ActivityNotFoundException e) {
					if (LogSettings.MARKET) {
						MktLog.e(TAG, "add to existing contact start activity error: " + e.toString());
					}
				}
				dismiss();
			}
		};

		/**
		 * Keeps "Add To Favorites" button click listener.
		 */
		private OnClickListener mAddTofavoritesButtonListener = new OnClickListener() {
			public void onClick(View v) {
				if (LogSettings.MARKET) {
					MktLog.i(TAG, "User pressed 'Add To Favorites'");
				}
				if (!mCb.hasContact) {
					if (LogSettings.MARKET) {
						MktLog.w(TAG, "Add To Favorites: number not binded; do nothing");
					}
					return;
				}
				FlurryTypes.onEventScreen(FlurryTypes.EVENT_ADD_FAVORITES, FlurryTypes.SCR_KEYPAD);
				if (mCb.isPhoneNumberFavorite) {
					if (LogSettings.MARKET) {
						MktLog.w(TAG, "Add To Favorites: number already in Favorites; do nothing");
					}
					return;
				}

				if (mCb.isPersonalContact) {
					Cont.acts().addToPersonalFavorites(mCb.phoneId, mCb.cpn.phoneNumber.numRaw, mCb.cpn.phoneNumber.numRaw, mContext);
				} else {
					CompanyFavorites.setAsFavorite(mContext, mMailboxid, mCb.phoneId);
				}
				dismiss();
			}
		};

		/**
		 * Sets owner.
		 * 
		 * @param alert
		 *            the owner
		 */
		public void setDialog(AlertDialog alert) {
			mAlert = alert;
		}

		/**
		 * Dismiss the dialog.
		 */
		private void dismiss() {
			if (mAlert != null) {
				try {
					mAlert.dismiss();
				} catch (java.lang.Throwable error) {
					if (LogSettings.MARKET) {
						MktLog.e(TAG, "NumberActionSelectionListAdapter() dismiss error: ", error);
					}
				}
			}
			mAlert = null;
			mContext = null;
		}
	}

	// -------------------------------------------------------------------

	/* View.OnLongClickListener interface implementation */

	public boolean onLongClick(View view) {
		switch (view.getId()) {
		case R.id.backspace:
			mDigits.getEditableText().clear();
			mBtnBackspace.setPressed(false);
			return true;
		case R.id.zero:
			playTone(view, ToneGenerator.TONE_DTMF_0, ToneGeneratorUtils.LONG_KEY_TONE_LENGTH_MS);
			injectKeyEvent(KeyEvent.KEYCODE_PLUS);
			return true;
		default:
			return false;
		}
	}

	// -------------------------------------------------------------------

	/* TextWatcher interface implementation */

	@Override
	public void afterTextChanged(Editable input) {
		mBtnCall.setClickable(isCallBtnClickable());
		mBtnUser.setClickable(mDigits.length() != 0);
		mBtnUser.setText(getResources().getString(R.string.toprightadd));
		if (mDigits.length() > 0) {
			mBtnUser.setTextColor(getResources().getColor(R.color.textColorWhite));
		} else {
			mBtnUser.setTextColor(getResources().getColor(R.color.textColorGray));
		}
		setBtnBackspace();
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

	// -------------------------------------------------------------------

	/**
	 * Build layout according to screen orientation
	 */
	private void buildLayout(int orientation) {
		setContentView(R.layout.ringoutview); // PORTRAIT or SQUARE
		this.mMenuCustomDialog = new MenuCustomDialog(RingOut.this, android.R.style.Theme_Translucent_NoTitleBar);
		mDisplayOrientation = orientation;

		String old_number = (mDigits != null ? mDigits.getText().toString() : "");

		mDigits = (EditText) findViewById(R.id.digits);
		mDigits.getEditableText().append(old_number);
		Selection.setSelection(mDigits.getEditableText(), mDigits.length()); // set cursor to the end
		mDigits.setKeyListener(new RingOutKeyListener());
		mDigits.setFocusable(false); // MUST be always AFTER mDigits.setKeyListener()
										// otherwise setKeyListener() will re-set 'focusable' to 'true'
		mDigits.addTextChangedListener(this);
		registerForContextMenu(mDigits);

		mBtnBackspace = findViewById(R.id.backspace);
		mBtnBackspace.setOnClickListener(this);
		mBtnBackspace.setClickable(mDigits.length() != 0);
		mBtnBackspace.setOnLongClickListener(this);
		mBtnBackspace.setLongClickable(mDigits.length() != 0);

		mBtnCall = findViewById(R.id.btnCall);
		mBtnCall.setOnClickListener(this);
		mBtnCall.setClickable(isCallBtnClickable());

		mBtnUser = (Button) findViewById(R.id.btnTopRight);
		mBtnUser.setVisibility(View.VISIBLE);
		mBtnUser.setOnClickListener(this);
		mBtnUser.setText(getResources().getString(R.string.toprightadd));
		mBtnUser.setClickable(mDigits.length() != 0);
		if (mDigits.length() > 0) {
			mBtnUser.setTextColor(getResources().getColor(R.color.textColorWhite));
		} else {
			mBtnUser.setTextColor(getResources().getColor(R.color.textColorGray));
		}
		mBtnBack = (Button) findViewById(R.id.btnTopLeft);
		mBtnBack.setVisibility(View.VISIBLE);
		mBtnBack.setText(getResources().getString(R.string.topleftback));
		mBtnBack.setTextColor(getResources().getColor(R.color.textColorWhite));
		mBtnBack.setOnClickListener(this);

		findViewById(R.id.one).setOnClickListener(this);
		findViewById(R.id.two).setOnClickListener(this);
		findViewById(R.id.three).setOnClickListener(this);
		findViewById(R.id.four).setOnClickListener(this);
		findViewById(R.id.five).setOnClickListener(this);
		findViewById(R.id.six).setOnClickListener(this);
		findViewById(R.id.seven).setOnClickListener(this);
		findViewById(R.id.eight).setOnClickListener(this);
		findViewById(R.id.nine).setOnClickListener(this);
		findViewById(R.id.star).setOnClickListener(this);
		findViewById(R.id.pound).setOnClickListener(this);

		View view = findViewById(R.id.zero);
		view.setOnClickListener(this);
		view.setOnLongClickListener(this);
	}

	private void configurationChanged(Configuration newConfig) {
		int orientation = newConfig.orientation;

		if (orientation == Configuration.ORIENTATION_UNDEFINED) {
			orientation = DeviceUtils.calcDisplayOrientation(this);
			if (LogSettings.MARKET) {
				MktLog.w(TAG, "configurationChanged(): ORIENTATION_UNDEFINED; calculated orientation = " + orientation);
			}
		}

		if (orientation != mDisplayOrientation) {
			if (LogSettings.MARKET) {
				MktLog.i(TAG, "configurationChanged(): orientation changed; rebuild");
			}
			buildLayout(orientation);
		} else {
			if (LogSettings.MARKET) {
				MktLog.i(TAG, "configurationChanged(): orientation not changed");
			}
		}
	}

	private void setBtnBackspace() {
		if (mDigits.length() > 0) {
			mBtnBackspace.setVisibility(View.VISIBLE);
			mBtnBackspace.setClickable(mDigits.length() != 0);
			mBtnBackspace.setLongClickable(mDigits.length() != 0);
		} else {
			mBtnBackspace.setVisibility(View.GONE);
		}
	}

	private boolean isCallBtnClickable() {
		return (mDigits.length() != 0 || !TextUtils.isEmpty(mLastSuccessNumber));
	}

	private void injectKeyEvent(int keyCode) {
		mDigits.getKeyListener().onKeyDown(mDigits, mDigits.getEditableText(), keyCode, new KeyEvent(KeyEvent.ACTION_DOWN, keyCode));
	}

	private void playTone(View view, int tone, int length) {
		if (!mKeyTonesEnabled || mToneGenerator == null) {
			return;
		}

		final int ringerMode = ((AudioManager) getSystemService(Context.AUDIO_SERVICE)).getRingerMode();
		if ((ringerMode == AudioManager.RINGER_MODE_SILENT) || (ringerMode == AudioManager.RINGER_MODE_VIBRATE)) {
			return;
		}
		if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
			PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.DIALER_KEYTONE);
		}
		mToneGenerator.playTone(tone, length);
	}

	// -----------------------------------------------------------------------------

	private class RingoutBroadcastReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			if (action.equals(RCMConstants.ACTION_CONFIGURATION_CHANGED)) {

				// Workaround for Android bug #6893 (https://code.google.com/p/android/issues/detail?id=6893):
				// sometimes onConfigurationChanged() is not invoked at screen orientation change
				// As a workaround, host activity 'RingCentral.java' sends ACTION_CONFIGURATION_CHANGED broadcast
				// from its own onConfigurationChanged()
				if (LogSettings.MARKET) {
					MktLog.i(TAG, "mBroadcastReceiver.onReceive(ACTION_CONFIGURATION_CHANGED)...");
				}
				configurationChanged((Configuration) intent.getParcelableExtra(RCMConstants.EXTRA_NEW_CONFIG));
			}
		}
	}

	// -----------------------------------------------------------------------------

	// Support input from hardware keyboard
	private class RingOutKeyListener extends NumberKeyListener {

		@Override
		public int getInputType() {
			return InputType.TYPE_NULL; // disable soft keyboard
		}

		@Override
		protected char[] getAcceptedChars() {
			return DIAL_CHARS;
		}

		@Override
		public boolean onKeyDown(View view, Editable content, int keyCode, KeyEvent event) {
			switch (keyCode) {
			case KeyEvent.KEYCODE_ENTER:
			case KeyEvent.KEYCODE_CALL:
				if (mCallActive) {
					return true; // To prevent multiple call initiation
				}

				if (mDigits.length() != 0) {
					mCallActive = true;
					String number = mDigits.getText().toString();
					if (LogSettings.MARKET) {
						MktLog.i(TAG, "Start RO Agent: " + number);
					}
					FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, FlurryTypes.SCR_KEYPAD);
					mRingOutAgent.call(number, null);
				} else {
					mLastSuccessNumber = RCMProviderHelper.getLastCallNumber(RingOut.this);
					mDigits.setText(mLastSuccessNumber); // display last called number
				}
				return true;
			case KeyEvent.KEYCODE_DEL:
				Editable text = mDigits.getEditableText();
				int i;

				del_last_typed_char: for (i = text.length() - 1; i >= 0; i--) {
					for (char ch : DIAL_CHARS) {
						if (text.charAt(i) == ch) {
							text.delete(i, text.length());
							break del_last_typed_char;
						}
					}
				}

				if (i < 0) { // no valid symbols found
					text.clear();
					return true;
				}

				mDigits.setText(mFormatter.getFormatted(text.toString()));
				mDigits.setSelection(mDigits.length()); // move cursor to the end
				return true;
			case KeyEvent.KEYCODE_BACK:
				RingOut.this.finish();
				return true;
			case KeyEvent.KEYCODE_MENU:
				mMenuCustomDialog.show();
				return true;
			default:
				super.onKeyDown(view, content, keyCode, event);
				mDigits.setText(mFormatter.getFormatted(mDigits.getText().toString()));
				mDigits.setSelection(mDigits.length()); // move cursor to the end
				return true;
			}
		}
	}

	// ====================== Workaround Section (BN) ==========================
	// == Reduces memory leaks. The section can be just added to any Activity ==
	// == Call leakCleanUpRootView onDestroy and before setting new root view ==

	@Override
	public void setContentView(int layoutResID) {
		ViewGroup rootView = (ViewGroup) LayoutInflater.from(this).inflate(layoutResID, null);
		setContentView(rootView);
	}

	@Override
	public void setContentView(View view) {
		super.setContentView(view);
		leakCleanUpRootView();
		mLeakContentView = (ViewGroup) view;
	}

	@Override
	public void setContentView(View view, LayoutParams params) {
		super.setContentView(view, params);
		leakCleanUpRootView();
		mLeakContentView = (ViewGroup) view;
	}

	/**
	 * Cleanup root view to reduce memory leaks.
	 */
	private void leakCleanUpRootView() {
		if (mLeakContentView != null) {
			ViewGroup v = mLeakContentView;
			mLeakContentView = null;
			mStackRecursions = 0;
			leakCleanUpChildsDrawables(v);
			System.gc();
		}
	}

	/**
	 * Clean-up Drawables in the view including child.
	 * 
	 * @param v
	 */
	private void leakCleanUpChildsDrawables(View v) {
		if (v != null) {
			try {
				ViewGroup group = (ViewGroup) v;
				int childs = group.getChildCount();
				for (int i = 0; i < childs; i++) {
					if (LEAKS_CLEAN_UP_STACK_RECURSIONS_LIMIT > mStackRecursions) {
						mStackRecursions++;
						leakCleanUpChildsDrawables(group.getChildAt(i));
						mStackRecursions--;
					} else {
						break;
					}
				}
			} catch (java.lang.Throwable th) {
			}
			leakCleanUpDrawables(v);
		}
	}

	/**
	 * Keeps recursions number for memory clean-ups.
	 */
	private int mStackRecursions;

	/**
	 * Limit of leaks clean-ups stack recursions.
	 */
	private static final int LEAKS_CLEAN_UP_STACK_RECURSIONS_LIMIT = 256;

	/**
	 * Cleans drawables of in the view.
	 * 
	 * @param v
	 *            the view to clean-up
	 */
	private void leakCleanUpDrawables(View v) {
		if (v == null) {
			return;
		}

		try {
			if (v.getBackground() != null) {
				v.getBackground().setCallback(null);
			}
		} catch (java.lang.Throwable th) {
		}

		try {
			v.setBackgroundDrawable(null);
		} catch (java.lang.Throwable th) {
		}

		try {
			ImageView imageView = (ImageView) v;
			imageView.setImageDrawable(null);
			imageView.setBackgroundDrawable(null);
		} catch (java.lang.Throwable th) {
		}

	}

	/**
	 * Keeps current root view.
	 */
	private ViewGroup mLeakContentView = null;

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
    	if (event.getKeyCode() == KeyEvent.KEYCODE_MENU && event.getAction() == KeyEvent.ACTION_DOWN) {
    		RMenuWindow.MainMenu menuView = new MainMenu(this);
    		menuView.show();
            return true;
        } else {
        	return super.dispatchKeyEvent(event);
        }
    }
    
}
