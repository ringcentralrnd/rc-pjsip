/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.ringout;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.service.PJSIP;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.phoneparser.PhoneNumber;
import com.ringcentral.android.phoneparser.PhoneNumberParser;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.provider.RCMDataStore.AccountInfoTable;
import com.ringcentral.android.provider.RCMDataStore.ExtensionsTable;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.DeviceUtils;
import com.ringcentral.android.utils.NetworkUtils;
import com.ringcentral.android.utils.PhoneUtils;
import com.ringcentral.android.utils.UserInfo;
import com.ringcentral.android.utils.NetworkUtils.NetworkState;


public class CallValidator extends Activity {

    private static final String TAG = "[RC]CallValidator";
    private static final boolean DEBUG = false;
    
    private static final int DIALOG_TO_NUMBER_INVALID_ID 		= 1;
    private static final int DIALOG_FROM_NUMBER_INVALID_ID 		= 2;
    private static final int DIALOG_NO_PERMISSIONS_ID 			= 3;
    private static final int DIALOG_EXTENSION_NOT_FOUND_ID 		= 4;
    private static final int DIALOG_NO_INTERNATIONAL_ID 		= 5;
    private static final int DIALOG_NO_FREE_INTERNATIONAL_ID 	= 6;
    private static final int DIALOG_TOO_MANY_VOIP_CALLS			= 7;

    private static final int VALID_NUMBER = 1;
    private static final int VALID_EXTENSION = 2;
    private static final int VALIDATION_FAILED = 3;
    
    private boolean mUseVoip;
    
    protected boolean isUseVoip(){
        if (BUILD.VOIP_ENABLED) {
            if (!RCMProviderHelper.isVoipEnabled_Env_Acc_SipFlagHttpReg_ToS_UserVoIP(this))
                return false;

            final RingCentralApp rcApp = (RingCentralApp) this.getApplication();
            if (rcApp != null) {
                if (!rcApp.isSipServiceOnForVoipCalls())
                    return false;
            }

            NetworkState netState = NetworkUtils.getNetworkState(this);
            if (RCMProviderHelper.isVoipSwitchedOn_UserWiFi(this)) {
                if (netState == NetworkState.WIFI || netState == NetworkState.FULL)
                    return true;
            }

            if (RCMProviderHelper.isVoipSwitchedOn_User3g4g(this)) {
                if (netState == NetworkState.MOBILE || netState == NetworkState.FULL)
                    return true;
            }
        }
        return false;
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (LogSettings.ENGINEERING && DEBUG) {
            EngLog.d(TAG, "onCreate()..." + (savedInstanceState == null ?
                    "savedInstanceState==null" : "savedInstanceState!=null"));
        }
        
        
        if (savedInstanceState != null) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "onCreate(): Activity re-creation forbidden; FINISH");
            }
            finish();
            return;
        }
        
        
        mUseVoip = isUseVoip();
        
        String toNumber = getIntent().getStringExtra(RCMConstants.EXTRA_CALL_TO_NUMBER);
        
        String fromNumber;
        if (mUseVoip){
            fromNumber = DeviceUtils.getDeviceNumber(this);
        } else if (RCMProviderHelper.getRingoutMode(this) == AccountInfoTable.RINGOUT_MODE_MY_ANDROID) {
            fromNumber = DeviceUtils.getDeviceNumber(this);
        } else {
            fromNumber = RCMProviderHelper.getRingoutAnotherPhone(this);
        }
        
        String toName = getIntent().getStringExtra(RCMConstants.EXTRA_CALL_TO_NAME);        
        if(PhoneNumber.isPhoneNumber(toName)){
        	toName = null;
        }
        
        int validation = validateCall(toNumber, fromNumber);  
        
//        if (validation == VALIDATION_FAILED) {
//            return;
//        }

        /*
         * Check number of calls for VoIP
         */
        if (mUseVoip && BUILD.VOIP_ENABLED) {
            final int callState = NetworkUtils.getCelluarCallState(this);
            if (callState == TelephonyManager.CALL_STATE_OFFHOOK) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "Device native call state: Make RingOut");
                }
                mUseVoip = false;
            }

            if (mUseVoip) {
                final RingCentralApp rcApp = (RingCentralApp) this.getApplication();
                if (rcApp != null) {
                    mUseVoip = rcApp.canMakeVoipOutboundCall();

                    if (mUseVoip) {
                        final int sipCallCount = rcApp.getSipActiveCallsCount();
                        if (sipCallCount >= PJSIP.sf_MAX_NUMBER_CALLS) {
                            if (LogSettings.MARKET) {
                                MktLog.w(TAG, "Max. VoIP calls limit");
                            }
                            showDialog(DIALOG_TOO_MANY_VOIP_CALLS);
                            return;
                        }
                    }
                }
            }
        }
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "Calling " + toNumber + " from " + fromNumber + " use Voip:" + mUseVoip);
        }

//        if (validation == VALID_EXTENSION) {
//            mUseVoip = false;
//        }
        
        if( mUseVoip && BUILD.VOIP_ENABLED) {
        	final RingCentralApp rcApp = (RingCentralApp)this.getApplication();
        	if( rcApp != null ){
        		FlurryTypes.onEvent(FlurryTypes.VOIP_OUT_WIFI, FlurryTypes.ORIGIN, FlurryTypes.RCM);

                rcApp.makeCallInit(toNumber, toName);
        		RCMProviderHelper.saveLastCallNumber(this, PhoneUtils.getContactPhoneNumber(toNumber).phoneNumber.canonical);
        		finish(RCMConstants.RINGOUT_RESULT_OK);
        	}        	
        	
        } else {
            Intent i;
            i = new Intent(this, RingOutCall.class);
            i.putExtra(RingOutCall.NO_DELAY, getIntent().getBooleanExtra(RingOutCall.NO_DELAY, false));
            i.putExtra(RCMConstants.EXTRA_CALL_TO_NUMBER, PhoneUtils.getNumRaw(toNumber));
            i.putExtra(RCMConstants.EXTRA_CALL_FROM_NUMBER, PhoneUtils.getNumRaw(fromNumber));
            i.putExtra(RCMConstants.EXTRA_CALL_TO_NAME, toName);
            startActivityForResult(i, RCMConstants.ACTIVITY_REQUEST_CODE_RINGOUT_CALL);
        }        
    }    
    
    @Override
    protected void onStart() {
    	super.onStart();
    	FlurryTypes.onStartSession(this);
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	FlurryTypes.onEndSession(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        finish(resultCode);
    }

    
    private int validateCall(String toNumber, String fromNumber) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "validateCall(toNumber = " + toNumber + ", fromNumber = " + fromNumber);
        }

        if (TextUtils.isEmpty(toNumber)) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "Empty TO number; validation failed");
            }
            showDialog(DIALOG_TO_NUMBER_INVALID_ID);
            return VALIDATION_FAILED;
        }

        PhoneNumberParser parser = PhoneUtils.getParser();
        
        PhoneNumber to_number_parsed = parser.parse(toNumber);
        if (to_number_parsed == null) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "Invalid TO number (1): " + toNumber + "; validation failed");
            }
            showDialog(DIALOG_TO_NUMBER_INVALID_ID);
            return VALIDATION_FAILED;
        }
            
        if (to_number_parsed.useDeviceForDial()) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "Special phone number: " +  toNumber + ". Starting native dialer.");
            }

            //Cannot use ACTION_CALL here as Android prohibits calling emergency numbers from 3rd party apps.
            //On Android 2.* ACTION_CALL for tel:911 acts as ACTION_DIAL (invokes native dialer).
            //On Android 1.5, 1.6 ACTION_CALL for tel:911 does not invoke dialer which would be confusing for user.
            RCMProviderHelper.saveLastCallNumber(this, to_number_parsed.canonical);
            startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", to_number_parsed.numRaw, null)));
            finish(RCMConstants.RINGOUT_RESULT_OK);
            return VALIDATION_FAILED;
        }

        if (TextUtils.isEmpty(fromNumber)) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "Empty FROM number; validation failed");
            }
            showDialog(DIALOG_FROM_NUMBER_INVALID_ID);
            return VALIDATION_FAILED;
        }
        
        Cursor cursor = getContentResolver().query(
                UriHelper.getUri(RCMProvider.ACCOUNT_INFO, RCMProviderHelper.getCurrentMailboxId(this)),
                new String[] {
                    AccountInfoTable.MAILBOX_ID,
                    AccountInfoTable.JEDI_ACCOUNT_NUMBER,
                    AccountInfoTable.JEDI_FREE,
                    AccountInfoTable.JEDI_TIER_SETTINGS },
                null, null, null);
        
        if (cursor == null || cursor.getCount() == 0) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "No account data; validation failed");
            }
            if (cursor != null) {
                cursor.close();
            }
            
            return VALIDATION_FAILED;
        }
        
        cursor.moveToFirst();
        String account_number = cursor.getString(cursor.getColumnIndex(AccountInfoTable.JEDI_ACCOUNT_NUMBER));
        long tier_settings = cursor.getLong(cursor.getColumnIndex(AccountInfoTable.JEDI_TIER_SETTINGS));
        boolean account_free = (cursor.getInt(cursor.getColumnIndex(AccountInfoTable.JEDI_FREE)) != 0);
        cursor.close();
        
        int extensionType = UserInfo.getUserType(CallValidator.this);
        if (extensionType == UserInfo.TAKE_MESSAGES_ONLY || extensionType == UserInfo.ANNOUNCEMENTS_ONLY || extensionType == UserInfo.DEPARTMENT ||
                (tier_settings & RCMConstants.TIERS_PHS_DIAL_FROM_CLIENT) == 0) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "Calling prohibited from this account; validation failed");
            }
            showDialog(DIALOG_NO_PERMISSIONS_ID);
            return VALIDATION_FAILED;
        }

        PhoneNumber account_number_parsed = parser.parse(account_number);

        if (account_number_parsed.isUSAOrCanada && (!to_number_parsed.isValid || !to_number_parsed.isUSAValid)) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "Invalid TO number (2): " + toNumber + "; validation failed");
            }
            showDialog(DIALOG_TO_NUMBER_INVALID_ID);
            return VALIDATION_FAILED;
        }

        if (to_number_parsed.isShortNumber) {
            if (!isShortNumberValid(to_number_parsed, tier_settings)) {
                showDialog(DIALOG_EXTENSION_NOT_FOUND_ID);
                return VALIDATION_FAILED;
            }

            if (LogSettings.MARKET) {
                MktLog.i(TAG, "Call validate OK: extension");
            }
            return VALID_EXTENSION;
        }
    
        if (((tier_settings & RCMConstants.TIERS_PHS_INTERNATIONAL_CALLING) == 0)
                && account_number_parsed.isInternationalCallTo(to_number_parsed)) {
            if (account_free == true) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "Free account: international calls prohibited; validation failed");
                }
                showDialog(DIALOG_NO_FREE_INTERNATIONAL_ID);
            } else {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "International calls prohibited from this account; validation failed");
                }
                showDialog(DIALOG_NO_INTERNATIONAL_ID);
            }
            return VALIDATION_FAILED;
        }
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "Call validate OK: normal number");
        }
        return VALID_NUMBER;
    }

    
    private boolean isShortNumberValid(PhoneNumber phoneNumber, long tierSettings) {
        if((tierSettings & RCMConstants.TIERS_PHS_CAN_VIEW_EXTENSIONS) == 0) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "Extensions hidden for this account; validation failed");
            }
            return false;
        }
        
        return checkExtension(phoneNumber.numRaw);
    }


    private boolean checkExtension(String extNum) {
        Cursor c = getContentResolver().query(
                UriHelper.getUri(RCMProvider.EXTENSIONS, RCMProviderHelper.getCurrentMailboxId(this)),
                new String[]{ExtensionsTable.MAILBOX_ID, ExtensionsTable.JEDI_PIN},
                ExtensionsTable.JEDI_PIN + "=?",
                new String[]{extNum},
                null);

        boolean result = (c.getCount() > 0);    
        c.close();
        
        if (LogSettings.MARKET) {
            MktLog.e(TAG, "No such extension: " + extNum + "; validation failed");
        }
        return result;
    }
    

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DIALOG_TO_NUMBER_INVALID_ID:
            return alertDialog(R.string.call_error_title_invalid_phone_number, R.string.call_cannotbe_completed);
        case DIALOG_FROM_NUMBER_INVALID_ID:
            return alertDialog(R.string.call_error_title_invalid_call, R.string.enter_number_in_settings);
        case DIALOG_NO_PERMISSIONS_ID:
            return alertDialog(R.string.call_error_title_call_error, R.string.no_permissions);
        case DIALOG_EXTENSION_NOT_FOUND_ID:
            return alertDialog(R.string.call_error_title_wrong_extension, R.string.extension_not_found);
        case DIALOG_NO_INTERNATIONAL_ID:
            return alertDialog(R.string.call_error_title_call_error, R.string.no_international_calls);
        case DIALOG_NO_FREE_INTERNATIONAL_ID:
            return alertDialog(R.string.call_error_title_call_error, R.string.no_international_for_free_or_trials);
        case DIALOG_TOO_MANY_VOIP_CALLS:
            return alertDialog(R.string.call_error_title_too_many_calls, R.string.too_many_calls);
        default:
            return super.onCreateDialog(id);
        }
    }

    private AlertDialog alertDialog(int title, int message) {
        return RcAlertDialog.getBuilder(this)
        .setTitle(title)
        .setMessage(message)
        .setIcon(R.drawable.symbol_error)
        .setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                finish(RCMConstants.RINGOUT_RESULT_FAIL);
            }
        })
        .setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish(RCMConstants.RINGOUT_RESULT_FAIL);
            }
        })
        .create();
    }
    

    /**
     * Set result and finish Activity
     */
    private void finish(int resultCode) {
        if (LogSettings.ENGINEERING && DEBUG) {
            EngLog.d(TAG, "finish(" + resultCode + ")...");
        }
        
        setResult(resultCode);
        finish();
    }

    
}
