package com.ringcentral.android.ringout;

import java.util.HashMap;

import com.ringcentral.android.R;

public class CallStatusStrings {

    public static final String JEDI_RINGOUT_CALL_STATE_SUCCESS = "csSuccess";
    public static final String JEDI_RINGOUT_CALL_STATE_IN_PROGRESS = "csInProgress";
    public static final String JEDI_RINGOUT_CALL_STATE_CANNOT_REACH = "csCannotReach";
    public static final String JEDI_RINGOUT_CALL_STATE_ERROR = "csError";
    public static final String JEDI_RINGOUT_CALL_STATE_INVALID = "csInvalid";
    public static final String JEDI_RINGOUT_CALL_STATE_NO_ANSWERING_MACHINE = "csNoAnweringMachine";
    public static final String JEDI_RINGOUT_CALL_STATE_NO_SESSION_FOUND = "csNoSessionFound";

    public static final String JEDI_RINGOUT_PARTY_STATUS_INVALID = "eInvalid";
    public static final String JEDI_RINGOUT_PARTY_STATUS_SUCCESS = "eSuccess";
    public static final String JEDI_RINGOUT_PARTY_STATUS_IN_PROGRESS = "eInProgress";
    public static final String JEDI_RINGOUT_PARTY_STATUS_BUSY = "eBusy";
    public static final String JEDI_RINGOUT_PARTY_STATUS_NO_ANSWER = "eNoAnswer";
    public static final String JEDI_RINGOUT_PARTY_STATUS_REJECTED = "eRejected";
    public static final String JEDI_RINGOUT_PARTY_STATUS_GENERIC_ERROR = "eGenericError";
    public static final String JEDI_RINGOUT_PARTY_STATUS_FINISHED = "eFinished";
    public static final String JEDI_RINGOUT_PARTY_STATUS_INTERNATIONAL_DISABLED = "eInternationalDisabled";
    public static final String JEDI_RINGOUT_PARTY_STATUS_DESTINATION_BLOCKED = "eDestinationBlocked";
    public static final String JEDI_RINGOUT_PARTY_STATUS_NOT_ENOUGH_FUNDS = "eNotEnoughFunds";
    public static final String JEDI_RINGOUT_PARTY_STATUS_NO_SUCH_USER = "eNoSuchUser";

    
    static HashMap<String, Integer> sUserStatusStrings  = new HashMap<String, Integer>(); 
    static HashMap<String, Integer> sPartyStatusStrings = new HashMap<String, Integer>(); 

    static {
        sUserStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_IN_PROGRESS,               R.string.ringout_userStatus_eInProgress);
        sUserStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_BUSY,                      R.string.ringout_userStatus_eBusy);
        sUserStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_NO_ANSWER,                 R.string.ringout_userStatus_eNoAnswer);
        sUserStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_REJECTED,                  R.string.ringout_userStatus_eRejected);
        sUserStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_GENERIC_ERROR,             R.string.ringout_userStatus_eGenericError);
        sUserStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_FINISHED,                  R.string.ringout_userStatus_eFinished);
        sUserStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_INTERNATIONAL_DISABLED,    R.string.ringout_userStatus_eInternationalDisabled);
        sUserStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_DESTINATION_BLOCKED,       R.string.ringout_userStatus_eDestinationBlocked);
        sUserStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_NOT_ENOUGH_FUNDS,          R.string.ringout_userStatus_eNotEnoughFunds);

        sPartyStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_IN_PROGRESS,              R.string.ringout_partyStatus_eInProgress);
        sPartyStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_BUSY,                     R.string.ringout_partyStatus_eBusy);
        sPartyStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_NO_ANSWER,                R.string.ringout_partyStatus_eNoAnswer);
        sPartyStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_REJECTED,                 R.string.ringout_partyStatus_eRejected);
        sPartyStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_GENERIC_ERROR,            R.string.ringout_partyStatus_eGenericError);
        sPartyStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_INTERNATIONAL_DISABLED,   R.string.ringout_partyStatus_eInternationalDisabled);
        sPartyStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_DESTINATION_BLOCKED,      R.string.ringout_partyStatus_eDestinationBlocked);
        sPartyStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_NOT_ENOUGH_FUNDS,         R.string.ringout_partyStatus_eNotEnoughFunds);
        sPartyStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_FINISHED,                 R.string.ringout_partyStatus_eFinished);
        sPartyStatusStrings.put(JEDI_RINGOUT_PARTY_STATUS_SUCCESS,                  R.string.ringout_partyStatus_eSuccess);
    }

}
