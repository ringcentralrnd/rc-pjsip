/** 
 * Copyright (C) 2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.ringout;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.LoginScreen;
import com.ringcentral.android.RingCentral;

/**
 * 
<!-- 
        DO NOT ENABLE THIS ACTIVITY
        Functionality below is responsible for providing RingCentral as a caller 
        in native menus(http://jira.ringcentral.com/browse/AB-6). Currently disabled as there is no way to 
        initiate emergency calls properly, when RCM is set as default dialer by user, refer (http://jira.ringcentral.com/browse/AB-542) 
        for details.        
         -->
 *
 */

public class OutgoingCallLauncher extends Activity{
	private static final String TAG = "[RC]OutgoingCallLauncher";

	//for now let it e disabled
	private static Boolean mCallerEnabled;
	
	private static String mRONumber; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		
		final Intent intent = getIntent();
		
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "Got intent : " + intent);			
		}
		
		String number = PhoneNumberUtils.getNumberFromIntent(intent, this);
		
		if (number != null) {
			number = PhoneNumberUtils.convertKeypadLettersToDigits(number);
			number = PhoneNumberUtils.stripSeparators(number);
        }
		
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "Comapre mRONumber : " + mRONumber + " intent number : " + number);			
		}
		
		if(PhoneNumberUtils.compare(mRONumber, number)){
			if(LogSettings.ENGINEERING){
				EngLog.d(TAG, "received intent second time.. ignoring ");
			}
			mRONumber = null;			
			finish();
		}
		
		final boolean isAlive = RingCentral.isAlive();
		
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "received number : " + number + " isAlive : " + isAlive);
		}
		
		if(!TextUtils.isEmpty(number)){
			if(isAlive){
				RingOutAgent roAgent = new RingOutAgent(this);
				roAgent.call(number, null, true);
			}else{
				//should pass through LoginScreen		
				final Intent callIntent = new Intent(this, LoginScreen.class);
				callIntent.putExtra(RingOut.PHONE_NUMBER, number);
				startActivity(callIntent);				
			}
		}			
		finish();
	}
	
	
	/**
	 * Method used to enable/disable OutgoingCallLauncher component and ability to select
	 * RCM as a dialer in native menu's
	 * NOTE app get killed on most devices, so ignore this
	 * Currently unused
	 * @param enabled true/false, enabled or disabled state
	 */
	public static void setEnabled(Context context, boolean enabled){		
		final ComponentName component = new ComponentName(context, OutgoingCallLauncher.class);
		final PackageManager pm = context.getPackageManager();
		
		try {
			if (LogSettings.MARKET) {
				MktLog.d(TAG, "setEnabled : " + enabled + " current status, mCallerEnabled : " + mCallerEnabled);
			}			
			
			if (mCallerEnabled == null) {						
				mCallerEnabled = new Boolean((pm.getComponentEnabledSetting(component) == PackageManager.COMPONENT_ENABLED_STATE_ENABLED));
			}
			if (mCallerEnabled.booleanValue() != enabled) {		
				pm.setComponentEnabledSetting(component, 
										  enabled ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED : 
													PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
				mCallerEnabled = enabled;
			}						
		} catch (Exception e) {
			if (LogSettings.MARKET) {
				MktLog.e(TAG, "Exception", e);
			}
			mCallerEnabled = null;
			try {				
				pm.setComponentEnabledSetting(component, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);								 
			} catch (Exception e1) {
				if (LogSettings.MARKET) {
					MktLog.e(TAG, "Ignored...", e1);
				}
			}
		}
	}
	
	
	public static void setROStarted(String number){
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "ROStarted number : " + number + " current : " + mRONumber);
		}
		if((mRONumber != null) &&  !mRONumber.equals(number)){
			mRONumber = number;
		}
	}

}
