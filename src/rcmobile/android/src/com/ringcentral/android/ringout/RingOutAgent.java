/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.ringout;

import android.app.Activity;
import android.content.Intent;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.utils.PhoneUtils;

public class RingOutAgent {

    private static final String TAG = "[RC]RingOutAgent";

    private Activity mActivity;
    private RingCentralApp mApp;


    public RingOutAgent(Activity activity) {
        mActivity = activity;
        mApp = (RingCentralApp) activity.getApplication();
    }

    
    public void call(String toNumber, String toName) {
        call(toNumber, toName, false);
    }    
    
    /*
     * nodelay means start one leg ringout without waiting 1.5 sec for connecting to server
     */
    public void call(String toNumber, String toName, boolean nodelay) {
    	if (LogSettings.MARKET) {
            MktLog.i(TAG, "Calling :" + toNumber + " name :" + toName + " nodelay :" + nodelay);
        }
        
        if (mActivity != null && !mActivity.isFinishing()) {    //PP-2096
            Intent i = new Intent(mApp, CallValidator.class);
            i.putExtra(RCMConstants.EXTRA_CALL_TO_NUMBER, PhoneUtils.getNumRaw(toNumber));            
            i.putExtra(RCMConstants.EXTRA_CALL_TO_NAME, toName);            
            i.putExtra(RingOutCall.NO_DELAY, nodelay);
            mActivity.startActivityForResult(i, RCMConstants.ACTIVITY_REQUEST_CODE_RINGOUT_CALL);
        }
    }

}

