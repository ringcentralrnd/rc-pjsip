/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.ringout;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.PhoneNumberUtils;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.api.RequestInfoStorage;
import com.ringcentral.android.api.network.NetworkManager;
import com.ringcentral.android.api.network.NetworkManagerNotifier;
import com.ringcentral.android.api.pojo.BasePOJO;
import com.ringcentral.android.api.pojo.SessionStatusPOJO;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.SyncStatusEnum;
import com.ringcentral.android.settings.CallerIDDialog;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.DeviceUtils;
import com.ringcentral.android.utils.NetworkUtils;
import com.ringcentral.android.utils.PhoneUtils;
import com.ringcentral.android.utils.NetworkUtils.NetworkState;


public class RingOutCall extends Activity implements NetworkManagerNotifier {

    private static final String TAG = "[RC]ROCall";
    private static final boolean DEBUG = false;
    
    public static final String NO_DELAY = "NO_DELAY"; 

    private static final String JEDI_PARAM_RINGOUT_STATISTIC_FROM_TYPE = "KeyPad";
    private static final String JEDI_PARAM_RINGOUT_STATISTIC_TO_TYPE = "Main";
    private static final String JEDI_PARAM_RING_OUT_TYPE = "fromMobile";
    private static final String JEDI_PARAM_BLOCKLIST = "0";      //blocklist not checked

    private static final long NO_SESSION_ID = -1;
    
    private static final int DIALOG_CONNECTION_ERROR_ID = 1;
    private static final int DIALOG_CALL_FAIL_ID = 2;
    private static final int DIALOG_CALL_FAIL_TRY_AGAIN_ID = 3;
    private static final int DIALOG_INVALID_CALLER_ID = 4;
    
    private static final int MSG_SHOW_DELAYED_STATUS_SCREEN = 1;
    private static final int MSG_CLOSE_STATUS_SCREEN = 2;
    private static final int MSG_REQUEST_CALL_STATUS = 3;
    
    private static final long STATUS_SCREEN_SHOW_DELAY_AT_CALL_START = 1500; //1.5 sec
    private static final long STATUS_SCREEN_CLOSE_DELAY_AFTER_CALL_SUCCESS = 5000; //5 sec
    private static final long STATUS_SCREEN_CLOSE_DELAY_AFTER_CALL_FAIL = 10000; //10 sec
    private static final long REQUEST_CALL_STATUS_INTERVAL = 1000; //1 sec


    /*
     * This listener is declared static as we want to receive Phone state notifications
     * during the whole native call life cycle when doing Direct ReingOut,
     * even after this Activity is finished
     * (just for logging purpose; required by PP-2359)
     */
    private static OneLegPhoneStateListener sOneLegPhoneStateListener;
    
    /*
     * This listener is non-static as it manages the Activity lifecycle
     * during Classic (two-leg) RingOut call.
     */
    private TwoLegPhoneStateListener mTwoLegPhoneStateListener;

    private boolean mIsVisible;
    private int mDisplayOrientation;

    private String mToNumber;
    private String mFromNumber;
    private String mCallerID;
    
    private int mRingOutMode;
    private boolean mTwoLegCallOnSelf; 
    
    private Button mBtnEndCall;
    private boolean mBtnEndCallEnabled;

    private String mCallStatusText;
    private TextView mCallStatusLabel;
    private int mCallStatusLabelVisibility;

    private boolean mBackKeyDisabled;
    
    private long mSessionId;

    private NetworkManagerNotifierHandler mNotifierHandler;
    private Message mNotifierMessage;

    private TimeoutHandler mTimeoutHandler; 
    
//-------------------------------------------------------------------

    /* Overridden Activity class methods */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (LogSettings.ENGINEERING && DEBUG) {
            EngLog.d(TAG, "onCreate()..." + (savedInstanceState == null ?
                    "savedInstanceState==null" : "savedInstanceState!=null"));
        }
        
        if (savedInstanceState != null) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "onCreate(): Activity re-creation forbidden; FINISH");
            }
            finish();
            return;
        }
        
        mToNumber = getIntent().getStringExtra(RCMConstants.EXTRA_CALL_TO_NUMBER);
        mFromNumber = getIntent().getStringExtra(RCMConstants.EXTRA_CALL_FROM_NUMBER);

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onCreate(): TO = " + mToNumber + ", FROM = " + mFromNumber);
        }
        
        if (TextUtils.isEmpty(mToNumber) || TextUtils.isEmpty(mFromNumber)) {
            finish();
            return;
        }                
        
        mCallerID = PhoneUtils.getNumRaw(RCMProviderHelper.getCallerID(this));
        mRingOutMode = RCMProviderHelper.getRingoutMode(this);
        mBtnEndCallEnabled = false;
        mIsVisible = (mRingOutMode == RCMConstants.RINGOUT_MODE_MY_ANDROID ? getIntent().getBooleanExtra(NO_DELAY, false) : true);
        mCallStatusText = "";
        mBackKeyDisabled = true;
        mSessionId = NO_SESSION_ID;
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        buildLayout(DeviceUtils.getDisplayOrientation(this));
        setDefaultKeyMode(DEFAULT_KEYS_DISABLE);

        mNotifierHandler = new NetworkManagerNotifierHandler();
        mTimeoutHandler = new TimeoutHandler();
        
        sOneLegPhoneStateListener = null;
        mTwoLegPhoneStateListener = null;
        
        if (mRingOutMode == RCMConstants.RINGOUT_MODE_ANOTHER_PHONE
                && PhoneUtils.equals(RCMProviderHelper.getRingoutAnotherPhone(RingOutCall.this),
                        DeviceUtils.getDeviceNumber(RingOutCall.this))) {
            
            mTwoLegCallOnSelf = true;
        } else {
            mTwoLegCallOnSelf = false;
        }

        if (LogSettings.ENGINEERING && DEBUG) {
            EngLog.d(TAG, "onCreate(): mTwoLegCallOnSelf = " + mTwoLegCallOnSelf);
        }
        
        if (mRingOutMode == RCMConstants.RINGOUT_MODE_ANOTHER_PHONE) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "onCreate(): mode - ANOTHER PHONE");
            }
            twoLegRingOutCall(this, mFromNumber, mToNumber);
        } else {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "onCreate(): mode - MY ANDROID");
            }
            oneLegRingOutCall(this, mToNumber);
        }
        
        FlurryTypes.onEvent(FlurryTypes.EVENT_RINGOUT_CALL, FlurryTypes.EVENT_RINGOUT_CALL_TYPE, 
        		(mRingOutMode == RCMConstants.RINGOUT_MODE_ANOTHER_PHONE)?
        				FlurryTypes.RINGOUT_CLASSIC :
        				FlurryTypes.RINGOUT_DIRECT);
        
        
    
        if (!mIsVisible) {
            mTimeoutHandler.sendEmptyMessageDelayed(MSG_SHOW_DELAYED_STATUS_SCREEN, STATUS_SCREEN_SHOW_DELAY_AT_CALL_START);
        }
    }

    
    @Override
    protected void onRestart() {
        super.onRestart();

        //onConfigurationChanged() is not always called when the Activity becomes visible (Android bug?)
        //That's why display orientation is checked here
        int orientation = DeviceUtils.getDisplayOrientation(this);
        if (orientation != mDisplayOrientation) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "onRestart(): orientation changed: " + orientation);
            }
            buildLayout(orientation);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryTypes.onStartSession(this);
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        FlurryTypes.onEndSession(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        int orientation = newConfig.orientation;
        
        if (orientation == Configuration.ORIENTATION_UNDEFINED) {
            orientation = DeviceUtils.calcDisplayOrientation(this);
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "onConfigurationChanged(): ORIENTATION_UNDEFINED; calculated orientation = " + orientation);
            }
        }

        if (orientation != mDisplayOrientation) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "configurationChanged(): orientation changed; rebuild");
            }
            buildLayout(orientation);
        } else {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "configurationChanged(): orientation not changed");
            }
        }

    }

    
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        default:
            return super.onCreateDialog(id);
       
        case DIALOG_CONNECTION_ERROR_ID:
            return RcAlertDialog.getBuilder(this)
                .setTitle(R.string.ringout_alert_title_call_failed)
                .setMessage(R.string.ringout_alert_msg_connection_error)
                .setIcon(R.drawable.symbol_error)
                .setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish(RCMConstants.RINGOUT_RESULT_FAIL);
                    }
                })
                .create();

        case DIALOG_CALL_FAIL_ID:
            return RcAlertDialog.getBuilder(this)
                .setTitle(R.string.ringout_alert_title_call_failed)
                .setMessage(R.string.ringout_alert_msg_call_fail)
                .setIcon(R.drawable.symbol_error)
                .setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish(RCMConstants.RINGOUT_RESULT_FAIL);
                    }
                })
                .create();

        case DIALOG_CALL_FAIL_TRY_AGAIN_ID:
            return RcAlertDialog.getBuilder(this)
                .setTitle(R.string.ringout_alert_title_call_failed)
                .setMessage(R.string.ringout_alert_msg_call_fail_try_again)
                .setIcon(R.drawable.symbol_error)
                .setPositiveButton(R.string.dialog_btn_retry, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        oneLegRingOutCall(RingOutCall.this, mToNumber);
                    }
                })
                .setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish(RCMConstants.RINGOUT_RESULT_FAIL);
                    }
                })
                .create();

        case DIALOG_INVALID_CALLER_ID:
            return RcAlertDialog.getBuilder(this)
                .setTitle(R.string.ringout_alert_title_invalid_callerID)
                .setMessage(String.format(getString(R.string.ringout_alert_msg_invalid_callerID), PhoneUtils.getLocalCanonical(mCallerID)))
                .setIcon(R.drawable.symbol_error)
                .setPositiveButton(R.string.ringout_alert_btn_choose, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        final CallerIDDialog callerIDDialog = new CallerIDDialog(RingOutCall.this);
                        callerIDDialog.setOnDismiss(new OnDismissListener() {
                            public void onDismiss(DialogInterface dialog) {
                                callerIDDialog.onDismiss(dialog);
                                finish(RCMConstants.RINGOUT_RESULT_FAIL);
                            }
                        });
                        callerIDDialog.show();
                    }
                })
                .setNegativeButton(R.string.ringout_alert_btn_main, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        RCMProviderHelper.saveCallerID(RingOutCall.this, RCMProviderHelper.getMainNumber(RingOutCall.this));
                        finish(RCMConstants.RINGOUT_RESULT_FAIL);
                    }
                })
                .create();
        }
    }    
        

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        mIsVisible = visible;
    }
    

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
        case KeyEvent.KEYCODE_SEARCH:
            return true;
        case KeyEvent.KEYCODE_BACK:
            if (mBackKeyDisabled) {
                return true;
            }
            break;
        }

        return super.onKeyDown(keyCode, event);
    }

    
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
        case KeyEvent.KEYCODE_SEARCH:
            return true;
        case KeyEvent.KEYCODE_BACK:
            if (mBackKeyDisabled) {
                return true;
            }
            break;
        }

        return super.onKeyUp(keyCode, event);
    }

//-------------------------------------------------------------------

    /**
     * Set result and finish Activity
     */
    private void finish(int resultCode) {
        finish(resultCode, 0);
    }
    

    /**
     * Set result and finish Activity
     */
    private void finish(int resultCode, long delay) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "finish(resultCode = " + resultCode + ", delay = " + delay + "ms)...");
        }
        
        if (resultCode == RCMConstants.RINGOUT_RESULT_OK || resultCode == RCMConstants.RINGOUT_RESULT_CANCELLED) {
            RCMProviderHelper.saveLastCallNumber(this, PhoneUtils.getContactPhoneNumber(mToNumber).phoneNumber.canonical);
        }
        
        setResult(resultCode);
        if (delay > 0) {
            mTimeoutHandler.sendEmptyMessageDelayed(MSG_CLOSE_STATUS_SCREEN, delay);
        } else {
            finish();
        }
    }

    
//-------------------------------------------------------------------

    /**
     * Build Layout View according to orientation
     */
    private void buildLayout(int orientation) {
        mDisplayOrientation = orientation;

        setContentView(orientation == Configuration.ORIENTATION_LANDSCAPE ?
                R.layout.ringout_status_landscape               //LANDSCAPE
                : R.layout.ringout_status_portrait);            //PORTRAIT or SQUARE

        setVisible(mIsVisible);

        if (mRingOutMode == RCMConstants.RINGOUT_MODE_MY_ANDROID) {
            ((TextView)findViewById(R.id.ringout_status_mid_hint)).setVisibility(View.GONE);
        }
        
        ((TextView)findViewById(R.id.ringout_status_top_number)).setText(PhoneUtils.getLocalCanonical(mToNumber));
        ((TextView)findViewById(R.id.ringout_status_mid_callerID)).setText(PhoneUtils.getLocalCanonical(mCallerID));

        mCallStatusLabel = (TextView)findViewById(R.id.ringout_status_call_state);
        setCallStateLabel(mCallStatusText);
        
        mBtnEndCall = (Button)findViewById(R.id.ringout_btn_end_call);
        mBtnEndCall.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                if (mRingOutMode == RCMConstants.RINGOUT_MODE_ANOTHER_PHONE) {
                    mTimeoutHandler.removeMessages(MSG_REQUEST_CALL_STATUS);
                    cancelCall(RingOutCall.this);
                } else {
                    mTimeoutHandler.removeMessages(MSG_SHOW_DELAYED_STATUS_SCREEN);
                }

                finish(RCMConstants.RINGOUT_RESULT_CANCELLED);
            }
        });
        mBtnEndCall.setEnabled(mBtnEndCallEnabled);
    }

//-------------------------------------------------------------------

   private void oneLegRingOutCall(Context context, String toNumber) {
       String deviceCallerId = PhoneUtils.getNumRaw(DeviceUtils.getDeviceNumber(this));
       
       if (LogSettings.MARKET) {
           MktLog.i(TAG, "Start Direct RingOut call:\n"
                   + "\t\t calledNumber: " + toNumber + "\n"
                   + "\t\t deviceCallerId: " + deviceCallerId + "\n"
                   + "\t\t fromType: " + JEDI_PARAM_RINGOUT_STATISTIC_FROM_TYPE + "\n"
                   + "\t\t SCallerID: " + mCallerID + "\n"
                   + "\t\t toType: " + JEDI_PARAM_RINGOUT_STATISTIC_TO_TYPE);
       }
       
       NetworkManager.getInstance().callDirectRingOut(
                context,                                                        //Context            
                toNumber,                                                       //JEDI callRequest: calledNumber 
                deviceCallerId,                                                 //JEDI callRequest: deviceCallerID
                JEDI_PARAM_RINGOUT_STATISTIC_FROM_TYPE,                         //JEDI callRequest: fromType
                mCallerID,                                                      //JEDI callRequest: SCallerID
                JEDI_PARAM_RINGOUT_STATISTIC_TO_TYPE,                           //JEDI callRequest: toType
                true);                                                          //NetworkManager: backgroundTask

       mBtnEndCall.setEnabled(true);
       mBtnEndCallEnabled = true;

       setCallStateLabel((String)getText(R.string.ringout_status_call_initiated));
    }
   
    @SuppressWarnings("unused")
	private void oneLegRingOutResponseHandler(Message msg) {
        if (mRingOutMode != RCMConstants.RINGOUT_MODE_MY_ANDROID) {
            if (LogSettings.QA) {
                QaLog.e(TAG, "oneLegRingOutResponseHandler(): mode mis-match");
                throw new RuntimeException("oneLegRingOutResponseHandler(): mode mis-match");
            }
            return;
        }

        
        mBtnEndCall.setEnabled(false);
        mBtnEndCallEnabled = false;
        mTimeoutHandler.removeMessages(MSG_SHOW_DELAYED_STATUS_SCREEN);

        Bundle data = msg.getData();
        int request_status = data.getInt(NetworkManager.REQUEST_INFO.STATUS);
        int error_code = data.getInt(NetworkManager.REQUEST_INFO.ERROR_CODE);
        String error_message = data.getString(NetworkManager.REQUEST_INFO.MESSAGE_SYSTEM);

        if (LogSettings.MARKET) {
            if (!LogSettings.QA) {
                if (request_status == SyncStatusEnum.SYNC_STATUS_ERROR) {
                    MktLog.e(TAG, "NetErr: code = " + error_code + ", msg = \"" + error_message + '\"');
                }            
            } else {
                if (request_status == SyncStatusEnum.SYNC_STATUS_ERROR) {
                    QaLog.e(TAG, "oneLegRingOutResponseHandler(): Network error:\n"
                            + "\t\t Error code: " + error_code + '\n'
                            + "\t\t Error message: " + error_message);
                } else {
                    QaLog.i(TAG, "oneLegRingOutResponseHandler(): Network success");
                }
            }
        }        

        if (request_status != SyncStatusEnum.SYNC_STATUS_ERROR) {
            //String account_number = RCMProviderHelper.getAccountNumber(this);
            String login_number = RCMProviderHelper.getLoginNumber(this);   //PP-2806: use login number instead of main account number 
            
            if (LogSettings.MARKET) {
                //MktLog.i(TAG, "oneLegRingOutResponseHandler(): Start NATIVE CALL to the account number " + account_number);
                MktLog.i(TAG, "oneLegRingOutResponseHandler(): Start NATIVE CALL to the login number " + login_number);
            }
            
            sOneLegPhoneStateListener = new OneLegPhoneStateListener(this);
            sOneLegPhoneStateListener.enable(login_number);
            
            startActivity(new Intent(Intent.ACTION_CALL,
                    //Uri.fromParts("tel", "+" + account_number, null)));
                    Uri.fromParts("tel", "+" + login_number, null)));

            finish(RCMConstants.RINGOUT_RESULT_OK);
            return;
        }
       

        /* Call failed: status == RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR */

        if (!mIsVisible) {
            setVisible(true);
        }
       
        if (error_code == NetworkManager.ERROR_CODES.NETWORK_NOT_AVAILABLE
                || error_code == NetworkManager.ERROR_CODES.NETWORK_NOT_AVAILABLE_AIRPLANE_ON
                || msg.obj == null) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "oneLegRingOutResponseHandler(): Network not available");
            }
            showDialog(DIALOG_CONNECTION_ERROR_ID);
        } else {
            showDialog(DIALOG_CALL_FAIL_TRY_AGAIN_ID);
        }
   }
   
   
    private void twoLegRingOutCall(Context context, String from, String to) {
        String jedi_param_from = PhoneUtils.getNumRaw(from);
        String jedi_param_to = PhoneUtils.getNumRaw(to);
        boolean jedi_param_prompt = RCMProviderHelper.isConfirmConnection(this);
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "Start two-leg RingOut call:\n"
                    + "\t\t blocklist: " + JEDI_PARAM_BLOCKLIST + "\n"
                    + "\t\t clid: " + mCallerID + "\n"
                    + "\t\t from: " + jedi_param_from + "\n"
                    + "\t\t fromType: " + JEDI_PARAM_RINGOUT_STATISTIC_FROM_TYPE + "\n"
                    + "\t\t prompt: " + jedi_param_prompt + "\n"
                    + "\t\t to: " + jedi_param_to + "\n"
                    + "\t\t toType: " + JEDI_PARAM_RINGOUT_STATISTIC_TO_TYPE + "\n"
                    + "\t\t type: " + JEDI_PARAM_RING_OUT_TYPE);
        }
        
        NetworkManager.getInstance().callRingOut(
                context,                                              //Context                        
                JEDI_PARAM_BLOCKLIST,                                 //JEDI oneLegRingOutSessionRequest: blocklist             
                mCallerID,                                            //JEDI oneLegRingOutSessionRequest: clid           
                jedi_param_from,                                      //JEDI oneLegRingOutSessionRequest: from                 
                JEDI_PARAM_RINGOUT_STATISTIC_FROM_TYPE,               //JEDI oneLegRingOutSessionRequest: fromType                
                jedi_param_prompt,                                    //JEDI oneLegRingOutSessionRequest: prompt                   
                jedi_param_to,                                        //JEDI oneLegRingOutSessionRequest: to
                JEDI_PARAM_RINGOUT_STATISTIC_TO_TYPE,                 //JEDI oneLegRingOutSessionRequest: toType
                JEDI_PARAM_RING_OUT_TYPE,                             //JEDI oneLegRingOutSessionRequest: type
                true);                                                //NetworkManager: backgroundTask
    
        if (mTwoLegCallOnSelf) {
            mTwoLegPhoneStateListener = new TwoLegPhoneStateListener(this);
            mTwoLegPhoneStateListener.enable();
        }
    }


    private void twoLegRingOutResponseHandler(Message msg, boolean isFirstStatus) {
        if (mRingOutMode != RCMConstants.RINGOUT_MODE_ANOTHER_PHONE) {
            if (LogSettings.QA) {
                QaLog.e(TAG, "twoLegRingOutResponseHandler(): mode mis-match");
                throw new RuntimeException("twoLegRingOutResponseHandler(): mode mis-match");
            }
            return;
        }

        
        Bundle data = msg.getData();
        int request_status = data.getInt(NetworkManager.REQUEST_INFO.STATUS);
        int error_code = data.getInt(NetworkManager.REQUEST_INFO.ERROR_CODE);
        String error_message = data.getString(NetworkManager.REQUEST_INFO.MESSAGE_SYSTEM);
        
        if (LogSettings.MARKET) {
            if (!LogSettings.QA) {
                if (request_status == SyncStatusEnum.SYNC_STATUS_ERROR) {
                    MktLog.e(TAG, "NetErr: code = " + error_code + ", msg = \"" + error_message + '\"');
                }            
            } else {
                if (request_status == SyncStatusEnum.SYNC_STATUS_ERROR) {
                    QaLog.e(TAG, "twoLegRingOutResponseHandler(): Network error:\n"
                            + "\t\t Error code: " + error_code + '\n'
                            + "\t\t Error message: " + error_message);
                } else {
                    QaLog.i(TAG, "twoLegRingOutResponseHandler(): Network success");
                }
            }
        }        
        
        if ((request_status == SyncStatusEnum.SYNC_STATUS_ERROR
                && (error_code == NetworkManager.ERROR_CODES.NETWORK_NOT_AVAILABLE
                        || error_code == NetworkManager.ERROR_CODES.NETWORK_NOT_AVAILABLE_AIRPLANE_ON))
                || msg.obj == null) {       //Connection error

            if (LogSettings.MARKET) {
                MktLog.w(TAG, "twoLegRingOutResponseHandler(): Network not available");
            }

            if (isFirstStatus) {
                showDialog(DIALOG_CONNECTION_ERROR_ID);
            } else {
                showDialog(DIALOG_CALL_FAIL_ID);
            }
            return;
        }
        
        if (((BasePOJO) msg.obj).getSuccess() == false) {        //JEDI request failed
            if (error_code == NetworkManager.ERROR_CODES.CALLER_ID_IS_NOT_ALLOWED
                    || error_code == NetworkManager.ERROR_CODES.CALL_SESSION_ID_IS_NULL) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "twoLegRingOutResponseHandler(): Invalid Caller ID");
                }
                showDialog(DIALOG_INVALID_CALLER_ID);
            } else {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "twoLegRingOutResponseHandler(): Call failed");
                }
                showDialog(DIALOG_CALL_FAIL_ID);
            }
            return;
        }

        
        String callState = ((SessionStatusPOJO) msg.obj).getCallState();
        String userStatus = ((SessionStatusPOJO) msg.obj).getUserStatus();
        String partyStatus = ((SessionStatusPOJO) msg.obj).getPartyStatus();
        
        if (LogSettings.MARKET) {
            QaLog.i(TAG, "twoLegRingOutResponseHandler():\n"
                    + "\t\t callState = " + callState + '\n'
                    + "\t\t userStatus = " + userStatus + '\n'
                    + "\t\t partyStatus = " + partyStatus);
        }
        
        setCallStateLabel(getCallStatusText(callState, userStatus, partyStatus));
        
        if (callState.equals(CallStatusStrings.JEDI_RINGOUT_CALL_STATE_SUCCESS)) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "twoLegRingOutResponseHandler(): Call success");
            }
            
            mBtnEndCall.setEnabled(false);
            mBtnEndCallEnabled = false;
            mBackKeyDisabled = false;

            finish(RCMConstants.RINGOUT_RESULT_OK, STATUS_SCREEN_CLOSE_DELAY_AFTER_CALL_SUCCESS);
            return;
        }
        
        if (callState.equals(CallStatusStrings.JEDI_RINGOUT_CALL_STATE_ERROR)
                || callState.equals(CallStatusStrings.JEDI_RINGOUT_CALL_STATE_CANNOT_REACH)) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "twoLegRingOutResponseHandler(): Call error:\n"
                        + "\t\t callState = " + callState + '\n'
                        + "\t\t userStatus = " + userStatus + '\n'
                        + "\t\t partyStatus = " + partyStatus);
            }
            
            mBtnEndCall.setEnabled(false);
            mBtnEndCallEnabled = false;
            mBackKeyDisabled = false;

            finish(RCMConstants.RINGOUT_RESULT_FAIL, STATUS_SCREEN_CLOSE_DELAY_AFTER_CALL_FAIL);
            return;
        }
        
        if (!callState.equals(CallStatusStrings.JEDI_RINGOUT_CALL_STATE_IN_PROGRESS)) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "twoLegRingOutResponseHandler(): Unexpected call state: " + callState);
            }
            showDialog(DIALOG_CALL_FAIL_ID);
            return;
        }
        
        if (isFirstStatus) {
            mSessionId = ((SessionStatusPOJO) msg.obj).getSessionId();

            mBtnEndCall.setEnabled(true);
            mBtnEndCallEnabled = true;
        }
        
        boolean overGPRS = (mTwoLegCallOnSelf == true)
            && (NetworkUtils.getNetworkState(RingOutCall.this) == NetworkState.MOBILE)
            && NetworkUtils.isGPRSNetwork(RingOutCall.this);

        if (overGPRS) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "GPRS; stop status polling");
            }
            return;
        }

        mTimeoutHandler.sendEmptyMessageDelayed(MSG_REQUEST_CALL_STATUS, REQUEST_CALL_STATUS_INTERVAL);
    }
    
    
    private void requestTwoLegCallStatus() {
        NetworkManager.getInstance().getRingOutCallStatus(this, mSessionId, false, true);
    }


    private void cancelCall(Context context) {
        mTimeoutHandler.removeMessages(MSG_REQUEST_CALL_STATUS);
        NetworkManager.getInstance().cancelRingOut(context, mSessionId, true);
    }

    
    @Override   /* NetworkManagerNotifier.statusChanged() */
    public void statusChanged(Bundle status, Object result) {
        if (status.getInt(NetworkManager.REQUEST_INFO.TYPE) == RequestInfoStorage.DIRECT_RINGOUT
                || status.getInt(NetworkManager.REQUEST_INFO.TYPE) == RequestInfoStorage.RINGOUT_CALL
                || status.getInt(NetworkManager.REQUEST_INFO.TYPE) == RequestInfoStorage.CALL_STATUS) {
            mNotifierMessage = mNotifierHandler.obtainMessage();
            mNotifierMessage.setData(status);
            mNotifierMessage.obj = result;
            mNotifierHandler.sendMessage(mNotifierMessage);
        }
    }
    

    @Override   /* NetworkManagerNotifier.getNotifier() */
    public NetworkManagerNotifier getNotifier() {
        return this;
    }
  
    
    private String getCallStatusText(String callState, String userStatus, String partyStatus) {
        if (callState.equals(CallStatusStrings.JEDI_RINGOUT_CALL_STATE_SUCCESS)) {
            return (String)getText(R.string.ringout_callState_csSuccess);
        }
    
        if (callState.equals(CallStatusStrings.JEDI_RINGOUT_CALL_STATE_ERROR)) {
            return (String)getText(R.string.ringout_callState_csError);
        }
        
        if (!callState.equals(CallStatusStrings.JEDI_RINGOUT_CALL_STATE_IN_PROGRESS)
                && !callState.equals(CallStatusStrings.JEDI_RINGOUT_CALL_STATE_CANNOT_REACH)) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "getCallStatus(): Unexpected callState: " + callState);
            }
            return "";
        }
    
        Integer status_text_id;
        
        if (!userStatus.equals(CallStatusStrings.JEDI_RINGOUT_PARTY_STATUS_SUCCESS)) {
            status_text_id = CallStatusStrings.sUserStatusStrings.get(userStatus);
            if (status_text_id == null) {
                if (LogSettings.ENGINEERING) {
                    EngLog.e(TAG, "getCallStatus(): Unexpected userStatus: " + userStatus);
                }
                return "";
            }
        } else {
            status_text_id = CallStatusStrings.sPartyStatusStrings.get(partyStatus);
            if (status_text_id == null) {
                if (LogSettings.ENGINEERING) {
                    EngLog.e(TAG, "getCallStatus(): Unexpected partyStatus: " + partyStatus);
                }
                return "";
            }
        }

        return (String)getText(status_text_id);
    }


    private void setCallStateLabel(String text) {
        mCallStatusText = text;
        mCallStatusLabel.setText(text);
        
        mCallStatusLabelVisibility = TextUtils.isEmpty(text) ? View.GONE : View.VISIBLE;
        mCallStatusLabel.setVisibility(mCallStatusLabelVisibility);
    }
    
///////////////////////////////////////////////////////////////////////////////

    private class NetworkManagerNotifierHandler extends Handler {     

        @Override
        public void handleMessage(Message msg) {
            if (LogSettings.ENGINEERING && DEBUG) {
                EngLog.i(TAG, "handleMessage(): sync_status = " + msg.getData().getInt(NetworkManager.REQUEST_INFO.STATUS)
                        + ", object_type = " + msg.getData().getInt(NetworkManager.REQUEST_INFO.TYPE)
                        + ", error_code = " + msg.getData().getInt(NetworkManager.REQUEST_INFO.ERROR_CODE)
                        + ", message_system = " + msg.getData().getString(NetworkManager.REQUEST_INFO.MESSAGE_SYSTEM)
                        + ", server_type = " + msg.getData().getInt(NetworkManager.REQUEST_INFO.SERVER_TYPE)
                        + ", result = " + (msg.obj == null ? "null" : msg.obj.toString()));
                
            }

            if (isFinishing()) {
                if (LogSettings.ENGINEERING && DEBUG) {
                    EngLog.w(TAG, "handleMessage(): dead activity; do nothing");
                }
                return;
            }
            
            Bundle data = msg.getData();

            int sync_status = data.getInt(NetworkManager.REQUEST_INFO.STATUS);
            switch(sync_status) {
            case RCMDataStore.SyncStatusEnum.SYNC_STATUS_NOT_LOADED:
            case RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING:
                return;
            case RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED:
                break;
            case RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR:
                break;
            default:
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "handleMessage(): unknown status: " + sync_status);
                    throw new RuntimeException("handleMessage(): unknown status: " + sync_status);
                }
            }

            int request_type = data.getInt(NetworkManager.REQUEST_INFO.TYPE);
            switch(request_type) {
            case RequestInfoStorage.DIRECT_RINGOUT:
                oneLegRingOutResponseHandler(msg);
                break;
            case RequestInfoStorage.RINGOUT_CALL:
                twoLegRingOutResponseHandler(msg, true);
                break;
            case RequestInfoStorage.CALL_STATUS:
                twoLegRingOutResponseHandler(msg, false);
                break;
            }
        }
    }

///////////////////////////////////////////////////////////////////////////////

    private class TimeoutHandler extends Handler {
        
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) { 
            case MSG_SHOW_DELAYED_STATUS_SCREEN:
                if (!mIsVisible) {
                    setVisible(true);
                }
                return;
            
            case MSG_CLOSE_STATUS_SCREEN:
                if (!isFinishing()) {
                    finish();
                }
                return;

            case MSG_REQUEST_CALL_STATUS:
                requestTwoLegCallStatus();
                return;
            }
        }
    }

///////////////////////////////////////////////////////////////////////////////

    private static class OneLegPhoneStateListener extends RingOutPhoneStateListener {

        private boolean mOutgoingCallPerformed = false;

        private OneLegPhoneStateListener(Context context) {
            super(context);
        }
        
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "OneLegPhoneStateListener: state = " + stateAsString(state)
                        + "; incomingNumber = " + (TextUtils.isEmpty(incomingNumber) ? "NONE" : incomingNumber));
            }            
            if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                mOutgoingCallPerformed = true;
                OutgoingCallLauncher.setROStarted(null);
                return;
            }
        
            if (state == TelephonyManager.CALL_STATE_IDLE && mOutgoingCallPerformed) {
                disable();
                mOutgoingCallPerformed = false;
            }
        }
    
        @Override
        void enable() {
            mOutgoingCallPerformed = false;
            super.enable();
        }
        
        void enable(String ignoreNext){
        	OutgoingCallLauncher.setROStarted(ignoreNext);
        	enable();
        }
    
    }

    
    private class TwoLegPhoneStateListener extends RingOutPhoneStateListener {

        private TwoLegPhoneStateListener(Context context) {
            super(context);
        }

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "TwoLegPhoneStateListener: state = " + stateAsString(state)
                        + "; incomingNumber = " + (TextUtils.isEmpty(incomingNumber) ? "NONE" : incomingNumber)
                        + " [RO destination number: " + mToNumber + ']');
            }

            if (state == TelephonyManager.CALL_STATE_RINGING && PhoneNumberUtils.compare(incomingNumber, mToNumber)) {
                disable();

                if (RingOutCall.this != null) {
                    if (mTimeoutHandler != null) {
                        mTimeoutHandler.removeMessages(MSG_REQUEST_CALL_STATUS);
                    }
                    
                    if (!isFinishing()) {
                        if (LogSettings.MARKET) {
                            MktLog.d(TAG, "onCallStateChanged(): finish activity");
                        }
                        
                        finish(RCMConstants.RINGOUT_RESULT_OK);
                    }
                }
            }
        }
    
    }  

}   /* class RingOutCall */ 


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


abstract class RingOutPhoneStateListener extends PhoneStateListener {

    private TelephonyManager mTelephonyManager;

    RingOutPhoneStateListener(Context context) {
        mTelephonyManager = (TelephonyManager)(context.getSystemService(Context.TELEPHONY_SERVICE));
        mTelephonyManager.listen(this, LISTEN_NONE);
    }

    void enable() {
        mTelephonyManager.listen(this, LISTEN_CALL_STATE);
    }

    void disable() {
        mTelephonyManager.listen(this, LISTEN_NONE);
    }

    final String stateAsString(int state) {
        switch (state) {
        case TelephonyManager.CALL_STATE_IDLE:
            return "CALL_STATE_IDLE";
        case TelephonyManager.CALL_STATE_OFFHOOK:
            return "CALL_STATE_OFFHOOK";
        case TelephonyManager.CALL_STATE_RINGING:
            return "CALL_STATE_RINGING";
        default:
            return "UNKNOWN (" + state + ")";
        }
    }

}   /* class RingOutPhoneStateListener */
