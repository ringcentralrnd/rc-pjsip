/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.messages;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.widget.Toast;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.api.pojo.MessagePOJO;
import com.ringcentral.android.api.pojo.MessagePOJO.MessageType;
import com.ringcentral.android.contacts.CompanyFavorites;
import com.ringcentral.android.contacts.Cont;
import com.ringcentral.android.contacts.PersonalFavorites;
import com.ringcentral.android.contacts.PhoneSelectionDialog;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.contacts.ViewExtensionsActivity;
import com.ringcentral.android.contacts.Cont.acts.BindSync;
import com.ringcentral.android.contacts.Projections.Personal;
import com.ringcentral.android.messages.MessagesQuery.MessageItem;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.provider.RCMDataStore.MessagesTable;
import com.ringcentral.android.ringout.RingOutAgent;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.EmailSender;
import com.ringcentral.android.utils.FileUtils;
import com.ringcentral.android.utils.NetworkUtils;

public class MessageUtils {

    private static final String TAG = "[RC]MessageUtils";
	
	private static final int MENU_ITEM_VIEW_CONTACT = 80;


    public static String getMIMEType(String fileExt) {
        String type = null;
        if (fileExt.equals("mp3") || fileExt.equals("wav")) {
            type = "audio/*";
        }
        if (fileExt.equals("tiff") || fileExt.equals("tif")) {
            type = "image/*";
        }
        if (fileExt.equals("pdf")) {
            type = "application/pdf";
        }
        return type;
    }

    public static void viewFaxMessage(final MessageItem mi, final Context context) {
        String filePath = FileUtils.getPath(FileUtils.getSDPath(context), String.valueOf(mi.msgId), mi.fileExtension);
        String mimeType = MessageUtils.getMIMEType(mi.fileExtension);
        final Intent intent = new Intent();

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.content.Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(filePath)), mimeType);

        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "No PDF viewer detected warning message");
            }
            RcAlertDialog.getBuilder(context).setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).setTitle(R.string.PDF_viewer_could_not_be_detected_title).setMessage(R.string.PDF_viewer_could_not_be_detected).show();

            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "viewFaxMessage()", e);
            } else if (LogSettings.MARKET) {
                QaLog.e(TAG, "viewFaxMessage(): " + e.getMessage());
            }
        }
    }
    
    public static void userActionDeleteMsg(final Activity context, final long msgId, final boolean finishActivity) {
    	FlurryTypes.onEvent(FlurryTypes.EVENT_DELETE_MSG);
        if (NetworkUtils.isRCAvaliable(context)) {// OnLine mode
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "User: Delete: Network error dialog");
            }
            RcAlertDialog.getBuilder(context)
            	.setTitle(R.string.app_name)
            	.setMessage(R.string.msg_del_confirmation2)
            	.setIcon(R.drawable.symbol_exclamation)
            	.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            // Delete all item.
                            // Show new list
                            MessagesHandler.deleteMessage(context, msgId);
                            if(finishActivity)
                            	context.finish();
                        }
                    })
                .setNegativeButton(R.string.dialog_btn_cancel,new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    })
                .show();
        } else {
        	RcAlertDialog.getBuilder(context)
    	 	.setTitle(R.string.error_code_alert_title)
    	 	.setMessage(R.string.offline_mode_alerttext_delmsg)
    	 	.setIcon(R.drawable.symbol_exclamation)
    	 	.setPositiveButton(R.string.dialog_btn_ok, 
    	 		new DialogInterface.OnClickListener() {
                     public void onClick(DialogInterface dialog, int whichButton) {
                     }
                })
            .create()
            .show();
        }
    }

    public interface T {
    	public void t();
    }
    
    public static void userActionDeleteMsg(final Activity context, final long msgId, final T t) {
    	FlurryTypes.onEvent(FlurryTypes.EVENT_DELETE_MSG);
        if (NetworkUtils.isRCAvaliable(context)) {// OnLine mode
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "User: Delete: Network error dialog");
            }
            RcAlertDialog.getBuilder(context)
            	.setTitle(R.string.app_name)
            	.setMessage(R.string.msg_del_confirmation2)
            	.setIcon(R.drawable.symbol_exclamation)
            	.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            // Delete the message item.
                        	MessagesHandler.deleteMessage(context, msgId);
                        	MessagesHandler.clearMessage(context, msgId);
                        	t.t();
                        	
                        }
                    })
                .setNegativeButton(R.string.dialog_btn_cancel,new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    })
                .show();
        } else {
        	RcAlertDialog.getBuilder(context)
    	 	.setTitle(R.string.error_code_alert_title)
    	 	.setMessage(R.string.offline_mode_alerttext_delmsg)
    	 	.setIcon(R.drawable.symbol_exclamation)
    	 	.setPositiveButton(R.string.dialog_btn_ok, 
    	 		new DialogInterface.OnClickListener() {
                     public void onClick(DialogInterface dialog, int whichButton) {
                     }
                })
            .create()
            .show();
        }
    }
    
    public static void userActionClearMsg(Activity context, long msgId){
        MessagesHandler.clearMessage(context, msgId);
    }

    public static void userActionMarkMsg(Context context, long msgId) {
        if (NetworkUtils.isRCAvaliable(context)) {
            MessagesHandler.changeReadStatus(context, msgId);
        } else {
        	RcAlertDialog.getBuilder(context)
    		.setTitle(R.string.error_code_alert_title)
    		.setMessage(R.string.offline_mode_alerttext_markmsg)
    		.setIcon(R.drawable.symbol_exclamation)
            .setPositiveButton(R.string.dialog_btn_ok, 
            	new DialogInterface.OnClickListener() {
                   	public void onClick(DialogInterface dialog, int whichButton) {
                   	}
                }
            )
            .create()
            .show();        	
        }
    }

    public static void userActionForwardMSG(Context activity, MessageItem mi) {

        if (mi.syncStatus != RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED) {
            if (mi.locallyDeleted == 0) {
                RcAlertDialog.getBuilder(activity).setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setTitle(R.string.cannot_forward_title).setMessage(R.string.cannot_forward_message_is_not_loaded).show();
            } else {
                RcAlertDialog.getBuilder(activity).setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setTitle(R.string.cannot_forward_title).setMessage(R.string.cannot_forward_message_is_not_loaded_deleted).show();
            }
            return;
        }

        String filePath = FileUtils.getPath(FileUtils.getSDPath(activity), String.valueOf(mi.msgId), mi.fileExtension);

        String Title = "Fwd: " + mi.messageType.getLabel() + " from " + mi.displayName;
        //Empty body according to AB-987
        String body = "";

        if (LogSettings.MARKET) {
            MktLog.d(TAG, "Forward: Start email client");
        }

        String flurryEvent = null;
        if(mi.messageType.equals(MessageType.FAX)){
        	flurryEvent = FlurryTypes.FORWARD_FAX;
        }else if(mi.messageType.equals(MessageType.VOICE)){
        	flurryEvent = FlurryTypes.FORWARD_VOICEMAIL;
        }else{
        	flurryEvent = FlurryTypes.FORWARD_MESSAGE;
        }
        FlurryTypes.onEvent(FlurryTypes.EVENT_FORWARD, FlurryTypes.EVENT_FORWARD_PARAM, flurryEvent);
        
        EmailSender emailSender = new EmailSender(activity);
        emailSender.sendEmail(null, Title, body, filePath);
        Toast.makeText(activity, "Switching to Email client...", Toast.LENGTH_SHORT).show();
    }

    public static void callBack(Context activity, RingOutAgent ringOutAgent, MessageItem mi, Cursor mCursor) {
    	if (mi.isValidNumber) {
			if (mCursor.getLong(MessagesQuery.MESSAGES_BIND_HAS_CONTACT_INDX) > 0) {
				int phoneId = mCursor.getInt(MessagesQuery.MESSAGES_BIND_ID_INDX);
				Cont.acts.PhoneContact cf = Cont.acts().lookUpPhoneContactById(activity, phoneId);
				
				// put phone number from which this message was sent to top of the list in this pop up
				List<Cont.acts.TaggedContactPhoneNumber> list = Cont.acts()
						.getPersonalContactPhoneNumbers(activity, cf.contactId, false);
				List<Cont.acts.TaggedContactPhoneNumber> contactList= new ArrayList<Cont.acts.TaggedContactPhoneNumber>();
				for(Cont.acts.TaggedContactPhoneNumber contact: list) {
					if(mi.cpn.phoneNumber.localCanonical.equals(contact.cpn.phoneNumber.localCanonical)){
						contactList.add(0,contact);
						list.remove(contact);
						contactList.addAll(list);
						break;
					}
				}
				
				if (contactList != null && contactList.size() > 1) {
					PhoneSelectionDialog dialog = new PhoneSelectionDialog((Activity) activity,
							contactList, mi.displayName, PhoneSelectionDialog.ACTION_CALL, false);
					dialog.show();
				}
				else {
					ringOutAgent.call(mi.cpn.phoneNumber.numRaw, mi.displayName);
				}
			}
			else {
				ringOutAgent.call(mi.cpn.phoneNumber.numRaw, mi.displayName);
			}

		}
		else {
			// -------------------ALERT--------------------------------------
			RcAlertDialog
					.getBuilder(activity)
					.setTitle(R.string.app_name)
					.setMessage(R.string.messageview_btnmsg_callback)
					.setIcon(R.drawable.symbol_information)
					.setPositiveButton(R.string.dialog_btn_ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int whichButton) {
								}
							}).show();
			// -----------------------------------------------------------------
		}
	}
    
    public static void createNewContact(Activity context, MessageItem mi, BindSync bsync){
    	if(bsync.bind.isValid){
    		try{
    			context.startActivity(Cont.acts().getCreateNewContactIntent(context, mi.displayName, bsync.bind.cpn.normalizedNumber));
    		}catch (ActivityNotFoundException e) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "create new contact start activity error: ", e);
                }
            }
    	}
    }
    
    public static void addToExistingContact(Activity context, BindSync bsync){
    	if (bsync.bind.isValid) {
            try {
                context.startActivity(Cont.acts().getAddToContactIntent(context, bsync.bind.cpn.normalizedNumber));
            } catch (ActivityNotFoundException e) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "add to existing contact start activity error: ", e);
                }
            }
        }
    }
    
    public static void addToFavorites(final Activity context, final MessageItem mi, final BindSync bsync){
    	if (bsync.bind.hasContact) {
			if (!bsync.bind.isPhoneNumberFavorite) {
				if (bsync.bind.isPersonalContact) {
					Cont.acts().addToPersonalFavorites(bsync.bind.phoneId, bsync.bind.phoneNumber, bsync.bind.cpn.normalizedNumber, context);
				} else {
					CompanyFavorites.setAsFavorite(context, RCMProviderHelper.getCurrentMailboxId(context), bsync.bind.phoneId);
				}
			}
		} else {
			if (!bsync.bind.isValid) {
				return;
			}
			Resources res = context.getResources();
			final CharSequence[] items = {
					res.getString(R.string.messageview_menu_add_to_contacts_new),
					res.getString(R.string.messageview_menu_add_to_existing) };
			
			AlertDialog.Builder builder = RcAlertDialog.getBuilder(context);
			builder.setTitle(res.getString(R.string.messageview_menu_first_create_contact_prompt));
			builder.setItems(items, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int item) {
					if (item == 0) {
						createNewContact(context, mi, bsync);
					} else {
						addToExistingContact(context, bsync);
					}
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
		}
    }
    
    public static void removeFromFavorites(Activity context, BindSync bsync){
    	if (bsync.bind.hasContact && bsync.bind.isPhoneNumberFavorite) {
            final long mailboxId = RCMProviderHelper.getCurrentMailboxId(context);
            if (bsync.bind.isPersonalContact) {                        	
            	PersonalFavorites.deletePersonalFavorite(context, mailboxId, bsync.bind.phoneId);
            } else {
            	CompanyFavorites.setAsNotFavorite(context, mailboxId, bsync.bind.phoneId);
            }
        }
    }
    
    public static void viewContact(Activity context, BindSync bsync){
    	if (bsync.bind.hasContact) {
            if (bsync.bind.isPersonalContact) {
           	 Cont.acts.PhoneContact cf = Cont.acts().lookUpPhoneContactById(context, bsync.bind.phoneId);
                if (cf == null) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "Contact lost");
                    }
                    return;
                }
                Uri personUri = ContentUris.withAppendedId(Cont.acts().getPeopleUri(), cf.contactId);
                Intent intent = new Intent(Intent.ACTION_VIEW, personUri);
                try {
                    context.startActivityForResult(Intent.createChooser(intent, context.getString(R.string.menu_viewContact)), MENU_ITEM_VIEW_CONTACT);
                } catch (android.content.ActivityNotFoundException e) {
                    if (LogSettings.MARKET) {
                        QaLog.e(TAG, "Messages - view personal contact: " + e.getMessage());
                    }
                }
            } else {
                Uri personUri = UriHelper.getUri(RCMProvider.EXTENSIONS, RCMProviderHelper.getCurrentMailboxId(context), bsync.bind.phoneId);
                Intent intent = new Intent(context, ViewExtensionsActivity.class);
                intent.setData(personUri);
                try {
                    context.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    if (LogSettings.MARKET) {
                        QaLog.e(TAG, "Messages - view company contact: " + e.getMessage());
                    }
                }
            }
        }
    }

    public static String getLengthLabel(int seconds) {
        if (seconds < 0) {
            seconds = 0;
        }
        return String.valueOf(seconds / 60) + ":"
                + (seconds % 60 < 10 ? "0" : "")
                + String.valueOf(seconds % 60);
    }

    public static String[] readMessageFieldsFromDb(Context context, long msgId, String[] projection) {
        if (LogSettings.MARKET) {
            QaLog.d(TAG, "readMessageFieldsFromDb(msgId = " + msgId + ")...");
        }
        
        if (projection == null) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "readMessageFieldsFromDb(): projection is NULL; query all columns");
            }
        }
        
        Cursor cursor = context.getContentResolver().query(
                UriHelper.getUri(RCMProvider.MESSAGES, RCMProviderHelper.getCurrentMailboxId(context)),
                projection,
                RCMDataStore.MessagesTable.JEDI_MSG_ID + '=' + msgId,
                null, null);
        
        if (cursor == null || !cursor.moveToFirst()) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "readMessageFieldsFromDb(): empty cursor; return null");
            }
            
            if (cursor != null) {
                cursor.close();
            }
            return null;
        }

        int column_count = cursor.getColumnCount();
        String[] values = new String[column_count];
        for (int i = 0; i < column_count; i++) {
            values[i] = cursor.getString(i);
        }
      
        cursor.close();
        return values; 
    }

    public static String readStringMessageFieldFromDb(Context context, long msgId, String column) {
        String[] result = readMessageFieldsFromDb(context, msgId, new String[] {column});
        
        if (result == null || result.length == 0) {
            return null;
        }
        
        return result[0];
    }

    public static int readIntMessageFieldFromDb(Context context, long msgId, String column) {
        int result = -1;
        
        String value = readStringMessageFieldFromDb(context, msgId, column);
        if (!TextUtils.isEmpty(value)) {
            try {
                result = Integer.parseInt(value);
            } catch (NumberFormatException e) {
            }
        }
        
        return result;
    }

    public static long readLongMessageFieldFromDb(Context context, long msgId, String column) {
        long result = -1;
        
        String value = readStringMessageFieldFromDb(context, msgId, column);
        if (!TextUtils.isEmpty(value)) {
            try {
                result = Long.parseLong(value);
            } catch (NumberFormatException e) {
            }
        }
        
        return result;
    }

    public static int updateMessageField(Context context, long msgId, String column, String value) {
        return RCMProviderHelper.updateSingleValue(
                context,
                RCMProvider.MESSAGES,
                column,
                value,
                RCMDataStore.MessagesTable.JEDI_MSG_ID + '=' + msgId);
    }
    
    public static int updateMessageField(Context context, long msgId, String column, int intValue) {
        return updateMessageField(context, msgId, column, String.valueOf(intValue));
     }
     
     public static int updateMessageField(Context context, long msgId, String column, long longValue) {
         return updateMessageField(context, msgId, column, String.valueOf(longValue));
     }

    public static int updateMessageFieldWithMailboxId(Context context, long msgId, String column, String value) {
        return RCMProviderHelper.updateSingleValueWithMailboxId(
                context,
                RCMProvider.MESSAGES,
                column,
                value,
                RCMDataStore.MessagesTable.JEDI_MSG_ID + '=' + msgId);
    }
    
    public static int updateMessageFieldWithMailboxId(Context context, long msgId, String column, int intValue) {
       return updateMessageFieldWithMailboxId(context, msgId, column, String.valueOf(intValue));
    }
    
    public static int updateMessageFieldWithMailboxId(Context context, long msgId, String column, long longValue) {
        return updateMessageFieldWithMailboxId(context, msgId, column, String.valueOf(longValue));
    }

    
    private static final String[] UPDATE_DURATION_QUERY_PROJECTION = new String[] {
        RCMDataStore.MessagesTable.JEDI_MSG_TYPE,
        RCMDataStore.MessagesTable.JEDI_FILE_EXT,
        RCMDataStore.MessagesTable.RCM_DURATION_SYNCHRONIZED };
    
    static void updateDuration(Context context, long msgId) {
        if (LogSettings.MARKET) {
            QaLog.d(TAG, "updateDuration(msgId = " + msgId + ")...");
        }                

        String[] attrs = readMessageFieldsFromDb(context, msgId, UPDATE_DURATION_QUERY_PROJECTION);
        
        if (attrs == null || attrs.length == 0) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "updateDuration(): no data from DB; return");
            }
            return;
        }
        
        String file_ext = attrs[1];
        int msg_type = 0;
        int duration_sync_state = 0;
        try {
            msg_type = Integer.parseInt(attrs[0]);
        } catch (NumberFormatException t) {
        	if(LogSettings.MARKET) {
        		MktLog.e(TAG, "Exception parsing message attributes [1]", t);
        	}
        }
        try {
            duration_sync_state = Integer.parseInt(attrs[2]);
        } catch (NumberFormatException t) {
        	if(LogSettings.MARKET) {
        		MktLog.e(TAG, "Exception parsing message attributes []", t);
        	}
        }
        
        
        if (msg_type == MessageType.VOICE.getValue() && isNeedToUpdateDuration(duration_sync_state)) {
            String realDuration = getDurationFromFile(FileUtils.getPath(FileUtils.getSDPath(context), String.valueOf(msgId), file_ext));
            setDurationProvidedByPlayer(context, msgId, realDuration, duration_sync_state);
        }
        
        
        
    }

    public static void setDurationProvidedByPlayer(Context context, long msgId, String realDuration, int duration_sync_state) {
        try {
            long mailbox_id = RCMProviderHelper.getCurrentMailboxId(context);
            String where = MessagesTable.JEDI_MSG_ID + "=" + msgId;        
            ContentValues values = new ContentValues();
            int numberOfUpdatedRows = 0;
            if (realDuration != null) {
                duration_sync_state = 1;
                values.put(MessagesTable.JEDI_DURATION, realDuration);
                numberOfUpdatedRows = context.getContentResolver().update(UriHelper.getUri(RCMProvider.MESSAGES, mailbox_id), values, where, null);
            } else {
                duration_sync_state--;
            }
            
            if (numberOfUpdatedRows > 0){ 
                values = new ContentValues();
                values.put(MessagesTable.RCM_DURATION_SYNCHRONIZED, duration_sync_state);
                numberOfUpdatedRows = context.getContentResolver().update(UriHelper.getUri(RCMProvider.MESSAGES, mailbox_id), values, where, null);
            }
        } catch (Exception e) {
            if (LogSettings.MARKET){
                MktLog.w(TAG, "writing to store error");
            }
        }
    }

    static boolean isNeedToUpdateDuration(Context context, long msgId) {
        int duration_sync_status = readIntMessageFieldFromDb(
                context, msgId, RCMDataStore.MessagesTable.RCM_DURATION_SYNCHRONIZED);
        
        return isNeedToUpdateDuration(duration_sync_status);
    }
    
    static boolean isNeedToUpdateDuration(int duration_sync_state) {
        return (duration_sync_state <= 0 && duration_sync_state > -3);
    }

    
    static void moveMessageToDeleted(Context context, long msgId){
        int rows_updated = MessageUtils.updateMessageField(context, msgId, RCMDataStore.MessagesTable.RCM_LOCALLY_DELETED, 1);
        
        if (rows_updated > 0) {
            context.sendBroadcast(
                    new Intent(RCMConstants.ACTION_UPDATE_MESSAGES_LIST)
                    .putExtra(Messages.MESSAGE_LIST_UPDATE_BROADCAST_EXTRA, Messages.INTENT_EXTRA_TYPES.MOVE_MESSAGE_TO_DELETED));
        }
    }
    
    static void toggleMessageReadStatusInDb(Context context, long msgId, int current_status) {
        int new_status = (current_status == MessagePOJO.READ ? MessagePOJO.UNREAD : MessagePOJO.READ);

        int rows_updated = MessageUtils.updateMessageField(context, msgId, RCMDataStore.MessagesTable.JEDI_READ_STATUS, new_status);

        if (rows_updated > 0) {
            if (LogSettings.ENGINEERING) {
                EngLog.d(TAG, "setMessageReadStatus() send broadcast: " + RCMConstants.ACTION_UPDATE_MESSAGES_LIST);
            }
            context.sendBroadcast(new Intent(RCMConstants.ACTION_UPDATE_MESSAGES_LIST).putExtra(Messages.MESSAGE_LIST_UPDATE_BROADCAST_EXTRA, Messages.INTENT_EXTRA_TYPES.READ_STATUS_CHANGED));
        }
    }

    
    public static int setMessagesReadStatus(Context context, long mailboxId, List<Long> msgIds, int newStatus) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "setMessagesReadStatus(): newStatus=" + newStatus);
        }
        
        if (msgIds == null || msgIds.size() <= 0) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "setMessagesReadStatus(): List msgIDs is empty; return 0");
            }
            return 0;
        }
        
        int msg_count = msgIds.size();
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "setMessagesReadStatus(): msg_count:" + msg_count);
        }

        StringBuilder sb = new StringBuilder(RCMDataStore.MessagesTable.JEDI_MSG_ID + '=' + msgIds.get(0));
        for (int i = 1; i < msg_count; i++) {
            sb.append(" OR ").append(RCMDataStore.MessagesTable.JEDI_MSG_ID).append('=').append(msgIds.get(i));
        }
        
        String where = sb.toString();
        
        ContentValues values = new ContentValues(1);
        values.put(RCMDataStore.MessagesTable.JEDI_READ_STATUS, newStatus);

        int rows_updated = context.getContentResolver().update(
                UriHelper.getUri(RCMProvider.MESSAGES, mailboxId), values, where, null);

        if (LogSettings.MARKET) {
            MktLog.d(TAG, "setMessagesReadStatus(): rows_updated: " + rows_updated);
        }

        return rows_updated;
    }

    public static int setMessagesDeletedStatus(Context context, long mailboxId, List<Long> msgIds, int newStatus) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "setMessagesDeletedStatus(): newStatus=" + newStatus);
        }
        
        if (msgIds == null || msgIds.size() <= 0) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "setMessagesDeletedStatus(): List msgIDs is empty; return 0");
            }
            return 0;
        }
        
        int msg_count = msgIds.size();
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "setMessagesDeletedStatus(): msg_count:" + msg_count);
        }

        StringBuilder sb = new StringBuilder(RCMDataStore.MessagesTable.JEDI_MSG_ID + '=' + msgIds.get(0));
        for (int i = 1; i < msg_count; i++) {
            sb.append(" OR ").append(RCMDataStore.MessagesTable.JEDI_MSG_ID).append('=').append(msgIds.get(i));
        }
        
        String where = sb.toString();
        
        ContentValues values = new ContentValues(1);
        values.put(RCMDataStore.MessagesTable.RCM_LOCALLY_DELETED, newStatus);

        int rows_updated = context.getContentResolver().update(
                UriHelper.getUri(RCMProvider.MESSAGES, mailboxId), values, where, null);

        if (LogSettings.MARKET) {
            MktLog.d(TAG, "setMessagesDeletedStatus(): rows_updated: " + rows_updated);
        }

        return rows_updated;
    }
    
    
    private static final String[] DB_QUERY_GET_MESSAGE_LIST_PROJECTION = new String[] {RCMDataStore.MessagesTable.JEDI_MSG_ID};
    private static final int DB_QUERY_GET_MESSAGE_LIST_COLUMN_INDEX_MSG_ID = 0; 
    private static final String DB_QUERY_GET_MESSAGE_LIST_ORDER = MessagesTable.JEDI_CREATE_DATE + " DESC ";

    public static List<Long> getMessageList(Context context, int messageType) {
        ContentResolver resolver = context.getContentResolver();
        long mailbox_id = RCMProviderHelper.getCurrentMailboxId(context);

        String selection = MessagesTable.RCM_LOCALLY_DELETED + "=" + messageType ;
        Cursor cursor = resolver.query(
                UriHelper.getUri(RCMProvider.MESSAGES, mailbox_id),
                DB_QUERY_GET_MESSAGE_LIST_PROJECTION, selection, null, DB_QUERY_GET_MESSAGE_LIST_ORDER);
        
        if (cursor == null || cursor.getCount() <= 0) {
            if(LogSettings.MARKET){
                MktLog.w(TAG, "getMessagesList(): no messages; return null");
            }
            if (cursor != null) {
                cursor.close();
            }
            return null;
        }
        
        ArrayList<Long> msg_list = new ArrayList<Long>(cursor.getCount());
        
        cursor.moveToFirst();
        do {
            msg_list.add(cursor.getLong(DB_QUERY_GET_MESSAGE_LIST_COLUMN_INDEX_MSG_ID));
        } while (cursor.moveToNext());
        cursor.close();
        
        return msg_list;
    }
    
    public static String getTextDate(Date dateTime) {
        if (dateTime == null) {
            return null;
        }
        
        // Set the date/time field by mixing relative and absolute
        // times.
        int flags = DateUtils.FORMAT_ABBREV_RELATIVE;
        long date = dateTime.getTime();

        String textDate = DateUtils.getRelativeTimeSpanString(date,
                System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS,
                flags).toString();

        return textDate;
    }

    /* Check that message body is exist.
     * Conditions: if Storage not accessible - don't change message status to NOT_LOADED. 
     */
    static boolean isValidMessageBody(Context context, long msgId) {
        if (!FileUtils.isStorageAccessible()) {
            return true;
        }

        String file_ext = MessageUtils.readStringMessageFieldFromDb(context, msgId, RCMDataStore.MessagesTable.JEDI_FILE_EXT);
        String accountPath = FileUtils.getSDPath(context);
        String path = FileUtils.getFilePathForMsg(accountPath, String.valueOf(msgId), file_ext);
        File file = new File(path);

        if (LogSettings.ENGINEERING) {
            EngLog.i(TAG, "isValidMessageBody: body is " + (file.exists() ? "present" : "absent") + " Message ID: " + msgId + ", path: " +  path);
        }
        if (file.exists()) {
            return true;
        } else {
            return false;
        }
    }
   
    static void setMessageSyncStatus(Context context, long msgId, int syncStatus, boolean sendBroadcast){

        if (LogSettings.ENGINEERING) {
            EngLog.i(TAG, "setMessageSyncStatus() started: " + msgId);
        }                
        
        ContentResolver resolver = context.getContentResolver();
        long mailbox_id = RCMProviderHelper.getCurrentMailboxId(context);
                
        String selection = MessagesTable.JEDI_MSG_ID + "=" + msgId;        
        ContentValues values = new ContentValues();
        values.put(MessagesTable.RCM_SYNC_STATUS, syncStatus);
        Uri uri = UriHelper.getUri(RCMProvider.MESSAGES, mailbox_id);

        int rowsUpdated = resolver.update(uri, values, selection, null);
        if (rowsUpdated > 0 && sendBroadcast) {
            if (LogSettings.ENGINEERING) {
                EngLog.d(TAG, "setMessageSyncStatus() rows updated: " + rowsUpdated + ", current status = " + syncStatus + ", send broadcast: " + RCMConstants.ACTION_UPDATE_MESSAGES_LIST);
            }
            context.sendBroadcast(new Intent(RCMConstants.ACTION_UPDATE_MESSAGES_LIST).putExtra(Messages.MESSAGE_LIST_UPDATE_BROADCAST_EXTRA, Messages.INTENT_EXTRA_TYPES.SYNC_STATUS_CHANGED));
        }

        if (LogSettings.ENGINEERING) {
            EngLog.i(TAG, "setMessageSyncStatus() finished: " + msgId + " status set to " + syncStatus );
        }                
    }

    public static void removeAllDeletedMessages(Context context){        
        ContentResolver resolver = context.getContentResolver();
        long mailbox_id = RCMProviderHelper.getCurrentMailboxId(context);
        
        String where = MessagesTable.RCM_LOCALLY_DELETED + "=" + 1;
        if (resolver.delete(UriHelper.getUri(RCMProvider.MESSAGES, mailbox_id), where, null) > 0) {
            context.sendBroadcast(new Intent(RCMConstants.ACTION_UPDATE_MESSAGES_LIST).putExtra(Messages.MESSAGE_LIST_UPDATE_BROADCAST_EXTRA, Messages.INTENT_EXTRA_TYPES.REMOVE_ALL_DELETED));
        }
    }

    public static void removeDeletedMessage(Context context, long msgId ){        
        ContentResolver resolver = context.getContentResolver();
        long mailbox_id = RCMProviderHelper.getCurrentMailboxId(context);
        
        String where = MessagesTable.RCM_LOCALLY_DELETED + "=" + 1 + " AND " + MessagesTable.JEDI_MSG_ID + "=" + msgId;
        if (resolver.delete(UriHelper.getUri(RCMProvider.MESSAGES, mailbox_id), where, null) > 0) {
            context.sendBroadcast(new Intent(RCMConstants.ACTION_UPDATE_MESSAGES_LIST).putExtra(Messages.MESSAGE_LIST_UPDATE_BROADCAST_EXTRA, Messages.INTENT_EXTRA_TYPES.REMOVE_DELETED));
        }
    }

    public static void updateValidBind(Context context, long messageId, boolean validNumber, String normalizedNumber) {
    	final long mailboxId = RCMProviderHelper.getCurrentMailboxId(context);
    	
    	try {
    		 Uri uri = UriHelper.getUri(RCMProvider.MESSAGES, mailboxId, messageId);
    		 ContentValues cv = new ContentValues();
    		 cv.put(RCMDataStore.MessagesTable.RCM_FROM_PHONE_IS_VALID_NUMBER, validNumber);
    		 cv.put(RCMDataStore.MessagesTable.RCM_FROM_PHONE_NORMALIZED_NUMBER, normalizedNumber);
    		 context.getContentResolver().update(uri, cv, null, null);
    	} catch (Throwable t) {
    		if(LogSettings.MARKET) {
    			MktLog.e(TAG, "updateValidBind(), error : " + t); 
    		}
    	}    	    	
    }
    
    private static String getDurationFromFile(String filePath) {
    	if(LogSettings.ENGINEERING) {
    		EngLog.v(TAG, "getDurationFromFile(), filePath : " + filePath);
    	}
    	int messageDuration = -1;
    	MediaPlayer mediaPlayer = null; 
    	try {
    		mediaPlayer = new MediaPlayer();
    		if(mediaPlayer != null) {
        		mediaPlayer.reset();
        		mediaPlayer.setDataSource(filePath);
        		mediaPlayer.prepare();
        		messageDuration = mediaPlayer.getDuration() / 1000;        		
        	}
    	} catch (Throwable t) {
			if (LogSettings.MARKET) {
				MktLog.e(TAG, "getDurationFromFile(), exception when getting audio duration file : " + filePath, t);				
			}
		} finally {
			if(LogSettings.ENGINEERING) {
				EngLog.v(TAG, "getDurationFromFile().finally releasing resources");
			}
			if(mediaPlayer != null) {
				try {
					mediaPlayer.stop();
					mediaPlayer.release();
					mediaPlayer = null;
				} catch(Throwable t) {
					if(LogSettings.MARKET) {
						MktLog.e(TAG, "getDurationFromFile().finally, exception when releasing resources");
					}
				}
			}
		}    	    	
		if (messageDuration > 0) {
			return String.valueOf(messageDuration);
		}
		return "1";
    }

}
