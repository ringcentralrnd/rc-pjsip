package com.ringcentral.android.messages;

public class MsgSyncRequestTemplate {

	public static final String DebugEol = "\n";

	public static final String MAIN = "MfcISAPICommand=%s&XMLREQ=" + DebugEol
			+ "<REQUEST>" + DebugEol
			+ "	<SesIdentity>%s:%s:%d:REQ</SesIdentity>" + DebugEol
			+ "	<RegId>%s</RegId>" + DebugEol
			+ "	<ProtocolVersion>%s</ProtocolVersion>" + DebugEol
			+ "	<AgentVersion>%s</AgentVersion>" + DebugEol + "	<REQUESTHDR>"
			+ DebugEol + "		<UserID>%s</UserID>" + DebugEol
			+ "		<MailboxExt>%s</MailboxExt>" + DebugEol
			+ "		<DgSignature>%s</DgSignature>" + DebugEol
			+ "		<Password>%d</Password>" + DebugEol
			+ "		<StartMsgID>%s</StartMsgID>" + DebugEol
			+ "		<ModificationCounter>%s</ModificationCounter>" + DebugEol
			+ "		<SessionID>%s</SessionID>" + DebugEol
			+ "		<Folder>%s</Folder>" + DebugEol
			+ "		<PNumListModificationCounter>%s</PNumListModificationCounter>"
			+ DebugEol + "	</REQUESTHDR>" + DebugEol;

	public static final String SESSION_ID = "	<REQSESSIONID>"
			+ MsgSyncRequestTemplate.DebugEol + "		<UserID>%s</UserID>"
			+ MsgSyncRequestTemplate.DebugEol + "		<MailboxExt>%s</MailboxExt>"
			+ MsgSyncRequestTemplate.DebugEol + "	</REQSESSIONID>" + MsgSyncRequestTemplate.DebugEol;

	public static final String MSG_BODY = "	<MSGCONTENTREQ>"
			+ DebugEol
			+ "		<CompType>%d</CompType>"
			+ DebugEol // compression type == mp3?
			+ "		<Status>0</Status>" + DebugEol + "		<MsgType>%d</MsgType>"
			+ DebugEol + "		<MsgID>%d</MsgID>" + DebugEol
			+ "		<Duration>0</Duration>" + DebugEol
			+ "		<BodySize>0</BodySize>" + DebugEol + "		<FileExt></FileExt>"
			+ DebugEol + "		<CreateDate></CreateDate>" + DebugEol
			+ "		<FromName></FromName>" + DebugEol
			+ "		<FromPhone></FromPhone>" + DebugEol + "		<ToName></ToName>"
			+ DebugEol + "		<ToPhone></ToPhone>" + DebugEol
			+ "		<Urgent>0</Urgent>" + DebugEol + "		<Comment></Comment>"
			+ DebugEol + "	</MSGCONTENTREQ>" + DebugEol;

	public static final String MESSAGE_STATUS = "	<MSGCHANGEREADREQ>"
			+ DebugEol
			+ "		<CompType>2</CompType>"
			+ DebugEol // compression type == mp3?
			+ "		<Status>%d</Status>" + DebugEol + "		<MsgType>1</MsgType>"
			+ DebugEol + "		<MsgID>%d</MsgID>" + DebugEol
			+ "		<Duration>0</Duration>" + DebugEol
			+ "		<BodySize>0</BodySize>" + DebugEol + "		<FileExt></FileExt>"
			+ DebugEol + "		<CreateDate></CreateDate>" + DebugEol
			+ "		<FromName></FromName>" + DebugEol
			+ "		<FromPhone></FromPhone>" + DebugEol + "		<ToName></ToName>"
			+ DebugEol + "		<ToPhone></ToPhone>" + DebugEol
			+ "		<Urgent>0</Urgent>" + DebugEol + "		<Comment></Comment>"
			+ DebugEol + "	</MSGCHANGEREADREQ>" + DebugEol;

	public static final String DELETE_MESSAGE = "	<MSGDELETEREQ>"
			+ DebugEol
			+ "		<CompType>2</CompType>"
			+ DebugEol // compression type == mp3?
			+ "		<Status>0</Status>" + DebugEol + "		<MsgType>1</MsgType>"
			+ DebugEol + "		<MsgID>%d</MsgID>" + DebugEol
			+ "		<Duration>0</Duration>" + DebugEol
			+ "		<BodySize>0</BodySize>" + DebugEol + "		<FileExt></FileExt>"
			+ DebugEol + "		<CreateDate></CreateDate>" + DebugEol
			+ "		<FromName></FromName>" + DebugEol
			+ "		<FromPhone></FromPhone>" + DebugEol + "		<ToName></ToName>"
			+ DebugEol + "		<ToPhone></ToPhone>" + DebugEol
			+ "		<Urgent>0</Urgent>" + DebugEol
			+ "		<Comment>deleted_ID=%d</Comment>" + DebugEol
			+ "	</MSGDELETEREQ>" + DebugEol;
	
	public static final String DETAILS ="	<MSGCONTENTREQ>"+ DebugEol
	+ "		<CompType>2</CompType>"
	+ DebugEol // compression type == mp3?
	+ "		<Status>0</Status>" + DebugEol
	+ "		<MsgType>1</MsgType>" + DebugEol
	+ "		<MsgID>%d</MsgID>" + DebugEol
	+ "		<Duration>0</Duration>" + DebugEol
	+ "		<BodySize>0</BodySize>" + DebugEol
	+ "		<FileExt></FileExt>" + DebugEol
	+ "		<CreateDate></CreateDate>" + DebugEol
	+ "		<FromName></FromName>" + DebugEol
	+ "		<FromPhone></FromPhone>" + DebugEol
	+ "		<ToName></ToName>" + DebugEol
	+ "		<ToPhone></ToPhone>" + DebugEol
	+ "		<Urgent>0</Urgent>" + DebugEol
	+ "		<Comment></Comment>" + DebugEol + "	</MSGCONTENTREQ>"
	+ DebugEol;

	public static final String MESSAGE_DETAILS = "	<MSGCONTENTREQ>"
		+ DebugEol
		+ "		<CompType>2</CompType>"
		+ DebugEol // compression type == mp3?
		+ "		<Status>0</Status>" + DebugEol
		+ "		<MsgType>1</MsgType>" + DebugEol
		+ "		<MsgID>%d</MsgID>" + DebugEol
		+ "		<Duration>0</Duration>" + DebugEol
		+ "		<BodySize>0</BodySize>" + DebugEol
		+ "		<FileExt></FileExt>" + DebugEol
		+ "		<CreateDate></CreateDate>" + DebugEol
		+ "		<FromName></FromName>" + DebugEol
		+ "		<FromPhone></FromPhone>" + DebugEol
		+ "		<ToName></ToName>" + DebugEol
		+ "		<ToPhone></ToPhone>" + DebugEol
		+ "		<Urgent>0</Urgent>" + DebugEol
		+ "		<Comment></Comment>" + DebugEol + "	</MSGCONTENTREQ>"
		+ DebugEol;
	
}

