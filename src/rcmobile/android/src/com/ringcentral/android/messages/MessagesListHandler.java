/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.messages;

import java.util.HashMap;
import java.util.Map;

import com.ringcentral.android.api.XmlParsingObjectAbstract;
import com.ringcentral.android.api.parsers.MessageHeaderHandler;
import com.ringcentral.android.api.parsers.MessageListHeaderHandler;
import com.ringcentral.android.api.pojo.MessagePOJO;

public class MessagesListHandler  extends XmlParsingObjectAbstract {

	private final static String sf_sf_tag_name		= "MESSAGESLIST";

	private MessageListHeaderHandler m_header = null;
	private MessageHeaderHandler m_message_header = null;
	private Map<Long, MessagePOJO> msgList = new HashMap<Long, MessagePOJO>();

	/*
	 * 
	 */
	public MessagesListHandler(XmlParsingObjectAbstract parent) {
		super(parent);
		
		m_header = new MessageListHeaderHandler( this );
		m_message_header = new MessageHeaderHandler( this );
		
		addChild( m_header.getTagName(), m_header );
		addChild( m_message_header.getTagName(), m_message_header );
	}


	public MessageListHeaderHandler getHeader() {
		return m_header;
	}

	public Map<Long, MessagePOJO> getMsgList(){
		return msgList;
	}
	
	public void addMessage(MessagePOJO msg){
		msgList.put(msg.getId(), msg);
	}
	
	@Override
	public String getTagName() {
		return sf_sf_tag_name;
	}

}
