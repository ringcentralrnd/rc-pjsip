/**
 * Copyright (C) 2011-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.messages;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.LoginScreen;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentral;
import com.ringcentral.android.api.pojo.MessagePOJO;
import com.ringcentral.android.api.pojo.MessagePOJO.MessageType;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.utils.ui.widget.TabIndicator;

public class MessagesNotification extends Activity {	
	private static final String TAG = "[RC]MessagesNotification";
	
	//Timeout to clear message
	private static final long RESET_TIMEOUT = 3 * 1000; // 3 seconds
	//User is on recent page
	private static boolean mOnRecent;	
	//if user is on recent page, clear on indication on exit from recent page
	private static boolean mClearOnExit;	
	//unread count
	private static int mCurrentCount = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LoginScreen.Launcher.launch(this, getIntent());
		finish();
	}
	
	/**
	 * @param context
	 * @param numUnread new number of unread messages
	 */
	public static void updateNotification(Context context, int newUnreadCount){
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "updateNotification : " + " mOnRecent : " + mOnRecent + " unread : " + newUnreadCount);
		}
		
		mCurrentCount = newUnreadCount;		
		mClearOnExit = mOnRecent;										
		sendNotificationUpdate(context);		
		sendIndicatorUpdate(context, mCurrentCount);
	}
	
	
	/**
	 * User visit recent page
	 * @param onRecent -> true means entered, false means exit
	 */
	public static void visitRecent(final Context context, boolean onRecent){
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "visit recent page : " + onRecent + ", was : " + mOnRecent + " clearOnExit : " + mClearOnExit);
		}
		if(mOnRecent != onRecent){
			mOnRecent = onRecent;
			if(mOnRecent){ //send notification to clear indicator
				if(LogSettings.ENGINEERING){
					EngLog.d(TAG, "Scheduling notification reset in " + RESET_TIMEOUT + " milliseconds");
				}
				new Thread(new Runnable() {					
					@Override
					public void run() {
						try {
							Thread.sleep(RESET_TIMEOUT);
						} catch (InterruptedException e) {
							if(LogSettings.MARKET){
								MktLog.e(TAG, "Interrupted reset thread", e);
							}							
						}
						cancelAll(context);						
					}
				}, TAG).start();				
			} else {
				if(mClearOnExit){
					cancelAll(context);
					mClearOnExit = false;
				}
			}
		}	
	}			
	
	
	/**
	 * Send update to message indicator
	 * @param context context
	 * @param number new number of unread messages
	 */
	public static void sendIndicatorUpdate(Context context, int unread){									
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "sendIndicatorUpdate : " + unread);
		}
		
		final Intent updateIntent = new Intent(RCMConstants.ACTION_UPDATE_MESSAGE_INDICATOR);
		updateIntent.putExtra(TabIndicator.NUM_ITEMS, unread);
        context.sendBroadcast(updateIntent);		
	}
	
	public static void updateIndicator(Context context) {
		sendIndicatorUpdate(context, mCurrentCount);
	}
	
	
	
	/**
	 * Send android notification for new messages refer 
	 * http://jira.ringcentral.com/browse/AB-54 for details
	 * @param context context
	 * @param integer number of unread messages
	 */
	private static final String[] SEND_NOTIFICATION_UPDATE_QUERY_PROJECTION =
	    new String[] {RCMDataStore.MessagesTable.JEDI_MSG_TYPE};
	private static final String SEND_NOTIFICATION_UPDATE_QUERY_SELECTION = RCMDataStore.MessagesTable.JEDI_READ_STATUS + '=' + MessagePOJO.UNREAD;
	
	private static void sendNotificationUpdate(Context context){
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "sendNotificationUpdate");
		}
		
		long mailboxId = RCMProviderHelper.getCurrentMailboxId(context);
		Cursor cursor = context.getContentResolver().query(
		        UriHelper.getUri(RCMProvider.MESSAGES, mailboxId),
		        SEND_NOTIFICATION_UPDATE_QUERY_PROJECTION,
		        SEND_NOTIFICATION_UPDATE_QUERY_SELECTION,
		        null, null);
		
		if (cursor == null || cursor.getCount() <= 0) { //no unread messages
            if (cursor != null) {
                cursor.close();
            }
		    cancelNotification(context);
            return;
		}

		// If we have unread messages, show persistent notification, not only ticker
		int unread = cursor.getCount();
        int voicemail = 0;
	    int fax = 0;
	    int other = 0;

	    cursor.moveToFirst();
	    int message_type;
	    do {
	        message_type = cursor.getInt(0);
            if(message_type == MessageType.VOICE.getValue()) {
                voicemail++;
            } else if (message_type == MessageType.FAX.getValue()) {
                fax++;
            } else {
                other++;
            }
	    } while (cursor.moveToNext());
	    cursor.close();
	    
		final NotificationManager notificationMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		final String tickerText = context.getString(R.string.messages_notification_ticker);
		final Notification notification = new Notification(R.drawable.ic_stat_notify_message, tickerText, System.currentTimeMillis());
	
		//this case cancel previous notification and send a new One
		//notificationMgr.cancel(ID_UNREAD_NOTIFICATION);			
		final String notifLine1 = context.getString(R.string.app_name);
		final String notifLine2 = getLabel(context, voicemail, fax, other);
		
		Intent notificationIntent = new Intent(context, MessagesNotification.class);
		notificationIntent.putExtra(RingCentral.TAB, RingCentral.TAB_MESSAGES_ID);
		
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0 , notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);			
		notification.setLatestEventInfo(context, notifLine1, notifLine2, contentIntent);			
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.defaults |= Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
		
		notificationMgr.notify(RCMConstants.NOTIFICATION_ID_UNREAD_MESSAGES, notification);			
	}
		
	/**
	 * Cancel messaging notification
	 * @param context
	 */
	private static void cancelNotification(Context context) {			
		final NotificationManager notificationMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationMgr.cancel(RCMConstants.NOTIFICATION_ID_UNREAD_MESSAGES);
	}
	
	/**
	 * Cancel both messaging and Indicator notification 
	 * @param context
	 */
	public static void cancelAll(Context context) {
		if(LogSettings.MARKET){
			MktLog.d(TAG, "cancelAll notification");
		}
		try {
			mCurrentCount = 0;
			sendIndicatorUpdate(context, TabIndicator.NUM_ITEMS_DEFAULT);
			cancelNotification(context);
		} catch (Throwable t) {
			if(LogSettings.MARKET) {
				MktLog.e(TAG, "cancelAll()", t);
			}
		}
	}	
	
	/**
	 * Generate label basing on message type, refer AB-54 for details 
	 * @param context
	 * @param messageList
	 * @return
	 */
	private static String getLabel(Context context, int voicemail, int fax, int other) {
		String label = null;
		if((fax == 0) && (other == 0)) {
			label = context.getString((voicemail > 1) ? R.string.messages_notification_line2_mult_voicemail : R.string.messages_notification_line2_single_voicemail, voicemail);
		} else if ((voicemail == 0) && (other == 0)) {
			label = context.getString((fax > 1) ? R.string.messages_notification_line2_mult_fax : R.string.messages_notification_line2_single_fax, fax);
		} else {
			label = context.getString(R.string.messages_notification_line2_mult_message, (fax + voicemail + other));
		}
		
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "Label Vm : " + voicemail + " Fax : " + fax + " Other : " + other + " Label : " + label);
		}
		
		return label;
	}		
}
