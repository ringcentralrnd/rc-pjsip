/** 
 * Copyright (C) 2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.messages;

import java.util.Date;
import java.util.StringTokenizer;

import android.content.AsyncQueryHandler;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.api.pojo.MessagePOJO;
import com.ringcentral.android.api.pojo.MessagePOJO.MessageType;
import com.ringcentral.android.contacts.Cont.acts.ContactBinding;
import com.ringcentral.android.contacts.ContactPhoneNumber;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMDataStore.MessagesTable;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.utils.PhoneUtils;

public class MessagesQuery {

	private static final String TAG = "[RC]MessagesQuery";

	public static final String[] MESSAGES_SUMMARY_PROJECTION = new String[] {
			MessagesTable._ID, MessagesTable.MAILBOX_ID,
			MessagesTable.JEDI_MSG_ID, MessagesTable.JEDI_MSG_TYPE,
			MessagesTable.JEDI_COMP_TYPE, MessagesTable.JEDI_FILE_EXT,
			MessagesTable.JEDI_BODY_SIZE, MessagesTable.JEDI_DURATION,
			MessagesTable.JEDI_CREATE_DATE, MessagesTable.JEDI_FROM_PHONE,
			MessagesTable.JEDI_FROM_NAME, MessagesTable.JEDI_TO_PHONE,
			MessagesTable.JEDI_TO_NAME, MessagesTable.JEDI_URGENT_STATUS,
			MessagesTable.JEDI_COMMENT, MessagesTable.JEDI_READ_STATUS,
			MessagesTable.RCM_SYNC_STATUS, MessagesTable.RCM_FILE_PATH,
			MessagesTable.RCM_LOCALLY_READ, MessagesTable.RCM_LOCALLY_DELETED,
			MessagesTable.RCM_DURATION_SYNCHRONIZED,
			MessagesTable.RCM_FROM_PHONE_IS_VALID_NUMBER,
			MessagesTable.RCM_FROM_PHONE_NORMALIZED_NUMBER,
			MessagesTable.BIND_HAS_CONTACT,
			MessagesTable.BIND_IS_PERSONAL_CONTACT, MessagesTable.BIND_ID,
			MessagesTable.BIND_DISPLAY_NAME, MessagesTable.BIND_ORIGINAL_NUMBER };

	static final int MESSAGES_ID_INDX = 0;
	static final int MESSAGES_MAILBOX_ID_INDX = 1;
	static final int MESSAGES_JEDI_MSG_ID_INDX = 2;
	static final int MESSAGES_JEDI_MSG_TYPE_INDX = 3;
	static final int MESSAGES_JEDI_COMP_TYPE_INDX = 4;
	static final int MESSAGES_JEDI_FILE_EXT_INDX = 5;
	static final int MESSAGES_JEDI_BODY_SIZE_INDX = 6;
	static final int MESSAGES_JEDI_DURATION_INDX = 7;
	static final int MESSAGES_JEDI_CREATE_DATE_INDX = 8;
	static final int MESSAGES_JEDI_FROM_PHONE_INDX = 9;
	static final int MESSAGES_JEDI_FROM_NAME_INDX = 10;
	static final int MESSAGES_JEDI_TO_PHONE_INDX = 11;
	static final int MESSAGES_JEDI_TO_NAME_INDX = 12;
	static final int MESSAGES_JEDI_URGENT_STATUS_INDX = 13;
	static final int MESSAGES_JEDI_COMMENT_INDX = 14;
	static final int MESSAGES_JEDI_READ_STATUS_INDX = 15;
	static final int MESSAGES_RCM_SYNC_STATUS_INDX = 16;
	static final int MESSAGES_RCM_FILE_PATH_INDX = 17;
	static final int MESSAGES_RCM_LOCALLY_READ_INDX = 18;
	static final int MESSAGES_RCM_LOCALLY_DELETED_INDX = 19;
	static final int MESSAGES_RCM_DURATION_SYNCHRONIZED_INDX = 20;
	static final int MESSAGES_RCM_FROM_PHONE_IS_VALID_NUMBER_INDX = 21;
	static final int MESSAGES_RCM_FROM_PHONE_NORMALIZED_NUMBER_INDX = 22;
	static final int MESSAGES_BIND_HAS_CONTACT_INDX = 23;
	static final int MESSAGES_BIND_IS_PERSONAL_CONTACT_INDX = 24;
	static final int MESSAGES_BIND_ID_INDX = 25;
	static final int MESSAGES_BIND_DISPLAY_NAME_INDX = 26;
	static final int MESSAGES_BIND_ORIGINAL_NUMBER_INDX = 27;

	static final String MESSAGES_QUERY_SELECTION_RECENT = MessagesTable.RCM_LOCALLY_DELETED
			+ "=" + MessagePOJO.DELETED_LOCALLY_FALSE;
	static final String MESSAGES_QUERY_SELECTION_DELETED = MessagesTable.RCM_LOCALLY_DELETED
			+ "=" + MessagePOJO.DELETED_LOCALLY_TRUE;

	static final String MESSAGES_QUERY_SORT_ORDER_BY_DATE = MessagesTable.JEDI_CREATE_DATE
			+ " DESC";
	static final String MESSAGES_QUERY_SORT_ORDER_BY_SENDER = "lower("
			+ MessagesTable.BIND_DISPLAY_NAME + ")" + " ASC";

	/**
	 * Read BindSync data (push) from a cursor with
	 * {@link #MESSAGES_SUMMARY_PROJECTION} projection.
	 * 
	 * @param context
	 *            the execution context
	 * @param c
	 *            the cursor (retrieved with
	 *            {@link #MESSAGES_SUMMARY_PROJECTION} projection)
	 * @return the bind sync with date from the cursor
	 */
	static ContactBinding readSyncBind(Context context, Cursor c) {
		ContactBinding bind = new ContactBinding();
		long validIndex = c
				.getLong(MESSAGES_RCM_FROM_PHONE_IS_VALID_NUMBER_INDX);
		String normalizedNumber = c
				.getString(MESSAGES_RCM_FROM_PHONE_NORMALIZED_NUMBER_INDX);
		if (validIndex == -1) {
			final String phoneNumber = c
					.getString(MESSAGES_JEDI_FROM_PHONE_INDX);
			ContactPhoneNumber cpn = PhoneUtils
					.getContactPhoneNumberWithoutTrim(phoneNumber);
			if (cpn == null || !cpn.isValid) {
				cpn = PhoneUtils.getContactPhoneNumber(phoneNumber);
			}
			final boolean isValid = (cpn != null && cpn.isValid);
			MessageUtils.updateValidBind(context, c.getLong(MESSAGES_ID_INDX),
					isValid, isValid ? cpn.normalizedNumber : phoneNumber);
			validIndex = isValid ? 1 : 0;
			normalizedNumber = isValid ? cpn.normalizedNumber : phoneNumber;
		}
		if (validIndex == 0) {
			bind.isValid = false;
			return bind;
		}

		bind.originalNumber = normalizedNumber;
		bind.cpn = PhoneUtils.getContactPhoneNumber(bind.originalNumber);
		bind.isValid = bind.cpn.isValid;

		if (!bind.isValid) {
			return bind;
		}

		bind.hasContact = (c.getLong(MESSAGES_BIND_HAS_CONTACT_INDX) > 0);

		if (bind.hasContact) {
			bind.phoneId = c.getLong(MESSAGES_BIND_ID_INDX);
			bind.displayName = c.getString(MESSAGES_BIND_DISPLAY_NAME_INDX);
			bind.isPersonalContact = (c
					.getLong(MESSAGES_BIND_IS_PERSONAL_CONTACT_INDX) > 0);
			bind.phoneNumber = c.getString(MESSAGES_BIND_ORIGINAL_NUMBER_INDX);
		}

		return bind;
	}

	public static final class MessageItem {

		public long msgId;
		public String displayName;
		public boolean isValidNumber;
		public ContactPhoneNumber cpn;
		public long timeInMillis;
		public Date dateTime;
		public int duration;
		public int readStatus;
		public int locallyDeleted;
		public String fileExtension;
		public MessageType messageType;
		public int syncStatus;
		public String name;
		public ContactPhoneNumber receiver;

	}

	/**
	 * Reads a Messageitem from cursor with {@link #MESSAGES_SUMMARY_PROJECTION}
	 * projection.
	 * 
	 * @param context
	 *            the execution context
	 * @param c
	 *            the cursor (retrieved with
	 *            {@link #MESSAGES_SUMMARY_PROJECTION} projection)
	 * @return the item
	 */
	public static final MessageItem readMessageItem(Context context, Cursor c) {
		MessageItem item = new MessageItem();

		final long rcmIsValidNumber = c
				.getLong(MessagesQuery.MESSAGES_RCM_FROM_PHONE_IS_VALID_NUMBER_INDX);

		if (rcmIsValidNumber == -1) {
			final String phoneNumber = c
					.getString(MESSAGES_JEDI_FROM_PHONE_INDX);
			ContactPhoneNumber cpn = PhoneUtils
					.getContactPhoneNumberWithoutTrim(phoneNumber);
			if (cpn == null || !cpn.isValid) {
				cpn = PhoneUtils.getContactPhoneNumber(phoneNumber);
			}
			final boolean isValid = (cpn != null && cpn.isValid);
			MessageUtils.updateValidBind(context, c.getLong(MESSAGES_ID_INDX),
					isValid, isValid ? cpn.normalizedNumber : phoneNumber);
			item.cpn = cpn;
			item.isValidNumber = isValid;
		} else {
			boolean isValidNumber = (rcmIsValidNumber > 0);

			if (isValidNumber) {
				item.cpn = PhoneUtils
						.getContactPhoneNumber(c
								.getString(MessagesQuery.MESSAGES_RCM_FROM_PHONE_NORMALIZED_NUMBER_INDX));
				if (item.cpn == null || !item.cpn.isValid) {
					isValidNumber = false;
				}
			}

			item.isValidNumber = isValidNumber;
		}

		String toPhone = c.getString(MessagesQuery.MESSAGES_JEDI_TO_PHONE_INDX);
		int np = toPhone.indexOf('|');
		if (np != -1) {
			item.receiver = PhoneUtils.getContactPhoneNumber(toPhone.substring(
					0, np));
		} else {
			item.receiver = PhoneUtils.getContactPhoneNumber(toPhone);
		}

		String oldDisplayName = c
				.getString(MessagesQuery.MESSAGES_BIND_DISPLAY_NAME_INDX);
		if (c.getLong(MessagesQuery.MESSAGES_BIND_HAS_CONTACT_INDX) > 0) {
			item.name = oldDisplayName;
			item.displayName = item.name;
		}

		if (item.name == null || item.name.trim().length() == 0) {
			item.name = c.getString(MessagesQuery.MESSAGES_JEDI_FROM_NAME_INDX);
			item.displayName = item.name;

			if (item.name == null || item.name.trim().length() == 0) {
				if (item.isValidNumber) {
					item.displayName = item.cpn.phoneNumber.localCanonical;
				} else {
					if (item.cpn != null && item.cpn.normalizedNumber != null
							&& (item.cpn.normalizedNumber.trim().length() > 0)) {
						item.displayName = item.cpn.normalizedNumber;
					} else {
						item.displayName = context
								.getString(R.string.calllog_display_name_unknown);
					}
				}
			}
		}

		if (TextUtils.isEmpty(oldDisplayName)) {
			updateDisplayName(context, c, item.displayName);
		}

		item.msgId = c.getLong(MESSAGES_JEDI_MSG_ID_INDX);
		item.timeInMillis = c.getLong(MESSAGES_JEDI_CREATE_DATE_INDX);
		item.dateTime = new Date(item.timeInMillis);
		item.duration = c.getInt(MESSAGES_JEDI_DURATION_INDX);
		item.syncStatus = c.getInt(MESSAGES_RCM_SYNC_STATUS_INDX);
		item.readStatus = c.getInt(MESSAGES_JEDI_READ_STATUS_INDX);
		item.locallyDeleted = c.getInt(MESSAGES_RCM_LOCALLY_DELETED_INDX);
		item.fileExtension = c.getString(MESSAGES_JEDI_FILE_EXT_INDX);
		item.messageType = MessageType.findByOrdinal(c
				.getInt(MESSAGES_JEDI_MSG_TYPE_INDX));

		return item;
	}

	private static void updateDisplayName(Context context, Cursor c,
			String displayName) {
		try {
			Uri uri = UriHelper.getUri(RCMProvider.MESSAGES,
					RCMProviderHelper.getCurrentMailboxId(context),
					c.getLong(MESSAGES_ID_INDX));
			ContentValues cv = new ContentValues();
			cv.put(RCMDataStore.MessagesTable.BIND_DISPLAY_NAME, displayName);
			context.getContentResolver().update(uri, cv, null, null);
		} catch (java.lang.Throwable error) {
			if (LogSettings.MARKET) {
				MktLog.e(TAG, "updateMessagesBind error: " + error.toString());
			}
		}
	}

	static void startAsyncMsgListQuery(AsyncQueryHandler handler, int token,
			int listMode, int sortOrder) {
		if (LogSettings.MARKET) {
			MktLog.i(TAG, "startAsyncMsgListQuery(): listMode = " + listMode
					+ ", sortOrder = " + sortOrder);
		}

		handler.cancelOperation(token);
		Uri uri = UriHelper.getUri(RCMProvider.MESSAGES, RCMProviderHelper
				.getCurrentMailboxId(RingCentralApp.getContextRC()));

		String query_selection = null;
		String query_sort_order = null;

		if (listMode == Messages.MODE_RECENT) {
			query_selection = MessagesQuery.MESSAGES_QUERY_SELECTION_RECENT;
		} else if (listMode == Messages.MODE_DELETED) {
			query_selection = MessagesQuery.MESSAGES_QUERY_SELECTION_DELETED;
		}

		if (sortOrder == Messages.SORT_ORDER_DATE) {
			query_sort_order = MessagesQuery.MESSAGES_QUERY_SORT_ORDER_BY_DATE;
		} else if (sortOrder == Messages.SORT_ORDER_SENDER) {
			query_sort_order = MessagesQuery.MESSAGES_QUERY_SORT_ORDER_BY_SENDER;
		}

		if (query_selection != null && query_sort_order != null) {
			handler.startQuery(token, null, uri,
					MessagesQuery.MESSAGES_SUMMARY_PROJECTION, query_selection,
					null, query_sort_order);
		}
	}

}
