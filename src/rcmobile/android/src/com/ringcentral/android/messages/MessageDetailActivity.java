/** 
 * Copyright (C) 2011-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.messages;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ActivityNotFoundException;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.ContentObserver;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.api.pojo.MessagePOJO;
import com.ringcentral.android.api.pojo.MessagePOJO.MessageType;
import com.ringcentral.android.contacts.Cont;
import com.ringcentral.android.contacts.Cont.acts.BindSync;
import com.ringcentral.android.contacts.Cont.acts.ContactBinding;
import com.ringcentral.android.contacts.LocalSyncService;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.messages.MessagesHandler.MessageBodyLoader;
import com.ringcentral.android.messages.MessagesQuery.MessageItem;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.ringout.RingOutAgent;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.DeviceUtils;
import com.ringcentral.android.utils.FileUtils;
import com.ringcentral.android.utils.LabelsUtils;
import com.ringcentral.android.utils.NetworkUtils;
import com.ringcentral.android.utils.RCMMediaPlayer;
import com.ringcentral.android.utils.RestartAppIntentReceiver;
import com.ringcentral.android.utils.SettingsFormatUtils;
import com.ringcentral.android.utils.StorageStateReceiver;
import com.ringcentral.android.utils.ui.widget.RMenuWindow.DropdownMenu;
import com.ringcentral.android.utils.ui.widget.RMenuWindow.DropdownMenu.TopMenuItem;

public class MessageDetailActivity extends Activity {

	private static final String TAG = "[RC]MessageDetailActivity";

	private static final int MESSAGE_LIST_INITIAL_QUERY_TOKEN = 1;
	private static final int MESSAGE_LIST_REQUERY_TOKEN = 2;

    
    private RestartAppIntentReceiver restartReceiver;
    private RingOutAgent mRingOutAgent;
    
    private HeadsetStateReceiver mHeadsetStateReceiver;

    private long mMessageId;
    private int mMessagePosition;   //position (number) of the message in the list
    private int mMessagesCount;
    private int  mListMode;
    private int  mSortOrder;

    private MessageItem mMessageItem;

    private Cursor mCursor;
    
    private MessageListQueryHandler mQueryHandler;
    private MessageListContentObserver mMessageListContentObserver;
    
    private ImageButton mBtnLeft;
    private ImageButton mBtnRight;
    private Button mBtnBack; 
    private ImageButton mBtnMenu;
   // private TopMenuDialogTest mTopMenuDialogTest;
    //private TopMenuDialog topMenu;
    private DropdownMenu dropdownMenu;
    private ArrayList<TopMenuItem> topMenuList;
    private View mBottomBar;
    
    private boolean isNoSDCardWarningShow = false;

    /**
     * Keeps the local synchronization service handle.
     */
    private LocalSyncService.LocalSyncBinder syncService;
    
    /**
     * Keeps the connection to the local synchronization service.
     */
    private ServiceConnection syncSvcConn = new ServiceConnection() {
    	
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            syncService = (LocalSyncService.LocalSyncBinder) rawBinder;
            if (LogSettings.ENGINEERING) {
                EngLog.d(TAG, "Sync service connected.");
            }
            try {
                syncService.syncMessages(RCMProviderHelper.getCurrentMailboxId(syncService.getService()));
            } catch (java.lang.Throwable error) {
                if (LogSettings.MARKET) {
                    QaLog.e(TAG, "onServiceConnected() call sync failed: ", error);
                }
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            syncService = null;
            if (LogSettings.MARKET) {
                EngLog.d(TAG, "Sync service disconnected.");
            }
        }
    };
    
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        getApplicationContext().bindService(new Intent(this, LocalSyncService.class), syncSvcConn, BIND_AUTO_CREATE);
        
        RCMMediaPlayer.getInstance().backupAudioState();

        mMessageId = getIntent().getLongExtra(RCMConstants.EXTRA_MESSAGE_ID, 0);
        mMessagePosition = getIntent().getIntExtra(RCMConstants.EXTRA_MESSAGE_POSITION, 0);
        mMessagesCount = getIntent().getIntExtra(RCMConstants.EXTRA_MESSAGES_COUNT, 1);
        mListMode = getIntent().getIntExtra(RCMConstants.EXTRA_MESSAGES_LIST_MODE, Messages.MODE_RECENT);
        mSortOrder = getIntent().getIntExtra(RCMConstants.EXTRA_MESSAGES_SORT_ORDER, Messages.SORT_ORDER_DATE);
        
        if (LogSettings.MARKET) {
            EngLog.i(TAG, "onCreate(): mMessageId = " + mMessageId + "; mSortingOrder = " + mSortOrder);
        }
        
        mCursor = getContentResolver().query(
                UriHelper.getUri(RCMProvider.MESSAGES, RCMProviderHelper.getCurrentMailboxId(this)),
                MessagesQuery.MESSAGES_SUMMARY_PROJECTION,
                RCMDataStore.MessagesTable.JEDI_MSG_ID + " = ? ",
                new String[]{String.valueOf(mMessageId)}, null);
        
        if (mCursor == null || !mCursor.moveToFirst()) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "onCreate(): empty cursor; finish");
            }
            
            if (mCursor != null) {
                mCursor.close();
            }
            
            finish();
            return;
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        buildLayout(true, DeviceUtils.getDisplayOrientation(this));
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.bar); 
        dropdownMenu = new DropdownMenu(MessageDetailActivity.this);
        mBtnLeft = (ImageButton) findViewById(R.id.btnLeft);
        mBtnRight = (ImageButton) findViewById(R.id.btnRight);
        mBtnBack = (Button)findViewById(R.id.btnTopLeft);
        mBtnMenu = (ImageButton) findViewById(R.id.btnTopRight);
        mBottomBar = findViewById(R.id.RLBar); 
		
        mBtnLeft.setVisibility(View.INVISIBLE);
        mBtnRight.setVisibility(View.INVISIBLE);
        mBtnBack.setVisibility(View.VISIBLE);
        mBtnMenu.setVisibility(View.VISIBLE);
		mBtnBack.setText(R.string.topleftback);
        
        mMessageListContentObserver = new MessageListContentObserver();
        
        mHeadsetStateReceiver = new HeadsetStateReceiver();
        registerReceiver(mHeadsetStateReceiver, new IntentFilter(Intent.ACTION_HEADSET_PLUG));
        
        restartReceiver = RestartAppIntentReceiver.registerRestartEvent(this);
        
        setListeners();
        updateView(true, true);

        mQueryHandler = new MessageListQueryHandler(this);
        startAsyncMsgListQuery(mQueryHandler, MESSAGE_LIST_INITIAL_QUERY_TOKEN);
        
        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
            PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.VOICEMAIL_OPEN);
    	}
        
        setReadStatus();
        
        mRingOutAgent = new RingOutAgent(this);               
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "onDestroy()");
        }
        
        if (syncService != null) {
            getApplicationContext().unbindService(syncSvcConn);
            syncService = null;
            syncSvcConn = null;
        }
        
        if(restartReceiver != null){
        	restartReceiver.destroy();
        	restartReceiver = null;
        }
        
        if(mHeadsetStateReceiver != null){
        	unregisterReceiver(mHeadsetStateReceiver);
        	mHeadsetStateReceiver = null;
        }
        
        if (mCursor != null && mMessageListContentObserver != null) {
            try {
                mCursor.unregisterContentObserver(mMessageListContentObserver);
            } catch (Throwable e) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "onDestroy(): unregisterContentObserver() failed: ", e);
                }
            }
            mMessageListContentObserver = null;
        }

        mRingOutAgent = null;
        RCMMediaPlayer.getInstance().stop();
        RCMMediaPlayer.getInstance().restoreAudioState();
        
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "onPause()");
        }
        RCMMediaPlayer.getInstance().stop();
        updateView(false, true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "onResume()");
        }        
        FlurryTypes.onEventScreen(FlurryTypes.EVENT_VIEW_MESSAGE, FlurryTypes.SCR_MESSAGES_VOICEMAIL);
        
        if (syncService != null) {
            try {
                syncService.syncMessages(RCMProviderHelper.getCurrentMailboxId(this));
            } catch (java.lang.Throwable error) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "onResume() call sync failed: ", error);
                }
            }
        }
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	FlurryTypes.onStartSession(this);
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	FlurryTypes.onEndSession(this);
    }
    
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
    	if (event.getKeyCode() == KeyEvent.KEYCODE_MENU && event.getAction() == KeyEvent.ACTION_DOWN) {
    		updateMenuItem(); 
    		dropdownMenu.setDropdownList(topMenuList);
    		dropdownMenu.show(mBottomBar); 
            return true;
        }
    	else if (event.getKeyCode() == KeyEvent.KEYCODE_SEARCH) {
    		return true;
    	}
    	else {
        	return super.dispatchKeyEvent(event);
        }
    }
    
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//    	if (keyCode == KeyEvent.KEYCODE_CALL) {
//    		MessagePOJO message = getMessage();
//    		if (message != null && message.isFromNameValid()){
//    			FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, FlurryTypes.SCR_MESSAGES_VOICEMAIL);
//    			MessageUtils.callBack(MessageDetailActivity.this, mRingOutAgent, message);
//    		}
//    		return true;
//    	}
//    	return super.onKeyDown(keyCode, event);
//    }
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
    	super.onWindowFocusChanged(hasFocus);
    	if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED && hasFocus){
    	    PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.VOICEMAIL_OPEN);
    	}
    	if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED && hasFocus){
            PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.FAX_OPEN);
        }
    }

    
    private void buildLayout(boolean onCreate, int orientation) {
        if (onCreate) {
            setContentView(R.layout.message_details_view);
        }
    }
    
    private void updateView(boolean play, boolean calledOnUserActivity) {
        if (isFinishing()) {
            return;
        }
        
        if (mCursor == null) {
            return;
        }
        MessageItem oldMessage = mMessageItem;
        mMessageItem = MessagesQuery.readMessageItem(this, mCursor);
        boolean isAnotherMsg = oldMessage == null || oldMessage.msgId != mMessageItem.msgId;
        setControls(isAnotherMsg);
        TextView title = (TextView) findViewById(R.id.title);
		 if(mMessageItem.messageType.equals(MessageType.VOICE)){ 
			title.setText(R.string.tab_name_messages);
        }else if(mMessageItem.messageType.equals(MessageType.FAX)){
        	title.setText(R.string.fax_info_title);
		}
		
        ((TextView) findViewById(R.id.message_details_name)).setText(mMessageItem.displayName);
        ((TextView) findViewById(R.id.message_details_to_name)).setText(mMessageItem.receiver.phoneNumber.localCanonical);

        String dateLabel = "Received: " + LabelsUtils.getFormatDateLabel(mMessageItem.dateTime, SettingsFormatUtils.getMessageDateFormatString())
        		+ "  " + LabelsUtils.getFormatDateLabel(mMessageItem.dateTime, SettingsFormatUtils.getMessageTimeFormatString());
        final boolean storageAccessible = FileUtils.isStorageAccessible();
        if(mMessageItem.messageType.equals(MessageType.VOICE)){ 
        	if  ((!storageAccessible)||(mMessageItem.syncStatus!= RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED)){        		
        		dateLabel += " (" + MessageUtils.getLengthLabel(mMessageItem.duration)+")";        		        
        	}
        } else if(mMessageItem.messageType.equals(MessageType.FAX)){
        	TextView pagesTextView = (TextView) findViewById(R.id.message_details_pages);
        	pagesTextView.setVisibility(View.VISIBLE);
        	if(mMessageItem.duration <= 1){
        		pagesTextView.setText(mMessageItem.duration + " " + "page");
        	} else{
        		pagesTextView.setText(mMessageItem.duration + " " + "pages");
        	}
        }
        
        ((TextView) findViewById(R.id.message_details_date)).setText(dateLabel);                		                        
        ((TextView) findViewById(R.id.message_counter)).setText((mMessagePosition + 1)  + " of " + mMessagesCount);

        boolean canPlay = true;
        if (!calledOnUserActivity) {
            canPlay = !isApplicationBroughtToBackground(this) && hasWindowFocus();
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "canPlay=" + canPlay);
            }
        }
        
        if (play && canPlay) {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "Playing");
            }
            if (mMessageItem != null && mMessageItem.messageType == MessagePOJO.MessageType.FAX 
            		&& mMessageItem.readStatus == MessagePOJO.UNREAD){
            	FlurryTypes.onEvent(FlurryTypes.FAX_VIEWED);
            }
            if (mMessageItem != null && !storageAccessible && !isNoSDCardWarningShow) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "SD Card error dialog showing");
                }
                RcAlertDialog.getBuilder(MessageDetailActivity.this).setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setTitle(R.string.messages_storage_error_title).setMessage(StorageStateReceiver.getStateMessage(this)).show();
                isNoSDCardWarningShow=true;
            } else if (mMessageItem != null && mMessageItem.messageType == MessagePOJO.MessageType.VOICE
                    && mMessageItem.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED) {
            	if (mMessageItem.readStatus == MessagePOJO.UNREAD){
            		FlurryTypes.onEvent(FlurryTypes.VOICEMAIL_PLAYED);
            	}
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "Auto-playing");
                }
                findViewById(R.id.playback_btnPlay).performClick();
            }
        } else {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "No playing.");
            }
        }
    }
    

    private void setReadStatus(){
        int read_status = mMessageItem.readStatus;
        boolean network_available = NetworkUtils.isRCAvaliable(this);
        
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "setReadStatus(): msg read status: " + read_status + "; network_available: " + network_available);
        }
        
        if (read_status == MessagePOJO.UNREAD && network_available) {
            MessagesHandler.changeReadStatus(this, mMessageItem.msgId);
        }
    }
    
    
    private void setControls(boolean isAnotherMsg) {
    	final boolean storageAccessible = FileUtils.isStorageAccessible(); 
        if (isAnotherMsg){
            RCMMediaPlayer.getInstance().stop();
            ((SeekBar) findViewById(R.id.playback_seekbar)).setProgress(0);
        }

        findViewById(R.id.btnCall).setEnabled(mMessageItem.isValidNumber);

        if ((mMessageItem.messageType == MessagePOJO.MessageType.VOICE)) {
        	setVoiceMessageControls(isAnotherMsg, storageAccessible);
        } else {
            setFaxMessageControls(storageAccessible);
        }
    }

	private void setVoiceMessageControls(boolean isAnotherMsg, final boolean storageAccessible) {
		setViewLoadAndLoading2Gone();
		TextView totalDur = (TextView) findViewById(R.id.txtTotalDuration);
		totalDur.setText(RCMMediaPlayer.formatDuration(mMessageItem.duration * 1000)); // in milliseconds
		totalDur.setVisibility(View.VISIBLE);
		
		if (isAnotherMsg) {
		    TextView playedDur = (TextView) findViewById(R.id.txtPlayedDuration);
		    playedDur.setText(RCMMediaPlayer.formatDuration(0));
		    playedDur.setVisibility(View.VISIBLE);
		}
		
		findViewById(R.id.playback_sdcard_error).setVisibility(View.GONE);
		findViewById(R.id.btnSpeaker).setEnabled(!mHeadsetStateReceiver.isConnected());
		if (storageAccessible) {
		    if (mMessageItem.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED) {
		    	
		        setPlaybackSpeakerVisibility(View.VISIBLE, View.VISIBLE);
		        setBtnSpeakerCompoundDrawablesWithIntrinsicBounds();
		        setLoadAndLoading2Gone();
		        
		        if (!RCMMediaPlayer.getInstance().isPlaying()){
		            findViewById(R.id.playback_btnPlay).setBackgroundResource(R.drawable.btn_player_play);
		        }
		    } else if (checkMessageDownloading()) {	// DK: check if message is on downloading list 
		    	
		        setPlaybackSpeakerVisibility(View.INVISIBLE, View.GONE);
		        setLoadAndLoadingVisibility(View.GONE, View.VISIBLE);
		        
		    } else if (mMessageItem.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_NOT_LOADED) {
		        setPlaybackSpeakerVisibility(View.INVISIBLE, View.GONE);
		        setViewLoadAndLoadingVisibility(View.GONE, View.VISIBLE, View.GONE);
		        findViewById(R.id.btnLoad).setEnabled(true);
		        findViewById(R.id.btnLoad).setClickable(true);
		    }
		} else {
		    setLoadAndLoading2Gone();
		    setPlaybackSpeakerVisibility(View.INVISIBLE, View.VISIBLE);
		    findViewById(R.id.btnSpeaker).setEnabled(false);
		    findViewById(R.id.playback_sdcard_error).setVisibility(View.VISIBLE);
		    setPlaybackSdcardErrorText();
		}
	}

	private boolean checkMessageDownloading() {
		return mMessageItem.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING ||
				MessagesHandler.isOnList(this, mMessageItem.msgId, MessageBodyLoader.sf_LOAD_PRIORITY_HIGH) || 
				MessagesHandler.isOnList(this, mMessageItem.msgId, MessageBodyLoader.sf_LOAD_PRIORITY_NORMAL);
	}
	
	private void setFaxMessageControls(final boolean storageAccessible) {
		setPlaybackSpeakerVisibility(View.INVISIBLE, View.GONE);
		findViewById(R.id.playback_sdcard_error).setVisibility(View.GONE);
		findViewById(R.id.btnView).setEnabled(true);
		if (storageAccessible) {
		    if (mMessageItem.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED) {
		        setViewLoadAndLoadingVisibility(View.VISIBLE, View.GONE, View.GONE);
		    }
			else if (checkMessageDownloading()) {
		    	setViewLoadAndLoadingVisibility(View.GONE, View.GONE, View.VISIBLE);
			}
		    else if (mMessageItem.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_NOT_LOADED) {
		        
		        setViewLoadAndLoadingVisibility(View.GONE, View.VISIBLE, View.GONE);
		        findViewById(R.id.btnLoad).setEnabled(true);
		        findViewById(R.id.btnLoad).setClickable(true);
		    }
		} else {
			if (mMessageItem.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED) {
		        setViewLoadAndLoadingVisibility(View.VISIBLE, View.GONE, View.GONE);
		        findViewById(R.id.btnView).setEnabled(false);
			}
			else {
		        setViewLoadAndLoadingVisibility(View.GONE, View.VISIBLE, View.GONE);
		        findViewById(R.id.btnLoad).setEnabled(false);
			}
			findViewById(R.id.playback_sdcard_error).setVisibility(View.VISIBLE);
			((TextView) findViewById(R.id.playback_sdcard_error_text)).setText(getString(R.string.messages_storage_cannot_load_fax));
		}
	}

	private void setPlaybackSdcardErrorText() {
		((TextView) findViewById(R.id.playback_sdcard_error_text)).setText(mMessageItem.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED ? 
				getString(R.string.messages_storage_cannot_playback) : getString(R.string.messages_storage_cannot_load));
	}

	private void setBtnSpeakerCompoundDrawablesWithIntrinsicBounds() {
		if (RCMMediaPlayer.getInstance().isSpeakerOn()) {
		    ((TextView) findViewById(R.id.btnSpeaker)).setCompoundDrawablesWithIntrinsicBounds(null, 
		    		MessageDetailActivity.this.getResources().getDrawable(R.drawable.player_action_speaker), null, null);
		} else {
		    ((TextView) findViewById(R.id.btnSpeaker)).setCompoundDrawablesWithIntrinsicBounds(null, 
		    		MessageDetailActivity.this.getResources().getDrawable(R.drawable.player_action_speaker_off), null, null);
		}
	}
    
    private void setViewLoadAndLoading2Gone() {
		setViewLoadAndLoadingVisibility(View.GONE, View.GONE, View.GONE);
	}

	private void setViewLoadAndLoadingVisibility(int viewVisibility, int loadVisibility, int loadingVisibility) {
		findViewById(R.id.btnView).setVisibility(viewVisibility);
		findViewById(R.id.btnLoad).setVisibility(loadVisibility);
		findViewById(R.id.proBarLoading).setVisibility(loadingVisibility);
	}

	private void setLoadAndLoading2Gone() {
		setLoadAndLoadingVisibility(View.GONE, View.GONE);
	}

	private void setLoadAndLoadingVisibility(int loadVisibility, int loadingVisibility) {
		findViewById(R.id.btnLoad).setVisibility(loadVisibility);
		findViewById(R.id.proBarLoading).setVisibility(loadingVisibility);
	}

	private void setPlaybackSpeakerVisibility(int playbackVisibility, int speakerVisibility) {
		findViewById(R.id.playback_controls).setVisibility(playbackVisibility);
		findViewById(R.id.btnSpeaker).setVisibility(speakerVisibility);
	}

    private void setListeners() {
        mBtnLeft.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Previous message");
                }
                
                if (mCursor == null || mCursor.getCount() <= 1) {    //only one message
                    return;
                }
                
                if (!mCursor.moveToPrevious()) {   //we are on the first message; move to the last
                    mCursor.moveToLast();
                }
                    
                mMessageId = mCursor.getLong(MessagesQuery.MESSAGES_JEDI_MSG_ID_INDX);
                mMessagePosition = mCursor.getPosition();
                updateView(true, true);
                setReadStatus();
            }
        });

        mBtnRight.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Next message");
                }

                if (mCursor == null || mCursor.getCount() <= 1) {    //only one message
                    return;
                }
                
                if (!mCursor.moveToNext()) {   //we are on the last message; move to the first
                    mCursor.moveToFirst();
                }
                    
                mMessageId = mCursor.getLong(MessagesQuery.MESSAGES_JEDI_MSG_ID_INDX);
                mMessagePosition = mCursor.getPosition();
                updateView(true, true);
                setReadStatus();
            }
        });

        findViewById(R.id.btnCall).setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Call");
                }
                FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, FlurryTypes.SCR_MESSAGES_VOICEMAIL);
                MessageUtils.callBack(MessageDetailActivity.this, mRingOutAgent, mMessageItem, mCursor);
            }
        });


        findViewById(R.id.btnView).setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: View");
                }
                if (mMessageItem.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED) {
                    MessageUtils.viewFaxMessage(mMessageItem, MessageDetailActivity.this);
                }
            }
        });

        findViewById(R.id.playback_btnPlay).setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Play/Pause");
                }
                if (!RCMMediaPlayer.getInstance().isReady()) {
                    int duration = RCMMediaPlayer.getInstance()
                    .play(FileUtils.getPath(FileUtils.getSDPath(MessageDetailActivity.this), String.valueOf(mMessageItem.msgId), mMessageItem.fileExtension),
                            new MediaPlayer.OnCompletionListener() {

                        public void onCompletion(MediaPlayer mediaPlayer) {
                            mediaPlayer.seekTo(0);
                            ((SeekBar) findViewById(R.id.playback_seekbar)).setProgress(0);
                            findViewById(R.id.playback_btnPlay).setBackgroundResource(R.drawable.btn_player_play);
                        }
                    },
                    new MediaPlayer.OnErrorListener() {

                        public boolean onError(MediaPlayer mediaPlayer, int a, int b) {
                            findViewById(R.id.playback_btnPlay).setBackgroundResource(R.drawable.btn_player_play);
                            if (LogSettings.MARKET) {
                                MktLog.w(TAG, "Can't play message error dialog is shown");
                            }
                            RcAlertDialog.getBuilder(MessageDetailActivity.this).setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).setTitle(R.string.player_error).setMessage(R.string.cannot_play_voice_message).show();
                            return true;
                        }
                    },
                    ((SeekBar) findViewById(R.id.playback_seekbar)));
                    if (duration>=0) {
                        //-3 said, that duration should be updated the last time 
                        MessageUtils.setDurationProvidedByPlayer(MessageDetailActivity.this, mMessageItem.msgId,
                                String.valueOf(RCMMediaPlayer.trimToSeconds(duration)), -3);
                        ((SeekBar) findViewById(R.id.playback_seekbar)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                                ((TextView) findViewById(R.id.txtPlayedDuration)).setText(RCMMediaPlayer.getInstance().getCurrentPositionAtMin());
                            }

                            public void onStartTrackingTouch(SeekBar seekBar) {
                                findViewById(R.id.playback_btnPlay).setBackgroundResource(R.drawable.btn_player_play);
                                RCMMediaPlayer.getInstance().pause();
                            }

                            public void onStopTrackingTouch(SeekBar seekBar) {
                                findViewById(R.id.playback_btnPlay).setBackgroundResource(R.drawable.btn_player_pause);
                                RCMMediaPlayer.getInstance().seekTo(seekBar.getProgress());
                                RCMMediaPlayer.getInstance().resume();
                            }
                        });
                        
                        findViewById(R.id.txtTotalDuration).setVisibility(View.VISIBLE);
                        findViewById(R.id.txtPlayedDuration).setVisibility(View.VISIBLE);
                        if (duration == 0){
                            duration = 1000;
                        }
                        ((TextView) findViewById(R.id.txtTotalDuration)).setText(RCMMediaPlayer.formatDuration(duration));
                        findViewById(R.id.playback_btnPlay).setBackgroundResource(R.drawable.btn_player_pause);
                    } else {
                        if (LogSettings.MARKET) {
                            MktLog.w(TAG, "Can't play message error dialog is shown");
                        }
                        RcAlertDialog.getBuilder(MessageDetailActivity.this).setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).setTitle(R.string.player_error).setMessage(R.string.cannot_play_voice_message).show();
                    }
                } else if (RCMMediaPlayer.getInstance().isPlaying()) {
                    findViewById(R.id.playback_btnPlay).setBackgroundResource(R.drawable.btn_player_play);
                    RCMMediaPlayer.getInstance().pause();
                } else {
                    findViewById(R.id.playback_btnPlay).setBackgroundResource(R.drawable.btn_player_pause);
                    RCMMediaPlayer.getInstance().resume();
                }
            }
        });

        findViewById(R.id.btnLoad).setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Load");
                }
                
                if (mMessageItem.locallyDeleted == 1) {
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "Can't play deleted message error dialog is shown");
                    }
                    RcAlertDialog.getBuilder(MessageDetailActivity.this)
                    .setTitle(R.string.cannot_download_title)
                    .setMessage(R.string.cannot_download_deleted_message)
                    .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();

                    return;
                }

              	MessagesHandler.put(MessageDetailActivity.this, mMessageItem.msgId, MessageBodyLoader.sf_LOAD_PRIORITY_HIGH);

              	findViewById(R.id.btnLoad).setClickable(false);
              	setControls(true);
            }
        });

        findViewById(R.id.btnSpeaker).setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Speaker");
                }
                RCMMediaPlayer.getInstance().toggleSpeakerState();
                if (RCMMediaPlayer.getInstance().isSpeakerOn()) {
                	FlurryTypes.onEvent(FlurryTypes.EVENT_SPEAKER, FlurryTypes.SPEAKER_ON);
                    ((TextView) findViewById(R.id.btnSpeaker)).setCompoundDrawablesWithIntrinsicBounds(null, MessageDetailActivity.this.getResources().getDrawable(R.drawable.player_action_speaker), null, null);
                } else {
                	FlurryTypes.onEvent(FlurryTypes.EVENT_SPEAKER, FlurryTypes.SPEAKER_OFF);
                    ((TextView) findViewById(R.id.btnSpeaker)).setCompoundDrawablesWithIntrinsicBounds(null, MessageDetailActivity.this.getResources().getDrawable(R.drawable.player_action_speaker_off), null, null);
                }
            }
        });
        
        mBtnBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
        	
        });
        
        findViewById(R.id.btnTrash).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Menu: Delete");
                }
                FlurryTypes.onEvent(FlurryTypes.EVENT_DELETE_MSG);
        		MessageUtils.userActionDeleteMsg(MessageDetailActivity.this, mMessageItem.msgId, new MessageUtils.T() {
							
					@Override
					public void t() {
						if (mCursor.moveToNext() == false) {
							finish();
						}
						else{  
            				mMessageId = mCursor.getLong(MessagesQuery.MESSAGES_JEDI_MSG_ID_INDX);
            				mMessagePosition = mCursor.getPosition();
            				updateView(true, true);
            				setReadStatus();
						}
						
					}
        		});
				
			}
		});
        
        mBtnMenu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				updateMenuItem();
				dropdownMenu.setDropdownList(topMenuList);
	    		dropdownMenu.show(mBottomBar);
			}
		});
       
        
        dropdownMenu.setOnDropdownClickListener(getWindow(), new DropdownMenu.OnDropdownClickListener() {
			
			@Override
			public void onDropdownClick(DropdownMenu view, int option) {
				ContactBinding bind = MessagesQuery.readSyncBind(MessageDetailActivity.this, mCursor);
		        BindSync bsync = Cont.acts.syncBind(MessageDetailActivity.this, RCMProviderHelper.getCurrentMailboxId(MessageDetailActivity.this), bind);
		        
		        switch (option) {
		            case R.id.msg_details_add_to_contact:
		            	FlurryTypes.onEvent(FlurryTypes.EVENT_CREATE_NEW_CONTACT, FlurryTypes.SCR_MESSAGES_VOICEMAIL);
		                try {
		                startActivity(Cont.acts().getCreateNewContactIntent(MessageDetailActivity.this, mMessageItem.displayName,
		                        mMessageItem.cpn.normalizedNumber));
		                } catch (ActivityNotFoundException e) {
		                    if (LogSettings.QA) {
		                        QaLog.e(TAG, "create new contact start activity error: " + e.toString());
		                    }
		                }
		                break;
		                
		            case R.id.msg_details_add_to_existing_contact: 
		                if (LogSettings.MARKET) {
		                    QaLog.i(TAG, "User: Menu : Add to Contact");
		                }
		                if (bsync.bind.isValid) {
		                	FlurryTypes.onEventScreen(FlurryTypes.EVENT_ADD_EXISTING_CONTACT, FlurryTypes.SCR_MESSAGES_VOICEMAIL);
		                    MessageUtils.addToExistingContact(MessageDetailActivity.this, bsync);
		                }
		                break;

		    		case R.id.msg_details_add_to_favorites:
		    			if (LogSettings.MARKET) {
		    				QaLog.i(TAG, "User: Add to Favorites");
		    			}
		    			if (bsync.bind.hasContact && !bsync.bind.isPhoneNumberFavorite) {					
		    				FlurryTypes.onEventScreen(FlurryTypes.EVENT_ADD_FAVORITES, FlurryTypes.SCR_MESSAGES_VOICEMAIL);
		    			}
		    			MessageUtils.addToFavorites(MessageDetailActivity.this, mMessageItem, bsync);
		    			break;
		    			
		    		case R.id.msg_details_remove_from_favorites:
		    			
		    			if (LogSettings.MARKET) {
		                    QaLog.i(TAG, "User: Remove from Favorites");
		                }
		                if (bsync.bind.hasContact && bsync.bind.isPhoneNumberFavorite) {
		                    FlurryTypes.onEventScreen(FlurryTypes.EVENT_REMOVE_FAVORITES, FlurryTypes.SCR_MESSAGES_VOICEMAIL);                        
		                }
		                MessageUtils.removeFromFavorites(MessageDetailActivity.this, bsync);
		                break;
		                

		            case R.id.msg_details_change_read_status:
		                if (LogSettings.MARKET) {
		                    MktLog.d(TAG, "User: Menu: Mark read/unread");
		                }
		                FlurryTypes.onEvent(FlurryTypes.EVENT_MARK_UNOPENED, FlurryTypes.MARK_UNOPEN_VOICEMAIL);
		                MessageUtils.userActionMarkMsg(MessageDetailActivity.this, mMessageItem.msgId);                
		                break;
		                
		            case R.id.msg_details_forward:
		                if (LogSettings.MARKET) {
		                    MktLog.d(TAG, "User: Menu: Forward");
		                }
		                MessageUtils.userActionForwardMSG(MessageDetailActivity.this, mMessageItem);
		                break;
		        }
			}
    		
    	});

   }
    
    private void updateMenuItem(){
    	topMenuList = new ArrayList<TopMenuItem>();
    	topMenuList.add(new TopMenuItem(getString(R.string.messageview_menu_forward),R.id.msg_details_forward));
    	if (mMessageItem.readStatus == MessagePOJO.READ) {
    		topMenuList.add(new TopMenuItem(getString(R.string.messageview_mark_unopened),R.id.msg_details_change_read_status));
        } else {
        	topMenuList.add(new TopMenuItem(getString(R.string.messageview_mark_opened),R.id.msg_details_change_read_status));
        }
    	 //AB-349 implementation
        ContactBinding bind = MessagesQuery.readSyncBind(this, mCursor);
        BindSync bsync = Cont.acts.syncBind(this, RCMProviderHelper.getCurrentMailboxId(this), bind);
        if(!bsync.bind.hasContact){
        	topMenuList.add(new TopMenuItem(getString(R.string.messageview_menu_contact),R.id.msg_details_add_to_contact));
        	topMenuList.add(new TopMenuItem(getString(R.string.messageview_menu_add_to_existing),R.id.msg_details_add_to_existing_contact));
        }
        if(bsync.bind.hasContact && bsync.bind.isPhoneNumberFavorite){
        	topMenuList.add(new TopMenuItem(getString(R.string.messageview_menu_remove_from_favorites),R.id.msg_details_remove_from_favorites));
        }
        else {
        	topMenuList.add(new TopMenuItem(getString(R.string.messageview_menu_favorites),R.id.msg_details_add_to_favorites));
        }
    }

//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//
//        if (LogSettings.ENGINEERING)
//            EngLog.i(TAG, "onConfigurationChanged(): orientation = " + newConfig.orientation);
//
//        int orientation = newConfig.orientation != Configuration.ORIENTATION_UNDEFINED ?
//                newConfig.orientation : DeviceUtils.calcDisplayOrientation(this);
//        
//        if (orientation != mDisplayOrientation) {
//            buildLayout(false, orientation);
//        }
//    }
    
    private static boolean isApplicationBroughtToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }

        return false;
    }
    
    private void startAsyncMsgListQuery(AsyncQueryHandler handler, int token) {
        MessagesQuery.startAsyncMsgListQuery(handler, token, mListMode, mSortOrder);
    }
    
    public int getMessagePosition() { 
        return mMessagePosition;
    }
    
    
/////////////////////////////////////////////////////////////////////////////////////
    
    private final class MessageListQueryHandler extends AsyncQueryHandler {

        public MessageListQueryHandler(Context context) {
            super(context.getContentResolver());
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            if (LogSettings.MARKET) {
                QaLog.d(TAG, "onQueryComplete(token = " + token + ")");
            }
            
            if (isFinishing()) {
                if (LogSettings.MARKET) {
                    QaLog.w(TAG, "onQueryComplete(): activity is finishing; return");
                }
                
                return;
            }
            
            if (mCursor != null && !mCursor.isClosed()) {
                mCursor.close();
            }
            
            mCursor = cursor;
            
            if (mCursor == null || !mCursor.moveToFirst()) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "onQueryComplete(): empty cursor; finish");
                }
                if (mCursor != null) {
                    mCursor.close();
                }
                
                finish();
                return;
            }

            do {
                if (mCursor.getLong(MessagesQuery.MESSAGES_JEDI_MSG_ID_INDX) == mMessageId) {
                    break;
                }
            } while (mCursor.moveToNext());
            
            if (mCursor.isAfterLast()) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "onQueryComplete(): current message has been deleted externally; finish");
                }
                
                mCursor.close();
                finish();
                
                return;
            }

            mCursor.registerContentObserver(mMessageListContentObserver);
                
            if (mCursor.getCount() <= 1 ) {      //only one message in the list, hide message switching control
            	mBtnLeft.setVisibility(View.INVISIBLE);
            	mBtnRight.setVisibility(View.INVISIBLE);
            } else {
            	mBtnLeft.setVisibility(View.VISIBLE);
                mBtnRight.setVisibility(View.VISIBLE);
            }
            
            mMessagePosition = mCursor.getPosition(); 
            mMessagesCount = mCursor.getCount();
        
            if (token == MESSAGE_LIST_REQUERY_TOKEN) {
                updateView(false, false);
            }
        }
    }

/////////////////////////////////////////////////////////////////////////////////////
    
    private class MessageListContentObserver extends ContentObserver {

        private MessageListContentObserver() {
            super(new Handler());
        }
        
        @Override
        public boolean deliverSelfNotifications() {
            return true;
        } 

        @Override
        public void onChange(boolean selfChange) {
            if (!isFinishing()) {
                startAsyncMsgListQuery(mQueryHandler, MESSAGE_LIST_REQUERY_TOKEN);
            }
        }
    }
    
/////////////////////////////////////////////////////////////////////////////////////

    private class HeadsetStateReceiver extends BroadcastReceiver {
    	private static final String EXTRA_HEADSET_STATE = "state";
    	private static final String EXTRA_HEADSET_NAME = "name";
    	
    	private boolean mConnected = false;    	
    	
    	public boolean isConnected(){
			return mConnected;				
    	}

		@Override
		public void onReceive(Context context, Intent intent) {
			if(Intent.ACTION_HEADSET_PLUG.equals(intent.getAction())){
				final int headsetState = intent.getIntExtra(EXTRA_HEADSET_STATE, 0);
				final String headsetName = intent.getStringExtra(EXTRA_HEADSET_NAME);
				if(LogSettings.MARKET) {
					MktLog.d(TAG, "HeadsetReceiver.onReceive(), name : " + headsetName + (headsetState == 0 ? ", disconnected" : ", connected"));
				}				
				mConnected = (headsetState == 1);
				
				MessageDetailActivity.this.findViewById(R.id.btnSpeaker).setEnabled(!isConnected());
			}			
		}
    	
    }

}
