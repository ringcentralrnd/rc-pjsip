/** 
 * Copyright (C) 2011-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.messages;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.AbsListView.OnScrollListener;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.api.network.NetworkManager;
import com.ringcentral.android.api.pojo.MessagePOJO;
import com.ringcentral.android.api.pojo.MessagePOJO.MessageType;
import com.ringcentral.android.contacts.Cont;
import com.ringcentral.android.contacts.ContactPhoneNumber;
import com.ringcentral.android.contacts.LocalSyncService;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.contacts.Cont.acts.BindSync;
import com.ringcentral.android.contacts.Cont.acts.ContactBinding;
import com.ringcentral.android.messages.MessagesQuery.MessageItem;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.ringout.RingOutAgent;
import com.ringcentral.android.settings.DNDialog;
import com.ringcentral.android.settings.SettingsLauncher;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.LabelsUtils;
import com.ringcentral.android.utils.NetworkUtils;
import com.ringcentral.android.utils.UserInfo;

public class Messages extends ListActivity implements View.OnCreateContextMenuListener {

	private static final String TAG = "[RC]Messages";
    
    public static final int MODE_RECENT = 1;
    public static final int MODE_DELETED = 2;

    public static final String MESSAGE_LIST_UPDATE_BROADCAST_EXTRA = "com.ringcentral.android.messages.Messages.broadcast_extra_info";   

    private static final String ACTION_SWITCH_MESSAGES_TAB = "com.ringcentral.android.messages.ACTION_SWITCH_MESSAGES_TAB";
    private static final String MESSAGE_TAB = "MESSAGE_TAB"; 
    
    private static final int MENU_ITEM_SORT = 1;
    private static final int MENU_ITEM_DELETE_ALL = 2;
    private static final int MENU_ITEM_MARK = 3;
    private static final int MENU_ITEM_CALL = 4;
    private static final int MENU_ITEM_DELETE = 5;
    private static final int MENU_ITEM_SHOW_DELETED = 6;
    private static final int MENU_ITEM_SHOW_NORMAL = 7;
    private static final int MENU_ITEM_CLEAR_DELETED = 8;
    private static final int MENU_ITEM_FORWARD = 9;
    private static final int MENU_ITEM_CREATE_NEW_CONTACT = 10;
    private static final int MENU_ITEM_ADD_TO_EXISTING_CONTACT = 11;
    private static final int MENU_ITEM_ADD_TO_FAVORITES = 12;
    private static final int MENU_ITEM_VIEW_CONTACT = 13;
    private static final int MENU_ITEM_REMOVE_FROM_FAVORITES = 14;

    private static final int MESSAGES_LIST_RECEIVED = 0;
    private static final int MESSAGES_LIST_RECEIVED_EMPTY = 1;
    private static final int MESSAGES_LIST_NOT_LOADED = 2;

    private static final int MESSAGES_LIST_VIEW_STATE_NO_MESSAGES 		= 0;
    private static final int MESSAGES_LIST_VIEW_STATE_LOADING_MESSAGES 	= 1;
    private static final int MESSAGES_LIST_VIEW_STATE_MESSAGES 			= 2;
    
    private static final int QUERY_TOKEN = 106;
    
    //Items' indexes  in the Sort dialog
    //Values must match the items' positions the "messages_sort_dialog_items" array in strings.xml
    private static final int MENU_SORT_ITEM_SENDER = 0;
    private static final int MENU_SORT_ITEM_DATE = 1;

    static final int SORT_ORDER_SENDER = MENU_SORT_ITEM_SENDER;
    static final int SORT_ORDER_DATE = MENU_SORT_ITEM_DATE;
    
    private ToggleButton btnRecent;
    private ToggleButton btnDeleted;
    private LinearLayout mSubTabs;

    private View noMessagesIndication;
    private View messagesLoadingIndication;

    private int mSortOrder;

    private ListView listView;
    private MessagesListAdapter mAdapter;

    private RingOutAgent mRingOutAgent;
    private MessageListChangeReceiver m_MessageListChangeReceiver;
    private MessageTabChangeReceiver mMessageTabChangedReceiver;


    /**
     * Keeps the local synchronization service handle.
     */
    private LocalSyncService.LocalSyncBinder syncService;
    
    /**
     * Keeps query handler.
     */
    private QueryHandler mQueryHandler;

    
    /**
     * Keeps the connection to the local synchronization service.
     */
    private ServiceConnection syncSvcConn = new ServiceConnection() {
    	
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            syncService = (LocalSyncService.LocalSyncBinder) rawBinder;
            if (LogSettings.ENGINEERING) {
                EngLog.d(TAG, "Sync service connected.");
            }
            syncMessages(syncService.getService());
        }

        public void onServiceDisconnected(ComponentName className) {
            syncService = null;
            if (LogSettings.MARKET) {
                EngLog.d(TAG, "Sync service disconnected.");
            }
        }
    };

    public interface INTENT_EXTRA_TYPES {
        public static final int NOT_SET = 0;
        public static final int READ_STATUS_CHANGED = 1;
        public static final int SYNC_STATUS_CHANGED = 2;
        public static final int ADD_MESSAGES = 3;
        public static final int DELETE_CURRENT_MAILBOX = 4;
        public static final int MOVE_MESSAGE_TO_DELETED = 5;
        public static final int REMOVE_ALL_DELETED = 6;
        public static final int REMOVE_DELETED = 7;
        public static final int STORAGE_STATE_CHANGED = 8;
        public static final int BIND_STATE_CHANGED = 9;
    }

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "onCreate()");
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.messages);

        mSortOrder = SORT_ORDER_DATE;
        
        initSubTabs();
        initViews();

        mQueryHandler = new QueryHandler(this);

        mAdapter = new MessagesListAdapter();
        setListAdapter(mAdapter);
        
        setViewState(MESSAGES_LIST_VIEW_STATE_LOADING_MESSAGES);
        updateMessagesList(null);

        m_MessageListChangeReceiver = new MessageListChangeReceiver();
        IntentFilter intentFilter = new IntentFilter(RCMConstants.ACTION_UPDATE_MESSAGES_LIST);
        registerReceiver(m_MessageListChangeReceiver, intentFilter);
        
        mMessageTabChangedReceiver = new MessageTabChangeReceiver();
        registerReceiver(mMessageTabChangedReceiver, new IntentFilter(ACTION_SWITCH_MESSAGES_TAB));

        mRingOutAgent = new RingOutAgent(this);
        
        getApplicationContext().bindService(new Intent(this, LocalSyncService.class), syncSvcConn, BIND_AUTO_CREATE);        
    }

    private boolean hasDeleted() {
        return (RCMProviderHelper.getMessagesLocallyDeletedCount(this) > 0);
    }
    
    @Override
    protected void onResume() {        
        super.onResume();
        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "onResume()");
        }        
        
        MessagesNotification.visitRecent(this, (btnRecent != null) && btnRecent.isChecked() );
        
        FlurryTypes.onEventScreen(FlurryTypes.EVENT_SCREEN_OPEN, btnRecent.isChecked()?
        		FlurryTypes.SCR_MESSAGES_RECENT:
        		FlurryTypes.SCR_MESSAGES_DELETED);
        NetworkManager.getInstance().refreshMessages(this, false);
        syncMessages(this);
    }    
    
    @Override
    protected void onPause() {
    	super.onPause();
    	 if (LogSettings.ENGINEERING) {
             EngLog.d(TAG, "onPause()");
         }
    	MessagesNotification.visitRecent(Messages.this, false);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "onDestroy()");
        }                
        
        if (m_MessageListChangeReceiver != null) {
            unregisterReceiver(m_MessageListChangeReceiver);
            m_MessageListChangeReceiver = null;
        }
        if (mMessageTabChangedReceiver != null) {
        	unregisterReceiver(mMessageTabChangedReceiver);
        	mMessageTabChangedReceiver = null;
        }
        if (listView != null) {
        	listView.setAdapter(null);
        	listView.setOnCreateContextMenuListener(null);
        	listView.setOnItemClickListener(null);
        	listView = null;
        }
        mAdapter = null;        
        mRingOutAgent = null;                
        
        try {
            if (syncService != null) {
                getApplicationContext().unbindService(syncSvcConn);
                syncService = null;
                syncSvcConn = null;
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                QaLog.e(TAG, "onDestroy() error: " + error.getMessage());
            }
        }
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	FlurryTypes.onStartSession(this);
    }
    
    @Override
    protected void onStop() {
    	super.onStop();    	
    	FlurryTypes.onEndSession(this);    	      	
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_CALL) {
//            Object messObj = listView.getSelectedItem();
//            if (messObj != null && messObj instanceof MessagePOJO ){
//                MessagePOJO message = (MessagePOJO)messObj;
//                if (message.isFromNameValid()){
//                    FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, btnRecent.isChecked()?
//                            FlurryTypes.SCR_MESSAGES_RECENT_CONTEXT:
//                            FlurryTypes.SCR_MESSAGES_DELETED_CONTEXT);
//                    MessageUtils.callBack(Messages.this, mRingOutAgent, message);
//                }
//                return true;
//            }
//        }
//        return super.onKeyDown(keyCode, event);
//    }
    
//    @Override
//    protected void onListItemClick(ListView l, View v, int position, long id) {
//        if (LogSettings.MARKET)  {
//            QaLog.d(TAG, "onListItemClick(): position = " + position);
//        }
//        
//        try {
//            Cursor cursor = (Cursor) getListAdapter().getItem(position);
//            if (cursor == null) {
//                if (LogSettings.MARKET)  {
//                    MktLog.e(TAG, "onListItemClick(): cursor is null!!!");
//                }
//                return;
//            }
//
//            MessagesQuery.MessageItem mi = MessagesQuery.readMessageItem(this, cursor);
//            if (mi == null) {
//                if (LogSettings.MARKET)  {
//                    MktLog.e(TAG, "onListItemClick(): MessageItem is null!!!");
//                }
//                return;
//            }
//            
//            FlurryTypes.onEvent(FlurryTypes.EVENT_VIEW_MESSAGE);
//            if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
//                PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.VOICEMAIL_OPEN);
//            }
//            Uri uri = UriHelper.getUri(RCMProvider.MESSAGES, RCMProviderHelper.getCurrentMailboxId(this), m);
//            Intent intent = new Intent(this, ViewCalllogDetailActivity.class);
//            intent.setData(uri);
//            try {
//                startActivity(intent);
//            } catch (ActivityNotFoundException e) {
//                if (LogSettings.ENGINEERING) {
//                    EngLog.e(TAG, "viewDetails()", e);
//                }
//            }
//
//            
//            
//            
//            
//            
//            Intent intent = new Intent();
//            intent.setClass(Messages.this, MessageDetailActivity.class);
//            
//            Bundle bundle = new Bundle
//            intent.p(CURRENT_MESSAGE, mi);
//            startActivity(intent);
//        
//        } catch (java.lang.Throwable error) {
//            if (LogSettings.MARKET) {
//                MktLog.e(TAG, "onListItemClick failed: ", error);
//            }
//        }
//    }

    
    private void initViews() {
        listView = getListView();
        listView.setOnCreateContextMenuListener(this);
        listView.setCacheColorHint(Color.TRANSPARENT);
        listView.clearDisappearingChildren();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	if (LogSettings.MARKET) {
            	    QaLog.d(TAG, "onItemClick(): position = " + position + "; id = " + id);
            	}
                
                FlurryTypes.onEvent(FlurryTypes.EVENT_VIEW_MESSAGE);
                
            	Cursor c = ((CursorAdapter) listView.getAdapter()).getCursor();
            	MessageItem mi = MessagesQuery.readMessageItem(Messages.this, c);
            	
                if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED && mi.messageType == MessagePOJO.MessageType.VOICE){
                    PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.VOICEMAIL_OPEN);
                }
                
                if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED && mi.messageType == MessagePOJO.MessageType.FAX){
                    PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.FAX_OPEN);
                }
                
            	Intent intent = new Intent();
                intent.setClass(Messages.this, MessageDetailActivity.class);
                intent.putExtra(RCMConstants.EXTRA_MESSAGE_ID, mi.msgId);
                intent.putExtra(RCMConstants.EXTRA_MESSAGE_POSITION, position);
                intent.putExtra(RCMConstants.EXTRA_MESSAGES_COUNT, parent.getCount());
                intent.putExtra(RCMConstants.EXTRA_MESSAGES_SORT_ORDER, mSortOrder);
                intent.putExtra(RCMConstants.EXTRA_MESSAGES_LIST_MODE, getCurrentMode());
                startActivity(intent);
            }
        });
        
        listView.setOnScrollListener(new OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED && getCurrentMode() == MODE_RECENT && totalItemCount > 0){
				    PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.MSG_SWITCH);
				}
			}
		});

        noMessagesIndication = findViewById(R.id.no_messages_indication);
        messagesLoadingIndication = findViewById(R.id.messages_loading_bar);
    }

    /**
     * Update list of messages via broadcast event.
     * Via Intent extra MESSAGE_LIST_UPDATE_BROADCAST_EXTRA we can determine who initiate broadcast.
     *
     * @param intent
     */
    synchronized void updateMessagesList(final Intent intent) {
        if (LogSettings.MARKET) {
            EngLog.d(TAG, "updateMessagesList(): Intent is " + ( intent != null ? "present" : "absent"));
        }

//        new Thread() {
//            @Override
//            public void run() {
                int senderType = INTENT_EXTRA_TYPES.NOT_SET;
                
                if (intent != null) {
                    senderType = intent.getExtras().getInt(MESSAGE_LIST_UPDATE_BROADCAST_EXTRA);
                    if (LogSettings.MARKET) {
                        MktLog.i(TAG, "updateMessagesList(): senderType = " + senderType);
                    }
                    if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED && senderType == Messages.INTENT_EXTRA_TYPES.ADD_MESSAGES){
                        PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.REFRESH_VOICEMAL);
                    }
                }

                int senderMode = getCurrentMode();
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "updateMessagesList(): senderMode = " + senderMode);
                }

                if (senderMode == MODE_DELETED && 
                		(senderType == INTENT_EXTRA_TYPES.SYNC_STATUS_CHANGED)) {
                	//state on sync status is not linked with delete items folder
                    if (LogSettings.MARKET) {
                        EngLog.w(TAG, "updateMessagesList Update SYNC status on Delete mode. Do nothing.");
                    }
                    return;
                }
                
                startQuery();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.layout.messages_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        boolean recentMode = btnRecent.isChecked();
        
        MenuItem clear_all = menu.findItem(R.id.msg_clear_all);
        if (clear_all != null) {
            clear_all.setVisible(!recentMode && hasDeleted());
        }
        
        MenuItem dnd = menu.findItem(R.id.msg_dnd);
        if (dnd != null) {
            dnd.setVisible(UserInfo.dndAllowed());
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.msg_sort_az:
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Menu: Sort");
                }
                showSort();
                return true;
            case R.id.msg_clear_all:
                RcAlertDialog.getBuilder(this).setTitle(R.string.app_name).setMessage(R.string.clear_all_confirm).setIcon(
                        R.drawable.symbol_exclamation).setPositiveButton(
                        R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                MessagesHandler.clearAllMessages(Messages.this);
                            }
                        }).setNegativeButton(R.string.dialog_btn_cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        }).show();
                return true;
            case R.id.msg_dnd:
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Menu: Set DND");
                }
                DNDialog.showDNDDialog(this);
                return true;
            case R.id.msg_refresh:
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Menu: Refresh");
                }
                if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
                    PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.REFRESH_VOICEMAL);
                }
                
                if (!NetworkUtils.isRCAvaliable(this)) {
                    if (LogSettings.MARKET) {
                        MktLog.i(TAG, "Refresh initiated by the user: Network is not available. Warning is shown to the user.");
                    }
                    try {
                        RcAlertDialog.getBuilder(this)
                        	.setTitle(R.string.error_code_alert_title)
                        	.setMessage(R.string.offline_mode_alerttext_msg_refresh)
                        	.setIcon(R.drawable.symbol_exclamation)
                        	.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
                        		public void onClick(DialogInterface dialog, int whichButton) {
                        		}
                        	})
                        	.create()
                        	.show();
                    } catch (java.lang.Throwable th) {
                    }
                } else {
	                // TODO : move messages pooling to NetworkManager
	                NetworkManager.getInstance().refreshMessages(this, true);
	                MessagesHandler.downloadAllMessages(true);
                }
                return true;
            case R.id.msg_settings:
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Menu: Settings");
                }
                SettingsLauncher.launch(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    
//    Handler handler = new Handler() {
//
//        @SuppressWarnings("unchecked")
//		@Override
//        public void handleMessage(Message msg) {
//        	
//            if (LogSettings.ENGINEERING) {
//                EngLog.d(TAG, "handleMessage(): mode = " + msg.arg1 + " (current mode recent? = " + btnRecent.isChecked() + "), what = " + msg.what + ", extra = " + msg.arg2
//                        + ", messages = " + (msg.obj == null ? "null" : ((List<MessagePOJO>) msg.obj).size()));
//            }
//            
//            if(isFinishing() || (listView == null)){
//            	return;
//            }
//
//            if (msg.arg1 != (btnRecent.isChecked() ? MessagesHandler.MESSAGE_TYPE_RECENT : MessagesHandler.MESSAGE_TYPE_DELETED)) {
//                return;
//            } 
//            
//            setViewState(sf_MESSAGES_LIST_VIEW_STATE_LOADING_MESSAGES);
//            
//            switch (msg.what) {
//                case MESSAGES_LIST_RECEIVED:
//                    msgInfoList = (List<MessagePOJO>) msg.obj;
//                    MessagePOJO items[] = msgInfoList.toArray(new MessagePOJO[0]);
//
//                    boolean sendBroadcastToDetailActivity = false;
//                    if ((msg.arg2 == INTENT_EXTRA_TYPES.SYNC_STATUS_CHANGED || msg.arg2 == INTENT_EXTRA_TYPES.STORAGE_STATE_CHANGED || msg.arg2 == INTENT_EXTRA_TYPES.BIND_STATE_CHANGED)
//                            && MessageDetailActivity.getCurrentMessageCounter() < listViewAdapter.getCount()) {
//                        MessagePOJO currentDetailViewMessage = (MessagePOJO) listViewAdapter.getItem(MessageDetailActivity.getCurrentMessageCounter());
//                        for (MessagePOJO message : items) {
//                            if (currentDetailViewMessage.getId() == message.getId() 
//                            		&& (currentDetailViewMessage.getSyncStatus() != message.getSyncStatus() 
//                            		|| currentDetailViewMessage.isBinded()!= message.isBinded()
//                            		|| currentDetailViewMessage.getBindDisplayName()!=message.getBindDisplayName())) {
//                                sendBroadcastToDetailActivity = true;
//                                break;
//                            }
//                        }
//                    }
//                    listViewAdapter.update(items);                    
//                    // send broadcast to MessageDetailActivity (only about sync and storage state) that affected only to the current viewed message (via extra)
//                    if (sendBroadcastToDetailActivity) {
//                        sendBroadcast(new Intent(RCMConstants.ACTION_UPDATE_MESSAGE_DETAIL_VIEW));
//                    }
//                    setViewState(sf_MESSAGES_LIST_VIEW_STATE_MESSAGES);
//                    
//                    break;
//                case MESSAGES_LIST_RECEIVED_EMPTY:
//                	setViewState(sf_MESSAGES_LIST_VIEW_STATE_NO_MESSAGES);
//                    break;
//            }
//        }
//    };

    
    private void setViewState( int state ){
    	
    	switch( state ){
    	case MESSAGES_LIST_VIEW_STATE_NO_MESSAGES:
            messagesLoadingIndication.setVisibility(View.GONE);
            listView.setVisibility(View.GONE);
            noMessagesIndication.setVisibility(View.VISIBLE);
    		break;
    	case MESSAGES_LIST_VIEW_STATE_LOADING_MESSAGES:
            messagesLoadingIndication.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            noMessagesIndication.setVisibility(View.GONE);
    		break;
    	case MESSAGES_LIST_VIEW_STATE_MESSAGES:
            messagesLoadingIndication.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            noMessagesIndication.setVisibility(View.GONE);
    		break;
    	}
    }
    
    // === Context MENU

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo menuInfo) {

        menu.setHeaderTitle(R.string.message_title);

        final AdapterView.AdapterContextMenuInfo info;
        try {
            info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        } catch (ClassCastException e) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "onCreateContextMenu() menuInfo", e);
            } else if (LogSettings.MARKET) {
                QaLog.e(TAG, "onCreateContextMenu() menuInfo: " + e.getMessage());
            }
            return;
        }
        
        try {
            final MessageListItemCache itemView = (MessageListItemCache) info.targetView.getTag();
            Cursor cursor = (Cursor) getListAdapter().getItem(info.position);
            if (cursor == null) {
                return;
            }

            long mailboxId = Long.parseLong(cursor.getString(MessagesQuery.MESSAGES_MAILBOX_ID_INDX));
            long recordId = cursor.getLong(MessagesQuery.MESSAGES_ID_INDX);
            ContactBinding bind = MessagesQuery.readSyncBind(this, cursor);
            BindSync bsync = Cont.acts.syncBind(this, mailboxId, bind);
            
            if (bsync.syncState == BindSync.State.INVALID_RECORD) {
                Cont.acts.bindSyncTrace(this, bsync, TAG, "onCreateContextMenu", recordId);
            } else if (bsync.syncState == BindSync.State.NOT_CHANGED) {
            } else {
                Cont.acts.bindSyncTrace(this, bsync, TAG, "onCreateContextMenu", recordId);
                MessagesSync.updateMessagesBind(this, mailboxId, recordId, bsync);
                if (syncService != null) {
                    try {
                        syncService.syncMessages(RCMProviderHelper.getCurrentMailboxId(this));
                    } catch (java.lang.Throwable error) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "onCreateContextMenu() call sync failed: ", error);
                        }
                    }
                }
            }
            
            MessagesQuery.MessageItem mi = MessagesQuery.readMessageItem(this, cursor);
            itemView.nameView.setText(mi.displayName);
            itemView.cpn = mi.cpn;

            if (itemView.cpn != null) {
                menu.add(0, MENU_ITEM_CALL, 0, R.string.call);
            }
            
            String markLabel;
            if (mi.readStatus == MessagePOJO.READ) {
                markLabel = getResources().getString(R.string.messageview_mark_unopened);
            } else {
                markLabel = getResources().getString(R.string.messageview_mark_opened);
            }
            menu.add(0, MENU_ITEM_MARK, 0, markLabel);
    
            if (mi.locallyDeleted == MessagePOJO.DELETED_LOCALLY_TRUE) {
                menu.add(0, MENU_ITEM_CLEAR_DELETED, 0, R.string.clear);
            } else {
                menu.add(0, MENU_ITEM_DELETE, 0, R.string.delete);
            }
    
            menu.add(0, MENU_ITEM_FORWARD, 0, R.string.messageview_menu_forward);
    
            if (bsync.bind.hasContact){
            	menu.add(0, MENU_ITEM_VIEW_CONTACT, 0, R.string.messageview_menu_view);
            	if (bsync.bind.isPhoneNumberFavorite) {
                    menu.add(0, MENU_ITEM_REMOVE_FROM_FAVORITES, 0, R.string.messageview_menu_remove_from_favorites);
                } else {
                    menu.add(0, MENU_ITEM_ADD_TO_FAVORITES, 0, R.string.messageview_menu_favorites);
                }
            }else{
            	if (bsync.bind.cpn !=null) {
    				menu.add(0, MENU_ITEM_CREATE_NEW_CONTACT, 0, R.string.messageview_menu_contact);
    				menu.add(0, MENU_ITEM_ADD_TO_EXISTING_CONTACT, 0, R.string.messageview_menu_add_to_existing);
    				menu.add(0, MENU_ITEM_ADD_TO_FAVORITES, 0, R.string.messageview_menu_favorites);				
    			}
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "onCreateContextMenu() error", error);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info;
        try {
            info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        } catch (ClassCastException e) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "onContextItemSelected() bad menuInfo", e);
            } else if (LogSettings.QA) {
                QaLog.e(TAG, "onContextItemSelected() bad menuInfo: " + e.getMessage());
            }
            return false;
        }

        try {
            Cursor cursor = (Cursor) getListAdapter().getItem(info.position);

            long mailboxId = Long.parseLong(cursor.getString(MessagesQuery.MESSAGES_MAILBOX_ID_INDX));
            long recordId = cursor.getLong(MessagesQuery.MESSAGES_ID_INDX);
            ContactBinding bind = MessagesQuery.readSyncBind(this, cursor);
            BindSync bsync = Cont.acts.syncBind(this, mailboxId, bind);
            if (bsync.syncState == BindSync.State.INVALID_RECORD) {
                Cont.acts.bindSyncTrace(this, bsync, TAG, "onContextItemSelected", recordId);
            } else if (bsync.syncState == BindSync.State.NOT_CHANGED) {
                
            } else {
                Cont.acts.bindSyncTrace(this, bsync, TAG, "onContextItemSelected", recordId);
                MessagesSync.updateMessagesBind(this, mailboxId, recordId, bsync);
                if (syncService != null) {
                    try {
                        syncService.syncMessages(mailboxId);
                    } catch (java.lang.Throwable error) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "onContextItemSelected() call sync failed: ", error);
                        }
                    }
                }
                return false;
            }


        MessagesQuery.MessageItem mi = MessagesQuery.readMessageItem(this, cursor);
        switch (item.getItemId()) {
            case MENU_ITEM_CALL: {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Menu: Call");
                }
                if (mi.isValidNumber) {
                    FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, btnRecent.isChecked()?
                            FlurryTypes.SCR_MESSAGES_RECENT_CONTEXT:
                            FlurryTypes.SCR_MESSAGES_DELETED_CONTEXT);
                    mRingOutAgent.call(mi.cpn.phoneNumber.numRaw, mi.displayName);
                }
                return true;
            }

            case MENU_ITEM_MARK: {
            	MessageUtils.userActionMarkMsg(this, mi.msgId);
                return true;
            }

            case MENU_ITEM_DELETE: {
            	MessageUtils.userActionDeleteMsg(this, mi.msgId, false);
                return true;
            }
            case MENU_ITEM_CLEAR_DELETED:
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Menu: Clear");
                }
                MessageUtils.userActionClearMsg(this, mi.msgId);
                return true;

            case MENU_ITEM_FORWARD:
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Menu: Forward");
                }
                MessageUtils.userActionForwardMSG(this, mi);
                return true;
                
            case MENU_ITEM_VIEW_CONTACT:
            	if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Menu: View item");
                }
            	MessageUtils.viewContact(this, bsync);
            	return true;            
            	
            case MENU_ITEM_CREATE_NEW_CONTACT:
            	if (LogSettings.MARKET) {
                    MktLog.d(TAG, "User: Menu: Create new Contact");
                }
           		FlurryTypes.onEventScreen(FlurryTypes.EVENT_CREATE_NEW_CONTACT, btnRecent.isChecked() ? 
           				FlurryTypes.SCR_MESSAGES_RECENT_CONTEXT : 
           				FlurryTypes.SCR_MESSAGES_DELETED_CONTEXT);
           		MessageUtils.createNewContact(this, mi, bsync);
            	return true;
            
            case MENU_ITEM_ADD_TO_EXISTING_CONTACT: 
                if (LogSettings.MARKET) {
                    QaLog.i(TAG, "User: Menu : Add to Contact");
                }
                if (bsync.bind.isValid) {
                	FlurryTypes.onEventScreen(FlurryTypes.EVENT_ADD_EXISTING_CONTACT, btnRecent.isChecked() ?
                			FlurryTypes.SCR_MESSAGES_RECENT_CONTEXT : 
                			FlurryTypes.SCR_MESSAGES_DELETED_CONTEXT);
                    MessageUtils.addToExistingContact(this, bsync);
                }
                return true;
	
			case MENU_ITEM_ADD_TO_FAVORITES:
				if (LogSettings.MARKET) {
					QaLog.i(TAG, "User: Add to Favorites");
				}
				if (bsync.bind.hasContact && !bsync.bind.isPhoneNumberFavorite) {					
					FlurryTypes.onEventScreen(FlurryTypes.EVENT_ADD_FAVORITES,
							btnRecent.isChecked() ? FlurryTypes.SCR_MESSAGES_RECENT_CONTEXT : FlurryTypes.SCR_MESSAGES_DELETED_CONTEXT);
				}
				MessageUtils.addToFavorites(this, mi, bsync);
				return true;
				
			case MENU_ITEM_REMOVE_FROM_FAVORITES:
				
				if (LogSettings.MARKET) {
                    QaLog.i(TAG, "User: Remove from Favorites");
                }
                if (bsync.bind.hasContact && bsync.bind.isPhoneNumberFavorite) {
                    FlurryTypes.onEventScreen(FlurryTypes.EVENT_REMOVE_FAVORITES, btnRecent.isChecked() ?
                    		FlurryTypes.SCR_MESSAGES_RECENT_CONTEXT : 
                    		FlurryTypes.SCR_MESSAGES_DELETED_CONTEXT);                        
                }
                MessageUtils.removeFromFavorites(this, bsync);
                return true;
			
            }
            return false;
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                QaLog.e(TAG, "onContextItemSelected() error: " + error.getMessage());
            }
            return false;
        }
   }

    private void showSort() {
        AlertDialog.Builder builder = RcAlertDialog.getBuilder(this);
        builder.setTitle(R.string.sort_title);
        builder.setSingleChoiceItems(R.array.messages_sort_dialog_items, mSortOrder,
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int item) {
                        mSortOrder = item;

                        if (item == MENU_SORT_ITEM_SENDER) {
                            FlurryTypes.onEvent(FlurryTypes.EVENT_SORT_BY, FlurryTypes.SORT_BY_SENDER);
                        } else if (item == MENU_SORT_ITEM_DATE){
                            FlurryTypes.onEvent(FlurryTypes.EVENT_SORT_BY, FlurryTypes.SORT_BY_DATE);
                        }

                        dialog.dismiss();
                        updateMessagesList(null);
                    }
                });
        builder.create().show();
    }

    private void syncMessages(Context context){
    	if (syncService != null) {
            try {
                syncService.syncMessages(RCMProviderHelper.getCurrentMailboxId(context));
            } catch (java.lang.Throwable error) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "syncMessages() call sync failed: ", error);
                }
            }
        }
    }

//    private static class NameComparator implements Comparator<MessagePOJO> {
//
//        public int compare(MessagePOJO obj1, MessagePOJO obj2) {
//            if ((obj1 == null || obj2 == null) || (obj1.getComputedLabel() == null || obj1.getComputedLabel() == null)) {
//                if (LogSettings.ENGINEERING) {
//                    EngLog.d(TAG, "null objects");
//                }
//                return 0;
//            } else {
//                return obj1.getComputedLabel().toLowerCase().compareTo(obj2.getComputedLabel().toLowerCase());
//            }
//        }
//    }
//
//    // TODO update DATE comparator
//    private class DateComparator implements Comparator<MessagePOJO> {
//
//        public int compare(MessagePOJO obj1, MessagePOJO obj2) {
//            return obj2.getDateTime().compareTo(obj1.getDateTime());
//        }
//    }
    

    /**
     * Initialize sub tabs
     */
    private void initSubTabs() {
        mSubTabs = (LinearLayout) findViewById(R.id.subtabs);
        mSubTabs.setVisibility(View.VISIBLE);

        btnRecent = (ToggleButton) findViewById(R.id.btnLeft);
        btnRecent.setTextOn(getString(R.string.messages_recent_title));
        btnRecent.setTextOff(getString(R.string.messages_recent_title));
        btnRecent.setText(getString(R.string.messages_recent_title));
        btnRecent.setChecked(true);
        btnRecent.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                if (btnDeleted.isChecked()) {
                    btnDeleted.setChecked(false);
                    btnDeleted.setSelected(false);
                    btnRecent.setChecked(true);
                    btnRecent.setSelected(true);
                    if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
                        PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.MSG_SWITCH);
                    }
                    setViewState(MESSAGES_LIST_VIEW_STATE_LOADING_MESSAGES);                    

                    updateMessagesList(null);
                    //startQuery();
                    FlurryTypes.onEventScreen(FlurryTypes.EVENT_SCREEN_OPEN, FlurryTypes.SCR_MESSAGES_RECENT);
                    
                } else {
                    btnRecent.setChecked(!btnRecent.isChecked());
                }
            }
        });
        btnRecent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {				
				MessagesNotification.visitRecent(Messages.this, isChecked);
			}
		});        

        btnDeleted = (ToggleButton) findViewById(R.id.btnRight);
        btnDeleted.setTextOn(getString(R.string.messages_deleted_title));
        btnDeleted.setTextOff(getString(R.string.messages_deleted_title));
        btnDeleted.setText(getString(R.string.messages_deleted_title));
        btnDeleted.setChecked(false);
        btnDeleted.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                if (btnRecent.isChecked()) {
                    btnRecent.setChecked(false);
                    btnRecent.setSelected(false);
                    btnDeleted.setChecked(true);
                    btnDeleted.setSelected(true);
                    
                    setViewState(MESSAGES_LIST_VIEW_STATE_LOADING_MESSAGES);
                    updateMessagesList(null);
                    //startQuery();
                    FlurryTypes.onEventScreen(FlurryTypes.EVENT_SCREEN_OPEN, FlurryTypes.SCR_MESSAGES_DELETED);
                    
                } else {
                    btnDeleted.setChecked(!btnDeleted.isChecked());
                }
            }
        });
    }

    private int getCurrentMode() {
		int senderMode = btnRecent.isChecked() ? MODE_RECENT : MODE_DELETED;
		return senderMode;
	}

    
    private void startQuery() {
        MessagesQuery.startAsyncMsgListQuery(mQueryHandler, QUERY_TOKEN, getCurrentMode(), mSortOrder);
    }
    
    
	private class MessageListChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LogSettings.ENGINEERING) {
                EngLog.d(TAG, "MessageListChangeReceiver from " + context.getClass().getCanonicalName() + ", intent: " + intent.getAction()
                        + ", extra caller type: " + (intent.getExtras().getInt(MESSAGE_LIST_UPDATE_BROADCAST_EXTRA)));
            }
            updateMessagesList(intent);
            //startQuery();
        }
    }
    
    public static void fireSwitchMessageTabEvent(Context context, int newTab){
    	if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "fireSwitchMessageTabEvent : " + newTab);
		}
    	context.sendBroadcast(new Intent(Messages.ACTION_SWITCH_MESSAGES_TAB).putExtra(Messages.MESSAGE_TAB, newTab));
    }
    
    private class MessageTabChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {         
            if(intent.hasExtra(MESSAGE_TAB)){
                final int targetTab = intent.getIntExtra(MESSAGE_TAB, MODE_RECENT);
                if(LogSettings.ENGINEERING){
                    EngLog.d(TAG, "Tab is about to switch : " + targetTab);
                }
                if ((targetTab == MODE_RECENT) && (btnRecent != null) && !btnRecent.isChecked()) {
                    btnRecent.performClick();
                } else if ((targetTab == MODE_DELETED) && (btnDeleted != null) && !btnDeleted.isChecked()) {
                    btnDeleted.performClick();
                } // else ignored
            }                       
        }
    }

    private final class QueryHandler extends AsyncQueryHandler {

        public QueryHandler(Context context) {
            super(context.getContentResolver());
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "onQueryComplete(token = " + token + ")");
            }
            
            if (cursor == null) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "onQueryComplete(): cursor==null; return");
                }
                return;
            }

            if (!isFinishing()) {
                syncMessages(Messages.this);

                mAdapter.changeCursor(cursor);
                try {
                    if (cursor.getCount() > 0) {
                        setViewState(MESSAGES_LIST_VIEW_STATE_MESSAGES);
                    } else {
                        setViewState(MESSAGES_LIST_VIEW_STATE_NO_MESSAGES);
                    }
                } catch (java.lang.Throwable error) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "onQueryComplete process error: " + error.getMessage());
                    }
                }
            } else {
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }
        }
    }
    
    /**
     * Item view holder.
     */
    private static final class MessageListItemCache {
        TextView nameView;
        TextView dateView;      
        TextView timeView;
        ImageView presenceView;
        ProgressBar progressView;
        ContactPhoneNumber cpn;
    }

    private class MessagesListAdapter extends ResourceCursorAdapter {

        public MessagesListAdapter() {
            super(Messages.this, R.layout.message_list_item_main, null);
        }

        @Override
        protected void onContentChanged() {
            startQuery();
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            final View view = super.newView(context, cursor, parent);
            
            final MessageListItemCache cache = new MessageListItemCache();
            
            cache.nameView = (TextView) view.findViewById(R.id.name);
            cache.timeView = (TextView) view.findViewById(R.id.label);
            cache.dateView = (TextView) view.findViewById(R.id.date);
            cache.presenceView = (ImageView) view.findViewById(R.id.presence);
            cache.progressView = (ProgressBar) view.findViewById(R.id.progress);
            
            view.setTag(cache);
            return view;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            try {
                final MessageListItemCache itemView = (MessageListItemCache) view.getTag();
                MessagesQuery.MessageItem mi = MessagesQuery.readMessageItem(context, cursor);
                
//                String name = item.getComputedLabel();
//                String duration = "";
//                // What is this ?  
//                if (name != null && (name.equals("MP3") || name.equals("WAV"))) {
//                    name = "Voice messsage";
//                }
                itemView.nameView.setText(mi.displayName);

                String duration;
                if(mi.messageType.equals(MessageType.VOICE)){
                    duration = MessageUtils.getLengthLabel(mi.duration);
                } else {
                    duration = String.valueOf(mi.duration) + " pgs";
                }
                
                itemView.timeView.setText(LabelsUtils.getTimeLabel(mi.dateTime.getTime()) + " ("+ duration + ")");
                itemView.dateView.setText(MessageUtils.getTextDate(mi.dateTime));
                
                setMessageIcon(mi, itemView);
            } catch (java.lang.Throwable error) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "bindView() error", error);
                }
            }
        }
        

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return super.getView(position, convertView, parent);
        }
            
        private void setMessageIcon(MessageItem mi, MessageListItemCache cache) {

            //progress
            if (mi.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING) {
                cache.progressView.setVisibility(View.VISIBLE);
                cache.progressView.bringToFront();
            } else {
                cache.progressView.setVisibility(View.GONE);
            }
            if (mi.messageType.equals(MessageType.VOICE)) {
                if (MessagePOJO.UNREAD == mi.readStatus) {
                    if (mi.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED
                            || mi.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING) {
                        cache.presenceView.setImageResource(R.drawable.ico_vm_new);
                    } else {
                        cache.presenceView.setImageResource(R.drawable.ico_vm_new_not_downloaded);
                    }
                } else {
                    if (mi.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED
                            || mi.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING) {
                        cache.presenceView.setImageResource(R.drawable.ico_vm_read);
                    } else {
                        cache.presenceView.setImageResource(R.drawable.ico_vm_read_not_downloaded);
                    }
                }
            } else if (mi.messageType.equals(MessageType.FAX)) {
                if (MessagePOJO.UNREAD == mi.readStatus) {
                    if (mi.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED
                            || mi.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING) {
                        cache.presenceView.setImageResource(R.drawable.ico_fax_new);
                    } else {
                        cache.presenceView.setImageResource(R.drawable.ico_fax_new_not_downloaded);
                    }
                } else {
                    if (mi.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED
                            || mi.syncStatus == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING) {
                        cache.presenceView.setImageResource(R.drawable.ico_fax_read);
                    } else {
                        cache.presenceView.setImageResource(R.drawable.ico_fax_read_not_downloaded);
                    }
                }
            }
        }
    }    
    
}
