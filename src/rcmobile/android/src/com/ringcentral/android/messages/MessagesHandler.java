/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.messages;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Process;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConfig;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.api.Base64Stream;
import com.ringcentral.android.api.Utils;
import com.ringcentral.android.api.XMLHelper;
import com.ringcentral.android.api.network.ServerConnector;
import com.ringcentral.android.api.parsers.MessageResponseHandler;
import com.ringcentral.android.api.pojo.MessagePOJO;
import com.ringcentral.android.api.pojo.MessagePOJO.CompType;
import com.ringcentral.android.contacts.ContactPhoneNumber;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.provider.RCMDataStore.MessagesTable;
import com.ringcentral.android.utils.FileUtils;
import com.ringcentral.android.utils.PhoneUtils;
import com.ringcentral.android.utils.WaveCodecDetector;


public class MessagesHandler {
	
	private static final String TAG = "[RC]MessagesHandler";
    private static final boolean DEBUG = true;

    private static final String sf_SYNC_protocol_version = "1.4";
    private static final String sf_SYNC_agent_version = "4.60.111.11";

    private enum MessagesAction {
        DELETE, SESSION_ID, LIST, MARK_READ, MARK_UNREAD, MSG_BODY
    }
    
    private static MessageBodyLoader m_msgLoader = new MessageBodyLoader();
    private static volatile boolean mPoolingTask = false;

    
    private MessagesHandler() {
    } 
    

    static void put(Context context, long msg_id, int priority){
        m_msgLoader.setContext(context);
        m_msgLoader.put(context, msg_id, priority);
    }

    
    public static boolean isOnList(Context context, long msgId, int priority){
    	return m_msgLoader.isOnList(context, msgId, priority);
    }       
    
    public static void clearBodyLoaderTask(){
    	m_msgLoader.clearAll();
    }

    
    public static void updateList(final boolean backgroundTask) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "updateList(backgroundTask = " + backgroundTask);
        }
        
        updateList(RingCentralApp.getContextRC(), backgroundTask);
    }
    
    private static void updateList(final Context context, final boolean backgroundTask) {
        if (DEBUG && LogSettings.ENGINEERING) {
            EngLog.d(TAG, "updating list...");
        }

        if(backgroundTask){        	
        	new Thread(new Runnable() {
        		public void run() {
        			Map<Long, MessagePOJO> messagesList = loadMessagesListFromServer(context);
        			if (messagesList != null) {
        				storeMessages(context, messagesList);
        				downloadAllMessages(context, backgroundTask);                    	                    
        			}
        		}
        	}, "MSG:updateList").start();
        }else{
        	Map<Long, MessagePOJO> messagesList = loadMessagesListFromServer(context);
			if (messagesList != null) {
				storeMessages(context, messagesList);
				downloadAllMessages(context, backgroundTask);                    	                    
			}
        }

    }


    private static String buildRequestBody(Context context, MessagesAction action, long msgId, String sessionID) {
        String canonicalPhoneNumber = "0";

        if (RCMProviderHelper.getLoginNumber(context) != null || !RCMProviderHelper.getLoginNumber(context).equals("")) {
            canonicalPhoneNumber = PhoneUtils.getNumRaw(RCMProviderHelper.getLoginNumber(context));
        }

        String zero = "0";
        String blank = "";

        // header defaults for first pass
        String headerUserId = blank;
        String headerMailbox = blank;
        long headerPassword = 0;
        String headerDgSignature = blank;

        String startMessageID = "0";


        if (sessionID == null) {
            sessionID = zero;
        }

        String command = "";

        headerUserId = canonicalPhoneNumber;
        headerMailbox = RCMProviderHelper.getLoginExt(context);
        headerPassword = Utils.getPasswordHash(RCMProviderHelper.getLoginPassword(context));
        headerDgSignature = Utils.computeDigitalSignature(RCMProviderHelper.getLoginNumber(context), sessionID, RCMProviderHelper.getLoginPassword(context));

        switch (action) {
            case LIST:
                command = "GetMessagesList";
                break;
            case DELETE:
                command = "DeleteMessage";
                // load up the real data for the second (and subsequent) pass(es)
                break;
            case SESSION_ID:
                command = "GetSessionID";
                break;
            case MSG_BODY:
                command = "GetMessage";
                break;
            case MARK_READ:
            case MARK_UNREAD:
                command = "ChangeMessageReadStatus";
                break;
        }

        String folder = zero;

        String pNumListModificationCounter = "1";

        String body = String.format(MsgSyncRequestTemplate.MAIN, command, canonicalPhoneNumber, RCMProviderHelper.getLoginExt(context), ((13) % 10000), 0, sf_SYNC_protocol_version, sf_SYNC_agent_version,
                headerUserId, headerMailbox, headerDgSignature, headerPassword, startMessageID, 0, sessionID, folder,
                pNumListModificationCounter);

        switch (action) {
            case SESSION_ID:
                body += String.format(MsgSyncRequestTemplate.SESSION_ID, RCMProviderHelper.getLoginNumber(context), RCMProviderHelper.getLoginExt(context));
                break;

            case LIST:
                break;

            case MSG_BODY:
                String[] msg_attrs = MessageUtils.readMessageFieldsFromDb(context, msgId,
                        new String[] {RCMDataStore.MessagesTable.JEDI_COMP_TYPE, RCMDataStore.MessagesTable.JEDI_MSG_TYPE});
                
                int comp_type = Integer.parseInt(msg_attrs[0]);
                int msg_type = Integer.parseInt(msg_attrs[1]);
                
                body += String.format(MsgSyncRequestTemplate.MSG_BODY, comp_type, msg_type, msgId);
                break;

            case DELETE:
                body += String.format(MsgSyncRequestTemplate.DELETE_MESSAGE, msgId, msgId);
                break;

            case MARK_READ:
                body += String.format(MsgSyncRequestTemplate.MESSAGE_STATUS, MessagePOJO.READ, msgId);
                break;

            case MARK_UNREAD:
                body += String.format(MsgSyncRequestTemplate.MESSAGE_STATUS, MessagePOJO.UNREAD, msgId);
                break;
        }

        body += "</REQUEST>";

        return body;

    }

    private static Map<Long, MessagePOJO> loadMessagesListFromServer(Context context) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "Loading Messages List mailBoxId:" + RCMProviderHelper.getCurrentMailboxId(context));    
        }
        HttpResponse http_response = null;
        InputStream is = null;
        int logPointForError = 0;
        try {
            String sessionID = getSessionId(context);
            if (LogSettings.MARKET) {
                if (sessionID == null) {
                    MktLog.w(TAG, "loadMessagesList():sessionID=null");    
                }
            }
            logPointForError++;
            String listRequestBody = buildRequestBody(context, MessagesAction.LIST, 0, sessionID);
            if (LogSettings.MARKET) {
                if (listRequestBody == null) {
                    MktLog.w(TAG, "loadMessagesList():listRequestBody=null");    
                }
            }
            logPointForError++;
            http_response = executeHttpRequest(listRequestBody);
            if (http_response == null) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "loadMessagesList():http_response=null");    
                }
                return null;
            }
            logPointForError++;
            is = http_response.getEntity().getContent();
            if (is == null) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "loadMessagesList():istream=null");    
                }
                return null;
            }

            logPointForError++;
            MessageResponseHandler response = new MessageResponseHandler(null);
            logPointForError++;
            MessagesListHandler msgList = new MessagesListHandler(response);
            logPointForError++;
            response.addChild(msgList.getTagName(), msgList);
            logPointForError++;

            try {
                response.parseResponse(is);
                logPointForError++;
            } catch (SAXException e_sax) {
                if (LogSettings.ENGINEERING) {
                    EngLog.e(TAG, "loadMessagesList():s:" + e_sax.getMessage());
                } else if (LogSettings.MARKET) {
                    MktLog.e(TAG, "loadMessagesList():s: Could not parse server response");
                }
            } catch (NullPointerException e) {
                if (LogSettings.ENGINEERING) {
                    EngLog.e(TAG, "loadMessagesList():in:" + e.getMessage());
                } else if (LogSettings.MARKET) {
                    MktLog.e(TAG, "loadMessagesList():in: Could not parse server response");
                }
            }

            if (!response.isFailed()) {
                logPointForError++;
                int ret_code = response.getResult().getCode();
                logPointForError++;
                if (LogSettings.MARKET) {
                    try {
                        MktLog.i(TAG, "loadMessagesList result: " + ret_code + " number messages on server list: " + msgList.getHeader().getMsg_number());
                    } catch (java.lang.Throwable th) {
                    }
                }

                if (ret_code == 0) {
                    /*
                          * DK: Need to check if server modification ID is equal local modification ID
                          * If there is TRUE server will return EMPTY list. It means No changes detected!
                          */
                    long current_mod_counter = RCMProviderHelper.getMsgModCounter(context);
                    long mod_counter = response.getModificationCounterMsg();

                    if (LogSettings.MARKET) {
                        MktLog.i(TAG, "loadMessagesList Modification counter: current: " + current_mod_counter + " received: " + mod_counter);
                    }

                    if (current_mod_counter <= 0 || current_mod_counter != mod_counter) {

                        RCMProviderHelper.saveMsgModCounter(context, mod_counter);
                        return msgList.getMsgList();
                    }
                }
            }

        }
        catch (Exception e) {

            if (LogSettings.MARKET) {
                QaLog.e(TAG, "loadMessagesList():cnt=" + logPointForError + ":" , e);
            }
        }  finally {
            if (is != null) {
                try {
                    is.close();
                    is = null;
                } catch (java.lang.Throwable tr1) {
                }
            }
            
            if (http_response != null) {
                try {
                    http_response.getEntity().consumeContent();
                    http_response = null;
                } catch (java.lang.Throwable tr1) {
                }
            }
        }

        return null;
    }

    private static String streamToString(InputStream in) {
        if (in == null) {
            return null;
        }

        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(in), 8192);

            StringBuffer res = new StringBuffer();
            try {
                String line;
                while ((line = rd.readLine()) != null) {
                    res.append(line);
                }
            } catch (IOException e) {
                if (LogSettings.MARKET) {
                    QaLog.e(TAG, "streamToString()", e);
                }
            }
            return res.toString();
        } catch (NullPointerException e) {
            if (LogSettings.ENGINEERING && DEBUG) {
                EngLog.e(TAG, e.getLocalizedMessage(), e);
            }
            return null;
        }
    }

    private static HttpResponse executeHttpRequest(String body) {
        return ServerConnector.makeRequest(RCMConfig.get_MSGSYNC_URL(RingCentralApp.getContextRC()), body);
    }

    private static String getSessionId(Context context) {
        String sessionId = null;
        HttpResponse http_response = null;
        InputStream is = null;
        int logPointForError = 0;
        try {
            String sessionIdRequest = buildRequestBody(context, MessagesAction.SESSION_ID, 0, null);
            if (LogSettings.MARKET) {
                if (sessionIdRequest == null) {
                    MktLog.w(TAG, "getSessionId():sessionIdRequest=null");    
                }
            }
            logPointForError++;
            http_response = executeHttpRequest(sessionIdRequest);
            if (LogSettings.MARKET) {
                if (http_response == null) {
                    MktLog.w(TAG, "getSessionId():http_response=null");    
                }
            }
            logPointForError++;
            is = http_response.getEntity().getContent();
            if (LogSettings.MARKET) {
                if (is == null) {
                    MktLog.w(TAG, "getSessionId():is=null");    
                }
            }
            logPointForError++;
            String response = streamToString(is);
            logPointForError++;
            if (response != null) {
                logPointForError++;
                Document document = XMLHelper.getDocumentBuilder().parse(new ByteArrayInputStream(response.getBytes()));
                logPointForError++;
                sessionId = getNodeValue(document, "sessionID");
                logPointForError++;
            } else {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "getSessionId():response is null ");
                }
            }
        } catch (Exception e) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "getSessionId():cnt=" + logPointForError + ":", e);
            } else if (LogSettings.MARKET) {
                MktLog.e(TAG, "getSessionId():cnt=" + logPointForError + ":" + e.getMessage());
            }
        }  finally {
            if (is != null) {
                try {
                    is.close();
                    is = null;
                } catch (java.lang.Throwable tr1) {
                }
            }
            
            if (http_response != null) {
                try {
                    http_response.getEntity().consumeContent();
                    http_response = null;
                } catch (java.lang.Throwable tr1) {
                }
            }
        }
        return sessionId;
    }

    private static String getNodeValue(Document document, String nodeName) {
        String value = null;
        Node response = XMLHelper.findchildByElement(document, "RESPONSE");
        if (response != null) {
            Node node = XMLHelper.findchildByElement(response, nodeName);
            if (node != null) {
                value = XMLHelper.getNodeValue(node);
            }
        }
        return value;
    }

    public static void deleteMessage(final Context context, final long msgId) {

        new Thread(new Runnable() {
            public void run() {
                //move to deleted
                MessageUtils.moveMessageToDeleted(context, msgId);

                HttpResponse http_response = null;
                InputStream is = null;
                
                //delete from server
                try {
                    String sessionID = getSessionId(context);
                    String body = buildRequestBody(context, MessagesAction.DELETE, msgId, sessionID);

                    MessageResponseHandler response = new MessageResponseHandler(null);
                    http_response = executeHttpRequest(body); 
                    is = http_response.getEntity().getContent();
                    if (is != null) {
                        response.parseResponse(is);
                        if (!response.isFailed()) {
                            int ret_code = response.getResult().getCode();
                            if (LogSettings.MARKET) {
                                try {
                                    MktLog.i(TAG, "deleteMessage() message ID: " + msgId + " result: " + ret_code);
                                } catch (java.lang.Throwable th) {
                                }
                            }

                            /*
                            * Modification counter should be checked if no error detected or
                            * "Wrong message ID" or "Server is busy" errors were received
                            */
                            if (ret_code == 0 || ret_code == 4 || ret_code == 9) {
                                long mod_counter = response.getModificationCounterMsg();
                                checkMessageModificationCounter(context, mod_counter);
                            }
                        }
                    }
                } catch (Exception e) {
                    if (LogSettings.MARKET) {
                        QaLog.e(TAG, "deleteMessage(): " + e.getMessage());
                    }
                }finally {
                    if (is != null) {
                        try {
                            is.close();
                        } catch (Throwable tr1) {
                        }
                        is = null;
                    }
                    
                    if (http_response != null) {
                        try {
                            http_response.getEntity().consumeContent();
                        } catch (Throwable tr1) {
                        }
                        http_response = null;
                    }
                }
            }
        }, "MSG:deleteMessage").start();

    }

    
    private static final String[] CLEAR_MESSAGE_QUERY_PROJECTION =
        new String[] {RCMDataStore.MessagesTable.JEDI_FILE_EXT, RCMDataStore.MessagesTable.RCM_SYNC_STATUS}; 
    private static final int CLEAR_MESSAGE_QUERY_FILE_EXT_INDEX = 0;
    private static final int CLEAR_MESSAGE_QUERY_SYNC_STATUS_INDEX = 1;
    
    public static void clearMessage(final Context context, long msgId ){
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "clearMessage(msgId = " + msgId + ")...");
        }
        
        String[] msg_attrs = MessageUtils.readMessageFieldsFromDb(context, msgId, CLEAR_MESSAGE_QUERY_PROJECTION); 
        
        if (msg_attrs == null) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "clearMessage(msgId = " + msgId + "): message not found; return");
            }
            return;
        }
        
        int sync_status = Integer.parseInt(msg_attrs[CLEAR_MESSAGE_QUERY_SYNC_STATUS_INDEX]);
        String file_ext = msg_attrs[CLEAR_MESSAGE_QUERY_FILE_EXT_INDEX];
        
        if (sync_status == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED) {
            clearMessageBody(context, msgId, file_ext);
        }
    
        MessageUtils.removeDeletedMessage(context, msgId);       
    }
    
	
    private static final String[] CLEAR_ALL_MESSAGES_QUERY_PROJECTION = new String[] {
        RCMDataStore.MessagesTable.JEDI_MSG_ID,
        RCMDataStore.MessagesTable.JEDI_FILE_EXT,
        RCMDataStore.MessagesTable.RCM_SYNC_STATUS }; 
    private static final int CLEAR_ALL_MESSAGES_QUERY_MSG_ID_INDEX = 0;
    private static final int CLEAR_ALL_MESSAGES_QUERY_FILE_EXT_INDEX = 1;
    private static final int CLEAR_ALL_MESSAGES_QUERY_SYNC_STATUS_INDEX = 2;
    
    public static void clearAllMessages(final Context context) {

        new Thread(new Runnable() {
            public void run() {
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "clearAllMessages()...");
                }
                
                long mailbox_id = RCMProviderHelper.getCurrentMailboxId(context);
                Cursor cursor = context.getContentResolver().query(
                        UriHelper.getUri(RCMProvider.MESSAGES, mailbox_id),
                        CLEAR_ALL_MESSAGES_QUERY_PROJECTION,
                        RCMDataStore.MessagesTable.RCM_LOCALLY_DELETED + '=' + MessagePOJO.DELETED_LOCALLY_TRUE,
                        null, null);
                
                if (cursor == null || cursor.getCount() <= 0) {
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "clearAllMessages(): no deleted messages; return");
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return; 
                }
                
                cursor.moveToFirst();
                do {
                    long msg_id = cursor.getLong(CLEAR_ALL_MESSAGES_QUERY_MSG_ID_INDEX);
                    String file_ext = cursor.getString(CLEAR_ALL_MESSAGES_QUERY_FILE_EXT_INDEX);
                    int sync_status = cursor.getInt(CLEAR_ALL_MESSAGES_QUERY_SYNC_STATUS_INDEX);

                    if (sync_status == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED) {
                        clearMessageBody(context, msg_id, file_ext);
                    }
                } while (cursor.moveToNext());
                
                //remove message from data base
                MessageUtils.removeAllDeletedMessages(context);
            }
        }, "MSG:ClearAllMsg").start();

    }

	private static void clearMessageBody(Context context, long msgId, String fileExt) {
	    String filePath = FileUtils.getFilePathForMsg(FileUtils.getSDPath(context), String.valueOf(msgId), fileExt);
        File file = new File(filePath);
        if (file.exists())
            file.delete();
	}
	
	
    public static void changeReadStatus(final Context context, final long msgId) {
        final int current_status = MessageUtils.readIntMessageFieldFromDb(context, msgId, RCMDataStore.MessagesTable.JEDI_READ_STATUS);

        if (LogSettings.MARKET) {
            MktLog.d(TAG, "changeReadStatus(): msgId = " + msgId + "; current_status = " + current_status);
        }
        
        //delete from server
        new Thread(new Runnable() {
            public void run() {
                HttpResponse http_response = null;
                InputStream is = null;
                try {
                    //set read in db
                    MessageUtils.toggleMessageReadStatusInDb(context, msgId, current_status);

                    String sessionID = getSessionId(context);
                    
                    MessagesAction action =
                        (current_status == MessagePOJO.READ ? MessagesAction.MARK_UNREAD : MessagesAction.MARK_READ);
                    String body = buildRequestBody(context, action, msgId, sessionID);

                    MessageResponseHandler response = new MessageResponseHandler(null);

                    http_response = executeHttpRequest(body); 
                    is = http_response.getEntity().getContent();
                    if (is != null) {
                        response.parseResponse(is);
                        if (!response.isFailed()) {
                            int ret_code = response.getResult().getCode();
                            if (LogSettings.MARKET) {
                                try {
                                    QaLog.i(TAG, "changeReadStatus() message ID: " + msgId + " result: " + ret_code);
                                } catch (java.lang.Throwable th) {
                                }
                            }

                            /*
                            * Modification counter should be checked if no error detected or 
                            * "Wrong message ID" or "Server is busy" errors were received  
                            */
                            if (ret_code == 0 || ret_code == 4 || ret_code == 9) {
                                long mod_counter = response.getModificationCounterMsg();
                                checkMessageModificationCounter(context, mod_counter);
                            }

                        }
                    }
                } catch (Exception e) {
                    if (LogSettings.MARKET) {
                        QaLog.w(TAG, "changeReadStatus(): " + e.getMessage());
                    }
                } finally {
                    if (is != null) {
                        try {
                            is.close();
                            is = null;
                        } catch (Throwable tr1) {
                        }
                    }
                    
                    if (http_response != null) {
                        try {
                            http_response.getEntity().consumeContent();
                            http_response = null;
                        } catch (Throwable tr1) {
                        }
                    }
                }
            }
        }, "MSG:ChangeReadStatus").start();

    }


    private static String getMessageBody(Context context, long msgId) throws IOException, ParserConfigurationException, SAXException {
        if (DEBUG && LogSettings.QA) {
            try {
                QaLog.d(TAG, "LOADING MESSAGE BODY loading msg: " + msgId);
            } catch (java.lang.Throwable th) {
            }
        }
        HttpResponse http_response = null;
        InputStream is = null;
        String tmpFilePath = FileUtils.getTmpBase64File(FileUtils.getSDPath(context), msgId);

        String sessionID = getSessionId(context);
        String msgBodyRequestBody = buildRequestBody(context, MessagesAction.MSG_BODY, msgId, sessionID);
        http_response = executeHttpRequest(msgBodyRequestBody); 
        is = http_response.getEntity().getContent();
        if (is != null) {
            try {
                Writer wr = FileUtils.getStreamForBase64(tmpFilePath);

                MessageResponseHandler response = new MessageResponseHandler(null);
                MessageDataHandler msgBodyLoader = new MessageDataHandler(response, wr);
                response.addChild(msgBodyLoader.getTagName(), msgBodyLoader);
                response.parseResponse(is);
                wr.close();
                try {
                    // try to close InputStream, ignore exception
                    is.close();
                } catch (IOException e) {}

                int errorCode = response.getResult().getCode();

                if (errorCode == 4 || errorCode == 7 || errorCode == 9) {
                    if (LogSettings.MARKET) {
                        try {
                            MktLog.w(TAG, "getMessageBody Error: " + response.getResult().getCode() + ", msg.getId() =  " + msgId);
                        } catch (java.lang.Throwable th) {
                        }
                    }

                    return null;
                }

                if (DEBUG && LogSettings.MARKET) {
                    try {
                        QaLog.d(TAG, "LOADING MESSAGE BODY COMPLETE received message id: " + msgBodyLoader.getMessageID() + " encoded file path: " + tmpFilePath);
                    } catch (java.lang.Throwable th) {
                    }
                }
                return tmpFilePath;
            } catch (NullPointerException e) {
                if (LogSettings.QA && DEBUG) {
                    EngLog.e(TAG, e.getLocalizedMessage(), e);
                }
                return null;
            } finally {
                if (is != null) {
                    try {
                        is.close();
                        is = null;
                    } catch (java.lang.Throwable tr1) {
                    }
                }
                
                if (http_response != null) {
                    try {
                        http_response.getEntity().consumeContent();
                        http_response = null;
                    } catch (java.lang.Throwable tr1) {
                    }
                }
            }
        }

        return null;
    }

    /*
     * 
     */
    
    private final static int BASE64_DECODE_BUFFER	= 1024 * 500;
    private static void decodeFile(final Context context, long msgId, String base64FilePath) throws IOException {
        String[] attrs = MessageUtils.readMessageFieldsFromDb(context, msgId,
                new String[] {RCMDataStore.MessagesTable.JEDI_FILE_EXT, RCMDataStore.MessagesTable.JEDI_COMP_TYPE});
        
        String msg_file_ext = attrs[0];
        int msg_comp_type = Integer.parseInt(attrs[1]);
        
        String realFilePath = FileUtils.getFilePathForMsg(FileUtils.getSDPath(context), String.valueOf(msgId), msg_file_ext);

        File file_out = new File(realFilePath);
        File file_in = new File(base64FilePath);
        int decoded_size = 0;
        
        try{
            BufferedInputStream in_buf_stream = new BufferedInputStream( new FileInputStream(file_in ), BASE64_DECODE_BUFFER );
            BufferedOutputStream out_buf_stream = new BufferedOutputStream( new FileOutputStream(file_out), BASE64_DECODE_BUFFER );
            
            decoded_size = Base64Stream.decodeBase64(in_buf_stream, out_buf_stream);

            in_buf_stream.close();
            out_buf_stream.close();
        }
        catch( IOException e_io ){
        	
        }
        
        if( !file_in.delete()){
        	
            if (LogSettings.MARKET) {
                try {
                    MktLog.e(TAG, "decodeFile Could not delete Base64 file: " + file_in.getCanonicalPath());
                } catch (java.lang.Throwable th) {
                }
            }
        }

        try {
            if (decoded_size < 0 || file_out.length() == 0 ) {
                if (LogSettings.MARKET) {
                    try {
                        MktLog.e(TAG, "decodeFile Could not detect message body: " + Long.toString(msgId));
                    } catch (java.lang.Throwable th) {
                    }
                }

                file_out.delete();
            } else {
                if (msg_comp_type == CompType.WAVE.getValue() && WaveCodecDetector.isGSM(file_out)) {
                    MessageUtils.updateMessageField(
                            context, msgId, RCMDataStore.MessagesTable.JEDI_COMP_TYPE, CompType.GSM.getValue());
                }
            }
        } catch(SecurityException e_se) {
            if (LogSettings.MARKET) {
                try {
                    QaLog.e(TAG, "Security exception is occurred during work with : " + realFilePath + " error:" + e_se.getMessage());
                } catch (java.lang.Throwable th) {
                }
            }
        }
    }

    
    private static final String DB_QUERY_SELECTION_MESSAGES_DOWNLOAD =
        RCMDataStore.MessagesTable.RCM_SYNC_STATUS + "!=" + RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED + " AND " +
        RCMDataStore.MessagesTable.RCM_SYNC_STATUS + "!=" + RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING + " AND " +
        RCMDataStore.MessagesTable.JEDI_MSG_TYPE + "=" + MessagePOJO.MessageType.VOICE.getValue() + " AND " +
        RCMDataStore.MessagesTable.RCM_LOCALLY_DELETED + "= 0";
   
    private static final String[] DB_QUERY_PROJECTION_MESSAGES_DOWNLOAD = new String[] {RCMDataStore.MessagesTable.JEDI_MSG_ID};
    private static final int COLUMN_INDEX_MSG_ID = 0; 
    
    
    public static void downloadAllMessages(boolean backgroundTask) {
        downloadAllMessages(RingCentralApp.getContextRC(), backgroundTask);
    }
    
    private static void downloadAllMessages(Context context, boolean backgroundTask) {
        if(LogSettings.MARKET){
            MktLog.i(TAG, "downloadAllMessages(backgroundTask = " + backgroundTask + "): mPoolingTask = " + mPoolingTask);
        }
        
        String sdcard_state = Environment.getExternalStorageState();
        if (!sdcard_state.equals(Environment.MEDIA_MOUNTED)) {
            if(LogSettings.MARKET){
                MktLog.w(TAG, "downloadAllMessages(): SD card state not valid: " + sdcard_state + "; return");
            }
            return;
        }
    	
        mPoolingTask = !backgroundTask;

    	Cursor cursor = context.getContentResolver().query(
    	        UriHelper.getUri(RCMProvider.MESSAGES, RCMProviderHelper.getCurrentMailboxId(context)),
    	        DB_QUERY_PROJECTION_MESSAGES_DOWNLOAD, DB_QUERY_SELECTION_MESSAGES_DOWNLOAD, null, null);
     
    	if (cursor == null || cursor.getCount() <= 0) {
            if(LogSettings.MARKET){
                MktLog.w(TAG, "downloadAllMessages(): no messages to download; return");
            }
    	    if (cursor != null) {
    	        cursor.close();
    	    }
            return;
    	}
    	
    	cursor.moveToFirst();
    	do {
    	    long msg_id = cursor.getLong(COLUMN_INDEX_MSG_ID);
            if (LogSettings.ENGINEERING) {
                EngLog.i(TAG, "downloadAllMessages(): added message for body loading: " + msg_id);
            }

            if(backgroundTask){
                put(context, msg_id, MessageBodyLoader.sf_LOAD_PRIORITY_NORMAL);
            }else{                          
                m_msgLoader.setContext(context);
                BodyLoadedAsyncTask bodyLoader = new BodyLoadedAsyncTask(m_msgLoader);
                bodyLoader.doInBackground(msg_id);
                if(!mPoolingTask){
                    if (LogSettings.ENGINEERING) {
                        EngLog.i(TAG, "downloadAllMessages() received update from UI task, skipping pooling");
                    }
                    return;
                }
            }                        

            if(!backgroundTask){
                mPoolingTask = false;
            }
    	} while (cursor.moveToNext());
    	
    	cursor.close();
    }

    public static boolean loadMessage(Context context, long msgId) {
        if (LogSettings.ENGINEERING) {
            EngLog.i(TAG, "loadMessage() start process message: " + msgId);
        }

        if (!FileUtils.isStorageAccessible()) {
        	if (LogSettings.MARKET) {
                EngLog.i(TAG, "loadMessage(): Do nothing because storage is not accessible" );
            }
            return false;
        } 
        
        int current_status = MessageUtils.readIntMessageFieldFromDb(context, msgId, RCMDataStore.MessagesTable.RCM_SYNC_STATUS);
        //DK: Let's check message status before loading (it can be loaded already from other place)
        if (current_status != RCMDataStore.SyncStatusEnum.SYNC_STATUS_NOT_LOADED) {
            if (LogSettings.ENGINEERING) {
                EngLog.i(TAG, "loadMessage(): Do nothing because message: " + msgId +
                		( current_status == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED ? 
                				" is loaded already." : " is being loading" ));
            }

            if (current_status == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING) {
                return false;
            }
                
            if (current_status == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED
                    && MessageUtils.isValidMessageBody(context, msgId)) {
                return true;
            }
        }
        
        //DK: will not load body for deleted messages
        if (MessageUtils.readIntMessageFieldFromDb(context, msgId, RCMDataStore.MessagesTable.RCM_LOCALLY_DELETED) == 1) {
        	if (LogSettings.ENGINEERING) {
                EngLog.i(TAG, "loadMessage(): Do nothing because message: " + msgId + " was deleted" );
            }
        	return false;
        }
        
        MessageUtils.setMessageSyncStatus(context, msgId, RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADING, true);

        boolean result = false;

        try {
            String base64FilePath = getMessageBody(context, msgId);

            if (LogSettings.MARKET) {
                try {
                    EngLog.i(TAG, "loadMessage() body message loaded: " + msgId);
                } catch (java.lang.Throwable th) {
                }
            }

            if (base64FilePath != null && base64FilePath.length() > 0) {
                decodeFile( context, msgId, base64FilePath);
                result = true;
                MessageUtils.setMessageSyncStatus(context, msgId, RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED, true);
            }
        } catch (Exception e) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "loadMessage() Failed to load file:", e);
            } else if (LogSettings.MARKET) {
                QaLog.e(TAG, "loadMessage() Failed to load file: " + e.getMessage());
            }
        }

        if (LogSettings.MARKET) {
            try {
                QaLog.i(TAG, "loadMessage()finished: " + msgId + " result:" + (result ? "TRUE" : "FALSE"));
            } catch (java.lang.Throwable th) {
            }
        }

        if (!result) {
            MessageUtils.setMessageSyncStatus(context, msgId, RCMDataStore.SyncStatusEnum.SYNC_STATUS_NOT_LOADED, true);
        }

        return result;
    }



    /*
     * 
     */
    public static class MessageBodyLoader  {

    	private static final String TAG = "[RC]MessageBodyLoader";
    	
    	public static final int sf_LOAD_PRIORITY_NORMAL	= 0;
    	public static final int sf_LOAD_PRIORITY_HIGH	= 1;
    	
        private SortedSet<WeightMessagePOJO> m_msg_list;
        
        private Context m_context;

        private BodyLoadedAsyncTask m_loader = null;
        

        private long getID(){
			if( m_loader == null )
				return 0;
			
			return m_loader.getId();
		}
		
		/**
		 * @param loader the loader to set
		 */
        synchronized public void setLoader(BodyLoadedAsyncTask loader, long msgId) {
            m_loader = loader;
            if( loader != null ){
                m_loader.execute(msgId);
            }
        }
        
        public void setContext(Context context){
        	this.m_context = context;
        }

		public Context getContext(){
        	return m_context;
        }
        
        public MessageBodyLoader() {            
            m_msg_list = new TreeSet<WeightMessagePOJO>(new MessagePOJOComparator());
        }

        synchronized public boolean isOnList(Context context, long msgId, int priority){
            
            if( msgId == getID()){
                if (DEBUG && LogSettings.ENGINEERING) {
                    EngLog.i(TAG, "MessageBodyLoader is processing message with ID:" + msgId + ", priority: " + priority);
                }
                return true;
            }
            
            WeightMessagePOJO key = new WeightMessagePOJO(msgId, priority);
            final boolean result = m_msg_list.contains(key);
            
            if (DEBUG && LogSettings.ENGINEERING) {
                EngLog.i(TAG, "MessageBodyLoader Found message with ID:" + msgId + ", priority: " + priority);
            }
            
            return result;
        }
        
        synchronized void put(Context context, long msgId, int priority) {
            long msg_time = MessageUtils.readLongMessageFieldFromDb(context, msgId, RCMDataStore.MessagesTable.JEDI_CREATE_DATE);
            
            WeightMessagePOJO item = new WeightMessagePOJO(msgId, msg_time, priority);
            if (m_msg_list.contains(item)) { //check if this message in the list already
                return;
            }
            
            m_msg_list.add(item);

            if (DEBUG && LogSettings.ENGINEERING) {
                EngLog.i(TAG, "MessageBodyLoader Added message to queue message ID:" + msgId + "; priority: " + item.getPriority());
            }

            if (m_loader == null) {
                this.setLoader(new BodyLoadedAsyncTask( this ), getNext());

                if (DEBUG && LogSettings.ENGINEERING) {
                    EngLog.i(TAG, "MessageBodyLoader.put A new instance of BodyLoadedAsyncTask was created");
                }
            }
        }

        synchronized private long getNext() {
            long msg_id = 0;

            if (!m_msg_list.isEmpty()) {
            	
            	WeightMessagePOJO last_key = m_msg_list.last();
            	msg_id = last_key.getMsgId();
                m_msg_list.remove(last_key);

                if (DEBUG && LogSettings.ENGINEERING) {
                    EngLog.i(TAG, String.format( "MessageBodyLoader Will be downloaded body for message ID: %d[%d]. Message count: %d ",last_key.getMsgId(),last_key.getPriority(),m_msg_list.size() ));
                }
            }
            
            return msg_id;
        }

        synchronized private void clearAll(){
        	m_msg_list.clear();
        	
            if (DEBUG && LogSettings.ENGINEERING) {
                EngLog.i(TAG, "MessageBodyLoader Queue has been cleared");
            }
        	
        }        
    }

    
    public static class BodyLoadedAsyncTask extends AsyncTask<Long, Void, Integer> {
    	MessageBodyLoader m_owner;
    	private long m_curr_msg_id = 0;
    	
    	public long getId(){
    		return m_curr_msg_id;
    	}
    	
    	public BodyLoadedAsyncTask(MessageBodyLoader owner ){
    		m_owner = owner;
    	}

		@Override
		protected Integer doInBackground(Long... msgIds) {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
			Integer result = 0;
            if (msgIds != null && msgIds.length > 0 && msgIds[0] != null
                    && m_owner != null && m_owner.getContext() != null) {
            	
            	m_curr_msg_id = msgIds[0];
            	try {
                    FileUtils.createNomediaFileIfNotExist();
                    result = MessagesHandler.loadMessage(m_owner.getContext(), msgIds[0]) ? 1 : 0;
                    MessageUtils.updateDuration(m_owner.getContext(), msgIds[0]);
                } catch (Exception e) {
                    if (LogSettings.MARKET){
                        MktLog.e(TAG, "BodyLoadedAsyncTask:doInBackground: msg ID: "+m_curr_msg_id+" exc:"+e);
                    }
                }
            	m_curr_msg_id = 0;
            	
                if (DEBUG && LogSettings.ENGINEERING) {
                    EngLog.i(TAG, "BodyLoadedAsyncTask doInBackground is finished");
                }
            }
            
			return result;
		}

        @Override
		protected void onPostExecute( Integer result ){
			long msg_id = m_owner.getNext();
			if (msg_id != 0) {
			    m_owner.setLoader(new BodyLoadedAsyncTask(m_owner), msg_id );

	            if (DEBUG && LogSettings.ENGINEERING) {
	                EngLog.i(TAG, "BodyLoadedAsyncTask.onPostExecute A new instance of BodyLoadedAsyncTask was created");
	            }
			} else {
			    m_owner.setLoader(null, 0);
			    m_owner.setContext(null);
				if (DEBUG && LogSettings.ENGINEERING) {
	                EngLog.i(TAG, "BodyLoadedAsyncTask.onPostExecute Queue is empty");
				}
			}
		}		
    }

    
    
    
    private static synchronized void storeMessages(Context context, Map<Long, MessagePOJO> receivedMessagesList) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "storeMessages(): server list has " + receivedMessagesList.size() + " messages.");
        }
        
        long mailbox_id = RCMProviderHelper.getCurrentMailboxId(context);
        if( mailbox_id <= 0 ){
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "storeMessages(): failed. Mailbox ID is not detected " );
            }
            return;
        }

        /*
         * message SYNC procedure
         * 1. if message is present on local list and not present on server list - delete it locally (mark as deleted)
         * Note: We should have maximum(!) message ID of loaded messages.
         * 2. if message is marked as deleted locally but not present on server list - delete it from server (TODO; not implemented yet) 
         */
        long max_stored_msg_id = RCMProviderHelper.getLastLoadedMsgId(context);
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "storeMessages(): max_stored_msg_id = " + max_stored_msg_id);
        }
        
        long max_received_msg_id = 0; 
        if (receivedMessagesList.size() > 0) {
            max_received_msg_id = Collections.max(receivedMessagesList.keySet());
        }
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "storeMessages(): max_received_msg_id = " + max_received_msg_id);
        }
        
        if (max_received_msg_id > max_stored_msg_id) {
            RCMProviderHelper.saveLastLoadedMsgId(context, max_received_msg_id);
        }
        
        ArrayList<ContentValues> new_messages_to_store = new ArrayList<ContentValues>();
        ArrayList<Long> messages_to_delete_locally = new ArrayList<Long>();
        ArrayList<Long> messages_to_set_read = new ArrayList<Long>();
        ArrayList<Long> messages_to_set_unread = new ArrayList<Long>();
        
        int new_unread_count = 0;
        
        Map<Long, String[]> stored_message_list = readMessageListFromDb(context);
        if (LogSettings.MARKET) {
            QaLog.i(TAG, "storeMessages(): stored_message_list.size() = " + stored_message_list.size());
        }
        
        Set<Map.Entry<Long, String[]>> stored_message_set = stored_message_list.entrySet();
        Set<Map.Entry<Long, MessagePOJO>> received_messages_set = receivedMessagesList.entrySet();

        for (Map.Entry<Long, MessagePOJO> map_entry : received_messages_set) {
            long msg_id = map_entry.getKey();
            MessagePOJO received_msj_pojo = map_entry.getValue();

            if (msg_id > max_stored_msg_id) {     //new message; will be added to DB
                ContentValues values = fillContentValues(mailbox_id, received_msj_pojo); 
                new_messages_to_store.add(values);
                
                if (received_msj_pojo.getReadStatus() == MessagePOJO.UNREAD) {
                    new_unread_count++;
                }
            } else {                              //"old" message which already has been stored in DB 
                String[] stored_msg = stored_message_list.get(msg_id);
                if (stored_msg != null) { //message with this ID exists in DB
                    int stored_read_status = Integer.parseInt(stored_msg[MSG_LIST_QUERY_JEDI_READ_STATUS_INDEX]);
                    if (stored_read_status != received_msj_pojo.getReadStatus()) {
                        if (stored_read_status == MessagePOJO.READ) {
                            messages_to_set_unread.add(msg_id);
                        } else {
                            messages_to_set_read.add(msg_id);
                        }
                    }
                }
            }
        }

        if (LogSettings.MARKET) {
            QaLog.i(TAG, "storeMessages(): new_messages_to_store.size() = " + new_messages_to_store.size());
        }

        for (Map.Entry<Long, String[]> map_entry : stored_message_set) {
            long msg_id = map_entry.getKey();
            String[] stored_msg = map_entry.getValue();

            int msg_locally_deleted_status = Integer.parseInt(stored_msg[MSG_LIST_QUERY_RCM_LOCALLY_DELETED_INDEX]);
            if (msg_locally_deleted_status != MessagePOJO.DELETED_LOCALLY_TRUE
                    && !receivedMessagesList.containsKey(msg_id)) {  //message deleted on server; will be marked as LOCALLY DELETED in DB
                messages_to_delete_locally.add(msg_id);
            }
        }
        
        int db_rows_changed = 0;
        
        if (messages_to_delete_locally.size() > 0) {
            int count = MessageUtils.setMessagesDeletedStatus(
                    context, mailbox_id, messages_to_delete_locally, MessagePOJO.DELETED_LOCALLY_TRUE);

            if (LogSettings.MARKET) {
                MktLog.i(TAG, "storeMessages(): " + count + " records deleted");
            }
       
            db_rows_changed += count; 
        }

        if (messages_to_set_read.size() > 0) {
            int count = MessageUtils.setMessagesReadStatus(context, mailbox_id, messages_to_set_read, MessagePOJO.READ);

            if (LogSettings.MARKET) {
                MktLog.i(TAG, "storeMessages(): " + count + " records set READ");
            }
            
            db_rows_changed += count; 
        }

        if (messages_to_set_unread.size() > 0) {
            int count = MessageUtils.setMessagesReadStatus(context, mailbox_id, messages_to_set_unread, MessagePOJO.UNREAD);

            if (LogSettings.MARKET) {
                MktLog.i(TAG, "storeMessages(): " + count + " records set UNREAD");
            }
            
            db_rows_changed += count; 
        }

        if (new_messages_to_store != null && new_messages_to_store.size() > 0) {
            ContentValues[] values = new_messages_to_store.toArray(new ContentValues[new_messages_to_store.size()]);
            int count = context.getContentResolver().bulkInsert(UriHelper.getUri(RCMProvider.MESSAGES), values);

            if (LogSettings.MARKET) {
                MktLog.i(TAG, "storeMessages(): " + count + " records added");
            }
            
            db_rows_changed += count; 
        }
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "storeMessages(): Total DB rows changed: " + db_rows_changed);
        }
        
        if( db_rows_changed > 0 ) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "storeMessages(): sending broadcast notification");
            }
            context.sendBroadcast(new Intent(RCMConstants.ACTION_UPDATE_MESSAGES_LIST).
                    putExtra(Messages.MESSAGE_LIST_UPDATE_BROADCAST_EXTRA, Messages.INTENT_EXTRA_TYPES.ADD_MESSAGES));
        }
        
        if (new_unread_count > 0) {
            if(LogSettings.MARKET){
                MktLog.d(TAG, "storeMessages(): Received new messages : " + new_unread_count + " , updating notifications");
            }
            MessagesNotification.updateNotification(context, new_unread_count);
        }
        
       
        //DK: we should update server state now
    }

    
    private static final String[] MSG_LIST_QUERY_PROJECTION = new String[] {
        RCMDataStore.MessagesTable.JEDI_MSG_ID,
        RCMDataStore.MessagesTable.JEDI_READ_STATUS,
        RCMDataStore.MessagesTable.RCM_LOCALLY_DELETED};
    private static final String MSG_LIST_QUERY_LIST_ORDER = MessagesTable.JEDI_CREATE_DATE + " DESC ";
    private static final int MSG_LIST_QUERY_JEDI_MSG_ID_INDEX = 0;
    private static final int MSG_LIST_QUERY_JEDI_READ_STATUS_INDEX = 1;
    private static final int MSG_LIST_QUERY_RCM_LOCALLY_DELETED_INDEX = 2;
    
    static Map<Long, String[]> readMessageListFromDb(Context context) {
        HashMap<Long, String[]> msg_list = new HashMap<Long, String[]>();

        long mailbox_id = RCMProviderHelper.getCurrentMailboxId(context);

        Cursor cursor = context.getContentResolver().query(
                UriHelper.getUri(RCMProvider.MESSAGES, mailbox_id),
                MSG_LIST_QUERY_PROJECTION, null, null, MSG_LIST_QUERY_LIST_ORDER);
        
        if (cursor == null || cursor.getCount() <= 0) {
            if(LogSettings.MARKET){
                MktLog.w(TAG, "readMessagesFromDb(): no messages; return null");
            }
            if (cursor != null) {
                cursor.close();
            }
            return msg_list;
        }
        
        
        cursor.moveToFirst();
        do {
            String[] item = new String[MSG_LIST_QUERY_PROJECTION.length];
            long msg_id = cursor.getLong(MSG_LIST_QUERY_JEDI_MSG_ID_INDEX);
            for (int i = 0; i < MSG_LIST_QUERY_PROJECTION.length; i++) {
                item[i] = cursor.getString(i);
            }
            msg_list.put(msg_id, item);
        } while (cursor.moveToNext());
        cursor.close();
        
        return msg_list;
    }
    
    private static ContentValues fillContentValues(long mailbox_id, MessagePOJO msg) {
        ContentValues values = new ContentValues();
    
        values.put(MessagesTable.MAILBOX_ID,          mailbox_id);            
        values.put(MessagesTable.JEDI_BODY_SIZE,      msg.getBody_size());            
        values.put(MessagesTable.JEDI_DURATION,       msg.getDuration());            
        values.put(MessagesTable.JEDI_CREATE_DATE,    msg.getTimeInMillis());            
        values.put(MessagesTable.JEDI_FILE_EXT,       msg.getFileExtension());
        values.put(MessagesTable.JEDI_FROM_NAME,      msg.getFromName());
        values.put(MessagesTable.BIND_DISPLAY_NAME,   "");   //temporary use JEDI name until binding performed
        values.put(MessagesTable.JEDI_FROM_PHONE,     msg.getFromPhone());
        values.put(MessagesTable.JEDI_MSG_ID,         msg.getId());
        values.put(MessagesTable.JEDI_MSG_TYPE,       msg.getMessageType().getValue());
        values.put(MessagesTable.JEDI_READ_STATUS,    msg.getReadStatus());
        values.put(MessagesTable.JEDI_COMP_TYPE,      msg.getCompType().getValue());
        values.put(MessagesTable.RCM_LOCALLY_DELETED, msg.getDeletedLocally());
        values.put(MessagesTable.RCM_DURATION_SYNCHRONIZED, msg.getDurationSyncState());
        values.put(MessagesTable.RCM_LOCALLY_READ,    msg.getReadStatusChangedLocally());
        values.put(MessagesTable.RCM_SYNC_STATUS,     msg.getSyncStatus() );
        values.put(MessagesTable.JEDI_TO_NAME,        msg.getToPhone()); //In the response string, ToName is stored in the ToPhone place. 
		values.put(MessagesTable.JEDI_TO_PHONE, 	  msg.getToName());  //In the response string, ToPhone is stored in the ToName place. 
        
        String from_number = msg.getFromPhone();
        ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumberWithoutTrim(from_number);
        if (cpn == null || !cpn.isValid) {
            cpn = PhoneUtils.getContactPhoneNumber(from_number);    
        }
        if (cpn != null && cpn.isValid) {
            values.put(RCMDataStore.MessagesTable.RCM_FROM_PHONE_NORMALIZED_NUMBER, cpn.normalizedNumber);
            values.put(RCMDataStore.MessagesTable.RCM_FROM_PHONE_IS_VALID_NUMBER, 1);
        } else {
            values.put(RCMDataStore.MessagesTable.RCM_FROM_PHONE_NORMALIZED_NUMBER, from_number);
            values.put(RCMDataStore.MessagesTable.RCM_FROM_PHONE_IS_VALID_NUMBER, 0);
        }
        
        return values;
    }

    private static void checkMessageModificationCounter( Context context, long mod_counter ){
        long current_mod_counter = RCMProviderHelper.getMsgModCounter(context);

        if( LogSettings.ENGINEERING ) {
            EngLog.i(TAG, "checkMessageModificationCounter counter local: " + current_mod_counter + " from server: " + mod_counter );
        }

        if( current_mod_counter < mod_counter ){

                if( LogSettings.MARKET ) {
                    MktLog.i(TAG, "checkMessageModificationCounter: Sent notification (changed)");
                }

                MessagesHandler.updateList(context, true);
        }
    }


    
}
