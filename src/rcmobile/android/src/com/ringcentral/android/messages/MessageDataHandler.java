/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.messages;

import java.io.IOException;
import java.io.Writer;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.api.XmlParsingObjectAbstract;

public class MessageDataHandler extends XmlParsingObjectAbstract {

	private boolean isMsgBody = false;
	private final static String sf_tag_name 				= "MESSAGE";
	private final static String sf_msg_body 				= "MsgBody";
	private final static String sf_msg_id 					= "MsgID";
	
	private Writer m_wr;

	private static final String TAG = "[RC] MessageDataHandler";

	private String m_messageID = new String();
	
    public MessageDataHandler(XmlParsingObjectAbstract parent, Writer wr) {
    	super( parent );
		m_wr = wr;
	}

	public void startElement(String uri, String localName, String qName, Attributes attributes )  throws SAXException {
		super.startElement(uri, localName, qName, attributes );

		String elementName = getElementName( localName, qName );
	
		if (elementName.equals( sf_msg_body )){
			isMsgBody = true;
		}
	}

	public void endElement(String uri, String localName, String qName ) {
		super.endElement(uri, localName, qName);
		
		String elementName = getElementName( localName, qName );
		
		if( elementName.equals( sf_msg_body )) {
			isMsgBody = false;
		}
		else if( elementName.equals( sf_msg_id ) ){
			m_messageID = getValue();
		}
	}

	public String getMessageID() {
		return m_messageID;
	}

	public void characters(char chars[], int start, int length) {
		 if( isMsgBody ) {
			try {
			    String strCharacters = new String(chars, start, length);					
				m_wr.write( strCharacters );
			} 
			catch (IOException e) {
				if (LogSettings.ENGINEERING) {
                    EngLog.w(TAG, "characters()", e);
                }
            }
		}
		 else{
			 super.characters(chars, start, length );
		 }
	}

	@Override
	public String getTagName() {
		return sf_tag_name;
	}
}
