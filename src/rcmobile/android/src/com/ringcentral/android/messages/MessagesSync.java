/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.messages;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.contacts.Cont;
import com.ringcentral.android.contacts.ContactPhoneNumber;
import com.ringcentral.android.contacts.Cont.acts.BindSync;
import com.ringcentral.android.contacts.Cont.acts.ContactBinding;
import com.ringcentral.android.contacts.LocalSyncService.LocalSyncControl;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.provider.RCMDataStore.MessagesTable;
import com.ringcentral.android.utils.PhoneUtils;

public class MessagesSync {
	private static final String TAG = "[RC]MessagesSync";
	
	 static class SyncItem {
	        long id;
	        BindSync sync;
	 }
	 
	 private static final String[] SYNC_PROJECTION = { 
            MessagesTable._ID,
            MessagesTable.JEDI_FROM_PHONE,
            MessagesTable.RCM_FROM_PHONE_IS_VALID_NUMBER,
            MessagesTable.RCM_FROM_PHONE_NORMALIZED_NUMBER,
            MessagesTable.BIND_HAS_CONTACT,
            MessagesTable.BIND_ID,
            MessagesTable.BIND_DISPLAY_NAME,
            MessagesTable.BIND_ORIGINAL_NUMBER,
            MessagesTable.BIND_IS_PERSONAL_CONTACT            
    };
	 
    private static final int SYNC_ID_INDX = 0;
    private static final int SYNC_JEDI_FROM_PHONE_INDX = 1;
    private static final int SYNC_IS_VALID_NUMBER_INDX = 2;
    private static final int SYNC_NORMALIZED_NUMBER_INDX = 3;
    private static final int SYNC_BIND_HAS_CONTACT_INDX = 4;
    private static final int SYNC_BIND_ID_INDX = 5;
    private static final int SYNC_BIND_DISPLAY_NAME_INDX = 6;
    private static final int SYNC_BIND_ORIGINAL_NUMBER_INDX = 7;
    private static final int SYNC_BIND_IS_PERSONAL_CONTACT_INDX = 8;
	
	
	/**
     * Synchronize Messages records.
     * 
     * @param sync the sync control
     * 
     * @return the status
     */
    public static final boolean sync(LocalSyncControl sync) {
    	Uri uri = UriHelper.getUri(RCMProvider.MESSAGES, sync.getMailboxId());
        Cursor c = null;
        ArrayList<SyncItem> updates = new ArrayList<SyncItem>();
        try{
        	c = sync.getContext().getContentResolver().query(uri, SYNC_PROJECTION, null, null, null);
        	if (c != null) {
                c.moveToPosition(-1);
                while (c.moveToNext()) {
                    try {
                        if (sync.isTerminated()) {
                            return false;
                        }                        
                        SyncItem item = new SyncItem();
                        item.id = c.getLong(SYNC_ID_INDX);
                        ContactBinding bind = readSyncBind(sync.getContext(), c);
                        BindSync bsync = Cont.acts.syncBind(sync.getContext(), sync.getMailboxId(), bind);
                        
                        if (bsync.syncState == BindSync.State.INVALID_RECORD) {
                            Cont.acts.bindSyncTrace(sync.getContext(), bsync, TAG, "", item.id);
                        } else if (bsync.syncState == BindSync.State.NOT_CHANGED) {
                            continue;
                        } else {
                            Cont.acts.bindSyncTrace(sync.getContext(), bsync, TAG, "", item.id);
                            item.sync = bsync;
                            updates.add(item);
                        }
                        
                    } catch (java.lang.Throwable error) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "sync (read): ", error);
                        }
                    }
                }
            }
        } catch (java.lang.Throwable t) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "sync: ", t);
            }
            return false;
        } finally {
            if (c != null) {
                c.close();
            }
        }
        if (sync.isTerminated()) {
            return false;
        }
        
        if (updates.size() > 0) {
            for (SyncItem item: updates) {
                updateMessagesBind(sync.getContext(), sync.getMailboxId(), item.id, item.sync);
                if (sync.isTerminated()) {
                    return true;
                }
            }
            sync.getContext().sendBroadcast(new Intent(RCMConstants.ACTION_UPDATE_MESSAGES_LIST).putExtra(Messages.MESSAGE_LIST_UPDATE_BROADCAST_EXTRA, Messages.INTENT_EXTRA_TYPES.BIND_STATE_CHANGED));
            return true;
        }
        return false;
    }
    
    private static ContactBinding readSyncBind(Context context, Cursor c) {
        ContactBinding bind = new ContactBinding();
        
        long validIndex = c.getLong(SYNC_IS_VALID_NUMBER_INDX);
        String normalizedNumber = c.getString(SYNC_NORMALIZED_NUMBER_INDX);
        
        if (validIndex == -1) {
        	final String phoneNumber = c.getString(SYNC_JEDI_FROM_PHONE_INDX);        	        	
        	ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumberWithoutTrim(phoneNumber);
            if (cpn == null || !cpn.isValid) {
                cpn = PhoneUtils.getContactPhoneNumber(phoneNumber);    
            }       
            final boolean isValid = (cpn != null && cpn.isValid);
            MessageUtils.updateValidBind(context, c.getLong(SYNC_ID_INDX),  isValid, isValid ? cpn.normalizedNumber : phoneNumber);            
            validIndex = isValid ? 1 : 0;
            normalizedNumber = isValid ? cpn.normalizedNumber : phoneNumber;
        }
        if (validIndex == 0) {
            bind.isValid = false;
            return bind;
        }

        
        bind.originalNumber = normalizedNumber;
        bind.cpn = PhoneUtils.getContactPhoneNumber(bind.originalNumber);
        bind.isValid = bind.cpn.isValid;
        
        if (!bind.isValid) {
            return bind;
        }
        
        bind.hasContact = (c.getLong(SYNC_BIND_HAS_CONTACT_INDX) > 0);
        
        if (bind.hasContact) {
            bind.phoneId = c.getLong(SYNC_BIND_ID_INDX);
            bind.displayName = c.getString(SYNC_BIND_DISPLAY_NAME_INDX);
            bind.isPersonalContact = (c.getLong(SYNC_BIND_IS_PERSONAL_CONTACT_INDX) > 0);
            bind.phoneNumber = c.getString(SYNC_BIND_ORIGINAL_NUMBER_INDX);
        }
        
        return bind;
    }

//    static ContactBinding readSyncBind(Context context, MessagePOJO message) {
//        ContactBinding bind = new ContactBinding();
//        final String phoneNumber = message.getFromPhone();
//        ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumberWithoutTrim(phoneNumber);
//        if (!cpn.isValid) {
//            cpn = PhoneUtils.getContactPhoneNumber(phoneNumber);    
//        }
//        if(!cpn.isValid){
//            bind.isValid = false;
//            return bind;
//        }                                   
//        bind.originalNumber = cpn.isValid ? cpn.normalizedNumber : phoneNumber;
//        bind.cpn = PhoneUtils.getContactPhoneNumber(bind.originalNumber);
//        bind.isValid = bind.cpn.isValid;        
//        if (!bind.isValid) {
//            return bind;
//        }        
//        bind.hasContact = message.isBinded();
//        if (bind.hasContact) {
//            bind.phoneId = message.getBindId();
//            bind.displayName = message.getBindDisplayName();
//            bind.isPersonalContact = message.isBindPersonalContact();
//            bind.phoneNumber = message.getBindOriginalNumber();
//        }        
//        return bind;
//    }
    
    public static void updateMessagesBind(Context context, long mailboxId, long recordId, BindSync sync) {
        try {
            Uri uri = UriHelper.getUri(RCMProvider.MESSAGES, mailboxId, recordId);
            context.getContentResolver().update(uri, getCvForUpdate(sync), null, null);
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "updateMessagesBind error: " + error.toString());
            }
        }
    }
    
    private static ContentValues getCvForUpdate(BindSync sync) {
        ContentValues cv = new ContentValues();
        cv.put(RCMDataStore.MessagesTable.BIND_HAS_CONTACT, sync.bind.hasContact ? 1:0);
        cv.put(RCMDataStore.MessagesTable.BIND_ID, sync.bind.phoneId);
        cv.put(RCMDataStore.MessagesTable.BIND_DISPLAY_NAME, sync.bind.displayName);
        cv.put(RCMDataStore.MessagesTable.BIND_IS_PERSONAL_CONTACT, sync.bind.isPersonalContact ? 1:0);
        cv.put(RCMDataStore.MessagesTable.BIND_ORIGINAL_NUMBER, sync.bind.phoneNumber);
        return cv;
    }
    


}
 