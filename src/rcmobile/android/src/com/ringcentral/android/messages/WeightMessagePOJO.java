/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.messages;


public class WeightMessagePOJO {

    private long m_msgId = 0;
	private int m_priority = 0;
	private long m_createDate = 0;
	
	/**
	 * @return the createDate
	 */
	public long getCreateDate() {
		return m_createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(long createDate) {
		m_createDate = createDate;
	}

	/**
	 * @return the msgId
	 */
	public long getMsgId() {
		return m_msgId;
	}

	/**
	 * @param msgId the msgId to set
	 */
	public void setMsgId(long msgId) {
		m_msgId = msgId;
	}

	/**
	 * @return the priority
	 */
	public int getPriority() {
		return m_priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(int priority) {
		m_priority = priority;
	}

	@Override
	public boolean equals(Object obj) {
	    if (obj == null || !(obj instanceof WeightMessagePOJO)) {
	        return false;
	    }
	    
	    if (((WeightMessagePOJO)obj).m_msgId == m_msgId
	            && ((WeightMessagePOJO)obj).m_priority == m_priority) {
	        return true;
	    } else {
		    return false;
	    }
	}
	
    @Override
	public int hashCode() {
	    return (int)m_msgId;
	}
	
	
	public WeightMessagePOJO(long msgId, long createDate, int priority ){
		this.setMsgId(msgId);
		this.setPriority(priority);
		this.setCreateDate(createDate);
	}

    public WeightMessagePOJO(long msgId, int priority ){
        this.setMsgId(msgId);
        this.setPriority(priority);
    }
}
