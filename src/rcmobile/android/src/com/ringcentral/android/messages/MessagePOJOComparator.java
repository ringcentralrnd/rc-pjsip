/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.messages;

import java.util.Comparator;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;


public class MessagePOJOComparator implements Comparator<WeightMessagePOJO> {

    private static final boolean DEBUG = false;
    private static final String TAG = "[RC] MessagePOJOComparator";
	
	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(WeightMessagePOJO msg1, WeightMessagePOJO msg2) {
		int result;

		if( msg1.getPriority() != msg2.getPriority()) {
			result = (msg1.getPriority() > msg2.getPriority() ? 1 : -1 );
		} else if( msg1.getCreateDate() != msg2.getCreateDate() ){
			result = (msg1.getCreateDate() > msg2.getCreateDate() ? 1 : -1 );
		} else {
		    result = 0;
		}

        if (DEBUG && LogSettings.ENGINEERING) {
            EngLog.i(TAG, String.format("Compare messages 1: %d[%d][%d] 2: %d[%d][%d] result %d",
                    msg1.getMsgId(), msg1.getCreateDate(), msg1.getPriority(), msg2.getMsgId(), msg2.getCreateDate(), msg2.getPriority(), result));
        }
		
		return result;
	}
	
}
