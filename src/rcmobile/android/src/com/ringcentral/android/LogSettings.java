package com.ringcentral.android;

public class LogSettings {
    private static final int MARKET_ENABLED = 0;
    private static final int QA_ENABLED = 1;
    private static final int ENGINEERING_ENABLED = 3;
    @SuppressWarnings("unused")
    private static final int NONE = -1;

    // Do not touch the line and log-levels definitions. Otherwise update build.xml
    private static final int LOG_LEVEL=ENGINEERING_ENABLED;

    public static final boolean MARKET = (LogSettings.LOG_LEVEL >= LogSettings.MARKET_ENABLED);
    public static final boolean QA = (LogSettings.LOG_LEVEL >= LogSettings.QA_ENABLED);
    public static final boolean ENGINEERING = (LogSettings.LOG_LEVEL >= LogSettings.ENGINEERING_ENABLED);
}
