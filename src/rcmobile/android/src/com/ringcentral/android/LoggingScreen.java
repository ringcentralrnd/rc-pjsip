/**
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 */
package com.ringcentral.android;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.LoggingManager;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.rcbase.android.logging.utils.AppInfo;
import com.rcbase.android.logging.utils.LogFilesUtils;
import com.rcbase.android.logging.utils.LogsZipper;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.utils.EmailSender;
import com.ringcentral.android.utils.StorageStateReceiver;
import com.ringcentral.android.utils.ui.ActivityUtils;

public class LoggingScreen extends ListActivity implements View.OnCreateContextMenuListener, OnItemClickListener {
    private static final String TAG = "[RC]LoggingScreen"; //
    private static final int currentLogPosition = 0; //

    //
    private static final int SEND_VIA_EMAIL = 1; //
    private static final int VIEW = 2; //
    private static final int DELETE = 3; //

    // progress dialogs identifier
    private static final int PROGRESS_DIALOG_COLLECTING_LOG = 0;
    private static final int PROGRESS_DIALOG_UNZIPPING_LOG = 1;
    
    private static final int OBSERVING_LOG_DIALOG = 2;

    // actions which could be completed using handler
    private static final int ACTION_GET_CURRENT_LOG = 1;
    private static final int ACTION_CURRENT_LOG_COLLECTED = 2;
    private static final int ACTION_SEND_SELECTED_LOG = 3;

    private static final int ACTION_VIEW_SELECTED_LOG = 4;
    private static final int ACTION_UNZIP_SELECTED_LOG = 5;
    private static final int ACTION_SELECTED_LOG_UNZIPED = 6;
    protected static final boolean DEBUG = false;
    
    private boolean isItNecessaryToShowLog = true;

    ZipThread progressThread;
    UnZipThread unzipThread;
    ProgressDialog progressDialog;
    Dialog observeDialog;
    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            this.isItNecessaryToShowLog = true;
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.logging_and_troubleshooting_screen_view);

            String[] listOfItemsToBeShown = getListOfItemsToBeShown();

            setListAdapter(new ArrayAdapter<String>(this, R.layout.logging_and_troubleshooting_list_item,listOfItemsToBeShown));

            ListView ldv = getListView();
            if (null != ldv) {
                ldv.setOnItemClickListener(this);
                registerForContextMenu(getListView());
            }
        } catch (java.lang.Throwable e) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, " Logging screen ", e);
            }
        }
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        
        switch (id) {

        case PROGRESS_DIALOG_COLLECTING_LOG:
            progressDialog = new ProgressDialog(LoggingScreen.this);
            progressDialog.setMessage(getString(R.string.landt_collecting_log_message));
            return progressDialog;

        case PROGRESS_DIALOG_UNZIPPING_LOG:
            progressDialog = new ProgressDialog(LoggingScreen.this);
            progressDialog.setMessage(getString(R.string.landt_collecting_log_message));
            return progressDialog;

        default:
            return null;
        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {

        if (LoggingManager.isStorageAccessible()) {
            AdapterView.AdapterContextMenuInfo aMenuInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
            int position = currentLogPosition + 42; // some digit which not
                                                    // equals currentLogPosition

            if (null != aMenuInfo) {
                position = aMenuInfo.position;
            } else {
                if (null != view) {
                    try {
                        String textstring = (String) ((TextView) view).getText();

                        if (null != textstring && textstring.equals(getString(R.string.landt_current_log))) {
                            position = currentLogPosition;
                        }

                    } catch (Throwable th) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, " onCreateContextMenu ", th);
                        }
                    }
                }
            }

            menu.add(0, SEND_VIA_EMAIL, 0, R.string.landt_send_log);
            // TODO: create
            // menu.add(0, VIEW, VIEW, R.string.landt_view_log);

            // if it is not current log - we
            if (currentLogPosition != position) {
                menu.add(0, DELETE, 0, R.string.landt_delete_log);
                menu.add(0, VIEW, 0, R.string.landt_view_log);
            }
        } else {
            // showing the alert dialog
            RcAlertDialog.getBuilder(this).setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).setTitle(R.string.messages_storage_error_title).setMessage(StorageStateReceiver.getStateMessage(this)).show();
        }
        
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) < 5 && keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            onBackPressed();
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        this.dropCurrentUnZipProcess();
        
        this.finish();
    }
    
    
    /** Nested class that performs progress calculations (counting) */
    private class ZipThread extends Thread {
        Handler mHandler;
        
        ZipThread(Handler h) {
            super("ZipThread");
            this.mHandler = h;
        }

        public void run() {
            try {
                String selectedLogFilePath = LoggingManager.finalizeLogsCollection(LoggingManager.LogsFinalizationReason.USER_INITIATED, null);

                Message msg = handler.obtainMessage();
                msg.what = ACTION_CURRENT_LOG_COLLECTED;
                msg.obj = selectedLogFilePath;
                handler.sendMessage(msg);
            } catch (java.lang.Throwable th) {
            }
        }
    }

    void dropCurrentUnZipProcess() {
        this.isItNecessaryToShowLog = false;
        
        if(null != this.unzipThread) {
            this.unzipThread.shouldWeDropUnZip = true;
        }
    }

    /** Nested class that performs progress calculations (counting) */
    public class UnZipThread extends Thread {
        public volatile boolean shouldWeDropUnZip = false;
        public          String  file = "";
        private         Handler mHandler = null;
        

        UnZipThread(Handler h, String fileToBeUnZipped) {
            super("UnZipThread");
            if (null != h && null != fileToBeUnZipped) {
                this.mHandler = h;
                this.file = fileToBeUnZipped;
            }
        }
        
        public boolean shouldWeContinue() {
            return !this.shouldWeDropUnZip;
        }

        public void run() {
            try {
                if (null != this.mHandler && !this.shouldWeDropUnZip) {
                    Message superMsg = this.mHandler.obtainMessage();
                    superMsg.what = ACTION_SELECTED_LOG_UNZIPED;
                    superMsg.obj = LogsZipper.unzipFileAndProvideContentAsString(this);
                    if(this.shouldWeContinue()){
                        this.mHandler.sendMessage(superMsg);
                    }
                }
            } catch (java.lang.Throwable th) {
                if(LogSettings.MARKET) {
                    MktLog.e(TAG, " UnZipThread ", th);
                }
            }
        }
    }


    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        
        switch (id) {

        case PROGRESS_DIALOG_COLLECTING_LOG:
            progressDialog.setProgress(0);
            progressDialog.setTitle(getString(R.string.landt_collecting_log_message));
            progressDialog.setMessage(getString(R.string.landt_collecting_log_message));
            break;
        
        case PROGRESS_DIALOG_UNZIPPING_LOG:
            progressDialog.setProgress(0);
            progressDialog.setTitle(getString(R.string.landt_collecting_log_message));
            progressDialog.setMessage(getString(R.string.landt_collecting_log_message));
            break;
        
        default:
            break;
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // if it's not possible to get access to storage - notify user
        if (!LoggingManager.isStorageAccessible()) {
            RcAlertDialog.getBuilder(this).setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).setTitle(R.string.messages_storage_error_title).setMessage(StorageStateReceiver.getStateMessage(this)).show();
            listChanged();
        }

        AdapterView.AdapterContextMenuInfo info;
        try {
            info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        } catch (ClassCastException e) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "onContextItemSelected() menuInfo ", e);
            } else if (LogSettings.QA) {
                QaLog.e(TAG, "onContextItemSelected() menuInfo: " + e.getMessage());
            }
            return false;
        }

        String selectedLogFilePath = null;
        Message msg = null;

        try {

            if (currentLogPosition != info.position) {
                selectedLogFilePath = (String) getListView().getItemAtPosition(info.position);
                selectedLogFilePath = LogFilesUtils.getPathToTheFileBySpecifiedName(selectedLogFilePath);
            }

            switch (item.getItemId()) {
            
            case SEND_VIA_EMAIL:
                msg = handler.obtainMessage();
                if (null != msg) {
                    if (currentLogPosition == info.position) {
                        msg.what = ACTION_GET_CURRENT_LOG;
                    } else {
                        msg.what = ACTION_SEND_SELECTED_LOG;
                        msg.obj = selectedLogFilePath;

                    }

                    handler.sendMessage(msg);
                }
                break;
                
            case VIEW:
                msg = handler.obtainMessage();
                if (null != msg) {
                    if (currentLogPosition != info.position) {
                        msg.what = ACTION_UNZIP_SELECTED_LOG;
                        msg.obj = selectedLogFilePath;
                        handler.sendMessage(msg);
                    }
                }

                break;
                
            case DELETE:
                if (LogFilesUtils.removeLogFile(selectedLogFilePath)) {
                    listChanged();
                }
                break;
            default:
                break;
            }

        } catch (Throwable e) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "onContextItemSelected: " , e);
            }
        }

        return true;
    }


    /**
     * This is handler for receiving the
     */
    public Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            try {
                if (null != msg) {

                    if (LogSettings.ENGINEERING & DEBUG) {
                        EngLog.e(TAG, "handleMessage " + msg.toString());
                    }

                    int what = msg.what;

                    switch (what) {

                    case ACTION_GET_CURRENT_LOG:
                        showDialog(PROGRESS_DIALOG_COLLECTING_LOG);
                        progressThread = new ZipThread(handler);
                        progressThread.start();

                        break;

                    case ACTION_CURRENT_LOG_COLLECTED:
                        Message msgToSend = handler.obtainMessage();

                        if (null != msgToSend) {
                            msgToSend.what = ACTION_SEND_SELECTED_LOG;
                            msgToSend.obj = msg.obj;
                            handler.sendMessage(msgToSend);
                            progressDialog.dismiss();
                        }

                        break;

                    case ACTION_SEND_SELECTED_LOG:
                        String _file = (String) msg.obj;

                        if (null != _file) {
                            EmailSender emailSender = new EmailSender(LoggingScreen.this);
                            emailSender.sendEmail(
                                    new String[] { getString(R.string.landt_email) },
                                    getString(R.string.landt_email_subj,
                                            RCMProviderHelper.getCurrentMailboxId(LoggingScreen.this)),
                                    AppInfo.shortAppInfo(LoggingScreen.this),
                                    _file);
                        }
                        break;

                    case ACTION_UNZIP_SELECTED_LOG:
                        String file = (String) msg.obj;

                        if (null != unzipThread) {
                            dropCurrentUnZipProcess();
                            unzipThread = null;
                        }

                        isItNecessaryToShowLog = true;

                        showDialog(PROGRESS_DIALOG_UNZIPPING_LOG);
                        unzipThread = new UnZipThread(handler, file);
                        unzipThread.start();

                        break;

                    case ACTION_SELECTED_LOG_UNZIPED:
                        if (null != msg && null != msg.obj && !isFinishing() && isItNecessaryToShowLog) {
                            try {
                                if (null != unzipThread) {
                                    unzipThread = null;
                                }
                                progressDialog.dismiss();

                                if(null != msg.obj) {
                                    Intent logObservingIntent = new Intent(getApplicationContext(), LogObserveScreen.class);
                                    Bundle extraData = new Bundle();
                                    extraData.putString(LogObserveScreen.logContentKey, (String) msg.obj);
                                    logObservingIntent.putExtras(extraData);
                                    startActivityForResult(logObservingIntent, 0);
                                }
                            } catch (Throwable th) {
                                if (LogSettings.MARKET & DEBUG) {
                                    MktLog.e(TAG, " ACTION_SELECTED_LOG_UNZIPPED ", th);
                                }
                            }
                        }
                        break;

                    default:
                        break;
                    }
                }
            } catch (Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, " ", th);
                }
            }
        }
            
    };


    @Override
    public void onConfigurationChanged(Configuration conf){
        super.onConfigurationChanged(conf);
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        listChanged();
    }


    private void listChanged() {
        String[] listOfItemsToBeShown = getListOfItemsToBeShown();

        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(this, R.layout.logging_and_troubleshooting_list_item, listOfItemsToBeShown);
        setListAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();
    }

    
    private String[] getListOfItemsToBeShown() {
        String[] listOfItemsToBeShown = null;
        try {
            String[] listOfFiles = LogFilesUtils.getListOfLogs();

            listOfItemsToBeShown = new String[(null != listOfFiles ? listOfFiles.length : 0) + 1];

            if (null != listOfFiles) {
                for (int i = 0; i < listOfFiles.length; i++) {
                    listOfItemsToBeShown[i + 1] = listOfFiles[i];
                }
            }

            listOfItemsToBeShown[currentLogPosition] = getString(R.string.landt_current_log);
        } catch (Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, " ", th);
            }
        }

        return listOfItemsToBeShown;
    }


    @Override
    public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
        try {
            if (null != view) {
                openContextMenu(view);
            }
        } catch (java.lang.Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, " onItemClick ", th);
            }
        }
    }

}
