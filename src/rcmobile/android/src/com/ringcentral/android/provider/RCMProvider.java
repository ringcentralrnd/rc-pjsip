/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.provider.RCMDataStore.RCMColumns;

public class RCMProvider extends ContentProvider {

    private static final boolean DEBUG = false;

    static final boolean DEBUG_ENBL = LogSettings.ENGINEERING && DEBUG;

    static final String TAG = "[RC]RCMProvider";

    /* URI authority string */
    public static final String AUTHORITY = "com.ringcentral.android.provider.rcmprovider";

    /* URI paths names */
    public static final String MAILBOX_CURRENT = "mailbox_current";
    public static final String DEVICE_ATTRIB = "device_attrib";
    public static final String USER_CREDENTIALS = "user_credentials";
    public static final String SERVICE_INFO = "service_info";
    public static final String ACCOUNT_INFO = "account_info";
    public static final String CALLER_IDS = "caller_ids";
    public static final String FW_NUMBERS = "fw_numbers";
    public static final String EXTENSIONS = "extensions";
    public static final String FAVORITES = "favorites";
    public static final String CALL_LOG = "call_log";
    public static final String MESSAGES = "messages";
    public static final String SYNC_STATUS = "sync_status";
    public static final String APP_CONFIG = "app_config";
    public static final String TSC_CONFIG = "tsc_config";

    public static final String TIER_SETTINGS = "tier_settings";        //account_info/tier_settings
    public static final String MAILBOX_STATE = "mailbox_state";        //account_info/mailbox_state
    public static final String EXT_MOD_COUNTER = "ext_mod_counter";    //account_info/ext_mod_counter
    public static final String MSG_MOD_COUNTER = "msg_mod_counter";    //account_info/msg_mod_counter

//    public static final String CALLER_IDS_REQUEST_SYNC = "caller_ids_request_sync";         //sync_status/caller_ids_request_sync
//    public static final String CALLER_IDS_SYNC_STATUS = "caller_ids_sync_status";           //sync_status/caller_ids_sync_status
//    public static final String CALLER_IDS_LAST_SYNC = "caller_ids_last_sync";               //sync_status/caller_ids_last_sync
//
//    public static final String FW_NUMBERS_REQUEST_SYNC = "fw_numbers_request_sync";         //sync_status/fw_numbers_request_sync
//    public static final String FW_NUMBERS_SYNC_STATUS  = "fw_numbers_sync_status";          //sync_status/fw_numbers_sync_status
//    public static final String FW_NUMBERS_LAST_SYNC = "fw_numbers_last_sync";               //sync_status/fw_numbers_last_sync
//
//    public static final String CALL_LOG_REQUEST_SYNC = "call_log_request_sync";             //sync_status/call_log_request_sync
//    public static final String CALL_LOG_SYNC_STATUS = "call_log_sync_status";               //sync_status/call_log_sync_status
//    public static final String CALL_LOG_LAST_SYNC = "call_log_last_sync";                   //sync_status/call_log_last_sync
//
//    public static final String CALL_LOG_REQUEST_MORE = "call_log_request_more";             //sync_status/call_log_request_more
//    public static final String CALL_LOG_MORE_STATUS = "call_log_more_status";               //sync_status/call_log_more_status
//
//    public static final String CALL_LOG_BREAK_FLAG_ALL = "call_log_break_flag_all";         //sync_status/call_log_break_flag_all
//    public static final String CALL_LOG_BREAK_FLAG_MISSED = "call_log_break_flag_missed";   //sync_status/call_log_break_flag_missed
//
//    public static final String MSG_LIST_REQUEST_SYNC = "msg_list_request_sync";             //sync_status/msg_list_request_sync
//    public static final String MSG_LIST_SYNC_STATUS = "msg_list_sync_status";               //sync_status/msg_list_sync_status
//    public static final String MSG_LIST_LAST_SYNC = "msg_list_last_sync";                   //sync_status/msg_list_last_sync

    /**
     * sync_status URI
     */
    public static final String LOGIN_STATUS = "login_status";
    public static final String LOGIN_LASTSYNC = "login_lastsync";
    public static final String LOGIN_ERROR = "login_error";

    public static final String ACCOUNT_INFO_STATUS = "account_info_status";
    public static final String ACCOUNT_INFO_LASTSYNC = "account_info_lastsync";
    public static final String ACCOUNT_INFO_ERROR = "account_info_error";

    public static final String ALL_CALL_LOGS_STATUS = "all_call_logs_status";
    public static final String ALL_CALL_LOGS_LASTSYNC = "all_call_logs_lastsync";
    public static final String ALL_CALL_LOGS_ERROR = "all_call_logs_error";

    public static final String MISSES_CALL_LOGS_STATUS = "misses_call_logs_status";
    public static final String MISSES_CALL_LOGS_LASTSYNC = "misses_call_logs_lastsync";
    public static final String MISSES_CALL_LOGS_ERROR = "misses_call_logs_error";

    
    private RCMDbHelper dbHelper;

    
    @Override
    public boolean onCreate() {
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "RCMProvider.onCreate()");
        }

        dbHelper = new RCMDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "query(" + uri + ",...)");
        }
        int match = sUriMatcher.match(uri);
        if (match == UriMatcher.NO_MATCH) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "query(): Wrong URI");
            }
            throw new IllegalArgumentException("Wrong URI: " + uri);
        }

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        int where_append_count = 0;

        if (mailboxIdRequired(match)) {
            String mailbox_id_string = uri.getQueryParameter(RCMColumns.MAILBOX_ID);
            if (mailbox_id_string == null) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "query(): Wrong URI: no mailboxID: " + uri);
                }
                throw new IllegalArgumentException("Wrong URI: no mailboxID: " + uri);
            }

            long mailbox_id;
            try {
                mailbox_id = new Long(mailbox_id_string);
            } catch (NumberFormatException e) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "query(): Wrong mailboxID: " + mailbox_id_string, e);
                }
                
                throw new IllegalArgumentException("Wrong mailboxID: " + mailbox_id_string);
            }

            long current_mailbox_id = RCMProviderHelper.getCurrentMailboxId(getContext());
            if (mailbox_id != current_mailbox_id) {
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "query(): mailboxID mis-match: " + mailbox_id + ". Current mailboxID: " + current_mailbox_id);
                }
                return null;
            }
            qb.appendWhere((where_append_count++ == 0 ? "" : " AND ") + (RCMColumns.MAILBOX_ID + '=' + mailbox_id));
        }

        qb.setTables(tableName(match));
        if (uriWithID(match)) {
            qb.appendWhere((where_append_count++ == 0 ? "" : " AND ") + (BaseColumns._ID + "=" + uri.getLastPathSegment()));
        }

        if (projection == null) {
            projection = defaultProjection(match);
        }

        if (TextUtils.isEmpty(sortOrder)) {
            sortOrder = RCMColumns.DEFAULT_SORT_ORDER;
        }

        SQLiteDatabase db;
        try {
            db = dbHelper.getReadableDatabase();
        } catch (SQLiteException e) {
            //TODO Implement proper error handling
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "query(): Error opening readable database", e);
            }
            
            throw e;
        }

        Cursor cursor;
        synchronized (this) {
            try {
                cursor = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
            } catch (Throwable e) {
                if (LogSettings.MARKET) {
                    EngLog.e(TAG, "query(): Exception at db query", e);
                }
                
                throw new RuntimeException("Exception at db query: " + e.getMessage());
            }
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        if (cursor == null) {
            if (RCMProvider.DEBUG_ENBL) {
                EngLog.d(TAG, "query(): null cursor returned from db query");
            }
        }

        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "query(): Cursor has " + cursor.getCount() + " rows");
        }
        return cursor;
    }


    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "insert(" + uri + ", ...)");
        }

        int match = sUriMatcher.match(uri);

        if (match == UriMatcher.NO_MATCH) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "insert(): Wrong URI: " + uri);
            }
            throw new IllegalArgumentException("Wrong URI: " + uri);
        }

//        if (uriWithID(match) || uriDerived(match)) {
        if (uriWithID(match)) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "insert(): Insert not allowed for this URI: " + uri);
            }
            throw new IllegalArgumentException("Insert not allowed for this URI: " + uri);
        }


        //MailboxID parameter does not make sense for insert operations and shall not be used
        //So it throws exception on ENGINEERING and QA builds
        //and is ignored on MARKET builds
        if (LogSettings.QA) {
            if (uri.getQueryParameter(RCMColumns.MAILBOX_ID) != null) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "insert(): Insert not allowed for this URI: " + uri);
                }
                throw new IllegalArgumentException("Insert not allowed for this URI: " + uri);
            }
        }
        
        SQLiteDatabase db;
        long rowId;

        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            // TODO Implement proper error handling
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "insert(): Error opening writeable database", e);
            }
            
            throw e;
        }

        synchronized (this) {
            try {
                rowId = db.insert(tableName(match), null, values);
            } catch (SQLException e) {
                // TODO Implement proper error handling
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "Insert() failed", e);
                }
                
                throw e;
            }
        }

        if (rowId <= 0) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "insert(): Error: insert() returned " + rowId);
            }
            throw new RuntimeException("DB insert failed");
        }

        uri = ContentUris.withAppendedId(UriHelper.removeQuery(uri), rowId);
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "insert(): new uri with rowId: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);

        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "insert(): return " + uri);
        }
        return uri;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "bulkInsert(" + uri + ", ...)");
        }
        int match = sUriMatcher.match(uri);

        if (match == UriMatcher.NO_MATCH) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "bulkInsert(): Wrong URI: " + uri);
            }
            throw new IllegalArgumentException("Wrong URI: " + uri);
        }

//        if (uriWithID(match) || uriDerived(match)) {
        if (uriWithID(match)) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "bulkInsert(): Insert not allowed for this URI: " + uri);
            }
            throw new IllegalArgumentException("Insert not allowed for this URI: " + uri);
        }

        //MailboxID parameter does not make sense for insert operations and shall not be used
        //So it throws exception on ENGINEERING and QA builds
        //and is ignored on MARKET builds
        if (LogSettings.MARKET) {
            if (uri.getQueryParameter(RCMColumns.MAILBOX_ID) != null) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "bulkInsert(): Insert not allowed for this URI: " + uri);
                }
                throw new IllegalArgumentException("Insert not allowed for this URI: " + uri);
            }
        }

        
        SQLiteDatabase db;
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            // TODO Implement proper error handling
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "bulkInsert(): Error opening writable database", e);
            }
            
            throw e;
        }

        int added = 0;
        long rowId = 0;
        String table = tableName(match);

        synchronized (this) {
            try {
                db.beginTransaction();
                for (int i = 0; i < values.length; i++) {
                    try {
                        rowId = db.insert(table, null, values[i]);
                    } catch (SQLException e) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "bulkInsert() P1", e);
                        }
                        continue;
                    }

                    if (rowId <= 0) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "bulkInsert() P2: " + rowId);
                        }
                        continue;
                    }
                    added = added + 1;
                }
                db.setTransactionSuccessful();
            } catch (SQLException e) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "bulkInsert() P3", e);
                }
                
                throw new RuntimeException("bulkInsert(): DB insert failed: " + e.getMessage());
            } finally {
                db.endTransaction();
            }
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return added;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "update(" + uri + ", ...)");
        }
        int match = sUriMatcher.match(uri);
        if (match == UriMatcher.NO_MATCH) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "update(): Wrong URI: " + uri);
            }
            throw new IllegalArgumentException("Wrong URI: " + uri);
        }

        if (uriWithID(match)) {
            selection = BaseColumns._ID + "=" + uri.getLastPathSegment() + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
        }

        String mailbox_id_string = uri.getQueryParameter(RCMColumns.MAILBOX_ID);
        if (!TextUtils.isEmpty(mailbox_id_string)) {
            selection = RCMColumns.MAILBOX_ID + "=" + mailbox_id_string + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
        }
        

        SQLiteDatabase db;
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            //TODO Implement proper error handling
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "update(): Error opening writable database", e);
            }
            
            throw e;
        }

        int count;
        synchronized (this) {
            try {
                count = db.update(tableName(match), values, selection, selectionArgs);
            } catch (SQLException e) {
                //TODO Implement proper error handling
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "update() failed", e);
                }
                
                throw e;
            }
        }

        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "update(): " + count + " rows updated");
        }

        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "delete(" + uri + ", ...)");
        }
        int match = sUriMatcher.match(uri);
        if (match == UriMatcher.NO_MATCH) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "delete(): Wrong URI: " + uri);
            }
            throw new IllegalArgumentException("Wrong URI: " + uri);
        }

        if (uriDerived(match)) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "delete(): Row delete not allowed for this URI: " + uri);
            }
            throw new IllegalArgumentException("Row delete not allowed for this URI: " + uri);
        }

        if (uriWithID(match)) {
            selection = BaseColumns._ID + "=" + uri.getLastPathSegment() + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
        }

        String mailbox_id_string = uri.getQueryParameter(RCMColumns.MAILBOX_ID);
        if (!TextUtils.isEmpty(mailbox_id_string)) {
            selection = RCMColumns.MAILBOX_ID + "=" + mailbox_id_string + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
        }
        
        SQLiteDatabase db;
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            //TODO Implement proper error handling
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "delete(): Error opening writable database", e);
            }
            
            throw e;
        }

        int count;
        synchronized (this) {
            try {
                count = db.delete(tableName(match), selection, selectionArgs);
            } catch (SQLException e) {
                //TODO Implement proper error handling
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "delete(): DB rows delete error", e);
                }
                
                throw e;
            }
        }

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "delete(): " + count + " rows deleted");

        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }


    @Override
    public String getType(Uri uri) {
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "getType(" + uri + ')');
        }

        return null;
    }


    private boolean uriWithID(int uri_match) {
        switch (uri_match) {
            case CALLER_IDS_ID_MATCH:
            case FW_NUMBERS_ID_MATCH:
            case EXTENSIONS_ID_MATCH:
            case FAVORITES_ID_MATCH:
            case CALL_LOG_ID_MATCH:
            case MESSAGES_ID_MATCH:
                return true;

            default:
                return false;
        }
    }

    private boolean uriDerived(int uri_match) {
        switch (uri_match) {

            case TIER_SETTINGS_MATCH:
            case MAILBOX_STATE_MATCH:
            case EXT_MOD_COUNTER_MATCH:
            case MSG_MOD_COUNTER_MATCH:
            case LOGIN_STATUS_MATCH:
            case LOGIN_LASTSYNC_MATCH:
            case LOGIN_ERROR_MATCH:
            case ACCOUNT_INFO_STATUS_MATCH:
            case ACCOUNT_INFO_LASTSYNC_MATCH:
            case ACCOUNT_INFO_ERROR_MATCH:
            case ALL_CALL_LOGS_STATUS_MATCH:
            case ALL_CALL_LOGS_LASTSYNC_MATCH:
            case ALL_CALL_LOGS_ERROR_MATCH:
            case MISSES_CALL_LOGS_STATUS_MATCH:
            case MISSES_CALL_LOGS_LASTSYNC_MATCH:
            case MISSES_CALL_LOGS_ERROR_MATCH:
//        case CALLER_IDS_REQUEST_SYNC_MATCH:
//        case CALLER_IDS_SYNC_STATUS_MATCH:
//        case CALLER_IDS_LAST_SYNC_MATCH:
//        case FW_NUMBERS_REQUEST_SYNC_MATCH:
//        case FW_NUMBERS_SYNC_STATUS_MATCH:
//        case FW_NUMBERS_LAST_SYNC_MATCH:
//        case CALL_LOG_REQUEST_SYNC_MATCH:
//        case CALL_LOG_SYNC_STATUS_MATCH:
//        case CALL_LOG_LAST_SYNC_MATCH:
//        case CALL_LOG_REQUEST_MORE_MATCH:
//        case CALL_LOG_MORE_STATUS_MATCH:
//        case CALL_LOG_BREAK_FLAG_ALL_MATCH:
//        case CALL_LOG_BREAK_FLAG_MISSED_MATCH:
//        case MSG_LIST_REQUEST_SYNC_MATCH:
//        case MSG_LIST_SYNC_STATUS_MATCH:
//        case MSG_LIST_LAST_SYNC_MATCH:
                return true;

            default:
                return false;
        }
    }

    private boolean mailboxIdRequired(int uri_match) {
        switch (uri_match) {
            case MAILBOX_CURRENT_MATCH:
            case DEVICE_ATTRIB_MATCH:
            case USER_CREDENTIALS_MATCH:
            case SYNC_STATUS_MATCH:
            case APP_CONFIG_MATCH:
            case TSC_CONFIG_MATCH:
                return false;
            default:
                return true;
        }
    }

    private String tableName(int uri_match) {
        switch (uri_match) {

            case MAILBOX_CURRENT_MATCH:
                return RCMDataStore.MailboxCurrentTable.getInstance().getName();

            case DEVICE_ATTRIB_MATCH:
                return RCMDataStore.DeviceAttribTable.getInstance().getName();

            case USER_CREDENTIALS_MATCH:
                return RCMDataStore.UserCredentialsTable.getInstance().getName();

            case SERVICE_INFO_MATCH:
                return RCMDataStore.ServiceInfoTable.getInstance().getName();

            case ACCOUNT_INFO_MATCH:
            case TIER_SETTINGS_MATCH:
            case MAILBOX_STATE_MATCH:
            case EXT_MOD_COUNTER_MATCH:
            case MSG_MOD_COUNTER_MATCH:
                return RCMDataStore.AccountInfoTable.getInstance().getName();

            case CALLER_IDS_MATCH:
            case CALLER_IDS_ID_MATCH:
                return RCMDataStore.CallerIDsTable.getInstance().getName();

            case FW_NUMBERS_MATCH:
            case FW_NUMBERS_ID_MATCH:
                return RCMDataStore.FwNumbersTable.getInstance().getName();

            case EXTENSIONS_MATCH:
            case EXTENSIONS_ID_MATCH:
                return RCMDataStore.ExtensionsTable.getInstance().getName();

            case FAVORITES_MATCH:
            case FAVORITES_ID_MATCH:
                return RCMDataStore.FavoritesTable.getInstance().getName();

            case CALL_LOG_MATCH:
            case CALL_LOG_ID_MATCH:
                return RCMDataStore.CallLogTable.getInstance().getName();

            case MESSAGES_MATCH:
            case MESSAGES_ID_MATCH:
                return RCMDataStore.MessagesTable.getInstance().getName();

            case SYNC_STATUS_MATCH:
            case LOGIN_STATUS_MATCH:
            case LOGIN_LASTSYNC_MATCH:
            case LOGIN_ERROR_MATCH:
            case ACCOUNT_INFO_STATUS_MATCH:
            case ACCOUNT_INFO_LASTSYNC_MATCH:
            case ACCOUNT_INFO_ERROR_MATCH:
            case ALL_CALL_LOGS_STATUS_MATCH:
            case ALL_CALL_LOGS_LASTSYNC_MATCH:
            case ALL_CALL_LOGS_ERROR_MATCH:
            case MISSES_CALL_LOGS_STATUS_MATCH:
            case MISSES_CALL_LOGS_LASTSYNC_MATCH:
            case MISSES_CALL_LOGS_ERROR_MATCH:
                return RCMDataStore.SyncStatusTable.getInstance().getName();
            case APP_CONFIG_MATCH:
            	return RCMDataStore.AppConfigTable.getInstance().getName();
            case TSC_CONFIG_MATCH:
            	return RCMDataStore.TSCConfigTable.getInstance().getName();


            default:
                throw new Error(TAG + " No table defined for #" + uri_match);
        }
    }


    private static final String[] TierSettingsProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.AccountInfoTable.JEDI_TIER_SETTINGS};
    private static final String[] ExtModCounterProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.AccountInfoTable.JEDI_EXT_MOD_COUNTER};
    private static final String[] MsgModCounterProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.AccountInfoTable.JEDI_MSG_MOD_COUNTER};
    private static final String[] MailboxStateProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.AccountInfoTable.RCM_MAILBOX_STATE};

    private static final String[] LoginStatusProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.LOGIN_STATUS};
    private static final String[] LoginLastSyncProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.LOGIN_LASTSYNC};
    private static final String[] LoginErrorProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.LOGIN_ERROR};
    private static final String[] AccountInfoStatusProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.ACCOUNT_INFO_STATUS};
    private static final String[] AccountInfoLastSyncProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.ACCOUNT_INFO_LASTSYNC};
    private static final String[] AccountInfoErrorProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.ACCOUNT_INFO_ERROR};
    private static final String[] AllCallLogsStatusProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.ALL_CALL_LOGS_STATUS};
    private static final String[] AllCallLogsLastSyncProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.ALL_CALL_LOGS_LASTSYNC};
    private static final String[] AllCallLogsErrorProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.ALL_CALL_LOGS_ERROR};
    private static final String[] MissesCallLogsStatusProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.MISSES_CALL_LOGS_STATUS};
    private static final String[] MissesCallLogsLastSyncProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.MISSES_CALL_LOGS_LASTSYNC};
    private static final String[] MissesCallLogsErrorProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.MISSES_CALL_LOGS_ERROR};

//    private static final String[] CallerIDsRequestSyncProjection   = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.CALLER_IDS_REQUEST_SYNC};
//    private static final String[] CallerIDsSyncStatusProjection    = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.CALLER_IDS_SYNC_STATUS};
//    private static final String[] CallerIDsLastSyncProjection      = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.CALLER_IDS_LAST_SYNC};
//    private static final String[] FwNumbersRequestSyncProjection   = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.FW_NUMBERS_REQUEST_SYNC};
//    private static final String[] FwNumbersSyncStatusProjection    = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.FW_NUMBERS_SYNC_STATUS};
//    private static final String[] FwNumbersLastSyncProjection      = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.FW_NUMBERS_LAST_SYNC};
//    private static final String[] CallLogRequestSyncProjection     = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.CALL_LOG_REQUEST_SYNC};
//    private static final String[] CallLogSyncStatusProjection      = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.CALL_LOG_SYNC_STATUS};
//    private static final String[] CallLogLastSyncProjection        = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.CALL_LOG_LAST_SYNC};
//    private static final String[] CallLogRequestMoreProjection     = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.CALL_LOG_REQUEST_MORE};
//    private static final String[] CallLogMoreStatusProjection      = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.CALL_LOG_MORE_STATUS};
//    private static final String[] CallLogBreakFlagAllProjection    = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.CALL_LOG_BREAK_FLAG_ALL};
//    private static final String[] CallLogBreakFlagMissedProjection = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.CALL_LOG_BREAK_FLAG_MISSED};
//    private static final String[] MsgListRequestSyncProjection     = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.MSG_LIST_REQUEST_SYNC};
//    private static final String[] MsgListSyncStatusProjection      = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.MSG_LIST_SYNC_STATUS};
//    private static final String[] MsgListLastSyncProjection        = {RCMDataStore.RCMColumns.MAILBOX_ID, RCMDataStore.SyncStatusTable.MSG_LIST_LAST_SYNC};

    private String[] defaultProjection(int uri_matcher) {
        switch (uri_matcher) {

            case TIER_SETTINGS_MATCH:
                return TierSettingsProjection;
            case MAILBOX_STATE_MATCH:
                return MailboxStateProjection;
            case EXT_MOD_COUNTER_MATCH:
                return ExtModCounterProjection;
            case MSG_MOD_COUNTER_MATCH:
                return MsgModCounterProjection;
            case LOGIN_STATUS_MATCH:
                return LoginStatusProjection;
            case LOGIN_LASTSYNC_MATCH:
                return LoginLastSyncProjection;
            case LOGIN_ERROR_MATCH:
                return LoginErrorProjection;
            case ACCOUNT_INFO_STATUS_MATCH:
                return AccountInfoStatusProjection;
            case ACCOUNT_INFO_LASTSYNC_MATCH:
                return AccountInfoLastSyncProjection;
            case ACCOUNT_INFO_ERROR_MATCH:
                return AccountInfoErrorProjection;
            case ALL_CALL_LOGS_STATUS_MATCH:
                return AllCallLogsStatusProjection;
            case ALL_CALL_LOGS_LASTSYNC_MATCH:
                return AllCallLogsLastSyncProjection;
            case ALL_CALL_LOGS_ERROR_MATCH:
                return AllCallLogsErrorProjection;
            case MISSES_CALL_LOGS_STATUS_MATCH:
                return MissesCallLogsStatusProjection;
            case MISSES_CALL_LOGS_LASTSYNC_MATCH:
                return MissesCallLogsLastSyncProjection;
            case MISSES_CALL_LOGS_ERROR_MATCH:
                return MissesCallLogsErrorProjection;
//        case CALLER_IDS_REQUEST_SYNC_MATCH:
//            return CallerIDsRequestSyncProjection;
//        case CALLER_IDS_SYNC_STATUS_MATCH:
//            return CallerIDsSyncStatusProjection;
//        case CALLER_IDS_LAST_SYNC_MATCH:
//            return CallerIDsLastSyncProjection;
//        case FW_NUMBERS_REQUEST_SYNC_MATCH:
//            return FwNumbersRequestSyncProjection;
//        case FW_NUMBERS_SYNC_STATUS_MATCH:
//            return FwNumbersSyncStatusProjection;
//        case FW_NUMBERS_LAST_SYNC_MATCH:
//            return FwNumbersLastSyncProjection;
//        case CALL_LOG_REQUEST_SYNC_MATCH:
//            return CallLogRequestSyncProjection;
//        case CALL_LOG_SYNC_STATUS_MATCH:
//            return CallLogSyncStatusProjection;
//        case CALL_LOG_LAST_SYNC_MATCH:
//            return CallLogLastSyncProjection;
//        case CALL_LOG_REQUEST_MORE_MATCH:
//            return CallLogRequestMoreProjection;
//        case CALL_LOG_MORE_STATUS_MATCH:
//            return CallLogMoreStatusProjection;
//        case CALL_LOG_BREAK_FLAG_ALL_MATCH:
//            return CallLogBreakFlagAllProjection;
//        case CALL_LOG_BREAK_FLAG_MISSED_MATCH:
//            return CallLogBreakFlagMissedProjection;
//        case MSG_LIST_REQUEST_SYNC_MATCH:
//            return MsgListRequestSyncProjection;
//        case MSG_LIST_SYNC_STATUS_MATCH:
//            return MsgListSyncStatusProjection;
//        case MSG_LIST_LAST_SYNC_MATCH:
//            return MsgListLastSyncProjection;

            default:
                return null;
        }
    }


    /* UriMatcher codes */
    private static final int MAILBOX_CURRENT_MATCH = 10;
    private static final int DEVICE_ATTRIB_MATCH = 11;
    private static final int SERVICE_INFO_MATCH = 12;
    private static final int ACCOUNT_INFO_MATCH = 20;
    //private static final int ACCOUNT_INFO_ID_MATCH = 21;
    private static final int CALLER_IDS_MATCH = 30;
    private static final int CALLER_IDS_ID_MATCH = 31;
    private static final int FW_NUMBERS_MATCH = 40;
    private static final int FW_NUMBERS_ID_MATCH = 41;
    private static final int EXTENSIONS_MATCH = 50;
    private static final int EXTENSIONS_ID_MATCH = 51;
    private static final int FAVORITES_MATCH = 55;
    private static final int FAVORITES_ID_MATCH = 56;
    private static final int CALL_LOG_MATCH = 60;
    private static final int CALL_LOG_ID_MATCH = 61;
    private static final int MESSAGES_MATCH = 70;
    private static final int MESSAGES_ID_MATCH = 71;
    private static final int SYNC_STATUS_MATCH = 80;
    private static final int TIER_SETTINGS_MATCH = 90;
    private static final int MAILBOX_STATE_MATCH = 100;
    private static final int EXT_MOD_COUNTER_MATCH = 110;
    private static final int MSG_MOD_COUNTER_MATCH = 120;
    
    public static final int LOGIN_STATUS_MATCH = 130;
    public static final int LOGIN_LASTSYNC_MATCH = 131;
    public static final int LOGIN_ERROR_MATCH = 132;
    public static final int ACCOUNT_INFO_STATUS_MATCH = 140;
    public static final int ACCOUNT_INFO_LASTSYNC_MATCH = 141;
    public static final int ACCOUNT_INFO_ERROR_MATCH = 142;
    public static final int ALL_CALL_LOGS_STATUS_MATCH = 150;
    public static final int ALL_CALL_LOGS_LASTSYNC_MATCH = 151;
    public static final int ALL_CALL_LOGS_ERROR_MATCH = 152;
    public static final int MISSES_CALL_LOGS_STATUS_MATCH = 160;
    public static final int MISSES_CALL_LOGS_LASTSYNC_MATCH = 161;
    public static final int MISSES_CALL_LOGS_ERROR_MATCH = 162;

//    private static final int CALLER_IDS_REQUEST_SYNC_MATCH = 130;
//    private static final int CALLER_IDS_SYNC_STATUS_MATCH = 131;
//    private static final int CALLER_IDS_LAST_SYNC_MATCH = 132;
//    private static final int FW_NUMBERS_REQUEST_SYNC_MATCH = 140;
//    private static final int FW_NUMBERS_SYNC_STATUS_MATCH = 141;
//    private static final int FW_NUMBERS_LAST_SYNC_MATCH = 142;
//    private static final int CALL_LOG_REQUEST_SYNC_MATCH = 150;
//    private static final int CALL_LOG_SYNC_STATUS_MATCH = 151;
//    private static final int CALL_LOG_LAST_SYNC_MATCH = 152;
//    private static final int CALL_LOG_REQUEST_MORE_MATCH = 153;
//    private static final int CALL_LOG_MORE_STATUS_MATCH = 154;
//    private static final int CALL_LOG_BREAK_FLAG_ALL_MATCH = 155;
//    private static final int CALL_LOG_BREAK_FLAG_MISSED_MATCH = 156;
//    private static final int MSG_LIST_REQUEST_SYNC_MATCH = 160;
//    private static final int MSG_LIST_SYNC_STATUS_MATCH = 161;
//    private static final int MSG_LIST_LAST_SYNC_MATCH = 162;

    private static final int USER_CREDENTIALS_MATCH = 500;
    private static final int APP_CONFIG_MATCH = 510;
    private static final int TSC_CONFIG_MATCH = 520;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(AUTHORITY, MAILBOX_CURRENT, MAILBOX_CURRENT_MATCH);
        sUriMatcher.addURI(AUTHORITY, DEVICE_ATTRIB, DEVICE_ATTRIB_MATCH);
        sUriMatcher.addURI(AUTHORITY, SERVICE_INFO, SERVICE_INFO_MATCH);
        sUriMatcher.addURI(AUTHORITY, USER_CREDENTIALS, USER_CREDENTIALS_MATCH);
        sUriMatcher.addURI(AUTHORITY, ACCOUNT_INFO, ACCOUNT_INFO_MATCH);
        //sUriMatcher.addURI(AUTHORITY, ACCOUNT_INFO+"/#", ACCOUNT_INFO_ID_MATCH);
        sUriMatcher.addURI(AUTHORITY, CALLER_IDS, CALLER_IDS_MATCH);
        sUriMatcher.addURI(AUTHORITY, CALLER_IDS + "/#", CALLER_IDS_ID_MATCH);
        sUriMatcher.addURI(AUTHORITY, FW_NUMBERS, FW_NUMBERS_MATCH);
        sUriMatcher.addURI(AUTHORITY, FW_NUMBERS + "/#", FW_NUMBERS_ID_MATCH);
        sUriMatcher.addURI(AUTHORITY, EXTENSIONS, EXTENSIONS_MATCH);
        sUriMatcher.addURI(AUTHORITY, EXTENSIONS + "/#", EXTENSIONS_ID_MATCH);
        sUriMatcher.addURI(AUTHORITY, FAVORITES, FAVORITES_MATCH);
        sUriMatcher.addURI(AUTHORITY, FAVORITES + "/#", FAVORITES_ID_MATCH);
        sUriMatcher.addURI(AUTHORITY, CALL_LOG, CALL_LOG_MATCH);
        sUriMatcher.addURI(AUTHORITY, CALL_LOG + "/#", CALL_LOG_ID_MATCH);
        sUriMatcher.addURI(AUTHORITY, MESSAGES, MESSAGES_MATCH);
        sUriMatcher.addURI(AUTHORITY, MESSAGES + "/#", MESSAGES_ID_MATCH);
        sUriMatcher.addURI(AUTHORITY, SYNC_STATUS, SYNC_STATUS_MATCH);
        sUriMatcher.addURI(AUTHORITY, APP_CONFIG, APP_CONFIG_MATCH);
        sUriMatcher.addURI(AUTHORITY, TSC_CONFIG, TSC_CONFIG_MATCH);
        sUriMatcher.addURI(AUTHORITY, TIER_SETTINGS, TIER_SETTINGS_MATCH);
        sUriMatcher.addURI(AUTHORITY, MAILBOX_STATE, MAILBOX_STATE_MATCH);
        sUriMatcher.addURI(AUTHORITY, EXT_MOD_COUNTER, EXT_MOD_COUNTER_MATCH);
        sUriMatcher.addURI(AUTHORITY, MSG_MOD_COUNTER, MSG_MOD_COUNTER_MATCH);

        sUriMatcher.addURI(AUTHORITY, LOGIN_STATUS, LOGIN_STATUS_MATCH);
        sUriMatcher.addURI(AUTHORITY, LOGIN_LASTSYNC, LOGIN_LASTSYNC_MATCH);
        sUriMatcher.addURI(AUTHORITY, LOGIN_ERROR, LOGIN_ERROR_MATCH);
        sUriMatcher.addURI(AUTHORITY, ACCOUNT_INFO_STATUS, ACCOUNT_INFO_STATUS_MATCH);
        sUriMatcher.addURI(AUTHORITY, ACCOUNT_INFO_LASTSYNC, ACCOUNT_INFO_LASTSYNC_MATCH);
        sUriMatcher.addURI(AUTHORITY, ACCOUNT_INFO_ERROR, ACCOUNT_INFO_ERROR_MATCH);
        sUriMatcher.addURI(AUTHORITY, ALL_CALL_LOGS_STATUS, ALL_CALL_LOGS_STATUS_MATCH);
        sUriMatcher.addURI(AUTHORITY, ALL_CALL_LOGS_LASTSYNC, ALL_CALL_LOGS_LASTSYNC_MATCH);
        sUriMatcher.addURI(AUTHORITY, ALL_CALL_LOGS_ERROR, ALL_CALL_LOGS_ERROR_MATCH);
        sUriMatcher.addURI(AUTHORITY, MISSES_CALL_LOGS_STATUS, MISSES_CALL_LOGS_STATUS_MATCH);
        sUriMatcher.addURI(AUTHORITY, MISSES_CALL_LOGS_LASTSYNC, MISSES_CALL_LOGS_LASTSYNC_MATCH);
        sUriMatcher.addURI(AUTHORITY, MISSES_CALL_LOGS_ERROR, MISSES_CALL_LOGS_ERROR_MATCH);

//        sUriMatcher.addURI(AUTHORITY, CALLER_IDS_REQUEST_SYNC, CALLER_IDS_REQUEST_SYNC_MATCH);
//        sUriMatcher.addURI(AUTHORITY, CALLER_IDS_SYNC_STATUS, CALLER_IDS_SYNC_STATUS_MATCH);
//        sUriMatcher.addURI(AUTHORITY, CALLER_IDS_LAST_SYNC, CALLER_IDS_LAST_SYNC_MATCH);
//        sUriMatcher.addURI(AUTHORITY, FW_NUMBERS_REQUEST_SYNC, FW_NUMBERS_REQUEST_SYNC_MATCH);
//        sUriMatcher.addURI(AUTHORITY, FW_NUMBERS_SYNC_STATUS, FW_NUMBERS_SYNC_STATUS_MATCH);
//        sUriMatcher.addURI(AUTHORITY, FW_NUMBERS_LAST_SYNC, FW_NUMBERS_LAST_SYNC_MATCH);
//        sUriMatcher.addURI(AUTHORITY, CALL_LOG_REQUEST_SYNC, CALL_LOG_REQUEST_SYNC_MATCH);
//        sUriMatcher.addURI(AUTHORITY, CALL_LOG_SYNC_STATUS, CALL_LOG_SYNC_STATUS_MATCH);
//        sUriMatcher.addURI(AUTHORITY, CALL_LOG_LAST_SYNC, CALL_LOG_LAST_SYNC_MATCH);
//        sUriMatcher.addURI(AUTHORITY, CALL_LOG_REQUEST_MORE, CALL_LOG_REQUEST_MORE_MATCH);
//        sUriMatcher.addURI(AUTHORITY, CALL_LOG_MORE_STATUS, CALL_LOG_MORE_STATUS_MATCH);
//        sUriMatcher.addURI(AUTHORITY, CALL_LOG_BREAK_FLAG_ALL, CALL_LOG_BREAK_FLAG_ALL_MATCH);
//        sUriMatcher.addURI(AUTHORITY, CALL_LOG_BREAK_FLAG_MISSED, CALL_LOG_BREAK_FLAG_MISSED_MATCH);
//        sUriMatcher.addURI(AUTHORITY, MSG_LIST_REQUEST_SYNC, MSG_LIST_REQUEST_SYNC_MATCH);
//        sUriMatcher.addURI(AUTHORITY, MSG_LIST_SYNC_STATUS, MSG_LIST_SYNC_STATUS_MATCH);
//        sUriMatcher.addURI(AUTHORITY, MSG_LIST_LAST_SYNC, MSG_LIST_LAST_SYNC_MATCH);
    }

}

