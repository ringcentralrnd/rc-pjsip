/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.provider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.rcbase.android.sip.service.CodecInfo;
import com.rcbase.android.sip.service.CodecInfoComparator;
import com.rcbase.android.sip.service.CodecInfo.Codec;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConfig;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.api.pojo.CallLogPOJO;
import com.ringcentral.android.api.pojo.MessagePOJO;
import com.ringcentral.android.provider.RCMDataStore.AccountInfoTable;
import com.ringcentral.android.provider.RCMDataStore.CallLogTable;
import com.ringcentral.android.provider.RCMDataStore.CallerIDsTable;
import com.ringcentral.android.provider.RCMDataStore.DeviceAttribTable;
import com.ringcentral.android.provider.RCMDataStore.ExtensionsTable;
import com.ringcentral.android.provider.RCMDataStore.MailboxCurrentTable;
import com.ringcentral.android.provider.RCMDataStore.MessagesTable;
import com.ringcentral.android.provider.RCMDataStore.RCMColumns;
import com.ringcentral.android.provider.RCMDataStore.SyncStatusEnum;
import com.ringcentral.android.provider.RCMDataStore.SyncStatusTable;
import com.ringcentral.android.provider.RCMDataStore.TSCConfigTable;

public class RCMProviderHelper {

    private static final String TAG = "[RC]RCMProviderHelper";


    public static void setCurrentMailboxId(Context context, long mailbox_id) {
    	long mailbox_id_current = getCurrentMailboxId(context);
        if (mailbox_id_current != mailbox_id) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "setCurrentMailboxId(): " + mailbox_id);
            }
            updateSingleValue(context, RCMProvider.MAILBOX_CURRENT, MailboxCurrentTable.MAILBOX_ID, String.valueOf(mailbox_id), null);
        }
    }

    public static long getCurrentMailboxId(Context context) {
        String result = simpleQuery(context, RCMProvider.MAILBOX_CURRENT, MailboxCurrentTable.MAILBOX_ID);
        long id = TextUtils.isEmpty(result) ? MailboxCurrentTable.MAILBOX_ID_NONE : Long.valueOf(result);
        return id;
    }

    public static void clearUserCredentialsTable(Context context) {
        if (LogSettings.MARKET) {
            QaLog.i(TAG, "clearUserCredentialsTable()");
        }
        clearTableAndInsertEmptyRow(context, RCMProvider.USER_CREDENTIALS);
    }
    
    public static void clearSyncStatusTable(Context context) {
        if (LogSettings.MARKET) {
            QaLog.i(TAG, "clearSyncStatusTable()");
        }
        clearTableAndInsertEmptyRow(context, RCMProvider.SYNC_STATUS);
    }
    
    public static String getAccessLevel(Context context) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getAccessLevel()...");

        String value = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_ACCESS_LEVEL);

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getAccessLevel(): return " + value);
        return value;
    }

    /**
     * Returns Account tier type (Tier Service type, e.g. "RCMobile", "RCOffice", "RCVoice", "RCFax"). Valid from 5.0.x
     * 
     * @param context the execution context
     * 
     * @return tier service type
     */
    public static String getTierServiceType(Context context) {
        String value = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_TIER_SERVICE_TYPE);
    
        if (RCMProvider.DEBUG_ENBL){
            EngLog.d(TAG, "getTierServiceType(): return " + value);
        }
        return value;
    }
    
    public static String getExtensionType(Context context) {
        if (RCMProvider.DEBUG_ENBL){
            EngLog.d(TAG, "getExtensionType()...");
        }

        String value = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_EXTENSION_TYPE);

        if (RCMProvider.DEBUG_ENBL){
            EngLog.d(TAG, "getExtensionType(): return " + value);
        }
        return value;
    }

    public static boolean isSystemExtension(Context context) {
        if (RCMProvider.DEBUG_ENBL){
            EngLog.d(TAG, "isSystemExtension()...");
        }        

        String result_str = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_SYSTEM_EXTENSION);
        boolean result = TextUtils.isEmpty(result_str) ? false : (Integer.valueOf(result_str) != 0); //false - default value in case of DB error

        if (RCMProvider.DEBUG_ENBL){
            EngLog.d(TAG, "isSystemExtension(): return " + result);
        }
        return result;
    }

    public static int getServiceVersion(Context context) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getServiceVersion()...");       
        
        int result = (int)simpleQueryWithMailboxIdLong(context,
                RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_SERVICE_VERSION, AccountInfoTable.SERVICE_VERSION_4);
    
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getServiceVersion(): return " + result);
        return result;
    }

    
    /*
     * Tier settings
     */
//    public static void saveTierSettings(Context context, long value) {
//        if (LogSettings.MARKET) {
//            MktLog.i(TAG, "saveTierSettings(): " + value);
//        }
//        updateSingleValueWithMailboxId(context, RCMProvider.TIER_SETTINGS, AccountInfoTable.JEDI_TIER_SETTINGS, String.valueOf(value));
//    }

    public static long getTierSettings(Context context) {
        return simpleQueryWithMailboxIdLong(context, RCMProvider.TIER_SETTINGS, AccountInfoTable.JEDI_TIER_SETTINGS, 0 );
    }

    public static String getAccountFree(Context context) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getAccountFree()...");

        String result = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_FREE);

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getAccountFree(): return " + result);
        return result;
    }

    /*
     * Extensions Modification Counter
     */
    public static void saveExtModCounter(Context context, long value) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveExtModCounter(): " + value);
        }
        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.EXT_MOD_COUNTER, AccountInfoTable.JEDI_EXT_MOD_COUNTER, String.valueOf(value));
    }

    public static long getExtModCounter(Context context) {
        return simpleQueryWithMailboxIdLong(context, RCMProvider.EXT_MOD_COUNTER, AccountInfoTable.JEDI_EXT_MOD_COUNTER, -1 );
    }

    public static long getExtModCounterTemp(Context context) {
        return simpleQueryWithMailboxIdLong(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_EXT_MOD_COUNTER_TEMP, -1 );
    }

    /*
     * Message Modification Counter 
     */
    public static void saveMsgModCounter(Context context, long value) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveMsgModCounter(): " + value);
        }
        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.MSG_MOD_COUNTER, AccountInfoTable.JEDI_MSG_MOD_COUNTER, String.valueOf(value));
    }

    public static long getMsgModCounter(Context context) {
        return simpleQueryWithMailboxIdLong(context, RCMProvider.MSG_MOD_COUNTER, AccountInfoTable.JEDI_MSG_MOD_COUNTER, -1 );
    }

    public static long getMsgModCounterTemp(Context context) {
        return simpleQueryWithMailboxIdLong(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_MSG_MOD_COUNTER_TEMP, -1 );
    }

    /*
     * Last Loaded Message ID 
     */
    public static void saveLastLoadedMsgId(Context context, long value) {
        if (LogSettings.MARKET) {
            EngLog.i(TAG, "saveLastLoadedMsgId(): " + value);
        }
        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_LAST_LOADED_MSG_ID, String.valueOf(value));
    }
    
    public static long getLastLoadedMsgId(Context context) {
        return simpleQueryWithMailboxIdLong(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_LAST_LOADED_MSG_ID, 0 );
    }
    
    /*
     * Last complete setup request
     */
    public static void saveLastCompleteSetupRequest(Context context, long value) {
        if (LogSettings.MARKET) {
            EngLog.i(TAG, "saveLastCompleteSetupRequest(): " + value);
        }
        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_LAST_COMPLETE_SETUP_REQ, String.valueOf(value));
    }
    
    public static long getLastCompleteSetupRequest(Context context) {    	
       return simpleQueryWithMailboxIdLong(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_LAST_COMPLETE_SETUP_REQ, 0 );
    }
    
    /*
     * Last expiration reminders
     */
    public static void saveLastExpirationReminder(Context context, long value) {
        if (LogSettings.MARKET) {
            EngLog.i(TAG, "saveLastExpirationReminder(): " + value);
        }
        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_LAST_EXPIRED_REQ, String.valueOf(value));
    }
    
    public static long getLastExpirationReminder(Context context) {    	
       return simpleQueryWithMailboxIdLong(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_LAST_EXPIRED_REQ, 0 );
    }
    
    /*
     * Custom Phone Number
     */
    public static void saveCustomPhoneNumber(Context context, String number) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveCustomPhoneNumber(): " + (number == null ? "null":number));
        }
        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_CUSTOM_NUMBER, number);
    }

    public static String getCustomPhoneNumber(Context context) {
        return simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_CUSTOM_NUMBER);
    }


    /*
     * Device IMSI
     */
    public static void saveDeviceIMSI(Context context, String IMSI) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveDeviceIMSI(): " + (IMSI == null ? "null":IMSI));
        }
        updateSingleValue(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_DEVICE_IMSI, IMSI, null);
    }

    public static String getDeviceIMSI(Context context) {
        return simpleQuery(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_DEVICE_IMSI);
    }
    
    public static boolean getDeviceEchoState(Context context) {
        String resultString = simpleQuery(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_DEVICE_ECHO_STATE);
        
        boolean result = TextUtils.isEmpty(resultString) ? false : (Integer.valueOf(resultString) != 0); //false - default value in case of DB error
        
        if(LogSettings.MARKET) {
            MktLog.i(TAG, "getDeviceEchoState " + result);
        }
        
        return result;
    }
    public static void saveDeviceEchoState(Context context, boolean value) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveDeviceEchoState(): " + value);
        }
        String valueString = String.valueOf(value ? "1" : "0");
        
        updateSingleValue(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_DEVICE_ECHO_STATE, valueString, null);
    }
    

    public static boolean isTosAccepted(Context context) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "isTosAccepted()...");

        String result_str = simpleQuery(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_TOS_ACCEPTED);

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "isTosAccepted(): result_str == " + result_str);

        boolean result = TextUtils.isEmpty(result_str) ? false : result_str.equals("1"); //false - default value in case of DB error

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "isTosAccepted(): return " + result);
        return result;
    }

    /**
     * 
     * @param context
     * @return 0 - user has never passed dialog
     *         1 - Accept
     *         2 - Decline
     */
    public static int getTos911State(Context context){
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "isTos911Accepted()...");

        String result_str = simpleQuery(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_TOS911_ACCEPTED);

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "isTos911Accepted(): result_str == " + result_str);

        int result = TextUtils.isEmpty(result_str) ? 0 : Integer.parseInt(result_str); //false - default value in case of DB error

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "isTos911Accepted(): return " + result);
        return result;
    }
    
    public static boolean isTos911Accepted(Context context){
    	return getTos911State(context) == 1;
    }
    
    public static boolean isTos911Executed(Context context){
    	return getTos911State(context) != 0;
    }
    
    public static boolean isVoipWhatsNewDialogShown(Context context){
        if (RCMProvider.DEBUG_ENBL) { 
            EngLog.d(TAG, "isVoipWhatsNewDialogShown()...");
        }

        String result_str = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_VOIP_WHATS_NEW);

        if (RCMProvider.DEBUG_ENBL) { 
            EngLog.d(TAG, "isVoipWhatsNewDialogShown(): result_str == " + result_str);
        }

        boolean result = TextUtils.isEmpty(result_str) ? false : result_str.equals("1"); //false - default value in case of DB error

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "isVoipWhatsNewDialogShown(): return " + result);
        return result;
    }
    public static int getDeviceInternalSpeakerDelay(Context context){
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getInternalSpeakerDelay()...");

        String result_str = simpleQuery(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_DEVICE_ECHO_VALUE_EARPIECE);

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getInternalSpeakerDelay(): result_str == " + result_str);

        int result = TextUtils.isEmpty(result_str) ? RCMConstants.DEFAULT_AEC_DELAY_VALUE : Integer.valueOf(result_str); //false - default value in case of DB error

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "isTos911Accepted(): return " + result);
        return result;
    }
    public static int getDeviceExternalSpeakerDelay(Context context){
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getExternalSpeakerDelay()...");

        String result_str = simpleQuery(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_DEVICE_ECHO_VALUE_SPEAKER);

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getExternalSpeakerDelay(): result_str == " + result_str);

        int result = TextUtils.isEmpty(result_str) ? RCMConstants.DEFAULT_AEC_DELAY_VALUE : Integer.valueOf(result_str);

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getExternalSpeakerDelay(): return " + result);
        return result;
    }
    public static boolean getDeviceAudioWizard(Context context){
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getDeviceAudioWizard()...");

        String result_str = simpleQuery(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_DEVICE_AUDIO_SETUP_WIZARD);

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getDeviceAudioWizard(): result_str == " + result_str);

        boolean result = TextUtils.isEmpty(result_str) ? false : result_str.equals("1");

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getDeviceAudioWizard(): return " + result);
        
        return result;
    }
    /*
     * Caller ID
     */
    public static void saveCallerID(Context context, String id) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveCallerID(): " + (id == null ? "null":id));
        }
        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_CALLER_ID, id);
    }

    public static String getCallerID(Context context) {
        String id = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_CALLER_ID);
        if (TextUtils.isEmpty(id)) {
            if (RCMProvider.DEBUG_ENBL)
                EngLog.d(TAG, "getCallerID(): Empty CallerID; use Main Number");

            id = getMainNumber(context);
        }
        
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getCallerID(): return " + id);
        return id;
    }

    /**
     * Get the "MainNumber" Caller ID
     *
     * @param context
     * @return saved value or empty string
     */
    public static String getMainNumber(Context context) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getMainNumber()...");

        String number = simpleSelectionQueryWithMailboxId(
                context,
                RCMProvider.CALLER_IDS,
                CallerIDsTable.JEDI_NUMBER,
                CallerIDsTable.JEDI_USAGE_TYPE + " LIKE '" + CallerIDsTable.USAGE_TYPE_MAIN_NUMBER + "'");

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getMainNumber(): return " + number);
        return number;
    }
    
    /**
     * Defines if <code>number</code> exists in current CallerIds
     * 
     * @param context
     *            the execution context
     * @param number
     *            the number to be search
     * @return <code>true</code> if the <code>number</code> exists in the
     *         CallerIds, otherwise <code>false</code>
     * 
     */
    public static boolean isNumberInCallerIds(Context context, String number) {
        String found = simpleSelectionQueryWithMailboxId(
                context,
                RCMProvider.CALLER_IDS,
                CallerIDsTable.JEDI_NUMBER,
                CallerIDsTable.JEDI_NUMBER + " LIKE '" + number + "'");
        if (TextUtils.isEmpty(found)) {
            return false;
        }
        return true;
    }
    
    
    /**
     * Get account number from ACCOUNT_INFO
     *
     * @param context
     * @return saved value or empty string
     */
    public static String getAccountNumber(Context context) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getAccountNumber()...");

        String number = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_ACCOUNT_NUMBER);

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getAccountNumber(): return " + number);
        return number;
    }

    public static String getAccountPin(Context context) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getAccountPin()...");

        String pin = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_PIN);

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getAccountPin(): return " + pin);
        return pin;
    }

    /**
     * Save login number
     *
     * @param context
     * @param loginNumber
     */
    public static void saveLoginNumber(Context context, String loginNumber) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveLoginNumber(): " + (loginNumber == null ? "null":loginNumber));
        }
        updateSingleValue(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.RCM_LOGIN_NUMBER, loginNumber, null);
    }

    /**
     * Get login number
     *
     * @param context
     * @return saved value or empty string
     */
    public static String getLoginNumber(Context context) {
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "getLoginNumber()...");
        }

        String loginNumber = simpleQuery(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.RCM_LOGIN_NUMBER);

        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "getLoginNumber(): return " + loginNumber);
        }
        return loginNumber;
    }

    /**
     * Save login ext
     *
     * @param context
     * @param ext
     */
    public static void saveLoginExt(Context context, String ext) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveLoginExt(): " + (ext == null ? "null":ext));
        }

        updateSingleValue(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.RCM_LOGIN_EXT, ext, null);
    }

    /**
     * Get login ext
     *
     * @param context
     * @return saved value or empty string
     */
    public static String getLoginExt(Context context) {
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "getLoginExt()...");
        }

        String ext = simpleQuery(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.RCM_LOGIN_EXT);

        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "getLoginExt(): return " + ext);
        }
        return ext;
    }

    /**
     * Save login password
     *
     * @param context
     * @param password
     */
    public static void saveLoginPassword(Context context, String password) {
        updateSingleValue(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.RCM_PASSWORD, password, null);
    }

    /**
     * Get login password
     *
     * @param context
     * @return saved value or empty string
     */
    public static String getLoginPassword(Context context) {
        String password = simpleQuery(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.RCM_PASSWORD);
        return password;
    }

    /**
     * Save login IP address
     *
     * @param context
     * @param address
     */
    public static void saveLoginIPAddress(Context context, String address) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveLoginIPAddress(): " + (address == null ? "null":address));
        }

        updateSingleValue(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.JEDI_LOGIN_IP_ADDRESS, address, null);
    }

    /**
     * Get login IP address
     *
     * @param context
     * @return saved value or empty string
     */
    public static String getLoginIPAddress(Context context) {
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "getLoginIPAddress()...");
        }

        String address = simpleQuery(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.JEDI_LOGIN_IP_ADDRESS);

        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "getLoginIPAddress(): return " + address);
        }
        return address;
    }

    /**
     * Save login request ID
     *
     * @param context
     * @param id
     */
    public static void saveLoginRequestID(Context context, String id) {
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "saveLoginRequestID(" + id + ")...");
        }

        updateSingleValue(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.JEDI_LOGIN_REQUEST_ID, id, null);
    }

    /**
     * Get login request ID
     *
     * @param context
     * @return saved value or empty string
     */
    public static String getLoginRequestID(Context context) {
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "getLoginIPAddress()...");
        }

//        String id = simpleQuery(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.RCM_LOGIN_IP_ADDRESS);
        String id = simpleQuery(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.JEDI_LOGIN_REQUEST_ID);

        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "getLoginRequestID(): return " + id);
        }
        return id;
    }

    /**
     * Save login start time
     *
     * @param context
     * @param startTime
     */
    public static void saveLoginStartTime(Context context, String startTime) {
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "saveLoginStartTime(" + startTime + ")...");
        }

        updateSingleValue(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.JEDI_LOGIN_START_TIME, startTime, null);
    }

    /**
     * Get login start time
     *
     * @param context
     * @return saved value or empty string
     */
    public static String getLoginStartTime(Context context) {
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "getLoginStartTime()...");
        }

        String time = simpleQuery(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.JEDI_LOGIN_START_TIME);

        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "getLoginStartTime(): return " + time);
        }
        return time;
    }
    
    /**
     * Save login hash
     *
     * @param context
     * @param hash
     */
    public static void saveLoginHash(Context context, String hash) {
        if (LogSettings.MARKET) {
            QaLog.i(TAG, "saveLoginHash(): " + (hash == null ? "null":hash));
        }

        updateSingleValue(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.JEDI_LOGIN_HASH, hash, null);
    }

    /**
     * Get login hash
     *
     * @param context
     * @return saved value or empty string
     */
    public static String getLoginHash(Context context) {
        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "getLoginHash()...");
        }

        String hash = simpleQuery(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.JEDI_LOGIN_HASH);

        if (RCMProvider.DEBUG_ENBL) {
            EngLog.d(TAG, "getLoginHash(): return " + hash);
        }
        return hash;
    }

    /**
     * Save login cookies
     *
     * @param context
     * @param cookie
     */
    public static void saveLoginCookie(Context context, String cookie) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveLoginIPAddress(): " + (cookie == null ? "null":cookie));
        }

        updateSingleValue(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.JEDI_LOGIN_COOKIE, cookie, null);
    }

    /**
     * Get login cookies
     *
     * @param context
     * @return saved value or empty string
     */
    public static String getLoginCookie(Context context) {
    	if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getLoginCookie()...");

        String cookie = simpleQuery(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.JEDI_LOGIN_COOKIE);

    	if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getLoginCookie(): return " + cookie);

    	return cookie;
    }

    /**
     * Save service API version
     * 
     * @param context
     * @param value
     */
    public static void saveServiceApiVersion(Context context, String value) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveApiVersion(): " + (value == null ? "null":value));
        }

        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.SERVICE_INFO, RCMDataStore.ServiceInfoTable.JEDI_API_VERSION, value);
    }

    /**
     * DO NOT USE FOR UI ELEMENTS HIDING, PLEASE !!1
     * DO NOT MIX-UP WITH RCMProviderHelper.getServiceVersion !!1
     * 
     * Get service API version
     * used for voip status 
     *
     * @param context
     * @return saved value or empty string
     */
    public static String getServiceApiVersion(Context context) {
        if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getServiceApiVersion()...");

        String value = simpleQueryWithMailboxId(context, RCMProvider.SERVICE_INFO, RCMDataStore.ServiceInfoTable.JEDI_API_VERSION);

        if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getServiceApiVersion(): return " + value);

        return value;
    }
    
    
    /**
     * Save userId
     *
     * @param context
     * @param userId
     */
    public static void saveUserId(Context context, String userId) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveUserId(): " + (userId == null ? "null":userId));
        }
        updateSingleValue(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.JEDI_USER_ID, userId, null);
    }

    /**
     * Get userId received from Login request 
     *
     * @param context
     * @return saved value or empty string
     */
    public static String getUserId(Context context) {
    	if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getUserId()...");

        String userId = simpleQuery(context, RCMProvider.USER_CREDENTIALS, RCMDataStore.UserCredentialsTable.JEDI_USER_ID);

    	if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getUserId(): return " + userId);
    	
        return userId;
    }


    /**
     * Get userId received from AccountInfo request
     * Normally shall be the same as returned by getUserId()
     *
     * @param context
     * @return saved value or empty string
     */
    public static String getAccountInfoUserId(Context context) {
    	if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getAccountInfoUserId()...");

        String userId = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, RCMDataStore.AccountInfoTable.JEDI_USER_ID);

    	if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getAccountInfoUserId(): return " + userId);

    	return userId;
    }

    public static boolean isAgent(Context context) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "isAgent()...");

        String result_str = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_AGENT);

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "isAgent(): result_str == " + result_str);

        boolean result = TextUtils.isEmpty(result_str) ? false : (Integer.valueOf(result_str) == 1); //false - default value in case of DB error

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "isAgent(): return " + result);
        return result;
    }

    public static String getAgentStatus(Context context) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getAgentStatus()...");

        String status = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_AGENT_STATUS);

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getAgentStatus(): return " + status);
        return status;
    }
    
    public static void saveRingoutAnotherPhone(Context context, String number) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveRingoutAnotherPhone(): " + (number == null ? "null":number));
        }

        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_RINGOUT_ANOTHER_PHONE, number);
    }

    public static String getRingoutAnotherPhone(Context context) {
    	if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getRingoutAnotherPhone()...");

        String number = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_RINGOUT_ANOTHER_PHONE);

    	if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getRingoutAnotherPhone(): return " + number);
        return number;
    }

    public static void saveLastCallNumber(Context context, String number) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveLastCallNumber(): " + (number == null ? "null":number));
        }

        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_LAST_CALL_NUMBER, number);
    }

    public static String getLastCallNumber(Context context) {
    	if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getLastCallNumber()...");

        String number = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_LAST_CALL_NUMBER);

    	if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getLastCallNumber(): return " + number);
        return number;
    }

    public static void setConfirmConnection(Context context, boolean isConfirm) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "setConfirmConnection(): " + isConfirm);
        }

        String value = isConfirm ? "1" : "0";
        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_CONFIRM_CONNECTION, value);
    }

    public static boolean isConfirmConnection(Context context) {
    	if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "isConfirmConnection()...");

        String result_str = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_CONFIRM_CONNECTION);

    	if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "isConfirmConnection(): result_str == " + result_str);

        boolean result = TextUtils.isEmpty(result_str) ? true : (Integer.valueOf(result_str) != 0); //true - default value in case of DB error

    	if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getTierSettings(): return " + result);
        return result;
    }

    public static void setRingoutMode(Context context, int ringoutMode) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "setRingoutMode(): " + ringoutMode);
        }

        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_RINGOUT_MODE, String.valueOf(ringoutMode));
    }

    public static int getRingoutMode(Context context) {
    	if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getRingoutMode()...");

        String mode = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_RINGOUT_MODE);

        int value = TextUtils.isEmpty(mode) ? AccountInfoTable.RINGOUT_MODE_MY_ANDROID : Integer.valueOf(mode);      //MY_ANDDROID - default in case of DB error

    	if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getRingoutMode(): return " + value);
        return value;
    }

    public static String getSetupWizardState(Context context) {
        if (RCMProvider.DEBUG_ENBL){
            EngLog.d(TAG, "getSetupWizardState()...");
        }

        String state = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_SETUP_WIZARD_STATE);

        if (RCMProvider.DEBUG_ENBL){
            EngLog.d(TAG, "getSetupWizardState(): return " + state);
        }
        return state;
    }
    
    public static String getTrialState(Context context) {
        if (RCMProvider.DEBUG_ENBL){
            EngLog.d(TAG, "getTrialState()...");
        }

        String state = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_TRIAL_EXPIRATION_STATE);

        if (RCMProvider.DEBUG_ENBL){
            EngLog.d(TAG, "getTrialState(): return " + state);
        }
        return state;
    }
    
    public static int getTrialDaysLeft(Context context) {
        if (RCMProvider.DEBUG_ENBL){
            EngLog.d(TAG, "getTrialDaysLeft()...");
        }               
        
        int result = (int)simpleQueryWithMailboxIdLong(context,
                RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_DAYS_TO_EXPIRE, -1);
    
        if (RCMProvider.DEBUG_ENBL){
            EngLog.d(TAG, "getServiceVersion(): return " + result);
        }
        return result;
    }
    
    public static String getBrandId(Context context) {
        if (RCMProvider.DEBUG_ENBL){
            EngLog.d(TAG, "getBrandId()...");
        }

        String state = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_BRAND_ID);

        if (RCMProvider.DEBUG_ENBL){
            EngLog.d(TAG, "getBrandId(): return " + state);
        }
        return state;
    }

    /**************************************************************************************************************************************
     * VoIP related section.
     ***************************************************************************************************************************************/

    /************************************
     * VoIP getters
     ************************************/
    
    /**
     * Check VoIP settings
     */
    private static boolean isVoipSettingEnabled(Context context, long flag, long[] flag_add ) {
        String result_str = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_VOIP_SETTINGS);

    	if (LogSettings.ENGINEERING)
        	EngLog.d(TAG, "isVoipSettingEnabled(): VoIP settings:" + result_str );

        boolean result = TextUtils.isEmpty(result_str) ?
                (( RCMConfig.VOIP_DEFAULT_SETTINGS & flag) != 0 ) :
                	((Long.valueOf(result_str) & flag ) != 0);
       
        if( result && flag_add != null ){
        	
        	for( int i = 0; i < flag_add.length; i++ ){
        		
        		if( flag_add[i] > 0 ){
        			
                	result = result & TextUtils.isEmpty(result_str) ?
                            (( RCMConfig.VOIP_DEFAULT_SETTINGS & flag_add[i]) != 0 ) :
                            	((Long.valueOf(result_str) & flag_add[i] ) != 0);
        		}
        		
        		if( !result )
        			break;
        	}
        }
    	
        if (LogSettings.ENGINEERING)
           	EngLog.d(TAG, "isVoipSettingEnabled(): Function:" + flag + " Result:" + result );
        
        return result;        
    }
    
    /**
     * Get VoIP settings
     */
    public static long getVoipSettings(Context context) {

        String result_str = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_VOIP_SETTINGS);

    	if (LogSettings.ENGINEERING)
        	EngLog.d(TAG, "isVoipSettingEnabled(): VoIP settings:" + result_str );

        final long result = TextUtils.isEmpty(result_str) ?
                RCMConfig.VOIP_DEFAULT_SETTINGS : (Long.valueOf(result_str) );
       
        if (LogSettings.ENGINEERING)
           	EngLog.d(TAG, "getVoipSettings(): Result:" + result );
        
        return result;        
    }
    
    /**
     * Defines if VoIP enabled:
     * - On Service (Environment, Account, SIP Flag (HTTP-REG) and ToS
     * - User locally switched on
     */
    public static boolean isVoipEnabled_Env_Acc_SipFlagHttpReg_ToS_UserVoIP(Context context) {
        if (BUILD.isTos911Enabled){ 
            if (!isTos911Accepted( context )) {
                return false;
            }
        }
        final long[] flags = new long[3];
        flags[0] = RCMConstants.SIP_SETTINGS_USER_VOIP_ENABLED;
        flags[1] = RCMConstants.SIP_SETTINGS_ON_ENVIRONMENT_ENABLED;
        flags[2] = RCMConstants.SIP_SETTINGS_ON_ACCOUNT_ENABLED;
        return isVoipSettingEnabled( context, RCMConstants.SIP_SETTINGS_SIP_FLAG_HTTPREG_ENABLED, flags );
    }
    
    /**
     * Defines if VoIP enabled:
     * - On Service (Environment, Account, SIP Flag (HTTP-REG))
     */
    public static boolean isVoipEnabled_Env_Acc_SipFlagHttpReg( Context context ){
        final long[] flags = new long[2];
        flags[0] = RCMConstants.SIP_SETTINGS_ON_ENVIRONMENT_ENABLED;
        flags[1] = RCMConstants.SIP_SETTINGS_ON_ACCOUNT_ENABLED;
        
        return isVoipSettingEnabled( context, RCMConstants.SIP_SETTINGS_SIP_FLAG_HTTPREG_ENABLED,   flags );
    }

    /**
     * Defines if VoIP enabled:
     * - On Service (Environment, Account)
     */
    public static boolean isVoipEnabled_Env_Acc( Context context ){
        final long[] flags = new long[1];
        flags[0] = RCMConstants.SIP_SETTINGS_ON_ENVIRONMENT_ENABLED;
        return isVoipSettingEnabled( context, RCMConstants.SIP_SETTINGS_ON_ACCOUNT_ENABLED,   flags );
    }
    
    /**
     * Defines if VoIP enabled:
     * - On Service (Environment, Account, ToS)
     */
    public static boolean isVoipEnabled_Env_Acc_ToS( Context context ){
        if (BUILD.isTos911Enabled){ 
            if (!isTos911Accepted( context )) {
                return false;
            }
        }
        final long[] flags = new long[1];
        flags[0] = RCMConstants.SIP_SETTINGS_ON_ENVIRONMENT_ENABLED;
        return isVoipSettingEnabled( context, RCMConstants.SIP_SETTINGS_ON_ACCOUNT_ENABLED,   flags );
    }
    
    /**
     * Get VOIP enabled for this account on RC service SIP flag (HTTP-REG)
     */
    public static boolean isVoipEnabled_SipFlagHttpReg(Context context) {
        return isVoipSettingEnabled( context, RCMConstants.SIP_SETTINGS_SIP_FLAG_HTTPREG_ENABLED, null );
    }
    
    /**
     * Get VOIP enabled for account environment.
     */
    public static boolean isVoipEnabled_Env(Context context) {
        return isVoipSettingEnabled( context, RCMConstants.SIP_SETTINGS_ON_ENVIRONMENT_ENABLED, null );
    }
    
    /**
     * Get VOIP enabled for account.
     */
    public static boolean isVoipEnabled_Acc(Context context) {
        return isVoipSettingEnabled( context, RCMConstants.SIP_SETTINGS_ON_ACCOUNT_ENABLED, null );
    }
    
    
    /**
     * Get VOIP switched on locally flag in Application Settings
     */
    public static boolean isVoipSwitchedOn_UserVoip(Context context) {
        return isVoipSettingEnabled( context, RCMConstants.SIP_SETTINGS_USER_VOIP_ENABLED, null );
    }
    
    /**
     * Get VOIP over Wi-Fi switched on flag in Application Settings
     */
    public static boolean isVoipSwitchedOn_UserWiFi(Context context) {
        return isVoipSettingEnabled( context, RCMConstants.SIP_SETTINGS_USER_WIFI_ENABLED, null );
    }

    /**
     * Get VOIP over 3G/4G switched on flag in Application Settings
     */
    public static boolean isVoipSwitchedOn_User3g4g(Context context) {
        return isVoipSettingEnabled( context, RCMConstants.SIP_SETTINGS_USER_3G_4G_ENABLED, null );
    }

    /**
     * Get VOIP Incoming calls switched on flag in Application Settings
     */
    public static boolean isVoipSwitchedOn_UserIncomingCalls(Context context) {
        return isVoipSettingEnabled( context, RCMConstants.SIP_SETTINGS_USER_INCOMING_CALLS_ENABLED, null );
    }

    
    /*******************************************************
     * VoiP setters
     *******************************************************/
    
    
    /**
     * Set VoIP settings
     */
    private static void setVoipSettings(Context context, boolean enabled, long function) {
    	
        String result_str = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_VOIP_SETTINGS);
        if (TextUtils.isEmpty(result_str)){
            EngLog.d(TAG, "setVoipSettings(): query returns empty string" );
            return;
        }

    	if (LogSettings.ENGINEERING)
        	EngLog.d(TAG, "setVoipSettings(): VoIP settings:" + result_str + " Function:" + function + " Value:" + enabled );

        long settings = Long.valueOf(result_str);
        
        settings = ( enabled ? 
        		(settings | function ) : 
        			(settings & ~function ) );
        
        String value = Long.toString(settings);
        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_VOIP_SETTINGS, value);
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "setVoipSettings: " + voipGetCurrentVoipSettingsStatesTrace(context));
        }
    }
    
    /**
     * Set VOIP locally enabled flag
     */
    public static void setVoipSwitchedOn_UserVoip(Context context, boolean enabled) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "setVoipSwitchedOn_UserVoip(" + enabled + ")...");
        }
        
        setVoipSettings( context, enabled, RCMConstants.SIP_SETTINGS_USER_VOIP_ENABLED );
    }
    /**
     * Set VOIP over Wi-Fi flag
     */
    public static void setVoipSwitchedOn_UserWifi(Context context, boolean enabled) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "setVoipSwitchedOn_UserWifi(" + enabled + ")...");
        }
        
        setVoipSettings( context, enabled, RCMConstants.SIP_SETTINGS_USER_WIFI_ENABLED );
    }

    /**
     * Set VOIP over 3G/4G flag
     */
    public static void setVoipSwitchedOn_User3g4g(Context context, boolean enabled) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "setVoipSwitchedOn_User3g4g(" + enabled + ")...");
        }

        setVoipSettings( context, enabled, RCMConstants.SIP_SETTINGS_USER_3G_4G_ENABLED );
    }

    /**
     * Set VOIP Incoming calls flag
     */
    public static void setVoipSwitchedOn_UserIncomingCalls(Context context, boolean enabled) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "setVoipSwitchedOn_UserIncomingCalls(" + enabled + ")...");
        }

        setVoipSettings( context, enabled, RCMConstants.SIP_SETTINGS_USER_INCOMING_CALLS_ENABLED );
    }
    

    /**
     * Set VOIP on environment enabled flag and notify (if required) if changed
     */
    public static void setVoipEnabled_Env(Context context, boolean enabled, boolean notify) {
        if (enabled == isVoipEnabled_Env(context)) {
            return;
        }
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "setVoipEnabled_Env(" + enabled + ") notify(" + notify + ")");
        }
        
        setVoipSettings(context, enabled, RCMConstants.SIP_SETTINGS_ON_ENVIRONMENT_ENABLED);
        
        if (notify) {
            voipSettingsChangedNotification(context, RCMConstants.SIP_SETTINGS_ON_ENVIRONMENT_ENABLED);
        }
    }
    
    /**
     * Set VOIP for account enabled flag (AccountInfo.ServiceMask
     * TIERS_PHS_DIAL_FROM_CLIENT, later can be brand/tier) and notify (if required) if changed.
     */
    public static void setVoipEnabled_Acc(Context context, boolean enabled, boolean notify) {
        if (enabled == isVoipEnabled_Acc(context)) {
            return;
        }
        if (LogSettings.MARKET) {
           MktLog.w(TAG, "setVoipEnabled_Acc(" + enabled + ") notify(" + notify + ")");
        }
        
        setVoipSettings(context, enabled, RCMConstants.SIP_SETTINGS_ON_ACCOUNT_ENABLED);
        
        if (notify) {
            voipSettingsChangedNotification(context, RCMConstants.SIP_SETTINGS_ON_ACCOUNT_ENABLED);
        }
    }

    /**
     * Set VOIP on environment enabled flag and VOIP for account enabled flag.
     * Then notfy if required.
     * 
     * @param context
     *            the execution context
     * @param envEnabled
     *            environment flag
     * @param accEnabled
     *            account flag
     * @param notify
     *            defines if notification required
     */
    public static void setVoipEnabled_Env_Acc(Context context, boolean envEnabled, boolean accEnabled, boolean notify) {
        boolean envChanged = (envEnabled != isVoipEnabled_Env(context));
        boolean accChanged = (accEnabled != isVoipEnabled_Acc(context));
        if (!envChanged && !accChanged) {
            return;
        }
        
        if (LogSettings.MARKET) {
           MktLog.i(TAG, "setVoipEnabled_Env_Acc: Env(" + envEnabled + ") Acc(" + accEnabled + ") notify(" + notify +")");
        }

        long changes = 0;
        if (envChanged) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "setVoipEnabled_Env_Acc: Env flag changed (" + envEnabled + ")");
            }
            setVoipSettings(context, envEnabled, RCMConstants.SIP_SETTINGS_ON_ENVIRONMENT_ENABLED);
            changes |= RCMConstants.SIP_SETTINGS_ON_ENVIRONMENT_ENABLED;
        }
        
        if (accChanged) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "setVoipEnabled_Env_Acc: Acc flag changed (" + accEnabled + ")");
            }
            setVoipSettings(context, accEnabled, RCMConstants.SIP_SETTINGS_ON_ACCOUNT_ENABLED);
            changes |= RCMConstants.SIP_SETTINGS_ON_ACCOUNT_ENABLED;
        }
        
        if (notify) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "setVoipEnabled_Env_Acc: Env/Acc flags changed notification sent");
            }
            voipSettingsChangedNotification(context, changes);
        }
    }
    
    
    /**
     * Set VOIP SIP Flag (HttpReg) enabled flag
     */
    public static void setVoipEnabled_SipFlagHttpReg(Context context, boolean enabled) {
    	if (LogSettings.MARKET) {
    	    QaLog.i(TAG, "setVoipEnabled_SipFlagHttpReg(" + enabled + ")...");
    	}
        
        setVoipSettings( context, enabled, RCMConstants.SIP_SETTINGS_SIP_FLAG_HTTPREG_ENABLED );
    }

    /**
     * VoIP helpers
     */
    
    /**
     * Returns user-friendly string for current VoIP settings traces
     * 
     * @param context
     *            the execution context
     * @return the current settings string
     */
    public static final String voipGetCurrentVoipSettingsStatesTrace(Context context) {
        long currentSettings = RCMConfig.VOIP_DEFAULT_SETTINGS;
    
        try {
            String result_str = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_VOIP_SETTINGS);
            if (!TextUtils.isEmpty(result_str)) {
                currentSettings = Long.valueOf(result_str);
            }
        } catch (java.lang.Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "voipGetCurrentVoipSettingsStatesTrace error: " + th.toString());
            }
        }
            
        StringBuffer sb = new StringBuffer("CurrentVoIPSettings:");
        sb.append(" Env");
        sb.append(getFlagStateAsString(currentSettings, RCMConstants.SIP_SETTINGS_ON_ENVIRONMENT_ENABLED));
        
        sb.append(" Acc");
        sb.append(getFlagStateAsString(currentSettings, RCMConstants.SIP_SETTINGS_ON_ACCOUNT_ENABLED));
        
        sb.append(" SipFlagHttpReg");
        sb.append(getFlagStateAsString(currentSettings, RCMConstants.SIP_SETTINGS_SIP_FLAG_HTTPREG_ENABLED));
        
        sb.append(" UserVoIP");
        sb.append(getFlagStateAsString(currentSettings, RCMConstants.SIP_SETTINGS_USER_VOIP_ENABLED));
        
        sb.append(" UserWiFi");
        sb.append(getFlagStateAsString(currentSettings, RCMConstants.SIP_SETTINGS_USER_WIFI_ENABLED));
        
        sb.append(" User3g4g");
        sb.append(getFlagStateAsString(currentSettings, RCMConstants.SIP_SETTINGS_USER_3G_4G_ENABLED));

        sb.append(" UserIncoming");
        sb.append(getFlagStateAsString(currentSettings, RCMConstants.SIP_SETTINGS_USER_INCOMING_CALLS_ENABLED));

        return sb.toString();
    }
    private static final String getFlagStateAsString(long flags, long flagMask) {
        if ((flags & flagMask) !=0 ) {
            return "(1)";
        } else {
            return "(0)";
        }
    }
    
    /**
     * 
     * Sends notification about VoIP settings (flags) changing
     * 
     * @param context
     *            the execution context
     * @param voipConfigurationStateChanged
     *            VoIP settings changes
     */
    public static void voipSettingsChangedNotification(Context context, long voipConfigurationStateChanged) {
        if (LogSettings.MARKET) {
            StringBuffer sb = new StringBuffer("ChangedVoIPFlags:");

            if ((voipConfigurationStateChanged & RCMConstants.SIP_SETTINGS_ON_ENVIRONMENT_ENABLED) != 0) {
                sb.append(" Env");
            }

            if ((voipConfigurationStateChanged & RCMConstants.SIP_SETTINGS_ON_ACCOUNT_ENABLED) != 0) {
                sb.append(" Acc");
            }

            if ((voipConfigurationStateChanged & RCMConstants.SIP_SETTINGS_SIP_FLAG_HTTPREG_ENABLED) != 0) {
                sb.append(" SipFlagHttpReg");
            }

            if ((voipConfigurationStateChanged & RCMConstants.SIP_SETTINGS_USER_VOIP_ENABLED) != 0) {
                sb.append(" UserVoIP");
            }

            if ((voipConfigurationStateChanged & RCMConstants.SIP_SETTINGS_USER_WIFI_ENABLED) != 0) {
                sb.append(" UserWiFi");
            }

            if ((voipConfigurationStateChanged & RCMConstants.SIP_SETTINGS_USER_3G_4G_ENABLED) != 0) {
                sb.append(" User3g4g");
            }

            if ((voipConfigurationStateChanged & RCMConstants.SIP_SETTINGS_USER_INCOMING_CALLS_ENABLED) != 0) {
                sb.append(" UserIncoming");
            }
            
            MktLog.i(TAG, "voipSettingsChangedNotification(" + voipConfigurationStateChanged + ": " + sb.toString() + "; " + voipGetCurrentVoipSettingsStatesTrace(context));
        }
        try {
            Intent iVoipConfigurationChanged = new Intent(RCMConstants.ACTION_VOIP_CONFIGURATION_CHANGED);
            iVoipConfigurationChanged.putExtra(RCMConstants.EXTRA_VOIP_CONFIGURATION_STATE_CHANGED, voipConfigurationStateChanged);
            context.sendBroadcast(iVoipConfigurationChanged);
        } catch (java.lang.Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "voipSettingsChangedNotification error: " + th.toString());
            }
        }
    }
    

    /**************************************************************************************************************************************
     * End VoIP related section.
     ***************************************************************************************************************************************/

    
    
    
    
    /*
     * Get/Set Message SYNC interval
     */
    public static void setSyncInterval(Context context, int interval) {
    	if (LogSettings.ENGINEERING)
        	EngLog.d(TAG, "setSyncInterval Interval:" + interval );
        
        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_MESSAGE_CHECK_INTERVAL, String.valueOf(interval));
    }

    public static int getSyncInterval(Context context) {
        return simpleQueryWithMailboxIdInt(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_MESSAGE_CHECK_INTERVAL, RCMConstants.SERVICE_INTERVAL );
    }    
    
    /*
     * Get/Set SIP Provider URI
     */
    public synchronized static String getUriSipProvider(Context context){
    	return simpleQueryWithMailboxId( context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_URI_SIP_PROVIDER );
    }
    
    public synchronized static void setUriSipProvider( Context context, final String uri ){
        if (LogSettings.ENGINEERING)
        	EngLog.d(TAG, "setUriSipProvider URI:" + (uri == null ? "null" : uri) );
    	
    	if( uri == null || uri.trim().length() == 0 ){
    		return;
    	}
    	
    	updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_URI_SIP_PROVIDER, uri.trim() );
    }

    /*
     * Get/Set SIP Outbound Proxy URI
     */
    public synchronized static String getUriSipOutboundProxy(Context context){
    	return simpleQueryWithMailboxId( context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_URI_SIP_OUTBOUND_PROXY );
    }
    
    public synchronized static void setUriSipOutboundProxy( Context context, final String uri ){
        if (LogSettings.ENGINEERING)
        	EngLog.d(TAG, "setUriSipOutboundProxy URI:" + (uri == null ? "null" : uri) );

        if( uri == null || uri.trim().length() == 0 ){
    		return;
    	}
    	
    	updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_URI_SIP_OUTBOUND_PROXY, uri.trim() );
    }
    
    
    /**
     * Defines projection for retrieving wide-band codec.
     */
    private static final String[] GET_WIDEBAND_CODEC_PROJECTION =  {
        DeviceAttribTable.RCM_WIDEBAND_ILBC_CODEC_PRIO,
        DeviceAttribTable.RCM_WIDEBAND_PCMU_CODEC_PRIO,
        DeviceAttribTable.RCM_WIDEBAND_PCMA_CODEC_PRIO,
        DeviceAttribTable.RCM_WIDEBAND_GSM_CODEC_PRIO,
        DeviceAttribTable.RCM_WIDEBAND_SILK_CODEC_PRIO,
        /*DeviceAttribTable.RCM_WIDEBAND_SILKNB_CODEC_PRIO,
        DeviceAttribTable.RCM_WIDEBAND_SILKMB_CODEC_PRIO,
        DeviceAttribTable.RCM_WIDEBAND_SILKWB_CODEC_PRIO,
        DeviceAttribTable.RCM_WIDEBAND_SILKUWB_CODEC_PRIO,*/
        DeviceAttribTable.RCM_WIDEBAND_G722_CODEC_PRIO
    }; 
    
    /**
     * Defines projection for retrieving narrow-band codec.
     */
    private static final String[] GET_NARROWBAND_CODEC_PROJECTION =  {
        DeviceAttribTable.RCM_NARROWBAND_ILBC_CODEC_PRIO,
        DeviceAttribTable.RCM_NARROWBAND_PCMU_CODEC_PRIO,
        DeviceAttribTable.RCM_NARROWBAND_PCMA_CODEC_PRIO,
        DeviceAttribTable.RCM_NARROWBAND_GSM_CODEC_PRIO,
        DeviceAttribTable.RCM_NARROWBAND_SILK_CODEC_PRIO,
        /*DeviceAttribTable.RCM_NARROWBAND_SILKNB_CODEC_PRIO,
        DeviceAttribTable.RCM_NARROWBAND_SILKMB_CODEC_PRIO,
        DeviceAttribTable.RCM_NARROWBAND_SILKWB_CODEC_PRIO,
        DeviceAttribTable.RCM_NARROWBAND_SILKUWB_CODEC_PRIO,*/
        DeviceAttribTable.RCM_NARROWBAND_G722_CODEC_PRIO,
    };
    
    private static final int GET_CODEC_ILBC_CODEC_IDX = 0;
    private static final int GET_CODEC_PCMU_CODEC_IDX = 1;
    private static final int GET_CODEC_PCMA_CODEC_IDX = 2;
    private static final int GET_CODEC_GSM_CODEC_IDX  = 3;
    private static final int GET_CODEC_SILK_CODEC_IDX  = 4;
    /*private static final int GET_CODEC_SILKNB_CODEC_IDX  = 4;
    private static final int GET_CODEC_SILKMB_CODEC_IDX  = 5;
    private static final int GET_CODEC_SILKWB_CODEC_IDX  = 6;
    private static final int GET_CODEC_SILKUWB_CODEC_IDX  = 7;*/
    private static final int GET_CODEC_G722_CODEC_IDX  = 5;
    
    
    /**
     * Returns codec for wide-band.
     * 
     * @param ctx
     *            the execution context
     * @param wideBand
     *            defines if wide-band codec shall be retrieved, otherwise
     *            narrow-band
     * 
     * @return codec for wide-band usage
     */
    public static CodecInfo[] getCodec(Context ctx, boolean wideBand) {
        if (BUILD.VOIP_ENABLED) {
            CodecInfo[] codec = new CodecInfo[6];
            String[] projection;
            if (wideBand) {
                projection = GET_WIDEBAND_CODEC_PROJECTION;
            } else {
                projection = GET_NARROWBAND_CODEC_PROJECTION;
            }
            Cursor cursor = null;
            boolean success = false;
            try {
                cursor = ctx.getContentResolver().query(UriHelper.getUri(RCMProvider.DEVICE_ATTRIB), 
                        projection, null, null, null);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        codec[0] = new CodecInfo(Codec.ILBC, cursor.getInt(GET_CODEC_ILBC_CODEC_IDX));
                        codec[1] = new CodecInfo(Codec.PCMU, cursor.getInt(GET_CODEC_PCMU_CODEC_IDX));
                        codec[2] = new CodecInfo(Codec.PCMA, cursor.getInt(GET_CODEC_PCMA_CODEC_IDX));
                        codec[3] = new CodecInfo(Codec.GSM , cursor.getInt(GET_CODEC_GSM_CODEC_IDX));
                        codec[4] = new CodecInfo(Codec.SILK , cursor.getInt(GET_CODEC_SILK_CODEC_IDX));
                        /*codec[4] = new CodecInfo(Codec.SILKNB , cursor.getInt(GET_CODEC_SILKNB_CODEC_IDX));
                        codec[5] = new CodecInfo(Codec.SILKMB , cursor.getInt(GET_CODEC_SILKMB_CODEC_IDX));
                        codec[6] = new CodecInfo(Codec.SILKWB , cursor.getInt(GET_CODEC_SILKWB_CODEC_IDX));
                        codec[7] = new CodecInfo(Codec.SILKUWB , cursor.getInt(GET_CODEC_SILKUWB_CODEC_IDX));*/
                        codec[5] = new CodecInfo(Codec.G722 , cursor.getInt(GET_CODEC_G722_CODEC_IDX));
                        
                        success = true;
                    }
                }
            } catch (java.lang.Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "getCodec() failed: " + th.getMessage());
                }
            }

            if (cursor != null) {
                cursor.close();
            }

            if (success) {
                Arrays.sort(codec, new CodecInfoComparator());
            } else {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "getCodec() failed: return defaults");
                }
                if (wideBand) {
                    codec = Codec.getDefaultsForWideBand();
                } else {
                    codec = Codec.getDefaultsForNarrowBand();
                }
            }
            
            return codec;
        } else {
            if (wideBand) {
                return Codec.getDefaultsForWideBand();
            } else {
                return Codec.getDefaultsForNarrowBand();
            } 
        }
    }
    
    /**
     * Sets codec priority for a codec.
     * 
     * @param context
     *            the execution context
     * @param codec
     *            the codec
     * @param wideBand
     *            defines if wide-band codec shall be set, otherwise narrow-band
     */
    public static void setPriorityForCodec(Context context, CodecInfo codecInfo, boolean wideBand) {
        if (BUILD.VOIP_ENABLED) {
            if (codecInfo == null || codecInfo.getCodec() == null) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "setPriorityForCodec:null");
                }
                return;
            }
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "setPriorityForCodec codec:" + codecInfo.getCodec().getName() + ", prio:" + codecInfo.getPriority() + " for "
                        + (wideBand ? "wide-band" : "narrow-band"));
            }

            String column = null;
            Codec codec = codecInfo.getCodec();
            if (codec == Codec.ILBC) {
                if (wideBand) {
                    column = DeviceAttribTable.RCM_WIDEBAND_ILBC_CODEC_PRIO;
                } else {
                    column = DeviceAttribTable.RCM_NARROWBAND_ILBC_CODEC_PRIO;
                }
            } else if (codec == Codec.GSM) {
                if (wideBand) {
                    column = DeviceAttribTable.RCM_WIDEBAND_GSM_CODEC_PRIO;
                } else {
                    column = DeviceAttribTable.RCM_NARROWBAND_GSM_CODEC_PRIO;
                }
            }
            else if (codec == Codec.SILK) {
                if (wideBand) {
                    column = DeviceAttribTable.RCM_WIDEBAND_SILK_CODEC_PRIO;
                } else {
                    column = DeviceAttribTable.RCM_NARROWBAND_SILK_CODEC_PRIO;
                }
            } 
            /*else if (codec == Codec.SILKNB) {
                if (wideBand) {
                    column = DeviceAttribTable.RCM_WIDEBAND_SILKNB_CODEC_PRIO;
                } else {
                    column = DeviceAttribTable.RCM_NARROWBAND_SILKNB_CODEC_PRIO;
                }
            } 
            else if (codec == Codec.SILKMB) {
                if (wideBand) {
                    column = DeviceAttribTable.RCM_WIDEBAND_SILKMB_CODEC_PRIO;
                } else {
                    column = DeviceAttribTable.RCM_NARROWBAND_SILKMB_CODEC_PRIO;
                }
            } 
            else if (codec == Codec.SILKWB) {
                if (wideBand) {
                    column = DeviceAttribTable.RCM_WIDEBAND_SILKWB_CODEC_PRIO;
                } else {
                    column = DeviceAttribTable.RCM_NARROWBAND_SILKWB_CODEC_PRIO;
                }
            } 
            else if (codec == Codec.SILKUWB) {
                if (wideBand) {
                    column = DeviceAttribTable.RCM_WIDEBAND_SILKUWB_CODEC_PRIO;
                } else {
                    column = DeviceAttribTable.RCM_NARROWBAND_SILKUWB_CODEC_PRIO;
                }
            } */
            else if (codec == Codec.PCMU) {
                if (wideBand) {
                    column = DeviceAttribTable.RCM_WIDEBAND_PCMU_CODEC_PRIO;
                } else {
                    column = DeviceAttribTable.RCM_NARROWBAND_PCMU_CODEC_PRIO;
                }
            } else if (codec == Codec.PCMA) {
                if (wideBand) {
                    column = DeviceAttribTable.RCM_WIDEBAND_PCMA_CODEC_PRIO;
                } else {
                    column = DeviceAttribTable.RCM_NARROWBAND_PCMA_CODEC_PRIO;
                }
     
            }         else if (codec == Codec.G722) {
                if (wideBand) {
                    column = DeviceAttribTable.RCM_WIDEBAND_G722_CODEC_PRIO;
                } else {
                    column = DeviceAttribTable.RCM_NARROWBAND_G722_CODEC_PRIO;
                } }
            else {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "setPriorityForCodec : no codec to be set");
                }
                return;
            }

            updateSingleValue(context, RCMProvider.DEVICE_ATTRIB, column, String.valueOf(codecInfo
                    .getPriority()), null);
        }
    }
    
    /**
     * Returns if EDGE shall be included into 3g/4G VoIP cases
     * 
     * @param context the execution context
     * 
     * @return if EDGE shall be included into 3g/4G VoIP cases
     */
    public static boolean isNeedToIncludeEDGEintoMobVoIP(Context context) {
        String resultString = simpleQuery(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_INCLUDE_EDGE_INTO_3G4G_VOIP);
        return TextUtils.isEmpty(resultString) ? false : (Integer.valueOf(resultString) != 0); //false - default value in case of DB error
    }
    
    /**
     * Defines if EDGE shall be included into 3g/4G VoIP cases
     * 
     * @param context
     *            the execution context
     * @param value
     *            <code>true</code> shall be included, otherwise
     *            <code>false</code>
     */
    public static void includeEDGEintoMobVoIP(Context context, boolean value) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "includeEDGEintoMobVoIP(): " + value);
        }
        updateSingleValue(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_INCLUDE_EDGE_INTO_3G4G_VOIP, String.valueOf(value ? "1" : "0"),
                null);
    }
    
    /**
     * Returns if CPU lock shall be used for active SIP stack.
     * 
     * @param context the execution context
     * 
     * @return if CPU lock shall be used for active SIP stack.
     */
    public static boolean isAlwaysCPULockForSIPstack(Context context) {
        String resultString = simpleQuery(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_ALWAYS_CPU_PARTAIL_LOCK_WHEN_STACK_IS_ACTIVE);
        return TextUtils.isEmpty(resultString) ? false : (Integer.valueOf(resultString) != 0); //false - default value in case of DB error
    }
    
    /**
     * Defines if CPU lock shall be used for active SIP stack.
     * 
     * @param context
     *            the execution context
     * @param value
     *            <code>true</code> shall be locked, otherwise
     *            <code>false</code>
     */
    public static void setAlwaysCPULockForSIPstack(Context context, boolean value) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "setAlwaysCPULockForSIPstack(): " + value);
        }
        updateSingleValue(context, RCMProvider.DEVICE_ATTRIB, 
                DeviceAttribTable.RCM_ALWAYS_CPU_PARTAIL_LOCK_WHEN_STACK_IS_ACTIVE, 
                String.valueOf(value ? "1" : "0"),
                null);
    }
    
    /**
     * Get list of logs with given type
     *
     * @param context
     * @param type    logs type (All/Missed)
     * @return
     */
    public static List<CallLogPOJO> getCallLogs(Context context, long mailboxId, int type) {
        ContentResolver resolver = context.getContentResolver();

        String[] projection = new String[]{
                CallLogTable._ID,
                CallLogTable.MAILBOX_ID,
                CallLogTable.RCM_FOLDER,
                CallLogTable.JEDI_LENGTH,
                CallLogTable.JEDI_CALL_DIRECTION,
                CallLogTable.JEDI_CALL_TYPE,
                CallLogTable.JEDI_FROM_NAME,
                CallLogTable.JEDI_FROM_PHONE,
                CallLogTable.JEDI_LOCATION,
                CallLogTable.JEDI_MAILBOX_ID,
                CallLogTable.JEDI_PIN,
                CallLogTable.JEDI_RECORD_ID,
                CallLogTable.JEDI_START,
                CallLogTable.JEDI_STATUS,
                CallLogTable.JEDI_TO_NAME,
                CallLogTable.JEDI_TO_PHONE
        };

        Cursor cursor = resolver.query(
                UriHelper.getUri(RCMProvider.CALL_LOG, mailboxId),
                projection,
                "(" + CallLogTable.RCM_FOLDER + '=' + type + ") AND (" + CallLogTable.HAS_MORE_RECORD_ITEM + " = 0)",
                null,
                CallLogTable.JEDI_START + " DESC");

        ArrayList<CallLogPOJO> call_log = new ArrayList<CallLogPOJO>();

        if (cursor == null) {
            if (LogSettings.ENGINEERING) {
                EngLog.d(TAG, "getCallLog(): null cursor returned");
            }
            return call_log;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return call_log;
        }

        CallLogPOJO pojo_i;
        do {
            pojo_i = new CallLogPOJO();
            pojo_i.recordDbId = cursor.getLong(cursor.getColumnIndex(CallLogTable._ID));
            pojo_i.setCallDirection(cursor.getInt(cursor.getColumnIndex(CallLogTable.JEDI_CALL_DIRECTION)));
            pojo_i.setCallType(cursor.getInt(cursor.getColumnIndex(CallLogTable.JEDI_CALL_TYPE)));
            pojo_i.setFromName(cursor.getString(cursor.getColumnIndex(CallLogTable.JEDI_FROM_NAME)));
            pojo_i.setFromPhone(cursor.getString(cursor.getColumnIndex(CallLogTable.JEDI_FROM_PHONE)));
            pojo_i.setLength(cursor.getFloat(cursor.getColumnIndex(CallLogTable.JEDI_LENGTH)));
            pojo_i.setLocation(cursor.getString(cursor.getColumnIndex(CallLogTable.JEDI_LOCATION)));
            pojo_i.setMailboxId(cursor.getLong(cursor.getColumnIndex(CallLogTable.JEDI_MAILBOX_ID)));
            pojo_i.setPin(cursor.getString(cursor.getColumnIndex(CallLogTable.JEDI_PIN)));
            pojo_i.setRecordId(cursor.getLong(cursor.getColumnIndex(CallLogTable.JEDI_RECORD_ID)));
            pojo_i.setStart(cursor.getLong(cursor.getColumnIndex(CallLogTable.JEDI_START)));
            pojo_i.setStatus(cursor.getInt(cursor.getColumnIndex(CallLogTable.JEDI_STATUS)));
            pojo_i.setToName(cursor.getString(cursor.getColumnIndex(CallLogTable.JEDI_TO_NAME)));
            pojo_i.setToPhone(cursor.getString(cursor.getColumnIndex(CallLogTable.JEDI_TO_PHONE)));
            pojo_i.recordStatus = CallLogPOJO.Status.KEEP;
            call_log.add(pojo_i);
        } while (cursor.moveToNext());
        cursor.close();
        return call_log;
    }

    /**
     * SYNCHRONIZATION
     */

    /**
     * Save request status to db
     *
     * @param context
     * @param status
     */
    public static void setRequestStatus(Context context, String column, int status) {
        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "setRequestStatus(" + String.valueOf(status) + ")..." + " request: " + column);
        }
        if (updateSingleValue(context, RCMProvider.SYNC_STATUS, column, String.valueOf(status), null) <= 0) {
            ContentValues values = new ContentValues();
            values.put(column, status);
            context.getContentResolver().insert(UriHelper.getUri(RCMProvider.SYNC_STATUS), values);
        }
    }

    /**
     * Get request status from db
     *
     * @param context
     * @return status
     */
    public static int getRequestStatus(Context context, String column) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getRequestStatus(" + column + ")...");

        String status = simpleQuery(context, RCMProvider.SYNC_STATUS, column);
        int result = TextUtils.isEmpty(status) ? SyncStatusTable.SYNC_STATUS_NOT_LOADED : Integer.valueOf(status);      //MY_ANDDROID - default in case of DB error

        if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getRequestStatus(" + column + "): return " + status);
        return result;
    }

    
    public static String getAccountUserFirstName(Context context) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getAccountUserFirstName()...");

        String name = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_FIRST_NAME);

        if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getAccountUserFirstName(): return " + name);
        return name;
    }

    public static String getAccountUserLastName(Context context) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getAccountUserLastName()...");

        String name = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.JEDI_LAST_NAME);

        if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getAccountUserLastName(): return " + name);
        return name;
    }
    
    
    /**
     * Returns the account user name.
     * 
     * @param context
     *            the execution context
     * @param mailboxId
     *            the account
     * @return the user name
     */
    public static String getAccountUserName(Context context, long mailboxId) {
        Uri uri = UriHelper.getUri(RCMProvider.ACCOUNT_INFO, mailboxId);
        Cursor c = null;
        String finalName = null;
        try {
            c = context.getContentResolver().query(uri, PROJECTION_GET_ACCOUNT_USER_NAME, null, null, null);
            if (c != null && c.moveToFirst()) {
                String firstName = c.getString(PROJECTION_GET_ACCOUNT_USER_NAME__FIRST_NAME_IND);
                String lastName = c.getString(PROJECTION_GET_ACCOUNT_USER_NAME__LAST_NAME_IND);

                StringBuffer sb = new StringBuffer();

                if (firstName != null) {
                    sb.append(firstName.trim());
                }

                if (lastName != null) {
                    sb.append(' ');
                    sb.append(lastName.trim());
                }
                finalName = sb.toString().trim();
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.QA) {
                QaLog.e(TAG, "getAccountUserName(): " + error.getMessage());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }

        if (finalName == null || finalName.length() < 1) {
            throw new IllegalStateException("No user name"); 
        }

        return finalName;
    }

    private static final String[] PROJECTION_GET_ACCOUNT_USER_NAME = {
            AccountInfoTable.JEDI_FIRST_NAME,
            AccountInfoTable.JEDI_LAST_NAME };
    private static final int PROJECTION_GET_ACCOUNT_USER_NAME__FIRST_NAME_IND = 0;
    private static final int PROJECTION_GET_ACCOUNT_USER_NAME__LAST_NAME_IND = 1;

    
    public static int getCallerIDsCount(Context context) {
        return getRecordsCount(context, RCMProvider.CALLER_IDS, null); 
    }
    
    public static int getFwNumbersCount(Context context) {
        return getRecordsCount(context, RCMProvider.FW_NUMBERS, null); 
    }
    
    public static int getExtensionsCount(Context context) {
        return getRecordsCount(context, RCMProvider.EXTENSIONS, null); 
    }
    
    public static int getCompanyFavoritesCount(Context context) {
        return getRecordsCount(context, RCMProvider.EXTENSIONS, ExtensionsTable.RCM_STARRED + "=1"); 
    }
    
    public static int getPersonalFavoritesCount(Context context) {
        return getRecordsCount(context, RCMProvider.FAVORITES, null); 
    }

    public static int getCallLogAllCount(Context context) {
        return getRecordsCount(context, RCMProvider.CALL_LOG,
                "(" + CallLogTable.RCM_FOLDER + '=' + CallLogTable.LOGS_ALL + ") AND (" + CallLogTable.HAS_MORE_RECORD_ITEM + "=0)"); 
    }

    public static int getCallLogMissedCount(Context context) {
        return getRecordsCount(context, RCMProvider.CALL_LOG,
                "(" + CallLogTable.RCM_FOLDER + '=' + CallLogTable.LOGS_MISSED + ") AND (" + CallLogTable.HAS_MORE_RECORD_ITEM + "=0)"); 
    }

    public static int getMessagesCount(Context context) {
        return getRecordsCount(context, RCMProvider.MESSAGES, null);
    }

    public static int getMessagesReadCount(Context context) {
        return getRecordsCount(context, RCMProvider.MESSAGES, MessagesTable.JEDI_READ_STATUS + '=' + MessagePOJO.READ);
    }

    public static int getMessagesUnreadCount(Context context) {
        return getRecordsCount(context, RCMProvider.MESSAGES, MessagesTable.JEDI_READ_STATUS + '=' + MessagePOJO.UNREAD);
    }

    public static int getMessagesLocallyDeletedCount(Context context) {
        return getRecordsCount(context, RCMProvider.MESSAGES, MessagesTable.RCM_LOCALLY_DELETED + '=' + MessagePOJO.DELETED_LOCALLY_TRUE);
    }

    public static int getMessagesLoadedCount(Context context) {
        return getRecordsCount(context, RCMProvider.MESSAGES, MessagesTable.RCM_SYNC_STATUS + '=' + SyncStatusEnum.SYNC_STATUS_LOADED);
    }

    public static int getMessagesLoadingCount(Context context) {
        return getRecordsCount(context, RCMProvider.MESSAGES, MessagesTable.RCM_SYNC_STATUS + '=' + SyncStatusEnum.SYNC_STATUS_LOADING);
    }

    public static int getMessagesNotLoadedCount(Context context) {
        return getRecordsCount(context, RCMProvider.MESSAGES, MessagesTable.RCM_SYNC_STATUS + '=' + SyncStatusEnum.SYNC_STATUS_NOT_LOADED);
    }

    public static void saveHttpRegInstanceId(Context context, String id) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveHttpRegInstanceId(" + id + ")");
        }

        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.HTTPREG_INSTANCE_ID, id);
    }

    public static String getHttpRegInstanceId(Context context) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getHttpRegInstanceId()...");

        String id = simpleQueryWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.HTTPREG_INSTANCE_ID);

        if (LogSettings.ENGINEERING)
            EngLog.d(TAG, "getHttpRegInstanceId(): return " + id);
        return id;
    }

    /**
     * Get  VoIP HTTP-REG SIP flags
     */
    public static long getHttpRegSipFlags(Context context) {
        return simpleQueryWithMailboxIdLong(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_VOIP_HTTPREG_SIPFLAGS, 0);
    }
    
    /**
     * Set VoIP HTTP-REG SIP flags
     */
    public static void setHttpRegSipFlags(Context context, long sipFlags) {
        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_VOIP_HTTPREG_SIPFLAGS, String.valueOf(sipFlags));
    }
////////////////////////////////////////////////////////////////////////////////////////////

    /*
     * Service functions
     */
    
    public static String simpleQueryWithMailboxId(Context context, String uri_path, String column) {
        long mailbox_id = getCurrentMailboxId(context);
        Uri uri = UriHelper.getUri(uri_path, mailbox_id);
        return simpleQuery(context, uri, column, null);
    }
    
    public static String simpleQuery(Context context, String uri_path, String column ) {
        Uri uri = UriHelper.getUri(uri_path);
        return simpleQuery(context, uri, column, null);
    }

    private static String simpleQuery(Context context, Uri uri, String column, String selection) {
    	if (context == null) {
    		return "";    		
    	}
    	
    	boolean isPassword=false;
    	if (column!=null && column.indexOf(RCMDataStore.UserCredentialsTable.RCM_PASSWORD)>=0){
    		isPassword = true;
    	}
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "simpleQuery(" + uri + ", " + (isPassword?"":column) + ") " + ( selection == null ? "" : " selection: " + selection ) );

        Cursor cursor = context.getContentResolver().query(uri, new String[]{column}, selection, null, null);
        if (cursor == null) {
            if (RCMProvider.DEBUG_ENBL)
                EngLog.d(TAG, "simpleQuery(): null cursor received; return \"\"");
            return "";
        }

        if (!cursor.moveToFirst()) {
            if (RCMProvider.DEBUG_ENBL) {
                EngLog.d(TAG, "simpleQuery(): empty cursor received; return \"\", count: " + cursor.getCount());
            }
            cursor.close();
            return "";
        }

        String result = cursor.getString(0);
        if (result == null) {
            if (RCMProvider.DEBUG_ENBL)
                EngLog.d(TAG, "simpleQuery(): cursor returned null; return \"\"");
            result = "";
        }

        cursor.close();
        return result;
    }

    private static long simpleQueryWithMailboxIdLong(Context context, String uri_path, String column, long def_value ) {
        String result = simpleQueryWithMailboxId(context, uri_path, column );
        long value = TextUtils.isEmpty(result) ? def_value : Long.valueOf(result);

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "column: " + column + " result:" + value);
        
        return value;
    }

    private static int simpleQueryWithMailboxIdInt(Context context, String uri_path, String column, int def_value ) {
        String result = simpleQueryWithMailboxId(context, uri_path, column );
        int value = TextUtils.isEmpty(result) ? def_value : Integer.valueOf(result);

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "column: " + column + " result:" + value);
        
        return value;
    }
    

    private static String simpleSelectionQueryWithMailboxId(Context context, String uri_path, String column, String selection) {
        long mailbox_id = getCurrentMailboxId(context);
        Uri uri = UriHelper.getUri(uri_path, mailbox_id);
        return simpleQuery(context, uri, column, selection);
    }

    
    public static int updateSingleValue(Context context, String uri_path, String column, String value, String where) {
        Uri uri = UriHelper.getUri(uri_path);
        ContentValues values = new ContentValues();
        values.put(column, value);
        return context.getContentResolver().update(uri, values, where, null);   //returns the number of updated rows
    }

    public static int updateSingleValueWithMailboxId(Context context, String uri_path, String column, String value, String where) {
        long mailbox_id = getCurrentMailboxId(context);
        String where_where = RCMColumns.MAILBOX_ID + '=' + mailbox_id; 

        if (!TextUtils.isEmpty(where)) {
            where_where += " AND " + where;
        }
        
        return updateSingleValue(context, uri_path, column, value, where_where);
    }
    
    public static void updateOrInsertSingleValueWithMailboxId(Context context, String uri_path, String column, String value) {
        long mailbox_id = getCurrentMailboxId(context);
        String where = RCMColumns.MAILBOX_ID + '=' + mailbox_id;
        
        if (updateSingleValue(context, uri_path, column, value, where) <= 0)    //no record with this mailbox_id
        {
            insertSingleValueWithMailboxId(context, uri_path, column, value);
        }
    }

    private static void insertSingleValueWithMailboxId(Context context, String uri_path, String column, String value) {
        long mailbox_id = getCurrentMailboxId(context);
        ContentValues values = new ContentValues();
        values.put(RCMColumns.MAILBOX_ID, mailbox_id);
        values.put(column, value);
        context.getContentResolver().insert(UriHelper.getUri(uri_path), values);
    }

    private static void clearTableAndInsertEmptyRow(Context context, String uri_path) {
        ContentResolver resolver = context.getContentResolver();
        resolver.delete(UriHelper.getUri(uri_path), null, null);
        
        ContentValues val = new ContentValues(1);
        val.putNull(BaseColumns._ID);
        resolver.insert(UriHelper.getUri(uri_path), val);
    }
    
    private static int getRecordsCount(Context context, String uri_path, String selection) {
        Cursor cursor = context.getContentResolver().query(UriHelper.getUri(uri_path, getCurrentMailboxId(context)),
                new String[] {BaseColumns._ID}, selection, null, null);
        int count = (null != cursor ? cursor.getCount() : 0);
        if(null != cursor) {
            cursor.close();
        }
        return count;
    }

    public static void setTosAccepted(Context context, boolean accepted) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "setTosAccepted(" + accepted + ")...");

        String value = accepted ? "1" : "0";
        updateSingleValue(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_TOS_ACCEPTED, value, null);
    }
    /*
    * 0 - user has never passed dialog
    * 1 - Accept
    * 2 - Decline
    */
    public static void set911TosAccepted(Context context, boolean accepted) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "set911TosAccepted(" + accepted + ")...");

        String value = accepted ? "1" : "2";
        updateSingleValue(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_TOS911_ACCEPTED, value, null);
    }
    public static void setAccountVoipWhatsNewDialogShown(Context context, boolean shown) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "setAccountVoipWhatsNewDialogShown(" + shown + ")...");

        String value = shown ? "1" : "0";
        updateOrInsertSingleValueWithMailboxId(context, RCMProvider.ACCOUNT_INFO, AccountInfoTable.RCM_VOIP_WHATS_NEW, value);
    }
    public static void setDeviceExternalSpeakerDelay(Context context, int delay) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "setAECExternalSpeakerDelay(" + delay + ")...");

        String value = String.valueOf((delay > 0 ? delay : 0));
        
        updateSingleValue(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_DEVICE_ECHO_VALUE_SPEAKER, value, null);
    }
    public static void setDeviceInternalSpeakerDelay(Context context, int delay) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "setAECInternalSpeakerDelay(" + delay + ")...");
        
        String value = String.valueOf((delay > 0 ? delay : 0));
        
        updateSingleValue(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_DEVICE_ECHO_VALUE_EARPIECE, value, null);
    }
    public static void setDeviceAudioWizard(Context context, boolean accepted) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "setDeviceAudioWizard(" + accepted + ")...");

        String value = accepted ? "1" : "0";
        updateSingleValue(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_DEVICE_AUDIO_SETUP_WIZARD, value, null);
    }
    /*
     * TSC Config Section
     */
    public static boolean getTSCEnalbe(Context context){
		return simpleQuery(context, RCMProvider.TSC_CONFIG, TSCConfigTable.TSC_ENABLE).equals("1");
    }
    
    public static String getTSCTunnelAddress(Context context){
		return simpleQuery(context, RCMProvider.TSC_CONFIG, TSCConfigTable.TUNNEL_ADDRESS).toString();
    }

    public static String getTSCTunnelPort(Context context){
		return simpleQuery(context, RCMProvider.TSC_CONFIG, TSCConfigTable.TUNNEL_PORT).toString();
    }
 
    public static int getTSCTunnelPort_int(Context context){
		return Integer.valueOf(simpleQuery(context, RCMProvider.TSC_CONFIG, TSCConfigTable.TUNNEL_PORT).toString());
    }

    public static int getTSCTunnelMode(Context context){
		return Integer.valueOf(simpleQuery(context, RCMProvider.TSC_CONFIG, TSCConfigTable.TUNNEL_MODE).toString());
    }

    public static int getTSCRedundancy(Context context){
		return Integer.valueOf(simpleQuery(context, RCMProvider.TSC_CONFIG, TSCConfigTable.TSC_REDUNDANCY).toString());
    }

    public static boolean getTSCLogging(Context context){
		return simpleQuery(context, RCMProvider.TSC_CONFIG, TSCConfigTable.TSC_LOGGING).equals("1");
    }

    public static boolean getTSCDebug(Context context){
		return simpleQuery(context, RCMProvider.TSC_CONFIG, TSCConfigTable.DEBUG).equals("1");
    }

    public static boolean getTSCTunnelLoadBalance(Context context){
		return simpleQuery(context, RCMProvider.TSC_CONFIG, TSCConfigTable.TUNNEL_LOAD_BALANCE).equals("1");
    }
}
