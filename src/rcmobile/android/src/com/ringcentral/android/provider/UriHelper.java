package com.ringcentral.android.provider;

import android.net.Uri;

import com.rcbase.android.logging.EngLog;

public class UriHelper {

    private static String TAG = "[RC] UriHelper";
    private static final Uri URI_MAILBOX_CURRENT = Uri.parse(  "content://" 
                                                           + RCMProvider.AUTHORITY + '/'
                                                           + RCMProvider.MAILBOX_CURRENT);

    public static Uri getUriMailboxCurrent()
    {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getUriMailboxCurrent()");

        return URI_MAILBOX_CURRENT;
    }
    
    public static Uri getUri(String path) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getUri(" + path + ')');
        
        Uri uri = prepare(path).build();
        
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "uri: " + uri);

        return uri;
    }
    
    public static Uri getUri(String path, long mailbox_id) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getUri(" + path + ", " + mailbox_id + ')');
        
        Uri uri = prepare(path, mailbox_id).build();
        
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "uri: " + uri);

        return uri;
    }
    
    public static Uri getUri(String path, long mailbox_id, long item_id) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "getUri(" + path + ", " + mailbox_id + ", " + item_id + ')');
        
        Uri uri = prepare(path, mailbox_id).appendPath(String.valueOf(item_id)).build();

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "uri: " + uri);

        return uri;
    }
    
    static Uri removeQuery(Uri uri) {
        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "removeQuery(" + uri + ")");

        Uri newUri = uri.buildUpon().query("").build();

        if (RCMProvider.DEBUG_ENBL)
            EngLog.d(TAG, "new uri: " + newUri);
        
        return newUri;
    }
    
    private static Uri.Builder prepare(String path) {
        return new Uri.Builder().scheme("content")
                                .authority(RCMProvider.AUTHORITY)
                                .path(path)
                                .query("")      // This is a workaround for Android 1.5 bug:                                           
                                .fragment("");  // NullPointerException at android.net.Uri$HierarchicalUri.writeToParcel(Uri.java:1117,1118)
                                                // (called during ContentProvider.notifyChange() execution)                            
                                                
    }
    
    private static Uri.Builder prepare(String path, long mailbox_id) {
        return prepare(path).appendQueryParameter(RCMDataStore.RCMColumns.MAILBOX_ID, String.valueOf(mailbox_id));
    }
}
