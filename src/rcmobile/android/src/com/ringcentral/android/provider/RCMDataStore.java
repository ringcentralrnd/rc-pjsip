/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.provider;

import java.util.LinkedHashMap;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.sip.client.RCSIP;
import com.rcbase.android.sip.service.CodecInfo;
import com.ringcentral.android.RCMConfig;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.api.pojo.CallLogPOJO;


public final class RCMDataStore {
    
    private RCMDataStore() {};

    public static final int DB_VERSION = 53;
    static final String DB_FILE = "rcm.db";

    
    public interface RCMColumns {
        
        public static final String MAILBOX_ID = "mailboxId";                   //INTEGER (long)
        public static final String DEFAULT_SORT_ORDER = "_ID ASC";
    }

    
    public interface MailboxStateEnum {

        public static final int MAILBOX_STATE_ONLINE = 1;
        public static final int MAILBOX_STATE_ONLINE_NO_CELLULAR = 2;
        public static final int MAILBOX_STATE_OFFLINE = 3;
        public static final int MAILBOX_STATE_OFFLINE_SUSPENDED = 4;
        public static final int MAILBOX_STATE_LOGIN = 5;
        public static final int MAILBOX_STATE_LOGOUT = 6;
    }
    
    public interface SyncStatusEnum {
        public static final int SYNC_STATUS_NOT_LOADED = 0;
        public static final int SYNC_STATUS_LOADING = 1;
        public static final int SYNC_STATUS_LOADED = 2;
        public static final int SYNC_STATUS_ERROR = 3;
    }

        
    public static final class MailboxCurrentTable extends RCMDbTable implements BaseColumns, RCMColumns {

        private MailboxCurrentTable() {};
        private static final MailboxCurrentTable sInstance = new MailboxCurrentTable();
        static MailboxCurrentTable getInstance() {
            return sInstance;
        }

        public static final int MAILBOX_ID_NONE = 0;      //ZERO never used as mailboxId

        private static final String TABLE_NAME = "MAILBOX_CURRENT";
        
        private static final String CREATE_TABLE_STMT =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
          + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
          + MAILBOX_ID + " INTEGER" 
          + ");";

        private static final String INIT_TABLE_STMT =
            "INSERT INTO " + TABLE_NAME
          + " (" +  MAILBOX_ID + ") "
          + "VALUES (" + MAILBOX_ID_NONE + ")";


        @Override
        String getName() {
            return TABLE_NAME;
        }
        
        @Override
        void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_STMT);
            db.execSQL(INIT_TABLE_STMT);
        }
    }

    
    public static final class DeviceAttribTable extends RCMDbTable implements BaseColumns {
        
        private DeviceAttribTable() {};
        private static final DeviceAttribTable sInstance = new DeviceAttribTable();
        static DeviceAttribTable getInstance() {
            return sInstance;
        }

        private static final String TABLE_NAME = "DEVICE_ATTRIB";
       
        /* Columns */
        public static final String RCM_DEVICE_PHONE_NUMBER = "device_phone_number"; //TEXT
        public static final String RCM_DEVICE_IMSI = "device_IMSI"; // TEXT
        public static final String RCM_TOS_ACCEPTED = "TOS_accepted";
        public static final String RCM_TOS911_ACCEPTED = "TOS911_accepted";
        public static final String RCM_DEVICE_ECHO_STATE = "device_echo_state"; // INTEGER
        public static final String RCM_DEVICE_ECHO_VALUE_SPEAKER = "device_echo_value_speaker"; // INTEGER
        public static final String RCM_DEVICE_ECHO_VALUE_EARPIECE = "device_echo_value_earpiece"; // INTEGER
        public static final String RCM_DEVICE_AUDIO_SETUP_WIZARD = "device_audio_setup_wizard"; // INTEGER

        /**
         * Wide-band (Wi-Fi) oIP codec priorities
         */
        public static final String RCM_WIDEBAND_ILBC_CODEC_PRIO = "wb_ilbc_prio"; // INTEGER
        public static final String RCM_WIDEBAND_GSM_CODEC_PRIO  = "wb_gsm_prio";  // INTEGER
        public static final String RCM_WIDEBAND_SILK_CODEC_PRIO  = "wb_silk_prio";
        /*
        public static final String RCM_WIDEBAND_SILKNB_CODEC_PRIO  = "wb_silknb_prio";
        public static final String RCM_WIDEBAND_SILKMB_CODEC_PRIO  = "wb_silkmb_prio";
        public static final String RCM_WIDEBAND_SILKWB_CODEC_PRIO  = "wb_silkwb_prio";
        public static final String RCM_WIDEBAND_SILKUWB_CODEC_PRIO  = "wb_silkuwb_prio";*/
        public static final String RCM_WIDEBAND_PCMU_CODEC_PRIO = "wb_pcmu_prio"; // INTEGER
        public static final String RCM_WIDEBAND_PCMA_CODEC_PRIO = "wb_pcma_prio"; // INTEGER
        public static final String RCM_WIDEBAND_G722_CODEC_PRIO  = "wb_g722_prio";  // INTEGER
        
        /**
         * Narrow-band (3G/4G) oIP codec priorities
         */
        public static final String RCM_NARROWBAND_ILBC_CODEC_PRIO = "nb_ilbc_prio"; // INTEGER
        public static final String RCM_NARROWBAND_GSM_CODEC_PRIO  = "nb_gsm_prio";  // INTEGER
        public static final String RCM_NARROWBAND_SILK_CODEC_PRIO  = "nb_silk_prio"; 
        /*
        public static final String RCM_NARROWBAND_SILKNB_CODEC_PRIO  = "nb_silknb_prio";
        public static final String RCM_NARROWBAND_SILKMB_CODEC_PRIO  = "nb_silkmb_prio";
        public static final String RCM_NARROWBAND_SILKWB_CODEC_PRIO  = "nb_silkwb_prio";
        public static final String RCM_NARROWBAND_SILKUWB_CODEC_PRIO  = "nb_silkuwb_prio";*/
        public static final String RCM_NARROWBAND_PCMU_CODEC_PRIO = "nb_pcmu_prio"; // INTEGER
        public static final String RCM_NARROWBAND_PCMA_CODEC_PRIO = "nb_pcma_prio"; // INTEGER
        public static final String RCM_NARROWBAND_G722_CODEC_PRIO  = "nb_g722_prio";  // INTEGER
        
        /**
         * Defines if EDGE network shall be included for 3G/4G calls for testing
         */
        public static final String RCM_INCLUDE_EDGE_INTO_3G4G_VOIP = "edge_incl_mob_voip"; // INTEGER
        
        /**
         * Defines if needed keeping partial lock when stack is active. ATTENTION: CAN CAUSE BATTERY DRAIN
         */
        public static final String RCM_ALWAYS_CPU_PARTAIL_LOCK_WHEN_STACK_IS_ACTIVE = "always_partial_lock_for_stack"; // INTEGER
        
        private static final String CREATE_TABLE_STMT =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
          + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
          + RCM_DEVICE_PHONE_NUMBER + " TEXT," 
          + RCM_DEVICE_IMSI + " TEXT,"
          + RCM_TOS_ACCEPTED + " INTEGER DEFAULT 0,"
          + RCM_TOS911_ACCEPTED + " INTEGER DEFAULT 0,"
          + RCM_DEVICE_ECHO_STATE + " TEXT DEFAULT \'1\',"
          + RCM_DEVICE_ECHO_VALUE_SPEAKER + " INTEGER DEFAULT " + RCMConstants.DEFAULT_AEC_DELAY_VALUE +","
          + RCM_DEVICE_ECHO_VALUE_EARPIECE + " INTEGER DEFAULT " + RCMConstants.DEFAULT_AEC_DELAY_VALUE +","
          + RCM_DEVICE_AUDIO_SETUP_WIZARD + " INTEGER DEFAULT 0,"
          + RCM_INCLUDE_EDGE_INTO_3G4G_VOIP + " INTEGER DEFAULT 0,"
          
          + RCM_WIDEBAND_ILBC_CODEC_PRIO + " INTEGER DEFAULT " + CodecInfo.Codec.ILBC.getWideBandDefaultPriority() + ","
          + RCM_WIDEBAND_GSM_CODEC_PRIO  + " INTEGER DEFAULT " + CodecInfo.Codec.GSM.getWideBandDefaultPriority()  + ","
          + RCM_WIDEBAND_SILK_CODEC_PRIO  + " INTEGER DEFAULT " + CodecInfo.Codec.SILK.getWideBandDefaultPriority()  + ","
         /* + RCM_WIDEBAND_SILKNB_CODEC_PRIO  + " INTEGER DEFAULT " + CodecInfo.Codec.SILKNB.getWideBandDefaultPriority()  + ","
          + RCM_WIDEBAND_SILKMB_CODEC_PRIO  + " INTEGER DEFAULT " + CodecInfo.Codec.SILKMB.getWideBandDefaultPriority()  + ","
          + RCM_WIDEBAND_SILKWB_CODEC_PRIO  + " INTEGER DEFAULT " + CodecInfo.Codec.SILKWB.getWideBandDefaultPriority()  + ","
          + RCM_WIDEBAND_SILKUWB_CODEC_PRIO  + " INTEGER DEFAULT " + CodecInfo.Codec.SILKUWB.getWideBandDefaultPriority()  + ","*/
          + RCM_WIDEBAND_PCMU_CODEC_PRIO + " INTEGER DEFAULT " + CodecInfo.Codec.PCMU.getWideBandDefaultPriority() + ","
          + RCM_WIDEBAND_PCMA_CODEC_PRIO + " INTEGER DEFAULT " + CodecInfo.Codec.PCMA.getWideBandDefaultPriority() + ","
          + RCM_WIDEBAND_G722_CODEC_PRIO  + " INTEGER DEFAULT " + CodecInfo.Codec.G722.getWideBandDefaultPriority()  + ","
          
          
          + RCM_NARROWBAND_ILBC_CODEC_PRIO + " INTEGER DEFAULT " + CodecInfo.Codec.ILBC.getNarrowBandDefaultPriority() + ","
          + RCM_NARROWBAND_GSM_CODEC_PRIO  + " INTEGER DEFAULT " + CodecInfo.Codec.GSM.getNarrowBandDefaultPriority()  + ","
          + RCM_NARROWBAND_SILK_CODEC_PRIO  + " INTEGER DEFAULT " + CodecInfo.Codec.SILK.getNarrowBandDefaultPriority()  + ","
          /*+ RCM_NARROWBAND_SILKNB_CODEC_PRIO  + " INTEGER DEFAULT " + CodecInfo.Codec.SILKNB.getNarrowBandDefaultPriority()  + ","
          + RCM_NARROWBAND_SILKMB_CODEC_PRIO  + " INTEGER DEFAULT " + CodecInfo.Codec.SILKMB.getNarrowBandDefaultPriority()  + ","
          + RCM_NARROWBAND_SILKWB_CODEC_PRIO  + " INTEGER DEFAULT " + CodecInfo.Codec.SILKWB.getNarrowBandDefaultPriority()  + ","
          + RCM_NARROWBAND_SILKUWB_CODEC_PRIO  + " INTEGER DEFAULT " + CodecInfo.Codec.SILKUWB.getNarrowBandDefaultPriority()  + ","*/
          + RCM_NARROWBAND_PCMU_CODEC_PRIO + " INTEGER DEFAULT " + CodecInfo.Codec.PCMU.getNarrowBandDefaultPriority() + ","
          + RCM_NARROWBAND_PCMA_CODEC_PRIO + " INTEGER DEFAULT " + CodecInfo.Codec.PCMA.getNarrowBandDefaultPriority() + ","
          + RCM_NARROWBAND_G722_CODEC_PRIO  + " INTEGER DEFAULT " + CodecInfo.Codec.G722.getNarrowBandDefaultPriority()  + ","
          + RCM_ALWAYS_CPU_PARTAIL_LOCK_WHEN_STACK_IS_ACTIVE  + " INTEGER DEFAULT " +  RCSIP.ALWAYS_CPU_LOCK_FOR_SIP_INITAL_VALUE 
          + ");";

        private static final String INIT_TABLE_STMT =   
            "INSERT INTO " + TABLE_NAME
          + " (" 
          + RCM_DEVICE_PHONE_NUMBER + ',' 
          + RCM_DEVICE_IMSI + ','
          + RCM_TOS_ACCEPTED + ',' 
          + RCM_TOS911_ACCEPTED + ","
          + RCM_DEVICE_ECHO_STATE + ","
          + RCM_DEVICE_ECHO_VALUE_SPEAKER + ","
          + RCM_DEVICE_ECHO_VALUE_EARPIECE + ","
          + RCM_DEVICE_AUDIO_SETUP_WIZARD  + ","
          + RCM_INCLUDE_EDGE_INTO_3G4G_VOIP + ","
          
          + RCM_WIDEBAND_ILBC_CODEC_PRIO  + ","
          + RCM_WIDEBAND_GSM_CODEC_PRIO  + ","
          + RCM_WIDEBAND_SILK_CODEC_PRIO  + ","
          /*+ RCM_WIDEBAND_SILKNB_CODEC_PRIO  + ","
          + RCM_WIDEBAND_SILKMB_CODEC_PRIO  + ","
          + RCM_WIDEBAND_SILKWB_CODEC_PRIO  + ","
          + RCM_WIDEBAND_SILKUWB_CODEC_PRIO  + ","*/
          + RCM_WIDEBAND_PCMU_CODEC_PRIO  + ","
          + RCM_WIDEBAND_PCMA_CODEC_PRIO  + ","
          + RCM_WIDEBAND_G722_CODEC_PRIO  + ","
          
          + RCM_NARROWBAND_ILBC_CODEC_PRIO  + ","
          + RCM_NARROWBAND_GSM_CODEC_PRIO  + ","
          + RCM_NARROWBAND_SILK_CODEC_PRIO  + ","
          /*+RCM_NARROWBAND_SILKNB_CODEC_PRIO  + ","
          +RCM_NARROWBAND_SILKMB_CODEC_PRIO + ","
          +RCM_NARROWBAND_SILKWB_CODEC_PRIO + ","
          +RCM_NARROWBAND_SILKUWB_CODEC_PRIO + ","*/
          + RCM_NARROWBAND_PCMU_CODEC_PRIO  + ","
          + RCM_NARROWBAND_PCMA_CODEC_PRIO   + "," 
          + RCM_NARROWBAND_G722_CODEC_PRIO  + ","
          + RCM_ALWAYS_CPU_PARTAIL_LOCK_WHEN_STACK_IS_ACTIVE
          + ") "
          + "VALUES (NULL,NULL,0,0,1,"
          + RCMConstants.DEFAULT_AEC_DELAY_VALUE + ","
          + RCMConstants.DEFAULT_AEC_DELAY_VALUE + "," 
          + "0,0,"
          + CodecInfo.Codec.ILBC.getWideBandDefaultPriority() + ","
          + CodecInfo.Codec.GSM.getWideBandDefaultPriority()  + ","
          + CodecInfo.Codec.SILK.getWideBandDefaultPriority()  + ","
          /*+ CodecInfo.Codec.SILKNB.getWideBandDefaultPriority()  + ","
          + CodecInfo.Codec.SILKMB.getWideBandDefaultPriority()  + ","
          + CodecInfo.Codec.SILKWB.getWideBandDefaultPriority()  + ","
          + CodecInfo.Codec.SILKUWB.getWideBandDefaultPriority()  + ","*/
          + CodecInfo.Codec.PCMU.getWideBandDefaultPriority() + ","
          + CodecInfo.Codec.PCMA.getWideBandDefaultPriority() + ","
           + CodecInfo.Codec.G722.getWideBandDefaultPriority()  + ","
          
          + CodecInfo.Codec.ILBC.getNarrowBandDefaultPriority() + ","
          + CodecInfo.Codec.GSM.getNarrowBandDefaultPriority()  + ","
          + CodecInfo.Codec.SILK.getNarrowBandDefaultPriority()  + ","
         /*  + CodecInfo.Codec.SILKNB.getNarrowBandDefaultPriority()  + ","
            + CodecInfo.Codec.SILKMB.getNarrowBandDefaultPriority()  + ","
            + CodecInfo.Codec.SILKWB.getNarrowBandDefaultPriority()  + ","
            + CodecInfo.Codec.SILKUWB.getNarrowBandDefaultPriority()  + ","*/
          + CodecInfo.Codec.PCMU.getNarrowBandDefaultPriority() + ","
          + CodecInfo.Codec.PCMA.getNarrowBandDefaultPriority() + ","
           + CodecInfo.Codec.G722.getNarrowBandDefaultPriority()  + ","
          + RCSIP.ALWAYS_CPU_LOCK_FOR_SIP_INITAL_VALUE
          + ")";

        @Override
        String getName() {
            return TABLE_NAME;
        }
        
        @Override
        void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_STMT);
            db.execSQL(INIT_TABLE_STMT);
        }
    }

    
    public static final class UserCredentialsTable extends RCMDbTable implements BaseColumns {

        private UserCredentialsTable() {}
        private static final UserCredentialsTable sInstance = new UserCredentialsTable();
        static UserCredentialsTable getInstance() {
            return sInstance;
        }

        private static final String TABLE_NAME = "USER_CREDENTIALS";

        /* Columns */
        public static final String RCM_LOGIN_NUMBER = "login_number";   //TEXT
        public static final String RCM_LOGIN_EXT = "login_ext";                 //TEXT
        public static final String RCM_PASSWORD = "password";               //TEXT

        public static final String JEDI_LOGIN_IP_ADDRESS = "login_ip_address";               // TEXT
        public static final String JEDI_LOGIN_REQUEST_ID = "login_request_id";               // INTEGER
        public static final String JEDI_LOGIN_START_TIME = "login_start_time";               // INTEGER
        public static final String JEDI_LOGIN_HASH = "login_hash";               // TEXT

        public static final String JEDI_LOGIN_COOKIE = "login_cookie";               // TEXT

        public static final String JEDI_USER_ID = "userId";                         //INTEGER (long)

        private static final String CREATE_TABLE_STMT =
                "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
                        + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + RCM_LOGIN_NUMBER + " TEXT,"
                        + RCM_LOGIN_EXT + " TEXT,"
                        + RCM_PASSWORD + " TEXT,"
                        + JEDI_LOGIN_IP_ADDRESS + " TEXT,"
                        + JEDI_LOGIN_REQUEST_ID + " INTEGER,"
                        + JEDI_LOGIN_START_TIME + " INTEGER,"
                        + JEDI_LOGIN_HASH + " TEXT,"
                        + JEDI_LOGIN_COOKIE + " TEXT,"
                        + JEDI_USER_ID + " INTEGER"
                        + ");";

        private static final String INIT_TABLE_STMT =
                "INSERT INTO " + TABLE_NAME
                        + " (" + RCM_LOGIN_NUMBER + ',' 
                        + RCM_LOGIN_EXT + ',' 
                        + RCM_PASSWORD + ','
                        + JEDI_LOGIN_IP_ADDRESS + ',' 
                        + JEDI_LOGIN_REQUEST_ID + ','
                        + JEDI_LOGIN_START_TIME + ',' 
                        + JEDI_LOGIN_HASH + ','
                        + JEDI_LOGIN_COOKIE + ',' 
                        + JEDI_USER_ID 
                        + ") "
                        + "VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)";


        @Override
        String getName() {
            return TABLE_NAME;
        }
        
        @Override
        void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_STMT);
            db.execSQL(INIT_TABLE_STMT);
        }
    }

    
    public static final class ServiceInfoTable extends RCMDbTable implements RCMColumns, BaseColumns {
        
        private ServiceInfoTable() {};
        private static final ServiceInfoTable sInstance = new ServiceInfoTable();
        static ServiceInfoTable getInstance() {
            return sInstance;
        }

        private static final String TABLE_NAME = "SERVICE_INFO";
       
        /* Columns */
        public static final String JEDI_API_VERSION = "api_version"; //TEXT

        private static final String CREATE_TABLE_STMT =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
          + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
          + MAILBOX_ID + " INTEGER UNIQUE ON CONFLICT REPLACE," 
          + JEDI_API_VERSION + " TEXT" 
          + ");";


        @Override
        String getName() {
            return TABLE_NAME;
        }
        
        @Override
        void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_STMT);
        }
    }

    
    public static final class AccountInfoTable extends RCMDbTable implements RCMColumns, BaseColumns {
        
        private AccountInfoTable() {};
        private static final AccountInfoTable sInstance = new AccountInfoTable();
        static AccountInfoTable getInstance() {
            return sInstance;
        }

        private static final String TABLE_NAME = "ACCOUNT_INFO";
        
        public static final int SERVICE_VERSION_4 = 1;  //JEDI 4.0
        public static final int SERVICE_VERSION_5 = 2;  //JEDI 5.0.x
        
        public static final String ACCESS_LEVEL_ADMIN = "Admin";
        public static final String ACCESS_LEVEL_USER  = "User";
        public static final String ACCESS_LEVEL_VIEW  = "View";

        public static final String AGENT_STATUS_OFFLINE = "Offline";
        public static final String AGENT_STATUS_ONLINE = "Online";
 
        
        /* DND status values as defined in JEDI iPhone API (5.0.x only) */
        public static final String DND_STATUS_TAKE_ALL_CALLS = "TakeAllCalls";
        public static final String DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS = "DoNotAcceptDepartmentCalls";
        public static final String DND_STATUS_TAKE_DEPARTMENT_CALLS_ONLY = "TakeDepartmentCallsOnly";
        public static final String DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS = "DoNotAcceptAnyCalls";
       
        public static final String EXTENSION_TYPE_UNKNOWN = "Unknown";
        public static final String EXTENSION_TYPE_USER = "User";
        public static final String EXTENSION_TYPE_DEPARTMENT = "Department";
        public static final String EXTENSION_TYPE_ANNOUNCEMENT = "Announcement";
        public static final String EXTENSION_TYPE_VOICEMAIL = "Voicemail";

        /* Account tier type (Tier Service type, e.g. "RCMobile", "RCOffice", "RCVoice", "RCFax"). Valid from 5.0.x. */
        public static final String TIER_SERVICE_TYPE_RCMOBILE = "RCMobile";
        public static final String TIER_SERVICE_TYPE_RCOFFICE = "RCOffice";
        public static final String TIER_SERVICE_TYPE_RCVOICE = "RCVoice";
        public static final String TIER_SERVICE_TYPE_RCFAX = "RCFax";
        
        /* Setup Wizard state */
        public static final String SETUP_WIZARD_STATE_NOT_STARTED = "NotStarted";
        public static final String SETUP_WIZARD_STATE_INCOMPLETE = "Incomplete";
        public static final String SETUP_WIZARD_STATE_COMPLETED = "Completed";
        
        public static final String TRIAL_STATUS_NOTEXPIRED = 	 "NotExpired";
        public static final String TRIAL_STATUS_EXPIREDINXDAYS = "ExpiredInXDays";
        public static final String TRIAL_STATUS_EXPIRED = 		 "Expired";
        
        /* Columns */
        public static final String JEDI_USER_ID = "userId";                         //INTEGER (long)
        public static final String JEDI_ACCOUNT_NUMBER = "accountNumber";           //TEXT
        public static final String JEDI_EMAIL = "email";                            //TEXT
        public static final String JEDI_PIN = "pin";                                //TEXT
        public static final String JEDI_FIRST_NAME = "firstName";                   //TEXT
        public static final String JEDI_LAST_NAME = "lastName";                     //TEXT
        public static final String JEDI_AGENT = "agent";                            //INTEGER (boolean)
        public static final String JEDI_AGENT_STATUS = "agentStatus";               //TEXT (enum)
        public static final String JEDI_DND = "dnd";                                //INTEGER (boolean) //4.x only
        public static final String JEDI_DND_STATUS = "dndStatus";                   //TEXT (enum)   //5.0.x only
        public static final String JEDI_FREE = "free";                              //INTEGER (boolean)
        public static final String JEDI_ACCESS_LEVEL = "accessLevel";               //TEXT
        public static final String JEDI_EXTENSION_TYPE = "extensionType";           //TEXT
        public static final String JEDI_SYSTEM_EXTENSION  = "systemExtension";      //INTEGER (boolean)
        public static final String JEDI_SERVICE_VERSION = "serviceVersion";         //INTEGER
        public static final String JEDI_TIER_SETTINGS = "tierSettings";             //INTEGER (long)
        public static final String JEDI_TIER_SERVICE_TYPE = "tierServiceType";      //TEXT
        public static final String JEDI_EXT_MOD_COUNTER = "globalExtModCounter";    //INTEGER (long)
        public static final String JEDI_EXT_MOD_COUNTER_TEMP = "globalExtModCounter_TEMP";    //INTEGER (long)
        public static final String JEDI_MSG_MOD_COUNTER = "msgModCounter";          //INTEGER (long)
        public static final String JEDI_MSG_MOD_COUNTER_TEMP = "msgModCounter_TEMP";//INTEGER (long)
        public static final String JEDI_SETUP_WIZARD_STATE  = "setupWizardState";   //TEXT (enum)
        public static final String JEDI_EXPRESS_SETUP_MOBILE_URL  = "expressSetupMobileUrl";//TEXT
        public static final String JEDI_TRIAL_EXPIRATION_STATE  = "trialExpirationState";//TEXT
        public static final String JEDI_DAYS_TO_EXPIRE  = "daysToExpire";            //INTEGER
        public static final String JEDI_BRAND_ID = "brandId";						//TEXT

        public static final String HTTPREG_INSTANCE_ID = "httpreg_instance_id";     //TEXT
        public static final String RCM_LOGIN_NUMBER = "login_number";               //TEXT
        public static final String RCM_LOGIN_EXT = "login_ext";                     //TEXT
        public static final String RCM_PASSWORD = "password";                       //TEXT
        public static final String RCM_CALLER_ID = "caller_id";                     //_ID in CallerIDs table or actual number????
        public static final String RCM_RINGOUT_MODE = "ringout_mode";               //INTEGER ("My Android", "Another phone")
        public static final String RCM_RINGOUT_ANOTHER_PHONE = "ringout_another_phone"; //TEXT
        public static final String RCM_CUSTOM_NUMBER = "custom_number";             //TEXT
        public static final String RCM_CONFIRM_CONNECTION = "confirm_connection";   //INTEGER (boolean)
        public static final String RCM_LAST_CALL_NUMBER = "last_call_number";       //TEXT
        public static final String RCM_MAILBOX_STATE = "mailbox_state";             //INTEGER ("online", "online_no_cellular", "offline", "offline_suspended", "login", "logout")
        public static final String RCM_LAST_SYNC = "last_sync";                     //INTEGER (long): Time of last (successful) sync
        public static final String RCM_LAST_LOADED_MSG_ID = "lastLoadedMsgId";      //INTEGER (long)
        public static final String RCM_LAST_COMPLETE_SETUP_REQ = "lastCompleteSetupRequest"; // INTEGER (long) : Time of last request ro proceed with setup
        public static final String RCM_LAST_EXPIRED_REQ = "lastExpiredRequest";     // INTEGER (long) : Time of last notification of expiration
        public static final String RCM_VOIP_SETTINGS = "VoIP_Settings";           	//INTEGER (long)
        public static final String RCM_MESSAGE_CHECK_INTERVAL = "SyncInterval";     //INTEGER 
        public static final String RCM_URI_SIP_PROVIDER	= "URI_SipProvider";		//TEXT
        public static final String RCM_URI_SIP_OUTBOUND_PROXY	= "URI_SipOutboundProxy";		//TEXT
        public static final String RCM_VOIP_WHATS_NEW   = "voip_whats_new_dialog";       //INTEGER
        public static final String RCM_VOIP_HTTPREG_SIPFLAGS = "http_reg_sip_flags";   //INTEGER (long)

        
        /* RingOut mode enumeration */
        public static final int RINGOUT_MODE_MY_ANDROID = RCMConstants.RINGOUT_MODE_MY_ANDROID;
        public static final int RINGOUT_MODE_ANOTHER_PHONE = RCMConstants.RINGOUT_MODE_ANOTHER_PHONE;
    
		private static final String CREATE_TABLE_STMT =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
          + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
          + MAILBOX_ID + " INTEGER UNIQUE ON CONFLICT REPLACE," 
          + JEDI_USER_ID + " INTEGER," 
          + JEDI_ACCOUNT_NUMBER + " TEXT," 
          + JEDI_EMAIL + " TEXT," 
          + JEDI_PIN + " TEXT,"  
          + JEDI_FIRST_NAME + " TEXT,"  
          + JEDI_LAST_NAME +  " TEXT,"
          + JEDI_AGENT +  " INTEGER,"
          + JEDI_AGENT_STATUS +  " TEXT,"
          + JEDI_DND + " INTEGER,"
          + JEDI_DND_STATUS + " TEXT,"
          + JEDI_FREE + " INTEGER,"
          + JEDI_ACCESS_LEVEL + " TEXT,"
          + JEDI_EXTENSION_TYPE + " TEXT,"
          + JEDI_SYSTEM_EXTENSION + " INTEGER,"
          + JEDI_SERVICE_VERSION + " INTEGER," 
          + JEDI_TIER_SETTINGS + " INTEGER," 
          + JEDI_TIER_SERVICE_TYPE + " TEXT,"
          + JEDI_EXT_MOD_COUNTER + " INTEGER," 
          + JEDI_EXT_MOD_COUNTER_TEMP + " INTEGER," 
          + JEDI_MSG_MOD_COUNTER + " INTEGER," 
          + JEDI_MSG_MOD_COUNTER_TEMP + " INTEGER," 
          + JEDI_SETUP_WIZARD_STATE + " TEXT,"
          + JEDI_EXPRESS_SETUP_MOBILE_URL + " TEXT,"
          + JEDI_TRIAL_EXPIRATION_STATE + " TEXT,"
          + JEDI_DAYS_TO_EXPIRE + " INTEGER,"
          + JEDI_BRAND_ID + " TEXT,"
          + HTTPREG_INSTANCE_ID + " TEXT," 
          + RCM_LOGIN_NUMBER + " TEXT," 
          + RCM_LOGIN_EXT + " TEXT," 
          + RCM_PASSWORD + " TEXT," 
          + RCM_CALLER_ID + " INTEGER," 
          + RCM_RINGOUT_MODE + " INTEGER,"  
          + RCM_RINGOUT_ANOTHER_PHONE + " TEXT," 
          + RCM_CUSTOM_NUMBER + " TEXT," 
          + RCM_CONFIRM_CONNECTION + " INTEGER,"
          + RCM_LAST_CALL_NUMBER + " TEXT," 
          + RCM_MAILBOX_STATE + " INTEGER," 
          + RCM_LAST_SYNC + " INTEGER,"
          + RCM_LAST_LOADED_MSG_ID + " INTEGER,"
          + RCM_LAST_COMPLETE_SETUP_REQ + " INTEGER,"
          + RCM_LAST_EXPIRED_REQ + " INTEGER,"
          + RCM_VOIP_SETTINGS + " INTEGER DEFAULT " + RCMConfig.VOIP_DEFAULT_SETTINGS + ","
          + RCM_MESSAGE_CHECK_INTERVAL + " INTEGER DEFAULT " + RCMConstants.SERVICE_INTERVAL + ","
          + RCM_URI_SIP_PROVIDER + " TEXT DEFAULT \'" + BUILD.URI.URI_SIP_PROVIDER+ "\' ," 
          + RCM_URI_SIP_OUTBOUND_PROXY + " TEXT DEFAULT \'" + BUILD.URI.URI_SIP_OUTBOUND_PROXY+ "\' ,"
          + RCM_VOIP_WHATS_NEW + " INTEGER DEFAULT 0,"
          + RCM_VOIP_HTTPREG_SIPFLAGS + " INTEGER DEFAULT 0" 
          + ");";


        @Override
        String getName() {
            return TABLE_NAME;
        }
        
        @Override
        void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_STMT);
        }
    }

    
    public static final class CallerIDsTable extends RCMDbTable implements RCMColumns, BaseColumns {
        
        private CallerIDsTable() {};
        private static final CallerIDsTable sInstance = new CallerIDsTable();
        static CallerIDsTable getInstance() {
            return sInstance;
        }

        private static final String TABLE_NAME = "CALLER_IDS";
       
        /* Columns */
        //public static final String JEDI_CNAM_NAME = "cnamName";             //TEXT     //DO WE NEED THIS???   
        public static final String JEDI_NUMBER = "number";                  //TEXT
        public static final String JEDI_USAGE_TYPE = "usageType";           //TEXT

        /* JEDI_USAGE_TYPE value for Main Number */
        public static final String USAGE_TYPE_MAIN_NUMBER = "MainNumber";
        
        private static final String CREATE_TABLE_STMT =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
          + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
          + MAILBOX_ID + " INTEGER," 
          //+ JEDI_CNAM_NAME + " TEXT," 
          + JEDI_NUMBER + " TEXT," 
          + JEDI_USAGE_TYPE + " TEXT"  
          + ");";

        
        @Override
        String getName() {
            return TABLE_NAME;
        }
        
        @Override
        void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_STMT);
        }
    }

    
    public static final class FwNumbersTable extends RCMDbTable implements RCMColumns, BaseColumns {
        
        private FwNumbersTable() {}
        private static final FwNumbersTable sInstance = new FwNumbersTable();
        static FwNumbersTable getInstance() {
            return sInstance;
        }

        private static final String TABLE_NAME = "FW_NUMBERS";

        /* Columns */
        public static final String JEDI_NAME = "name";                  //TEXT          //!Added for legacy code compatibility (AccountInfoPOJO) Do we realy need this?                 
        public static final String JEDI_NUMBER = "number";              //TEXT                  
        public static final String JEDI_ORDER_BY = "orderBy";           //INTEGER (int) //!Added for legacy code compatibility (AccountInfoPOJO) Do we realy need this?                  
        public static final String JEDI_TYPE = "type";                  //TEXT

        private static final String CREATE_TABLE_STMT =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
          + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
          + MAILBOX_ID + " INTEGER," 
          + JEDI_NAME + " TEXT," 
          + JEDI_NUMBER + " TEXT," 
          + JEDI_ORDER_BY + " TEXT," 
          + JEDI_TYPE + " TEXT"  
          + ");";


        @Override
        String getName() {
            return TABLE_NAME;
        }
        
        @Override
        void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_STMT);
        }
    }

    
    public static final class ExtensionsTable extends RCMDbTable implements RCMColumns, BaseColumns {
        
        private ExtensionsTable() {};
        private static final ExtensionsTable sInstance = new ExtensionsTable();
        static ExtensionsTable getInstance() {
            return sInstance;
        }

        private static final String TABLE_NAME = "EXTENSIONS";
        
        /* Columns */
        public static final String JEDI_EMAIL = "email";                    //TEXT
        public static final String JEDI_FIRST_NAME = "firstName";           //TEXT
        public static final String JEDI_MIDDLE_NAME = "middleName";         //TEXT
        public static final String JEDI_LAST_NAME = "lastName";             //TEXT
        public static final String JEDI_MAILBOX_ID_EXT = "ext_mailboxId";   //INTEGER (long)
        public static final String JEDI_PIN = "pin";                        //TEXT
        //public static final String JEDI_TIER_SETTINGS = "tierSettings";     //TEXT => //INTEGER (long)
        public static final String JEDI_ADDRESS_LINE_1 = "addressLine1";    //TEXT
        public static final String JEDI_ADDRESS_LINE_2 = "addressLine2";    //TEXT
        public static final String JEDI_CITY = "city";                      //TEXT
        public static final String JEDI_COUNTRY = "country";                //TEXT
        public static final String JEDI_STATE = "state";                    //TEXT
        public static final String JEDI_ZIPCODE = "zipCode";                //TEXT
        public static final String RCM_DISPLAY_NAME = "display_name";       //TEXT
        public static final String RCM_STARRED = "starred";       			//INTEGER

        private static final String CREATE_TABLE_STMT =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
          + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
          + MAILBOX_ID + " INTEGER," 
          + JEDI_EMAIL + " TEXT," 
          + JEDI_FIRST_NAME + " TEXT,"  
          + JEDI_MIDDLE_NAME + " TEXT,"  
          + JEDI_LAST_NAME +  " TEXT,"  
          + JEDI_MAILBOX_ID_EXT +  " INTEGER,"  
          + JEDI_PIN + " TEXT,"  
          //+ JEDI_TIER_SETTINGS + " INTEGER," 
          + JEDI_ADDRESS_LINE_1 + " TEXT," 
          + JEDI_ADDRESS_LINE_2 + " TEXT," 
          + JEDI_CITY + " TEXT," 
          + JEDI_COUNTRY + " TEXT," 
          + JEDI_STATE + " TEXT," 
          + JEDI_ZIPCODE + " TEXT,"  
          + RCM_DISPLAY_NAME + " TEXT," 
      	  + RCM_STARRED +" INTEGER"
          + ");";


        @Override
        String getName() {
            return TABLE_NAME;
        }
        
        @Override
        void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_STMT);
        }
    }

    
    public static final class FavoritesTable extends RCMDbTable implements RCMColumns, BaseColumns {

        private FavoritesTable() {};
        private static final FavoritesTable sInstance = new FavoritesTable();
        static FavoritesTable getInstance() {
            return sInstance;
        }

        
        private static final String TABLE_NAME = "FAVORITES";
        
        /* Columns */
        public static final String DISPLAY_NAME = "display_name";
        public static final String NORMALIZED_NUMBER = "normalized_number";
        public static final String ORIGINAL_NUMBER = "original_number";
        public static final String TYPE = "type";
        public static final String LABEL = "label";
        public static final String CONTACT_ID = "contact_id";
        public static final String PHONE_ID = "phone_id";
        public static final String PHOTO_ID = "photo_id";
        
        private static final String CREATE_TABLE_STMT =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
          + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
          + MAILBOX_ID + " INTEGER," 
          + DISPLAY_NAME + " TEXT," 
          + NORMALIZED_NUMBER + " TEXT,"
          + ORIGINAL_NUMBER + " TEXT,"
          + TYPE + " INTEGER,"
          + LABEL + " TEXT,"
          + CONTACT_ID +  " INTEGER,"  
          + PHONE_ID +  " INTEGER,"  
          + PHOTO_ID +  " INTEGER"  
          + ");";

        
        @Override
        String getName() {
            return TABLE_NAME;
        }
        
        @Override
        void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_STMT);
        }
    }

    
    public static final class CallLogTable extends RCMDbTable implements RCMColumns, BaseColumns {
        
        private CallLogTable() {};
        private static final CallLogTable sInstance = new CallLogTable();
        static CallLogTable getInstance() {
            return sInstance;
        }

        private static final String TABLE_NAME = "CALL_LOG";

        /* Columns */
        public static final String JEDI_CALL_DIRECTION = "callDirection";   //INTEGER ("Incoming", "Outgoing")               
        public static final String JEDI_CALL_TYPE = "callType";             //INTEGER (enum)
        //public static final String JEDI_DELETED = "deleted";                //INTEGER (boolean)
        public static final String JEDI_FROM_NAME = "fromName";             //TEXT
        public static final String JEDI_FROM_PHONE = "fromPhone";           //TEXT
        public static final String JEDI_LENGTH = "length";                  //REAL (double) !!!!
        public static final String JEDI_LOCATION = "location";              //TEXT
        public static final String JEDI_MAILBOX_ID = "caller_mailboxId";    //INTEGER (long)  DO WE NEED THIS???
        public static final String JEDI_PIN = "pin";                        //TEXT
        public static final String JEDI_RECORD_ID = "recordId";             //INTEGER (long)
        public static final String JEDI_START = "start";                    //INTEGER (long)
        public static final String JEDI_STATUS = "status";                  //INTEGER (int)
        public static final String JEDI_TO_NAME = "toName";                 //TEXT
        public static final String JEDI_TO_PHONE = "toPhone";               //TEXT
        
        
		public static final String RCM_FOLDER = "folder";					//INTEGER (LOGS_ALL, LOGS_MISSED)
		public static final String RCM_PHONE_NUMBER = "phone_number";   	//TEXT (Incoming or outgoing number)

		public static final String HAS_MORE_RECORD_ITEM = "has_more_record_item";
	    public static final String IS_VALID_NUMBER = "is_valid_number";
		public static final String NORMALIZED_NUMBER = "normalized_number";
		
		public static final String BIND_HAS_CONTACT = "bind_has_contact";
		public static final String BIND_IS_PERSONAL_CONTACT = "bind_is_personal_contact";
		public static final String BIND_ID = "bind_id";
		public static final String BIND_DISPLAY_NAME = "bind_display_name";
        public static final String BIND_ORIGINAL_NUMBER = "bind_original_number";
        
        /* Call direction enumeration */
        public static final int CALL_DIRECTION_INCOMING = CallLogPOJO.DIRECTION_INCOMING_INT;
        public static final int CALL_DIRECTION_OUTGOING = CallLogPOJO.DIRECTION_OUTGOING_INT;
        
        /* Log type enumeration - stored in RCM_FOLDER */
        public static final int LOGS_ALL = 0;
        public static final int LOGS_MISSED =1;
        
        private static final String CREATE_TABLE_STMT =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
          + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
          + MAILBOX_ID + " INTEGER," 
          + JEDI_CALL_DIRECTION + " INTEGER," 
          + JEDI_CALL_TYPE + " INTEGER,"  
          //+ JEDI_DELETED + " INTEGER,"  
          + JEDI_FROM_NAME +  " TEXT,"  
          + JEDI_FROM_PHONE +  " TEXT,"  
          + JEDI_LENGTH + " REAL,"
          + JEDI_LOCATION + " TEXT," 
          + JEDI_MAILBOX_ID + " INTEGER," 
          + JEDI_PIN + " TEXT," 
          + JEDI_RECORD_ID + " INTEGER," 
          + JEDI_START + " INTEGER," 
          + JEDI_STATUS + " INTEGER," 
          + JEDI_TO_NAME + " TEXT,"  
          + JEDI_TO_PHONE + " TEXT,"
          + NORMALIZED_NUMBER +  " TEXT,"
          + IS_VALID_NUMBER + " INTEGER,"
          
          + RCM_FOLDER + " INTEGER,"
          + HAS_MORE_RECORD_ITEM + " INTEGER,"
          
          + BIND_HAS_CONTACT + " INTEGER,"
          + BIND_IS_PERSONAL_CONTACT + " INTEGER,"
          + BIND_ID  + " INTEGER,"
          + BIND_DISPLAY_NAME +  " TEXT,"
          + BIND_ORIGINAL_NUMBER +  " TEXT"
          + ");";


        @Override
        String getName() {
            return TABLE_NAME;
        }
        
        @Override
        void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_STMT);
        }
    }

    
    public static final class MessagesTable extends RCMDbTable implements RCMColumns, BaseColumns, SyncStatusEnum {
        
        private MessagesTable() {};
        private static final MessagesTable sInstance = new MessagesTable();
        static MessagesTable getInstance() {
            return sInstance;
        }
        
        private static final String TABLE_NAME = "MESSAGES";

        /* Columns */
        public static final String JEDI_MSG_ID = "MsgID";                   //INTEGER (long)
        public static final String JEDI_MSG_TYPE = "MsgType";               //INTEGER (enum) 
        public static final String JEDI_COMP_TYPE = "CompType";             //INTEGER (enum)              
        public static final String JEDI_FILE_EXT = "FileExt";               //TEXT
        public static final String JEDI_BODY_SIZE = "BodySize";             //INTEGER (int): Size of message body in bytes.
        public static final String JEDI_DURATION = "Duration";              //INTEGER (int): Duration of voice message on seconds.
        public static final String JEDI_CREATE_DATE = "CreateDate";         //TEXT
        public static final String JEDI_FROM_PHONE = "FromPhone";           //TEXT
        public static final String JEDI_FROM_NAME = "FromName";             //TEXT
        public static final String JEDI_TO_PHONE = "ToPhone";               //TEXT
        public static final String JEDI_TO_NAME = "ToName";                 //TEXT
        public static final String JEDI_URGENT_STATUS = "Urgent";           //INTEGER (boolean)
        public static final String JEDI_COMMENT = "Comment";                //TEXT
        public static final String JEDI_READ_STATUS = "Status";             //INTEGER (boolean)
        public static final String RCM_SYNC_STATUS = "sync_status";         //INTEGER (enum)
        public static final String RCM_FILE_PATH = "file_path";             //TEXT
        public static final String RCM_LOCALLY_READ = "locally_read";       //INTEGER (boolean)
        public static final String RCM_LOCALLY_DELETED = "locally_deleted"; //INTEGER (boolean)
        public static final String RCM_DURATION_SYNCHRONIZED = "duration_synchronized"; //INTEGER (boolean)
        public static final String RCM_FROM_PHONE_IS_VALID_NUMBER = "FromPhone_isValidNumber";  //INTEGER (-1 : not init, 0 - invalid, 1 - valid)
        public static final String RCM_FROM_PHONE_NORMALIZED_NUMBER = "FromPhone_normalizedNumber";  //TEXT
		public static final String BIND_HAS_CONTACT = "bind_has_contact";	//INTEGER (boolean)
		public static final String BIND_IS_PERSONAL_CONTACT = "bind_is_personal_contact"; //INTEGER (boolean)
		public static final String BIND_ID = "bind_id";                     //INTEGER
		public static final String BIND_DISPLAY_NAME = "bind_display_name"; //TEXT
        public static final String BIND_ORIGINAL_NUMBER = "bind_original_number";  //TEXT

        
        /* Message type enumeration
         * (see http://jira.rcoffice.ringcentral.com/secure/attachment/24330/RCClientSync_Ver_1_4_2008-01-16.pdf
         */
        public static final int MSG_TYPE_ANY = 0;
        public static final int MSG_TYPE_VOICE = 1;
        public static final int MSG_TYPE_FAX = 2;
        public static final int MSG_TYPE_GENERIC = 3;
        public static final int MSG_TYPE_CALL = 4;
        public static final int MSG_TYPE_EXTENSION = 5;
        
        /* Compression type enumeration
         * (see http://jira.rcoffice.ringcentral.com/secure/attachment/24330/RCClientSync_Ver_1_4_2008-01-16.pdf
         */
        public static final int COMP_TYPE_UNKNOWN = 0;
        public static final int COMP_TYPE_GSM = 1;
        public static final int COMP_TYPE_WAVE = 2;
        public static final int COMP_TYPE_AIFF = 3;
        public static final int COMP_TYPE_GIF = 4;
        public static final int COMP_TYPE_TIFF = 5;
        public static final int COMP_TYPE_R0Z = 6;
    

        private static final String CREATE_TABLE_STMT =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
          + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
          + MAILBOX_ID + " INTEGER," 
          + JEDI_MSG_ID + " INTEGER," 
          + JEDI_MSG_TYPE + " INTEGER,"  
          + JEDI_COMP_TYPE + " INTEGER,"  
          + JEDI_FILE_EXT +  " TEXT,"  
          + JEDI_BODY_SIZE +  " INTEGER,"  
          + JEDI_DURATION + " INTEGER,"  
          + JEDI_CREATE_DATE + " TEXT," 
          + JEDI_FROM_NAME + " TEXT,"
          + JEDI_FROM_PHONE + " TEXT," 
          + JEDI_TO_PHONE + " TEXT," 
          + JEDI_TO_NAME + " TEXT," 
          + JEDI_URGENT_STATUS + " INTEGER," 
          + JEDI_COMMENT + " TEXT," 
          + JEDI_READ_STATUS + " INTEGER,"  
          + RCM_SYNC_STATUS + " INTEGER," 
          + RCM_FILE_PATH + " TEXT," 
          + RCM_LOCALLY_READ + " INTEGER," 
          + RCM_LOCALLY_DELETED + " INTEGER,"
          + RCM_DURATION_SYNCHRONIZED + " INTEGER," 
          + RCM_FROM_PHONE_IS_VALID_NUMBER + " INTEGER DEFAULT -1,"
          + RCM_FROM_PHONE_NORMALIZED_NUMBER + " TEXT,"  
          + BIND_HAS_CONTACT + " INTEGER,"
          + BIND_IS_PERSONAL_CONTACT + " INTEGER,"
          + BIND_ID  + " INTEGER,"
          + BIND_DISPLAY_NAME +  " TEXT,"
          + BIND_ORIGINAL_NUMBER +  " TEXT"
          + ");";

        
        @Override
        String getName() {
            return TABLE_NAME;
        }
        
        @Override
        void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_STMT);
        }
    }

    
    public static final class SyncStatusTable extends RCMDbTable implements RCMColumns, BaseColumns, SyncStatusEnum {
        
        private SyncStatusTable() {};
        private static final SyncStatusTable sInstance = new SyncStatusTable();
        static SyncStatusTable getInstance() {
            return sInstance;
        }

        private static final String TABLE_NAME = "SYNC_STATUS";
        
        /* Columns */
        public static final String LOGIN_STATUS = "login_status";     //INTEGER (enum)
        public static final String LOGIN_LASTSYNC = "login_lastsync";       //INTEGER (long)
        public static final String LOGIN_ERROR = "login_error";           //TEXT : error code, example [JEDI, 200]

        public static final String API_VERSION_STATUS = "api_version_status";     //INTEGER (enum)
        public static final String API_VERSION_LASTSYNC = "api_version_lastsync";       //INTEGER (long)
        public static final String API_VERSION_ERROR = "api_version_error";           //TEXT : error code, example [JEDI, 200]

        public static final String ACCOUNT_INFO_STATUS = "account_info_status";     //INTEGER (enum)
        public static final String ACCOUNT_INFO_LASTSYNC = "account_info_lastsync";       //INTEGER (long)
        public static final String ACCOUNT_INFO_ERROR = "account_info_error";           //TEXT : error code, example [JEDI, 200]

        public static final String ALL_CALL_LOGS_STATUS = "all_call_logs_status";     //INTEGER (enum)
        public static final String ALL_CALL_LOGS_LASTSYNC = "all_call_logs_lastsync";       //INTEGER (long)
        public static final String ALL_CALL_LOGS_ERROR = "all_call_logs_error";           //TEXT : error code, example [JEDI, 200]

        public static final String MISSES_CALL_LOGS_STATUS = "misses_call_logs_status";     //INTEGER (enum)
        public static final String MISSES_CALL_LOGS_LASTSYNC = "misses_call_logs_lastsync";       //INTEGER (long)
        public static final String MISSES_CALL_LOGS_ERROR = "misses_call_logs_error";           //TEXT : error code, example [JEDI, 200]

        public static final String CALLER_IDS_STATUS = "caller_ids_status";     //INTEGER (enum)
        public static final String CALLER_IDS_LASTSYNC = "caller_ids_lastsync";       //INTEGER (long)
        public static final String CALLER_IDS_ERROR = "caller_ids_error";           //TEXT : error code, example [JEDI, 200]

        public static final String RINGOUT_CALL_STATUS = "ringout_call_status";     //INTEGER (enum)
        public static final String RINGOUT_CALL_LASTSYNC = "ringout_call_lastsync";       //INTEGER (long)
        public static final String RINGOUT_CALL_ERROR = "ringout_call_error";           //TEXT : error code, example [JEDI, 200]

        public static final String DIRECT_RINGOUT_STATUS = "direct_ringout_status";     //INTEGER (enum)
        public static final String DIRECT_RINGOUT_LASTSYNC = "direct_ringout_lastsync";       //INTEGER (long)
        public static final String DIRECT_RINGOUT_ERROR = "direct_ringout_error";           //TEXT : error code, example [JEDI, 200]

        public static final String CALL_STATUS = "call_status";     //INTEGER (enum)
        public static final String CALL_STATUS_LASTSYNC = "call_status_lastsync";       //INTEGER (long)
        public static final String CALL_STATUS_ERROR = "call_status_error";           //TEXT : error code, example [JEDI, 200]

        public static final String RINGOUT_CANCEL_STATUS = "ringout_cancel_status";     //INTEGER (enum)
        public static final String RINGOUT_CANCEL_LASTSYNC = "ringout_cancel_lastsync";       //INTEGER (long)
        public static final String RINGOUT_CANCEL_ERROR = "ringout_cancel_error";           //TEXT : error code, example [JEDI, 200]

        public static final String DND_STATUS = "dnd_status";     //INTEGER (enum)
        public static final String DND_STATUS_LASTSYNC = "dnd_status_lastsync";       //INTEGER (long)
        public static final String DND_STATUS_ERROR = "dnd_status_error";           //TEXT : error code, example [JEDI, 200]

        public static final String LIST_EXTENSIONS_STATUS = "list_extensions_status";     //INTEGER (enum)
        public static final String LIST_EXTENSIONS_LASTSYNC = "list_extensions_lastsync";       //INTEGER (long)
        public static final String LIST_EXTENSIONS_ERROR = "list_extensions_error";           //TEXT : error code, example [JEDI, 200]
        public static final String SETUP_WIZARD_STATE_STATUS = "setup_wizard_state_status";                       //INTEGER (enum)
        public static final String SETUP_WIZARD_STATE_STATUS_LASTSYNC = "setup_wizard_state_status_lastsync";     //INTEGER (long)
        public static final String SETUP_WIZARD_STATE_STATUS_ERROR = "setup_wizard_state_status_error";           //TEXT : error code, example [JEDI, 200]

        private static final String CREATE_TABLE_STMT =
                "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
                        + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"

                        + LOGIN_STATUS + " INTEGER,"
                        + LOGIN_LASTSYNC + " INTEGER,"
                        + LOGIN_ERROR + " TEXT,"
                        + API_VERSION_STATUS + " INTEGER,"
                        + API_VERSION_LASTSYNC + " INTEGER,"
                        + API_VERSION_ERROR + " TEXT,"
                        + ACCOUNT_INFO_STATUS + " INTEGER,"
                        + ACCOUNT_INFO_LASTSYNC + " INTEGER,"
                        + ACCOUNT_INFO_ERROR + " TEXT,"
                        + ALL_CALL_LOGS_STATUS + " INTEGER,"
                        + ALL_CALL_LOGS_LASTSYNC + " INTEGER,"
                        + ALL_CALL_LOGS_ERROR + " TEXT,"
                        + MISSES_CALL_LOGS_STATUS + " INTEGER,"
                        + MISSES_CALL_LOGS_LASTSYNC + " INTEGER,"
                        + MISSES_CALL_LOGS_ERROR + " TEXT,"
                        + CALLER_IDS_STATUS + " INTEGER,"
                        + CALLER_IDS_LASTSYNC + " INTEGER,"
                        + CALLER_IDS_ERROR + " TEXT,"
                        + RINGOUT_CALL_STATUS + " INTEGER,"
                        + RINGOUT_CALL_LASTSYNC + " INTEGER,"
                        + RINGOUT_CALL_ERROR + " TEXT,"
                        + DIRECT_RINGOUT_STATUS + " INTEGER,"
                        + DIRECT_RINGOUT_LASTSYNC + " INTEGER,"
                        + DIRECT_RINGOUT_ERROR + " TEXT,"
                        + CALL_STATUS + " INTEGER,"
                        + CALL_STATUS_LASTSYNC + " INTEGER,"
                        + CALL_STATUS_ERROR + " TEXT,"
                        + RINGOUT_CANCEL_STATUS + " INTEGER,"
                        + RINGOUT_CANCEL_LASTSYNC + " INTEGER,"
                        + RINGOUT_CANCEL_ERROR + " TEXT,"
                        + DND_STATUS + " INTEGER,"
                        + DND_STATUS_LASTSYNC + " INTEGER,"
                        + DND_STATUS_ERROR + " TEXT,"
                        + LIST_EXTENSIONS_STATUS + " INTEGER,"
                        + LIST_EXTENSIONS_LASTSYNC + " INTEGER,"
                        + LIST_EXTENSIONS_ERROR + " TEXT,"
                        + SETUP_WIZARD_STATE_STATUS + " INTEGER,"
                        + SETUP_WIZARD_STATE_STATUS_LASTSYNC + " INTEGER,"
                        + SETUP_WIZARD_STATE_STATUS_ERROR + " TEXT"
                        + ");";

        
        @Override
        String getName() {
            return TABLE_NAME;
        }
        
        @Override
        void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_STMT);
        }
    }
    
    public static final class AppConfigTable extends RCMDbTable implements BaseColumns {
        
        private AppConfigTable() {};
        private static final AppConfigTable sInstance = new AppConfigTable();
        static AppConfigTable getInstance() {
            return sInstance;
        }

        private static final String TABLE_NAME = "APP_CONFIG";
       
        /* Columns */

        public static final String CONFIG_REVISION = "config_revision";                           //INTEGER           
        public static final String URL_JEDI = "url_jedi";                  						  //TEXT
        public static final String URL_MSGSYNC = "url_msgsync";                    				  //TEXT
        public static final String URL_SIGNUP = "url_signup";                                     //TEXT
        public static final String URL_SETUP = "url_setup";                                       //TEXT
        public static final String URL_SETUP_SIGNUP = "url_setup_signup";                         //TEXT
        public static final String URL_WEBSETTING_BASE = "url_websettings_base";                  //TEXT
        public static final String IS_SETUP_OVERLOAD_ENABLED = "is_setup_overload_enabled";       //INTEGER(BOOLEAN)
        public static final String URL_HTTPREG = "url_httpreg";						  //TEXT
        public static final String URL_SIP_PROVIDER = "url_sip_provider";						  //TEXT
        public static final String URI_SIP_OUTBOUND_PROXY = "uri_sip_outbound_proxy";			  //TEXT
        

        
        private static final String CREATE_TABLE_STMT =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
          + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
          + CONFIG_REVISION + " INTEGER," 
          + URL_JEDI + " TEXT,"
          + URL_MSGSYNC + " TEXT,"
          + URL_SIGNUP + " TEXT,"
          + URL_SETUP + " TEXT,"
          + URL_SETUP_SIGNUP + " TEXT,"
          + URL_WEBSETTING_BASE + " TEXT,"
          + IS_SETUP_OVERLOAD_ENABLED + " INTEGER,"
          + URL_HTTPREG + " TEXT,"
          + URL_SIP_PROVIDER + " TEXT,"
          + URI_SIP_OUTBOUND_PROXY + " TEXT"
          + ");";
        
		private static final String INIT_TABLE_STMT =
            "INSERT INTO " + TABLE_NAME + " (" 
            + CONFIG_REVISION + ',' 
            + URL_JEDI + ','
            + URL_MSGSYNC + ',' 
            + URL_SIGNUP + ','
            + URL_SETUP + ',' 
            + URL_SETUP_SIGNUP + ','
            + URL_WEBSETTING_BASE + ',' 
            + IS_SETUP_OVERLOAD_ENABLED + ','
            + URL_HTTPREG + ',' 
            + URL_SIP_PROVIDER + ',' 
            + URI_SIP_OUTBOUND_PROXY
            + ") VALUES (NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)";

        
        @Override
        String getName() {
            return TABLE_NAME;
        }
        
        @Override
        void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_STMT);
            db.execSQL(INIT_TABLE_STMT);
        }
    }

    public static final class TSCConfigTable extends RCMDbTable implements RCMColumns,BaseColumns{

        private TSCConfigTable(){};
        private static final TSCConfigTable sInstance =new TSCConfigTable();
        static TSCConfigTable getInstance(){
        	return sInstance;
        }
        
        private static final String TABLE_NAME = "TSC_CONFIG";
        
        /* Columns */

        public static final String TSC_ENABLE = "tsc_enable";                           //INTEGER (boolean)           
        public static final String TUNNEL_ADDRESS = "tunnel_address";                  	//TEXT(ip)
        public static final String TUNNEL_PORT = "tunnel_port";                         //INTEGER(port)
        public static final String TUNNEL_MODE = "tunnel_mode";                         //INTEGER(enum[TCP/UDP/dtls/AuthDisable])
        public static final String TSC_REDUNDANCY = "tsc_redundancy";                   //INTEGER(0/1/2)
        public static final String TSC_LOGGING = "tsc_logging";                         //INTEGER(boolean)
        /* other low priority settings */
        public static final String DEBUG = "debug";                                     //INTEGER(boolean)
        public static final String TUNNEL_LOAD_BALANCE = "tunnel_load_balance";         //INTEGER(boolean)
        
        private static final String CREATE_TABLE_STMT =
                "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
              + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
              + TSC_ENABLE + " INTEGER," 
              + TUNNEL_ADDRESS + " TEXT,"
              + TUNNEL_PORT + " INTEGER,"
              + TUNNEL_MODE + " INTEGER,"
              + TSC_REDUNDANCY + " INTEGER,"
              + TSC_LOGGING + " INTEGER,"
              + DEBUG + " INTEGER,"
              + TUNNEL_LOAD_BALANCE + " INTEGER"
              + ");";
            
    		private static final String INIT_TABLE_STMT =
                "INSERT INTO " + TABLE_NAME + " (" 
                + TSC_ENABLE + ',' 
                + TUNNEL_ADDRESS + ','
                + TUNNEL_PORT + ',' 
                + TUNNEL_MODE + ','
                + TSC_REDUNDANCY + ',' 
                + TSC_LOGGING + ','
                + DEBUG + ',' 
                + TUNNEL_LOAD_BALANCE
                + ") VALUES (0,'',0,0,0,0,0,0)";
        
		@Override
		String getName() {
			return TABLE_NAME;
		}

		@Override
		void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_TABLE_STMT);
			db.execSQL(INIT_TABLE_STMT);
		}
    	
    }

    static LinkedHashMap<String, RCMDbTable> sRCMDbTables = new LinkedHashMap<String, RCMDbTable>();
    
    static {
        sRCMDbTables.put(MailboxCurrentTable.getInstance().getName(), MailboxCurrentTable.getInstance());
        sRCMDbTables.put(DeviceAttribTable.getInstance().getName(), DeviceAttribTable.getInstance());
        sRCMDbTables.put(UserCredentialsTable.getInstance().getName(), UserCredentialsTable.getInstance());
        sRCMDbTables.put(ServiceInfoTable.getInstance().getName(), ServiceInfoTable.getInstance());
        sRCMDbTables.put(AccountInfoTable.getInstance().getName(), AccountInfoTable.getInstance());
        sRCMDbTables.put(TSCConfigTable.getInstance().getName(), TSCConfigTable.getInstance());
        sRCMDbTables.put(CallerIDsTable.getInstance().getName(), CallerIDsTable.getInstance());
        sRCMDbTables.put(FwNumbersTable.getInstance().getName(), FwNumbersTable.getInstance());
        sRCMDbTables.put(ExtensionsTable.getInstance().getName(), ExtensionsTable.getInstance());
        sRCMDbTables.put(FavoritesTable.getInstance().getName(), FavoritesTable.getInstance());
        sRCMDbTables.put(CallLogTable.getInstance().getName(), CallLogTable.getInstance());
        sRCMDbTables.put(MessagesTable.getInstance().getName(), MessagesTable.getInstance());
        sRCMDbTables.put(SyncStatusTable.getInstance().getName(), SyncStatusTable.getInstance());
        sRCMDbTables.put(AppConfigTable.getInstance().getName(), AppConfigTable.getInstance());
    };
}
