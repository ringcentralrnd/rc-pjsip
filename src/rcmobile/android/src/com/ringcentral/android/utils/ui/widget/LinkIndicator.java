/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils.ui.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.utils.NetworkUtils;
import com.ringcentral.android.utils.NetworkUtils.NetworkState;

public class LinkIndicator extends ImageView {
    private static final String TAG = "[RC]LinkIndicator";
    private StateChangeReceiver stateChangeReceiver = new StateChangeReceiver();

    private void init(Context context) {
        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "LinkIndicator2() context = " + context.toString());
        }
    }

    public LinkIndicator(Context context) {
        super(context);
        this.setImageResource(getStatusIcon(super.getContext()));
        this.getContext().registerReceiver(stateChangeReceiver,
                new IntentFilter(RCMConstants.ACTION_NETWORK_STATE_CHANGED));
        init(context);
    }

    public LinkIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setImageResource(getStatusIcon(super.getContext()));
        this.getContext().registerReceiver(stateChangeReceiver,
                new IntentFilter(RCMConstants.ACTION_NETWORK_STATE_CHANGED));
        init(context);

    }

    public LinkIndicator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setImageResource(getStatusIcon(super.getContext()));
        this.getContext().registerReceiver(stateChangeReceiver,
                new IntentFilter(RCMConstants.ACTION_NETWORK_STATE_CHANGED));
        init(context);
    }

    public class StateChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            LinkIndicator.this.setImageResource(getStatusIcon(context));
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.setImageResource(getStatusIcon(super.getContext()));
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        try {
            this.getContext().unregisterReceiver(stateChangeReceiver);
        } catch (IllegalArgumentException e) {
            if (LogSettings.QA) {
                QaLog.e(TAG, "onDetachedFromWindow()", e);
            }
        }
    }

    private int getStatusIcon(Context context) {
        NetworkState state = NetworkUtils.getNetworkState(context);
        if (LogSettings.MARKET && (state != NetworkState.SERVERREQUEST)) {
            MktLog.d(TAG, "Net state: " + NetworkUtils.getNetworkState(context) + 
                    " [" + NetworkUtils.getDetailedNetState(context) + "]");
        }
        switch (state) {
            case SERVERREQUEST:
            case FULL:
            case WIFI:
            case MOBILE:
                return R.drawable.bg_fully_transparent;
            default:
                return R.drawable.ic_no_network;
        }
    }
}
