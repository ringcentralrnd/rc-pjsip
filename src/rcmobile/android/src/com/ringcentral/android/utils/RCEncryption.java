package com.ringcentral.android.utils;

public class RCEncryption {
    public static final String SIGNUP_ENCRPYPT_PREFIX = "CDEP1";
    final static public int START = 234;
    final static public int MULT = 34;
    final static public int ADD = 902;

    
    public static String decrypt(String ciphertext){
    	return decrypt(ciphertext, START, MULT, ADD);
    }
    
    public static String decrypt(String ciphertext, int start, int mult, int add) {
        ciphertext = hex2str(ciphertext);
        StringBuffer plaintext = new StringBuffer();
        long indexStart = start;
        for (int index = 0; index < ciphertext.length(); index++) {
            char decryptedChar = (char) ((int) ciphertext.charAt(index) ^ ((indexStart >> 8) & 0xFF));
            indexStart = ((ciphertext.charAt(index) + indexStart) * mult + add) & 0xFFF;
            plaintext.append(decryptedChar);
        }
        return plaintext.toString();
    }

    private static String hex2str(String hexStr) {
        StringBuffer result = new StringBuffer();
        byte[] bytes = hexStr.getBytes();
        for (int index = 0; index < hexStr.length(); index += 2) {
            String hex = new String(bytes, index, 2);
            char c = (char) Integer.valueOf(hex, 16).intValue();
            result.append(c);
        }
        return result.toString();
    }

    public static String encrypt(String plaintext) {
        return encrypt(plaintext, START, MULT, ADD);
    }

    public static String encrypt(String plaintext, int start, int mult, int add) {
        int plaintextLength = plaintext.length();
        StringBuffer ciphertext = new StringBuffer();
        long indexStart = start;
        for (int index = 0; index < plaintextLength; index++) {
            char encryptedChar = (char) ((int) plaintext.charAt(index) ^ (indexStart >> 8 & 0xFF));
            indexStart = ((encryptedChar + indexStart) * mult + add) & 0xFFF;
            ciphertext.append(Integer.toHexString((int) encryptedChar).toUpperCase());
        }
        return ciphertext.toString();
    }
}
