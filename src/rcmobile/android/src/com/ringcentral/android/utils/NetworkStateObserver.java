package com.ringcentral.android.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.utils.NetworkUtils.NetworkState;

public class NetworkStateObserver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		RingCentralApp rcApp = (RingCentralApp) context.getApplicationContext();
		NetworkState networkState = NetworkUtils.getNetworkState(context);
		rcApp.setNetworkState(networkState);
	}

}
