package com.ringcentral.android.utils.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class RODialpadBlueWidget extends ViewGroup {
    private final int mColumns = 3;
    private final int mPaddingTop = 0;
    private final int mPaddingLeft = 0;
    private final int mPaddingRight = 0;
    private final int mPaddingBottom = 0;

    public RODialpadBlueWidget(Context context) {
        super(context);
    }

    public RODialpadBlueWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RODialpadBlueWidget(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int y = mPaddingTop;
        int rows = getRows();
        int x2 = (getWidth() - mPaddingLeft - mPaddingRight) / mColumns;
        int y2 = (getHeight() - mPaddingBottom - mPaddingTop) / rows;
        int cW = x2;
        int cH = y2;
        final int xD = (getWidth() - mColumns * x2);
        final int yD = (getHeight() - y2 * rows);
        for (int row = 0; row < rows; row++) {
            int x = mPaddingLeft;
            cH = y2;
            if ((yD != 0) && (row == (rows - 1))) {
                cH = y2 + yD;
            }
            for (int col = 0; col < mColumns; col++) {
                int area = col + (row * mColumns);
                if (area >= getChildCount()) {
                    break;
                }
                cW = x2;
                if (xD == 2) {
                    if ((col == 0) || (col == 2)) {
                        cW = x2 + 1;
                    }
                }
                if ((xD == 1) && (col == 1)) {
                    cW = x2 + 1;
                }

                getChildAt(area).layout(x, y, cW + x, cH + y);

                x += cW;
            }
            y += cH;
        }
    }

    private int getRows() {
        return (getChildCount() + mColumns - 1) / mColumns;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int w = mPaddingLeft + mPaddingRight;
        int h = mPaddingTop + mPaddingBottom;
        View c = getChildAt(0);
        c.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        int cW = c.getMeasuredWidth();
        int cH = c.getMeasuredHeight();
        for (int i = 1; i < getChildCount(); i++) {
            getChildAt(i).measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        }
        w += mColumns * cW;
        w = resolveSize(w, widthMeasureSpec);
        h += getRows() * cH;
        h = resolveSize(h, heightMeasureSpec);
        setMeasuredDimension(w, h);
    }
}
