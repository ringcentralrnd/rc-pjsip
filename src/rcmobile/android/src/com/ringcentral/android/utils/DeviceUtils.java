/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Scanner;

import android.content.Context;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.utils.Compatibility;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.DeviceAttribTable;

public class DeviceUtils {

    private static final boolean DEBUG = false;
    private static final String TAG = "[RC]DeviceUtils";
    
    private static final String CMD_CAT = "/system/bin/cat";
    public static final String SYS_FILE_MAX_FREQ = "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq";
    public static final String SYS_FILE_CURR_FREQ = "/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq";

    private static final String NO_SIM_CARD = "No SIM";

    
    public static String getLine1Number(Context ctx) {
		return ((TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE)).getLine1Number();
	}

    
    /**
     * Check that SIM card has not been changed
     * Store device IMSI if changed or not yet in DB (first launch) 
     * @param ctx
     * @return true - if IMSI not changed
     *         false - if new IMSI changed or initially obtained 
     */
    public static boolean checkDeviceIMSI(Context ctx) {
        String deviceIMSI = RCMProviderHelper.getDeviceIMSI(ctx);
        String IMSI = ((TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE)).getSubscriberId();
 
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "checkDeviceIMSI(): Stored IMSI: " + deviceIMSI + "; current IMSI: " + IMSI);
        }
        
        if (IMSI == null) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "checkDeviceIMSI(): No SIM card!");
            }
            IMSI = NO_SIM_CARD;
        }
        
        if (TextUtils.isEmpty(deviceIMSI) || !deviceIMSI.equals(IMSI)) {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "checkDeviceIMSI(): First launch or SIM card changed: save new IMSI; return false");
            }
            
            RCMProviderHelper.saveDeviceIMSI(ctx, IMSI);
            return false;
        }
        
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "checkDeviceIMSI(): return true");
        }
        return true;
    }


    public static void setDeviceNumber(Context context, String number) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "setDeviceNumber(): " + (number == null ? "null":number));
        }
        RCMProviderHelper.updateSingleValue(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_DEVICE_PHONE_NUMBER, number, null);
        PhoneUtils.setStationLocation(number, RCMProviderHelper.getLoginNumber(context), true, context);
    }
        
    public static String getDeviceNumber(Context context) {
        return RCMProviderHelper.simpleQuery(context, RCMProvider.DEVICE_ATTRIB, DeviceAttribTable.RCM_DEVICE_PHONE_NUMBER);
    }

	public static int getDisplayOrientation(Context context) {
        int orientation = context.getResources().getConfiguration().orientation;
        if (DEBUG && LogSettings.ENGINEERING)
            EngLog.i(TAG, "getDisplayOrientation() #1: orientation = " + orientation);
        
        if (orientation == Configuration.ORIENTATION_UNDEFINED) {
            orientation = calcDisplayOrientation(context);
    
            if (DEBUG && LogSettings.ENGINEERING)
                EngLog.i(TAG, "getDisplayOrientation() #2: orientation = " + orientation);
        }
        return orientation;
    }

    public static int calcDisplayOrientation(Context context) {
        if (DEBUG && LogSettings.ENGINEERING)
            EngLog.i(TAG, "calcDisplayOrientation() #1");
    
        int orientation;
        DisplayMetrics disp_metrics = context.getResources().getDisplayMetrics();
        
        if (disp_metrics.heightPixels > disp_metrics.widthPixels)
            orientation = Configuration.ORIENTATION_PORTRAIT;
        else if (disp_metrics.heightPixels < disp_metrics.widthPixels)
            orientation = Configuration.ORIENTATION_LANDSCAPE;
        else
            orientation = Configuration.ORIENTATION_SQUARE;
    
        if (DEBUG && LogSettings.ENGINEERING)
            EngLog.i(TAG, "calcDisplayOrientation() #2: orientation = " + orientation);
        return orientation;
    }
    
    public static Integer getFrequencyKHz(String type) {
    	Integer result = null;
    	InputStream is = null;
    	String strCommandResult = null;
    	if(LogSettings.ENGINEERING) {
			EngLog.v(TAG, "getFrequency(), type : " + type);
		}
    	try {
    		final Process process = new ProcessBuilder(CMD_CAT, type).start();
    		is = process.getInputStream();
    		final StringBuilder sb = new StringBuilder();
    		
    		final Scanner sc = new Scanner(is);
    		while(sc.hasNextLine()) {
    			sb.append(sc.nextLine());
    		}
    		strCommandResult = sb.toString();	    		
    		result = Integer.parseInt(strCommandResult);
    	} catch (IOException ioEx) {
			if(LogSettings.ENGINEERING){
				EngLog.e(TAG, "getFrequency(), IOException ", ioEx);
			}
		} catch (NumberFormatException numEx) {
			if(LogSettings.ENGINEERING){
				EngLog.e(TAG, "getFrequency(), NumericEception, command : " + strCommandResult, numEx);
			}
			result = null;
		} finally {
			if(is != null) {
				try {
					is.close();
				} catch (IOException e) {
					if(LogSettings.ENGINEERING){
						EngLog.e(TAG, "getFrequency(), exception in finally");
					}
				}
				is = null;
			}
		}
		if(LogSettings.ENGINEERING) {
			EngLog.v(TAG, "getFrequency(), result : " + result);
		}
    	return result;
    }

    public static boolean isNecessaryUseTimeout() {
        return Compatibility.getApiLevel() <= 4;
    }
    
    public static boolean isHeadsetConnected() {
		boolean result = false;
		final AudioManager audioManager = (AudioManager) RingCentralApp.getContextRC().getSystemService(Context.AUDIO_SERVICE);
		if (Compatibility.getApiLevel() >= 5) {
			try {
				Method isWidredHeadsetOnMethod = ReflectionHelper.getDeclaredMethod(audioManager.getClass(),"isWiredHeadsetOn");
				isWidredHeadsetOnMethod.setAccessible(true);
				Object o = isWidredHeadsetOnMethod.invoke(audioManager);
				result = (Boolean) o;
			} catch (Exception ex) {
				if (LogSettings.MARKET) {
					MktLog.e(TAG, " isHeadsetConnected ", ex);
				}
				result = false;
			}
		}
		return result;
	}
}
