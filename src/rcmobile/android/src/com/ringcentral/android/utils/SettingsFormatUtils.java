/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils;

import java.util.TimeZone;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;
import android.text.format.DateFormat;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;

public class SettingsFormatUtils {
    private static final String TAG = "[RC] SettingsFormatUtils";
    private static String mTimeFormatString = "h:mm a";
    private static String mDateFormatString = "MM/dd/yyyy";
	private static String mMessageTimeFormatString = "hh:mm:ss";
    private static String mMessageDateFormatString = "EEE  MMM  dd";
    
    private static TimeZone mTimeZone = java.util.Calendar.getInstance().getTimeZone();

    private Context mContext;
    private DateTimeContentObserver mDateTimeContentObserver;

    public void destroy() {
        if (mDateTimeContentObserver != null) {
            mContext.getContentResolver().unregisterContentObserver(mDateTimeContentObserver);
        }
        mDateTimeContentObserver = null;
        mContext = null;
    }
    
    public SettingsFormatUtils(Context context) {
        mContext = context;
        mDateTimeContentObserver = new DateTimeContentObserver(null);
        mContext.getContentResolver().registerContentObserver(
                Settings.System.getUriFor(Settings.System.DATE_FORMAT),
                true, mDateTimeContentObserver);

        mContext.getContentResolver().registerContentObserver(Settings.System.getUriFor(
                Settings.System.TIME_12_24),
                true, mDateTimeContentObserver);

        updateData(mContext);
    }

    public static String getTimeFormat() {
        return mTimeFormatString;
    }

    public static String getDateFormat() {
        return mDateFormatString;
    }
    
    public static String getMessageTimeFormatString() {
		return mMessageTimeFormatString;
	}
    

	public static String getMessageDateFormatString() {
		return mMessageDateFormatString;
	}

    private class DateTimeContentObserver extends ContentObserver {

        public DateTimeContentObserver(Handler handler) {
            super(handler);
        }

        @Override
        public boolean deliverSelfNotifications() {
            return false;
        }

        @Override
        public void onChange(boolean arg0) {
            super.onChange(arg0);
            updateData(mContext);
        }
    }

    private void updateData(Context context) {
        if (DateFormat.is24HourFormat(context)) {
            mTimeFormatString = "H:mm";
        } else {
            mTimeFormatString = "h:mm a";
        }
        String format = Settings.System.getString(context.getContentResolver(), Settings.System.DATE_FORMAT);
        if (format != null) {
            if (!format.trim().equals("")) {
                mDateFormatString = format;
            }
        }
        mTimeZone = java.util.Calendar.getInstance().getTimeZone();

        if (LogSettings.ENGINEERING) {
            EngLog.i(TAG, "updateData(): timeForamt: " + mTimeFormatString + " dateFormat: " + mDateFormatString);
            EngLog.i(TAG, "updateData(): mTimeZone: " + mTimeZone);
        }
    }

}
