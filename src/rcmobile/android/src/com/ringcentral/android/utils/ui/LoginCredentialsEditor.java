/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils.ui;

import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;

import com.ringcentral.android.phoneparser.PhoneNumber;
import com.ringcentral.android.utils.PhoneUtils;


public class LoginCredentialsEditor {

    EditText mPhoneField;
    EditText mExtField;
    EditText mPasswordField;
    
    AccountWatcher mPhoneWatcher;
    AccountWatcher mExtWatcher;
    PasswordWatcher mPasswordWatcher;
    
    private boolean mPasswordReset;
    

    public LoginCredentialsEditor(EditText phoneField, EditText extField, EditText passwordField) {
        mPhoneField = phoneField;
        mExtField = extField;   
        mPasswordField = passwordField;
    }
    
    public void init(String number, String ext, String password) {

        if (!TextUtils.isEmpty(password)) {
            mPhoneWatcher = new AccountWatcher();
            mExtWatcher = new AccountWatcher();
            mPasswordWatcher = new PasswordWatcher();
            mPasswordReset = false;
        } else {
            mPasswordReset = true;
        }
        
        mPhoneField.setText(number);
        if (mPhoneWatcher != null) {
            mPhoneField.addTextChangedListener(mPhoneWatcher);
        }
        mPhoneField.setOnFocusChangeListener(new OnFocusChangeListener() {
            
            public void onFocusChange(View v, boolean hasFocus) {
                EditText txt = (EditText) v;
                PhoneNumber p = PhoneUtils.getPhoneNumber(txt.getText().toString(), new PhoneNumber());

                    if (hasFocus) {
                        if (p != null) {
                            if (mPhoneWatcher != null) {
                                mPhoneWatcher.setActive(false);
                            }
                            txt.setText(p.numRaw);
                            Selection.setSelection(txt.getEditableText(), p.numRaw.length());
                        }
                        if (mPhoneWatcher != null && !mPasswordReset) {
                            mPhoneWatcher.setActive(true);
                        }
                    } else {
                        if (mPhoneWatcher != null) {
                            mPhoneWatcher.setActive(false);
                        }
                        if (p != null) {
                            txt.setText(p.localCanonical);
                        }
                    }
                }
            }
        );
        
        mExtField.setText(ext);
        if (mExtWatcher != null) {
            mExtField.addTextChangedListener(mExtWatcher);
            mExtWatcher.setActive(true);
        }

        mPasswordField.setText(password);
        if (mPasswordWatcher != null) {
            mPasswordField.addTextChangedListener(mPasswordWatcher);
            mPasswordWatcher.setActive(true);
        }
    }

    
    public void reset() {
        if (!TextUtils.isEmpty(mPasswordField.getText())) {
            mPasswordReset = false;
        }
    }

    
    private abstract class CredentialsWatcher implements TextWatcher {

        protected boolean mActive = false;

        void setActive(boolean state) {
            mActive = state;
        }
        
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            return;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            return;
        }
    }

    
    private class AccountWatcher extends CredentialsWatcher {

        @Override
        public void afterTextChanged(Editable s) {
            if (mActive && !mPasswordReset) {
                mPasswordField.setText("");
                mPasswordReset = true;
                mActive = false;
            }
        }
    }

    
    private class PasswordWatcher extends CredentialsWatcher {

        @Override
        public void afterTextChanged(Editable s) {
            if (mActive && s.length() == 0) {
                mPasswordReset = true;
                mActive = false;
            }
        }
    }
    
}
