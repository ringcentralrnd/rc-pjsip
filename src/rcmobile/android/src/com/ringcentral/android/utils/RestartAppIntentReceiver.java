package com.ringcentral.android.utils;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConstants;

public class RestartAppIntentReceiver extends BroadcastReceiver {
	private static final String TAG = "[RC]RestartAppIntentReceiver";
	
	private Activity activity;

	/**
	 * Not really  good approach to restart application and kill activities that are on the way to logout screen
	 * @param activity
	 */
	private RestartAppIntentReceiver(Activity activity){
		super();
		this.activity = activity;
	}		
	
	public static RestartAppIntentReceiver registerRestartEvent(Activity activity){
		RestartAppIntentReceiver receiver = new RestartAppIntentReceiver(activity);
		IntentFilter intentFilter = new IntentFilter(RCMConstants.ACTION_RESTART_APP);
		activity.registerReceiver(receiver, intentFilter);
		return receiver;
	}
	
    @Override
    public void onReceive(Context context, Intent intent) {
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "Restart notification received for activity " + activity.getLocalClassName());
        }
        if(activity!= null){
        	activity.finish();
        	destroy();
        }
    }
    
    public void destroy(){
    	if(activity != null){
    		activity.unregisterReceiver(this);
    		activity = null;
    	}    	
    }
}
