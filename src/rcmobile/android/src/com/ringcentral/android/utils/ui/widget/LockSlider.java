package com.ringcentral.android.utils.ui.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.ToggleButton;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.utils.DeviceUtils;

public class LockSlider extends ViewGroup {
	private static final String TAG = "[RC]LockSlider";
	
	private static final int STATE_NORMAL = 0;
	private static final int STATE_PRESSED = 1;
	
	private static final int DELTA_Y_REPAINT = 5;
	
	private ToggleButton mLockBar;
	private OnUnlockListener mOnUnlockListener;
	
	private boolean isTracking = false;
	private boolean isTriggered = false;
	

	public LockSlider(Context context) {
		super(context);
		initSlider(context);
	}	
	
	private void initSlider(Context context){
		if(LogSettings.ENGINEERING){
			EngLog.v(TAG, "initSlider()");
		}
		final String empty = "";
		
		mLockBar = new ToggleButton(getContext());
        int image_id = (DeviceUtils.getDisplayOrientation(context) == Configuration.ORIENTATION_LANDSCAPE ?
        		  R.drawable.bar_locker_land              //LANDSCAPE
                : R.drawable.bar_locker_port);            //PORTRAIT or SQUARE
		mLockBar.setBackgroundResource(image_id);		
		mLockBar.setTextColor(R.color.voip_call_screen_locker_text_color);
		mLockBar.setTextSize(getContext().getResources().getDimension(R.dimen.voip_call_screen_unlock_text_size));
		mLockBar.setText(empty);
		mLockBar.setTextOn(empty);
		mLockBar.setTextOff(empty);		
		mLockBar.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));				
		addView(mLockBar);
	}
	
	@Override
	protected void onDetachedFromWindow() {		
		super.onDetachedFromWindow();
		if(LogSettings.ENGINEERING){
			EngLog.v(TAG, "onDetachedFromWindow()");
		}
		mLockBar = null;
		mOnUnlockListener = null;		
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {		
		int widthSpecSize = MeasureSpec.getSize(widthMeasureSpec);
		final float density = getResources().getDisplayMetrics().density;
		final int barWidth = (int) (density * mLockBar.getBackground().getIntrinsicWidth() + 0.5f);
		final int barHeight = (int) (density * mLockBar.getBackground().getIntrinsicHeight() + 0.5f);
		final int width = Math.max(widthSpecSize, barWidth);
		setMeasuredDimension(width, barHeight);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		if (!changed) {
			return;
		}				
		mLockBar.layout(0, 0, (r - l), (b - t));						
		mLockBar.invalidate();
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		final Rect frame = new Rect(
				mLockBar.getLeft(),
				mLockBar.getTop(), 
				mLockBar.getRight(),
				(mLockBar.getTop()+mLockBar.getBackground().getIntrinsicHeight()));
		
		boolean isHit = frame.contains((int) event.getX(), (int) event.getY());
		
		if (!isTracking && !isHit) {
			return false;
		}

		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN: {
				isTracking = true;
				isTriggered = false;
				setState(STATE_PRESSED);
				break;
			}
		}
		return true;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (isTracking) {		
			final float y = event.getY();
			switch (event.getAction()) {
				case MotionEvent.ACTION_MOVE:			
					int deltaY = (int) y - mLockBar.getTop() - (mLockBar.getBackground().getIntrinsicHeight() / 2);
					if(deltaY > DELTA_Y_REPAINT){
						mLockBar.offsetTopAndBottom(deltaY);				
						invalidate();
					}				
					if (!isTriggered && ((mLockBar.getTop() + mLockBar.getBackground().getIntrinsicHeight() )> getHeight())) { // reached end of the screen
						isTriggered = true;
						isTracking = false;								
						onUnlocked();
					}
					if ((y <= mLockBar.getTop()+mLockBar.getBackground().getIntrinsicHeight()) && (y >= mLockBar.getTop())) { // in bar coordinates
						break;
					}
				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_CANCEL:
					isTracking = false;
					isTriggered = false;
					resetView();
					break;
			}
		}
		return isTracking || super.onTouchEvent(event);
	}		
	

	public void setUnlockListener(OnUnlockListener mUnlockListener) {
		this.mOnUnlockListener = mUnlockListener;
	}

	private void setState(int state){
		mLockBar.setChecked(state == STATE_PRESSED);		
	}
	
	private void onUnlocked(){
		resetView();
		if(LogSettings.ENGINEERING){
			EngLog.v(TAG, "onUnlocked() notifying listeners");
		}
		if(mOnUnlockListener != null){			
			mOnUnlockListener.onUnLocked();
		}			
	}
	
	private void resetView() {
		setState(STATE_NORMAL);
		onLayout(true, getLeft(), getTop(), getLeft() + getWidth(), getTop() + getHeight());
	}
	
	/**
	 * Unlock listener handler
	 *
	 */
	public interface OnUnlockListener {
		/*
		 * Called, when user unlocked call screen 
		 */
		public void onUnLocked();
		
	}
}


