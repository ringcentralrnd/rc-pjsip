/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.messages.Messages;

/**
 * Receiver for storage (SDCard) states.
 * Via current state we can control accessibility of voice/fax messages and other resources.
 */

public class StorageStateReceiver extends BroadcastReceiver {
    private static final String TAG = "[RC] StorageStateReceiver";
    @SuppressWarnings("unused")
    private static final boolean DEBUG = false;

    private static String lastState = "unknown";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "Storage state changed to " + intent.getAction() + ", was " + lastState);
        }

        if (!lastState.equals(intent.getAction())) {
            context.sendBroadcast(new Intent(RCMConstants.ACTION_UPDATE_MESSAGES_LIST).putExtra(Messages.MESSAGE_LIST_UPDATE_BROADCAST_EXTRA, Messages.INTENT_EXTRA_TYPES.STORAGE_STATE_CHANGED));
        }
        lastState = intent.getAction();
    }

    public static String getStateMessage(Context context) {
        if (lastState.equals(Intent.ACTION_MEDIA_UNMOUNTED)) {
            return context.getString(R.string.messages_storage_error_unmounted);
        } else if (lastState.equals(Intent.ACTION_MEDIA_REMOVED)) {
            return context.getString(R.string.messages_storage_error_removed);
        }
        // default error message
        return context.getString(R.string.messages_storage_error);
    }

}
