package com.ringcentral.android.utils.ui.widget;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * This class I dedicate to Marat Daishev :)
 * 
 * @author ivan.alyakskin
 */
public class RMenuButton extends LinearLayout {
	private static final String TAG = "[RC]RMenuButton";
	
	public RMenuButton(Context context) {
		super(context);
	}
	
	public RMenuButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(attrs, context);
	}

	protected void initView(AttributeSet attrs, Context context) {
		inflate(context, R.layout.main_menu_button, this);
		setClickable(true);
		setFocusable(true);
		Drawable	drawableId	= null;
		String		textValue	= null;
		if (attrs != null) {
			final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.MainMenuButtonStyle);
			drawableId	= a.getDrawable(R.styleable.MainMenuButtonStyle_btn_image);
			textValue	= a.getString(R.styleable.MainMenuButtonStyle_btn_text);
			a.recycle();

			if (LogSettings.ENGINEERING) {
				EngLog.d(TAG, "Init, drawableId :" + drawableId + " textValue " + textValue);
			}
		}

		((ImageView) findViewById(R.id.main_menu_button_image)).setBackgroundDrawable(drawableId);;
		((TextView) findViewById(R.id.main_menu_button_text)).setText(textValue);;
	}
}
