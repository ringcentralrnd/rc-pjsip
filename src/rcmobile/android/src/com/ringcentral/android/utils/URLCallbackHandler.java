package com.ringcentral.android.utils;

import java.net.URI;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONObject;

import android.content.Intent;
import android.net.Uri;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;

public class URLCallbackHandler {    	
	private static final String TAG = "[RC]URLCallbackHandler";
	
	/** Callback command after signup */
	public static final String CMD_URL_PROCEED_WITH_SETUP = "/proceed-with-setup";
	public static final String CMD_URL_SETTINGS_COMPLETE = "/settings-completed";
	
	/** Login parameter name */ 
	private static final String SW_PARAM_LOGIN = "ol";
    
    /**	Sign-up/Setup commands tags. */
    private static final String SW_CMD_TAG = "cmd";
    private static final String SW_NUMBER_TAG = "number";
    private static final String SW_PIN_TAG = "pin";
    
    /** Complete setup command */
    private static final String SW_COMPLETE_SETUP_COMMAND = "completeSetup";
    
    /**
     * Checking that callback command is present in the URL
     * 
     * @param intent
     * 			target intent
     * @param command
     * 			target command
     * @return
     * 			
     */
    public static final boolean isUrlCommand(Intent intent, String command){
    	return intent != null 
    		&& intent.getData()!= null 
    		&& intent.getData().getPath()!=null
    		&& intent.getData().getPath().startsWith(command);
    }
    
    /**
     * Extracts login information from Callback URi
     * 
     * @param uri 
     * 			uri to check for the login
     * @return LoginInfo 
     * 			with credentials from URL or empty one
     */
    
    public static LoginInfo getLoginInfo(Uri uri){
    	if (uri != null){
    		if(LogSettings.MARKET){
    			MktLog.d(TAG, "Handling Uri : " + uri.toString());
    		}
    		try{    		
	    		List<NameValuePair> query = URLEncodedUtils.parse(new URI(uri.toString()), null);				
				if(query != null){	
					for(NameValuePair queryParameter : query) {
						if(LogSettings.MARKET){
							MktLog.d(TAG, "Query parameters, name : " + queryParameter.getName() + " value : " + queryParameter.getValue());													 
						}
						if(SW_PARAM_LOGIN.equals(queryParameter.getName()) 
							&& queryParameter.getValue()!=null
							&& queryParameter.getValue().startsWith(RCEncryption.SIGNUP_ENCRPYPT_PREFIX) ){		
							
							String content = queryParameter.getValue().substring(RCEncryption.SIGNUP_ENCRPYPT_PREFIX.length());
							String decryptedContent = RCEncryption.decrypt(content);
							if (LogSettings.ENGINEERING) {
	                            EngLog.i(TAG, "decrypted content:" + decryptedContent);
	                        }
							JSONObject json = new JSONObject(decryptedContent);
																				
							String cmd = json.getString(SW_CMD_TAG);
		                    if (cmd != null) {	                    	
		                    	if (cmd.compareTo(SW_COMPLETE_SETUP_COMMAND) == 0) {
		                            String accountNumber = json.optString(SW_NUMBER_TAG);
		                            String accountExtension  = json.optString(SW_PIN_TAG);
		                            if (LogSettings.MARKET){
		                            	MktLog.d(TAG, "Found credentials, account number : " + accountNumber + " account extension : " + accountExtension);
		                            }
		                            return new LoginInfo(accountNumber, accountExtension, "", false);
		                        } else {
		                            if (LogSettings.MARKET) {
		                                MktLog.d(TAG, "command is unknown: " + cmd);
		                            }
		                        }
		                    } else {
		                        if (LogSettings.MARKET) {
		                            MktLog.e(TAG, "command is not defined");
		                        }
		                    }
		                    break;
						}					
					}						
				} else {
					if (LogSettings.MARKET) {
	                    MktLog.e(TAG, "Unable to parse query");
	                }
				}
    		}catch (Throwable t) {
				if(LogSettings.MARKET){
					MktLog.e(TAG, "Exception when parsing queue", t);
				}
			}
    	}    	
    	return LoginInfo.empty();
    }        
}