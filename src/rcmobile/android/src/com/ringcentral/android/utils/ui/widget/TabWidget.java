package com.ringcentral.android.utils.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnFocusChangeListener;
import android.widget.LinearLayout;

public class TabWidget extends LinearLayout implements OnFocusChangeListener {
    private OnTabSetChanged mSetChangedListener;
    private int mSelectedTab = 0;

    public TabWidget(Context context) {
        this(context, null);
    }

    public TabWidget(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.tabWidgetStyle);
    }

    public TabWidget(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        setFocusable(true);
        setOnFocusChangeListener(this);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    public void childDrawableStateChanged(View child) {
        if (child == getChildAt(mSelectedTab)) {
            invalidate();
        }
        super.childDrawableStateChanged(child);
    }

    @Override
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
    }

    @Override
    public void removeAllViews() {
        super.removeAllViews();
        mSelectedTab = 0;
    }

    public void setTab(int index) {
        if (index < 0 || index >= getChildCount()) {
            return;
        }
        if (getChildAt(mSelectedTab) != null) {
            getChildAt(mSelectedTab).setSelected(false);
            mSelectedTab = index;
            getChildAt(mSelectedTab).setSelected(true);
        }
    }

    public void focusCurrentTab(int index) {
        int oldTab = mSelectedTab;
        setTab(index);
        if (oldTab != index) {
            getChildAt(index).requestFocus();
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            getChildAt(i).setEnabled(enabled);
        }
    }

    @Override
    public void addView(View view) {
        if (view.getLayoutParams() == null) {
            final LinearLayout.LayoutParams linearParams = 
                new LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
            linearParams.setMargins(0, 0, 0, 0);
            view.setLayoutParams(linearParams);
        }
        view.setFocusable(true);
        view.setClickable(true);
        super.addView(view);
        view.setOnClickListener(new TabClickListener(getChildCount() - 1));
        view.setOnFocusChangeListener(this);
    }

    void setTabSelectionListener(OnTabSetChanged listener) {
        mSetChangedListener = listener;
    }

    public void onFocusChange(View view, boolean inFocus) {
        if (getChildCount() == 0) {
            return;
        }
        if (view == this && inFocus) {
            getChildAt(mSelectedTab).requestFocus();
            return;
        }
        if (inFocus) {
            int i = 0;
            while (i < getChildCount()) {
                if (view == getChildAt(i)) {
                    setTab(i);
                    mSetChangedListener.onTabSetChanged(i, false);
                    break;
                }
                i++;
            }
        }
    }

    private class TabClickListener implements OnClickListener {

        private final int mIndex;

        private TabClickListener(int index) {
            mIndex = index;
        }

        public void onClick(View v) {
            mSetChangedListener.onTabSetChanged(mIndex, true);
        }
    }

    static interface OnTabSetChanged {
        void onTabSetChanged(int index, boolean click);
    }
}