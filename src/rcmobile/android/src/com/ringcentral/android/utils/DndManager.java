/** 
 * Copyright (C) 2011-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils;

import android.content.Context;
import android.content.Intent;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.api.network.NetworkManager;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.AccountInfoTable;


public class DndManager {
    
    private static final String TAG = "[RC]DndManager";
    
    public static final int DND_STATUS_TAKE_ALL_CALLS = 1;
    public static final int DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS = 2;
    public static final int DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS = 3;
    public static final int DND_STATUS_TAKE_DEPARTMENT_CALLS_ONLY = 4;

    
    private static final DndManager sInstance = new DndManager();
    
    private DndManager() {}

    public static DndManager getInstance() {
        return sInstance;
    }


    private boolean mDndCache;
    private boolean mDndCacheValid = false;
    
    private int mExtendedDndStatusCache;
    private boolean mExtendedDndStatusCacheValid = false;
    

    /**
     * Set new DND status on server (4.x only)
     * For 5.0.x use setExtendedDndStatus()
     * 
     * @param context
     * @param dnd
     */
    public void setDndStatus(Context context, boolean dnd) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "setDndStatus(dnd = " + dnd + ")..." );
        }

//        if (RCMProviderHelper.getServiceVersion(context) != AccountInfoTable.SERVICE_VERSION_4) {
//            throw new RuntimeException("Cannot use setDndStatus() with Service 5.0.x. Use setExtendedDndStatus() instead!");
//        }
        
        NetworkManager.getInstance().setDndStatus(context, dnd, true);
    }
    

    /**
     * Set new DND status on server (5.0.x and later)
     * For 4.x use setDndStatus()
     * 
     * @param context
     * @param dndStatus:
     * One of: DND_STATUS_TAKE_ALL_CALLS, DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS, 
     * DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS (for department users only)
     * 
     * Setting DND_STATUS_TAKE_DEPARTMENT_CALLS_ONLY is forbidden
     * and throws IllegalArgumentException
     */
    public void setExtendedDndStatus(Context context, int dndStatus) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "setExtendedDndStatus(): " + dndStatus + " (" + extendedDndStatusAsString(dndStatus) + ')');
        }

        if (RCMProviderHelper.getServiceVersion(context) <= AccountInfoTable.SERVICE_VERSION_4) {
            throw new RuntimeException("Cannot use setExtendedDndStatus() with Service 4.x. Use setDndStatus() instead!");
        }
        
        if (validateDndStatus(dndStatus) == false) {
            throw new IllegalArgumentException("Inappropriate dndStatus: " + extendedDndStatusAsString(dndStatus));
        }
        
        if (dndStatus == DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS && !RCMProviderHelper.isAgent(context)) {
            throw new IllegalArgumentException("Inappropriate dndStatus: " + extendedDndStatusAsString(dndStatus) + ": not department user");
        }
        
        NetworkManager.getInstance().setExtendedDndStatus(context, extendedDndStatusAsString(dndStatus), true);
    }
    
    
    /**
     * Read current DND status from the local database (4.x only)
     * 
     * @param context
     * @return current DND status (true/false)
     */
    public boolean getDndStatus(Context context) {
        if (LogSettings.QA) {
            MktLog.d(TAG, "getDndStatus()...");
        }

//        if (RCMProviderHelper.getServiceVersion(context) != AccountInfoTable.SERVICE_VERSION_4) {
//            throw new RuntimeException("Cannot use getDndStatus() with Service 5.0.x. Use getExtendedDndStatus() instead!");
//        }
        
        return getDndStatus(context, true);
    }

    private synchronized boolean getDndStatus(Context context, boolean useCache) {
        boolean result;
        if (useCache && mDndCacheValid) {
            result = mDndCache;
        } else {
            String status = RCMProviderHelper.simpleQueryWithMailboxId(context,
                    RCMProvider.ACCOUNT_INFO, RCMDataStore.AccountInfoTable.JEDI_DND);
            result = status.equalsIgnoreCase("true") || status.equalsIgnoreCase("1") ? true : false;
        }
        
        if (LogSettings.QA) {
            MktLog.d(TAG, "getDndStatus(): return " + result);
        }
        return result;
    }
    
    /**
     * Read current DND status from the local database (5.0.x and later)
     *
     * @param context
     * @return current DND status:
     * One of: DND_STATUS_TAKE_ALL_CALLS, DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS,
     * DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS, DND_STATUS_TAKE_DEPARTMENT_CALLS_ONLY
     */
    public int getExtendedDndStatus(Context context) {
        if (RCMProviderHelper.getServiceVersion(context) <= AccountInfoTable.SERVICE_VERSION_4) {
            throw new RuntimeException("Cannot use getExtendedDNDStatus() with Service 4.x. Use getDNDStatus() instead!");
        }
        
        return getExtendedDndStatus(context, true);
    }


    private synchronized int getExtendedDndStatus(Context context, boolean useCache) {
        if (LogSettings.MARKET) {
            QaLog.d(TAG, "getExtendedDNDStatus(useCache = " + useCache + ")...");
        }

        int result;
        if (useCache && mExtendedDndStatusCacheValid) {
            result = mExtendedDndStatusCache;
        } else {
            String status = RCMProviderHelper.simpleQueryWithMailboxId(context,
                    RCMProvider.ACCOUNT_INFO, RCMDataStore.AccountInfoTable.JEDI_DND_STATUS);
            result = extendedDndStatusAsInt(status);
        }

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "getExtendedDndStatus(): return " + result);
        }
        return result;
    }

    
    /**
     * Save DND status to the local database
     * (for use by NetworkManager)
     *
     * @param context
     * @param status
     */
    public synchronized void saveDndStatus(Context context, boolean status) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveDndStatus(status = " + status + ")..." );
        }
        
        if (status != mDndCache || !mDndCacheValid) {
            mDndCache = status;
            mDndCacheValid = true;
    
            RCMProviderHelper.updateOrInsertSingleValueWithMailboxId(context,
                    RCMProvider.ACCOUNT_INFO, RCMDataStore.AccountInfoTable.JEDI_DND,
                    status == true ? "1" : "0");
    
            context.sendBroadcast(new Intent(RCMConstants.ACTION_DND_STATUS_CHANGED));
        }
    }

    /**
     * Save Extended DND status to the local database (5.0.x only)
     * (for use by NetworkManager)
     *
     * @param context
     * @param status
     */
    public synchronized void saveExtendedDndStatus(Context context, String status) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "saveExtendedDndStatus(status = " + status + ")..." );
        }
        
        int status_code = extendedDndStatusAsInt(status);
        if (status_code != mExtendedDndStatusCache || !mExtendedDndStatusCacheValid) {
            mExtendedDndStatusCache = status_code;
            mExtendedDndStatusCacheValid = true;
    
            RCMProviderHelper.updateOrInsertSingleValueWithMailboxId(context,
                    RCMProvider.ACCOUNT_INFO, RCMDataStore.AccountInfoTable.JEDI_DND_STATUS,
                    status);
    
            context.sendBroadcast(new Intent(RCMConstants.ACTION_DND_STATUS_CHANGED));
        }
    }

    
    private int extendedDndStatusAsInt(String status) {
        if (AccountInfoTable.DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS.equals(status)) {
            return DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS;
        } else if (AccountInfoTable.DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS.equals(status)) {
            return DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS;
        } else if (AccountInfoTable.DND_STATUS_TAKE_DEPARTMENT_CALLS_ONLY.equals(status)) {
            return DND_STATUS_TAKE_DEPARTMENT_CALLS_ONLY;
        } else {
            return DND_STATUS_TAKE_ALL_CALLS;
        }
    }
    
    private String extendedDndStatusAsString(int status) {
        switch (status) {
        case DND_STATUS_TAKE_ALL_CALLS:
            return AccountInfoTable.DND_STATUS_TAKE_ALL_CALLS;
        case DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS:
            return AccountInfoTable.DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS;
        case DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS:
            return AccountInfoTable.DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS;
        case DND_STATUS_TAKE_DEPARTMENT_CALLS_ONLY:
            return AccountInfoTable.DND_STATUS_TAKE_DEPARTMENT_CALLS_ONLY;
        default:
            return String.valueOf(status);
        }
    }

    private boolean validateDndStatus(int status) {
        switch (status) {
        case DND_STATUS_TAKE_ALL_CALLS:
        case DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS:
        case DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS:
            return true;
        default:
            return false;
        }
    }
    
}
