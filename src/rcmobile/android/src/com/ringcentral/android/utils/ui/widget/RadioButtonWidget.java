package com.ringcentral.android.utils.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.ringcentral.android.R;

public class RadioButtonWidget extends LinearLayout {

    private RadioButton mRadioBotton;

    public RadioButtonWidget(Context context) {
        super(context);
    }

    public RadioButtonWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        setDrawingCacheEnabled(true);
        setAlwaysDrawnWithCacheEnabled(true);
        mRadioBotton = (RadioButton) findViewById(R.id.radiobox);
        mRadioBotton.setClickable(false);
        mRadioBotton.setFocusable(false);
    }

    @Override
    public boolean performClick() {
        return super.performClick();
    }

    @Override
    public void setPressed(boolean pressed) {
        super.setPressed(pressed);
    }
}
