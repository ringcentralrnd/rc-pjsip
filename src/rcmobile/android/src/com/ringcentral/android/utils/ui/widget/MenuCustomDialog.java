package com.ringcentral.android.utils.ui.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.contacts.CallLogListActivity;
import com.ringcentral.android.contacts.ContactsListActivity;
import com.ringcentral.android.contacts.FavoritesListActivity;
import com.ringcentral.android.messages.Messages;
import com.ringcentral.android.settings.DNDialog;
import com.ringcentral.android.settings.SettingsLauncher;
import com.ringcentral.android.utils.UserInfo;

public class MenuCustomDialog extends Dialog {
	private static final String TAG = "[RC]RingOut";
	private static final float DIMAMOUNT = 0.5f;
	private View mMenuMessageBtn;
	private View mMenuContactBtn;
	private View mMenuFavoritesBtn;
	private View mMenuCallogBtn;
	private View mMenuSettingBtn;
	private View mMenuDndBtn;
	private LinearLayout mLinearLayoutFirstRow;
	private LinearLayout mLinearLayoutSecondRow;
	
	private Context mContext;
	public MenuCustomDialog(Context context, int theme) {
		super(context, theme);
		this.mContext = context;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setCanceledOnTouchOutside(true);

	}

	@Override
	public void show() {
		orienration(getContext().getResources().getConfiguration().orientation);
		mLinearLayoutFirstRow = (LinearLayout) findViewById(R.id.dialog_menu_row_first);
		mLinearLayoutSecondRow = (LinearLayout) findViewById(R.id.dialog_menu_row_second);
		
		mMenuMessageBtn = (View) findViewById(R.id.dialog_menu_message_button);
		mMenuMessageBtn.setOnClickListener(mMenuMessageListener);
		
		mMenuContactBtn = (View) findViewById(R.id.dialog_menu_contact_button);
		mMenuContactBtn.setOnClickListener(mMenuContactListener);
		
		mMenuFavoritesBtn = (View) findViewById(R.id.dialog_menu_favorites_button);
		mMenuFavoritesBtn.setOnClickListener(mMenuFavoritesListener);
		
		mMenuCallogBtn = (View) findViewById(R.id.dialog_menu_callog_button);
		mMenuCallogBtn.setOnClickListener(mMenuCalllogListener);
		
		mMenuSettingBtn = (View) findViewById(R.id.dialog_menu_setting_button);
		mMenuSettingBtn.setOnClickListener(mMenuSettingListener);
		
		mMenuDndBtn = (View) findViewById(R.id.dialog_menu_dnd_button);
		mMenuDndBtn.setOnClickListener(mMenuDNDListener);
		super.show();
	}
	
	private View.OnClickListener mMenuMessageListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toActivity(RCMConstants.ACTION_LIST_MESSAGES,Messages.class);
		}
	};
	
	private View.OnClickListener mMenuContactListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toActivity(RCMConstants.ACTION_LIST_CONTACTS, ContactsListActivity.class);
		}
	};
	
	private View.OnClickListener mMenuFavoritesListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			toActivity(RCMConstants.ACTION_LIST_FAVORITES,FavoritesListActivity.class);
		}
	};
	
	private View.OnClickListener mMenuCalllogListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
	        toActivity(RCMConstants.ACTION_LIST_RECENT_CALL,CallLogListActivity.class); 
		}
	};
	
	private View.OnClickListener mMenuSettingListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (LogSettings.MARKET) {
                MktLog.i(TAG, "User pressed Settings menu btn");
            }
         SettingsLauncher.launch(mContext);
         dismiss();
		}
	};
	
	private View.OnClickListener mMenuDNDListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (LogSettings.MARKET) {
                MktLog.i(TAG, "User pressed DND menu btn");
            }
			DNDialog.showDNDDialog(mContext);
			dismiss();
		}
	};
	
	private void toActivity(String action,Class<?> activitycls){
		Intent intent = new Intent(action);
		intent.setClass(mContext, activitycls);
		mContext.startActivity(intent);
		dismiss();
	}
	
	public void orienration(int orientation) {
		setContentView(R.layout.menu_dialog_layout);
		WindowManager.LayoutParams lp = this.getWindow().getAttributes(); 
        lp.dimAmount = DIMAMOUNT;
        this.getWindow().setAttributes(lp);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
	}
	@Override         
	public boolean onKeyDown(int keyCode, KeyEvent event)  { 
		if(keyCode == KeyEvent.KEYCODE_MENU){
			dismiss();
		}
		if(keyCode == KeyEvent.KEYCODE_BACK){
			dismiss();
		}
		return true;
	}
	
	 @Override 
	 public void onWindowFocusChanged(boolean hasFocus) {  
		 // TODO Auto-generated method stub  
		 super.onWindowFocusChanged(hasFocus); 
		 LayoutParams mLinearLayoutParams;
		 boolean isDnd = UserInfo.dndAllowed();
			if(isDnd){
				mMenuDndBtn.setVisibility(View.VISIBLE);
			}else{
				mMenuDndBtn.setVisibility(View.GONE);
				mLinearLayoutParams = (LayoutParams) mLinearLayoutSecondRow.getLayoutParams();
				mLinearLayoutParams.width = mLinearLayoutFirstRow.getWidth()/3*2; 
				mLinearLayoutParams.height = mLinearLayoutFirstRow.getHeight();
				mLinearLayoutSecondRow.setLayoutParams(mLinearLayoutParams);
			}
		
	}
}