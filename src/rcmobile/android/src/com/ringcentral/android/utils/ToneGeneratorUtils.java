/** 
 * Copyright (C) 2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils;

import android.content.Context;
import android.media.ToneGenerator;
import android.os.Handler;
import android.os.Message;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;

public class ToneGeneratorUtils {
	private static final String TAG = "[RC]ToneGeneratorUtils";
	
	public static final int SHORT_KEY_TONE_LENGTH_MS = 100;		
	public static final int LONG_KEY_TONE_LENGTH_MS = 300;		
	public static final int KEY_TONE_RELATIVE_VOLUME_30 = 30;
	public static final int KEY_TONE_RELATIVE_VOLUME_50 = 50;
	
	private static final int STOP_KEY_TONE = 1;
	
	private ToneGenerator mToneGenerator;
    private Object mToneGeneratorLock;
    private Handler mToneStopper;
    
    public ToneGeneratorUtils(Context context, int streamId, int volume) {
    	create(context, streamId, volume);
    }
    	    	    	   	    
    
    private void create(Context context, int streamId, int volume) {
    	if(LogSettings.ENGINEERING){
    		EngLog.d(TAG, "Creating, streamId : " + streamId + " volume : " + volume);
    	}
    	
    	mToneGeneratorLock = new Object();
    	
        mToneStopper = new Handler() {
            
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == STOP_KEY_TONE) {
                    synchronized(mToneGeneratorLock) {
                        if (mToneGenerator != null) mToneGenerator.stopTone();
                    }
                }
            }
        };
        
        synchronized (mToneGeneratorLock) {
            if (mToneGenerator == null) {
                try {
                    mToneGenerator = new ToneGenerator(streamId, volume);
                } catch (RuntimeException e) {
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "create: Error creating tone generator: " + e.getMessage(), e);
                    }
                    mToneGenerator = null;
                }
            }
        }
    }
    
    public void destroy(){
    	if(LogSettings.ENGINEERING){
    		EngLog.d(TAG, "Destroying..");
    	}
    	synchronized (mToneGeneratorLock) {
    		
    		if (mToneStopper != null){
    			mToneStopper.removeMessages(STOP_KEY_TONE);
    			mToneStopper = null;
    		}
    		
            if (mToneGenerator != null) {
                mToneGenerator.stopTone();
                mToneGenerator.release();
                mToneGenerator = null;
            }
        }
    	mToneGeneratorLock = null;	    		    	
    }
    
	public void playTone(int tone, int length) {	
		if(LogSettings.ENGINEERING){
    		EngLog.d(TAG, "play tone : " + tone + " length : " + length);
    	}
		synchronized (mToneGeneratorLock) {
			if ( mToneGenerator == null ) {
				return;
			}
			mToneStopper.removeMessages(STOP_KEY_TONE);
			mToneGenerator.startTone(tone);
			mToneStopper.sendEmptyMessageDelayed(STOP_KEY_TONE, length);			
		}
	}
	
}