/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils.ui.widget;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.KeyEvent;

import com.ringcentral.android.R;

public class LoginWaitingDialog extends ProgressDialog {

    private boolean mCancelledByUser = false;
    

    public LoginWaitingDialog(Context context) {
        super(context);
        setMessage(context.getString(R.string.connecting_with_server));
        setCancelable(true);
        setCanceledOnTouchOutside(false);
    }

    public void prepare() {
        mCancelledByUser = false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        synchronized(this) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                mCancelledByUser = true;
            }
            if (keyCode == KeyEvent.KEYCODE_SEARCH){ // otherwise, dialog can be skipped with search button
				return true;
			}
        }

        return super.onKeyDown(keyCode, event);
    }
    
    public boolean cancelledByUser() {
        return mCancelledByUser;
    }

}
