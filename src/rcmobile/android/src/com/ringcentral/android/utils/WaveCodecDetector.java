package com.ringcentral.android.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * 
 */

/**
 * @author Denis Kudja
 *
 */
public class WaveCodecDetector {

	public static int s_Codec_Unknown 		= 0x0000; //0  (0x0000)		Unknown
	public static int s_Codec_PCM_uncompressed 	= 0x0001; //1  (0x0001) 	PCM/uncompressed
	public static int s_Codec_MS_ADPCM		= 0x0002; //2  (0x0002) 	Microsoft ADPCM
	public static int s_Codec_ITU_G711_a_law	= 0x0006; //6  (0x0006) 	ITU G.711 a-law
	public static int s_Codec_ITU_G711_Au_law	= 0x0007; //7  (0x0007) 	ITU G.711 Au-law
	public static int s_Codec_IMA_ADPCM		= 0x0011; //17 (0x0011) 	IMA ADPCM
	public static int s_Codec_ITU_G723_ADPCM	= 0x0016; //20 (0x0016) 	ITU G.723 ADPCM (Yamaha)
	public static int s_Codec_GSM_6_10		= 0x0031; //49 (0x0031) 	GSM 6.10
	public static int s_Codec_ITU_G721_ADPCM	= 0x0040; //64 (0x0040) 	ITU G.721 ADPCM
	public static int s_Codec_MPEG   		= 0x0050; //80 (0x0050) 	MPEG
	public static int s_Codec_Experemental		= 0xFFFF; //65,536 (0xFFFF) Experimental

	public static String s_WAVE_CHUNK_ID		= "RIFF";
	public static String s_RIFF_WAVE		= "WAVE";
	public static String s_format_chunk		= "fmt";
	
	public class Chunk{
		
		private String 	m_chunkID;
		private int 	m_chunkSize	= 0;
		private byte[]	m_chunkData = null;
		private String	m_riff_type;
		
		public Chunk( InputStream is, boolean bMainChunk ){
			m_chunkID = getStringValue( is );
			m_chunkSize = getIntValue( is );
			if( isValid() ){
				if( bMainChunk ){
					m_riff_type = getStringValue( is );;							
				}
				else{
					m_chunkData = getByteValue( is, m_chunkSize );
				}
			}
		}
		
		public boolean isWaveMailChunk(){
			return ( isValid() && 
					(m_chunkID != null && s_WAVE_CHUNK_ID.equalsIgnoreCase(m_chunkID) ) &&
					(m_riff_type != null && s_RIFF_WAVE.equalsIgnoreCase(m_riff_type))); 
		}

		public boolean isFormatChunk(){
			return ( isValid() &&  (m_chunkID != null && s_format_chunk.equalsIgnoreCase(m_chunkID) ) );
		}
		
		private byte[] getByteValue( InputStream is, int chunkSize ){
			byte[] result = new byte[chunkSize];
			try{
				int chunk_id_size = is.read( result, 0, chunkSize );
				if( chunk_id_size != chunkSize ){
					return null;
				}			
			}
			catch( IOException e_io ){
				result = null;
			}
			
			return result;
		}
		
		private String getStringValue( InputStream is ){
			String result = null; 
			byte[] buffer = new byte[4];

			try{
				int chunk_id_size = is.read( buffer, 0, 4 );
				if( chunk_id_size == 4 ){
					result = new String( buffer ).trim();
				}			
			}
			catch( IOException e_io ){
				result = null;
			}
			
			return result;
		}
		
		private int getIntValue(InputStream is ){
			byte[] buffer = new byte[4];
			
			try{
				int real_read = is.read( buffer, 0, 4 );
				if( real_read != 4 )
					return 0;
			}
			catch( IOException e_io ){
			}
			
			return bytesToInt( buffer, 4 );
		}
		
		public int bytesToInt( byte[] data, int size ){

			int result = 0;
			for( int i =0; i < size; i++ ){
				int item = ( data[i] & 0x000000FF );
				result += ( item << ( 8 * i) );
			}
			
			return result;	
		}
		
		public boolean isValid(){
			return ( m_chunkSize > 0 );
		}

		public String getM_chunkID() {
			return m_chunkID;
		}

		public int getM_chunkSize() {
			return m_chunkSize;
		}

		public byte[] getM_chunkData() {
			return m_chunkData;
		}

		public String getM_riff_type() {
			return m_riff_type;
		}
	};
	/*
	 * 
	 */
	public int getCodec( String filename ){
		final File file = new File( filename );
		return getCodec( file );
	}
	
	/*
	 * 
	 */
	public int getCodec( File file ){
		int compression_code = s_Codec_Unknown;
		
		if( !file.exists())
			return s_Codec_Unknown;
		
		if( !file.isFile())
			return s_Codec_Unknown;
		
		try{
			long flen = file.length();

			InputStream is = new FileInputStream( file );
			Chunk chunkMainHeader = new Chunk( is, true );
			
			if( chunkMainHeader.isWaveMailChunk()){
				
				Chunk fmt_chunk = new Chunk( is, false );
				if( fmt_chunk.isFormatChunk() ){
					compression_code = fmt_chunk.bytesToInt(fmt_chunk.getM_chunkData(), 2 );
				}
			}

			is.close();
		}
		catch( FileNotFoundException e_fnf ){
			
		}
		catch( IOException e_io ){
			
		}
		
		return compression_code;
	}
	
	/*
	 * 
	 */
	public static boolean isGSM( File file ){
		WaveCodecDetector wcd = new WaveCodecDetector();
		return ( wcd.getCodec(file) == s_Codec_GSM_6_10 );
	}
}
