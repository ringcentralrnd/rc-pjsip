package com.ringcentral.android.utils.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.widget.RelativeLayout;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;

public class ScreenLockerLayout extends RelativeLayout implements OnTouchListener, LockSlider.OnUnlockListener {
	
	private static final String TAG = "[RC]ScreenLockerLayout";
	
	private static final int LOCKER_HEIGHT_DEFAULT = 40;
	
	private static final int LOCK_DELAYED = 0;
	private static final int UNLOCK_DELAYED = 1;
	
	public static final int LOCK_DELAY_SHORT = 3000;
	public static final int LOCK_DELAY_LONG = 6000;	
		
	private LockSlider mUnlockSlider;
	private static boolean isRelock;
	private static boolean isLocked;
	
	//in percent ;o)
	private int lockerOffset = LOCKER_HEIGHT_DEFAULT;
	
	public ScreenLockerLayout(Context context) {
		super(context);		
		init(context, null);
		
	}
	
	public ScreenLockerLayout(Context context, AttributeSet attrs) {
		super(context, attrs);		
		init(context, attrs);					
	}
	
	public ScreenLockerLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);		
		init(context, attrs);
	}
	
	private void init(Context context, AttributeSet attrs){
		setOnTouchListener(this);		
		
		mUnlockSlider = new LockSlider(getContext());
		mUnlockSlider.setUnlockListener(this);
		mUnlockSlider.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		addView(mUnlockSlider);

		if(isRelock&&!isLocked){
			show();
		}	
		
		if(attrs != null){
			final TypedArray a = getContext().obtainStyledAttributes(attrs,R.styleable.LockerStyle);
			lockerOffset = a.getInt(R.styleable.LockerStyle_locker_height, LOCKER_HEIGHT_DEFAULT);
		}
	}

	
	
	/*
	 * M.D. if no sensor detected, we should re-lock screen after it has been unlocked
	 */
	public static void setRelock(boolean isRelock){
		if(LogSettings.ENGINEERING){
			EngLog.v(TAG, "Setting automatic re-lock : " + isRelock);
		}
		ScreenLockerLayout.isRelock = isRelock;
	}
	
	
	public boolean isLocked(){
		return isLocked;
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);		
		final int parentWidth = r - l;
		final int parentHeight = b - t;				
		
		mUnlockSlider.layout(0, (int)(parentHeight * (1 - (float)lockerOffset / 100)), parentWidth, parentHeight);				
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if(LogSettings.ENGINEERING){
			EngLog.v(TAG, "onDetachedFromWindow()");
		}
		mUnlockSlider = null;
		if(mHandler != null){
			mHandler.removeMessages(LOCK_DELAYED);
			mHandler.removeMessages(UNLOCK_DELAYED);
			mHandler = null;			
		}			
	}
			
	public void show() {
		if(LogSettings.ENGINEERING){
			EngLog.v(TAG, "show() lockbar");
		}
		setVisibility(VISIBLE);
		
		Window win = ((Activity)getContext()).getWindow();
		win.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    win.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);	
		isLocked = true;
	}
		
	public void show(int delayMillis) {
		if(LogSettings.ENGINEERING){
			EngLog.v(TAG, "show() lockbar time : "+delayMillis);
		}
		if(mHandler != null){
			mHandler.removeMessages(LOCK_DELAYED);			
			mHandler.sendEmptyMessageDelayed(LOCK_DELAYED, delayMillis);
		}else{
			show();
		}			
	}		
		
	public void hide() {
		if(LogSettings.ENGINEERING){
			EngLog.v(TAG, "hide() lockbar");
		}
		setVisibility(GONE);
		
		Window win = ((Activity)getContext()).getWindow();
		win.addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
	    win.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

		isLocked = false;
		if(isRelock){
			show(LOCK_DELAY_LONG);
		}		
	}
		
	public void hide(int delayMillis) {
		if(LogSettings.ENGINEERING){
			EngLog.v(TAG, "hide() lockbar time : "+delayMillis);
		}
		if(mHandler != null){
			mHandler.removeMessages(UNLOCK_DELAYED);
			mHandler.sendEmptyMessageDelayed(UNLOCK_DELAYED, delayMillis);
		}else{
			hide();
		}			
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		return true;
	}
	
	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case LOCK_DELAYED:		
				show();
				break;
			case UNLOCK_DELAYED:
				hide();
				break;
			}
			
		}
	};	
	
	@Override
	public void onUnLocked(){
		hide();
	}		

}
