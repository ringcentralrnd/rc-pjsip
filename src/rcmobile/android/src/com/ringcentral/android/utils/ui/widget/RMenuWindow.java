package com.ringcentral.android.utils.ui.widget;

import java.util.ArrayList;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentral;
import com.ringcentral.android.provider.RCMProviderHelper; 
import com.ringcentral.android.settings.DNDialog;
import com.ringcentral.android.settings.SettingsLauncher;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.res.Resources;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public abstract class RMenuWindow implements OnClickListener {
	private static final String TAG = "[RC]RMenuWindow"; 
	private static final int INT_REGULAR_MENU_ITEMS_COUNT = 6;
	
	protected ArrayList<RMenuItem>	items;
	protected Dialog 				dialog;
	protected int 					layout;
	protected int 					windowHeight;
	protected int 					windowWidth;
	
	/**
	 * Actions which should be executed when dialog will be closed 
	 */
	private static interface RMenuWindowCloseCallback {
		public void buttonPressed();
	}
	private final RMenuWindowCloseCallback mCallback = new RMenuWindowCloseCallback() {
		@Override
		public void buttonPressed() {
			if(LogSettings.ENGINEERING) {
				EngLog.d(TAG, " RMenuWindowCallback ");
			}
			hideMenu();
		}
	};

	protected abstract Dialog	getDialog();
	
	private RMenuWindow(Context context) {
		items = null;
		
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		windowHeight	= display.getHeight();
		windowWidth	= display.getWidth();
	} 
	
	private RMenuWindow(Context context, int ilayout) {
		items = new ArrayList<RMenuItem>(INT_REGULAR_MENU_ITEMS_COUNT);
		layout = ilayout;
		
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		windowHeight	= display.getHeight();
		windowWidth	= display.getWidth();
	}
	
	protected void addControl(RMenuItem rmi) {
		rmi.setCloseCallback(mCallback);
		items.add(rmi);
	}
	public void clearControls() {
		items.clear();
	}
	
	/**
	 * Use that method to perform any actions after menu is closed, 
	 * for example change elements states, finish activity & etc
	 * @param odl
	 */
	public void setOnDismissListener(OnDismissListener odl) {
		if(null != dialog) { 
			dialog.setOnDismissListener(odl);
		}
	}
	public void close() {
		hideMenu();
	}
	
	protected void hideMenu() {
		if(LogSettings.ENGINEERING) { 
			EngLog.d(TAG, "hide menu");
		}
		
		if(null != dialog) {
			if(dialog.isShowing()) { 
				dialog.dismiss();
			}
			
			dialog = null;
		}
	}
	
	protected final void showDialog(Context context, OnDismissListener odl) {
		dialog = getDialog();
		dialog.show();
		dialog.setOnDismissListener(odl);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if(keyCode == KeyEvent.KEYCODE_MENU && event.getAction() == KeyEvent.ACTION_DOWN) {
					dialog.dismiss();
				}
				return false;
			}
		});
	}
	
	@Override
	public void onClick(View v) {
	}
	
	static class RMenuItem implements OnClickListener {
		private int								drawableId;
		private RMenuWindowCloseCallback		mCloseCallbak;
		private OnClickListener					onClickListener;
		private String 							mTag = null;
		
		private RMenuItem(int drawable) {
			drawableId = drawable;
		}
		
		public void setOnClickListener(OnClickListener l) {
			onClickListener = l;
		}
		
		public void setView(int id) {
			drawableId = id;
		}
		
		private void setCloseCallback(RMenuWindowCloseCallback rmwc) {
			mCloseCallbak = rmwc;
		}
		
		View getViewFromLayout(Context context) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        View view = null;

            view = inflater.inflate(R.layout.popup_menu_element, null, false);
            ImageButton btn = (ImageButton)view.findViewById(R.id.btn_popup_menu_btn_preparation);
            btn.setBackgroundDrawable(context.getResources().getDrawable(drawableId));
            btn.setOnClickListener(this);
            
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);
	        params.weight = 1.0f;
            view.setLayoutParams(params);
            
			return view;
		}
		
		@Override
		public void onClick(View v) {
			if(null != v && null != mTag) {
				v.setTag(mTag);
			}
			if(null != onClickListener) { 
				onClickListener.onClick(v);
			}
			if(null != mCloseCallbak) { 
				mCloseCallbak.buttonPressed();
			}
		}
		
		static RMenuItem getItem(int drawable) {
			return new RMenuItem(drawable);
		}
		
		RMenuItem(int drawable, OnClickListener ocl, String tag) {
			drawableId = drawable;
			setOnClickListener(ocl);
			setTag(tag);
		}

		public void setTag(String tag) {
			mTag = tag;
		}
	}
	
	public static MainMenu	getMainMenuControl(Context context) {
		MainMenu result = new MainMenu(context);
		return result;
	}
	
	public static class MainMenu extends RMenuWindow {
		private static final int MODE_MAIN_SCREEN	= 1;
		private static final int MODE_DIALER_SCREEN	= 2;
		
		protected int		dialogHeight;
		protected Context	mContext;
		protected int 		displayMode;
		
		protected boolean 	isDNDVisible;
		
		protected OnClickListener ocl = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (null == v) {
					return;
				}
				
				boolean dndPressed = false;
				boolean settingsPressed = false;
				
				Intent notificationIntent = new Intent(RCMConstants.ACTION_CURRENT_TAB_CHANGED);
				switch (v.getId()) {
				case R.id.mm_call_log:
					notificationIntent.putExtra(RingCentral.TAB_TAG, RingCentral.TAB_CALL_LOG);
					break;

				case R.id.mm_contacts:
					notificationIntent.putExtra(RingCentral.TAB_TAG, RingCentral.TAB_CONTACTS);
					break;

				case R.id.mm_dnd:
					DNDialog.showDNDDialog(mContext);
					dndPressed = true;
					break;

				case R.id.mm_favorites:
					notificationIntent.putExtra(RingCentral.TAB_TAG, RingCentral.TAB_FAVORITES);
					break;

				case R.id.mm_messages:
					notificationIntent.putExtra(RingCentral.TAB_TAG, RingCentral.TAB_MESSAGES);
					break;

				case R.id.mm_settings:
					SettingsLauncher.launch(mContext);
					settingsPressed = true;
					break;
				}
				
				mContext.sendBroadcast(notificationIntent);
				hideMenu();
				
				switch(displayMode) {
				case MODE_DIALER_SCREEN:
					if(!dndPressed && !settingsPressed) {
						Intent finishRingOut = new Intent(RCMConstants.ACTION_CLOSE_RINGOUT);
						mContext.sendBroadcast(finishRingOut);
					}
					break;
				case MODE_MAIN_SCREEN:
					break;
				}
			}
		};

		public MainMenu(Context context) {
			super(context, R.layout.popupwindow);
			mContext = context;
			isDNDVisible = ((RCMProviderHelper.getTierSettings(context) & RCMConstants.TIERS_PHS_DO_NOT_DISTURB) == 0) ? false : true; 
		}
		
		public void show() {
			displayMode = MODE_DIALER_SCREEN;
			hideMenu();
			dialogHeight = windowHeight;
			showDialog(mContext, null);
		}
		
		public void show(View bottomBar) {
			if(null == bottomBar) {
				new IllegalArgumentException("Impossible show dialog with null context");
				
			}
			displayMode = MODE_MAIN_SCREEN;
			
			if(LogSettings.ENGINEERING) {
				EngLog.d(TAG, "show -> popupWindow");
			}
			hideMenu();
			dialogHeight = windowHeight - bottomBar.getMeasuredHeight();
			showDialog(mContext, null);
		}

		@Override
		protected Dialog getDialog() {
			if(null == mContext) {
				new IllegalArgumentException("Impossible show dialog with null context");
			}
			
			final Dialog result = new Dialog(mContext, R.style.CustomDialogTheme);
			result.setContentView(layout);
			
			LayoutParams lp = result.getWindow().getAttributes();
			
			
			switch(displayMode) {
			case MODE_MAIN_SCREEN:
				result.findViewById(R.id.controlsContainer).setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.bg_mainnav_without_arrow));
				result.findViewById(R.id.popupWindowBottomImage).setVisibility(View.VISIBLE);
				break;
				
			case MODE_DIALER_SCREEN:
				result.findViewById(R.id.controlsContainer).setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.bg_mainnav_without_arrow));
				result.findViewById(R.id.popupWindowBottomImage).setVisibility(View.GONE);
				break;
			}
			lp.height = dialogHeight;
			lp.gravity = Gravity.TOP;
			lp.width = windowWidth;
			
			result.findViewById(R.id.mm_call_log).setOnClickListener(ocl);
			result.findViewById(R.id.mm_contacts).setOnClickListener(ocl);
			result.findViewById(R.id.mm_dnd).setOnClickListener(ocl);
			result.findViewById(R.id.mm_dnd).setVisibility(isDNDVisible ? View.VISIBLE : View.GONE);
			result.findViewById(R.id.mm_favorites).setOnClickListener(ocl);
			result.findViewById(R.id.mm_messages).setOnClickListener(ocl);
			result.findViewById(R.id.mm_settings).setOnClickListener(ocl);
			
			result.getWindow().getAttributes().windowAnimations = 1;
			result.findViewById(R.id.popupWindowBckgrnd).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					result.dismiss();
				}
			});
			
			result.setCanceledOnTouchOutside(true);
			
			return result;
		}
	}
	
	public static class DropdownMenu extends RMenuWindow {
		
		protected int		dialogHeight;
		protected Context	mContext;
		
		protected boolean 	isDNDVisible;
		
		protected OnDropdownClickListener mOnDropdownClickListener;
		protected Window mWindow;
		protected LinearLayout mTopMenuLayout;
		protected ArrayList<TopMenuItem> mTopMenuList;
		public interface OnDropdownClickListener {
			void onDropdownClick(DropdownMenu view, int option);
		}
		
		public void setOnDropdownClickListener(Window window,
				OnDropdownClickListener onDropdownClickListener) {
			mWindow = window;
			mOnDropdownClickListener = onDropdownClickListener;
			
		}
		
		public DropdownMenu(Context context) {
			super(context, R.layout.top_dropdown_menu);
			mContext = context;
			isDNDVisible = ((RCMProviderHelper.getTierSettings(context) & RCMConstants.TIERS_PHS_DO_NOT_DISTURB) == 0) ? false : true; 
		}
		public void setDropdownList(ArrayList<TopMenuItem> list){
			mTopMenuList = list;
		}
		public void show() {
			hideMenu();
			dialogHeight = windowHeight;
			showDialog(mContext, null);
		}
		
		public void show(View bottomBar) {
			if(null == bottomBar) {
				new IllegalArgumentException("Impossible show dialog with null context");
				
			}
			
			if(LogSettings.ENGINEERING) {
				EngLog.d(TAG, "show -> popupWindow");
			}
			hideMenu();
			dialogHeight = windowHeight - bottomBar.getMeasuredHeight();
			showDialog(mContext, null);
		}
		
		public static class TopMenuItem {
			String name;
			int index;

			public TopMenuItem(String name, int type) {
				this.name = name;
				this.index = type;
			}
		}
		
		public void constructButton(ArrayList<TopMenuItem> list) {
			for (int i = 0; i < list.size(); i++) {
				TopMenuItem item = list.get(i);
				Button btn = new Button(mContext);
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
												   LayoutParams.FILL_PARENT, 
												   mContext.getResources().getDimensionPixelSize(R.dimen.top_menu_btn_height));
				int margin =  mContext.getResources().getDimensionPixelSize(R.dimen.top_menu_btn_margin);
				int correction =  mContext.getResources().getDimensionPixelSize(R.dimen.top_menu_btn_margin_correction);
				if(i == 0){
					params.setMargins(margin, margin+correction, margin, margin);
				}else{
					params.setMargins(margin, margin, margin, margin);
				}
			
				btn.setLayoutParams(params);
				btn.setBackgroundResource(R.drawable.event_detail_one_item_gb);
				btn.setText(item.name);
				btn.setTag(item.index);
				btn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					hideMenu();
					if (mOnDropdownClickListener != null) {
						int t = (Integer) v.getTag();
						mOnDropdownClickListener.onDropdownClick(
								DropdownMenu.this, t);
					}
				}
			});
				mTopMenuLayout.addView(btn);
			}
			mTopMenuLayout.invalidate();
			
		}
		@Override
		protected Dialog getDialog() {
			if(null == mContext) {
				new IllegalArgumentException("Impossible show dialog with null context");
			}
			
			final Dialog result = new Dialog(mContext, R.style.CustomDialogTheme);
			result.setContentView(layout);
			mTopMenuLayout = (LinearLayout) result.findViewById(R.id.text_chat_menu_linear);
			LayoutParams lp = result.getWindow().getAttributes();
					
			lp.height = dialogHeight;
			lp.gravity = Gravity.TOP;
			lp.width = windowWidth;
			result.findViewById(R.id.dropdown_bright).setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					result.dismiss();
				}
			});
			result.getWindow().getAttributes().windowAnimations = 1;
			result.findViewById(R.id.dropdownBckgrnd).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					result.dismiss();
				}
			});
			constructButton(mTopMenuList);
			result.setCanceledOnTouchOutside(true);
			
			return result;
		}
		
	}
	
}

