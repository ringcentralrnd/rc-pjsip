package com.ringcentral.android.utils.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

import com.ringcentral.android.R;

public class ToggleButtonWidget extends RelativeLayout {

    private ToggleButton mToggleBotton;

    public ToggleButtonWidget(Context context) {
        super(context);
    }

    public ToggleButtonWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        setDrawingCacheEnabled(true);
        setAlwaysDrawnWithCacheEnabled(true);
        mToggleBotton = (ToggleButton) findViewById(R.id.toggle);
        mToggleBotton.setClickable(false);
        mToggleBotton.setFocusable(false);
    }

    @Override
    public boolean performClick() {
        mToggleBotton.setChecked(!mToggleBotton.isChecked());
        return super.performClick();
    }

    @Override
    public void setPressed(boolean pressed) {
        super.setPressed(pressed);
    }

    public boolean isChecked() {
        return mToggleBotton.isChecked();
    }

    public void setChecked(boolean state) {
        mToggleBotton.setChecked(state);
    }
}
