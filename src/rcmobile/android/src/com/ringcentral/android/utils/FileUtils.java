/** 
 * Copyright (C) 2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import android.content.Context;
import android.os.Environment;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.provider.RCMProviderHelper;


public class FileUtils {
	
	private static final String NOMEDIA = ".nomedia";

    private static String MSG_PREFIX = "rc_msg_"; 
	
	private static String DOT = ".";
    private static final String TAG = "[RC] FileUtils";


    public static String getPath(String accountPath,String msgId,String ext){
		return new StringBuilder()
					.append(accountPath).append('/').append(MSG_PREFIX).append(msgId).append(DOT).append(ext)
					.toString();
	}
	
	
	public static String getTmpBase64File(String accountPath,long msgId){
		String filePath = getPath(accountPath,String.valueOf(msgId), "base64");
		return filePath;
	}
	
	public static String getFilePathForMsg(String accountPath,String msgId,String fileExtension){
		String filePath = getPath(accountPath,msgId, fileExtension);
		return filePath;
	}
	
	public static Writer getStreamForBase64(String filePath){
		File f = new File(filePath);
		if (f.exists()) {
			if (!f.delete()) {
                if (LogSettings.ENGINEERING) {
                    EngLog.e(TAG, "cannot delete file");
                }
			}
		}else{
			f.getParentFile().mkdirs();
		}
		OutputStream os = null;
		try {
			os = new FileOutputStream(f);
		} catch (FileNotFoundException e) {
            if (LogSettings.ENGINEERING) {
                EngLog.w(TAG, "getStreamForBase64(): " + e.getMessage());
            }
		}
		
		Writer out = null;
		
		try {
			 out = new OutputStreamWriter(os, "UTF8");
		} catch (UnsupportedEncodingException e) {
            if (LogSettings.ENGINEERING) {
                EngLog.w(TAG, "getStreamForBase64(): " + e.getMessage());
            }
		}
		return out;
	}

	/**
	 * Get path to sd card
	 * 
	 * @param context
	 * @return
	 */
	public static String getSDPath(Context context) {
		return getExternalRootDirectory() + RCMProviderHelper.getCurrentMailboxId(context);
	}
	
    /**
	 * Get path to sd card where log entries are stored
	 * @return - path to the folder where logs are located
	 */
	public static String getSDPathLogs() {
		return getExternalRootDirectory() + "logs";
	}

    /**
     * Detect state of external storage (such as SDCard)
     * @return
     */
    public static boolean isStorageAccessible() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }
    
    public static String getExternalRootDirectory(){
        return Environment.getExternalStorageDirectory() + "/" + BUILD.APP_FOLDER_NAME_ON_SDCARD + "/";
    }
    /**
     * Creates ".nomedia" file in application external files root directory. 
     * Mediascape app skips folders with ".nomedia" file, so it will not play RC messages.  
     */
    public static void createNomediaFileIfNotExist(){
        if (!isStorageAccessible()){
            if (LogSettings.MARKET) {
                QaLog.w(TAG, "nomedia: SDcard not accessable");
            }
            return;
        }
        try {
            File f = new File(getExternalRootDirectory() + NOMEDIA);
            if (!f.exists()){
                File parentFile = f.getParentFile();
                if (parentFile != null){
                    parentFile.mkdirs();
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        if (LogSettings.MARKET) {
                            MktLog.w(TAG, "nomedia: it's not possible to create the file: " + e.getMessage());
                        }
                    }
                }
            } else {
                return;
            }
        } catch (Throwable e) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "nomedia: " + e.getMessage());
            }
        }
    }
}
