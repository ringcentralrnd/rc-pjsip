/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.ringcentral.android.R;
import com.ringcentral.android.api.pojo.CallLogPOJO;

public class LogsUtils {
    private Context mContext;
    private Drawable mCallLogMissedDrawable;
    private Drawable mCallLogCallInDrawable;
    private Drawable mCallLogCallOutDrawable;
    private Drawable mCallLogFaxXDrawable;
    private Drawable mCallLogFaxInDrawable;
    private Drawable mCallLogFaxOutDrawable;
    private Drawable mMenuDialogDndButtonNor;

    public LogsUtils(Context context) {
        mContext = context;
        Resources res = context.getResources();
        mCallLogMissedDrawable = res.getDrawable(R.drawable.ic_call_log_call_missed);
        mCallLogCallInDrawable = res.getDrawable(R.drawable.ic_call_log_call_in);
        mCallLogCallOutDrawable = res.getDrawable(R.drawable.ic_call_log_call_out);
        mCallLogFaxXDrawable = res.getDrawable(R.drawable.ic_call_log_fax_x);
        mCallLogFaxInDrawable = res.getDrawable(R.drawable.ic_call_log_fax_in);
        mCallLogFaxOutDrawable = res.getDrawable(R.drawable.ic_call_log_fax_out);
        mMenuDialogDndButtonNor = res.getDrawable(R.drawable.menu_dialog_dnd_button_nor);
        
    }

    public static String getLengthLabel(float d) {
        if (d < 0)
            d = 0;
        String format = "0:ss";
        int minutes = (int) Math.floor(d);
        int seconds = (int) Math.round((d - minutes) * 60);
        int totalSeconds = seconds + (minutes * 60);
        if (minutes > 0) {
            format = "mm:ss";
            if (minutes >= 60) {
                format = "HH:mm:ss";
            }
        }
        Date time = new Date(totalSeconds * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        return sdf.format(time);
    }

    public String getStatusText(int status) {
        String result = "";
        switch (status) {
        case CallLogPOJO.CALLSTATUS_UNKNOWN:
            result = mContext.getString(R.string.calllog_status_175_unknown);
            break;
        case CallLogPOJO.CALLSTATUS_INPROGRESS:
            result = mContext.getString(R.string.calllog_status_176_in_progress);
            break;
        case CallLogPOJO.CALLSTATUS_MISSED:
            result = mContext.getString(R.string.calllog_status_177_missed);
            break;
        case CallLogPOJO.CALLSTATUS_ACCEPT:
            result = mContext.getString(R.string.calllog_status_178_accept_call);
            break;
        case CallLogPOJO.CALLSTATUS_VOICEMAIL:
            result = mContext.getString(R.string.calllog_status_179_voicemail);
            break;
        case CallLogPOJO.CALLSTATUS_REJECT:
            result = mContext.getString(R.string.calllog_status_180_reject);
            break;
        case CallLogPOJO.CALLSTATUS_REPLY:
            result = mContext.getString(R.string.calllog_status_182_reply);
            break;
        case CallLogPOJO.CALLSTATUS_FAX_RECEIVE:
            result = mContext.getString(R.string.calllog_status_183_fax_received);
            break;
        case CallLogPOJO.CALLSTATUS_FAX_RECEIVE_ERROR:
            result = mContext.getString(R.string.calllog_status_184_fax_receive_error);
            break;
        case CallLogPOJO.CALLSTATUS_FAX_ONDEMAND:
            result = mContext.getString(R.string.calllog_status_185_fax_on_demand);
            break;
        case CallLogPOJO.CALLSTATUS_FAX_PARTIAL_RECEIVE:
            result = mContext.getString(R.string.calllog_status_186_fax_partial_receive);
            break;
        case CallLogPOJO.CALLSTATUS_BLOCKED:
            result = mContext.getString(R.string.calllog_status_187_blocked);
            break;
        case CallLogPOJO.CALLSTATUS_CONNECTED:
            result = mContext.getString(R.string.calllog_status_188_connected);
            break;
        case CallLogPOJO.CALLSTATUS_NOANSWER:
            result = mContext.getString(R.string.calllog_status_189_no_answer);
            break;
        case CallLogPOJO.CALLSTATUS_INTERNATIONAL_DISABLED:
            result = mContext.getString(R.string.calllog_status_190_international_disabled);
            break;
        case CallLogPOJO.CALLSTATUS_BUSY:
            result = mContext.getString(R.string.calllog_status_191_busy);
            break;
        case CallLogPOJO.CALLSTATUS_FAX_SEND_ERROR:
            result = mContext.getString(R.string.calllog_status_192_fax_send_error);
            break;
        case CallLogPOJO.CALLSTATUS_FAX_SENT:
            result = mContext.getString(R.string.calllog_status_193_fax_sent);
            break;
        case CallLogPOJO.CALLSTATUS_NO_FAX_MACHINE:
            result = mContext.getString(R.string.calllog_status_194_no_fax_machine);
            break;
        case CallLogPOJO.CALLSTATUS_SUSPENDED_ACOUNT:
            result = mContext.getString(R.string.calllog_status_203_suspended_account);
            break;
        case CallLogPOJO.CALLSTATUS_CALL_FAILED:
            result = mContext.getString(R.string.calllog_status_204_call_failed);
            break;
        case CallLogPOJO.CALLSTATUS_CALL_FAILURE:
            result = mContext.getString(R.string.calllog_status_205_call_failure);
            break;
        case CallLogPOJO.CALLSTATUS_INTERNAL_ERROR:
            result = mContext.getString(R.string.calllog_status_206_internal_error);
            break;
        case CallLogPOJO.CALLSTATUS_IP_PHONE_OFFLINE:
            result = mContext.getString(R.string.calllog_status_207_ip_phone_offline);
            break;
        case CallLogPOJO.CALLSTATUS_NO_CALLING_CREDIT:
            result = mContext.getString(R.string.calllog_status_208_no_calling_credit);
            break;
        case CallLogPOJO.CALLSTATUS_RESTRICTED_NUMBER:
            result = mContext.getString(R.string.calllog_status_209_restricted_number);
            break;
        case CallLogPOJO.CALLSTATUS_WRONG_NUMBER:
            result = mContext.getString(R.string.calllog_status_210_wrong_number);
            break;
        case CallLogPOJO.CALLSTATUS_ANSWERED_NOT_ACCEPTED:
            result = mContext.getString(R.string.calllog_status_211_answered_not_accepted);
            break;
        case CallLogPOJO.CALLSTATUS_FORCE_STOPPED:
            result = mContext.getString(R.string.calllog_status_237_force_stopped);
            break;
        case CallLogPOJO.CALLSTATUS_ABANDONED:
            result = mContext.getString(R.string.calllog_status_252_abandoned);
            break;
        case CallLogPOJO.CALLSTATUS_DECLINED:
            result = mContext.getString(R.string.calllog_status_253_declined);
            break;
        default:
            result = "";
            break;
        }
        return result;
    }

    public String getCallTypeText(int type) {
        String result = "";
        switch (type) {
        case CallLogPOJO.CALLTYPE_UNKNOWN:
            result = mContext.getString(R.string.calllog_calltype_159_unknown);
            break;
        case CallLogPOJO.CALLTYPE_PHONECALL:
            result = mContext.getString(R.string.calllog_calltype_160_phone_call);
            break;
        case CallLogPOJO.CALLTYPE_PHONELOGIN:
            result = mContext.getString(R.string.calllog_calltype_161_phone_login);
            break;
        case CallLogPOJO.CALLTYPE_FAXRECEIVE:
            result = mContext.getString(R.string.calllog_calltype_162_fax_receive);
            break;
        case CallLogPOJO.CALLTYPE_ACCEPTCALL:
            result = mContext.getString(R.string.calllog_calltype_163_accept_call);
            break;
        case CallLogPOJO.CALLTYPE_FINDME:
            result = mContext.getString(R.string.calllog_calltype_164_findme);
            break;
        case CallLogPOJO.CALLTYPE_FOLLOWME:
            result = mContext.getString(R.string.calllog_calltype_165_followme);
            break;
        case CallLogPOJO.CALLTYPE_OUTGOINGFAX:
            result = mContext.getString(R.string.calllog_calltype_166_putgoing_fax);
            break;
        case CallLogPOJO.CALLTYPE_CALLRETURN:
            result = mContext.getString(R.string.calllog_calltype_167_call_return);
            break;
        case CallLogPOJO.CALLTYPE_CALLINGCARD:
            result = mContext.getString(R.string.calllog_calltype_168_calling_card);
            break;
        case CallLogPOJO.CALLTYPE_RINGDIRECTLY:
            result = mContext.getString(R.string.calllog_calltype_169_ring_directly);
            break;
        case CallLogPOJO.CALLTYPE_RINGOUTWEB:
            result = mContext.getString(R.string.calllog_calltype_170_ringout_web);
            break;
        case CallLogPOJO.CALLTYPE_SIPCALL:
            result = mContext.getString(R.string.calllog_calltype_171_sip_call);
            break;
        case CallLogPOJO.CALLTYPE_RINGOUTPC:
            result = mContext.getString(R.string.calllog_calltype_172_ringout_pc);
            break;
        case CallLogPOJO.CALLTYPE_RINGME:
            result = mContext.getString(R.string.calllog_calltype_173_ringme);
            break;
        case CallLogPOJO.CALLTYPE_TRANSFERCALL:
            result = mContext.getString(R.string.calllog_calltype_174_transfer_call);
            break;
        case CallLogPOJO.CALLTYPE_RINGOUTMOBILE:
            result = mContext.getString(R.string.calllog_calltype_265_ringOut_mobile);
            break;
        default:
            result = "";
            break;
        }
        return result;
    }

    /**
     * Defines invalid Call Log icon type {@link #getCallLogListIconDrawable(int, int, int, ImageView, int).
     */
    public static final int INVALID_CALL_LOG_ICON = 0;
    public static final int CALL_LOG_ICON_MISSED = 1;
    public static final int CALL_LOG_ICON_CALL_IN = 2;
    public static final int CALL_LOG_ICON_CALL_OUT = 3;
    public static final int CALL_LOG_ICON_FAXX = 4;
    public static final int CALL_LOG_ICON_FAX_IN = 5;
    public static final int CALL_LOG_ICON_FAX_OUT = 6;

    /**
     * Sets an Call Log icon to <code>view</code> if required.
     * 
     * @param direction
     *            outgoing/incoming indication
     * @param callType
     *            call type
     * @param callStatus
     *            call status
     * @param view
     *            the view to set icon
     * @param currentIconType
     *            current icon type assigned to the <code>view</code>
     * @return icon type set to the <code>view</code> or the same value if replacement is not required
     */
    public int setCallLogListIconDrawable(int direction, int callType, int callStatus, ImageView view,
            int currentIconType) {
        int iconType = getIconType(direction, callType, callStatus);
        Drawable icon = getCallIcon(iconType);

        if (currentIconType != iconType) {
            view.setImageDrawable(icon);
        }

        return iconType;
    }
    
    public Drawable getDndIcon(){
    	return mMenuDialogDndButtonNor;
    }

    public Drawable getCallIcon(int iconType) {
        Drawable icon;
        switch (iconType) {
        case CALL_LOG_ICON_CALL_IN:
            icon = mCallLogCallInDrawable;
            break;
        case CALL_LOG_ICON_CALL_OUT:
            icon = mCallLogCallOutDrawable;
            break;
        case CALL_LOG_ICON_MISSED:
            icon = mCallLogCallInDrawable;
            break;
        case CALL_LOG_ICON_FAX_IN:
            icon = mCallLogFaxInDrawable;
            break;
        case CALL_LOG_ICON_FAX_OUT:
            icon = mCallLogFaxOutDrawable;
            break;
        case CALL_LOG_ICON_FAXX:
            icon = mCallLogFaxXDrawable;
            break;
        default:
            icon = mCallLogCallInDrawable;
            break;
        }
        return icon;
    }

    public int getIconType(int direction, int callType, int callStatus) {
        int iconType = CALL_LOG_ICON_MISSED;
        if (isCallIcont(callType) == 1) {
            if (direction == CallLogPOJO.DIRECTION_INCOMING_INT) {
                iconType = CALL_LOG_ICON_CALL_IN;
            } else {
                iconType = CALL_LOG_ICON_CALL_OUT;
            }
            if (changeIcon(callStatus)) {
                iconType = CALL_LOG_ICON_MISSED;
            }
        } else {
            if (direction == CallLogPOJO.DIRECTION_INCOMING_INT) {
                iconType = CALL_LOG_ICON_FAX_IN;
            } else {
                iconType = CALL_LOG_ICON_FAX_OUT;
            }
            if (changeIcon(callStatus)) {
                iconType = CALL_LOG_ICON_FAXX;
            }
        }
        return iconType;
    }

    public int getCallLogIcon(int direction, int type, int status) {

        int result = R.drawable.ic_call_log_call_missed_white;
        if (isCallIcont(type) == 1) { // Call
            if (direction == CallLogPOJO.DIRECTION_INCOMING_INT) { // incoming
                result = R.drawable.ic_call_log_call_in_white;
            } else { // outgoing
                result = R.drawable.ic_call_log_call_out_white;
            }
            if (changeIcon(status)) { // set X mark
                result = R.drawable.ic_call_log_call_missed_white;
            }
        } else { // Fax
            if (direction == CallLogPOJO.DIRECTION_INCOMING_INT) { // incoming
                result = R.drawable.ic_call_log_fax_in_white;
            } else { // outgoing
                result = R.drawable.ic_call_log_fax_out_white;
            }
            if (changeIcon(status)) { // set X mark
                result = R.drawable.ic_call_log_fax_x_white;
            }
        }
        return result;
    }

    private int isCallIcont(int type) {
        int result = 2;
        switch (type) {
        case CallLogPOJO.CALLTYPE_UNKNOWN:
            result = 2;
            break;
        case CallLogPOJO.CALLTYPE_PHONECALL:
        case CallLogPOJO.CALLTYPE_PHONELOGIN:
        case CallLogPOJO.CALLTYPE_ACCEPTCALL:
        case CallLogPOJO.CALLTYPE_FINDME:
        case CallLogPOJO.CALLTYPE_FOLLOWME:
        case CallLogPOJO.CALLTYPE_CALLRETURN:
        case CallLogPOJO.CALLTYPE_CALLINGCARD:
        case CallLogPOJO.CALLTYPE_RINGDIRECTLY:
        case CallLogPOJO.CALLTYPE_RINGOUTWEB:
        case CallLogPOJO.CALLTYPE_SIPCALL:
        case CallLogPOJO.CALLTYPE_RINGOUTPC:
        case CallLogPOJO.CALLTYPE_RINGME:
        case CallLogPOJO.CALLTYPE_TRANSFERCALL:
        case CallLogPOJO.CALLTYPE_RINGOUTMOBILE:
            result = 1;
            break;
        case CallLogPOJO.CALLTYPE_OUTGOINGFAX:
        case CallLogPOJO.CALLTYPE_FAXRECEIVE:
            result = 0;
            break;
        default:
            result = 2;
            break;
        }
        return result;
    }

    private boolean changeIcon(int status) {
        boolean result = false;
        switch (status) {
        case CallLogPOJO.CALLSTATUS_UNKNOWN:
        case CallLogPOJO.CALLSTATUS_INPROGRESS:
        case CallLogPOJO.CALLSTATUS_ACCEPT:
        case CallLogPOJO.CALLSTATUS_REPLY:
        case CallLogPOJO.CALLSTATUS_FAX_RECEIVE:
        case CallLogPOJO.CALLSTATUS_FAX_ONDEMAND:
        case CallLogPOJO.CALLSTATUS_CONNECTED:
        case CallLogPOJO.CALLSTATUS_FAX_SENT:

        case CallLogPOJO.CALLSTATUS_REJECT:
        case CallLogPOJO.CALLSTATUS_BLOCKED:
        case CallLogPOJO.CALLSTATUS_NOANSWER:
        case CallLogPOJO.CALLSTATUS_INTERNATIONAL_DISABLED:
        case CallLogPOJO.CALLSTATUS_BUSY:
        case CallLogPOJO.CALLSTATUS_SUSPENDED_ACOUNT:
        case CallLogPOJO.CALLSTATUS_CALL_FAILED:
        case CallLogPOJO.CALLSTATUS_CALL_FAILURE:
        case CallLogPOJO.CALLSTATUS_INTERNAL_ERROR:
        case CallLogPOJO.CALLSTATUS_IP_PHONE_OFFLINE:
        case CallLogPOJO.CALLSTATUS_NO_CALLING_CREDIT:
        case CallLogPOJO.CALLSTATUS_RESTRICTED_NUMBER:
        case CallLogPOJO.CALLSTATUS_WRONG_NUMBER:
        case CallLogPOJO.CALLSTATUS_ANSWERED_NOT_ACCEPTED:
        case CallLogPOJO.CALLSTATUS_FORCE_STOPPED:
        case CallLogPOJO.CALLSTATUS_ABANDONED:
        case CallLogPOJO.CALLSTATUS_DECLINED:
            result = false;
            break;
        case CallLogPOJO.CALLSTATUS_VOICEMAIL:
        case CallLogPOJO.CALLSTATUS_MISSED:
        case CallLogPOJO.CALLSTATUS_FAX_RECEIVE_ERROR:
        case CallLogPOJO.CALLSTATUS_FAX_PARTIAL_RECEIVE:
        case CallLogPOJO.CALLSTATUS_FAX_SEND_ERROR:
        case CallLogPOJO.CALLSTATUS_NO_FAX_MACHINE:

            result = true;
            break;

        default:
            result = false;
            break;
        }
        return result;
    }
}
