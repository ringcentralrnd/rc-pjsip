/** 
 * Copyright (C) 2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils;

import android.content.Context;
import android.text.TextUtils;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.AccountInfoTable;
import com.ringcentral.android.ringout.CallValidator;

public class UserInfo {
    /**
     * Defines logging tag.
     */
    private static final String TAG = "[RC]UserInfo";
    
    public static final int ADMIN = 1;
    public static final int USER = 2;
    public static final int DEPARTMENT = 3;
    public static final int TAKE_MESSAGES_ONLY = 4;
    public static final int ANNOUNCEMENTS_ONLY = 5;

    public static final int TIER_SERVICE_TYPE_UNKNOWN = 0;
    public static final int TIER_SERVICE_TYPE_RCMOBILE = 1;
    public static final int TIER_SERVICE_TYPE_RCOFFICE = 2;
    public static final int TIER_SERVICE_TYPE_RCVOICE = 3;
    public static final int TIER_SERVICE_TYPE_RCFAX = 4;
    
    
    /* Tier settings */
    private static boolean sExtensionsAllowed = true;
    private static boolean sCallerIdsAllowed = true;
    private static boolean sDndAllowed = true;

    /**
     * Returns extension type.
     * 
     * @param context
     *            the execution context
     * 
     * @return {@link #ADMIN} or
     *         {@link #USER} or
     *         {@link #DEPARTMENT} or
     *         {@link #TAKE_MESSAGES_ONLY} or
     *         {@link #ANNOUNCEMENTS_ONLY}
     */
    public static int getUserType(Context context) {
        if (RCMProviderHelper.isSystemExtension(context)
                || RCMProviderHelper.getAccessLevel(context).equals(AccountInfoTable.ACCESS_LEVEL_ADMIN)) {
            return ADMIN;
        }
        
        String extensionType = RCMProviderHelper.getExtensionType(context);
        if (AccountInfoTable.EXTENSION_TYPE_DEPARTMENT.equals(extensionType)) {
            return DEPARTMENT;
        } else if (AccountInfoTable.EXTENSION_TYPE_VOICEMAIL.equals(extensionType)){
            return TAKE_MESSAGES_ONLY;
        } else if (AccountInfoTable.EXTENSION_TYPE_ANNOUNCEMENT.equals(extensionType)){
            return ANNOUNCEMENTS_ONLY;
        } else {
            return USER;
        }
    }

    /**
     * Returns account label (account number x extension)
     * 
     * @param context
     *            the execution context
     * 
     * @return the account label
     */
    public static String getAccountLabel(Context context) {
        String accountNumber = PhoneUtils.getLocalCanonical(RCMProviderHelper.getLoginNumber(context));
        String accountExtension = RCMProviderHelper.getLoginExt(context);
        if (accountExtension != null) {
            accountExtension = accountExtension.trim();
            if (accountExtension.length() > 0) {
                accountNumber = accountNumber + " x " + accountExtension;
            }
        }
        return accountNumber;
    }
    
    
    /**
     * Returns account tier type (Tier Service type, e.g. "RCMobile",
     * "RCOffice", "RCVoice", "RCFax"). Valid from 5.0.x.
     * 
     * @param context
     *            the execution context
     * 
     * @return {@link #TIER_SERVICE_TYPE_UNKNOWN} or
     *         {@link #TIER_SERVICE_TYPE_RCFAX} or
     *         {@link #TIER_SERVICE_TYPE_RCMOBILE} or
     *         {@link #TIER_SERVICE_TYPE_RCOFFICE} or
     *         {@link #TIER_SERVICE_TYPE_RCVOICE}
     */
    public static int getTierServiceType(Context context) {
        try {
            return getTierServiceTypeByString(context, RCMProviderHelper.getTierServiceType(context));
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "getTierServiceType():error:", error);
            }
        }
        return TIER_SERVICE_TYPE_UNKNOWN;
    }
    
    public static int getTierServiceTypeByString(Context context, String tierType) {
        if (tierType != null) {
            tierType = tierType.trim();
            if (AccountInfoTable.TIER_SERVICE_TYPE_RCMOBILE.equals(tierType)) {
                return TIER_SERVICE_TYPE_RCMOBILE;
            } else if (AccountInfoTable.TIER_SERVICE_TYPE_RCOFFICE.equals(tierType)) {
                return TIER_SERVICE_TYPE_RCOFFICE;
            } else if (AccountInfoTable.TIER_SERVICE_TYPE_RCFAX.equals(tierType)) {
                return TIER_SERVICE_TYPE_RCFAX;
            } else if (AccountInfoTable.TIER_SERVICE_TYPE_RCVOICE.equals(tierType)) {
                return TIER_SERVICE_TYPE_RCVOICE;
            }
        }
        return TIER_SERVICE_TYPE_UNKNOWN;
    }


    /*
     * Get/set methods for account/extension permissions (tier settings)
     */
    public static boolean companyExtensionsAllowed() {
        return sExtensionsAllowed;
    }

    public static void setCompanyExtensionsAllowed(boolean allowed) {
        sExtensionsAllowed = allowed;
    }

    public static boolean callerIdsAllowed() {
        return sCallerIdsAllowed;
    }

    public static void setCallerIdsAllowed(boolean allowed) {
        sCallerIdsAllowed = allowed;
    }
    
    public static boolean dndAllowed() {
        return sDndAllowed;
    }

    public static void setDndAllowed(boolean allowed) {
        sDndAllowed = allowed;
    }
    
    /**
     * DO NOT USE FOR UI ELEMENTS HIDING, PLEASE
     * DO NOT MIX-UP WITH
     * 
     * Get service API version
     * used for voip status 
     *
     * @param context
     * @return saved value or empty string
     */
    public static int getServiceApiMainVersion(Context context) {
        String full_version = RCMProviderHelper.getServiceApiVersion(context);

        if (LogSettings.MARKET) {
            MktLog.d(TAG, "getServiceApiMainVersion(): " + full_version);
        }
        
        if (TextUtils.isEmpty(full_version)) {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "getServiceApiMainVersion(): no value in DB; return 0");
            }
            return 0;
        }
        
        int end_index = full_version.indexOf('.');
        if (end_index < 0) {
            end_index = full_version.length();  
        }
    
        int main_version;
        try {
            main_version = Integer.parseInt(full_version.substring(0, end_index).trim());
        } catch (NumberFormatException e) {
            main_version = 0;
        }
    
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "getServiceApiMainVersion(): return " + main_version);
        }
        return main_version;
    }

    public static boolean isVoipOnEnvironmentEnabled(Context context) {
        boolean result;
        
        if (getServiceApiMainVersion(context) >= 5) {
            result = true;
        } else {
            result = false;
        }

        if (LogSettings.MARKET) {
            MktLog.d(TAG, "isVoipOnEnvironmentEnabled(): return " + result);
        }
        return result;
    }

    /**
     * Returns if VoIP for current account enabled (TIERS_PHS_DIAL_FROM_CLIENT),
     * by brand/tier ???
     * 
     * @param context
     *            the execution context
     * @return <code>true</code> if enabled, otherwise <code>false</code>
     */
    public static boolean isVoIPForAccountEnabled(Context context) {
        long tierSettings = RCMProviderHelper.getTierSettings(context);

        if ((tierSettings & RCMConstants.TIERS_PHS_DIAL_FROM_CLIENT) == 0) {
            return false;
        }
        return true;
    }
    
    
    public static boolean isCallingAllowed(Context context) {
    	int extensionType = UserInfo.getUserType(context);
    	long tier_settings = RCMProviderHelper.getTierSettings(context);
        return !(extensionType == UserInfo.TAKE_MESSAGES_ONLY || extensionType == UserInfo.ANNOUNCEMENTS_ONLY || extensionType == UserInfo.DEPARTMENT || (tier_settings & RCMConstants.TIERS_PHS_DIAL_FROM_CLIENT) == 0);
    }
}
