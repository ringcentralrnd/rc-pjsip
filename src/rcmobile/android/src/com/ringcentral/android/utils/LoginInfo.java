package com.ringcentral.android.utils;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.provider.RCMProviderHelper;

/**
 * LoginInfo utility class
 */
public class LoginInfo {    	
	private static final String TAG = "[RC]LoginInfo";
	
    public static final String PASSWORD = "password";
    public static final String PHONE = "phone";
    public static final String EXT = "ext";
    public static final String AUTOLOGIN = "autologin";
    public static final String RESTART = "restart";
    public static final String AUTOFILL = "autofill";
	
	public String phone;
	public String ext;
	public String password;
	public boolean autologin;
	public boolean restart;
	public boolean autofill;
    	
	private LoginInfo(){
		this("", "", "", false);
	}    	
	
	public LoginInfo(String phone, String ext, String password, boolean autologin){
		this(phone, ext, password, autologin, false, true);
	}
	
	private LoginInfo(String phone, String ext, String password, boolean autologin, boolean restart, boolean autofill) {
		this.phone = (phone == null) ? "" : phone;
		this.ext = (ext == null) ? "" : ext;
		this.password = (password == null) ? "" : password;
		this.autologin = autologin;
		this.restart = restart;		
		this.autofill = autofill;
	}
	
	/**
	 * Check that credentials are present in intent
	 * @param intent
	 * @return
	 */
	public static boolean inIntent(Intent intent){
		return intent != null &&
    		   !TextUtils.isEmpty(intent.getStringExtra(LoginInfo.PHONE));
	}    	    	
	
	/**
	 * Create login info from Intent
	 * @param intent
	 * @return
	 */
	public static LoginInfo fromIntent(Intent intent){
		if(inIntent(intent)){
			if (LogSettings.ENGINEERING) {
				EngLog.i(TAG, "Create LoginInfo from Intent");
			}    			
			return new LoginInfo(intent.getStringExtra(LoginInfo.PHONE), 
				 				intent.getStringExtra(LoginInfo.EXT), 
				 				intent.getStringExtra(LoginInfo.PASSWORD), 
				 				intent.getBooleanExtra(LoginInfo.AUTOLOGIN, false),
				 				intent.getBooleanExtra(LoginInfo.RESTART, false),
				 				intent.getBooleanExtra(LoginInfo.AUTOFILL, true));
		}
		return empty();
	}
	/**
	 * Check that credentials are present in the DB 
	 * @param context
	 * @return
	 */
	public static boolean inDB(Context context){
		return context != null && 
			   !TextUtils.isEmpty(RCMProviderHelper.getLoginNumber(context)) &&
			   !TextUtils.isEmpty(RCMProviderHelper.getLoginPassword(context));
	}
	
	/**
	 * Create login info from DB
	 * @param intent
	 * @return
	 */
	public static LoginInfo fromDB(Context context){    		
		if(inDB(context)){
			if (LogSettings.ENGINEERING) {
				EngLog.i(TAG, "Create LoginInfo from DB");
			}    
			return new LoginInfo(RCMProviderHelper.getLoginNumber(context), 
					 	RCMProviderHelper.getLoginExt(context), 
					 	RCMProviderHelper.getLoginPassword(context), 
					 	true);
		}
		return empty();
	}
	
	/**
	 * Check that credentials are present in the URI 
	 * @param context
	 * @return
	 */
	public static boolean isURLCommand(Intent intent, String cmd){
		return intent!=null &&
			   !TextUtils.isEmpty(cmd) &&
			   URLCallbackHandler.isUrlCommand(intent, URLCallbackHandler.CMD_URL_PROCEED_WITH_SETUP);
	}
	
	/**
	 * Create login info from URL
	 * @param intent
	 * @return
	 */
	public static LoginInfo fromURL(Intent intent, String cmd){    		
		if(isURLCommand(intent, cmd)){
			if (LogSettings.ENGINEERING) {
				EngLog.i(TAG, "Create LoginInfo from URL");
			}    
			return URLCallbackHandler.getLoginInfo(intent.getData());
		}
		return empty();
	}
	
	/**
	 * Empty creator
	 * @return
	 */
	public static LoginInfo empty(){
		return new LoginInfo();
	}
	
	public static LoginInfo fillLoginInfo(Context context, Intent intent) {
		// Received new Intent
		if (LoginInfo.inIntent(intent)) {
			return LoginInfo.fromIntent(intent);
		} else if (LoginInfo.isURLCommand(intent, URLCallbackHandler.CMD_URL_PROCEED_WITH_SETUP)) {
			if (LoginInfo.inDB(context)) {
				return LoginInfo.fromDB(context);
			} else {
				return LoginInfo.fromURL(intent, URLCallbackHandler.CMD_URL_PROCEED_WITH_SETUP);
			}
		} else if ( LoginInfo.isURLCommand(intent, URLCallbackHandler.CMD_URL_SETTINGS_COMPLETE)&&
				    LoginInfo.inDB(context)){
			return LoginInfo.fromDB(context);
		}else if (LoginInfo.inDB(context)) {
			return LoginInfo.fromDB(context);
		}
		return LoginInfo.empty();
	} 
	
	@Override
	public String toString() {    		
		return  new StringBuffer("LoginInfo phone:").append(phone)
				.append(" ext:").append(ext)
				.append(" autologin:").append(autologin).toString();
	}
}