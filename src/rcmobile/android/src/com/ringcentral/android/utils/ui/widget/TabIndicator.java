/** 
 * Copyright (C) 2011, RingCentral, Inc. 
 * All Rights Reserved.
 */

package com.ringcentral.android.utils.ui.widget;

import java.util.HashMap;
import java.util.Map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;

/**
 * Class to hold TabIndicator and perform specific activities
 * 
 * @author marat.daishev
 */
public class TabIndicator extends RelativeLayout{	
	private static final String TAG = "[RC]TabIndicator";
	
	/** Positive for display some actual number of items, negative or zero to hide number indicator */
	public static final String NUM_ITEMS = "com.ringcentral.android.utils.ui.widget.TabIndicator.NUM_ITEMS";
	/** Not showing indicator in this case number is equal or less than default*/
	public static final int NUM_ITEMS_DEFAULT = 0;
	/** MAX number of items to display otherwise dots*/
	private static final int MAX_NUM_ITEMS = 999;
	/** DOTS pattern */
	private static final String DOTS_PATTERN = "...";		
	
	//view for indicator
	private TextView mIndicatorView;	
	//receiver to listen number change changes
	private IndicatorUpdateReceiver mIndicatorReceiver;
	//Acton to use in intent filter
	private String mIntentFilterAction;
	//static holder is needed, as View will be recreated during orientation change
	//HashMap for future if separate indicators will be needed for separate tabs
	private static Map<String, Integer> mNumberHolder = new HashMap<String, Integer>();
	//indicator number for current view
	private Integer mNumber;
	
	public TabIndicator(Context context) {
		super(context);
	}
	
	public TabIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public TabIndicator(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);	
	}	
	
			
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		if (mIntentFilterAction != null) {						
			if(LogSettings.ENGINEERING){
				EngLog.d(TAG, "attached, Number : " + mNumber + ", Action : " + mIntentFilterAction);								
			}									
			mIndicatorView = (TextView) findViewById(R.id.number);										
			updateIndicator();
		}		
	}
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		
		if(mIntentFilterAction != null){
			getContext().unregisterReceiver(mIndicatorReceiver);
			mIndicatorReceiver = null;
			
			mNumberHolder.remove(mIntentFilterAction);
			if(mNumber != null){
				mNumberHolder.put(mIntentFilterAction, mNumber);				
			}
			mIntentFilterAction = null;
			mIndicatorView = null;
		}							
	}
	
	/**
	 * Set receiver to update indicator with messages number
	 * @param receiver
	 */
	public void setIntentFilter(String filter){
		if(LogSettings.ENGINEERING){
			EngLog.i(TAG, "setIntentFilter : " + filter);
		}
		
		if(filter != null){
			mIntentFilterAction = filter;
			IntentFilter intentFilter = new IntentFilter(mIntentFilterAction);
			mIndicatorReceiver = new IndicatorUpdateReceiver();
			getContext().registerReceiver(mIndicatorReceiver, intentFilter);
			
			mNumber = mNumberHolder.get(mIntentFilterAction);			
			if(LogSettings.ENGINEERING){
				EngLog.d(TAG, "Number : " + mNumber + ", Action : " + mIntentFilterAction);								
			}
		}
	}	
	
	/**
	 * Show numeric indicator
	 * 
	 * @param number number to display in indicator
	 */
	private void showIndicator(int number){
		if(LogSettings.ENGINEERING){
			EngLog.i(TAG, "showIndicator was : " + mNumber + ", new number : " + number);
		}	
		
		if(mNumber == null){
			mNumber = new Integer(number);
			updateIndicator();
		}
		if(number != mNumber.intValue()){
			mNumber = number;		
			updateIndicator();
		}
	}
	
	/**
	 * Hide numeric indicator
	 */
	private void hideIndicator(){
		if(LogSettings.ENGINEERING){
			EngLog.i(TAG, "hideIndicator");
		}
		
		mNumber = null;		 
		updateIndicator();		
	}
	
	/**
	 * Updating current indicator value or simply hiding it
	 */
	private void updateIndicator(){ 
		if(mIndicatorView != null){
			mIndicatorView.setVisibility((mNumber != null)? VISIBLE : GONE);
			if(mNumber != null){
				if(mNumber > MAX_NUM_ITEMS){
					mIndicatorView.setText(DOTS_PATTERN);
				}else{
					mIndicatorView.setText(mNumber.toString());
				}
			}			
		}
	}	
	
	
	/**
	 * Receiver to handle indicator updates
	 */
	private class IndicatorUpdateReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			if(LogSettings.ENGINEERING){
				EngLog.i(TAG, "onReceive, intent has extra : " + intent.hasExtra(NUM_ITEMS) + " extra : " + intent.getIntExtra(NUM_ITEMS, -1));
			}
			
			final Bundle bundle = intent.getExtras();
			if (bundle != null) {
				final int numItems = bundle.getInt(NUM_ITEMS, NUM_ITEMS_DEFAULT);
				if(numItems > NUM_ITEMS_DEFAULT){
					showIndicator(numItems);							
				} else { 
					hideIndicator();
				}
			} else {
				hideIndicator();
			}
		}		
	}

}
