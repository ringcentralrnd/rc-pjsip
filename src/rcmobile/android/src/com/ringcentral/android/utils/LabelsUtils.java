/** 
 * Copyright (C) 2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.text.format.DateUtils;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;

public class LabelsUtils {
    private static final String TAG = "[RC] LabelsUtils";

    public static String getRelativeDateLabel(long time) {
        int flags = DateUtils.FORMAT_ABBREV_RELATIVE;
        long curTime = System.currentTimeMillis();
        if (time > curTime) {
            if (LogSettings.ENGINEERING) {
                EngLog.d(TAG, "getRelativeDateLabel(): Current time is less that time from Server. cur=" + curTime);
            }
            return getDateLabel(time);
        }
        return DateUtils.getRelativeTimeSpanString(time, curTime, DateUtils.MINUTE_IN_MILLIS, flags).toString();
    }
    
    public static String getTimeLabel(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat(SettingsFormatUtils.getTimeFormat());
        return sdf.format(new Date(time));
    }
    
    public static String getTimeLabel(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(SettingsFormatUtils.getTimeFormat());
        return sdf.format(date);
    }

    public static String getDateLabel(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat(SettingsFormatUtils.getDateFormat());
        return sdf.format(new Date(time));
    }
    
    public static String getDateLabel(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(SettingsFormatUtils.getDateFormat());
        return sdf.format(date);
    }
    
    public static String getFormatTimeLabel(long time, String pattern) {
   	 	SimpleDateFormat sdf = new SimpleDateFormat(pattern);
   	 	return sdf.format(new Date(time));
    }
    
    public static String getFormatDateLabel(Date date, String pattern) {
    	 SimpleDateFormat sdf = new SimpleDateFormat(pattern);
         return sdf.format(date);
    }
}
