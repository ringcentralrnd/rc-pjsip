package com.ringcentral.android.utils.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class RODialpadWidget extends ViewGroup {
    private final int mColumns = 1;
    private int mPaddingTop = 0;
    private int mPaddingLeft = 0;
    private int mPaddingRight = 0;
    private int mPaddingBottom = 0;

    public RODialpadWidget(Context context) {
        super(context);
    }

    public RODialpadWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RODialpadWidget(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int y = mPaddingTop;
        int rows = getRows();
        int y2 = (getHeight() - mPaddingBottom - mPaddingTop) / (rows + 1);
        int x2 = (getWidth() - mPaddingLeft - mPaddingRight) / mColumns;
        int cW = x2;
        int cH = y2;

        int yD = (getHeight() - ((rows + 1) * y2));

        for (int row = 0; row < rows; row++) {
            int x = mPaddingLeft;
            cH = y2;
            if (row == 1) {
                cH = yD + (2 * y2);
            }
            for (int col = 0; col < mColumns; col++) {
                int area = col + (row * mColumns);
                if (area >= getChildCount()) {
                    break;
                }
                getChildAt(area).layout(x, y, x + cW, y + cH);
                x += cW;
            }
            y += cH;
        }
    }

    private int getRows() {
        return (getChildCount() + mColumns - 1) / mColumns;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int w = mPaddingLeft + mPaddingRight;
        int h = mPaddingTop + mPaddingBottom;
        View c = getChildAt(0);
        c.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        for (int i = 1; i < getChildCount(); i++) {
            getChildAt(0).measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        }
        h += c.getMeasuredHeight() * getRows();
        h = resolveSize(h, heightMeasureSpec);
        w += mColumns * c.getMeasuredWidth();
        w = resolveSize(w, widthMeasureSpec);
        setMeasuredDimension(w, h);
    }
}
