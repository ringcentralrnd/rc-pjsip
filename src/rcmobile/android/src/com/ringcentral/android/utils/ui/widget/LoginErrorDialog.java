/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils.ui.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Message;
import android.text.TextUtils;

import com.ringcentral.android.R;
import com.ringcentral.android.api.network.NetworkManager;
import com.ringcentral.android.contacts.RcAlertDialog;

public class LoginErrorDialog extends RcAlertDialog {

    public LoginErrorDialog(Context context) {
        super(context);
        initDialog(context, null);
    }

    public LoginErrorDialog(Context context, String message) {
        super(context);
        initDialog(context, message);
    }

    public void setMessage(Context context, Message networkManagerMessage) {
        String msg = (networkManagerMessage == null ?
                context.getString(R.string.generic_network_error)
                : networkManagerMessage.getData().getString(NetworkManager.REQUEST_INFO.MESSAGE_USER));
        if (TextUtils.isEmpty(msg)) {
            msg = context.getString(R.string.generic_network_error);
        }

        setMessage(msg);
    }
    
    private void initDialog(Context context, CharSequence message) {
        setTitle(R.string.error_code_alert_title);
        setIcon(R.drawable.symbol_exclamation);
        setMessage(message != null ? message : "");
        setButton(AlertDialog.BUTTON_POSITIVE,
                context.getString(R.string.dialog_btn_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {}
                });
    }

}
