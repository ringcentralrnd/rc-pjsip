/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.api.network.NetworkManager;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.MailboxCurrentTable;

/**
 * Internal Service for request pooling via AlarmManager.
 * {@see http://developer.android.com/reference/android/app/Service.html}
 */
public class RCMService extends IntentService {
	
	private static final String TAG = "[RC]RCMService";
	private static final String SERVICE_NAME = "SyncService";
	private static final String LOCK_NAME = "RingCentral SyncService"; 

    private Method m_setIntentRedelivery = null;
    private static final Class<?>[] sf_setIntentRedelivery = new Class[] {  boolean.class};
	private final Object[] f_setIntentRedeliveryArgs = new Object[1];
    
	private static volatile PowerManager.WakeLock systemLock = null;

	public RCMService(){
		super(SERVICE_NAME);
		
		try {
			m_setIntentRedelivery = ReflectionHelper.getMethod(getClass(), "setIntentRedelivery", sf_setIntentRedelivery);			
			if( m_setIntentRedelivery != null ){
				f_setIntentRedeliveryArgs[0] = Boolean.TRUE;
				try {
					m_setIntentRedelivery.invoke(this, f_setIntentRedeliveryArgs );
				} catch (IllegalArgumentException e) {
				} catch (IllegalAccessException e) {
				} catch (InvocationTargetException e) {
				}
			}
		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
		}
	}	

	private static synchronized PowerManager.WakeLock getLock(Context context) {
		if (systemLock == null) {
			try{
				PowerManager mgr = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
				systemLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, LOCK_NAME);
				systemLock.setReferenceCounted(true);
			}catch(Throwable t){
				if(LogSettings.MARKET){
					MktLog.e(TAG,"getLock(), error when obtaining log : " + t);
				}
			}
		}
		return systemLock;
	}
	

	public static void scheduleUpdate(Context context, Class<RCMService> clsService) {		
		try{
			final PowerManager.WakeLock lock = getLock(context.getApplicationContext());
			if(LogSettings.MARKET) {
				MktLog.d(TAG, "scheduleUpdate() started, acquiring lock : " + lock);
			}
			lock.acquire();
			context.startService(new Intent(context, clsService));
		}catch(Throwable t){
			if(LogSettings.MARKET){
				MktLog.d(TAG, "scheduleUpdate() error when acquiring lock : " + t);
			}	
		}
	}


	@Override
	protected final void onHandleIntent(Intent intent) {
		try {
		    if (RCMProviderHelper.getCurrentMailboxId(this) != MailboxCurrentTable.MAILBOX_ID_NONE) {
				if (LogSettings.MARKET) {
					MktLog.d(TAG, "onHandleIntent(). Mailbox valid, get AccountInfo (Message and Extension counter).");
				}			
				// M.D. do not substitute call with refresh messages, to prevent NetworkManager blocking
				// M.B. Here we use application context instead of 'this' as NetworkManager operations
				// may be asyncronous and continue after the RCMService has become invalid!!! 
				NetworkManager.getInstance().getAccountInfo(getApplicationContext(), false, false, false);
				NetworkManager.getInstance().checkExtCounter(getApplicationContext(), false);
				NetworkManager.getInstance().checkMsgCounter(getApplicationContext(), false);
				
			} else {
				if (LogSettings.ENGINEERING) {
					EngLog.d(TAG, "onHandleIntent(). MailboxId invalid. Omit.");
				}
			}
		} catch (Throwable t) {
			if (LogSettings.MARKET) {
				MktLog.e(TAG, "onHandleIntent() Exception when handling update", t);
			}
		} finally {			
			try{
				final PowerManager.WakeLock lock = getLock(getApplicationContext());
				if(LogSettings.MARKET){
					EngLog.d(TAG, "onHandleIntent() update finished, releasing lock : " + lock);
				}
				if(lock.isHeld()){
					lock.release();
				}
			}catch(Throwable t){
				MktLog.e(TAG, "onHandleIntent() error when releasing lock : ", t);
			}
		}
	}
}
