/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.text.TextUtils;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.contacts.ContactPhoneNumber;
import com.ringcentral.android.phoneparser.PhoneNumber;
import com.ringcentral.android.phoneparser.PhoneNumberFormatter;
import com.ringcentral.android.phoneparser.PhoneNumberParser;
import com.ringcentral.android.phoneparser.PhoneNumberParserConfiguration;
import com.ringcentral.android.phoneparser.PhoneParserException;


public class PhoneUtils {
	
    /**
     * Define logging tag.
     */
    private static final String TAG = "[RC]PhoneUtils";

    /**
     * Keeps current (account) parser.
     */
    private static PhoneNumberParser s_current_parser = null;
    
    /**
     * Keeps current (vanity) parser.
     */
    private static PhoneNumberParser s_vanity_parser = null;
    
    /**
     * Keeps sync-primitive. 
     */
    private static final Object s_lock = new Object();
    
    /**
     * Defines if the parser initialized.
     */
    private static boolean s_initialized = false;
    
    /**
     * Defines if detail logging required (debugging case).
     */
    private static final boolean DEBUG_DETAILS = false && LogSettings.ENGINEERING;
    
    /**
     * Defines if sanity check required.
     */
    private static final boolean SANITY_TEST = true && DEBUG_DETAILS;
    

    /**
	 * 
	 * @param phoneNum
	 * @return number in format 16502223344
	 */
	public static String getNumRaw(final String phoneNum){
		PhoneNumber phoneToCall =  getParser().parse(phoneNum);
		if ( phoneToCall != null){
			return phoneToCall.numRaw;
		}
		return "";
	}

	public static String getCanonical(final String phoneNum){
		PhoneNumber phoneNumber = getParser().parse(phoneNum);
		if (phoneNumber != null){
			return phoneNumber.canonical;
		}
		return "";
	}

	/**
     * Returns local canonical view of <code>number</code>
     * 
     * @param number
     *            the number to get the local canonical view
     * @return local canonical view of <code>number</code>
     */
    public static String getLocalCanonical(final String number) {
        PhoneNumber phoneNumber = getParser().parse(number);
        if (phoneNumber != null) {
            return phoneNumber.localCanonical;
        }
        return "";
    }
	
	
	/* Compare two raw phone numbers - digits only */   
	public static boolean equals(String phoneNum1, String phoneNum2) {
	    if (TextUtils.isEmpty(phoneNum1) && TextUtils.isEmpty(phoneNum2))
	        return true;
	    
	    if (TextUtils.isEmpty(phoneNum1) || TextUtils.isEmpty(phoneNum2))
	        return false;
	    
	    final int len1 = phoneNum1.length();
	    final int len2 = phoneNum2.length();
	    
	    int i = 0, j = 0;
	    char ch1 = '\u0000', ch2 = '\u0000';
	    
	    for (;;) {
	        while (i < len1  &&  !Character.isDigit(ch1 = phoneNum1.charAt(i)))  i++;
    	    while (j < len2  &&  !Character.isDigit(ch2 = phoneNum2.charAt(j)))  j++;

    	    if (i == len1 && j == len2)
    	        return true;
    	    
    	    if (i == len1 || j == len2)
    	        return false;
	    
    	    if (ch1 != ch2)
    	        return false;
	    
    	    i++; j++;
	    }
	}
	
	/**
     * Returns current (account) parser.
     * 
     * @return the account parser. 
     */
    public static PhoneNumberParser getParser(){
        ensureParsersLoaded();
        return s_current_parser;
    }
    
    /**
     * Returns vanity parser.
     * 
     * @return the vanity parser. 
     */
    public static PhoneNumberParser getVanityParser(){
        ensureParsersLoaded();
        return s_vanity_parser;
    }

    /**
     * Returns contact number container.
     * 
     * @param number the phone number
     * 
     * @return the contact phone number container. 
     */
    public static final ContactPhoneNumber getContactPhoneNumber(String number) {
        ContactPhoneNumber cpn = new ContactPhoneNumber();

        if (number != null && number.trim().length() > 0) {
            cpn.originalNumber = number;

            cpn.normalizedNumber = PhoneNumberFormatter.trimNonNumberSymbols(number);

            if (cpn.normalizedNumber != null && cpn.normalizedNumber.trim().length() > 0) {
                PhoneNumberParser p = PhoneUtils.getParser();
                PhoneNumber pn = new PhoneNumber();

                if (p.parse(cpn.normalizedNumber, pn) != null) {
                    cpn.phoneNumber = pn;
                    cpn.isValid = pn.isValid;
                }
            }
        }
        return cpn;
    }
    
    /**
     * Returns contact number container (without normalizing/trim.
     * 
     * @param number
     *            the phone number
     * 
     * @return the contact phone number container.
     */
    public static final ContactPhoneNumber getContactPhoneNumberWithoutTrim(String number) {
        ContactPhoneNumber cpn = new ContactPhoneNumber();

        if (number != null && number.trim().length() > 0) {
            cpn.originalNumber = number;
            cpn.normalizedNumber = number;

            PhoneNumberParser p = PhoneUtils.getParser();
            PhoneNumber pn = new PhoneNumber();

            if (p.parse(cpn.normalizedNumber, pn) != null) {
                cpn.phoneNumber = pn;
                cpn.isValid = pn.isValid;
                cpn.normalizedNumber = pn.numRaw;
            }
        }
        return cpn;
    }
    
    /**
     * Loads parsers.
     */
    private static final void ensureParsersLoaded() {
        synchronized (s_lock) {
            if (s_initialized) {
                return;
            }
            
            long startTime = 0;
            
            if (DEBUG_DETAILS ) {
                startTime = SystemClock.elapsedRealtime();
            }
            
            try {
                if (!PhoneNumberParserConfiguration.isDataLoaded()) {
                    PhoneNumberParserConfiguration.loadData();
                }
            } catch (Exception ex) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "ensureParsersLoaded:load data:failed:" + ex.getMessage());
                }
                return;
            }

            try {
                s_current_parser = PhoneNumberParserConfiguration.getNewParser();
//                s_current_parser.setNationalAsPriority(true);
                s_current_parser.setStationLocation("1", "650", "011");
            } catch (Exception ex) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "ensureParsersLoaded:default parser creation::failed:" + ex.getMessage());
                }
                return;
            }

            try {
                s_vanity_parser = PhoneNumberParserConfiguration.getNewParser();
//                s_vanity_parser.setNationalAsPriority(true);
                s_vanity_parser.setStationLocation("1", "650", "011");
            } catch (Exception ex) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "ensureParsersLoaded:vanity parser creation::failed:" + ex.getMessage());
                }
                return;
            }

            s_initialized = true;
            
            if (DEBUG_DETAILS ) {
                EngLog.i(TAG, "Loading parsers time(ms): " + (SystemClock.elapsedRealtime() - startTime));
            }
            
            if (SANITY_TEST) {
                try {
                    sanityTest();
                    EngLog.w(TAG, "Parsers: sanity check completed.");
                } catch (Exception ex) {
                    EngLog.e(TAG, "Parsers sanity check failed: " + ex.getMessage());
                }
            }
        }
    }


    /**
     * Sets station location.
     * 
     * @param deviceNumber
     *            the mobile device number
     * @param loginNumber
     *            the login (account) number
     * @param notify
     *            make broadcast notification about statuon location change              
     */
    public static void setStationLocation(String deviceNumber, String loginNumber, boolean notify, Context context) {
        if (TextUtils.isEmpty(deviceNumber) || TextUtils.isEmpty(loginNumber)) {
            return;
        }

        PhoneNumber phone_number_device = PhoneUtils.getPhoneNumber(deviceNumber, new PhoneNumber());
        PhoneNumber phone_number_login = PhoneUtils.getPhoneNumber(loginNumber, new PhoneNumber());
        PhoneNumber phone_number = null;

        if (phone_number_login != null && !phone_number_login.isTollFree) {
            phone_number = phone_number_login;
        } else if (phone_number_device != null && phone_number_device.isFullValid) {
            phone_number = phone_number_device;
        }

        if (phone_number != null) {
            String internationalPrefix = "011";
            int pid = android.os.Process.myPid();
            ensureParsersLoaded();

            if (!s_current_parser.isStationLocationSetTo(phone_number.countryCode, phone_number.areaCode, internationalPrefix)) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "Phone Parser station location changed to: country code [" + phone_number.countryCode + "]" + ", area code [" + phone_number.areaCode
                            + "] by pid:" + pid);
                }

                s_current_parser.setStationLocation(phone_number.countryCode, phone_number.areaCode, internationalPrefix);
            } else {
                if (LogSettings.MARKET && notify) {
                    MktLog.i(TAG, "Phone Parser station location not changed in pid:" + pid + ", however, notification will be sent for concistency");
                }
            }
            
            if (notify) {
                try {
                    if (LogSettings.MARKET && notify) {
                        MktLog.i(TAG, "Phone Parser station location changed notification from pid:" + pid);
                    }
                    Intent bIntent = new Intent(RCMConstants.ACTION_STATION_LOCATION_CHANGE);
                    bIntent.putExtra(EXTRA_COUNTRY_STR, phone_number.countryCode);
                    bIntent.putExtra(EXTRA_AREA_STR, phone_number.areaCode);
                    bIntent.putExtra(EXTRA_INT_PREFIX_STR, internationalPrefix);
                    bIntent.putExtra(EXTRA_PID_INT, pid);
                    context.sendBroadcast(bIntent, RCMConstants.INTERNAL_PERMISSION_NAME);
                } catch (java.lang.Throwable th) {
                    if (LogSettings.MARKET) {
                        if (LogSettings.MARKET && notify) {
                            MktLog.w(TAG, "Phone Parser station location change notification error:" + th.toString());
                        }
                    }
                }
                
            }
        }
    }

    /**
     * Sets station location on notification.
     * 
     * @param intent
     *            the notification intent
     */
    public static void onSetStationLocation(Intent intent) {
        try {
            if (!intent.getAction().equals(RCMConstants.ACTION_STATION_LOCATION_CHANGE)) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "Phone Parser onSetStationLocation(intent) error: invalid action");
                }
                return;
            }

            int pid = android.os.Process.myPid();
            
            String country = intent.getStringExtra(EXTRA_COUNTRY_STR);
            String area = intent.getStringExtra(EXTRA_AREA_STR);
            String intPrefix = intent.getStringExtra(EXTRA_INT_PREFIX_STR);
            int senderPid = intent.getIntExtra(EXTRA_PID_INT, 0);

            if (pid == senderPid) {
                return;
            }

            ensureParsersLoaded();
            
            if (!s_current_parser.isStationLocationSetTo(country, area, intPrefix)) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "Phone Parser onSetStationLocation: country code [" + country + "]" + ", area code [" + area + "] sender pid:"
                            + senderPid + " for pid:" + pid);
                }

                s_current_parser.setStationLocation(country, area, intPrefix);
            } else {
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "Phone Parser onSetStationLocation, no changes in pid:" + pid);
                }
            }
        } catch (java.lang.Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "Phone Parser onSetStationLocation(intent) error:" + th.toString());
            }
        }
    }
    
    /**
     * Key for RCMConstants.ACTION_STATION_LOCATION_CHANGE. Value keeps the country.
     */
    private static final String EXTRA_COUNTRY_STR = "EXTRA_COUNTRY";

    /**
     * Key for RCMConstants.ACTION_STATION_LOCATION_CHANGE. Value keeps the area.
     */
    private static final String EXTRA_AREA_STR = "EXTRA_AREA";

    /**
     * Key for RCMConstants.ACTION_STATION_LOCATION_CHANGE. Value keeps the international prefix.
     */
    private static final String EXTRA_INT_PREFIX_STR = "INT_PREFIX";
    
    /**
     * Key for RCMConstants.ACTION_STATION_LOCATION_CHANGE. Value keeps the PID of the notification initiator.
     */
    private static final String EXTRA_PID_INT = "PID";

    
    
    /**
     * Returns current parser settings configuration as string for logging.
     * 
     * @return current parser settings configuration as string for logging
     */
    public static final String getParserSettingsDescription() {
        ensureParsersLoaded();
        return s_current_parser.getParserSettingsDescription();
    }

    /**
     * Makes parser sanity tests.
     */
    private static void sanityTest() throws PhoneParserException{
        if (SANITY_TEST) {
            PhoneNumberParser p = PhoneNumberParserConfiguration.getNewParser();
            p.setNationalAsPriority(true);
            p.setStationLocation("1", "650", "011");
            
            PhoneNumberParser p2 = PhoneNumberParserConfiguration.getNewParser();
            p2.setNationalAsPriority(true);
            p2.setStationLocation("1", "650", "011");
            
            t(p, "1", "+16504704567", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    false, // isShortNumber
                    false, // isTollFree
                    true,  // isUSAOrCanada
                    "United States", 
                    true, // isInternationalCallTo
                    "1",   // destination
                    false, // useDeviceForDial
                    "+1 (650) 470-4567", //canonical
                    "(650) 470-4567", // localCanonical
                    "1", // countryCode
                    "650", // areaCode
                    "4704567", // number
                    "", // subAddress
                    "16504704567", // dialable
                    "+16504704567", // e164
                    "470-4567", // formattedNumber
                    "16504704567",  // numRow
                    "+16504704567" ); //fullNumber
            
            
            t(p, "2", "16504704567", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    false, // isShortNumber
                    false, // isTollFree
                    true,  // isUSAOrCanada
                    "United States", 
                    false, // isInternationalCallTo
                    "16504704568",   // destination
                    false, // useDeviceForDial
                    "+1 (650) 470-4567", //canonical
                    "(650) 470-4567", // localCanonical
                    "1", // countryCode
                    "650", // areaCode
                    "4704567", // number
                    "", // subAddress
                    "16504704567", // dialable
                    "+16504704567", // e164
                    "470-4567", // formattedNumber
                    "16504704567",  // numRow
                    "+16504704567" ); //fullNumber
            
            
            t(p, "3", "6504704567", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    false, // isShortNumber
                    false, // isTollFree
                    true,  // isUSAOrCanada
                    "United States", 
                    false, // isInternationalCallTo
                    "16504704568",   // destination
                    false, // useDeviceForDial
                    "+1 (650) 470-4567", //canonical
                    "(650) 470-4567", // localCanonical
                    "1", // countryCode
                    "650", // areaCode
                    "4704567", // number
                    "", // subAddress
                    "16504704567", // dialable
                    "+16504704567", // e164
                    "470-4567", // formattedNumber
                    "16504704567",  // numRow
                    "+16504704567" ); //fullNumber
            
            t(p, "4", "8884704567", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    false, // isShortNumber
                    true, // isTollFree
                    true,  // isUSAOrCanada
                    "United States", 
                    false, // isInternationalCallTo
                    "16504704568",   // destination
                    false, // useDeviceForDial
                    "+1 (888) 470-4567", //canonical
                    "(888) 470-4567", // localCanonical
                    "1", // countryCode
                    "888", // areaCode
                    "4704567", // number
                    "", // subAddress
                    "18884704567", // dialable
                    "+18884704567", // e164
                    "470-4567", // formattedNumber
                    "18884704567",  // numRow
                    "+18884704567" ); //fullNumber
           

            t(p, "5", "+79219139901", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    false, // isShortNumber
                    false, // isTollFree
                    false,  // isUSAOrCanada
                    "Russia", 
                    true, // isInternationalCallTo
                    "16504704568",   // destination
                    false, // useDeviceForDial
                    "+7 (921) 913-9901", //canonical
                    "+7 (921) 913-9901", // localCanonical
                    "7", // countryCode
                    "921", // areaCode
                    "9139901", // number
                    "", // subAddress
                    "01179219139901", // dialable
                    "+79219139901", // e164
                    "913-9901", // formattedNumber
                    "79219139901",  // numRow
                    "+79219139901" ); //fullNumber
            
            
            t(p, "6", "4732", 
                    true,  // isValid
                    false,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    true, // isShortNumber
                    false, // isTollFree
                    false,  // isUSAOrCanada
                    "", 
                    true, // isInternationalCallTo
                    "16504704568",   // destination
                    false, // useDeviceForDial
                    "4732", //canonical
                    "4732", // localCanonical
                    "", // countryCode
                    "", // areaCode
                    "4732", // number
                    "", // subAddress
                    "4732", // dialable
                    "4732", // e164
                    "4732", // formattedNumber
                    "4732",  // numRow
                    "4732" ); //fullNumber
            
            t(p, "7", "4561234", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    false, // isShortNumber
                    false, // isTollFree
                    true,  // isUSAOrCanada
                    "United States", 
                    false, // isInternationalCallTo
                    "16504704568",   // destination
                    false, // useDeviceForDial
                    "+1 (650) 456-1234", //canonical
                    "(650) 456-1234", // localCanonical
                    "1", // countryCode
                    "650", // areaCode
                    "4561234", // number
                    "", // subAddress
                    "16504561234", // dialable
                    "+16504561234", // e164
                    "456-1234", // formattedNumber
                    "16504561234",  // numRow
                    "+16504561234" ); //fullNumber
            
            t(p, "8", "911", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    true, // isSpecialNumber
                    false, // isShortNumber
                    false, // isTollFree
                    true,  // isUSAOrCanada
                    "United States", 
                    false, // isInternationalCallTo
                    "16504704568",   // destination
                    true, // useDeviceForDial
                    "911", //canonical
                    "911", // localCanonical
                    "1", // countryCode
                    "", // areaCode
                    "911", // number
                    "", // subAddress
                    "911", // dialable
                    "+1911", // e164
                    "911", // formattedNumber
                    "911",  // numRow
                    "+1911" ); //fullNumber
            
            p.setStationLocation("44", "203", "011");
            
            t(p, "9", "+16504704567", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    false, // isShortNumber
                    false, // isTollFree
                    true,  // isUSAOrCanada
                    "United States", 
                    true, // isInternationalCallTo
                    "1",   // destination
                    false, // useDeviceForDial
                    "+1 650 470 4567", //canonical
                    "+1 650 470 4567", // localCanonical
                    "1", // countryCode
                    "650", // areaCode
                    "4704567", // number
                    "", // subAddress
                    "01116504704567", // dialable
                    "+16504704567", // e164
                    "470 4567", // formattedNumber
                    "16504704567",  // numRow
                    "+16504704567" ); //fullNumber
            
            t(p2, "9-2", "+16504704567", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    false, // isShortNumber
                    false, // isTollFree
                    true,  // isUSAOrCanada
                    "United States", 
                    true, // isInternationalCallTo
                    "1",   // destination
                    false, // useDeviceForDial
                    "+1 (650) 470-4567", //canonical
                    "(650) 470-4567", // localCanonical
                    "1", // countryCode
                    "650", // areaCode
                    "4704567", // number
                    "", // subAddress
                    "16504704567", // dialable
                    "+16504704567", // e164
                    "470-4567", // formattedNumber
                    "16504704567",  // numRow
                    "+16504704567" ); //fullNumber

            t(p, "10", "+79219139901", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    false, // isShortNumber
                    false, // isTollFree
                    false,  // isUSAOrCanada
                    "Russia", 
                    true, // isInternationalCallTo
                    "16504704568",   // destination
                    false, // useDeviceForDial
                    "+7 921 913 9901", //canonical
                    "+7 921 913 9901", // localCanonical
                    "7", // countryCode
                    "921", // areaCode
                    "9139901", // number
                    "", // subAddress
                    "01179219139901", // dialable
                    "+79219139901", // e164
                    "913 9901", // formattedNumber
                    "79219139901",  // numRow
                    "+79219139901" ); //fullNumber
            
            t(p2, "10-2", "16504704567", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    false, // isShortNumber
                    false, // isTollFree
                    true,  // isUSAOrCanada
                    "United States", 
                    false, // isInternationalCallTo
                    "16504704568",   // destination
                    false, // useDeviceForDial
                    "+1 (650) 470-4567", //canonical
                    "(650) 470-4567", // localCanonical
                    "1", // countryCode
                    "650", // areaCode
                    "4704567", // number
                    "", // subAddress
                    "16504704567", // dialable
                    "+16504704567", // e164
                    "470-4567", // formattedNumber
                    "16504704567",  // numRow
                    "+16504704567" ); //fullNumber

            t(p, "11", "9139901", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    false, // isShortNumber
                    false, // isTollFree
                    false,  // isUSAOrCanada
                    "United Kingdom", 
                    false, // isInternationalCallTo
                    "16504704568",   // destination
                    false, // useDeviceForDial
                    "+44 9139901", //canonical
                    "0 913 9901", // localCanonical
                    "44", // countryCode
                    "", // areaCode
                    "9139901", // number
                    "", // subAddress
                    "449139901", // dialable
                    "+449139901", // e164
                    "913 9901", // formattedNumber
                    "449139901",  // numRow
                    "+449139901" ); //fullNumber

            t(p2, "11-4", "8884704567", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    false, // isShortNumber
                    true, // isTollFree
                    true,  // isUSAOrCanada
                    "United States", 
                    false, // isInternationalCallTo
                    "16504704568",   // destination
                    false, // useDeviceForDial
                    "+1 (888) 470-4567", //canonical
                    "(888) 470-4567", // localCanonical
                    "1", // countryCode
                    "888", // areaCode
                    "4704567", // number
                    "", // subAddress
                    "18884704567", // dialable
                    "+18884704567", // e164
                    "470-4567", // formattedNumber
                    "18884704567",  // numRow
                    "+18884704567" ); //fullNumber

                        
            t(p, "12", "0800 048 8569", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    false, // isShortNumber
                    false, // isTollFree
                    false,  // isUSAOrCanada
                    "United Kingdom", 
                    true, // isInternationalCallTo
                    "+16504704568",   // destination
                    false, // useDeviceForDial
                    "+44 800 048 8569", //canonical
                    "0800 048 8569", // localCanonical
                    "44", // countryCode
                    "800", // areaCode
                    "0488569", // number
                    "", // subAddress
                    "448000488569", // dialable
                    "+448000488569", // e164
                    "048 8569", // formattedNumber
                    "448000488569",  // numRow
                    "+448000488569" ); //fullNumber        
            
            t(p2, "12-5", "+79219139901", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    false, // isShortNumber
                    false, // isTollFree
                    false,  // isUSAOrCanada
                    "Russia", 
                    true, // isInternationalCallTo
                    "16504704568",   // destination
                    false, // useDeviceForDial
                    "+7 (921) 913-9901", //canonical
                    "+7 (921) 913-9901", // localCanonical
                    "7", // countryCode
                    "921", // areaCode
                    "9139901", // number
                    "", // subAddress
                    "01179219139901", // dialable
                    "+79219139901", // e164
                    "913-9901", // formattedNumber
                    "79219139901",  // numRow
                    "+79219139901" ); //fullNumber
            
               t(p, "13", "2032970122", 
                    true,  // isValid
                    true,  // isFullValid
                    true,  // isUSAValid
                    false, // isEmpty
                    false, // isSpecialNumber
                    false, // isShortNumber
                    false, // isTollFree
                    false,  // isUSAOrCanada
                    "United Kingdom", 
                    true, // isInternationalCallTo
                    "+16504704568",   // destination
                    false, // useDeviceForDial
                    "+44 203 297 0122", //canonical
                    "0203 297 0122", // localCanonical
                    "44", // countryCode
                    "203", // areaCode
                    "2970122", // number
                    "", // subAddress
                    "442032970122", // dialable
                    "+442032970122", // e164
                    "297 0122", // formattedNumber
                    "442032970122",  // numRow
                    "+442032970122" ); //fullNumber
            
               t(p, "14", "+16504704567", 
                          true,  // isValid
                          true,  // isFullValid
                          true,  // isUSAValid
                          false, // isEmpty
                          false, // isSpecialNumber
                          false, // isShortNumber
                          false, // isTollFree
                          true,  // isUSAOrCanada
                          "United States", 
                          false, // isInternationalCallTo
                          "+16504724136",   // destination
                          false, // useDeviceForDial
                          "+1 650 470 4567", //canonical
                          "+1 650 470 4567", // localCanonical
                          "1", // countryCode
                          "650", // areaCode
                          "4704567", // number
                          "", // subAddress
                          "01116504704567", // dialable
                          "+16504704567", // e164
                          "470 4567", // formattedNumber
                          "16504704567",  // numRow
                          "+16504704567" ); //fullNumber
               
               t(p, "15", "4732", 
                       true,  // isValid
                       false,  // isFullValid
                       true,  // isUSAValid
                       false, // isEmpty
                       false, // isSpecialNumber
                       true, // isShortNumber
                       false, // isTollFree
                       false,  // isUSAOrCanada
                       "", 
                       true, // isInternationalCallTo
                       "+16504704568",   // destination
                       false, // useDeviceForDial
                       "4732", //canonical
                       "4732", // localCanonical
                       "", // countryCode
                       "", // areaCode
                       "4732", // number
                       "", // subAddress
                       "4732", // dialable
                       "4732", // e164
                       "4732", // formattedNumber
                       "4732",  // numRow
                       "4732" ); //fullNumber

            
        }
    }
    
    /**
     * Test case. 
     * @param test identifier
     * @param phoneNumber original number 
     * 
     * @param isValid isValid validation
     * @param isFullValid isFullValid validation
     * @param isUSAValid isUSAValid validation
     * @param isEmpty isEmpty validation
     * @param isSpecialNumber isSpecialNumber validation
     * @param isShortNumber isShortNumber validation
     * @param isTollFree isTollFree validation
     * 
     * @param isUSAOrCanada isUSAOrCanada validation
     * @param countryName countryName validation
     * @param isInternationalCallTo isInternationalCallTo validation
     * @param destination destination for isInternationalCallTo validation
     * @param useDeviceForDial useDeviceForDial validation
     * 
     * @param canonical canonical validation
     * @param localCanonical localCanonical validation
     * @param countryCode countryCode validation
     * @param areaCode areaCode validation
     * @param number number validation
     * 
     * @param subAddress subAddress validation
     * @param dialable dialable validation
     * @param e164 e164 validation
     * @param formattedNumber formattedNumber validation
     * 
     * @param numRow numRow validation
     * @param fullNumber fullNumber validation
     * @throws PhoneParserException an error occurred
     */
    private static void t(PhoneNumberParser parser, String test, String phoneNumber, boolean isValid,
            boolean isFullValid, boolean isUSAValid, boolean isEmpty, boolean isSpecialNumber, boolean isShortNumber,
            boolean isTollFree, boolean isUSAOrCanada, String countryName, boolean isInternationalCallTo,
            String destination, boolean useDeviceForDial, String canonical, String localCanonical, String countryCode,
            String areaCode, String number, String subAddress, String dialable, String e164, String formattedNumber,
            String numRow, String fullNumber

    ) throws PhoneParserException {
        if (SANITY_TEST) {

            PhoneNumber pn = parser.getPhoneNumberContainer();

            parser.parse(phoneNumber, pn);
            boolean success = false;
            if (!(pn.isValid != isValid || !pn.original.equals(phoneNumber) || pn.isFullValid != isFullValid
                    || pn.isUSAValid != isUSAValid || pn.isEmpty != isEmpty || pn.isSpecialNumber != isSpecialNumber
                    || pn.isShortNumber != isShortNumber || pn.isTollFree != isTollFree
                    || isUSAOrCanada != pn.isUSAOrCanada || pn.useDeviceForDial() != useDeviceForDial
                    || !pn.countryName.equals(countryName) || !pn.canonical.equals(canonical)
                    || !pn.localCanonical.equals(localCanonical) || !pn.countryCode.equals(countryCode)
                    || !pn.areaCode.equals(areaCode) || !pn.number.equals(number) || !pn.subAddress.equals(subAddress)
                    || !pn.dialable.equals(dialable) || !pn.e164.equals(e164)
                    || !pn.formattedNumber.equals(formattedNumber) || !pn.numRaw.equals(numRow) || !pn.fullNumber
                    .equals(fullNumber))) {
                PhoneNumber pdest = parser.parse(destination);

                if (pn.isInternationalCallTo(pdest) == isInternationalCallTo) {
                    success = true;
                }
            }

            parser.returnPhoneNumberContainer(pn);

            if (!success) {
                throw new PhoneParserException("PhoneParserTest(" + test + ") Failed");
            }
        }
    }

	public static PhoneNumber getPhoneNumber(String phoneNumber, PhoneNumber container) {
	    phoneNumber = PhoneNumberFormatter.trimNonNumberSymbols(phoneNumber);
	    PhoneNumberParser parser = getParser();
	    if (parser == null) {
	        return null;
	    }
	    return parser.parse(phoneNumber, container);
	}

	public static String parsePhoneNum(String number, boolean isCanonical) {
	    PhoneNumberParser parser = getParser();
	    if (LogSettings.ENGINEERING) {
	        EngLog.d(TAG, "parsePhoneNum(...), number == null? " + (number == null ? "true" : "false, value: " + number));
	    }
	    if (parser != null) {
	        PhoneNumber parsedNumber = parser.parse(number);
	        if (parsedNumber != null) {
	            if (isCanonical) {
	                return parser.parse(number).canonical;
	            } else {
	                return parser.parse(number).numRaw;
	            }
	        }
	    }
	    return number;
	}
}
