package com.ringcentral.android.utils;


import java.io.IOException;
import java.lang.reflect.Field;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.widget.SeekBar;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.utils.Compatibility;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentralApp;

/**
 * Safe mediaplayer wrapper for RCM.
 * Catch all MediaPlayer exception and don't let them throw something outside this class.
 */
public class RCMMediaPlayer {
    private static final String TAG = "[RC] RCMMediaPlayer";
    private final static boolean ADVANCED_AUDIO_MODE_ON = true;

    private static RCMMediaPlayer instance;
    private static MediaPlayer mediaPlayer;
    private static AudioManager mAudioManager;

    // DK: current audio mode - restore when play is stopped
    private int mInitialAudioMode = AudioManager.MODE_CURRENT; // keeps phones audio manager mode
    private boolean mInitialSpeakerState = false; // keeps phone state - flag, is speaker on/off
    private static boolean mInternalSpeakerState = false; // our internal setting, toggle mode

    // IA: work around - Samsung Galaxy S GT-I9000
    // Android sdk 8 has a bug, it's necessary use coefficient 2.8 for media
    // player status/progress bar
    private static double coefficientForMediaPlayerProgressBar = 1;
     
    
    
    private RCMMediaPlayer() {
        mediaPlayer = new MediaPlayer();
        try{
            
            if (Integer.valueOf(android.os.Build.VERSION.SDK) == 8) {
                Class<?> execClass = Class.forName("android.os.Build");
                if (null != execClass) {
                    Field manufacturerF = execClass.getField("MANUFACTURER");
                    
                    if (null != manufacturerF) {
                        String manuf = null;
                        manuf = (String) manufacturerF.get(null);
                        if(0 != manuf.compareToIgnoreCase("samsung")) {
                            coefficientForMediaPlayerProgressBar = 2.8;
                        }
                    }
                }
            }
        }catch (Throwable th){
            
        }
    }

    public static RCMMediaPlayer getInstance() {
        if (instance == null) {
            instance = new RCMMediaPlayer();
        }
        if (mAudioManager == null) {
            mAudioManager = (AudioManager) RingCentralApp.getContextRC().getSystemService(Context.AUDIO_SERVICE);
        }
        return instance;
    }

    public void setSource(Context context, String path) {
        mediaPlayer = MediaPlayer.create(context, Uri.parse(RCMConstants.FILE_PROTOCOL + path));
    }


	public void backupAudioState() {		
		if (mAudioManager != null) {
			mInitialAudioMode = mAudioManager.getMode();
			mInitialSpeakerState = mAudioManager.isSpeakerphoneOn();
			setSpeakerState(mInternalSpeakerState);
			if (LogSettings.MARKET) {
				MktLog.d(TAG, "backupAudioState(), mInitialAudioMode : " + mInitialAudioMode + ", mInitialSpeakerState : " + mInitialSpeakerState + ", mInternalSpeakerState : " + mInternalSpeakerState);
			}
		}
	}

	public void restoreAudioState() {

		if (mAudioManager != null) {
			mInternalSpeakerState = false;
			mAudioManager.setSpeakerphoneOn(mInitialSpeakerState);
			mAudioManager.setMode(mInitialAudioMode);			
			
			if (LogSettings.ENGINEERING) {
				EngLog.d(TAG, "restoreAudioState(), mInitialSpeakerState : "+ mInitialSpeakerState + ", mInitialAudioMode : " + mInitialAudioMode);
			}
		}

	}
    
    /**
     * Play media file via Path to file (cannot be used for resume after pause, use resume() for this).
     * You can specify onCompletionListener and SeekBar for feedback or just type null.
     *
     * @param path                 - to media file
     * @param onCompletionListener - instance of listener or null
     * @param seekBar              - instance of SeekBar or null
     */
    public synchronized int play(String path, MediaPlayer.OnCompletionListener onCompletionListener, MediaPlayer.OnErrorListener onErrorListener, final SeekBar seekBar) {
        try {
            if (mediaPlayer != null) {
                stop();
            }
            mediaPlayer = new MediaPlayer();
            if (mediaPlayer == null) {
                if (LogSettings.ENGINEERING) {
                    EngLog.e(TAG, "cannot create mediaplayer via default constructor.");
                }
                return -1;
            }

            if (onErrorListener != null) {
                mediaPlayer.setOnErrorListener(onErrorListener);
            }

            // WORKAROUND for wav files
            if(path.contains("wav")) {
                coefficientForMediaPlayerProgressBar = 1.0;
            }
            
            mediaPlayer.setDataSource(path);
            mediaPlayer.prepare();

            int duration = mediaPlayer.getDuration();
            mediaPlayer.start();

            if (onCompletionListener != null) {
                mediaPlayer.setOnCompletionListener(onCompletionListener);
            }
            if (seekBar != null) {
                seekBar.setProgress(0);
                seekBar.setMax(mediaPlayer.getDuration());

                new Thread("RCMMediaPlayr:play") {
                    public void run() {
                        while (true) {
                            try {
                                sleep(200);
                                if (mediaPlayer != null) {
                                    if (isPlaying()) {
                                        seekBar.setProgress(getCurrentPosition());
                                    }
                                } else {
                                    seekBar.setProgress(0);
                                    return;
                                }
                            } catch (Exception e) {
                                if (LogSettings.ENGINEERING) {
                                    EngLog.w(TAG, "at SeekBar setProgress", e);
                                }
                            }
                        }
                    }
                }.start();
            }
            return duration;
        } catch (IOException e) {
            if (mediaPlayer != null) {
                mediaPlayer.setOnErrorListener(null);
            }
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "cannot create mediaplayer. ", e);
            }
            return -1;
        }
    }

    public void resume() {
        if (mediaPlayer != null) {
            if (!mediaPlayer.isPlaying()) {
                mediaPlayer.start();
            }
        }
    }

    public void pause() {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
        }
    }

    public synchronized void stop() {

        // IA: Conditional compilation 
        // if advanced playback feature using both speakers is turned on - before stop, will be necessary restore audio settings   
        if (ADVANCED_AUDIO_MODE_ON) {
            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
            }

        }
        
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public synchronized boolean isPlaying() {
        if (mediaPlayer != null) {
            return mediaPlayer.isPlaying();
        } else {
            return false;
        }
    }

    public void setSpeakerState(boolean state) {
        
        if (mAudioManager != null) {
            // IA: Conditional compilation
            // if advanced playback feature is turned on - in case of silent
            // mode audio will go to the internal earpiece speaker,
            // in case of loud mode(speaker picture with big count of waves)
            // audio will be streamed on external speaker
            // in else case
            // audio will transmitted by the old way: thru external speaker,
            // with 2 modes: little bit silent/little bit louder
            if (ADVANCED_AUDIO_MODE_ON) {
                mAudioManager.setSpeakerphoneOn(state);
                setAudioMode(state);

                if (LogSettings.ENGINEERING) {
                    EngLog.d(TAG, "setSpeakerState. " + (state ? "Speaker on & Mode NORMAL" : " Speaker Off & Mode IN_CALL"));
                }
            } else {
                mAudioManager.setSpeakerphoneOn(!mAudioManager.isSpeakerphoneOn());
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    if (!mAudioManager.isSpeakerphoneOn()) {
                        mediaPlayer.setVolume(0.2f, 0.2f);
                    } else {
                        mediaPlayer.setVolume(1.0f, 1.0f);
                    }
                }
            }
        }
    }
    
    private void setAudioMode(boolean speakerState){
    	final int manufacturerCode = Compatibility.getManufacturerCode();
    	final int apiLevel = Compatibility.getApiLevel();
    	
    	if(LogSettings.ENGINEERING){
    		EngLog.d(TAG, "setAudioMode(), manufacturerCode : " + manufacturerCode + ", apiLevel : " + apiLevel);    		    		
    	}
    	
    	mAudioManager.setMode(speakerState ? AudioManager.MODE_NORMAL : AudioManager.MODE_IN_CALL);
    }

    public void toggleSpeakerState() {
        mInternalSpeakerState = !isSystemSpeakerOn();
        setSpeakerState(!isSystemSpeakerOn());
    }
    
    public boolean isSpeakerOn(){
        return mInternalSpeakerState;
    }

    boolean isSystemSpeakerOn() {
        if (mAudioManager != null) {
            return mAudioManager.isSpeakerphoneOn();
        }
        return false;
    }

    public void seekTo(int position) {        
        if (mediaPlayer != null) {
            // INFO : division current position by coefficient is done because
            // of: in Android platform SDK 8 there is bug (coefficient is 2.8)
            // coefficient is calculating during RCMMediaPlayer init
            mediaPlayer.seekTo((int) ((double) position / coefficientForMediaPlayerProgressBar));
        }
    }

    public int getCurrentPosition() {
    	int position = 0;
        if (mediaPlayer != null) {
            // INFO : multiplication current position by coefficient is done
            // because of: in Android platform SDK 8 there is bug (coefficient is 2.8)
            // coefficient is calculating during RCMMediaPlayer init
			try {
				position = (int) ((double) mediaPlayer.getCurrentPosition() * coefficientForMediaPlayerProgressBar);
			} catch (Exception e) {
				MktLog.w(TAG, "getCurrentPosition:"+e);
			}
        }
        return position;
    }

    public String getCurrentPositionAtMin() {
        if (mediaPlayer != null) {
            return formatDuration(getCurrentPosition());
        } else {
            return formatDuration(0);
        }
    }
    
    public static String formatDuration(int time){
        String smin = "00", ssec = "00";
        int min = time / 60000;
        int sec = (time - min * 60000) / 1000;
        if (min < 10) {
            smin = "0" + min;
        } else {
            smin = "" + min;
        }
        if (sec < 10) {
            ssec = "0" + sec;
        } else {
            ssec = "" + sec;
        }
        return smin + ":" + ssec;
    }


	public static int trimToSeconds(int totaltime) {
		int timeInSeconds = totaltime / 1000;
		if (timeInSeconds == 0){
			return 1;
		} else {
			return timeInSeconds;
		}
	}
    

    public boolean isReady() {
        // TODO : more complex checking needed
        if (mediaPlayer != null) {
            return true;
        }
        return false;
    }

}