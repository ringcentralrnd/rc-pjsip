package com.ringcentral.android.utils.ui;

import java.lang.reflect.Field;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;

public class ActivityUtils {
	
	private static final String TAG = "[RC]ActivityUtils";
	private static final String MCLASSFIELD = "mClass";
		
	public static boolean isTopActivity(final Context context, final Class<? extends Activity> activityClass) {
        ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        List<RunningTaskInfo> ti = am.getRunningTasks(1);
        if (!ti.isEmpty()) {
            ComponentName top = ti.get(0).topActivity;
            Field mClassField = null;
            try {
                mClassField = ComponentName.class.getDeclaredField(MCLASSFIELD);
                mClassField.setAccessible(true);
                return (((String) mClassField.get(top)).equals(activityClass.getCanonicalName()));
            } catch (Exception e) {
            }
        }
        return false;
    }
	
	
	/**
	 * Checks if backstack is in foreground(visible to the end user)
	 * if some problems will be occuring with incorrect result, should be called from background thread
	 * @param context context
	 * @return is app on foreground
	 */
	public static boolean isAppOnForeground(Context context) {		
		boolean result = false;
		final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);		
		List<RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
		if (appProcesses != null) {
			final String packageName = context.getPackageName();
			for (RunningAppProcessInfo appProcess : appProcesses) {
				if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND
						&& appProcess.processName.equals(packageName)) {
					result = true;
					break;
				}
			}
		}
		if(LogSettings.ENGINEERING){
			EngLog.v(TAG, "isAppOnForeground : " + result);
		}
		return result;  
	}

	
	/**
	 * Checking that we already have a task stack with base Activity activityClass,
	 * kind of a workaround to be sure that only one instance of TAB activity will present,
	 * manifest launch mode flags sometimes work incorrectly, for details please refer Google ;o)
	 * @param context
	 * @param activityClass
	 * 		class that should be base for backStack
	 * @return
	 */
	public static boolean isBaseActivity(final Context context, final Class<? extends Activity> activityClass) {
		try{
			final ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
			final List<RunningTaskInfo> taskInfo = am.getRunningTasks(1024);        
			if (!taskInfo.isEmpty()) {        	
				for(RunningTaskInfo task : taskInfo){
					if(activityClass.getCanonicalName().equals(task.baseActivity.getClassName())){
						if(LogSettings.ENGINEERING){
							EngLog.v(TAG, "isBaseActivity : true " + activityClass.toString());
						}
						return true;
					}
				}				
			}
		}catch(Throwable t){
			if(LogSettings.MARKET){
				MktLog.e(TAG, "isBaseActivity", t);
			}
		}
		if(LogSettings.ENGINEERING){
			EngLog.v(TAG, "isBaseActivity : false " + activityClass.toString());
		}
        return false;
    }
}
