package com.ringcentral.android.utils.ui.widget;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.LocalActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentral;
import com.ringcentral.android.contacts.RcAlertDialog;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.AccountInfoTable;
import com.ringcentral.android.ringout.RingOut;
import com.ringcentral.android.utils.UserInfo;

public class ActivityManager extends FrameLayout implements OnClickListener {
    protected static final String TAG = "[RC]ActivityManager";
    
    private FrameLayout mFrameView;
    private View 		mCurrentView = null;
    protected int		mCurrentActivityIndex = -1;
    private				List<ActivityRules> mTabRules = new ArrayList<ActivityRules>(5);
    
    protected	LocalActivityManager		mAManager = null;
    private		OnTabChangeListener			mOnTabChangeListener;
    
    
    public ActivityManager(Context context) {
        super(context);
        initManager();
    }

    public ActivityManager(Context context, AttributeSet attrs) {
        super(context, attrs);
        initManager();
    }

    private final void initManager() {
        setFocusableInTouchMode(true);
        setDescendantFocusability(FOCUS_AFTER_DESCENDANTS);
        mCurrentActivityIndex = -1;
        mCurrentView = null;
    }

    public ActivityRules newTabSpec(String tag, Intent intent) {
        return new ActivityRules(tag, intent);
    }

    public void setup() {
        mFrameView = (FrameLayout) findViewById(R.id.tabcontent);
        if (mFrameView == null) {
            throw new IllegalStateException();
        }
        
        
        btn_mainMenuAction = (ImageButton)findViewById(R.id.btn_main_menu_action_menu);
    	btn_mainMenuAction.setOnClickListener(this);
        
        menuView = RMenuWindow.getMainMenuControl(getContext());
        
        if(RCMProviderHelper.getServiceVersion(getContext()) >= AccountInfoTable.SERVICE_VERSION_5) {
        	boolean isFaxTier = (UserInfo.TIER_SERVICE_TYPE_RCFAX == UserInfo.getTierServiceType(getContext()));
            findViewById(R.id.btn_main_menu_action_call).setVisibility(isFaxTier ? View.GONE : View.VISIBLE);
        }
    	findViewById(R.id.btn_main_menu_action_call).setOnClickListener(this);
    	
    }

    public void setup(LocalActivityManager activityGroup) {
        setup();
        mAManager = activityGroup;
    }

    public void addScreen(ActivityRules tabRules) {
        mTabRules.add(tabRules);

        if (mCurrentActivityIndex == -1) {
            setCurrentScreen(0);
        }
    }

    public void clearAllTabs() {
        initManager();
        mFrameView.removeAllViews();
        mTabRules.clear();
        requestLayout();
        invalidate();
        menuView.clearControls();
    }

    public int getCurrentTab() {
        return mCurrentActivityIndex;
    }
    
    public String getCurrentTabTAG() {
    	return mTabRules.get(mCurrentActivityIndex).getTag();
    }

    public String getCurrentTabTag() {
        if (mCurrentActivityIndex >= 0 && mCurrentActivityIndex < mTabRules.size()) {
            return mTabRules.get(mCurrentActivityIndex).getTag();
        }
        return null;
    }

    public View getCurrentView() {
        return mCurrentView;
    }

    public void setCurrentTabByTag(String tag) {
        int i;
        for (i = 0; i < mTabRules.size(); i++) {
            if (mTabRules.get(i).getTag().equals(tag)) {
                setCurrentScreen(i);
                break;
            }
        }
    }

    public FrameLayout getTabContentView() {
        return mFrameView;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
    	if (event.getKeyCode() == KeyEvent.KEYCODE_MENU && event.getAction() == KeyEvent.ACTION_DOWN) {
        	showNavigationMenu();
            return true;
        } else {
        	return super.dispatchKeyEvent(event);
        }
    }

    @Override
    public void dispatchWindowFocusChanged(boolean hasFocus) {
        mCurrentView.dispatchWindowFocusChanged(hasFocus);
    }

    public void setCurrentScreen(int index) {
        if (index < 0 || index >= mTabRules.size()) {
            return;
        }

        if (index == mCurrentActivityIndex) {
            return;
        }

        if (mCurrentActivityIndex != -1) {
            mTabRules.get(mCurrentActivityIndex).tabClosed();
        }
        
        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
            if (mCurrentActivityIndex > RingCentral.TAB_RINGOUT_ID && index == RingCentral.TAB_RINGOUT_ID){
                PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.DIALER_OPEN);
            }
        }
        
        mCurrentActivityIndex = index;
        final ActivityManager.ActivityRules rules = mTabRules.get(index);

        mCurrentView = rules.getTabView();

        if (mCurrentView.getParent() == null) {
            if (mFrameView.indexOfChild(mCurrentView) != -1) {
                mFrameView.removeView(mCurrentView);
            }
            try {
                mFrameView.addView(mCurrentView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
            } catch (IllegalStateException e) {
                if (LogSettings.ENGINEERING) {
                    EngLog.w("[RC] TabManager", "Warning! Temporary fix for PP-2153. " + e.getLocalizedMessage(), e);
                }
            }
        }


        invokeOnTabChangeListener();
    }

    public void setOnTabChangedListener(OnTabChangeListener l) {
        mOnTabChangeListener = l;
    }

    private void invokeOnTabChangeListener() {
        if (mOnTabChangeListener != null) {
            mOnTabChangeListener.onTabChanged(getCurrentTabTag());
        }
    }

    public interface OnTabChangeListener {
        void onTabChanged(String tabId);
    }

    public class ActivityRules {
        private String mTag;
        private Intent mI;

        private View mActiveView;

        private ActivityRules(String tag, Intent intent) {
            mTag = tag;
            mI = intent;
        }

        String getTag() {
            return mTag;
        }

		public View getTabView() {
			if (mAManager == null) {
                throw new IllegalStateException();
            }
            final Window w = mAManager.startActivity(mTag, mI);
            final View wd = w != null ? w.getDecorView() : null;
            if (mActiveView != wd && mActiveView != null) {
                if (mActiveView.getParent() != null) {
                    mFrameView.removeView(mActiveView);
                }
            }
            mActiveView = wd;

            if (mActiveView != null) {
                mActiveView.setVisibility(View.VISIBLE);
                mActiveView.setFocusableInTouchMode(true);
                ((ViewGroup) mActiveView).setDescendantFocusability(FOCUS_AFTER_DESCENDANTS);
            }
            
            return mActiveView;
		}

		public void tabClosed() {
			if (mActiveView != null) {
                mActiveView.setVisibility(View.GONE);
            }
		}
    }
    
    /********************************************************************************************************************************************
     * NEW UI TRASH
     *********************************************************************************************************************************************/
    private ImageButton						btn_mainMenuAction;
    private RMenuWindow.MainMenu			menuView;

	public void showNavigationMenu() {
		btn_mainMenuAction.performClick();
	}

	@Override
	public void onClick(View v) {
		if(null == v) {
			return;
		}
		
		switch(v.getId()) {
		case R.id.btn_main_menu_action_menu:
			View bottomBar = findViewById(R.id.bottomBar);
			menuView.show(bottomBar);
			break;
		case R.id.btn_main_menu_action_call:
			if(UserInfo.isCallingAllowed(getContext())) {
				Intent i = new Intent();
				i.setClass(getContext(), RingOut.class);
				getContext().startActivity(i);
			} else {
				alertDialog(R.string.call_error_title_call_error, R.string.no_permissions).show();
			}
			
			break;
		}
	}
	
		
	private AlertDialog alertDialog(int title, int message) {
		return RcAlertDialog
				.getBuilder(getContext())
				.setTitle(title)
				.setMessage(message)
				.setIcon(R.drawable.symbol_error)
				.setPositiveButton(R.string.dialog_btn_ok,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int whichButton) {
								dialog.dismiss();
							}
						}).create();
	}
}