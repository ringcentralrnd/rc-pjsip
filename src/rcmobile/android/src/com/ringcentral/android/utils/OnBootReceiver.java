/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.api.httpreg.HttpRegisterExecutor;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.MailboxCurrentTable;

/**
 * Receive system intent after device is booted and start our service.
 */
public class OnBootReceiver extends BroadcastReceiver {

    private static final String TAG = "[RC]OnBootReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {    	
        if (!Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            return;
        }

        if (LogSettings.MARKET) {
            MktLog.d(TAG, "onReceive(): System boot event received");
        }

        if (RCMProviderHelper.getCurrentMailboxId(context) == MailboxCurrentTable.MAILBOX_ID_NONE) {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "onReceive(): No mailbox ID; return");
            }
            return;
        }
        
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "onReceive(): start HTTP registration");
        }
        // Execute HTTP registration for SIP flags receiving
        new HttpRegisterExecutor().execute(context);
        
        if (LogSettings.MARKET) {
			MktLog.d(TAG, "onReceive(): starting timer.");
		}
        scheduleTimer(context, true);
    }
    
    
    public static synchronized void scheduleTimer(Context context, boolean onBoot){
    	
    	if (RCMProviderHelper.getCurrentMailboxId(context) == MailboxCurrentTable.MAILBOX_ID_NONE) {

    		if (LogSettings.MARKET) {
    			MktLog.d(TAG, "scheduleTimer(). Mailbox is invalid, ignoring...");
    		}
    		return;
    	}
    	
    	int interval = RCMProviderHelper.getSyncInterval( context );
    	if (LogSettings.ENGINEERING) {
    		EngLog.d(TAG, "scheduleTimer(). Scheduling timer, interval : " + interval + " onBoot : " + onBoot);
        }

    	if( interval == 0 ){
    		if (LogSettings.MARKET) {
    			MktLog.d(TAG, "scheduleTimer(). Sync interval is set to 0 (disabled), ignoring...");
    		}
    		return;
    	}
    	
    	Intent alarmIntent = new Intent(context, RepeatingAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
                    
    	AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    	alarmMgr.cancel(pendingIntent);
    	alarmMgr.setRepeating(
    			AlarmManager.ELAPSED_REALTIME_WAKEUP,
    			( onBoot ? 
    					(SystemClock.elapsedRealtime() + RCMConstants.SERVICE_FIRST_RUN_AFTER_CREATING  ) 
    					: SystemClock.elapsedRealtime() 
    			),	
    			RCMConstants.SERVICE_INTERVAL * 1000,
    			pendingIntent);
    }
    
    public static void cancelTimer(Context context){
    	if (LogSettings.ENGINEERING) {
    		EngLog.d(TAG, "cancelTimer().");
        }
    	Intent alarmIntent = new Intent(context, RepeatingAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
        
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmMgr.cancel(pendingIntent);    	
    }

}
