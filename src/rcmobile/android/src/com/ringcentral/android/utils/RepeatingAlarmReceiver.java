/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.MailboxCurrentTable;

public class RepeatingAlarmReceiver extends BroadcastReceiver {

    private static final String TAG = "[RC]RepeatingAlarmReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "onReceive(). Get AccountInfo pooling timer expired");
        }    	    	

        if (RCMProviderHelper.getCurrentMailboxId(context) != MailboxCurrentTable.MAILBOX_ID_NONE) {
            if (LogSettings.QA) {
                QaLog.d(TAG, "onReceive(). Mailbox valid");
            }
            RCMService.scheduleUpdate(context, RCMService.class);
        } else {
        	if (LogSettings.QA) {
                QaLog.d(TAG, "onReceive(). Mailbox invalid");
            }
        }
    }
}
