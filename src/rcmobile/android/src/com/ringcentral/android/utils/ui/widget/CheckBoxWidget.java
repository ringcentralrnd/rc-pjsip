package com.ringcentral.android.utils.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.ringcentral.android.R;

public class CheckBoxWidget extends LinearLayout {

    private CheckBox mCB;

    public CheckBoxWidget(Context context) {
        super(context);
    }

    public CheckBoxWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        setDrawingCacheEnabled(true);
        setAlwaysDrawnWithCacheEnabled(true);
        mCB = (CheckBox) findViewById(R.id.checkbox);
        mCB.setClickable(false);
        mCB.setFocusable(false);
    }

    @Override
    public boolean performClick() {
        mCB.setChecked(!mCB.isChecked());
        return super.performClick();
    }

    @Override
    public void setPressed(boolean pressed) {
        // Log.i("ListRowToggleButton","setPressed: pressed= "+pressed);
        // if (pressed){
        // //mBotton.setChecked(!mBotton.isChecked()); // Toggle Button
        // }
        super.setPressed(pressed);
    }

}
