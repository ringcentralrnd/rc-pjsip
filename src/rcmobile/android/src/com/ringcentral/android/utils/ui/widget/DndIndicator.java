/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils.ui.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.AccountInfoTable;
import com.ringcentral.android.settings.DNDialog;
import com.ringcentral.android.utils.DndManager;
import com.ringcentral.android.utils.UserInfo;

public class DndIndicator extends ImageView {
    private static final String TAG = "[RC]DndIndicator";

    private StateChangeReceiver stateChangeReceiver = new StateChangeReceiver();

    public DndIndicator(Context context) {
        super(context);
        init();
    }

    public DndIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DndIndicator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    
    private void init(){
    	setImageResource(getStatusIcon(getContext()));
    	updateDndIconVisibility(getContext());
        getContext().registerReceiver(stateChangeReceiver, new IntentFilter(RCMConstants.ACTION_DND_STATUS_CHANGED));
        
//        setOnClickListener(new View.OnClickListener() {			
//			@Override
//			public void onClick(View v) {
//				DNDialog.showDNDDialog(getContext());	
//			}
//		});
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        
        setImageResource(getStatusIcon(super.getContext()));
        updateDndIconVisibility(super.getContext());
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        
        getContext().unregisterReceiver(stateChangeReceiver);
        setOnClickListener(null);
    }

    private int getStatusIcon(Context context) {
        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "getStatusIcon");
        }
        
        setVisibility(UserInfo.dndAllowed() ? View.VISIBLE : View.GONE);
                
        if (AccountInfoTable.SERVICE_VERSION_5 <= RCMProviderHelper.getServiceVersion(context)) {
        	int dndStatus = DndManager.getInstance().getExtendedDndStatus(context);
        	switch (dndStatus) {
				case DndManager.DND_STATUS_TAKE_ALL_CALLS:
					return R.drawable.ic_dnd_off;
				case DndManager.DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS:
					return R.drawable.ic_dnd_depts;			
				case DndManager.DND_STATUS_TAKE_DEPARTMENT_CALLS_ONLY:
					return R.drawable.ic_dnd_depts_only;
				case DndManager.DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS:
					return R.drawable.ic_dnd_on;			
				default:
					if(LogSettings.MARKET){
						MktLog.e(TAG, "CARRAMBA, unknown dnd status, temporary settings DND-free icon status : " + dndStatus);
					}
					return R.drawable.ic_dnd_off;					
			}
        } else { // AccountInfoTable.SERVICE_VERSION_4
	        if (DndManager.getInstance().getDndStatus(context)) {
	            return R.drawable.ic_dnd_on;
	        } else {
	            return R.drawable.ic_dnd_off;
	        }
        }
    }
    
    private boolean updateVisibility(Context context) {
    	boolean result = false;
    	if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "updateVisibility");
        }
     
        if (AccountInfoTable.SERVICE_VERSION_5 <= RCMProviderHelper.getServiceVersion(context)) {
        	int dndStatus = DndManager.getInstance().getExtendedDndStatus(context);
        	
        	switch (dndStatus) {
				case DndManager.DND_STATUS_TAKE_ALL_CALLS:
					result = true;
					break;
				case DndManager.DND_STATUS_DO_NOT_ACCEPT_DEPARTMENT_CALLS:
					result = false;	
					break;
				case DndManager.DND_STATUS_TAKE_DEPARTMENT_CALLS_ONLY:
					result = false;	
					break;
				case DndManager.DND_STATUS_DO_NOT_ACCEPT_ANY_CALLS:
					result = false;	
					break;
				default:
					if(LogSettings.MARKET){
						MktLog.e(TAG, "CARRAMBA, unknown dnd status, temporary settings DND-free icon status : " + dndStatus);
					}
					result = true;				
			}
        } else { // AccountInfoTable.SERVICE_VERSION_4
	        if (DndManager.getInstance().getDndStatus(context)) {
	        	result = false;
	        } else {
	        	
	        	result = true;
	        }
        }
        return result;
    }
    
    
    private void updateDndIconVisibility(Context context){
    	boolean result = updateVisibility(context);
    
    	if(result) {
    		setVisibility(View.GONE);
    	}else {
    		setVisibility(View.VISIBLE);
    	}
    }
    
    
    /**
     * DND Update receiver
     *
     */
    public class StateChangeReceiver extends BroadcastReceiver {
    	
        @Override
        public void onReceive(Context context, Intent intent) {
        	if (LogSettings.ENGINEERING) {
                EngLog.d(TAG, "StateChangeReceiver : onReceive, updating image resource");
            }
        	
            setImageResource(getStatusIcon(context));
            updateDndIconVisibility(context);
        }
    }
    
}
