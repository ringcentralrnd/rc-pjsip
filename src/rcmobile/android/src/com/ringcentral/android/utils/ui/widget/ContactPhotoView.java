/** 
 * Copyright (C) 2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.utils.ui.widget;


import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * View with rounded corners
 *
 */
public class ContactPhotoView extends ImageView {
	
	private static final String TAG = "[RC]ContactPhotoView";
		
	private static final int DEFAULT_PHOTO_SIZE = 96;
	private static final int DEFAULT_CORNER_RADIUS = 15; // for 96x96 image..	
	private static final int DEFAULT_CONTACT_PHOTO = R.drawable.ic_contact_list_picture_96x96;
	
	private int mCornerRadius = DEFAULT_CORNER_RADIUS;
	private int mViewSize = DEFAULT_PHOTO_SIZE;	
		
	public ContactPhotoView(Context context) {
		super(context);
		init(context, null);
	}
	
	public ContactPhotoView(Context context, AttributeSet attrs) {		
		this(context, attrs, 0);
		
	}

	public ContactPhotoView(Context context, AttributeSet attrs, int defStyle) {		
		super(context, attrs, defStyle);
		init(context, attrs);
		
	}
	
	private void init(Context context, AttributeSet attrs){
		if(LogSettings.ENGINEERING) {
			EngLog.d(TAG, "init.. reading xml attributes");
		}
		
		if(attrs != null){
			final TypedArray a = getContext().obtainStyledAttributes(attrs,R.styleable.RoundedCornerImageViewStyle);
			mCornerRadius = a.getInt(R.styleable.RoundedCornerImageViewStyle_corner_radius, DEFAULT_CORNER_RADIUS);
			mViewSize = a.getInt(R.styleable.RoundedCornerImageViewStyle_image_size, DEFAULT_PHOTO_SIZE);
			a.recycle();						
			
			if(LogSettings.ENGINEERING){
				EngLog.d(TAG, "Init, corner radius : " + mCornerRadius + " photo size :" + mViewSize);
			}
			
		}		
		setContactPhoto(null);
	}
	
	public void setContactPhoto(Bitmap photo) {						
		if(photo == null){			
			setImageDrawable(getContext().getResources().getDrawable(DEFAULT_CONTACT_PHOTO));			
		} else {
			setImageDrawable(new BitmapDrawable(photo));
		}
	}
	
	/**
	 * All other methods for settings images will pass through this one
	 */
	@Override
	public void setImageDrawable(Drawable drawable) {
		if(drawable != null) {
			BitmapDrawable roundedDrawable = null;
			try {
				Bitmap srcBitmap = ((BitmapDrawable)drawable).getBitmap();		
				if(LogSettings.ENGINEERING) {
					EngLog.d(TAG, "setImageDrawable, width : " + srcBitmap.getWidth() + " height : " + srcBitmap.getHeight());
				}							
				roundedDrawable = new BitmapDrawable(getRoundedCornerBitmap(getContext(), srcBitmap, mCornerRadius));
			} catch (Throwable e) {
				if(LogSettings.MARKET){
					MktLog.e(TAG, "Exception when creation rounded corners, possibly OOM", e);					
				}
			}
			if(roundedDrawable != null){
				drawable = roundedDrawable;
			}
						
		}
		super.setImageDrawable(drawable);
	}
	
	@Override
	public void setImageBitmap(Bitmap bm) {
		setContactPhoto(bm);
	}
	
	@Override
	public void setImageResource(int resId) {
		throw new RuntimeException("Not supported");
	}
	
	@Override
	public void setImageURI(Uri uri) {
		throw new RuntimeException("Not supported");
	}
	
	/**
	 * Create a bitmap with rounded corners
	 * @param bitmap original bitmap
	 * @param cornerRadius corner radius in pixels 
	 * @return bitmap with rounded corners
	 */
	public static Bitmap getRoundedCornerBitmap(Context context, Bitmap bitmap, int cornerRadius) {
		Bitmap result = null;
		try {
			final int width = bitmap.getWidth();
			final int height = bitmap.getHeight();
			final float scaleFactor = Math.min(((float) DEFAULT_PHOTO_SIZE) / (width) , ((float) DEFAULT_PHOTO_SIZE) / (height));
			final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());        
	        final RectF rectF = new RectF(0,0, width* scaleFactor , height * scaleFactor );
			
			result = Bitmap.createBitmap(DEFAULT_PHOTO_SIZE, DEFAULT_PHOTO_SIZE, Config.ARGB_8888);
			
	        final Canvas canvas = new Canvas(result);	        
	        final Paint paint = new Paint();	        	        	        
	        paint.setAntiAlias(true);
	        paint.setColor(Color.WHITE);	        
	        canvas.drawARGB(0, 0, 0, 0); // filling with transparent background
	        canvas.drawRoundRect(rectF, cornerRadius, cornerRadius, paint);	
	        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN)); // applying alpha composing        
	        canvas.drawBitmap(bitmap, rect, rectF, paint); // finally paint to bitmap
		} catch (Throwable t) {
			if (LogSettings.MARKET) {
				MktLog.e(TAG, "getRoundedCornerBitmap, possible OOM", t);
			}
		} 
        return result;
    }
	

	
}
