package com.ringcentral.android;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;
import android.widget.TextView;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.statistics.FlurryTypes;


public class LogObserveScreen extends Activity {
    static final String TAG = "[RC] LogObserveScreen ";
    static final String logContentKey = "logContent";
    static final int    LOG_READING_COMPLETED = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.log_observe);
        
        try {
            Bundle extras = getIntent().getExtras();
            
            if (null != extras) {
                String logContent = null;
                logContent = extras.getString(logContentKey);
                                
                TextView text = ((TextView) findViewById(R.id.logText));
                
                if (null != text && null != text) {
                    Thread rt = new Thread(new ReadThread(logContent), "LogObserver:onCreate");
                    if(null != rt) {
                        rt.start();
                    }
                }
            }
        } catch (Throwable th) {
            if(LogSettings.MARKET) {
                MktLog.e(TAG, " onCreate ", th);
            }
        }
    }
    
    Handler hndlr = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try {
                if (null != msg) {
                    switch (msg.what) {
                    case LOG_READING_COMPLETED:
                        TextView text = ((TextView) findViewById(R.id.logText));

                        if(null != text && null != msg.obj){
                            text.setText((String)msg.obj);
                        }
                        break;
                    default:
                        break;
                    }
                }
            } catch (Throwable th) {
                if(LogSettings.MARKET) {
                    MktLog.e(TAG, " handler ", th);
                }
            }
        }
    };

    class ReadThread implements Runnable {
        String fileContainer = null;

        @Override
        public void run() {
            if(null != fileContainer) {
                try{
                String result = readLogFromLog(this.fileContainer);
                Message msgToSend = hndlr.obtainMessage();
                
                if(null != msgToSend) {
                    msgToSend.what = LOG_READING_COMPLETED;
                    msgToSend.obj = result;
                    msgToSend.sendToTarget();
                }
                } catch (Throwable th) {
                    if(LogSettings.MARKET) {
                        MktLog.e(TAG, " thread ", th);
                    }
                }
            }
        }
        
        public ReadThread(String str) {
            if(null != str) {
                fileContainer = str;
            }
        }
    }
    
    String readLogFromLog(String logContent) {
        StringBuffer sbfr  = null;
        
        if(null != logContent) {
            DataInputStream in = null;
            BufferedReader br = null;
            FileInputStream fstream = null;
            File file = null;
            sbfr = new StringBuffer();
            sbfr.append("");
            try {
                file = new File(logContent);
                fstream = new FileInputStream(logContent);
                in = new DataInputStream(fstream);
                br = new BufferedReader(new InputStreamReader(in));
                
                String strLine = null;

                while ((strLine = br.readLine()) != null) {
                    sbfr.append(strLine);
                    sbfr.append("\n");
                }
            } catch (Throwable th) {
                if(LogSettings.MARKET) {
                    MktLog.e(TAG, " reading issue ", th);
                }
            } finally {
                try {
                    if (null != in) {
                        in.close();
                    }
                    if(null != br) {
                        br.close();
                    }
                    if(null != fstream) {
                        fstream.close();
                    }
                    if(null != file && file.exists()) {
                        file.delete();
                    }
                } catch (Throwable tth) {
                    MktLog.e(TAG, " unbelieveable exception ", tth);
                } 
            }
        }
        
        return null == sbfr ? null : sbfr.toString();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryTypes.onStartSession(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryTypes.onEndSession(this);
    }
}
