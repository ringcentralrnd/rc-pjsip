package com.ringcentral.android.voip;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.sip.client.CompletionInfo;
import com.rcbase.android.sip.client.SipClient;
import com.rcbase.android.sip.service.CallInfo;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.contacts.Cont;
import com.ringcentral.android.utils.PhoneUtils;
import com.ringcentral.android.utils.ui.widget.ContactPhotoView;

/**
 * Let it be a kind of a Mediator class
 *
 */
public class CallInfoView extends FrameLayout {
	
	private static final String TAG = "[RC]CallInfo";
	
	public static final int STATE_OUTGOING = 0;
	public static final int STATE_INCOMING = 1;
	
	private static final int OUTGOING_SINGLE = 2;
	private static final int OUTGOING_MULT = 3;
	
	//UI elements block
	private View mCallerInfoFrame;
	private TextView mCallStatus;
	private TextView mCallDuration;
	private TextView mCallerName; 
	private TextView mCallerNumber;
	private ContactPhotoView mPhoto;
	
	private int mViewState = STATE_OUTGOING;
	private int mOutgoingState = OUTGOING_SINGLE;
	
	public CallInfoView(Context context) {
		super(context);
		init(context, null);
	}

	public CallInfoView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}
	
	public CallInfoView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs);
	}
	
	private void init(Context context, AttributeSet attrs){
		final LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.call_info_view, this, true);		
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "onFinishInflate called ");
		}
		
		mCallerInfoFrame = findViewById(R.id.voip_call_info_caller_view);
		mCallStatus = (TextView) findViewById(R.id.voip_call_info_call_status);
		mCallDuration = (TextView) findViewById(R.id.voip_call_info_call_duration);
		mPhoto = (ContactPhotoView) findViewById(R.id.voip_call_info_caller_photo);
		mCallerName = (TextView) findViewById(R.id.voip_call_info_caller_name);
		mCallerNumber = (TextView) findViewById(R.id.voip_call_info_caller_number);		
		
		checkOutgoingState(null);
	}
	
	
	public void updateControls(SipClient sipClient, CallInfo callInfo, CompletionInfo completionInfo) {
		checkOutgoingState(sipClient);
		
		if(mOutgoingState == OUTGOING_SINGLE){
			updateSingleCallControl(sipClient, callInfo, completionInfo);
		}
		
				
	}	
	
	private void updateSingleCallControl(SipClient sipClient, CallInfo callInfo, CompletionInfo completionInfo){
		//updating ui elements
		final String contactName = getContactName(callInfo);
		String contactNumber = null;
		if(TextUtils.isEmpty(contactName)){
			contactNumber = getContactNumber(callInfo);			
		}
		
		mCallerName.setVisibility(TextUtils.isEmpty(contactName) ? GONE : VISIBLE);
		mCallerName.setText(contactName);
		
		mCallerNumber.setVisibility(TextUtils.isEmpty(contactNumber) ? GONE : VISIBLE);
		mCallerNumber.setText(contactNumber);		
	}
	
	/**
	 * Get contact name, first remoteContact(set during outbound initiation) then from SIP headers
	 * @param callInfo
	 * @return
	 */
	private String getContactName(CallInfo callInfo) {
		String contactName = null;
		
		if (callInfo != null) {
			//try to get local name if present
			contactName = CallInfo.isUnknown(callInfo.getRemoteContact()) ? 
								CallInfo.isUnknown(callInfo.getFromName()) ? 
										null : 
										callInfo.getFromName() :
								callInfo.getRemoteContact();
		}
		return contactName;
	}
	
	private String getContactNumber(CallInfo callInfo) {
		String contactNumber = null;
		if (callInfo != null) {
			contactNumber = CallInfo.getNumberFromURI(callInfo.getFromNumber());  
			contactNumber = (contactNumber == null) ?
		 		 	null :
		 		 	CallInfo.isUnknown(contactNumber) ? 
		 		 			getContext().getString(R.string.calllog_display_name_unknown) : 
		 		 			PhoneUtils.getLocalCanonical(contactNumber);
		}		
		return contactNumber;
	}

	
	
	
	private void checkOutgoingState(SipClient sipClient){
		final int callState = ((sipClient != null) && (sipClient.getActiveLines() > VoipCallStatusActivity.LINES_LIST_ENABLED)) ?
											OUTGOING_MULT :
											OUTGOING_SINGLE;
		if(callState != mOutgoingState){
			mOutgoingState = callState;
			mCallerInfoFrame.setVisibility((mOutgoingState == OUTGOING_MULT) ? GONE : VISIBLE);
		}
	}	

}
