package com.ringcentral.android.voip;

import android.content.Context;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.HapticFeedbackConstants;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.sip.client.SipClient;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.utils.ToneGeneratorUtils;

public class CallDialpad extends LinearLayout implements View.OnClickListener {
	private static final String TAG = "[RC]CallDialpad";
	
	//M.D. not sure should be enabled by default, not applicable for most devices
	private static final boolean HAPTIC_ENABLED = true;
	//M.D. will be flag controlled until requirements are established
	private static final boolean DIGITS_ENABLED = true;
	private static final boolean TONES_ENABLED = false;
		
	private EditText mTxtDigits;
	private SipClient mSipClient;
	private ToneGeneratorUtils mToneGenerator;
	//local value for entered text
	private static String mDigits;

	public CallDialpad(Context context) {
		super(context);
	}
	
	public CallDialpad(Context context, AttributeSet attrs) {
		super(context, attrs);
	}			
	
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		
		for(KeypressHandler handler : KeypressHandler.HANDLERS)
			findViewById(handler.viewId).setOnClickListener(this);
		
		mTxtDigits = (EditText) findViewById(R.id.dialpadDigits);		
        if(mTxtDigits != null){        	
        	mTxtDigits.setVisibility(DIGITS_ENABLED ? VISIBLE : GONE);        	
        	if(!TextUtils.isEmpty(mDigits)){        		
        		mTxtDigits.getText().append(mDigits);        		
        	} else {
        		mDigits = new String();
        	}
        }
        
	}
	
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if(mDigits != null)
			mDigits = mTxtDigits.getText().toString();
		mTxtDigits = null;
		hideDialpad();
		for(KeypressHandler handler : KeypressHandler.HANDLERS){
			final View view= findViewById(handler.viewId);
			if(view != null)
				view.setOnClickListener(null);
		}		
	}
		
	@Override
	public void onClick(View v) {
	    if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
	        PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.DIALPAD_DTMF);
	    }
		final KeypressHandler handler = KeypressHandler.getHandler(v.getId());
		
		if(HAPTIC_ENABLED){
			v.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
		}
		
		if(mTxtDigits != null && DIGITS_ENABLED){
			mTxtDigits.getKeyListener().onKeyDown(
		                mTxtDigits,
		                mTxtDigits.getEditableText(),
		                handler.keycodeId,
		                new KeyEvent(KeyEvent.ACTION_DOWN, handler.keycodeId));
		}
		
		if(mSipClient != null){
			mSipClient.dialDTMF(handler.dtmfTone);
		}
		if(TONES_ENABLED) {
			if(mToneGenerator != null){
				mToneGenerator.playTone(handler.toneId, ToneGeneratorUtils.SHORT_KEY_TONE_LENGTH_MS);
			}
		}
		if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
            PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.DIALPAD_DTMF);
        }
	}
	
	/**
	 * Called to show dialpad on the screen
	 * @param sipClient handler to SipClient object to send dtmf
	 */
	public void showDialpad(SipClient sipClient){
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "showDialpad : " + sipClient);
		}
		
		this.mSipClient = sipClient;
		setVisibility(VISIBLE);
		if(TONES_ENABLED) {
			mToneGenerator = new ToneGeneratorUtils(getContext(), AudioManager.STREAM_VOICE_CALL, ToneGeneratorUtils.KEY_TONE_RELATIVE_VOLUME_50);
		}
	}
	
	/**
	 * Called to hide dialpad from the screen
	 */
	public void hideDialpad(){
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "hideDialpad");
		}

		this.mSipClient = null;
		setVisibility(GONE);		
		if(TONES_ENABLED) {
			if(mToneGenerator != null) {
				mToneGenerator.destroy();
				mToneGenerator = null;
			}
		}		
	}
	
	/**
	 * Called to clear up dialpad area
	 */
	public void clearDialpad(){		
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "clearDialpad, current : " + mDigits);
		}
		if(DIGITS_ENABLED ){				
			mDigits = null;
		}
	}
	
	/**
	 * Inner class describing and holding configuration how to handle key presses.
	 */
	private static class KeypressHandler {
		
		static final KeypressHandler[] HANDLERS = 
		{			
			new KeypressHandler(R.id.one,   ToneGenerator.TONE_DTMF_1, KeyEvent.KEYCODE_1,     "1"),
			new KeypressHandler(R.id.two,   ToneGenerator.TONE_DTMF_2, KeyEvent.KEYCODE_2, 	   "2"),
			new KeypressHandler(R.id.three, ToneGenerator.TONE_DTMF_3, KeyEvent.KEYCODE_3, 	   "3"),
			new KeypressHandler(R.id.four,  ToneGenerator.TONE_DTMF_4, KeyEvent.KEYCODE_4, 	   "4"),
			new KeypressHandler(R.id.five,  ToneGenerator.TONE_DTMF_5, KeyEvent.KEYCODE_5,     "5"),
			new KeypressHandler(R.id.six,  	ToneGenerator.TONE_DTMF_6, KeyEvent.KEYCODE_6,     "6"),
			new KeypressHandler(R.id.seven, ToneGenerator.TONE_DTMF_7, KeyEvent.KEYCODE_7,     "7"),
			new KeypressHandler(R.id.eight, ToneGenerator.TONE_DTMF_8, KeyEvent.KEYCODE_8,     "8"),
			new KeypressHandler(R.id.nine, 	ToneGenerator.TONE_DTMF_9, KeyEvent.KEYCODE_9,     "9"),
			new KeypressHandler(R.id.star, 	ToneGenerator.TONE_DTMF_S, KeyEvent.KEYCODE_STAR,  "*"),
			new KeypressHandler(R.id.zero, 	ToneGenerator.TONE_DTMF_0, KeyEvent.KEYCODE_0,     "0"),
			new KeypressHandler(R.id.pound, ToneGenerator.TONE_DTMF_P, KeyEvent.KEYCODE_POUND, "#")
		};	

		//protected to avoid generation for unnecessary synthetic methods
		int viewId;
		int toneId;
		int keycodeId;
		String dtmfTone;
		
		KeypressHandler(int viewId, int toneId, int keycodeId, String dtmfTone) {
			this.viewId = viewId;
			this.toneId = toneId;
			this.keycodeId = keycodeId;
			this.dtmfTone = dtmfTone;
		}
		
		static KeypressHandler getHandler(int viewId){
			for(KeypressHandler handler : HANDLERS){
				if(handler.viewId == viewId){
					return handler;
				}			
			}
			return null;
		}				
	}

}
