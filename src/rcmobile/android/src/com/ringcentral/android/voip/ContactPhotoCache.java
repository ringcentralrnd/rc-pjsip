/** 
 * Copyright (C) 2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.voip;

import java.lang.ref.SoftReference;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.contacts.Cont;

/**
 * Cache holder for images in active call
 * 
 * 
 */

public class ContactPhotoCache {
	private static final String TAG = "[RC]ContactPhotoCache";	
	
	private static final int MSG_SET_IMAGE = 1;
	
	private static Map<Long, SoftReference<Bitmap>> mBitmapCache = new ConcurrentHashMap<Long, SoftReference<Bitmap>>();;
	private static Object mSync = new Object();
	
	private Handler mHandler;
	private ExecutorService mExecutor;
	
	
	public ContactPhotoCache() {
		if (LogSettings.ENGINEERING) {
			EngLog.v(TAG, "creating...");
		}
		mHandler = new Handler() {
			public void handleMessage(Message msg) {
				if(msg.what == MSG_SET_IMAGE) {
					ImageRequest imageRequest = (ImageRequest)msg.obj;
					if((imageRequest != null) &&
					   (imageRequest.targetView != null)){
						
						try {
							imageRequest.targetView.setImageBitmap(imageRequest.bitmap);			
							imageRequest.targetView = null;
							imageRequest.bitmap = null;
							imageRequest.imageId = null;
						} catch (Throwable t) {
							if(LogSettings.MARKET) {
								MktLog.e(TAG, "assigning image to target view", t);
							}
						}						
						
					}
				}
			};
		};
		mExecutor = Executors.newSingleThreadExecutor();				
	}

	public void destroy() {
		if (LogSettings.ENGINEERING) {
			EngLog.v(TAG, "destroying...");
		}		

		if (mExecutor != null) {
			mExecutor.shutdownNow();
			mExecutor = null;
		}
		
		if (mHandler != null) {
			mHandler.removeMessages(MSG_SET_IMAGE);
			mHandler = null;		
		}	
		
		clearCache();
		
		System.gc();
	}			
	
	private void clearCache() {
		synchronized (mSync) {
			if (LogSettings.ENGINEERING) {
				EngLog.v(TAG, "clearCache()");
			}
			for(Long key : mBitmapCache.keySet()){
				SoftReference<Bitmap> bitmapRef = mBitmapCache.get(key);
				if ((bitmapRef == null)||(bitmapRef.get() == null)) {
					mBitmapCache.remove(key);
					continue;
				}				
			}			
		}
	}

	private Bitmap getFromCache(long photoId) {					
		synchronized (mSync) {
			if (LogSettings.ENGINEERING) {
				EngLog.v(TAG, "getFromCache(), photoId : " + photoId );
			}
			
			/*if (photoId < 0) {
				if (LogSettings.ENGINEERING) {
					EngLog.v(TAG, "invalid photo id : " + photoId);
				}
				return null;
			}		*/
			
			SoftReference<Bitmap> bitmapRef = mBitmapCache.get(photoId);
			
			if (bitmapRef != null) {
				
				Bitmap bitmap = bitmapRef.get();
				if (bitmap != null) {
					
					if (LogSettings.ENGINEERING) {
						EngLog.v(TAG, "getPhoto found in cache, returning");
					}
					return bitmap;
				}			
			} 
			return null;			
		}				
	}
	
	private void addToCache(long photoId, Bitmap bitmap) {
		synchronized (mSync) {
			if (LogSettings.ENGINEERING) {
				EngLog.v(TAG, "addToCache(), photoId : " + photoId );
			}
			
			if ((photoId >= 0) && (bitmap != null)) {
				SoftReference<Bitmap> bitmapRef = mBitmapCache.get(photoId);
				 
				if (bitmapRef != null) {
					if (LogSettings.ENGINEERING) {
						EngLog.v(TAG, "addToCache() found old image with the same id, removing");
					}
					bitmapRef.clear();
					System.gc();					
				}
				bitmapRef = new SoftReference<Bitmap>(bitmap);
				mBitmapCache.put(photoId, bitmapRef);								
			}						
		}		
	}
	
	public void getPhoto(Long photoId, ImageView view) {
		if (LogSettings.ENGINEERING) {
			EngLog.v(TAG, "getPhoto(), photoId : " + photoId );
		}
		
		if ((view != null)) {
			ImageLoader loader = new ImageLoader(new ImageRequest(photoId, view));
			mExecutor.execute(loader);
		}		
	}
	

	private class ImageLoader implements Runnable {
		private ImageRequest requestItem;
		
		public ImageLoader(ImageRequest photoItem) {
			this.requestItem = photoItem;	
		}

		@Override
		public void run() {		
									
			if (Thread.interrupted()) {
				if (LogSettings.ENGINEERING) {
					EngLog.v(TAG, "Thread interrupted");
				}
                return;
            }	
			Bitmap bitmap = null;
			if(requestItem.imageId >= 0) {
			
				bitmap = getFromCache(requestItem.imageId);
				
				if (bitmap == null) {
					if (LogSettings.ENGINEERING) {
						EngLog.v(TAG, "ImageLoader fetching photo from DB : " + requestItem.imageId);
					}
					
					bitmap = Cont.acts().getContactPhoto(RingCentralApp.getContextRC(), requestItem.imageId, null);
					
					if(bitmap != null) {
						addToCache(requestItem.imageId, bitmap);
					}				
				}
				
				if (Thread.interrupted()) {
					if (LogSettings.ENGINEERING) {
						EngLog.v(TAG, "Thread interrupted");
					}
	                return;
	            }
			
			}
			
			//sending message
			requestItem.bitmap = bitmap;								
			mHandler.sendMessage(mHandler.obtainMessage(MSG_SET_IMAGE, requestItem));
		}//end run
	}
	
	
	private class ImageRequest {
		
		public ImageRequest(Long photoId, ImageView targetView) {
			this.imageId = photoId;
			this.targetView = targetView;
		}
		
		public Long imageId;
		public ImageView targetView;
		public Bitmap bitmap;
	}
}
