package com.ringcentral.android.voip;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.utils.ReflectionHelper;
import com.ringcentral.android.utils.ui.widget.ScreenLockerLayout;

public class ProximityUtils {
	private static final String TAG = "[RC]ProximityUtils";
	
	private static final String PROXIMITY_LOCK = "RingCentral Proximity Lock";

	public static final int PROXIMITY_DIM_SENSOR_SCREEN_OFF = 1;
    public static final int PROXIMITY_DIM_SENSOR_CUSTOM = 2;
    public static final int PROXIMITY_DIM_NO_SENSOR = 4;
    public static final int PROXIMITY_DIM_ALL = PROXIMITY_DIM_SENSOR_SCREEN_OFF | 
    											PROXIMITY_DIM_SENSOR_CUSTOM | 
    											PROXIMITY_DIM_NO_SENSOR;
    
    private PowerManager powerManager;
	private SensorManager sensorManager;
	private Sensor proximitySensor;

	private WakeLock proximityWakeLock;    	
	private ProximityListener m_proximityListener;
	
	private ScreenLockerLayout lockerLayout;
	
	
	private int mProximityMode = PROXIMITY_DIM_NO_SENSOR;
	
	public ProximityUtils(Context context){
		initProximity(context);    		
		if(mProximityMode==PROXIMITY_DIM_SENSOR_CUSTOM){
			m_proximityListener = new ProximityListener();
			sensorManager.registerListener(m_proximityListener, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
		}
		ScreenLockerLayout.setRelock(mProximityMode == PROXIMITY_DIM_NO_SENSOR);    		
	}		
    

	private void initProximity(Context context){
		if(LogSettings.MARKET){
			MktLog.d(TAG, "Initializing proximity");
		} 
		try {					
			
			powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
			sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
			proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
			
	        if(proximitySensor != null && powerManager != null) {
	        	mProximityMode = PROXIMITY_DIM_SENSOR_CUSTOM;
	     		try {	     			
	     			Method method = ReflectionHelper.getDeclaredMethod(powerManager.getClass(), "getSupportedWakeLockFlags");
					int supportedFlags = (Integer) method.invoke(powerManager);
					if(LogSettings.ENGINEERING){
						EngLog.d(TAG, "List supported flgs :" + supportedFlags);
					}
					Field f = PowerManager.class.getDeclaredField("PROXIMITY_SCREEN_OFF_WAKE_LOCK");
					int proximityScreenOffWakeLock = (Integer) f.get(null);
					if( (supportedFlags & proximityScreenOffWakeLock) != 0x0 ) {					
						proximityWakeLock = powerManager.newWakeLock(proximityScreenOffWakeLock, PROXIMITY_LOCK);
						proximityWakeLock.setReferenceCounted(false);
						mProximityMode = PROXIMITY_DIM_SENSOR_SCREEN_OFF;
						//M.D. some WIFI-degradation issues can occure on earlier platforms, can switch to always custom sensor 
						//if needed
						mProximityMode = PROXIMITY_DIM_SENSOR_CUSTOM;    						
					}    					
				} catch (Exception e) {
					if(LogSettings.MARKET){
						MktLog.i(TAG, "Impossible to get power manager supported wake lock flags: " + e.toString());
					}
				}
	        }
	        
	        if (LogSettings.ENGINEERING) {
	        	EngLog.d(TAG, "Proximity options : " + mProximityMode);
	        }
		} catch (java.lang.Throwable t){
			if (LogSettings.MARKET) {
	        	MktLog.w(TAG, "Error when evaluating proximity options: " + t.toString());
	        }
		}    	   	
	}
	
	/*
	 * 
	 */
	public void destroy(){    		
		unlockScreen(PROXIMITY_DIM_ALL);
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "destroy() called.");
		}
        try {
            if (mProximityMode == PROXIMITY_DIM_SENSOR_CUSTOM && sensorManager != null) {
                sensorManager.unregisterListener(m_proximityListener);
                m_proximityListener = null;
            }
        } catch (java.lang.Throwable t) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "proximity.destroy(1):", t);
            }
        }
		powerManager = null;
		sensorManager = null;
		proximitySensor = null;
        try {
            if ((proximityWakeLock != null) && (proximityWakeLock.isHeld())) {
                proximityWakeLock.release();
                proximityWakeLock = null;
            }
        } catch (java.lang.Throwable t) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "proximity.destroy(2):", t);
            }
        }
	}    	
	
	public void setLockerLayout(ScreenLockerLayout lockerLayout) {
		this.lockerLayout = lockerLayout;
	}

	
	/*
	 * 
	 */
	public void orientationChanged(){
		//M.D. for what purpose do we support landscape configuration??
		if(LogSettings.ENGINEERING){
			EngLog.d(TAG, "orientationChanged()");    			 
		}
		if((mProximityMode != PROXIMITY_DIM_SENSOR_SCREEN_OFF)&&(lockerLayout != null)&&(lockerLayout.isLocked())){
			lockScreen(PROXIMITY_DIM_ALL);
		}else if(mProximityMode == PROXIMITY_DIM_SENSOR_SCREEN_OFF){
			lockScreen(PROXIMITY_DIM_SENSOR_SCREEN_OFF);
		}else{
			unlockScreen(PROXIMITY_DIM_ALL);
		}
	}
	
	/*
	 * 
	 */
    public void lockScreen(int prMode) {
    	
    	if((prMode & mProximityMode) != mProximityMode){
    		return;
    	}        	            
        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "lockScreen(), proximityMode : " + mProximityMode);
        }
        try {
            if (mProximityMode == PROXIMITY_DIM_SENSOR_SCREEN_OFF) {
                if ((proximityWakeLock != null) && (!proximityWakeLock.isHeld())) {
                    if (LogSettings.ENGINEERING) {
                        EngLog.d(TAG, "acquiring lock.");
                    }
                    proximityWakeLock.acquire();                        
                }
            } else if (mProximityMode == PROXIMITY_DIM_SENSOR_CUSTOM) {                	                
            	if(lockerLayout != null){
            		lockerLayout.show();
            	}
            	
            } else if (mProximityMode == PROXIMITY_DIM_NO_SENSOR) {
                if (lockerLayout != null) {
                    if (lockerLayout.isLocked()) {
                    	lockerLayout.show();
                    } else {
                    	lockerLayout.show(ScreenLockerLayout.LOCK_DELAY_LONG);
                    }
                }
            }
        } catch (java.lang.Throwable t) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "lockScreen:", t);
            }
        }
    }
	    
	/*
	 * 
	 */
    public void unlockScreen(int prMode) {
    	if((prMode & mProximityMode) != mProximityMode){
    		return;
    	}
    	
        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "unlockScreen(), proximityMode : " + mProximityMode);
        }

        try {
            if (mProximityMode == PROXIMITY_DIM_SENSOR_SCREEN_OFF) {
                if ((proximityWakeLock != null) && (proximityWakeLock.isHeld())) {
                    if (LogSettings.ENGINEERING) {
                        EngLog.d(TAG, "releasing lock.");
                    }
                    proximityWakeLock.release();
                }
            } else if (mProximityMode == PROXIMITY_DIM_SENSOR_CUSTOM) {
                if (lockerLayout != null) {
                	lockerLayout.hide();
                }
            }
        } catch (java.lang.Throwable t) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "unlockScreen:error:", t);
            }
        }
    }
       
    
    private class ProximityListener implements SensorEventListener{
    	
    	private static final String TAG = "[RC]ProximityListener";
    	private static final float PROXIMITY_THRESHOLD = 5.0f;

		@Override
		public void onSensorChanged(SensorEvent event) {
			float distance = event.values[0];
			boolean isActive = (distance >= 0.0 && distance < PROXIMITY_THRESHOLD && distance < event.sensor.getMaximumRange());
			if( LogSettings.ENGINEERING ) {
				EngLog.v(TAG, "onSensorChanged() distance : " + distance + " isActive" + isActive);				
			}
			if(isActive){
				lockScreen(ProximityUtils.PROXIMITY_DIM_SENSOR_CUSTOM);
			}else{
				unlockScreen(ProximityUtils.PROXIMITY_DIM_SENSOR_CUSTOM);
			}			
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {			
		}
    	
    }
}