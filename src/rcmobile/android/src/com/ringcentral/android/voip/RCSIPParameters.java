/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.voip;

import android.content.Context;

import com.rcbase.android.sip.service.ISIPParameters;
import com.ringcentral.android.provider.RCMProviderHelper;

/**
 * @author Denis Kudja
 *
 */
public class RCSIPParameters implements ISIPParameters {

	private Context m_context = null;
	
	public RCSIPParameters( Context context ){
		m_context = context;
	}
	
	/* (non-Javadoc)
	 * @see com.rcbase.android.sip.service.ISIPParameters#isOutboundCallsOnly()
	 */
	@Override
	public boolean isOutboundCallsOnly() {
		
		return !RCMProviderHelper.isVoipSwitchedOn_UserIncomingCalls(m_context);
	}

	@Override
	public boolean use_TCP_For_SIP_registration() {
		return false;
	}

	@Override
	public String getServiceProvider() {
		return RCMProviderHelper.getUriSipProvider(m_context);
	}
	@Override
	public void setServiceProvider(String sipProvider) {
		RCMProviderHelper.setUriSipProvider( m_context, sipProvider );
	}
	
	@Override
	public String getSipOutboundProxy() {
		return RCMProviderHelper.getUriSipOutboundProxy(m_context);
	}
	
	@Override
	public void setSipOutboundProxy(String sipProxy) {
		RCMProviderHelper.setUriSipOutboundProxy(m_context, sipProxy);
	}

	@Override
	public boolean isVoipEnabled() {
		return RCMProviderHelper.isVoipEnabled_Env_Acc_SipFlagHttpReg_ToS_UserVoIP(m_context);
	}

	@Override
	public boolean isVoipOver3GEnabled() {
		return RCMProviderHelper.isVoipSwitchedOn_User3g4g(m_context);
	}

	@Override
	public boolean isVoipOverWiFiEnabled() {
		return RCMProviderHelper.isVoipSwitchedOn_UserWiFi(m_context);
	}

	@Override
	public long getVoipSettings() {
		return RCMProviderHelper.getVoipSettings(m_context);
	}

	@Override
	public boolean isVoipOnServiceEnabled() {
		return RCMProviderHelper.isVoipEnabled_SipFlagHttpReg(m_context);
	}

	@Override
	public void setVoipOnServiceEnabled(boolean enabled) {
		RCMProviderHelper.setVoipEnabled_SipFlagHttpReg( m_context, enabled );
		
	}

	@Override
	public void setHttpRegSipFlags(long flags) {
		RCMProviderHelper.setHttpRegSipFlags(m_context, flags );
		
	}

	@Override
	public long getHttpRegSipFlags() {
		return RCMProviderHelper.getHttpRegSipFlags(m_context);
	}
	
}
