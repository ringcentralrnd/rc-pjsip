/**
 * Copyright (C) 2011-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.voip;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.PowerManager.WakeLock;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.logging.QaLog;
import com.rcbase.android.sip.client.CompletionInfo;
import com.rcbase.android.sip.client.RCSIP;
import com.rcbase.android.sip.client.SipClient;
import com.rcbase.android.sip.service.CallInfo;
import com.rcbase.android.sip.service.PJSIP;
import com.rcbase.android.sip.service.RCCallInfo;
import com.rcbase.android.utils.media.AudioState;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.utils.DeviceUtils;
import com.ringcentral.android.utils.PhoneUtils;
import com.ringcentral.android.utils.ui.widget.ContactPhotoView;
import com.ringcentral.android.utils.ui.widget.ScreenLockerLayout;

public class VoipCallStatusActivity extends Activity {

private static final String TAG = "[RC]VoipCallStatusActivity";
    
    private static final String RC_CALL_WAKE_LOCK = "RingCentral VoIP Call";    
    static final int LINES_LIST_ENABLED = 1;
    
    private static final int MSG_CLOSE_STATUS_SCREEN = 2;    
    
    // AB-29:
    // Call Ended: call was completed either by me or by the other party. Screen is shown for 3 sec,
    // Network Error (<error number>): for all types of errors like network, server, etc. Screen is shown for 5 sec,
    private static final long SCREEN_CLOSE_DELAY_OK_USER_ENDED = 1500; //milliseconds
    private static final long SCREEN_CLOSE_DELAY_OK = 2700; //milliseconds
    private static final long SCREEN_CLOSE_DELAY_ERROR = 5000; //milliseconds
    
    private static final boolean BACK_KEY_DISABLED = false;            

    private final static int    sf_base_event               = 1100;
    private final static int    sf_call_hangup              = (sf_base_event + 1);
    private final static int    sf_update_duration          = (sf_base_event + 2);
    private final static int    sf_lock_screen              = (sf_base_event + 5);
    private final static int    sf_unlock_screen            = (sf_base_event + 6);
    private final static int    sf_update_photos            = (sf_base_event + 7);
    private final static int    sf_consistency_check        = (sf_base_event + 8);
    private final static long   CONSISTENCY_CHECK_TIMEOUT   = 15000;
    
    private SipClient m_sipClient = null;
    private CallsListAdapter m_callsListAdapter = null;       
            
    private ListView m_callsList;           
    private View mInfoView;
    private View mCallerInfoView;
    private TextView mNameView;
    private TextView mNumberView;
    private TextView mCallDurationView;
    private TextView mCallStatusView;   
    private ContactPhotoView mPhotoView;
    private ImageButton mBtnHold;
    private ImageButton mBtnDialpad;
    private ImageButton mBtnSpeaker;
    private ImageButton mBtnMute;
    private ImageButton mBtnBluetooth;
    private ImageButton mBtnEndCall;    
    private View mButtonLine1;
    private ScreenLockerLayout screenLocker;
    private CallDialpad mCallDialpad;
    
    private int mDisplayOrientation;
    private ContactPhotoCache mPhotoCache;
    
    private TimeoutHandler mTimeoutHandler; 
    private UIMessageHandler m_handler = new UIMessageHandler();
    
    private ProximityUtils proximity;
    
    private boolean m_mute 						= false;
    private boolean mDialpadShown 				= false;
    private AudioState mAudioState 				= null;

    private boolean mInitComplete = false;
    private boolean mOutgoingCallInitialStatusMessageSet = false; 
    
    private synchronized SipClient getSipClient() {
        if( m_sipClient == null ){
            m_sipClient = new SipClient(m_handler, getApplicationContext() );
        }
        return m_sipClient;
    }
    
    private synchronized CallsListAdapter getCallsListAdapter(){
        if( m_callsListAdapter == null )
            m_callsListAdapter = new CallsListAdapter(null);
        
        return m_callsListAdapter;
    }
    
    /**
     * Defines if the activity in finishing state
     */
    private boolean mIsActivityFinishing = false;
    
    /**
     * Defines if the activity in finishing state
     * 
     * @return <code>true</code> if it is in finishing state, otherwise <code>false</code>
     */
    private boolean isActivityFinishing() {
        if (isFinishing() || mIsActivityFinishing) {
            return true;
        }
        return false;
    }
    
    private void onCompletion(final CompletionInfo compInfo) {
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onCompletion :" + compInfo + " isFinishing:" + isFinishing());
        }
        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
            PerformanceMeasurementLogger.getInstance().printResults();
        }
        
        getSipClient().ringOff();
        
        getSipClient().unregisterCallback();
        getSipClient().unBindSipServiceRoutine( getApplicationContext());                               
        
        updateControls(compInfo, true);
        
        if (isActivityFinishing()) {
            return;
        }                   
        
        if( compInfo != null ){
            if(LogSettings.ENGINEERING){
                EngLog.i(TAG, "CompletionInfo : " + compInfo);
            }
            
            if (compInfo.isSuccess()) {
                if (compInfo.getEndingReason() == RCCallInfo.CALL_ENDED_BY_USER) {
                    finish(RCMConstants.RINGOUT_RESULT_OK, SCREEN_CLOSE_DELAY_OK_USER_ENDED);
                } else {
                    finish(RCMConstants.RINGOUT_RESULT_OK, SCREEN_CLOSE_DELAY_OK);
                }
            } else {                                                                                            
                finish(RCMConstants.RINGOUT_RESULT_FAIL, SCREEN_CLOSE_DELAY_ERROR);
            }
        }
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onCompletion(end)");
        }
    }
    

    @Override
    protected void onNewIntent(Intent newIntent){
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onNewIntent(): ****** Started!");
        }        
        setIntent( newIntent );
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onCreate(): starting...");
        }
        
        
        if (savedInstanceState != null) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "onCreate(): Activity re-creation forbidden; FINISH");
            }
            finish();
            return;
        }
        
//        final String toNumber = getIntent().getStringExtra(RCMConstants.EXTRA_RINGOUT_TO);
//        final String fromNumber = getIntent().getStringExtra(RCMConstants.EXTRA_RINGOUT_FROM);
//        final String toName = getIntent().getStringExtra(RCMConstants.EXTRA_RINGOUT_TO_NAME);
//        
//        final CallInfo callInfoIncoming = getIntent().getParcelableExtra( RCMConstants.EXTRA_VOIP_INCOMING_CALL_INFO );
//
//        if( callInfoIncoming != null ){
//            if (LogSettings.MARKET) {
//                MktLog.i(TAG, "onCreate(): callInfoIncoming is not null");
//            }
//            
//            getSipClient().addCallInfo( callInfoIncoming, true );
//            if( TextUtils.isEmpty(callInfoIncoming.getRemoteContact())){
//                final int line = getSipClient().getCurrentLine();
//               final String number = CallInfo.getNumberFromURI(callInfoIncoming.getRemoteURI());
//                if( !CallInfo.isUnknown(number) ){
//                    if (LogSettings.MARKET) {
//                        MktLog.i(TAG, "onCreate(): Incoming call. Find contact for number: " + number );
//                    }
//                    new DetectContactNameByNumber(line, number).execute(getApplicationContext());
//                }
//            }
//        }
//        else{           
//            if (LogSettings.MARKET) {
//                MktLog.i(TAG, "onCreate(): callInfoIncoming is null. TO = " + toNumber + ", FROM = " + fromNumber);
//            }            
//            if (!TextUtils.isEmpty(toNumber) && !TextUtils.isEmpty(fromNumber)) {
//                final CallInfo callInfoOutbound = new CallInfo( toNumber, toName );
//                final int index = getSipClient().addCallInfo( callInfoOutbound, true );
//                
//                /*
//                 * Save this number as last successfully dialed
//                 */
//                RCMProviderHelper.saveLastCallNumber(this, PhoneUtils.getLocalCanonical(toNumber));
//
//                if( index != SipClientLines.sf_INVALID_LINE_INDEX ){
//                    new DetectContactNameByNumber(index, toNumber ).execute(getApplicationContext());
//                }
//            }
//
//        }
//        
        String to_number = null;
        String to_name = null;

        int call_type = getIntent().getIntExtra(RCMConstants.EXTRA_VOIP_CALL_TYPE, RCMConstants.VOIP_CALL_TYPE_NONE);
        if (call_type == RCMConstants.VOIP_CALL_TYPE_OUTGOING) {
            to_number = getIntent().getStringExtra(RCMConstants.EXTRA_CALL_TO_NUMBER);
            to_name = getIntent().getStringExtra(RCMConstants.EXTRA_CALL_TO_NAME);
        } else if (call_type == RCMConstants.VOIP_CALL_TYPE_INCOMING) {
            CallInfo call_info = getIntent().getParcelableExtra( RCMConstants.EXTRA_VOIP_INCOMING_CALL_INFO );
            to_number = call_info.getFromNumber();
            to_name = call_info.getFromName();
        }
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onCreate(): to_number: " + to_number +" ,to_name: " + to_name);
        }
        
        mPhotoCache = new ContactPhotoCache();
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        buildLayout(DeviceUtils.getDisplayOrientation(this), to_number, to_name);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setDefaultKeyMode(DEFAULT_KEYS_DISABLE);        
        
        mTimeoutHandler = new TimeoutHandler();

        updateControls(null, false);

        boolean binded = getSipClient().bind(getApplicationContext());
        
        if( binded ) {
            acquireResources(); 
        }
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onCreate(): finished");
        }

    }

    private WakeLock mWakeLock;

    private void acquireResources() {
        //General wake lock
        mWakeLock = ((PowerManager) getSystemService(Context.POWER_SERVICE)).newWakeLock(PowerManager.ON_AFTER_RELEASE | PowerManager.SCREEN_BRIGHT_WAKE_LOCK
                | PowerManager.ACQUIRE_CAUSES_WAKEUP, RC_CALL_WAKE_LOCK);
        if(mWakeLock != null && !mWakeLock.isHeld()) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "Acquire Wake Lock");
            }
            mWakeLock.acquire();
        }
        
        //Proximity
        proximity = new ProximityUtils(this);        
        proximity.setLockerLayout(screenLocker);
        lockScreen(ProximityUtils.PROXIMITY_DIM_NO_SENSOR);
    }
    
    private void releaseResources() {
        //General wake lock
        if (mWakeLock != null && mWakeLock.isHeld()) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "Release Wake Lock");
            }
            mWakeLock.release();
            mWakeLock = null;
        }
        
        // Proximity
        if (proximity != null) {
            proximity.destroy();
            proximity = null;
        }       
        
    }
    
    /**
     * Switch call info view from single to multiple lines and via versa
     */
    private void switchCallInfoView(){      
        final boolean multiList = (getSipClient().getActiveLines() > LINES_LIST_ENABLED);                
        m_callsList.setVisibility(mDialpadShown ? View.GONE : (multiList ? View.VISIBLE : View.GONE));        
        mInfoView.setVisibility(mDialpadShown ? View.GONE : View.VISIBLE);
        mCallerInfoView.setVisibility(multiList ? View.GONE: View.VISIBLE);
        if(mDialpadShown){
            mCallDialpad.showDialpad(getSipClient());   
            if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
                PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.DIALPAD_OPENING);
            }
        }else{
            mCallDialpad.hideDialpad();    
            if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
                PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.DIALPAD_OPENING);
            }
        }        
        mButtonLine1.setVisibility((mDialpadShown && (mDisplayOrientation == Configuration.ORIENTATION_PORTRAIT)) ? View.GONE : View.VISIBLE);
    }
    
    /*
     * 
     */
    private void buildLayout(int orientation, String number, String name) {
        mDisplayOrientation = orientation;

        int content_view_id = (orientation == Configuration.ORIENTATION_LANDSCAPE ?
                R.layout.voip_call_status_screen_landscape              //LANDSCAPE
                : R.layout.voip_call_status_screen_portrait);            //PORTRAIT or SQUARE
        
        setContentView(content_view_id);
        mInfoView = findViewById(R.id.voip_call_status_screen_single_call_info_view);
        mCallerInfoView = findViewById(R.id.voip_call_info_caller_view);
        
        //removing previous observers;
        if(m_callsList != null){
            m_callsList.setAdapter(null);
        }
        
        m_callsList = (ListView)findViewById(R.id.voip_call_status_screen_call_list_view);
        m_callsList.setAdapter(getCallsListAdapter());
        m_callsList.setOnItemClickListener(new OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {         
                if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
                    PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.CALLS_SWITCHING);
                }
            	getSipClient().selectLine(((CallInfo)getCallsListAdapter().getItem(arg2)).getRcCallId());
            	lockScreen(ProximityUtils.PROXIMITY_DIM_NO_SENSOR);
            }
        });                
        
        mNameView = (TextView) findViewById(R.id.voip_call_info_caller_name);
        mNumberView = (TextView) findViewById(R.id.voip_call_info_caller_number);        

        if (!TextUtils.isEmpty(name)) {
            mNameView.setText(name);
            mNameView.setVisibility(View.VISIBLE);
            mNumberView.setVisibility(View.GONE);
        } else if (!TextUtils.isEmpty(number)) {
            mNumberView.setText(PhoneUtils.getLocalCanonical(number));
            mNumberView.setVisibility(View.VISIBLE);
            mNameView.setVisibility(View.GONE);
        }
        
        mNameView.setSingleLine(true);
        mNameView.setEllipsize(TextUtils.TruncateAt.END);

        mCallDurationView = (TextView) findViewById(R.id.voip_call_info_call_duration);
        mCallStatusView = (TextView) findViewById(R.id.voip_call_info_call_status);
        mPhotoView = (ContactPhotoView) findViewById(R.id.voip_call_info_caller_photo);        
        
        mButtonLine1 = findViewById(R.id.voip_call_status_screen_buttons_1);
        
        mBtnHold = (ImageButton) findViewById(R.id.btn_call_hold);
        mBtnHold.setEnabled(false);
        mBtnHold.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
                    PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.HOLD);
                }
                getSipClient().hold();                
                if( getSipClient().getActiveLines() > LINES_LIST_ENABLED ){
                        getCallsListAdapter().invalidate();
                 }
                lockScreen(ProximityUtils.PROXIMITY_DIM_NO_SENSOR);
            }
        });
        
        mBtnDialpad = (ImageButton) findViewById(R.id.btn_call_dialpad);     
        mBtnDialpad.setEnabled(false);
        mBtnDialpad.setSelected(mDialpadShown);
        mBtnDialpad.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {      
                toggleDialpadBtn(!mBtnDialpad.isSelected());      
                lockScreen(ProximityUtils.PROXIMITY_DIM_NO_SENSOR);
            }
        });
        
        mCallDialpad = (CallDialpad) findViewById(R.id.voip_call_status_screen_dialpad_container);
        
        switchCallInfoView();                  
        
        mBtnSpeaker = (ImageButton) findViewById(R.id.btn_call_speaker);
        mBtnSpeaker.setEnabled(false);
        mBtnSpeaker.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    public void run() {
                        try{
                            getSipClient().changeSpeakerRoute();
                        } catch (Exception e) {
                            if(LogSettings.MARKET) {
                                MktLog.e(TAG, " btnSpeaker listener: ", e);
                            }
                        }   
                    }
                }, "VCSactv:mBtnSpeaker:onClick").start();
                lockScreen(ProximityUtils.PROXIMITY_DIM_NO_SENSOR);
                
            }
        });
        
        
        mBtnMute = (ImageButton) findViewById(R.id.btn_call_mute);
        mBtnMute.setEnabled(false);
        mBtnMute.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {                
                if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
                    PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.MUTE);
                }
                if ( getSipClient().conf_adjust_rx_level( (isMute() ? SipClient.getUnMuteLevel() : SipClient.getMuteLevel()) ) ){
                    setMute(!isMute());
                    sendMessage( SipClient.sf_update_controls, 0, 0 );
                }
                lockScreen(ProximityUtils.PROXIMITY_DIM_NO_SENSOR);
            }

        });
        
        mBtnBluetooth = (ImageButton) findViewById(R.id.btn_call_bluetooth);
        mBtnBluetooth.setEnabled(false);
        mBtnBluetooth.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            getSipClient().changeBluetoothRoute();
                        } catch (Exception e) {
                            if (LogSettings.MARKET) {
                                MktLog.e(TAG, " btnBluetooth listener:", e);
                            }
                        }
                    }
                }, "VCSactv:mBtnBluetooth:onClick").start();
                lockScreen(ProximityUtils.PROXIMITY_DIM_NO_SENSOR);
            }
        });
        
        mBtnEndCall = (ImageButton) findViewById(R.id.btn_call_endcall);
        mBtnEndCall.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                final CallInfo callInfo = getSipClient().getCurrentLineCallInfo();

                mBtnSpeaker.setEnabled(false);
                
                final int activeCount = getSipClient().getActiveLines();
                
                if(LogSettings.MARKET){
                    MktLog.i(TAG, "EndCall pressed (Active calls:" + activeCount + ")");
                }
                
                /*
                 * If there is not active call  and we do not have any other active calls 
                 * then close activity 
                 */
                if( callInfo == null || 
                        callInfo.getRcCallId() == PJSIP.sf_INVALID_RC_CALL_ID ||
                            callInfo.getCall_state() == RCSIP.PJSUA_CALL_STATE_INIT || 
                                callInfo.getCall_state() == RCSIP.PJSUA_CALL_STATE_DISCONNECTED )
                {
                    if( activeCount > 1 ){                      
                        if(LogSettings.MARKET){
                            MktLog.d(TAG, "EndCall pressed , Delete current line for call w/o Call ID. Active calls:" + activeCount);
                        }               
                        getSipClient().removeCurrentLine();
                    }
                    else{
                        
                        if(LogSettings.MARKET){
                            MktLog.d(TAG, "EndCall pressed , Only one active call w/o Call ID. Close activity.");
                        }
                        getSipClient().setFinishingStatus();
                        getSipClient().sendMessage( new CompletionInfo(true, true, RCCallInfo.CALL_ENDED_BY_USER, CompletionInfo.ERROR_UNKNOWN, 0, null, false));
                    }
                }
                else{
                    
                    if(LogSettings.MARKET){
                        MktLog.d(TAG, "EndCall pressed , Hang up active call with Call ID: " + callInfo.getRcCallId() );
                    }
                    
                    sendMessage( sf_call_hangup, 0, getSipClient().getCurrentLineCallId());
                    getSipClient().setFinishingStatus();
                    
                    if( activeCount == 1 ){                      
                        if(LogSettings.MARKET){
                            MktLog.d(TAG, "EndCall pressed , Finish activity. No more active calls.");
                        }
                        
                        getSipClient().sendMessage( new CompletionInfo(true, true, RCCallInfo.CALL_ENDED_BY_USER, CompletionInfo.ERROR_UNKNOWN, 0, null, false));
                    }
                }
                lockScreen(ProximityUtils.PROXIMITY_DIM_NO_SENSOR);
                
            }
        });
        mBtnEndCall.setEnabled(false);
        
        screenLocker = (ScreenLockerLayout) findViewById(R.id.lockerOverlay);
        
        if(proximity != null){
            proximity.setLockerLayout(screenLocker);
        }
    }
    
    private void toggleDialpadBtn(boolean isSelected){      
        if(LogSettings.ENGINEERING){
            EngLog.d(TAG, "toggleDialpadBtn(), mDialpadShown : " + mDialpadShown + " new State : " + isSelected);
        }
        if(mDialpadShown != isSelected){
            if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
                PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.DIALPAD_OPENING);
            }
            mDialpadShown = isSelected;
            mBtnDialpad.setSelected(mDialpadShown);
            switchCallInfoView();
        }
    }        
    
    public boolean isMute() {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "isMute() :" + m_mute );
        }
        return m_mute;
    }
    public void setMute(boolean mute) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "setMute() From: " + m_mute + " To:" + mute );
        }
        m_mute = mute;
    }    

    @Override
    protected void onRestart() {
        super.onRestart();

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onRestart(): starting...");
        }
        
        //onConfigurationChanged() is not always called when the Activity becomes visible (Android bug?)
        //That's why display orientation is checked here
        int orientation = DeviceUtils.getDisplayOrientation(this);
        if (orientation != mDisplayOrientation) {
            if (LogSettings.MARKET) {
                QaLog.i(TAG, "onRestart(): orientation changed: " + orientation);
            }
            buildLayout(orientation, null, null);
        }
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onRestart(): finished");
        }
        
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);        
        
        int orientation = (newConfig.orientation != Configuration.ORIENTATION_UNDEFINED) 
                    ? newConfig.orientation
                    : DeviceUtils.calcDisplayOrientation(this);

        if (orientation != mDisplayOrientation) {
            if (LogSettings.MARKET) {
                QaLog.i(TAG, "configurationChanged(): orientation changed; rebuild");
            }
            buildLayout(orientation, null, null);
            
            updateControls(null, false);
            
            if(proximity != null){
                proximity.orientationChanged();
            }
            
        } else {
            if (LogSettings.MARKET) {
                QaLog.i(TAG, "configurationChanged(): orientation not changed");
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
        case KeyEvent.KEYCODE_BACK:
            if (BACK_KEY_DISABLED) {
                return true;
            }
            break;
        case KeyEvent.KEYCODE_SEARCH:
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }    
    
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
        case KeyEvent.KEYCODE_BACK:
            if (BACK_KEY_DISABLED) {
                return true;
            }
            break;
        case KeyEvent.KEYCODE_SEARCH:
            return true;
        }

        return super.onKeyUp(keyCode, event);
    }

//-------------------------------------------------------------------    
    /**
     * Set result and finish Activity
     */
    private void finish(int resultCode, long delay) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "finish(resultCode = " + resultCode + ", delay = " + delay + "ms)...");
        }
        mIsActivityFinishing = true;
        setResult(resultCode);
        if (delay > 0) {
            mTimeoutHandler.sendEmptyMessageDelayed(MSG_CLOSE_STATUS_SCREEN, delay);
        } else {
            finish();
        }
    }

    private class TimeoutHandler extends Handler {
        
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) { 
            case MSG_CLOSE_STATUS_SCREEN:
                if (!isFinishing()) {
                    finish();
                }
                return;
            }
        }
    }


    @Override
    protected void onResume() {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "onResume() is starting... ");
        }

        super.onResume();
        
        if(!getSipClient().registerCallback()){
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "onResume Register Callback failed. ");
            }
        }
        
        if( m_handler != null){
            try {
                Message msg = m_handler.obtainMessage(sf_update_duration, sf_update_duration, 0);
                m_handler.sendMessageDelayed(msg, 1000);
            } catch (java.lang.Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "onResume() send msg error " + th.toString());
                }
            }
            try {
                Message msg = m_handler.obtainMessage(sf_consistency_check, sf_consistency_check, 0);
                m_handler.sendMessageDelayed(msg, CONSISTENCY_CHECK_TIMEOUT);
            } catch (java.lang.Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "onResume() send cc msg error " + th.toString());
                }
            }
            
            
        }

        if (LogSettings.MARKET) {
            MktLog.d(TAG, "onResume() finished ");
        }
    }

    private void checkConcistency() {
        
        new Thread(new Runnable() {
            public void run() {
                try {
                    m_handler.removeMessages(sf_consistency_check);
                } catch (java.lang.Throwable th) {
                }
                if (isActivityFinishing()) return;
                int callCount = getSipClient().getNumberOfActiveCalls(2500);
                if (isActivityFinishing()) return;
                if( callCount <= 0) {
                    // Last chance for processing pending events
                    try {
                        Thread.currentThread().wait(1000);
                    } catch (java.lang.Throwable th) {
                    }
                    if (isActivityFinishing()) return;
                    callCount = getSipClient().getNumberOfActiveCalls(1000);
                    if (isActivityFinishing()) return;
                }

                if( callCount <= 0) {                           
                        getSipClient().ringOff();
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "checkConcistency: calls " + callCount + " TERMINATE");
                        }
                        finish();
                        return;
                } else {
                    if( m_handler != null){
                        try {
                            m_handler.removeMessages(sf_consistency_check);
                        } catch (java.lang.Throwable th) {
                        }
                        try {
                            Message msg = m_handler.obtainMessage(sf_consistency_check, sf_consistency_check, 0);
                            m_handler.sendMessageDelayed(msg, CONSISTENCY_CHECK_TIMEOUT);
                        } catch (java.lang.Throwable th) {
                            if (LogSettings.MARKET) {
                                MktLog.w(TAG, "checkConcistency() send cc msg error " + th.toString());
                            }
                        }
                    }
                }
            }
        }, "VCSactv:checkConcistency").start();
        
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (mInitComplete || !hasFocus) {
            return;
        }

        if (LogSettings.MARKET) {
            MktLog.d(TAG, "onWindowFocusChanged(" + hasFocus + "): FIRST START");
        }

        mInitComplete = true;
        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
            Intent activityStartedIntent = new Intent(RCMConstants.ACTION_CALL_STATE_ACTIVITY_STARTED);
            activityStartedIntent.putExtra(RCMConstants.EXTRA_CALL_STATE_ACTIVITY_STARTED, SystemClock.elapsedRealtime());
            getApplicationContext().sendBroadcast(activityStartedIntent);
        }
        /*
         * AB-445: Force delayed call handling (to prevent unnecessary call delay on fast phones)
         */
        int call_type = getIntent().getIntExtra(RCMConstants.EXTRA_VOIP_CALL_TYPE, RCMConstants.VOIP_CALL_TYPE_NONE);
        if (call_type == RCMConstants.VOIP_CALL_TYPE_OUTGOING) {
            long rc_call_id = getIntent().getLongExtra(RCMConstants.EXTRA_VOIP_OUTGOING_CALL_ID, -1);
            if (rc_call_id >= 0) {
                ((RingCentralApp)this.getApplication()).makeCallComplete(rc_call_id);
            }
        } else if (call_type == RCMConstants.VOIP_CALL_TYPE_INCOMING) {
            int call_action = getIntent().getIntExtra(RCMConstants.EXTRA_VOIP_CALL_ACTION, RCMConstants.VOIP_CALL_ACTION_NONE);
            CallInfo call_info = getIntent().getParcelableExtra(RCMConstants.EXTRA_VOIP_INCOMING_CALL_INFO);
            long rc_call_id = call_info.getRcCallId();

            switch(call_action) {
            case RCMConstants.VOIP_CALL_ACTION_ANSWER:
                ((RingCentralApp)this.getApplication()).callAnswer(rc_call_id, 0);
                break;
            case RCMConstants.VOIP_CALL_ACTION_ANSWER_AND_HOLD:
                ((RingCentralApp)this.getApplication()).callAnswerAndHold(rc_call_id, 0);
                break;
            case RCMConstants.VOIP_CALL_ACTION_ANSWER_AND_HANGUP:
                ((RingCentralApp)this.getApplication()).callAnswerAndHangup(rc_call_id, 0);
                break;
            }
        }
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onPause is starting... ");
        }
        if ( m_handler != null ) {
            try {
                m_handler.removeMessages(sf_update_duration);
            } catch (java.lang.Throwable th) {
            }
            try {
                m_handler.removeMessages(sf_consistency_check);
            } catch (java.lang.Throwable th) {
            }
        }
    }
    
    
    @Override
    public void onDestroy(){
        super.onDestroy();
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onDestroy is starting...");
        }
        
        getSipClient().ringOff();
        
        getSipClient().unregisterCallback();
        getSipClient().unBindSipServiceRoutine(getApplicationContext());
        
        m_callsListAdapter = null;
        mCallDialpad.clearDialpad();
        
        if(mPhotoCache != null){
        	mPhotoCache.destroy();
        	mPhotoCache = null;
        }
        
        releaseResources();                         
        
        leakCleanUpRootView();
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onDestroy: finished...");
        }
        
    }

    
    /**
     * 
     * Update state of controls
     * 
     * @param compInfo
     * @param finishing
     *            defines if the activity will be turned into finishing state
     */
    private void updateControls(CompletionInfo compInfo, boolean finishing){
        final CallInfo callInfo = getSipClient().getCurrentLineCallInfo();                                
        
       if( callInfo == null ){
            mBtnEndCall.setEnabled(false);
            mBtnMute.setEnabled(false);
            mBtnHold.setEnabled(false);
            mBtnSpeaker.setEnabled(false);
            mBtnDialpad.setEnabled(false);
            mBtnBluetooth.setEnabled(false);
            toggleDialpadBtn(false);
            mCallDialpad.clearDialpad();          
        } else {
        	
			final long callAcceptedTime = callInfo.getCallAcceptedTime();
			if (callAcceptedTime > 0) {
				mCallDurationView.setText(android.text.format.DateUtils.formatElapsedTime((SystemClock.elapsedRealtime() - callAcceptedTime) / 1000));
                mCallDurationView.setVisibility(View.VISIBLE);
            } else {
                mCallDurationView.setVisibility(View.INVISIBLE);
            }
            
            if (finishing) {
				mBtnHold.setEnabled(false);
                mBtnMute.setEnabled(false);
				mBtnBluetooth.setEnabled(false);
                mBtnSpeaker.setEnabled(false);
				toggleDialpadBtn(false);
				mBtnDialpad.setEnabled(false);
				mBtnEndCall.setEnabled(false);
            } else {
				/*
				 * If call state is not NULL then Enable possibility to hangup
				 * it.
				 */
				final boolean isPlayback = getSipClient().isPlaybackDevice();

                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "updateControls: audio: isPlayback:" + isPlayback + "; mAudioState(!=null).isSpeakerEnabled:"
                            + ((mAudioState != null) && mAudioState.isSpeakerEnabled()));
                    MktLog.i(TAG, "updateControls: media: " + callInfo.getMedia_state());
                }
				
                mBtnSpeaker.setEnabled(isPlayback && (mAudioState != null) && mAudioState.isSpeakerEnabled());
				mBtnSpeaker.setSelected((mAudioState != null) && mAudioState.isSpeaker());
        	
				mBtnBluetooth.setEnabled(isPlayback && (mAudioState != null) && mAudioState.isBluetoothEnabled());				
				mBtnBluetooth.setSelected((mAudioState != null) && mAudioState.isBluetooth());

                if ((callInfo.getCall_state() == RCSIP.PJSUA_CALL_STATE_DISCONNECTED) || callInfo.isFinishing()) {
                    mBtnEndCall.setEnabled(callInfo.isOutbound() && !callInfo.isFinishing());
                    mBtnHold.setEnabled(false);

                    mBtnDialpad.setEnabled(false);
                    toggleDialpadBtn(false);

                } else {
                    mBtnEndCall.setEnabled(true);
                    mBtnDialpad.setEnabled((callInfo.getCall_state() == RCSIP.PJSUA_CALL_STATE_CONFIRMED));
                    mBtnMute.setEnabled(isPlayback && (callInfo.getCall_state() == RCSIP.PJSUA_CALL_STATE_CONFIRMED));

                    if (callInfo.getMedia_state() == RCSIP.PJSUA_CALL_MEDIA_ACTIVE || callInfo.getMedia_state() == RCSIP.PJSUA_CALL_MEDIA_REMOTE_HOLD) {
                        mBtnHold.setEnabled(true);
                        if (!isPlayback) {
                            mBtnMute.setEnabled(true);
                        }
                    }

					if (callInfo.getMedia_state() == RCSIP.PJSUA_CALL_MEDIA_LOCAL_HOLD
							|| callInfo.isHold()) {
                        mBtnHold.setEnabled(true);
                    }
                
					if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
					    boolean switching = mBtnHold.isSelected() ^ callInfo.isHold();
					    if (switching){
					        PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.HOLD);
					    }
					}
                    mBtnHold.setSelected( callInfo.isHold() );
                }
                if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
                    boolean switching = mBtnMute.isSelected() ^ isMute();
                    if (switching){
                        PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.MUTE);
                    }
                }
                mBtnMute.setSelected(isMute());
            }
        }     
       updateCallInfoView(callInfo, compInfo);
    }    
    
    private void updateCallInfoView(CallInfo callInfo, CompletionInfo compInfo){
    	
    	if(callInfo == null) {
    	    if (LogSettings.MARKET) {
    	        MktLog.w(TAG, "updateControls callInfo = null");
    	    }
            
    	    int call_type = getIntent().getIntExtra(RCMConstants.EXTRA_VOIP_CALL_TYPE, RCMConstants.VOIP_CALL_TYPE_NONE);
    	    if (call_type == RCMConstants.VOIP_CALL_TYPE_OUTGOING && mOutgoingCallInitialStatusMessageSet == false) {
    	        mCallStatusView.setText(R.string.voip_call_connecting);
    	        mOutgoingCallInitialStatusMessageSet = true;
    	    }
    	    
            if ((compInfo != null)) {
                if (!compInfo.isSuccess()) {
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "updateControls callInfo = null - set unsuccess status");
                    }
                    mCallStatusView.setText(getErrorPromptOnUnsuccess(compInfo, getString(R.string.voip_call_general_failure)));
                } else {
                    mCallStatusView.setText(getString(R.string.voip_call_ended));
                }
            }
            
    	} else {
            
            final String callStatus = getCallStateNameByCode(
                    ( callInfo.getCall_state() == RCSIP.PJSUA_CALL_STATE_DISCONNECTED) || callInfo.isFinishing()  ?  
                            RCSIP.PJSUA_CALL_STATE_DISCONNECTED : 
                                         callInfo.getCall_state(), compInfo, callInfo.getNetworkType());
            
            if (LogSettings.MARKET) {
                MktLog.d(TAG, " getCallStateNameByCode CallId:" + callInfo.getRcCallId() +  
                		" State: " + callInfo.getCall_state() + 
                		" Status: " + callStatus + 
                		" Media state: " + callInfo.getMedia_state() + 
                		" Finishing: " + callInfo.isFinishing() + 
                		" Outbound: "  + callInfo.isOutbound() + 
                		" NetType: "   + callInfo.getNetworkType());
            }
            
            mCallStatusView.setText(callStatus);

            if( getSipClient().getActiveLines() > LINES_LIST_ENABLED ){
                mNameView.setVisibility(View.GONE);
                mNumberView.setVisibility(View.GONE);
            }
            else{
                String contact = callInfo.getFromName();                 
                String number  = callInfo.getFromNumber();
                number = ( CallInfo.isUnknown(number) ? getApplicationContext().getString(R.string.calllog_display_name_unknown) : PhoneUtils.getLocalCanonical(number));
                
                if (LogSettings.ENGINEERING) {
                    EngLog.i(TAG, " updateControls contact : " + contact + " number : " +number);
                }
                
                if(TextUtils.isEmpty(contact)){
                    mNameView.setVisibility(View.GONE);                                        
                } else {
                    mNameView.setText( contact );
                    mNameView.setVisibility(View.VISIBLE);
                }

                if( !TextUtils.isEmpty(contact) || TextUtils.isEmpty(number) ){
                    mNumberView.setVisibility(View.GONE);
                } else {
                    mNumberView.setText(number);
                    mNumberView.setVisibility(View.VISIBLE);
                }
            }
             
            mPhotoCache.getPhoto(((callInfo != null)&&(callInfo.getPhotoID()!=null))?callInfo.getPhotoID() : -1, mPhotoView);
            
    	}
    }               

    
    /*
     * 
     */
    private String getCallStateNameByCode( int state , CompletionInfo compInfo, int netState){        
        switch(state) {
        case RCSIP.PJSUA_CALL_STATE_CREATED:
        case RCSIP.PJSUA_CALL_STATE_INIT:
        case RCSIP.PJSUA_CALL_STATE_EARLY:
        case RCSIP.PJSUA_CALL_STATE_CALLING:
        case RCSIP.PJSUA_CALL_STATE_CONNECTING:
            return getString(R.string.voip_call_connecting);
        case RCSIP.PJSUA_CALL_STATE_CONFIRMED:
        	if( netState == ConnectivityManager.TYPE_WIFI)
        		return getString(R.string.voip_call_connected_over_wifi);
        	else
        		return getString(R.string.voip_call_connected_over_3g);
        case RCSIP.PJSUA_CALL_STATE_DISCONNECTED: {
            String prompt = getString(R.string.voip_call_ended);
            if ((compInfo != null) && (!compInfo.isSuccess()) 
                    && compInfo.getEndingReason() != RCCallInfo.CALL_ENDED_BY_ERROR 
                    && compInfo.getEndingReason() != RCCallInfo.CALL_ENDED_BY_PARTY) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "getCallStateNameByCode DISCONNECTED - unsuccess status prompt");
                }
                prompt = getErrorPromptOnUnsuccess(compInfo, prompt);
            }
            return prompt;
        }
        }
        return null;
    }
    
    /**
     * Returns user-friendly on unsuccessful CompletionInfo. See AB-29
     * 
     * @param compInfo
     *            completion info
     * @param defaultprompt
     *            default prompt
     * @return the status prompt
     */
    private final String getErrorPromptOnUnsuccess(CompletionInfo compInfo, String defaultPrompt) {
        String prompt = defaultPrompt;
        if ((compInfo != null) && (!compInfo.isSuccess())) {
            int errorCode = compInfo.getErrorCode();
            int subErrorCode = compInfo.getErrorSubCode();
            if (errorCode == CompletionInfo.ERROR_PJSIP
                    && (subErrorCode == RCSIP.SipCode.PJSIP_SC_BUSY_HERE.code() || subErrorCode == RCSIP.SipCode.PJSIP_SC_BUSY_EVERYWHERE.code())) {
                prompt = getString(R.string.voip_call_line_busy);
            } else {
                prompt = getString(R.string.voip_call_network_failure, compInfo.getErrorCode(), compInfo.getErrorSubCode());
            }
        }
        return prompt;
    }
    
    /*
     * 
     */
    private void updateCallDuration() {
        
        if( getSipClient().getActiveLines() > LINES_LIST_ENABLED ){
        //    getCallsListAdapter().invalidate();
        }
        else{
            final CallInfo callInfo = getSipClient().getCurrentLineCallInfo();
            
            if( callInfo != null && callInfo.getCallStartTime() > 0 && callInfo.isActive()) {
                long t =  callInfo.getCallAcceptedTime();
                if (t > 0) {
                    mCallDurationView.setText(android.text.format.DateUtils.formatElapsedTime(
                            (SystemClock.elapsedRealtime() - t)/1000 ));
                    mCallDurationView.setVisibility(View.VISIBLE);
                } else {
                    mCallDurationView.setVisibility(View.INVISIBLE);
                }
            }           
        }
        
        if( m_handler != null){
            Message msg = m_handler.obtainMessage(sf_update_duration, sf_update_duration, 0);
            m_handler.sendMessageDelayed(msg, 1000);
        }
    }



    private final class UIMessageHandler extends Handler {
        
        public void handleMessage( Message msg ){
            
            if (isActivityFinishing()) {
                return;
            }
            
            if( msg.what == SipClient.sf_update_controls ){             
                if (LogSettings.MARKET) {
                	MktLog.d(TAG, "UIMessageHandler sf_update_controls");
                }               
                updateControls(null, false);
            }
            else if( msg.what == SipClient.sf_switch_view ){
                
                if (LogSettings.MARKET) {
                	MktLog.d(TAG, " UIMessageHandler sf_switch_view");
                }
                switchCallInfoView();
                
            }
            else if( msg.what == SipClient.sf_set_mute ){               
                boolean isMute = ( msg.arg1 == SipClient.MUTE_ON );
                if (LogSettings.MARKET) {
                	MktLog.d(TAG, " UIMessageHandler sf_set_mute isMute: " + isMute );
                }
                setMute( isMute );

            }
            else if( msg.what == SipClient.sf_synchronize_call_state ){
                
                final boolean isDestroyIfNoActiveCalls = ( msg.arg1 > 0 );
                if (LogSettings.MARKET) {
                	MktLog.i(TAG, " UIMessageHandler sf_synchronize_call_state isDestroyIfNoActiveCalls:" + isDestroyIfNoActiveCalls );
                }
                
                final int callCount = getSipClient().syncCallInfo();
                if( isDestroyIfNoActiveCalls && callCount == 0 ){
                    if (LogSettings.MARKET) {
                    	MktLog.w(TAG, " UIMessageHandler sf_synchronize_call_state syncCallInfo No active calls");
                    }
                    
                    getSipClient().sendMessage( new CompletionInfo(true, true, RCCallInfo.CALL_ENDED_BY_SYSTEM, CompletionInfo.ERROR_UNKNOWN, 0, null, true));
                }
                
            }
            else if( msg.what == SipClient.sf_completion ){
                CompletionInfo info = (CompletionInfo)msg.obj;              
                if (LogSettings.MARKET) {
                	MktLog.i(TAG, " UIMessageHandler sf_completion info : " + info);
                }               
                if( info != null ){
                    onCompletion( info );
                }
            }
            else if (msg.what == SipClient.sf_update_call_state ) {
                if (LogSettings.MARKET) {
                	MktLog.i(TAG, " UIMessageHandler sf_update_call_state");
                }
                updateUICurrentCallState();
            }
            else if( msg.what == sf_call_hangup ){
                if( msg.obj != null ){
                    long callId = ((Long)msg.obj).longValue();
                    
                    if (LogSettings.MARKET) {
                    	MktLog.i(TAG, " UIMessageHandler sf_call_hangup ID:" + callId );
                    }
                    
                    getSipClient().hangup( callId );
                }
            }
            else if (msg.what == sf_update_duration) {
                    updateCallDuration();
            }
            else if (msg.what == sf_consistency_check) {
                checkConcistency();
            }

            else if (msg.what == sf_lock_screen){
                if (LogSettings.MARKET) {
                	MktLog.d(TAG, " UIMessageHandler sf_lock_screen");
                }               
                lockScreen(ProximityUtils.PROXIMITY_DIM_NO_SENSOR);
            }
            else if (msg.what == sf_unlock_screen){
                if (LogSettings.MARKET) {
                	MktLog.d(TAG, " UIMessageHandler sf_unlock_screen");
                }               
                unlockScreen(ProximityUtils.PROXIMITY_DIM_NO_SENSOR);
            }
            else if( msg.what == SipClient.sf_update_audio) {
            	if(msg.obj != null){
	            	final AudioState newState = (AudioState) msg.obj;
	                if (LogSettings.MARKET) {
	                	MktLog.i(TAG, " UIMessageHandler sf_update_audio : " + newState );                 
	                }
	                
               		mAudioState = newState;
               		updateControls(null, false);
            	}
            } 
  
        }
    }
    

    private void sendMessage(int what, int arg1, long id) {
        
        if (m_handler != null) {
            try {
                Message msg = Message.obtain(m_handler, what );
                msg.arg1 = arg1;
                msg.obj = new Long(id);
                msg.sendToTarget();
            } catch (Throwable e) {
            }
        }
    }
      
            
    public void updateUICurrentCallState() {

        if( getSipClient().getActiveLines() > LINES_LIST_ENABLED ){
            getCallsListAdapter().updateCallsList( getSipClient().getCallsArray());
        }       
        
        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
            PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.CALLS_SWITCHING);
        }
        
        final CallInfo callInfo = getSipClient().getCurrentLineCallInfo();
        if( callInfo == null ) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, " updateUICurrentCallState: wrong call id Send sf_update_controls & sf_synchronize_call_state ");
            }
            sendMessage( SipClient.sf_update_controls, 0, 0 );
            
        } else {

            if (LogSettings.MARKET) {
                MktLog.w(TAG, " updateUICurrentCallState callId:" + callInfo.getRcCallId() + 
                		" State:"+ callInfo.getCall_state());
            }
            
                switch(callInfo.getCall_state()) {
                case RCSIP.PJSUA_CALL_STATE_CREATED:
                    sendMessage( SipClient.sf_update_controls, 0, 0 );                    
                    sendMessage(sf_lock_screen, 0, 0 );
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, " updateUICurrentCallState: PJSUA_CALL_STATE_CREATED Send sf_update_controls");
                    }
                    break;
                case RCSIP.PJSUA_CALL_STATE_INIT:
                    sendMessage( SipClient.sf_update_controls, 0, 0 );                    
                    sendMessage(sf_lock_screen, 0, 0 );
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, " updateUICurrentCallState: PJSUA_CALL_STATE_INIT Send sf_update_controls");
                    }
                    break;
                
                case RCSIP.PJSUA_CALL_STATE_EARLY:
                    sendMessage( SipClient.sf_update_controls, 0, 0 );                    
                    sendMessage(sf_lock_screen, 0, 0 );
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, " updateUICurrentCallState: PJSUA_CALL_STATE_EARLY Send sf_update_controls");
                    }
                    break;
                case RCSIP.PJSUA_CALL_STATE_CALLING:
                case RCSIP.PJSUA_CALL_STATE_CONNECTING:
                    sendMessage( SipClient.sf_update_controls, 0, 0 );
                    sendMessage(sf_lock_screen, 0, 0 );
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, " updateUICurrentCallState: PJSUA_CALL_STATE_CONNECTING & CALLING Send sf_update_controls");
                    }
                    break;
                case RCSIP.PJSUA_CALL_STATE_CONFIRMED:
                    sendMessage( SipClient.sf_update_controls, 0, 0 );                    
                    sendMessage(sf_lock_screen, 0, 0 );
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, " updateUICurrentCallState: PJSUA_CALL_STATE_CONFIRMED Send sf_update_controls");
                    }
                    break;                    
                case RCSIP.PJSUA_CALL_STATE_DISCONNECTED:
                    sendMessage( SipClient.sf_update_controls, 0, 0 );
                    if( getSipClient().getActiveLines() <= LINES_LIST_ENABLED ){
                        sendMessage(sf_unlock_screen, 0, 0 );
                    }
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, " updateUICurrentCallState: PJSUA_CALL_STATE_DISCONNECTED Send sf_update_controls");
                    }
                    break;
                default:
                    break;
                }
        }
    }
    
    /*
     * Proximity convenience methods
     */
    private void lockScreen(int prMode){
        if( LogSettings.ENGINEERING ) {
            EngLog.i(TAG, "lockScreen() : " + prMode);              
        }
        
        if(proximity != null){
            proximity.lockScreen(prMode);
        }
    }
    
    private void unlockScreen(int prMode){
        if( LogSettings.ENGINEERING ) {
            EngLog.i(TAG, "unlockScreen() : " + prMode);                
        }
        
        if(proximity != null){
            proximity.unlockScreen(prMode);
        }
    }
   
    /*
     * 
     */
    private class CallsListAdapter extends BaseAdapter{

        private CallInfo[] m_calls;
        
        public CallsListAdapter( final CallInfo[] calls ){
            m_calls = calls;
        }
        
        public synchronized void invalidate(){
            notifyDataSetChanged();
        }
        
        public synchronized void updateCallsList(final CallInfo[] calls ){
            m_calls = calls;
            notifyDataSetChanged();

            if(LogSettings.ENGINEERING){
                EngLog.i(TAG, "Call list updateCallsList" );
            }
        }
        
        @Override
        public int getCount() {
            return ( m_calls == null ? 0 : m_calls.length );
        }

        @Override
        public Object getItem(int position) {
            return ( m_calls == null || position >= m_calls.length ? null : m_calls[position]);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if( m_calls == null || position >= m_calls.length )
                return null;
            
            long callId = getSipClient().getCurrentLineCallId();
            
            ViewHoder holder = null;
            
            if (convertView == null) {
                convertView = getLayoutInflater().inflate( 
                        (DeviceUtils.getDisplayOrientation(VoipCallStatusActivity.this) == Configuration.ORIENTATION_LANDSCAPE ? 
                                R.layout.voip_call_list_item_landscape : 
                                    R.layout.voip_call_list_item ), 
                                    parent, false);
                holder = new ViewHoder();
                holder.photo = (ContactPhotoView) convertView.findViewById(R.id.image);
                holder.status = (Chronometer) convertView.findViewById(R.id.status);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                convertView.setTag(holder);
            } else {
            	holder = (ViewHoder) convertView.getTag();
            }
                        
            String number = m_calls[position].getFromNumber();
            number = ( CallInfo.isUnknown(number) ? getApplicationContext().getString(R.string.calllog_display_name_unknown) : PhoneUtils.getLocalCanonical(number));
            
            String name = number;
            
            String contact = m_calls[position].getFromName();
            
            if( !CallInfo.isUnknown(contact)){
            	name = contact;
            }
            
            holder.name.setText( name );
            
            // TODO : Sync states and duration
            int state = m_calls[position].getCall_state();
            switch(state) {
            case RCSIP.PJSUA_CALL_STATE_CREATED:
            case RCSIP.PJSUA_CALL_STATE_INIT:
            case RCSIP.PJSUA_CALL_STATE_EARLY:
            case RCSIP.PJSUA_CALL_STATE_CALLING:
            case RCSIP.PJSUA_CALL_STATE_CONNECTING:
                holder.status.stop();
                holder.status.setText(getApplicationContext().getString(R.string.voip_call_connecting_wo_dots));
                break;
            case RCSIP.PJSUA_CALL_STATE_CONFIRMED:
                if (m_calls[position].isHold()) {
                    holder.status.stop();
                    holder.status.setText(getApplicationContext().getString(R.string.voip_call_screen_onhold_state_label));
                } else {
                    holder.status.setBase(m_calls[position].getCallAcceptedTime());
                    holder.status.start();
                }    
                break;
            case RCSIP.PJSUA_CALL_STATE_DISCONNECTED: {
                holder.status.stop();
                holder.status.setText(getApplicationContext().getString(R.string.voip_call_ended));
                break;
            }
            }
            
            mPhotoCache.getPhoto(((m_calls[position]!=null)&&(m_calls[position].getPhotoID()!=null))?m_calls[position].getPhotoID() : -1, holder.photo);
            
            if( callId == m_calls[position].getRcCallId() ){//selected item
            	convertView.setBackgroundResource(R.drawable.voip_call_list_item_selected);
            	holder.name.setTextColor(getResources().getColor(R.color.textColorWhite));
            	holder.status.setTextColor(getResources().getColor(R.color.textColorWhite));
            }
            else{
            	convertView.setBackgroundResource(R.drawable.voip_call_list_item_background_selector);
            	holder.name.setTextColor(R.color.voip_call_list_item_hold_name_text_color);
            	holder.status.setTextColor(R.color.voip_call_list_item_hold_name_text_color);
            }
            
            return convertView;
        }
    };
    static class ViewHoder {
    	ContactPhotoView photo;
    	TextView name;
    	Chronometer status;
    }
    
    // ====================== Workaround Section (BN) ==========================
    // == Reduces memory leaks. The section can be just added to any Activity ==
    // == Call leakCleanUpRootView onDestroy and before setting new root view ==
    
    @Override
    public void setContentView(int layoutResID) {
        ViewGroup rootView = (ViewGroup) 
            LayoutInflater.from(this).inflate(layoutResID, null);
        setContentView(rootView);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        leakCleanUpRootView();
        mLeakContentView = (ViewGroup) view;
    }

    @Override
    public void setContentView(View view, LayoutParams params) {
        super.setContentView(view, params);
        leakCleanUpRootView();
        mLeakContentView = (ViewGroup) view;
    }
    
    /**
     * Cleanup root view to reduce memory leaks.
     */
    private void leakCleanUpRootView() {
        if (mLeakContentView != null) {
            ViewGroup v = mLeakContentView;
            mLeakContentView = null;
            mStackRecursions = 0;
            leakCleanUpChildsDrawables(v);
            System.gc();
        }
    }
    
    /**
     * Clean-up Drawables in the view including child.
     * 
     * @param v
     */
    private void leakCleanUpChildsDrawables(View v) {
        if (v != null) {
            try {
                ViewGroup group = (ViewGroup) v;
                int childs = group.getChildCount();
                for (int i = 0; i < childs; i++) {
                    if (LEAKS_CLEAN_UP_STACK_RECURSIONS_LIMIT > mStackRecursions) {
                        mStackRecursions++;
                        leakCleanUpChildsDrawables(group.getChildAt(i));
                        mStackRecursions--;
                    } else {
                        break;
                    }
                }
            } catch (java.lang.Throwable th) {
            }
            leakCleanUpDrawables(v);
        }
    }

    /**
     * Keeps recursions number for memory clean-ups.
     */
    private int mStackRecursions;
    
    /**
     * Limit of leaks clean-ups stack recursions.
     */
    private static final int LEAKS_CLEAN_UP_STACK_RECURSIONS_LIMIT = 256;
    
    /**
     * Cleans drawables of in the view.
     * 
     * @param v the view to clean-up
     */
    private void leakCleanUpDrawables(View v) {
        if (v == null) {
            return;
        }
            
        try {
            if (v.getBackground() != null) {
                v.getBackground().setCallback(null);
            }
        } catch (java.lang.Throwable th) {
        }

        try {
            v.setBackgroundDrawable(null);
        } catch (java.lang.Throwable th) {
        }

        try {
            ImageView imageView = (ImageView) v;
            imageView.setImageDrawable(null);
            imageView.setBackgroundDrawable(null);
        } catch (java.lang.Throwable th) {
        }

    }
    
    /**
     * Keeps current root view.
     */
    private ViewGroup mLeakContentView = null;
        
    
 // ====================== Workaround Section END (BN) ==========================
}