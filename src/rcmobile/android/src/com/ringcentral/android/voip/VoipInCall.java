/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.voip;

import java.lang.reflect.Field;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.rcbase.android.sip.client.RCSIP;
import com.rcbase.android.sip.service.CallInfo;
import com.rcbase.android.sip.service.IServicePJSIP;
import com.rcbase.android.sip.service.IServicePJSIPCallback;
import com.rcbase.android.sip.service.PJSIP;
import com.rcbase.android.sip.service.RCCallInfo;
import com.rcbase.android.sip.service.ServicePJSIP;
import com.rcbase.android.utils.media.AudioState;
import com.rcbase.api.xml.XmlSerializableObjectAbstract;
import com.rcbase.parsers.sipmessage.SipMessage;
import com.rcbase.parsers.sipmessage.SipMessageInput;
import com.rcbase.parsers.sipmessage.SipMessageOutputSimple;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.DeviceUtils;
import com.ringcentral.android.utils.PhoneUtils;
import com.ringcentral.android.utils.ui.widget.ContactPhotoView;

/**
 * Incoming Call Activity
 * 
 * @author Denis Kudja
 *
 */
public class VoipInCall extends Activity {
    private static final String TAG = "[RC]VoipInCall";

    private final static int sf_finish					= 1;
    private final static int sf_update_controls			= 2;
    private final static int sf_update_controls_button	= 3;
    private final static int sf_update_photo 			= 4;
    private final static int sf_back_pressed			= 5;
    private final static int sf_reject					= 6;
    private final static int sf_answer					= 7;
    private final static int sf_answer_and_hold			= 8;
    private final static int sf_answer_and_hangup		= 9;
    private final static int sf_sync_call_state         = 10;
    private final static int sf_ringtone_off			= 11;
    
    private final UIMessageHandler m_handler = new UIMessageHandler();
    
    private CallInfo m_callInfo = null;
    private int m_countActiveCall = 0;
    
    private int mDisplayOrientation;
    private ImageButton m_btnAnswer;
    private ImageButton m_btnAnswer_and_hold;
    private ImageButton m_btnAnswer_and_hangup;

    private ImageButton m_btnIgnore;
    private TextView m_fromName;
    private TextView m_fromNumber;
    private ContactPhotoView mPhotoView;    
    private ContactPhotoCache mPhotoCache;

    private TelephonyManager m_telefMng = null;
    private PhoneStateChangedReceiver 	m_phstateReceiver 		= null;       
    
    boolean m_anwserEnabled = true;
    protected synchronized boolean isAnwserEnabled() {
		return m_anwserEnabled;
	}

	protected synchronized void setAnwserEnabled(boolean anwserEnabled) {
		m_anwserEnabled = anwserEnabled;
	}

	boolean m_processed = false;
    
    protected boolean isOtherCalls(){
    	return  ( m_countActiveCall > 1 );
    }
    
	protected boolean isProcessed() {
		return m_processed;
	}

	protected void setProcessed(boolean processed) {
		m_processed = processed;
	}

	/**
	 *	RC PJSIP service interface 
	 */
	private IServicePJSIP m_service = null;
    
	/*
	 * 
	 */
	private boolean bindSipService( Context context ){
        Intent sipService = new Intent( context, ServicePJSIP.class );
        boolean binded = context.bindService(sipService, m_serviceConn, Context.BIND_AUTO_CREATE);
        if( binded ) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "ServicePJSIP was binded successfuly");
            }
            return true;
        } 
        
        if (LogSettings.MARKET) {
            MktLog.e(TAG, "Failed to bind ServicePJSIP" );
        }

        return false;
	}
	
	/**
	 * Unbind RC PJSIP Service
	 */
	private void unBindSipService( Context context ) {

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "unBindSipServiceRoutine():start... Valid context:" + 
            		( context != null ) + " valid service:" + ( m_serviceConn != null ) );
        }
		
		if( context != null ){
            
            try {
                if( m_serviceConn != null ) {
                	context.unbindService( m_serviceConn );
                }
            } catch (Throwable e) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "unBindSipServiceRoutine():unbindService:error:" + e.toString());
                }
            } finally {
                m_serviceConn = null;
            }
		}
		
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "unBindSipService finished");
        }
		
	}
	
	/**
	 * Service connection implementation
	 */
	private ServiceConnection m_serviceConn = new ServiceConnection(){

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "onServiceConnected is starting...");
            }

            m_service = IServicePJSIP.Stub.asInterface(service);
            if (m_service == null) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "onServiceConnected(): Failed to get IServicePJSIP");
                }
                return;
            }
            
            registerCallback();
            
            sendMessage(sf_sync_call_state);
            
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "onServiceConnected finished");
            }
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "onServiceDisconnected is starting...");
            }
            
            m_service = null;
		}
	};
	
    /*
     * (non-Javadoc)
     * @see android.app.Activity#onNewIntent(android.content.Intent)
     */
    @Override
	protected void onNewIntent(Intent newIntent){
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "onNewIntent(): **** Started");
        }
        
        setIntent( newIntent );
	}
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "onCreate(): Started");
        }          
                        
        if (savedInstanceState != null) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "onCreate(): Activity re-creation forbidden; FINISH");
            }
            finish();
            return;
        }    
        
        m_countActiveCall = getIntent().getIntExtra( RCMConstants.EXTRA_VOIP_INCOMING_CALL_INFO_COUNT, 0);
        if (m_countActiveCall < 1) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "onCreate(): Invalid calls count: " + m_countActiveCall + "; FINISH");
            }
            finish();
            return;
        }
        
        m_callInfo = getIntent().getParcelableExtra( RCMConstants.EXTRA_VOIP_INCOMING_CALL_INFO );
        if( m_callInfo == null ) {
            MktLog.e(TAG, "onCreate(): Invalid CallInfo; FINISH");
            finish();
            return;
        }

        if (m_callInfo.getCall_state() != RCSIP.PJSUA_CALL_STATE_INCOMING) {
            MktLog.e(TAG, "onCreate(): Invalid call state: " + RCSIP.getCallStateForLog(m_callInfo.getCall_state()) + "; FINISH");
            finish();
            return;
        }
        
//        if( m_callInfo.getRemoteContact() == null ){
//            final String number = CallInfo.getNumberFromURI(m_callInfo.getRemoteURI());
//            if( !CallInfo.isUnknown(number) ){
//                if (LogSettings.MARKET) {
//                    MktLog.i(TAG, "onCreate(): Incoming call. Find contact for number: " + number );
//                }
//                new DetectContactNameByNumber( number ).execute(getApplicationContext());
//            }               
//        }
        
        if( !bindSipService(getApplicationContext())){
            MktLog.e(TAG, "onCreate(): Bind failed; FINISH");
            finish();
            return;
        }

        
        mPhotoCache = new ContactPhotoCache();
        
        if( m_telefMng == null ){
        	m_telefMng = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        	
        }

        if( m_telefMng != null ){
        	/*
        	 * If we active native calls do not allow to accept SIP call
        	 */
        	setAnwserEnabled( TelephonyManager.CALL_STATE_OFFHOOK != m_telefMng.getCallState() );
 
        	m_phstateReceiver = new PhoneStateChangedReceiver();
        	IntentFilter intentFilterPhoneState = new IntentFilter(	android.telephony.TelephonyManager.ACTION_PHONE_STATE_CHANGED );
        	registerReceiver(m_phstateReceiver, intentFilterPhoneState);
        }
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        buildLayout(DeviceUtils.getDisplayOrientation(this));
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);     
        /*getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);*/
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onCreate(): finished Active calls: " + m_countActiveCall);
        }
    } 

    @Override
    protected void onRestart() {
        super.onRestart();

        if (LogSettings.MARKET) {
            MktLog.w(TAG, "onRestart(): Started");
        }
        
        //onConfigurationChanged() is not always called when the Activity becomes visible (Android bug?)
        //That's why display orientation is checked here
        int orientation = DeviceUtils.getDisplayOrientation(this);
        if (orientation != mDisplayOrientation) {
            if (LogSettings.MARKET) {
                QaLog.i(TAG, "onRestart(): orientation changed: " + orientation);
            }
            buildLayout(orientation);
        }
        
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "onRestart(): Finished");
        }
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        int orientation = newConfig.orientation;
        
        if (orientation == Configuration.ORIENTATION_UNDEFINED) {
            orientation = DeviceUtils.calcDisplayOrientation(this);
            if (LogSettings.MARKET) {
                QaLog.w(TAG, "onConfigurationChanged(): ORIENTATION_UNDEFINED; calculated orientation = " + orientation);
            }
        }

        if (orientation != mDisplayOrientation) {
            if (LogSettings.MARKET) {
                QaLog.i(TAG, "configurationChanged(): orientation changed; rebuild");
            }
            buildLayout(orientation);
        } else {
            if (LogSettings.MARKET) {
                QaLog.i(TAG, "configurationChanged(): orientation not changed");
            }
        }
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	FlurryTypes.onStartSession(this);    	
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	FlurryTypes.onEndSession(this);
    }
    
    @Override
    public void onDestroy(){
        super.onDestroy();
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "onDestroy is starting...");
        }
        

		if( m_telefMng != null ){
			m_telefMng = null;
		}
        
		if( m_phstateReceiver != null ){
			unregisterReceiver( m_phstateReceiver );
			m_phstateReceiver = null;
		}
		
		if(mPhotoCache != null){
        	mPhotoCache.destroy();
        	mPhotoCache = null;
        }
		
        unregisterCallback();
        unBindSipService( getApplicationContext());
		
        leakCleanUpRootView();
		
		if (LogSettings.MARKET) {
            MktLog.i(TAG, "onDestroy: finished...");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onPause is starting... ");
        }             
        
        sendMessage( sf_ringtone_off );
    }
    
    @Override
    public void onResume(){
        super.onResume();                
        
    	if (LogSettings.MARKET) {
    	    MktLog.w(TAG, "onResume is starting... ");
    	}
        
    	registerCallback();
    	
		if (LogSettings.MARKET) {
            MktLog.i(TAG, "onResume: finished...");
        }
    }    
    
    @Override
    public boolean onSearchRequested() {
        return false;   //disable search button
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED && hasFocus) {
            Intent activityStartedIntent = new Intent(RCMConstants.ACTION_INVOIP_ACTIVITY_STARTED);
            activityStartedIntent.putExtra(RCMConstants.EXTRA_INVOIP_ACTIVITY_STARTED, SystemClock.elapsedRealtime());
            getApplicationContext().sendBroadcast(activityStartedIntent);
        }
    }
    
    /**
     * Register RC SIP service callback
     */
    private void registerCallback(){
    	
        if (LogSettings.ENGINEERING) {
            MktLog.w(TAG, "registerCallback(): Started");
        }
    	
    	if( m_service == null ){
            if (LogSettings.ENGINEERING) {
                MktLog.w(TAG, "registerCallback(): Failed. Service is not initialized");
            }
    		return;
    	}
    	
        try {
       		m_service.registerCallback( m_callback.hashCode(), m_callback );
       		
            if (LogSettings.ENGINEERING) {
                MktLog.w(TAG, "registerCallback(): Successfull");
            }
            
        } catch (Throwable e) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "registerCallback(): Failed to register calback: RemoteException", e);
            }
        }
    }
    
    /**
     * Unregister RC SIP Service callback 
     */
    private void unregisterCallback(){

        if (LogSettings.ENGINEERING) {
            MktLog.i(TAG, "unregisterCallback(): Started");
        }
    	
    	if( m_service == null )
    		return;
    	
        try {
            m_service.unregisterCallback( m_callback.hashCode());
        } catch (Throwable e) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "unregisterCallback(): Failed to unregister calback: RemoteException", e);
            }
        }
    	
        if (LogSettings.ENGINEERING) {
            MktLog.i(TAG, "unregisterCallback(): Finished");
        }
    }
    

    /*
     * 
     */
    private void buildLayout(int orientation) {
        mDisplayOrientation = orientation;

        int content_view_id = (orientation == Configuration.ORIENTATION_LANDSCAPE ?
                R.layout.voip_incoming_call_screen_landscape               //LANDSCAPE
                : R.layout.voip_incoming_call_screen_portrait);            //PORTRAIT or SQUARE
        
        setContentView(content_view_id);
        
        ViewStub stub = ( isOtherCalls() ? 
        		(ViewStub) findViewById( R.id.voip_incoming_call_buttons_interrupt ) :
        			(ViewStub) findViewById( R.id.voip_incoming_call_buttons_no_interrupt ) );
        View stubView = stub.inflate();
        
        if( isOtherCalls()){
        	m_btnAnswer_and_hold = (ImageButton) stubView.findViewById(R.id.btn_call_answer_and_hold);
        	m_btnAnswer_and_hold.setEnabled(isAnwserEnabled());
        	m_btnAnswer_and_hold.setOnClickListener(m_answerAndHoldListener);
        	
        	m_btnAnswer_and_hangup = (ImageButton) stubView.findViewById(R.id.btn_call_answer_and_hangup);
        	m_btnAnswer_and_hangup.setEnabled(isAnwserEnabled());
        	m_btnAnswer_and_hangup.setOnClickListener(m_answerAndHangupListener);
        }
        else{
            m_btnAnswer = (ImageButton) stubView.findViewById(R.id.btn_call_answer);
            m_btnAnswer.setEnabled(isAnwserEnabled());
            m_btnAnswer.setOnClickListener(m_answerListener);
        }
        
        m_btnIgnore = (ImageButton) stubView.findViewById(R.id.btn_call_send_to_voicemail);
        m_btnIgnore.setEnabled(true);
        m_btnIgnore.setOnClickListener(m_rejectListener);
        
        m_fromName = (TextView) findViewById(R.id.voip_call_info_caller_name );
        m_fromName.setVisibility(View.VISIBLE);
        m_fromNumber = (TextView) findViewById(R.id.voip_call_info_caller_number );
        m_fromNumber.setVisibility(View.VISIBLE);
        findViewById(R.id.voip_call_info_call_duration).setVisibility(View.GONE);
        
        final TextView callStatus = ((TextView)findViewById(R.id.voip_call_info_call_status));
        callStatus.setText(R.string.voip_call_incoming);
        callStatus.setVisibility(View.VISIBLE);
       
        mPhotoView = (ContactPhotoView) findViewById(R.id.voip_call_info_caller_photo);                        
        mPhotoCache.getPhoto(((m_callInfo!=null)&&(m_callInfo.getPhotoID()!=null)) ? m_callInfo.getPhotoID() : -1, mPhotoView);
        
        updateControls();
    }

    /*
     * 
     */
    private void updateControlsButtons(){
    	
    	if( isProcessed() ){
    		if( m_btnAnswer != null )
    			m_btnAnswer.setEnabled(false);
    		
    		if( m_btnAnswer_and_hold != null )
    			m_btnAnswer_and_hold.setEnabled(false);
    		
    		if( m_btnAnswer_and_hangup != null )
    			m_btnAnswer_and_hangup.setEnabled(false);
    		
            m_btnIgnore.setEnabled(false);
            
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "updateControls(): answering buttons is disabled");
            }
    	}
    	else{
    		if( m_btnAnswer != null )
    			m_btnAnswer.setEnabled(isAnwserEnabled());
    		
    		if( m_btnAnswer_and_hold != null )
    			m_btnAnswer_and_hold.setEnabled(isAnwserEnabled());
    		
    		if( m_btnAnswer_and_hangup != null )
    			m_btnAnswer_and_hangup.setEnabled(isAnwserEnabled());
    	}
    }    
    /*
     * 
     */
    private void updateControls(){
    	
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "updateControls(): started");
        }
    	
        updateControlsButtons();
    	
        if( m_callInfo != null ){
        	
        	String fromName = null;
        	String fromNumber = null;
        	
        	if( m_callInfo.getSipMsg() != null ){
        		fromName 	= m_callInfo.getFromName();
        		fromNumber 	= m_callInfo.getFromNumber();
        	}

        	if( CallInfo.isUnknown(fromName))
        		fromName = "" ;
        	
        	fromNumber = ( CallInfo.isUnknown(fromNumber) ? getApplicationContext().getString(R.string.calllog_display_name_unknown) : PhoneUtils.getLocalCanonical(fromNumber));
        	
        	m_fromName.setText(fromName);
        	m_fromName.setVisibility(TextUtils.isEmpty(fromName) ? View.GONE : View.VISIBLE);
        	
        	m_fromNumber.setText( fromNumber );
        	m_fromNumber.setVisibility(TextUtils.isEmpty(fromName) ? View.VISIBLE : View.GONE);
        }
        else{
        	m_fromName.setVisibility(View.GONE);
        	m_fromNumber.setVisibility(View.GONE);
        }
    	
    }
    
    private void updatePhotos(){
    	if (LogSettings.ENGINEERING) {
            EngLog.w(TAG, "updatePhotos(): ");
        }
    	
    	mPhotoCache.getPhoto(((m_callInfo!=null)&&(m_callInfo.getPhotoID()!=null)) ? m_callInfo.getPhotoID() : -1, mPhotoView);
    }
    
    /**
     * 
     */
	private OnClickListener m_rejectListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
			
			if( isProcessed() ){
		        if (LogSettings.MARKET) {
		            MktLog.i(TAG, "reject listener(): processed already");
		        }
				return;
			}
			
			setProcessed(true);
			sendMessage( sf_update_controls );
			sendMessage( sf_reject );
		}
	};
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			sendMessage( sf_finish );
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}
	
	/**
	 * Reject current call
	 */
	private void rejectCall(){

        if (LogSettings.MARKET) {
            MktLog.w(TAG, "reject listener(): started");
        }
		
        FlurryTypes.onEvent(FlurryTypes.VOIP_IN_WIFI, FlurryTypes.ACTION, FlurryTypes.REJECT);
		
		if( m_callInfo != null ){
			
			if( m_callInfo.getSipMsg() != null && m_callInfo.getSipMsg().getHdr() != null ){
				final SipMessageOutputSimple msg = 
						(SipMessageOutputSimple)SipMessage.prepareControlCallCommand( 
								SipMessage.sf_CMD_ClientVoiceMail,
								m_callInfo.getClientId(),
								m_callInfo.getSipMsg() );
					
					if( msg != null ) {
	    				final String To = SipMessage.prepareToAddress( m_callInfo.getSipMsg().getHdr().getFrom() );
	    				final int status = sendSipMessage( 0, To, msg );
	    				if( status == PJSIP.sf_PJ_SUCCESS){
	    					if( m_service != null ){
	    						try {
	    							m_service.call_mark_as_ending(m_callInfo.getRcCallId());
	    						} catch (RemoteException e) {
	    							if (LogSettings.MARKET) {
	    								MktLog.e(TAG, "RejectListener call_mark_as_ending Failed", e );
	    							}
	    						}
	    					}
	    				}
					}
			}
			else{
				if( m_service != null ){
					try {
						m_service.call_hangup(m_callInfo.getRcCallId(), RCSIP.SipCode.PJSIP_SC_BUSY_HERE.code(), false);
					} catch (RemoteException e) {
						if (LogSettings.MARKET) {
							MktLog.e(TAG, "RejectListener call_hangup Failed", e );
						}
					}
				}
			}
		}
		
		ringtoneOff();
		
		finish();
	}
	
	/*
	 * 
	 */
	private void ringtoneOff(){
		
		if( m_service != null ){
			try {
				m_service.ringtone_stop();
			} catch (RemoteException e) {
				if (LogSettings.MARKET) {
					MktLog.e(TAG, "RejectListener ringtone_stop Failed", e );
				}
			}
		}
	}

	/*
	 * 
	 */
	private void muteOff(){
		
		if( m_service != null ){
			try {
				m_service.conf_adjust_rx_level(0, 1);
			} catch (RemoteException e) {
				if (LogSettings.MARKET) {
					MktLog.e(TAG, "RejectListener conf_adjust_rx_level Failed", e );
				}
			}
		}
	}
	
    /**
     * Answer button click listener
     */
	private OnClickListener m_answerListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
	        
			if( isProcessed() ){
		        if (LogSettings.MARKET) {
		            MktLog.i(TAG, "answering Listener : Processed already");
		        }
				return;
			}

	        if (LogSettings.MARKET) {
	            MktLog.i(TAG, "answering Listener : started");
	        }
	        
	        FlurryTypes.onEvent(FlurryTypes.VOIP_IN_WIFI, FlurryTypes.ACTION, FlurryTypes.ANSWER);
			
	        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
	            Intent activityStartedIntent = new Intent(RCMConstants.ACTION_INVOIP_ANSWER);
	            activityStartedIntent.putExtra(RCMConstants.EXTRA_INVOIP_ANSWER_TIME, SystemClock.elapsedRealtime());
	            getApplicationContext().sendBroadcast(activityStartedIntent);
	        }
	        
			setProcessed(true);
			sendMessage( sf_update_controls );
			sendMessage( sf_answer );

	        if (LogSettings.MARKET) {
	            MktLog.i(TAG, "answering Listener : started");
	        }
		}
	};

	
	protected void Answer(){
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "Answer(): started");
        }
        
        startActivity(RCMConstants.VOIP_CALL_ACTION_ANSWER);
        callAnswer();
        
        finish();

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "Answer(): finished");
        }
	}
	
	protected void AnswerAndHold(){
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "AnswerAndHold(): started");
        }

        startActivity(RCMConstants.VOIP_CALL_ACTION_ANSWER_AND_HOLD);
        callAnswerAndHold();
        
        finish();

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "AnswerAndHold(): finished");
        }
	}
	
	protected void AnswerAndHangup(){
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "AnswerAndHangup(): started");
        }
        
        startActivity(RCMConstants.VOIP_CALL_ACTION_ANSWER_AND_HANGUP);
        callAnswerAndHangup();
        
        finish();

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "AnswerAndHangup(): finished");
        }
	}
	
    /**
     * Answer and Hold button click listener
     */
	private OnClickListener m_answerAndHoldListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
	        
			if( isProcessed() ){
		        if (LogSettings.MARKET) {
		            MktLog.i(TAG, "answer And Hold Listener : Processed already");
		        }
				return;
			}

	        if (LogSettings.MARKET) {
	            MktLog.w(TAG, "answer And Hold Listener : started");
	        }
			
	        FlurryTypes.onEvent(FlurryTypes.VOIP_IN_WIFI, FlurryTypes.ACTION, FlurryTypes.ANSWER_AND_HOLD);
	        
	        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
                Intent activityStartedIntent = new Intent(RCMConstants.ACTION_INVOIP_ANSWER_AND_HOLD);
                activityStartedIntent.putExtra(RCMConstants.EXTRA_INVOIP_ANSWER_AND_HOLD_TIME, SystemClock.elapsedRealtime());
                getApplicationContext().sendBroadcast(activityStartedIntent);
            }
	        
			setProcessed(true);
			sendMessage( sf_update_controls );
			sendMessage( sf_answer_and_hold );

		}
	};


    /**
     * Answer and Hangup button click listener
     */
	private OnClickListener m_answerAndHangupListener = new OnClickListener(){

		@Override
		public void onClick(View v) {
	        
			if( isProcessed() ){
		        if (LogSettings.MARKET) {
		            MktLog.i(TAG, "answer And Hangup Listener : Processed already");
		        }
				return;
			}

	        if (LogSettings.MARKET) {
	            MktLog.i(TAG, "answer And Hangup Listener : started");
	        }
			
            FlurryTypes.onEvent(FlurryTypes.VOIP_IN_WIFI, FlurryTypes.ACTION, FlurryTypes.ANSWER_AND_HANGUP);

            if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
                Intent activityStartedIntent = new Intent(RCMConstants.ACTION_INVOIP_ANSWER_AND_HOLD);
                activityStartedIntent.putExtra(RCMConstants.EXTRA_INVOIP_ANSWER_AND_HOLD_TIME, SystemClock.elapsedRealtime());
                getApplicationContext().sendBroadcast(activityStartedIntent);
            }

			setProcessed(true);
			sendMessage( sf_update_controls );
			sendMessage( sf_answer_and_hangup );
		}
	};
	
	/*
	 * 
	 */
	private void startActivity(int action){
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "startActivity(): action = " + action);
        }

		Intent intent = new Intent(getApplicationContext(), VoipCallStatusActivity.class);
        intent.putExtra(RCMConstants.EXTRA_VOIP_CALL_TYPE, RCMConstants.VOIP_CALL_TYPE_INCOMING);
        intent.putExtra(RCMConstants.EXTRA_VOIP_CALL_ACTION, action);
		intent.putExtra(RCMConstants.EXTRA_VOIP_INCOMING_CALL_INFO, m_callInfo );
		
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
		        | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		
		getApplication().startActivity(intent);

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "startActivity(): activity VoipCallStatusActivity started");
        }
	}
	
	/*
	 * 
	 */
	private void callAnswer(){
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "callAnswer(): Starting");
        }
		
    	if (m_callInfo != null && m_callInfo.getRcCallId() != PJSIP.sf_INVALID_RC_CALL_ID) {
    	    if( m_service != null ){
				try {
					m_service.call_answer(m_callInfo.getRcCallId(), RCMConstants.VOIP_ANSWER_DELAY);
					
			        if (LogSettings.MARKET) {
			            MktLog.i(TAG, "callAnswer(): sent call_answer()");
			        }
					
				} catch (RemoteException e) {
					if (LogSettings.ENGINEERING) {
						EngLog.e(TAG, "callAnswer() failed", e );
					}
				}
			}
    	}    	
	}
	
    private void callAnswerAndHold(){
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "callAnswerAndHold(): Starting");
        }
        
        if (m_callInfo != null && m_callInfo.getRcCallId() != PJSIP.sf_INVALID_RC_CALL_ID ) {
            if( m_service != null ){
                try {
                    m_service.call_answer_and_hold(m_callInfo.getRcCallId(), RCMConstants.VOIP_ANSWER_DELAY);
                    
                    if (LogSettings.MARKET) {
                        MktLog.i(TAG, "callAnswerAndHold(): sent call_answer_and_hold()");
                    }
                    
                } catch (RemoteException e) {
                    if (LogSettings.ENGINEERING) {
                        EngLog.e(TAG, "callAnswerAndHold() failed", e );
                    }
                }
            }
        }       
    }
    
    private void callAnswerAndHangup(){
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "callAnswerAndHangup(): Starting");
        }
        
        if (m_callInfo != null && m_callInfo.getRcCallId() != PJSIP.sf_INVALID_RC_CALL_ID ) {
            if( m_service != null ){
                try {
                    m_service.call_answer_and_hangup(m_callInfo.getRcCallId(), RCMConstants.VOIP_ANSWER_DELAY);
                    
                    if (LogSettings.MARKET) {
                        MktLog.i(TAG, "callAnswerAndHangup(): sent call_answer_and_hangup()");
                    }
                    
                } catch (RemoteException e) {
                    if (LogSettings.ENGINEERING) {
                        EngLog.e(TAG, "callAnswerAndHangup() failed", e );
                    }
                }
            }
        }       
    }
    
    /*
     * Send instant SIP message
     */
    private int sendSipMessage( int accountId, String To, XmlSerializableObjectAbstract msg ){
    	int status = PJSIP.sf_PJ_EUNKNOWN;
    	
    	if( accountId == PJSIP.sf_INVALID_ACCOUNT_ID )
    		return PJSIP.sf_PJ_EINVAL;
    	
		final String sContent = msg.toString();
		if( sContent != null ){
	        if (LogSettings.MARKET) {
		        MktLog.i( TAG, "SIP message: " + sContent );
	        }
			
			if( m_service != null ){
				try {
					status =  m_service.message_send( accountId, To, sContent );
					
				} catch (RemoteException e) {
		            if (LogSettings.MARKET) {
		            	MktLog.e( TAG, "message_send failed: ",e);
		            }
				}
			}
		}
    
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "sendSipMessage finished: " + status );
        }
		return status;
    }
	
    private final IServicePJSIPCallback m_callback = new IServicePJSIPCallback.Stub(){

		@Override
		public void account_changed_register_state(int accountId,
				boolean registered, int code) throws RemoteException {
		}

		@Override
		public void call_media_state(long callId, int state)
				throws RemoteException {
		}

		@Override
		public void call_media_stop(long callId) throws RemoteException {
		}

		@Override
		public void call_state(long callId, int state, int last_status, int ending_reason, int media_sate) throws RemoteException {
			
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "call_state  call ID Received:" + callId +
                		" current:" + ( m_callInfo != null ? m_callInfo.getRcCallId() : "Unk") + 
                		" state:" + state + "/" + RCSIP.getCallStatus(state) + 
                		" last status:" + last_status + "/" + RCSIP.getSipResponseForLog(last_status));
            }
			
			/*
			 * Close Activity if this call was disconnected before choice selection
			 */
			if( m_callInfo != null && m_callInfo.getRcCallId() == callId ){
				if( state == RCSIP.PJSUA_CALL_STATE_DISCONNECTED ){
					sendMessage( sf_finish );
				}
			}
			
		}

		@Override
		public void http_registration_state(int code) throws RemoteException {
		}

		@Override
		public void on_audio_route_changed(AudioState audioState) throws RemoteException {
		    if (LogSettings.MARKET) {
		        QaLog.i(TAG, "Audio Changed :  " + audioState);
		    }
		}

		@Override
		public void on_call_transfer_status(long callId, int status,
				String statusProgressText) throws RemoteException {
		}

		@Override
		public void on_incoming_call(CallInfo callInfo) throws RemoteException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void on_message(long callId, SipMessageInput sipMsg)
				throws RemoteException {

            if (LogSettings.ENGINEERING) {
                MktLog.w(TAG, "Message Call ID[" + callId + "] Cmd [" + ( sipMsg == null ? -1 : sipMsg.getCommand()) + "]" );
            }
			
			/*
			 * If RC message AlreadyProcessed 
			 * (it means that another agent already processed this call) or
			 * SessionClose ( session was closed by server request) were received 
			 * then close activity.
			 */
			if( m_callInfo != null && ( m_callInfo.getRcCallId() == callId || callId == PJSIP.sf_INVALID_RC_CALL_ID ) && sipMsg != null ){

				if( SipMessage.sf_CMD_AlreadyProcessed == sipMsg.getCommand() ||
						SipMessage.sf_CMD_SessionClose == sipMsg.getCommand() ){
					sendMessage( sf_finish );
				}
			}			
		}

		@Override
		public void on_sip_stack_started(boolean result) throws RemoteException {
		}

        @Override
        public void on_test_started(int id) throws RemoteException {
        }

        @Override
        public void on_test_completed(int id, int value) throws RemoteException {
        }

		@Override
		public void on_call_info_changed(long callId) throws RemoteException {
			sendMessage(sf_sync_call_state);			
		}
		
		@Override
	    public void notifyCallsStateChanged(final RCCallInfo[] calls) throws RemoteException {
		}
    };
    
    private void syncCallInfo() {
    	if(( m_service != null )&&(m_callInfo != null)){
			try {
				int status =  m_service.call_get_info(m_callInfo.getRcCallId(), m_callInfo);
				if (LogSettings.MARKET) {
		             MktLog.i(TAG, "syncCallInfo, status : " + status);
		         }
				
				if (status == PJSIP.sf_PJ_SUCCESS) {
					sendMessage(sf_update_controls);
					sendMessage(sf_update_photo);
				}
			} catch (RemoteException e) {
	            if (LogSettings.MARKET) {
	            	MktLog.e( TAG, "message_send failed: ",e);
	            }
			}
		}
    }
    
    /**
     * 
     */
    private void sendMessage( int what ){
    	Message msg = Message.obtain(m_handler, what );
    	msg.sendToTarget();
    }
    
    /**
     * 
     */
    private final class UIMessageHandler extends Handler {
        public UIMessageHandler(){
            super();
        }
        
        public void handleMessage( Message msg ){
            
        	if (isFinishing()) {
                return;
            }
        	
        	if( msg.what == sf_finish ){
        		
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "Activity is finished" );
                }
        		
        		ringtoneOff();
                
        		finish();
        	}
        	else if( msg.what == sf_ringtone_off) {
        		ringtoneOff();
        	}
        	else if( msg.what == sf_update_controls ){
        		updateControls();
        	}
        	else if( msg.what == sf_update_controls_button ){
        		updateControlsButtons();
        	}
        	else if ( msg.what == sf_update_photo ){
        		updatePhotos();
        	}
        	else if( msg.what == sf_back_pressed || msg.what == sf_reject){
        		rejectCall();
        	}
        	else if( msg.what == sf_answer ){
        		Answer();
        	}
        	else if( msg.what == sf_answer_and_hold ){
        		AnswerAndHold();
        	}
        	else if( msg.what == sf_answer_and_hangup ){
        		AnswerAndHangup();
        	} 
        	else if( msg.what == sf_sync_call_state ){
        		syncCallInfo();
        	}
        }
    };
    
    
    /*
     * 
     */
	private class PhoneStateChangedReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

	        if( m_telefMng != null ){
	        	/*
	        	 * If we active native calls do not allow to accept SIP call
	        	 */
	        	setAnwserEnabled( TelephonyManager.CALL_STATE_OFFHOOK != m_telefMng.getCallState() );
	        	sendMessage( sf_update_controls_button );
	        }
			
		}
	}
	
	private static class ScreenLockWrapper {
        private static final String KEY_LOCK = "RingCentral Incoming VoIP Keys lock";
    	private static final String POWER_LOCK = "RingCentral Incoming VoIP Power lock";
    	
    	private KeyguardManager.KeyguardLock keyLock;
    	private PowerManager.WakeLock wakeLock;
    	    	
    	private static Integer windowFlags = null;
    	
    	private static boolean inited = false;
    	
		public void unlock(Window window) {
			if(LogSettings.ENGINEERING) {
				EngLog.d(TAG, "ScreenLockWrapper.unlock()");
			}
			try {
				if(wakeLock == null) {
					final PowerManager powerMgr = (PowerManager) RingCentralApp.getContextRC().getSystemService(POWER_SERVICE);
					wakeLock = powerMgr.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP
							| PowerManager.FULL_WAKE_LOCK, POWER_LOCK);
					if((wakeLock != null)&&(!wakeLock.isHeld())){
						wakeLock.acquire();
					}
				}
	
				if(keyLock == null) {
					final KeyguardManager keyguardMgr = (KeyguardManager) RingCentralApp.getContextRC().getSystemService(KEYGUARD_SERVICE);
					keyLock = keyguardMgr.newKeyguardLock(KEY_LOCK);
					keyLock.disableKeyguard();
				}
			} catch (Exception e) {
				if(LogSettings.MARKET) {
					MktLog.e(TAG, "ScreenLockWrapper.unlock()", e);
				}
			}
			if(!inited){
				windowFlags = getFlags();
				inited = true;
			}
			if(windowFlags != null){
				window.addFlags(windowFlags.intValue());
			}
		}
		
		public void relock() {
			if(LogSettings.ENGINEERING) {
				EngLog.d(TAG, "ScreenLockWrapper.relock()");
			}
			try {		        
		        if((wakeLock != null) && wakeLock.isHeld()) {
					wakeLock.release();
					wakeLock = null;
				}
			} catch (Exception e) {
				if(LogSettings.MARKET) {
					MktLog.e(TAG, "ScreenLockWrapper.relock()", e);
				}
			}
		}
		
		private Integer getFlags() {
			Integer windowFlags = new Integer(0);
			try {
				Field showWhenLockedField = WindowManager.LayoutParams.class.getDeclaredField("FLAG_SHOW_WHEN_LOCKED");
				if (showWhenLockedField != null) {
					int showWhenLockedValue = (Integer) showWhenLockedField.get(null);
					windowFlags = showWhenLockedValue;
				}
				
				Field dismissKeyguardField = WindowManager.LayoutParams.class.getDeclaredField("FLAG_DISMISS_KEYGUARD");
				if (dismissKeyguardField != null) {
					int dismissKeyguardValue = (Integer) dismissKeyguardField.get(null);
					windowFlags |= dismissKeyguardValue;
				}
				Field turnScreenOnField = WindowManager.LayoutParams.class.getDeclaredField("FLAG_TURN_SCREEN_ON");
				if (turnScreenOnField != null) {
					int turnScreenOnValue = (Integer) turnScreenOnField.get(null);
					windowFlags |= turnScreenOnValue;
				}
			} catch (Exception e) {
				if(LogSettings.MARKET) {
					MktLog.d(TAG, "setFlags exceptions", e);
				}
				windowFlags = null;
			}
			return windowFlags;
		}
    }
	
			
	
    // ====================== Workaround Section (BN) ==========================
    // == Reduces memory leaks. The section can be just added to any Activity ==
    // == Call leakCleanUpRootView onDestroy and before setting new root view ==
    
    @Override
    public void setContentView(int layoutResID) {
        ViewGroup rootView = (ViewGroup) 
            LayoutInflater.from(this).inflate(layoutResID, null);
        setContentView(rootView);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        leakCleanUpRootView();
        mLeakContentView = (ViewGroup) view;
    }

    @Override
    public void setContentView(View view, LayoutParams params) {
        super.setContentView(view, params);
        leakCleanUpRootView();
        mLeakContentView = (ViewGroup) view;
    }
    
    /**
     * Cleanup root view to reduce memory leaks.
     */
    private void leakCleanUpRootView() {
        if (mLeakContentView != null) {
            ViewGroup v = mLeakContentView;
            mLeakContentView = null;
            mStackRecursions = 0;
            leakCleanUpChildsDrawables(v);
            System.gc();
        }
    }
    
    /**
     * Clean-up Drawables in the view including child.
     * 
     * @param v
     */
    private void leakCleanUpChildsDrawables(View v) {
        if (v != null) {
            try {
                ViewGroup group = (ViewGroup) v;
                int childs = group.getChildCount();
                for (int i = 0; i < childs; i++) {
                    if (LEAKS_CLEAN_UP_STACK_RECURSIONS_LIMIT > mStackRecursions) {
                        mStackRecursions++;
                        leakCleanUpChildsDrawables(group.getChildAt(i));
                        mStackRecursions--;
                    } else {
                        break;
                    }
                }
            } catch (java.lang.Throwable th) {
            }
            leakCleanUpDrawables(v);
        }
    }

    /**
     * Keeps recursions number for memory clean-ups.
     */
    private int mStackRecursions;
    
    /**
     * Limit of leaks clean-ups stack recursions.
     */
    private static final int LEAKS_CLEAN_UP_STACK_RECURSIONS_LIMIT = 256;
    
    /**
     * Cleans drawables of in the view.
     * 
     * @param v the view to clean-up
     */
    private void leakCleanUpDrawables(View v) {
        if (v == null) {
            return;
        }
            
        try {
            if (v.getBackground() != null) {
                v.getBackground().setCallback(null);
            }
        } catch (java.lang.Throwable th) {
        }

        try {
            v.setBackgroundDrawable(null);
        } catch (java.lang.Throwable th) {
        }

        try {
            ImageView imageView = (ImageView) v;
            imageView.setImageDrawable(null);
            imageView.setBackgroundDrawable(null);
        } catch (java.lang.Throwable th) {
        }

    }
    
    /**
     * Keeps current root view.
     */
    private ViewGroup mLeakContentView = null;
    
 // ====================== Workaround Section END (BN) ==========================

}
