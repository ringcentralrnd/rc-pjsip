/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.voip;

import android.content.Context;

import com.rcbase.android.sip.service.IUserConfigurationInfo;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.MailboxCurrentTable;

/**
 * @author Denis Kudja
 *
 */
public class RCUserConfigurationInfo implements IUserConfigurationInfo {

	private Context m_context = null;
	
	public RCUserConfigurationInfo( Context context ){
		m_context = context;
	}
	
	/* (non-Javadoc)
	 * @see com.rcbase.android.sip.service.IUserConfigurationInfo#getExtension()
	 */
	@Override
	public String getExtension() {
		return RCMProviderHelper.getAccountNumber(m_context).trim();
	}

	/* (non-Javadoc)
	 * @see com.rcbase.android.sip.service.IUserConfigurationInfo#getInstanceId()
	 */
	@Override
	public String getInstanceId() {
		return RCMProviderHelper.getHttpRegInstanceId(m_context);
	}

	/* (non-Javadoc)
	 * @see com.rcbase.android.sip.service.IUserConfigurationInfo#getPassword()
	 */
	@Override
	public String getPassword() {
		return RCMProviderHelper.getLoginPassword(m_context).trim();
	}

	/* (non-Javadoc)
	 * @see com.rcbase.android.sip.service.IUserConfigurationInfo#getPin()
	 */
	@Override
	public String getPin() {
		return RCMProviderHelper.getAccountPin(m_context).trim();
	}

	/* (non-Javadoc)
	 * @see com.rcbase.android.sip.service.IUserConfigurationInfo#setInstanceId(java.lang.String)
	 */
	@Override
	public void setInstanceId(String instanceId) {
		RCMProviderHelper.saveHttpRegInstanceId(m_context, instanceId );
	}

	/*
	 * (non-Javadoc)
	 * @see com.rcbase.android.sip.service.IUserConfigurationInfo#getCallerID()
	 */
	@Override
	public String getCallerID() {
		return RCMProviderHelper.getCallerID(m_context);
	}

	/*
	 * (non-Javadoc)
	 * @see com.rcbase.android.sip.service.IUserConfigurationInfo#getCurrentMailboxId()
	 */
	@Override
	public long getCurrentMailboxId() {
		return RCMProviderHelper.getCurrentMailboxId(m_context);
	}

	@Override
	public boolean isValidMailboxId() {
		return ( RCMProviderHelper.getCurrentMailboxId(m_context) != MailboxCurrentTable.MAILBOX_ID_NONE );
	}

}
