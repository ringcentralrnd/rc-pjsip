/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.voip;

import com.rcbase.android.sip.audio.IExternalResources;
import com.ringcentral.android.R;

/**
 * @author Denis Kudja
 *
 */
public class SipResources implements IExternalResources {

	/* (non-Javadoc)
	 * @see com.rcbase.android.sip.audio.IExternalResources#getForegroundIcon()
	 */
	@Override
	public int getServiceForegroundIcon() {
		return R.drawable.ic_call_in_progress;
	}

	/* (non-Javadoc)
	 * @see com.rcbase.android.sip.audio.IExternalResources#getForegroundText()
	 */
	@Override
	public int getServiceForegroundText() {
		return R.string.app_name;
	}

	/* (non-Javadoc)
	 * @see com.rcbase.android.sip.audio.IExternalResources#getForegroundTitle()
	 */
	@Override
	public int getServiceForegroundTitle() {
		return R.string.settings_item_version_title;
	}

	/* (non-Javadoc)
	 * @see com.rcbase.android.sip.audio.IExternalResources#getForegrountDescription()
	 */
	@Override
	public int getServiceForegroundDescription() {
		return R.string.app_name;
	}

	@Override
	public int getIncomingCallIcon() {
		return R.drawable.ic_call_log_call_in_white;
	}

	@Override
	public int getIncomingCallText() {
		return R.string.app_name;
	}

}
