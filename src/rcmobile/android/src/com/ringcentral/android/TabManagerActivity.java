package com.ringcentral.android;

import android.app.Activity;
import android.app.ActivityGroup;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.utils.ui.widget.RMenuWindow;
import com.ringcentral.android.utils.ui.widget.ActivityManager;
import com.ringcentral.android.utils.ui.widget.TabWidget;

public class TabManagerActivity extends ActivityGroup {
    private static final String TAB_TAG = "navigation_bar";
    private ActivityManager mActivityManager;
    private String mTab = null;
    private int mTabIndex = -1;
    
    public TabManagerActivity() {
    }
    
    public ActivityManager getActivityManager() {
        ensureTabHost();
        return mActivityManager;
    }
    

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        ensureTabHost();
        String cur = state.getString(TAB_TAG);
        if (cur != null) {
            mActivityManager.setCurrentTabByTag(cur);
        }
        if (mActivityManager.getCurrentTab() < 0) {
            if (mTab != null) {
                mActivityManager.setCurrentTabByTag(mTab);
            } else if (mTabIndex >= 0) {
                mActivityManager.setCurrentScreen(mTabIndex);
            }
        }
    }
    
    
    
    private void ensureTabHost() {
        if (mActivityManager == null) {
            this.setContentView(R.layout.tab_content);
        }
    }

    
    @Override
    protected void onPostCreate(Bundle icicle) {
        super.onPostCreate(icicle);
        ensureTabHost();        
        if (mActivityManager.getCurrentTab() == -1) {
            mActivityManager.setCurrentScreen(0);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        String tag = mActivityManager.getCurrentTabTag();
        if (tag != null) {
            outState.putString(TAB_TAG, tag);
        }
    }

    @Override
    public void onContentChanged() {
        super.onContentChanged();
        mActivityManager = (ActivityManager) findViewById(R.id.tab_manager);
        mActivityManager.setup(getLocalActivityManager());
    }

    @Override
    protected void onChildTitleChanged(Activity childActivity, CharSequence title) {
        if (getLocalActivityManager().getCurrentActivity() == childActivity) {
            /*View tView = mTabManager.getTabView();
            if (tView != null && tView instanceof TextView) {
                ((TextView) tView).setText(title);
            }*/
        }
    }
    
    protected void showNavigation() {
    	mActivityManager.showNavigationMenu();
    }
}