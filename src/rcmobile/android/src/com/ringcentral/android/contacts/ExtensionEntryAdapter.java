/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import java.util.ArrayList;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ringcentral.android.provider.RCMDataStore.ExtensionsTable;

public abstract class ExtensionEntryAdapter<E extends ExtensionEntryAdapter.Entry> extends BaseAdapter {

    public static final String[] EXTENSION_PROJECTION = new String[] { ExtensionsTable._ID, // 0
            ExtensionsTable.MAILBOX_ID, // 1
            ExtensionsTable.RCM_DISPLAY_NAME, // 2
            ExtensionsTable.JEDI_PIN, // 3
            ExtensionsTable.RCM_STARRED, // 4
            ExtensionsTable.JEDI_EMAIL, // 5
            ExtensionsTable.JEDI_ADDRESS_LINE_1, // 6
            ExtensionsTable.JEDI_ADDRESS_LINE_2, // 7
            ExtensionsTable.JEDI_CITY, // 8
            ExtensionsTable.JEDI_COUNTRY, // 9
            ExtensionsTable.JEDI_STATE, // 10
            ExtensionsTable.JEDI_ZIPCODE // 11
    };

    public static final int EXTENSION_ID_COLUMN = 0;
    public static final int EXTENSION_MAILBOX_ID_COLUMN = 1;
    public static final int EXTENSION_DISPLAY_NAME_COLUMN = 2;
    public static final int EXTENSION_PIN_COLUMN = 3;
    public static final int EXTENSION_STARRED_COLUMN = 4;
    public static final int EXTENSION_EMAIL_COLUMN = 5;
    public static final int EXTENSION_ADDRESS_LINE_1_COLUMN = 6;
    public static final int EXTENSION_ADDRESS_LINE_2_COLUMN = 7;
    public static final int EXTENSION_SITY_COLUMN = 8;
    public static final int EXTENSION_COUNTRY_COLUMN = 9;
    public static final int EXTENSION_STATE_COLUMN = 10;
    public static final int EXTENSION_ZIPCODE_COLUMN = 11;

    ArrayList<ArrayList<E>> mSections;
    LayoutInflater mInflater;
    Context mContext;
    boolean mSeparators;

    /**
     * Base class for adapter entries.
     */
    public static class Entry {
        public static final int KIND_PHONE = 1;
        public static final int KIND_EMAIL = 2;
        public static final int KIND_ADDRESS = 3;
        public static final int KIND_VIEW_CONTACT = 7;
        public static final int KIND_SEPARATOR = 8;
        public static final int KIND_CALLBACK_PHONE = 10;
        public static final int KIND_CREATE_NEW_CONTACT = 11;
        public static final int KIND_ADD_TO_EXISTING_CONTACT = 12;
        public static final int KIND_ADD_TO_FAVORITES = 13;
        public static final int KIND_REMOVE_FROM_FAVORITES = 14;
        public String label;
        public String type;
        public String data;
        public Uri uri;
        public long id = 0;
        public int maxLines = 1;
        public int kind;
    }

    ExtensionEntryAdapter(Context context, ArrayList<ArrayList<E>> sections, boolean separators) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSections = sections;
        mSeparators = separators;
    }

    public final void setSections(ArrayList<ArrayList<E>> sections, boolean separators) {
        mSections = sections;
        mSeparators = separators;
        notifyDataSetChanged();
    }

    public final int getCount() {
        return countEntries(mSections, mSeparators);
    }

    @Override
    public final boolean areAllItemsEnabled() {
        return mSeparators == false;
    }

    public final Object getItem(int position) {
        return getEntry(mSections, position, mSeparators);
    }

    public final static <T extends Entry> T getEntry(ArrayList<ArrayList<T>> sections, int position, boolean separators) {
        int numSections = sections.size();
        for (int i = 0; i < numSections; i++) {
            ArrayList<T> section = sections.get(i);
            int sectionSize = section.size();
            if (separators && sectionSize == 1) {
                continue;
            }
            if (position < section.size()) {
                return section.get(position);
            }
            position -= section.size();
        }
        return null;
    }

    public static <T extends Entry> int countEntries(ArrayList<ArrayList<T>> sections, boolean separators) {
        int count = 0;
        int numSections = sections.size();
        for (int i = 0; i < numSections; i++) {
            ArrayList<T> section = sections.get(i);
            int sectionSize = section.size();
            if (separators && sectionSize == 1) {
                continue;
            }
            count += sections.get(i).size();
        }
        return count;
    }

    public final long getItemId(int position) {
        Entry entry = getEntry(mSections, position, mSeparators);
        if (entry != null) {
            return entry.id;
        } else {
            return -1;
        }
    }

    protected abstract View newView(int position, ViewGroup parent);

    protected abstract void bindView(View view, E entry);
}
