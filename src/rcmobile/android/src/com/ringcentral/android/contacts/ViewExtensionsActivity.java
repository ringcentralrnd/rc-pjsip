/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.contacts.ExtensionEntryAdapter.Entry;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.provider.RCMDataStore.ExtensionsTable;
import com.ringcentral.android.ringout.RingOutAgent;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.EmailSender;

public class ViewExtensionsActivity extends ListActivity implements View.OnCreateContextMenuListener,
        View.OnClickListener, AdapterView.OnItemClickListener, DialogInterface.OnClickListener {

    private static final String TAG = "[RC] ViewExtensionsActivity";
    private static final boolean SHOW_SEPARATORS = false;
    private Uri mUri;
    private ContentResolver mResolver;
    private ViewAdapter mAdapter;
    private ArrayList<ViewEntry> mPhoneEntries = new ArrayList<ViewEntry>();
    private ArrayList<ViewEntry> mEmailEntries = new ArrayList<ViewEntry>();
    private ArrayList<ViewEntry> mPostalEntries = new ArrayList<ViewEntry>();
    private ArrayList<ArrayList<ViewEntry>> mSections = new ArrayList<ArrayList<ViewEntry>>();
    private Cursor mCursor;
    private boolean mObserverRegistered;
    private RingOutAgent mRingOutAgent;
    private TextView mNameView;
    private CheckBox mStarView;

    private ContentObserver mObserver = new ContentObserver(new Handler()) {
        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }

        @Override
        public void onChange(boolean selfChange) {
            if (mCursor != null && !mCursor.isClosed()) {
                dataChanged();
            }
        }
    };

    public void onClick(DialogInterface dialog, int which) {
        if (mCursor != null) {
            if (mObserverRegistered) {
                mCursor.unregisterContentObserver(mObserver);
                mObserverRegistered = false;
            }
            mCursor.close();
            mCursor = null;
        }
        // getContentResolver().delete(mUri, null, null);
        finish();
    }

    public void onClick(View view) {
        if (!mObserverRegistered) {
            return;
        }
        switch (view.getId()) {
        case R.id.star: {
            try {
                if (LogSettings.MARKET) {
                    QaLog.d(TAG, "User: Toggle favorite");
                }
                int oldStarredState = mCursor.getInt(ExtensionEntryAdapter.EXTENSION_STARRED_COLUMN);
                ContentValues values = new ContentValues(1);
                values.put(ExtensionsTable.RCM_STARRED, oldStarredState == 1 ? 0 : 1);
                getContentResolver().update(mUri, values, null, null);
            } catch (java.lang.Throwable error) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "onClick(view) set favorite failed: ", error);
                }
            }
            break;
        }
        }
    }

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.view_contact);
        getListView().setOnCreateContextMenuListener(this);
        mNameView = (TextView) findViewById(R.id.name);
        mStarView = (CheckBox) findViewById(R.id.star);
        mStarView.setOnClickListener(this);
        mUri = getIntent().getData();
        mResolver = getContentResolver();
        mSections.add(mPhoneEntries);
        mSections.add(mEmailEntries);
        mSections.add(mPostalEntries);
        mCursor = mResolver.query(mUri, ExtensionEntryAdapter.EXTENSION_PROJECTION, null, null, null);
        mRingOutAgent = new RingOutAgent(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mObserverRegistered = true;
        mCursor.registerContentObserver(mObserver);
        dataChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mCursor != null) {
            if (mObserverRegistered) {
                mObserverRegistered = false;
                mCursor.unregisterContentObserver(mObserver);
            }
            mCursor.deactivate();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (mCursor != null) {
                if (mObserverRegistered) {
                    mCursor.unregisterContentObserver(mObserver);
                    mObserverRegistered = false;
                }
                mCursor.close();
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.QA) {
                QaLog.e(TAG, "onSetroy() error 1: ", error);
            }
        }

        mUri = null;
        mResolver = null;
        mAdapter = null;
        mPhoneEntries = null;
        mEmailEntries = null;
        mPostalEntries = null;
        mSections = null;
        mCursor = null;
        mRingOutAgent = null;
        mNameView = null;
        mStarView = null;
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	FlurryTypes.onStartSession(this);
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	FlurryTypes.onEndSession(this);
    }
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED && hasFocus){
            PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.OPEN_CONTACT_DETAILS);
        }
    }

    private void dataChanged() {
        try {
            mCursor.requery();
            if (mCursor.moveToFirst()) {
                String name = mCursor.getString(ExtensionEntryAdapter.EXTENSION_DISPLAY_NAME_COLUMN);
                if (TextUtils.isEmpty(name)) {
                    mNameView.setText(getText(android.R.string.unknownName));
                } else {
                    mNameView.setText(name);
                }

                mStarView
                        .setChecked(mCursor.getInt(ExtensionEntryAdapter.EXTENSION_STARRED_COLUMN) == 1 ? true : false);
                buildEntries(mCursor);
                if (mAdapter == null) {
                    mAdapter = new ViewAdapter(this, mSections);
                    setListAdapter(mAdapter);
                } else {
                    mAdapter.setSections(mSections, SHOW_SEPARATORS);
                }
            } else {
                if (LogSettings.ENGINEERING) {
                    EngLog.w(TAG, "invalid contact uri: " + mUri);
                }
                finish();
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.QA) {
                QaLog.e(TAG, "dataChanged() error: ", error);
            }
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        ViewEntry entry = ViewAdapter.getEntry(mSections, position, SHOW_SEPARATORS);
        if (entry != null) {

            if (entry.kind == Entry.KIND_PHONE) {
            	FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, (mCursor.getInt(ExtensionEntryAdapter.EXTENSION_STARRED_COLUMN) == 1)?
            			FlurryTypes.SCR_FAVORITES_EXTENSION_DETAILS:
            			FlurryTypes.SCR_CONTACTS_EXTENSION_DETAILS);
                mRingOutAgent.call(entry.data, TextUtils.isEmpty(mNameView.getText())?null:(String)mNameView.getText());
                return;
            } else if (entry.kind == Entry.KIND_EMAIL) {
                EmailSender emailSender = new EmailSender(this);
                String[] to = { entry.data };
                emailSender.sendEmail(to, "", "", null);
                return;
            }

            Intent intent = entry.intent;
            if (intent != null) {
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    if (LogSettings.ENGINEERING) {
                        EngLog.e(TAG, "onListItemClick()", e);
                    } else if (LogSettings.MARKET) {
                        QaLog.e(TAG, "onListItemClick(): " + e.getMessage());
                    }
                }
            }
        }
    }

    private void buildSeparators() {
        ViewEntry separator;

        separator = new ViewEntry();
        separator.kind = ViewEntry.KIND_SEPARATOR;
        separator.data = getString(R.string.listSeparatorCallNumber);
        mPhoneEntries.add(separator);

        separator = new ViewEntry();
        separator.kind = ViewEntry.KIND_SEPARATOR;
        separator.data = getString(R.string.listSeparatorSendEmail);
        mEmailEntries.add(separator);

        separator = new ViewEntry();
        separator.kind = ViewEntry.KIND_SEPARATOR;
        separator.data = getString(R.string.listSeparatorAddress);
        mPostalEntries.add(separator);
    }

    private final void buildEntries(Cursor extensionCursor) {
        final int numSections = mSections.size();
        for (int i = 0; i < numSections; i++) {
            mSections.get(i).clear();
        }

        if (SHOW_SEPARATORS) {
            buildSeparators();
        }

        if (extensionCursor != null) {
            if (extensionCursor.moveToFirst()) {

                final long id = extensionCursor.getLong(ExtensionEntryAdapter.EXTENSION_ID_COLUMN);
                long mailbox_id = RCMProviderHelper.getCurrentMailboxId(this);
                Uri uri = UriHelper.getUri(RCMProvider.EXTENSIONS, mailbox_id, id);

                ViewEntry entry = new ViewEntry();
                entry.label = getString(R.string.ExtView_itemExtension);
                entry.data = extensionCursor.getString(ExtensionEntryAdapter.EXTENSION_PIN_COLUMN);
                entry.id = id;
                entry.uri = uri;
                entry.kind = ViewEntry.KIND_PHONE;
                entry.actionIcon = android.R.drawable.sym_action_call;
                mPhoneEntries.add(entry);

                String data = extensionCursor.getString(ExtensionEntryAdapter.EXTENSION_EMAIL_COLUMN);
                if (data != null && data.trim().length() > 0) {
                    // === Add a Email entry
                    entry = new ViewEntry();
                    entry.id = id;
                    entry.uri = uri;
                    entry.kind = ViewEntry.KIND_EMAIL;
                    entry.label = getString(R.string.ExtView_itemEmail);
                    entry.data = data;
                    entry.intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", entry.data, null));
                    entry.actionIcon = android.R.drawable.sym_action_email;
                    mEmailEntries.add(entry);
                }

                ArrayList<String> content = new ArrayList<String>();
                concatPost(extensionCursor.getString(ExtensionEntryAdapter.EXTENSION_ADDRESS_LINE_1_COLUMN), content);
                concatPost(extensionCursor.getString(ExtensionEntryAdapter.EXTENSION_ADDRESS_LINE_2_COLUMN), content);
                concatPost(extensionCursor.getString(ExtensionEntryAdapter.EXTENSION_SITY_COLUMN), content);
                concatPost(extensionCursor.getString(ExtensionEntryAdapter.EXTENSION_STATE_COLUMN), content);
                concatPost(extensionCursor.getString(ExtensionEntryAdapter.EXTENSION_ZIPCODE_COLUMN), content);
                concatPost(extensionCursor.getString(ExtensionEntryAdapter.EXTENSION_COUNTRY_COLUMN), content);

                int size = content.size();
                if (size > 0) {
                    StringBuffer sb = new StringBuffer();
                    if (size == 1) {
                        sb.append(content.get(0));
                    } else {
                        for (int i = 0; i < (size - 1); i++) {
                            sb.append(content.get(i));
                            sb.append(", ");
                        }
                        sb.append(content.get((size - 1)));
                    }

                    // === Add a Address entry
                    entry = new ViewEntry();
                    entry.label = getString(R.string.ExtView_itemAddress);
                    entry.data = sb.toString();
                    entry.maxLines = 4;
                    entry.kind = ViewEntry.KIND_ADDRESS;
                    // entry.intent = new Intent(Intent.ACTION_VIEW, uri);
                    entry.actionIcon = android.R.drawable.sym_contact_card;
                    mPostalEntries.add(entry);
                }
            }
        }
    }

    private static final void concatPost(String data, ArrayList<String> content) {
        if (data != null) {
            data = data.trim();
            if (data.length() > 0) {
                content.add(data);
            }
        }
    }

    /**
     * A basic structure with the data for a contact entry in the list.
     */
    private final static class ViewEntry extends ExtensionEntryAdapter.Entry {
        // public Context context = null;
        public String resPackageName = null;
        // public int primaryIcon = -1;
        public int actionIcon = -1;
        public boolean isPrimary = false;
        public int secondaryActionIcon = -1;
        public Intent intent;
        // public Intent auxIntent;
        public Intent secondaryIntent = null;
        public int maxLabelLines = 1;
        // public ArrayList<Long> ids = new ArrayList<Long>();
        // public int collapseCount = 0;

        public int presence = -1;
        public int presenceIcon = -1;

        public CharSequence footerLine = null;

        private ViewEntry() {
        }

    }

    // ============== ViewAdapter =============================
    private static final class ViewAdapter extends ExtensionEntryAdapter<ViewEntry> {
        /** Cache of the children views of a row */
        static class ViewCache {
            public TextView label;
            public TextView data;
            public TextView footer;
            public ImageView actionIcon;
            public ImageView presenceIcon;
            public ImageView primaryIcon;
            public ImageView secondaryActionButton;
            public View secondaryActionDivider;
            // ViewEntry entry;
        }

        ViewAdapter(Context context, ArrayList<ArrayList<ViewEntry>> sections) {
            super(context, sections, SHOW_SEPARATORS);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewEntry entry = getEntry(mSections, position, SHOW_SEPARATORS);
            View v = null;

            if (entry.kind == ViewEntry.KIND_SEPARATOR) {
                TextView separator = (TextView) mInflater.inflate(R.layout.list_separator, parent, false);
                separator.setText(entry.data);
                return separator;
            }

            ViewCache views = null;

            if (convertView != null) {
                v = convertView;
                views = (ViewCache) v.getTag();
            }

            if (views == null) {
                v = mInflater.inflate(R.layout.list_item_text_icons, parent, false);
                views = new ViewCache();
                views.label = (TextView) v.findViewById(android.R.id.text1);
                views.data = (TextView) v.findViewById(android.R.id.text2);
                views.footer = (TextView) v.findViewById(R.id.footer);
                views.actionIcon = (ImageView) v.findViewById(R.id.action_icon);
                views.primaryIcon = (ImageView) v.findViewById(R.id.primary_icon);
                views.presenceIcon = (ImageView) v.findViewById(R.id.presence_icon);
                views.secondaryActionButton = (ImageView) v.findViewById(R.id.secondary_action_button);
                // views.secondaryActionButton.setOnClickListener(this);
                views.secondaryActionDivider = v.findViewById(R.id.divider);
                v.setTag(views);
            }

            // views.entry = entry;
            bindView(v, entry);
            return v;
        }

        @Override
        protected View newView(int position, ViewGroup parent) {
            throw new UnsupportedOperationException();
        }

        @Override
        protected void bindView(View view, ViewEntry entry) {
            final Resources resources = mContext.getResources();
            ViewCache views = (ViewCache) view.getTag();

            TextView label = views.label;
            setMaxLines(label, entry.maxLabelLines);
            label.setText(entry.label);

            TextView data = views.data;
            if (data != null) {
                data.setText(entry.data);
                setMaxLines(data, entry.maxLines);
            }

            if (!TextUtils.isEmpty(entry.footerLine)) {
                views.footer.setText(entry.footerLine);
                views.footer.setVisibility(View.VISIBLE);
            } else {
                views.footer.setVisibility(View.GONE);
            }

            views.primaryIcon.setVisibility(entry.isPrimary ? View.VISIBLE : View.GONE);

            ImageView action = views.actionIcon;
            if (entry.actionIcon != -1) {
                Drawable actionIcon;
                if (entry.resPackageName != null) {
                    // Load external resources through PackageManager
                    actionIcon = mContext.getPackageManager().getDrawable(entry.resPackageName, entry.actionIcon, null);
                } else {
                    actionIcon = resources.getDrawable(entry.actionIcon);
                }
                action.setImageDrawable(actionIcon);
                action.setVisibility(View.VISIBLE);
            } else {
                action.setVisibility(View.INVISIBLE);
            }

            Drawable presenceIcon = null;
            if (entry.presenceIcon != -1) {
                presenceIcon = resources.getDrawable(entry.presenceIcon);
            } else if (entry.presence != -1) {
                presenceIcon = resources.getDrawable(entry.presenceIcon);
            }
            ImageView presenceIconView = views.presenceIcon;
            if (presenceIcon != null) {
                presenceIconView.setImageDrawable(presenceIcon);
                presenceIconView.setVisibility(View.VISIBLE);
            } else {
                presenceIconView.setVisibility(View.GONE);
            }

            ImageView secondaryActionView = views.secondaryActionButton;
            Drawable secondaryActionIcon = null;
            if (entry.secondaryActionIcon != -1) {
                secondaryActionIcon = resources.getDrawable(entry.secondaryActionIcon);
            }
            if (entry.secondaryIntent != null && secondaryActionIcon != null) {
                secondaryActionView.setImageDrawable(secondaryActionIcon);
                secondaryActionView.setTag(entry.secondaryIntent);
                secondaryActionView.setVisibility(View.VISIBLE);
                views.secondaryActionDivider.setVisibility(View.VISIBLE);
            } else {
                secondaryActionView.setVisibility(View.GONE);
                views.secondaryActionDivider.setVisibility(View.GONE);
            }
        }

        private void setMaxLines(TextView textView, int maxLines) {
            if (maxLines == 1) {
                textView.setSingleLine(true);
            } else {
                textView.setSingleLine(false);
                textView.setMaxLines(maxLines);
                textView.setEllipsize(null);
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
    }
}
