/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Photo;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.contacts.Cont.acts.BindSync;
import com.ringcentral.android.contacts.Cont.acts.ContactBinding;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.provider.RCMDataStore.FavoritesTable;
import com.ringcentral.android.search.ContactsSearchAgent;
import com.ringcentral.android.search.ContactsSearchAgent20;
import com.ringcentral.android.utils.PhoneUtils;

/**
 * Underlying platform Contacts proxy for Android 2.0 or above
 */
public class ContactProxy20 extends ContactProxy {
    /**
     * Define logging tag.
     */
    private static final String LOG_TAG = "[RC] ContactProxy20";

    @Override
    public Uri getPeopleUri() {
        return ContactsContract.Contacts.CONTENT_URI;
    }
    
    @Override
    public Intent getCreateNewContactIntent(Context context, String contactName, String contactNumber) {
        Intent i = new Intent(Intent.ACTION_INSERT);
        i.setType(ContactsContract.Contacts.CONTENT_TYPE);
        i.putExtra(ContactsContract.Intents.Insert.NAME, contactName);
        i.putExtra(ContactsContract.Intents.Insert.PHONE, contactNumber);
        return i;
    }
    
    @Override
    public Intent getAddToContactIntent(Context context, String contactNumber) {
        Intent i = new Intent(Intent.ACTION_INSERT_OR_EDIT);
        i.setType(ContactsContract.Contacts.CONTENT_ITEM_TYPE);
        i.putExtra(ContactsContract.Intents.Insert.PHONE, contactNumber);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        return i;
    }
    
    @Override
    public List<Cont.acts.EmailContact> getEmailAddresses(Context context, long contactId) {
        ArrayList<Cont.acts.EmailContact> list = new ArrayList<Cont.acts.EmailContact>();
        if (contactId < 0) {
            if (LogSettings.MARKET) {
                QaLog.w(LOG_TAG, "getEmailAddresses error: id is invalid");
            }
            return list;
        }
        
        Cursor c = null;
        try {
            c = context.getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION10,
                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[] { String.valueOf(contactId) }, null);
            if (c != null) {
                c.moveToPosition(-1);
                while (c.moveToNext()) {
                    try {
                        Cont.acts.EmailContact e = new Cont.acts.EmailContact();
                        e.displayName = c.getString(DISPLAY_NAME_INDX10);
                        e.contactId = contactId;
                        e.emailAddress = c.getString(DATA_INDX10);
                        int type = c.getInt(TYPE_INDX10);
                        Resources res = context.getResources();
                        if (type == ContactsContract.CommonDataKinds.Email.TYPE_CUSTOM) {
                            e.emailTag = c.getString(LABEL_INDX10);
                        } else {
                            if (type == ContactsContract.CommonDataKinds.Email.TYPE_HOME) {
                                e.emailTag = res.getString(R.string.email_tag_home);
                            } else if (type == ContactsContract.CommonDataKinds.Email.TYPE_MOBILE) {
                                e.emailTag = res.getString(R.string.email_tag_mobile);
                            } else if (type == ContactsContract.CommonDataKinds.Email.TYPE_WORK) {
                                e.emailTag = res.getString(R.string.email_tag_work);
                            } else if (type == ContactsContract.CommonDataKinds.Email.TYPE_OTHER) {
                                e.emailTag = res.getString(R.string.email_tag_other);
                            }
                        }
                        
                        if (e.emailTag == null || e.emailTag.trim().length() < 1) {
                            e.emailTag = res.getString(R.string.email_tag_other);
                        }
                        
                        list.add(e);
                    } catch (java.lang.Throwable error) {
                        if (LogSettings.MARKET) {
                            MktLog.e(LOG_TAG, "getEmailAddresses - read error: " + error.getMessage());
                        }
                    }
                }
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                MktLog.e(LOG_TAG, "getEmailAddresses: " + error.getMessage());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return list;
        
    }
    private static final String[] PROJECTION10 = {
        ContactsContract.CommonDataKinds.Email.TYPE,
        ContactsContract.CommonDataKinds.Email.DISPLAY_NAME,
        ContactsContract.CommonDataKinds.Email.LABEL,
        ContactsContract.CommonDataKinds.Email.DATA
    };
    private static final int TYPE_INDX10 = 0;
    private static final int DISPLAY_NAME_INDX10 = 1;
    private static final int LABEL_INDX10 = 2;
    private static final int DATA_INDX10 = 3;
    
    
    @Override
    public long lookupContactIDByPhoneNumber(String phoneNumber, Context context) {
        try {
            Uri uri = Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI, Uri
                    .encode(phoneNumber));
            Cursor cursor = context.getContentResolver().query(uri, PROJECTION1, null, null, null);
            if (cursor != null) {
                long id = -1;
                if (cursor.moveToFirst()) {
                    id = cursor.getLong(CONTACT_ID_INDX1);
                }
                cursor.close();
                return id;
            }
        } catch (Exception ex) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "Look-up contact by phone number failed: " + ex.getMessage());
            }
        }

        return -1;
    }

    private static final String[] PROJECTION1 = { ContactsContract.CommonDataKinds.Phone.CONTACT_ID };
    private static final int CONTACT_ID_INDX1 = 0;

    @Override
    public int addToPersonalFavorites(long phoneId, String originalNumber, String normalizedNumber, Context context) {
        if (PersonalFavorites.isFavorite(phoneId, context)) {
            return FV_RESULT_ADDED;
        }

//        if (PersonalFavorites.isFavorite(normalizedNumber, context)) {
//            return FV_RESULT_ADDED;
//        }

        long contactId = -1;
        long photoId = -1;
        long type = ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM;
        String number = null;
        Cursor cursor = null;
        String displayName = null;
        String label = null;
        try {
            Uri phoneUri = ContentUris.withAppendedId(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, phoneId);
            cursor = context.getContentResolver().query(phoneUri, PROJECTION2, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    contactId = cursor.getLong(CONTACT_ID_INDX2);
                    number = cursor.getString(NUMBER_INDX2);
                    type = cursor.getLong(TYPE_INDX2);
                    displayName = cursor.getString(DISPLAY_NAME_INDX2);
                    photoId = cursor.getLong(PHOTO_ID_INDX2);
                    label = cursor.getString(LABEL_INDX2);
                }
            }
        } catch (Exception ex) {
            if (LogSettings.MARKET) {
                MktLog.e(LOG_TAG, "addToPersonalFavorites() failed: " + ex.getMessage());
            }
        }

        if (cursor != null) {
            cursor.close();
        }

        if (contactId == -1 || number == null) {
            if (LogSettings.MARKET) {
                MktLog.w("RingCentral", "Add to favorites: contact not found.");
            }
            return FV_RESULT_CONTACT_RECORD_NOT_FOUND;
        }

        if (!number.equals(originalNumber)) {
            ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumber(number);
            if (!cpn.isValid) {
                if (LogSettings.MARKET) {
                    MktLog.w("RingCentral", "Add to favorites: number changed and is not valid.");
                }
                return FV_RESULT_CONTACT_RECORD_NOT_FOUND;
            }
            normalizedNumber = cpn.normalizedNumber;
        }

        long mailBoxID = RCMProviderHelper.getCurrentMailboxId(context);
        ContentValues values = new ContentValues();
        values.put(FavoritesTable.MAILBOX_ID, mailBoxID);
        values.put(FavoritesTable.DISPLAY_NAME, displayName);
        values.put(FavoritesTable.ORIGINAL_NUMBER, number);
        values.put(FavoritesTable.NORMALIZED_NUMBER, normalizedNumber);
        values.put(FavoritesTable.TYPE, type);
        
        if (isCustomTagPhoneNumber(context, type)) {
            values.put(FavoritesTable.LABEL, label);
        } else {
            values.put(FavoritesTable.LABEL, (String)null);
        }
        
        values.put(FavoritesTable.CONTACT_ID, contactId);
        values.put(FavoritesTable.PHONE_ID, phoneId);

        if (photoId != -1) {
            values.put(FavoritesTable.PHOTO_ID, photoId);
        }
        if (LogSettings.ENGINEERING) {
            EngLog.i(LOG_TAG, "addToPersonalFavorites(): displayName = " + (displayName == null ? "null" : displayName));
            EngLog.i(LOG_TAG, "addToPersonalFavorites(): original    = " + number);
            EngLog.i(LOG_TAG, "addToPersonalFavorites(): number      = " + normalizedNumber);
            EngLog.i(LOG_TAG, "addToPersonalFavorites(): type        = " + type);
            EngLog.i(LOG_TAG, "addToPersonalFavorites(): contactID   = " + contactId);
            EngLog.i(LOG_TAG, "addToPersonalFavorites(): phoneID     = " + phoneId);
            EngLog.i(LOG_TAG, "addToPersonalFavorites(): photoID     = " + photoId);
        }

        Uri uri = UriHelper.getUri(RCMProvider.FAVORITES);
        context.getContentResolver().insert(uri, values);

        return FV_RESULT_ADDED;
    }

    private static final String[] PROJECTION2 = { ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.TYPE,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, 
            ContactsContract.CommonDataKinds.Phone.PHOTO_ID,
            ContactsContract.CommonDataKinds.Phone.LABEL};
    private static final int CONTACT_ID_INDX2 = 0;
    private static final int NUMBER_INDX2 = 1;
    private static final int TYPE_INDX2 = 2;
    private static final int DISPLAY_NAME_INDX2 = 3;
    private static final int PHOTO_ID_INDX2 = 4;
    private static final int LABEL_INDX2 = 5;

    @Override
    public String lookupContactNameByPhoneNumber(String phoneNumber, Context context) {
        try {
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
            Cursor cursor = context.getContentResolver().query(uri, PROJECTION3, null, null, null);
            if (cursor != null) {
                String name = null;
                if (cursor.moveToFirst()) {
                    name = cursor.getString(DISPLAY_NAME_INDX3);
                }
                cursor.close();
                return name;
            }
        } catch (Exception ex) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "Look-up contact name by phone number failed: " + ex.getMessage());
            }
        }

        return null;
    }

    private static final String[] PROJECTION3 = { ContactsContract.PhoneLookup.DISPLAY_NAME };
    private static final int DISPLAY_NAME_INDX3 = 0;

    @Override
    public BindSync syncPersonalBind(Context context, long mailboxId, BindSync bindSync) {
        Cursor cursor = null;
        try {
            Uri phoneUri = ContentUris.withAppendedId(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, bindSync.bind.phoneId);
            cursor = context.getContentResolver().query(phoneUri, PROJECTION8, null, null, null);

            boolean found = false;
            String displayName = null;
            String phoneNumber = null;
            
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    displayName = cursor.getString(DISPLAY_NAME_INDX8);
                    phoneNumber = cursor.getString(NUMBER_ID_INDX8);
                    int type = cursor.getInt(TYPE_INDX8);
                    bindSync.bind.phoneNumberTag = getPhoneNumberTag(context, type);
                    if (isCustomTagPhoneNumber(context, type)) {
                        String label = cursor.getString(LABEL_INDX8);
                        if (label != null) {
                            bindSync.bind.phoneNumberTag = label;
                        }
                    }
                    bindSync.bind.isPhoneNumberFavorite = PersonalFavorites.isFavorite(bindSync.bind.phoneId, context);
                    bindSync.bind.isDefaultPhoneNumber = (cursor.getLong(IS_PRIMARY_INDX8) > 0);
                    found = true;
                }
            }
            if (found) {
                boolean success = true;
                if (!Cont.acts.compareStrings(phoneNumber, bindSync.bind.phoneNumber)) {
                    ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumber(phoneNumber);
                    if (!cpn.isValid) {
                        success = false;
                    } else {
                        if (!Cont.acts.compareStrings(bindSync.bind.cpn.phoneNumber.numRaw, 
                                cpn.phoneNumber.numRaw)) {
                            success = false;
                        } else {
                            bindSync.bind.displayName = displayName;
                            bindSync.bind.phoneNumber = phoneNumber;
                            bindSync.syncState = BindSync.State.UPDATED;
                            return bindSync;
                        }
                    }
                }
                
                if (success) {
                    if (!Cont.acts.compareStrings(displayName, bindSync.bind.displayName)) {
                        bindSync.bind.displayName = displayName;
                        bindSync.syncState = BindSync.State.UPDATED;
                    } else {
                        bindSync.syncState = BindSync.State.NOT_CHANGED;
                        return bindSync;
                    }
                }
            }
            
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                MktLog.e(LOG_TAG, "SyncBind: " + error.getMessage());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        
        ContactBinding newBind = Cont.acts.bindContactByNumber(context, mailboxId, bindSync.bind.originalNumber);
        if (newBind.isValid) {
            if (newBind.hasContact) {
                bindSync.bind = newBind;
                bindSync.syncState = BindSync.State.REBIND;
                return bindSync;
            }
        }

        ContactBinding cleanBind = new ContactBinding();
        cleanBind.hasContact = false;
        cleanBind.phoneId = -1;
        cleanBind.isValid = bindSync.bind.isValid;
        cleanBind.cpn = bindSync.bind.cpn;
        cleanBind.originalNumber = bindSync.bind.originalNumber;
        
        bindSync.syncState = BindSync.State.GONE;
        bindSync.bind = cleanBind;
        return bindSync;
    }
    
    private static final String[] PROJECTION8 = { 
            ContactsContract.CommonDataKinds.Phone.NUMBER, 
            ContactsContract.CommonDataKinds.Phone.TYPE,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, 
            ContactsContract.CommonDataKinds.Phone.LABEL,
            ContactsContract.CommonDataKinds.Phone.IS_SUPER_PRIMARY
    };
    private static final int NUMBER_ID_INDX8 = 0;
    private static final int TYPE_INDX8 = 1;
    private static final int DISPLAY_NAME_INDX8 = 2;
    private static final int LABEL_INDX8 = 3;
    private static final int IS_PRIMARY_INDX8 = 4;

    /**
     * Lookups phone id in Phones table by data got after phone lookup.
     * 
     * @param context
     *            the execution context
     * @param contactId
     *            the contact identifier
     * @param number
     *            the number found in the contact
     * @param type
     *            the tag of the <code>number</code>
     * @return <code>-1</code> if the phoneId has not been found, otherwise the
     *         identifier
     */
    private long getPhoneIdFromLookUp(Context context, long contactId, String number, long type) {
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    PROJECTION9,
                    "((" + ContactsContract.CommonDataKinds.Phone.NUMBER + " = ?) AND ("
                            + ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? ) AND (" +
                            ContactsContract.CommonDataKinds.Phone.TYPE + " = ? ))" ,
                    new String[] { number, String.valueOf(contactId), String.valueOf(type)}, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    return cursor.getLong(ID_INDX9);
                }
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "getPhoneIdFromLookUp: " + error.getMessage());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return -1;
    }
    private static final String[] PROJECTION9 = {
        ContactsContract.CommonDataKinds.Phone._ID,
        ContactsContract.CommonDataKinds.Phone.NUMBER, 
        ContactsContract.CommonDataKinds.Phone.TYPE,
        ContactsContract.CommonDataKinds.Phone.CONTACT_ID};
    private static final int ID_INDX9 = 0;

    
    @Override
    ContactBinding bindPersonalContact(Context context, long mailboxId, ContactBinding binding) {
        Cursor cursor = null;
        try {
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri
                    .encode(binding.cpn.normalizedNumber));
            cursor = context.getContentResolver().query(uri, PROJECTION4, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    binding.displayName = cursor.getString(DISPLAY_NAME_INDX4);
                    binding.phoneNumber = cursor.getString(NUMBER_INDX4);
                    int type = cursor.getInt(TYPE_INDX4);
                    binding.phoneId = getPhoneIdFromLookUp(context, cursor.getLong(ID_INDX4), binding.phoneNumber, type);
                    
                    if (binding.phoneId == -1) {
                        if (LogSettings.MARKET) {
                            QaLog.e(LOG_TAG, "bindPersonalContact, phoneId is not defined for lookup " + cursor.getLong(ID_INDX4));
                        }
                        binding.hasContact = false;
                        return binding;
                    }
                    
                    binding.hasContact = true;
                    binding.isPersonalContact = true;
                    binding.phoneNumberTag = getPhoneNumberTag(context, type);
                    if (isCustomTagPhoneNumber(context, type)) {
                        String label = cursor.getString(LABEL_INDX4);
                        if (label != null) {
                            binding.phoneNumberTag = label;
                        }
                    }
                    binding.isPhoneNumberFavorite = PersonalFavorites.isFavorite(binding.phoneId , context);
                    Cursor c = null;
                    try {
                        Uri phoneUri = ContentUris.withAppendedId(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, binding.phoneId);
                        c = context.getContentResolver().query(phoneUri, PROJECTION8, null, null, null);
                        if (c != null) {
                            if (c.moveToFirst()) {
                                binding.isDefaultPhoneNumber = (c.getLong(IS_PRIMARY_INDX8) > 0);
                            }
                        }
                    } catch (java.lang.Throwable error) {
                        if (LogSettings.MARKET) {
                            QaLog.e(LOG_TAG, "SyncBind2: " + error.getMessage());
                        }
                    } finally {
                        if (c != null) {
                            c.close();
                        }
                    }
                }
            }

        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "Contact binding: " + error.getMessage());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return binding;
    }

    private static final String[] PROJECTION4 = {
            /**
             * Actually this is contact identifier.
             */
            ContactsContract.PhoneLookup._ID, 
            ContactsContract.PhoneLookup.DISPLAY_NAME, 
            ContactsContract.PhoneLookup.TYPE,
            ContactsContract.PhoneLookup.LABEL,
            ContactsContract.PhoneLookup.NUMBER};
    private static final int ID_INDX4 = 0;
    private static final int DISPLAY_NAME_INDX4 = 1;
    private static final int TYPE_INDX4 = 2;
    private static final int LABEL_INDX4 = 3;
    private static final int NUMBER_INDX4 = 4;

    @Override
    public boolean isCustomTagPhoneNumber(Context context, long tag) {
        return (ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM == tag);
    }
    
    @Override
    public String getPhoneNumberTag(Context context, long tag) {
        switch ((int)tag) {
        case (ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM):
            return context.getString(R.string.phone_tag_custom);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE):
            return context.getString(R.string.phone_tag_mobile);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_HOME):
            return context.getString(R.string.phone_tag_home);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_WORK):
            return context.getString(R.string.phone_tag_work);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_OTHER):
            return context.getString(R.string.phone_tag_other);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_MAIN):
            return context.getString(R.string.phone_tag2_main);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_CAR):
            return context.getString(R.string.phone_tag2_car);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_COMPANY_MAIN):
            return context.getString(R.string.phone_tag2_company_main);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE):
            return context.getString(R.string.phone_tag2_work_mobile);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_FAX_WORK):
            return context.getString(R.string.phone_tag_fax_work);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_ASSISTANT):
            return context.getString(R.string.phone_tag2_assistant);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_CALLBACK):
            return context.getString(R.string.phone_tag2_callback);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_FAX_HOME):
            return context.getString(R.string.phone_tag_fax_home);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_ISDN):
            return context.getString(R.string.phone_tag2_isdn);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_MMS):
            return context.getString(R.string.phone_tag2_mms);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_OTHER_FAX):
            return context.getString(R.string.phone_tag2_other_fax);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_PAGER):
            return context.getString(R.string.phone_tag_pager);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_WORK_PAGER):
            return context.getString(R.string.phone_tag2_work_pager);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_RADIO):
            return context.getString(R.string.phone_tag2_radio);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_TELEX):
            return context.getString(R.string.phone_tag2_telex);
        case (ContactsContract.CommonDataKinds.Phone.TYPE_TTY_TDD):
            return context.getString(R.string.phone_tag2_tty_tdd);
        }
        return context.getString(R.string.phone_tag_other);
    }

    @Override
    public ContactsSearchAgent getContactsSearchAgent() {
        return ContactsSearchAgent20.getInstance();
    }

    @Override
    public Bitmap getContactPhoto(Context context, long photoId, BitmapFactory.Options options) {
        if (photoId < 0) {
            if (LogSettings.ENGINEERING) {
                EngLog.w(LOG_TAG, "Photo retrieving failed. Id is invalid.");
            }
            return null;
        }

        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(
                    ContentUris.withAppendedId(ContactsContract.Data.CONTENT_URI, photoId),
                    new String[] { Photo.PHOTO }, null, null, null);

            if (cursor != null && cursor.moveToFirst() && !cursor.isNull(0)) {
                byte[] photoData = cursor.getBlob(0);
                // Workaround for Android Issue 8488 http://code.google.com/p/android/issues/detail?id=8488
                if (options == null) {
                    options = new BitmapFactory.Options();
                }
                options.inTempStorage = new byte[16 * 1024];
                options.inSampleSize = 2;
                return BitmapFactory.decodeByteArray(photoData, 0, photoData.length, options);
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                QaLog.w(LOG_TAG, "Photo retrieving failed (id = " + photoId + "): " + error.getMessage());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    @Override
    public List<Cont.acts.TaggedContactPhoneNumber> getPersonalContactPhoneNumbers(Context context, long contactId,
            boolean onlyFullValidNumbers) {
        ArrayList<Cont.acts.TaggedContactPhoneNumber> list = new ArrayList<Cont.acts.TaggedContactPhoneNumber>();
        if (contactId < 0) {
            if (LogSettings.MARKET) {
                MktLog.w(LOG_TAG, TAG9 + " error: id is invalid");
            }
            return list;
        }

        Cursor cursor = null;

        try {
            String[] whereArgs = new String[] { String.valueOf(contactId) };
            cursor = context.getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    PROJECTION7, 
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", whereArgs, null);

            if (cursor != null) {
                cursor.moveToPosition(-1);
                while (cursor.moveToNext()) {
                    try {
                        Cont.acts.TaggedContactPhoneNumber num = new Cont.acts.TaggedContactPhoneNumber();
                        num.cpn = PhoneUtils.getContactPhoneNumber(cursor.getString(NUMBER_INDX7));
                        if (!num.cpn.isValid) {
                            continue;
                        }
                        
                        if (onlyFullValidNumbers && !num.cpn.phoneNumber.isFullValid) {
                            continue;
                        }
                        
                        int type = cursor.getInt(TYPE_INDX7);
                        num.numberTag = getPhoneNumberTag(context, type);
                        if (isCustomTagPhoneNumber(context, type)) {
                            String label = cursor.getString(LABEL_INDX7);
                            if (label != null) {
                                num.numberTag = label;
                            }
                        }
                        
                        if (cursor.getInt(IS_PRIMARY_INDX7) != 0) {
                            num.isDefault = true;
                        }
                        num.id = cursor.getLong(ID_INDX7);
                        list.add(num);
                    } catch (java.lang.Throwable error) {
                        if (LogSettings.MARKET) {
                            QaLog.e(LOG_TAG, TAG9 + " read error: " + error.getMessage());
                        }
                    }
                }
            }
        } catch (java.lang.Throwable ex) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, TAG9 + " error failed: " + ex.getMessage());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return list;
    }

    private static final String[] PROJECTION7 = {
        ContactsContract.CommonDataKinds.Phone.TYPE,
        ContactsContract.CommonDataKinds.Phone.NUMBER,
        ContactsContract.CommonDataKinds.Phone.IS_SUPER_PRIMARY,
        ContactsContract.CommonDataKinds.Phone.LABEL,
        ContactsContract.CommonDataKinds.Phone._ID
    };
    private static final int TYPE_INDX7 = 0;
    private static final int NUMBER_INDX7 = 1;
    private static final int IS_PRIMARY_INDX7 = 2;
    private static final int LABEL_INDX7 = 3;
    private static final int ID_INDX7 = 4;
    private static final String TAG9 = "Get personal contacts numbers";

    @Override
    public Cont.acts.PhoneContact lookUpPhoneContactById(Context context, long phoneId) {
        Cont.acts.PhoneContact contact = new Cont.acts.PhoneContact();
        Cursor cursor = null;
        try {
            Uri phoneUri = ContentUris.withAppendedId(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, phoneId);
            cursor = context.getContentResolver().query(phoneUri, PROJECTION5, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    contact.phoneId = phoneId;
                    contact.contactId = cursor.getLong(CONTACT_ID_INDX5);
                    contact.number = cursor.getString(NUMBER_INDX5);
                    contact.type = cursor.getInt(TYPE_INDX5);
                    contact.label = cursor.getString(LABEL_INDX5);
                    contact.displayName = cursor.getString(DISPLAY_NAME_INDX5);
                    contact.photoId = cursor.getLong(PHOTO_ID_INDX5);
                    return contact;
                }
            }
        } catch (Exception ex) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "lookUpPhoneContactById() failed: " + ex.getMessage());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    private static final String[] PROJECTION5 = { ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.TYPE,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, 
            ContactsContract.CommonDataKinds.Phone.PHOTO_ID,
            ContactsContract.CommonDataKinds.Phone.LABEL};
    private static final int CONTACT_ID_INDX5 = 0;
    private static final int NUMBER_INDX5 = 1;
    private static final int TYPE_INDX5 = 2;
    private static final int DISPLAY_NAME_INDX5 = 3;
    private static final int PHOTO_ID_INDX5 = 4;
    private static final int LABEL_INDX5 = 5;

    @Override
    public Cont.acts.PhoneContact lookUpPhoneContactByNumber(Context context, String number) {
        Cursor cursor = null;
        try {
            Uri uri = Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI, Uri
                    .encode(number));
            cursor = context.getContentResolver().query(uri, PROJECTION6, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    return lookUpPhoneContactById(context, cursor.getLong(PHONE_ID_INDX6));
                }
            }
        } catch (java.lang.Throwable ex) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "lookUpPhoneContactByNumber() failed: " + ex.getMessage());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    private static final String[] PROJECTION6 = { ContactsContract.CommonDataKinds.Phone._ID };
    private static final int PHONE_ID_INDX6 = 0;
}
