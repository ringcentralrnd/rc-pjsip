package com.ringcentral.android.contacts;

import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.contacts.Cont.acts.TaggedContactPhoneNumber;
import com.ringcentral.android.contacts.Projections.Personal;
import com.ringcentral.android.provider.RCMProviderHelper;

public class AddFavoritesActivity extends ContactsListActivity {
	private static String TAG = "[RC] AddFavoritesActivity";
	private TextView mTitle;
	private Button mBtnBack;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mTitle = (TextView) findViewById(R.id.title);
		final String action = getIntent().getAction();
		if (RCMConstants.ACTION_LIST_CONTACTS.equals(action)) {
			mTitle.setText(R.string.filter_name_personal);
		} else if (RCMConstants.ACTION_LIST_EXTENSIONS.equals(action)) {
			mTitle.setText(R.string.filter_name_company);
		}
		mBtnAddContact.setVisibility(View.INVISIBLE);
		mBtnBack = (Button) findViewById(R.id.btnTopLeft);
		mBtnBack.setVisibility(View.VISIBLE);
		mBtnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		mSubTabs.setVisibility(View.GONE);

	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		try {
			Cursor cursor = (Cursor) getListAdapter().getItem(position);
			if (cursor == null) {
				return;
			}
			if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
				CompanyFavorites.setAsFavorite(this, RCMProviderHelper.getCurrentMailboxId(this), id);
				finish();
				return;
			}
			int hasPhone = cursor.getInt(Personal.HAS_PHONE_COLUMN_INDEX);
			if (hasPhone != 0) {
				List<Cont.acts.TaggedContactPhoneNumber> list = Cont.acts().getPersonalContactPhoneNumbers(this, cursor.getLong(Personal.ID_COLUMN_INDEX), false);
				if (!list.isEmpty()) {
					addToFavorites(cursor);
					return;
				}
			}
		} catch (java.lang.Throwable error) {
			if (LogSettings.MARKET) {
				QaLog.e(TAG, " onListItemClick error: " + error.getMessage());
			}
		}
	}

	/**
	 * Toggle favorites.
	 * 
	 * @param cursor
	 *            the current cursor pointed to a contact
	 * @return
	 */
	private void addToFavorites(Cursor cursor) {
		if (cursor == null) {
			return;
		}
		super.togglePersonalFavorites(cursor);
	}

	@Override
	protected void addToPersonalFavorites(List<TaggedContactPhoneNumber> not_favorites) {
		super.addToPersonalFavorites(not_favorites);
		finish();
	}

	@Override
	protected void showAddSelectionDialog(List<TaggedContactPhoneNumber> not_favorites) {
		PhoneSelectionDialog dialog = new PhoneSelectionDialog(this, not_favorites, getString(R.string.menu_addStar), PhoneSelectionDialog.ACTION_ADD_TO_FAVORITE, true);
		dialog.show();
	}

	@Override
	protected void deletePersonalFavorite(List<TaggedContactPhoneNumber> favorites) {
	}

	protected void showRemoveSelectionDialog(List<TaggedContactPhoneNumber> favorites) {
		Builder dialog = new AlertDialog.Builder(AddFavoritesActivity.this).setTitle(R.string.no_number).setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
	}
}
