package com.ringcentral.android.contacts;



import java.util.ArrayList;
import java.util.List;
import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.calllog.CallLogQuery;
import com.ringcentral.android.calllog.CallLogSync;
import com.ringcentral.android.contacts.Cont.acts.BindSync;
import com.ringcentral.android.contacts.Cont.acts.ContactBinding;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.ringout.RingOutAgent;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.LabelsUtils;
import com.ringcentral.android.utils.UserInfo;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.calllog.CallLogQuery;
import com.ringcentral.android.calllog.CallLogSync;
import com.ringcentral.android.contacts.Cont.acts.BindSync;
import com.ringcentral.android.contacts.Cont.acts.ContactBinding;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMDataStore.AccountInfoTable;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.ringout.RingOutAgent;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.LabelsUtils;

public class EventDetailActivity extends Activity implements View.OnClickListener{
	
	private static final String TAG = "[RC]EventDetailActivity";
	private Button mBtnAddContact;
	private Button mAddToExistingContact;
	private Button mBtnCall;
	private TextView mTvDisplayName;
	private TextView mTvContactLocation;
	private TextView mTvCallLogTime;
	private ArrayList<DetailData> mDetailDataList = null;
	
	
	private EventDetailKnownContactAdapter mAnownContactAdapter;

	
    /**
     * Keeps an identifier of the contact.
     */
   // private long mContactId = -1;

    /**
     * Keeps the RO agent.
     */
    private RingOutAgent mRingOutAgent;

    /**
     * Keeps call log record URI.
     */
    private Uri mCallLogUri;

   

	/**
     * Keeps mailbox identifier.
     */
    private long mMailboxId;

    /**
     * Keeps the identifier of the call log record.
     */
    private long mRecordId;

    /**
     * Keeps Call Log item.
     */
    private CallLogQuery.CallLogItem mCallLogItem;

    /**
     * Keeps the local synchronization service handle.
     */
    private LocalSyncService.LocalSyncBinder syncService;
    
    /**
     * Keeps bind.
     */
    //private BindSync mBsync;
    
    private Cursor mCursor;
    
    private ContentResolver mResolver;
    
    private boolean mObserverRegistered = false;
    
//    List<Cont.acts.TaggedContactPhoneNumber> mContactDetailFavoritesList;
//	List<Cont.acts.TaggedContactPhoneNumber> mContactDetailNotFavoritesList;
	protected View mHeader;
	private ListView mKnownListView;	 
	
	private boolean isKnownContact = false;
	
	
	private ServiceConnection syncSvcConn = new ServiceConnection() {
	        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
	            syncService = (LocalSyncService.LocalSyncBinder) rawBinder;
	            if (LogSettings.ENGINEERING) {
	                EngLog.d(TAG, "Sync service connected.");
	            }
	            try {
	                syncService.syncCallLog(RCMProviderHelper.getCurrentMailboxId(syncService.getService()));
	            } catch (java.lang.Throwable error) {
	                if (LogSettings.MARKET) {
	                    QaLog.e(TAG, "onServiceConnected() call sync failed: ", error);
	                }
	            }
	        }

	        public void onServiceDisconnected(ComponentName className) {
	            syncService = null;
	            if (LogSettings.ENGINEERING) {
	                EngLog.d(TAG, "Sync service disconnected.");
	            }
	        }
	  };
	  
	  @Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			// TODO Auto-generated method stub
	    	 switch (keyCode) {
	         case KeyEvent.KEYCODE_SEARCH: {
	        	 return true;
	         }
	    	 }
	         return super.onKeyDown(keyCode, event);
		}
	  
	  private final ContentObserver mObserver = new ContentObserver(new Handler()) {
	        @Override
	        public boolean deliverSelfNotifications() {
	            return true;
	        } 

	        @Override
	        public void onChange(boolean selfChange) {
	            if (mCursor != null && !mCursor.isClosed()) {
	                try {
	                	dataChanged();
	                } catch (java.lang.Throwable error) {
	                    if (LogSettings.MARKET) {
	                        QaLog.e(TAG, "onChange() error: " + error.getMessage());
	                    }
	                }
	            }
	        }
	    };
	    
	 private void dataChanged() {
		 
			 if (LogSettings.MARKET) {
	             MktLog.d(TAG, "Event detail : dataChanged");
	         }
	        try {
	            mCursor.requery();
	            if (mCursor.moveToFirst()) {
	                if (mCursor.getLong(CallLogQuery.CALLLOG_HAS_MORE_RECORD_ITEM_INDX) != 0) {
	                    finish();
	                    return;
	                }

	                mMailboxId = Long.parseLong(mCursor.getString(CallLogQuery.CALLLOG_MAILBOX_INDX));
	                mRecordId = mCursor.getLong(CallLogQuery.CALLLOG_ID_INDX);
	                ContactBinding bind = CallLogQuery.readSyncBind(this, mCursor);
	                BindSync bsync = Cont.acts.syncBind(this, mMailboxId, bind);
	                if (bsync.syncState == BindSync.State.INVALID_RECORD) {
	                    Cont.acts.bindSyncTrace(this, bsync, TAG, "dataChanged", mRecordId);
	                    finish();
	                    return;
	                } else if (bsync.syncState == BindSync.State.NOT_CHANGED) {
	                } else {
	                    Cont.acts.bindSyncTrace(this, bsync, TAG, "dataChanged", mRecordId);
	                    CallLogSync.updateCallLogBind(this, mMailboxId, mRecordId, bsync);
	                    bsync.syncState = BindSync.State.NOT_CHANGED;
	                }
	               // mBsync = bsync;
	            	CallLogQuery.CallLogItem ci = CallLogQuery.readCallLogItem(this, mCursor);
	            	
	            	/**
	            	 * when entry into by unknown Contact and create contact finished,would return to 
	            	 * know Contact screen, so initKnowContactUi
	            	 *  
	            	 */
	                if(bsync.bind.hasContact ){
	                	initKnowContactUI();
	                	isKnownContact = true;
	                } else {
	                	initUnKnowContactUI();
	                	isKnownContact = false;
	                }
	            	
	                if(isKnownContact){          
	                	setDetailListView(this, bsync.bind.phoneId, ci.cpn.normalizedNumber);
	                }
	                mTvDisplayName.setText(ci.displayName);
	                if(!ci.location.equals(this.getString(R.string.calllog_calltype_159_unknown))) {
	                	 mTvContactLocation.setText(ci.getPhoneTypeOrLocation());
	                }else {
	                	mTvContactLocation.setVisibility(View.GONE);
	                }
	               
	                mTvCallLogTime.setText(LabelsUtils.getDateLabel(ci.date) + "  " + LabelsUtils.getTimeLabel(ci.date));
	                mCallLogItem = ci;

	                if (bsync.bind.hasContact ) {
	                    if (bsync.bind.isPersonalContact) {
	                        Cont.acts.PhoneContact cf = Cont.acts().lookUpPhoneContactById(this, bsync.bind.phoneId);
	                        if (cf == null) {
	                            if (LogSettings.MARKET) {
	                                MktLog.e(TAG, "Contact lost");
	                            }
	                            
	                            initUnKnowContactUI();
	    	                	isKnownContact = false;
	                            return;
	                        }
	                      //  mContactId = cf.contactId;
	                        Bitmap photo = null;
	                        if (cf.photoId != -1) {
	                            photo = Cont.acts().getContactPhoto(getBaseContext(), cf.photoId, null);
	                        }

	                    } else {
	                     //   mContactId = bsync.bind.phoneId;
	                    }
	                } 
	            }
	        } catch (java.lang.Throwable error) {
	            if (LogSettings.MARKET) {
	                MktLog.e(TAG, "dataChanged() error: " + error.getMessage());
	            }
	            finish();
	        }
	    }
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    mCallLogUri = getIntent().getData();
	  
	    mResolver = getContentResolver();
		
        mCursor = mResolver.query(mCallLogUri, CallLogQuery.CALLLOG_SUMMARY_PROJECTION, null, null, null);
		Intent intent = getIntent();

		isKnownContact = intent.getBooleanExtra(RCMConstants.ISKNOWNCONTACT, false);
		
		if(isKnownContact){
			initKnowContactUI();
		}else{
			initUnKnowContactUI();
		}
       
        getApplicationContext().bindService(new Intent(this, LocalSyncService.class), syncSvcConn, BIND_AUTO_CREATE);
        mRingOutAgent = new RingOutAgent(this);
		
	}
	
	private void initKnowContactUI(){
		setContentView(R.layout.event_detail_known_contact);
		initTitle();
		TextView title = (TextView) findViewById(R.id.title);
		title.setText(R.string.contact_info_title);
		initKnownTitle();
	
	}
	
	

	
	public void setDetailListView(Context context, long phoneId,
			String currentNum) {
		Cont.acts.PhoneContact p = Cont.acts().lookUpPhoneContactById(this, phoneId);
		
		mDetailDataList = new ArrayList<DetailData>();

		List<Cont.acts.TaggedContactPhoneNumber> list = Cont.acts()
				.getPersonalContactPhoneNumbers(context, p.contactId, false);
		
		for (Cont.acts.TaggedContactPhoneNumber item : list) {
			boolean flag = PersonalFavorites.isFavorite(item.id, context);
			DetailData data = null;
			if(flag){
				data = new DetailData(item,true);
				if(item.cpn.normalizedNumber.equals(currentNum)){
					mDetailDataList.add(0, data);
				}else{
					mDetailDataList.add(data);
				}
			}else{
				data = new DetailData(item,false);
				if(item.cpn.normalizedNumber.equals(currentNum)){
					mDetailDataList.add(0, data);
				}else{
					mDetailDataList.add(data);
				}
			}
		}
		
		mKnownListView = (ListView) findViewById(R.id.contact_detail_list);
		/**
		 * if add many times,it would cause exception
		 */
		if (mKnownListView.getHeaderViewsCount() == 0 ){
			mKnownListView.addHeaderView(mHeader);
		}
		

		mAnownContactAdapter = new EventDetailKnownContactAdapter(this,
				mDetailDataList);
		mKnownListView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long arg3) {

				try {
				
					Cont.acts.TaggedContactPhoneNumber contactPhoneNumber = (mDetailDataList.get(position-1)).contactPhoneNumber;

					if (contactPhoneNumber.cpn != null
							&& contactPhoneNumber.cpn.isValid) {

//						if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
//							PerformanceMeasurementLogger.getInstance().start(
//									PerformanceMeasurementLogger.CALL_FROM_CALLLOG);
//						}
//						boolean isNonFaxAccout = ((RCMProviderHelper.getTierSettings(EventDetailActivity.this) & RCMConstants.TIERS_PHS_DO_NOT_DISTURB) == 0) ? false : true; 
//						if(isNonFaxAccout){
//						mRingOutAgent.call(contactPhoneNumber.cpn.phoneNumber.numRaw,
//								contactPhoneNumber.numberTag);
//						}
						if(RCMProviderHelper.getServiceVersion(EventDetailActivity.this) >= AccountInfoTable.SERVICE_VERSION_5) {
							boolean isFaxTier = (UserInfo.TIER_SERVICE_TYPE_RCFAX == UserInfo.getTierServiceType(EventDetailActivity.this));
							if(!isFaxTier) {
								mRingOutAgent.call(contactPhoneNumber.cpn.phoneNumber.numRaw,
								contactPhoneNumber.numberTag);
							}
						}
					}

				} catch (java.lang.Throwable error) {
					if (LogSettings.MARKET) {
						QaLog.e(TAG, "onListItemClick failed: ", error);
					}
				}
			}});

		mKnownListView.setAdapter(mAnownContactAdapter);

	}

	
	private void initUnKnowContactUI(){
		
		setContentView(R.layout.event_details_unknow_contact);
		initTitle();
		mTvDisplayName = (TextView)findViewById(R.id.name);
		mTvContactLocation = (TextView)findViewById(R.id.location);
		mTvCallLogTime = (TextView)findViewById(R.id.time);
		
 
		mBtnCall = (Button)findViewById(R.id.event_detail_call);
		/**
		 * if fax Tier, hidden call button
		 */
	//	String tierServiceType =  RCMProviderHelper.getTierServiceType(getApplicationContext());
		
		if(RCMProviderHelper.getServiceVersion(this) >= AccountInfoTable.SERVICE_VERSION_5) {
			boolean isFaxTier = (UserInfo.TIER_SERVICE_TYPE_RCFAX == UserInfo.getTierServiceType(this));
			if(isFaxTier) {
				View layout1 = findViewById(R.id.event_detail_layout1);
				layout1.setVisibility(View.GONE);
			}
		}

		
		mBtnAddContact = (Button) findViewById(R.id.event_detail_createContact);
		mBtnAddContact.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addContact();
			}
		});
		
		mAddToExistingContact = (Button) findViewById(R.id.event_detail_add_to_contact);
		mAddToExistingContact.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				addToExistingContact();
			}
		});
		
		
		mBtnCall.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 mRingOutAgent.call(mCallLogItem.cpn.phoneNumber.numRaw, mCallLogItem.displayName);
			}
		});
	}
	
	private void initTitle(){
		TextView title = (TextView) findViewById(R.id.title);
		title.setText(R.string.information);
		Button btnBack = (Button)findViewById(R.id.btnTopLeft);
		btnBack.setVisibility(View.VISIBLE);
		btnBack.setText(R.string.topleftback);
		btnBack.setOnClickListener(this);
	}
	
	private void initKnownTitle(){
		mHeader = getLayoutInflater().inflate(
				R.layout.event_detail_header, null);
		mTvDisplayName = (TextView) mHeader.findViewById(R.id.name);
		mTvContactLocation = (TextView) mHeader.findViewById(R.id.location);
		mTvCallLogTime = (TextView) mHeader.findViewById(R.id.time);
	}


	public void onClick(View view){
		
		switch (view.getId()){
		case R.id.btnTopLeft :
			this.finish();
			break;
		}
	}
	
    @Override
    public void onDestroy() {
        super.onDestroy();
       
        
        try {
        	     	
            if (syncService != null) {
                getApplicationContext().unbindService(syncSvcConn);
                syncService = null;
                syncSvcConn = null;
            }
            
            if (mCursor != null && !mCursor.isClosed()) {
                if (mObserverRegistered && mObserver != null) {
                    mObserverRegistered = false;
                    mCursor.unregisterContentObserver(mObserver);
                }
               // mCursor.deactivate();
                mCursor.close();
            }
            
            
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                QaLog.e(TAG, "onDestroy() error: " + error.getMessage());
            }
        }
        mKnownListView.setAdapter(null);
        mAnownContactAdapter = null;
        mRingOutAgent = null;
        mCallLogUri = null;
        mCallLogItem = null;
       // mBsync = null;
        mResolver = null;
        mCursor = null;
        
    }
    
    
    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (mCursor != null) {
                if (mObserverRegistered) {
                    mObserverRegistered = false;
                    mCursor.unregisterContentObserver(mObserver);
                }
                mCursor.deactivate();
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                QaLog.e(TAG, "onPause() error: " + error.getMessage());
            }
            finish();
        }
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        FlurryTypes.onEventScreen(FlurryTypes.EVENT_SCREEN_OPEN, FlurryTypes.SCR_EVENT_DETAILS);
        if (syncService != null) {
            try {
                syncService.syncCallLog(RCMProviderHelper.getCurrentMailboxId(this));
            } catch (java.lang.Throwable error) {
                if (LogSettings.MARKET) {
                    QaLog.e(TAG, "onResume() call sync failed: ", error);
                }
            }
        }
        mObserverRegistered = true;
        mCursor.registerContentObserver(mObserver);
        try {
        	dataChanged();
            
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                QaLog.e(TAG, "onResume() error: " + error.getMessage());
            }
            finish();
        }
    } 
    
    
    @Override
    protected void onStop() {
    	super.onStop();
    	FlurryTypes.onEndSession(this);
    }

    
	
	/**
	 * add contact using local address list function
	 */
	private void addContact() {
		try {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "Event detail : add Contact");
            }
			startActivity(Cont.acts().getCreateNewContactIntent(this, mCallLogItem.displayName, mCallLogItem.cpn.normalizedNumber));
		} catch (ActivityNotFoundException e) {
			if (LogSettings.MARKET) {
				QaLog.e(TAG, "create new contact start activity error: " + e.toString());
			}
		}	
	}
	
	private void addToExistingContact() {
	     try {
	    	 if (LogSettings.MARKET) {
	                MktLog.d(TAG, "Event detail : addToExistingContact");
	            }
	    	 startActivity(Cont.acts().getAddToContactIntent(EventDetailActivity.this ,mCallLogItem.cpn.phoneNumber.numRaw));
         } catch (ActivityNotFoundException e) {
             if (LogSettings.MARKET) {
                 MktLog.e(TAG, "add to existing contact start activity error: " + e.toString());
             }
         }
	}
}
