/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.contacts.Projections.Personal;
import com.ringcentral.android.utils.ContactsUtils;

public class ContactsDelete implements DialogInterface.OnClickListener, DialogInterface.OnMultiChoiceClickListener {

    private static final String TAG = "[RC] ContactsDelete";
    private static final boolean DEBUG = false;

    private Activity mContext;
    private AlertDialog mDialog;

    private CharSequence[] items;
    private HashMap<CharSequence, Uri> itemsCache = new HashMap<CharSequence, Uri>();

    public ContactsDelete(Activity context) {
        mContext = context;
        Cursor cursor = mContext.getContentResolver().query(ContactsUtils.Uri_People, Personal.CONTACTS_SUMMARY_PROJECTION, null, null, "display_name COLLATE LOCALIZED ASC");
        if (cursor != null && (cursor.getCount() > 0)) {
            cursor.moveToPosition(-1);
            while (cursor.moveToNext()) {
                String name = cursor.getString(Projections.Personal.NAME_COLUMN_INDEX);
                Uri uri = ContentUris.withAppendedId(ContactsUtils.Uri_People, cursor.getLong(Projections.Personal.ID_COLUMN_INDEX));
                if (uri != null) {
                    itemsCache.put(name == null ? "" : name, uri);
                }
            }
            SortedSet<CharSequence> sortedSet= new TreeSet<CharSequence>(itemsCache.keySet());
            items = new CharSequence[sortedSet.toArray().length];
            System.arraycopy(sortedSet.toArray(), 0, items, 0, sortedSet.toArray().length);

            AlertDialog.Builder dialogBuilder = RcAlertDialog.getBuilder(mContext)
                    .setMultiChoiceItems(items, null, this)
                    .setTitle(R.string.delete_contacts)
                    .setPositiveButton(R.string.done, this)
                    .setNegativeButton(R.string.dialog_btn_cancel, this);
            mDialog = dialogBuilder.create();
        } else {
            AlertDialog.Builder messageBuilder = RcAlertDialog.getBuilder(mContext)
                    .setTitle(R.string.app_name)
                    .setMessage(R.string.noContacts)
                    .setIcon(R.drawable.symbol_help)
                    .setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                        }
                    });
            mDialog = messageBuilder.create();
        }
        
        if (cursor != null) {
            try {
                cursor.close();
            } catch (java.lang.Throwable error) {
            }
        }
    }

    public void show() {
        mDialog.show();
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_NEGATIVE:
                dialog.dismiss();
                break;
            case DialogInterface.BUTTON_POSITIVE:
                RcAlertDialog.getBuilder(mContext)
                        .setTitle(R.string.app_name)
                        .setMessage(R.string.contacts_del_confirmation)
                        .setIcon(R.drawable.symbol_help)
                        .setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (LogSettings.MARKET) {
                                    MktLog.i(TAG, "User: Delete");
                                }
                                for (Long position: mDialog.getListView().getCheckItemIds()) {
                                    try {
                                        mContext.getContentResolver().delete(itemsCache.get(items[position.intValue()]), null, null);
                                    } catch (Throwable error) {
                                        if (LogSettings.MARKET) {
                                            QaLog.e(TAG, "onContactDelete(): ", error);
                                        }
                                    }
                                }
                            }
                        })
                        .setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).show();
                break;
        }
    }

    public void onClick(DialogInterface dialogInterface, int count, boolean state) {
        if (LogSettings.ENGINEERING && DEBUG) {
            EngLog.d(TAG, "Selected -  name: " + items[count] + ", count: " + count + ", checked: " + state);
        }
    }
}
