/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.SimpleAdapter;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.ringout.RingOutAgent;

public class PhoneSelectionDialog implements DialogInterface.OnClickListener,
	DialogInterface.OnDismissListener {

    private static String TAG = "[RC] PhoneSelectionDialog";
    private Activity mActivity;
    private AlertDialog mDialog;
    private SimpleAdapter mPhonesAdapter;
    private List<HashMap<String, String>> mFillMaps;
    private List<Cont.acts.TaggedContactPhoneNumber> mList;
    private int mAction;
    private boolean needToFinishActivityAfterClick = false;
    private String mCallerName;
    private static final String DISPLAY_NUMBER = "DISPLAY_NUMBER";
    private static final String TYPE = "TYPE";
    public static final int ACTION_CALL = 0;
    public static final int ACTION_ADD_TO_FAVORITE = 1;
    public static final int ACTION_REMOVE_FROM_FAVORITES = 2;

    public PhoneSelectionDialog(Activity activity,
	    List<Cont.acts.TaggedContactPhoneNumber> list, String title,
	    int action, boolean needToFinishActivityAfterClick) {
	mActivity = activity;
	this.needToFinishActivityAfterClick = needToFinishActivityAfterClick;
	if (list == null
		|| list.size() == 0
		|| activity == null
		|| !(action == ACTION_CALL || action == ACTION_ADD_TO_FAVORITE || action == ACTION_REMOVE_FROM_FAVORITES)) {
	    throw new IllegalArgumentException(TAG);
	}

	if (action == ACTION_CALL) {
	    mCallerName = title;
	    title = activity.getString(R.string.contacts_cal_prefix) + " "
		    + title;
	}

	mFillMaps = new ArrayList<HashMap<String, String>>();

	for (Cont.acts.TaggedContactPhoneNumber num : list) {
	    HashMap<String, String> map = new HashMap<String, String>();
	    map.put(DISPLAY_NUMBER, num.cpn.phoneNumber.localCanonical);
	    map.put(TYPE, num.numberTag);
	    mFillMaps.add(map);
	}
	String[] from = new String[] { DISPLAY_NUMBER, TYPE };
	int[] to = new int[] { android.R.id.text1, android.R.id.text2 };
	mPhonesAdapter = new SimpleAdapter(mActivity, mFillMaps,
		R.layout.simple_list_item_2, from, to);
	AlertDialog.Builder dialogBuilder = RcAlertDialog.getBuilder(mActivity)
		.setAdapter(mPhonesAdapter, this).setTitle(title);
	mDialog = dialogBuilder.create();
	mList = list;
	mAction = action;
    }

    public void show() {
	if (mFillMaps.size() == 0) {
	    return;
	}
	mDialog.show();
    }

    public void onClick(DialogInterface dialog, int which) {
	try {
	    Cont.acts.TaggedContactPhoneNumber num = mList.get(which);

	    if (mAction == ACTION_CALL) {
		if (LogSettings.MARKET) {
		    QaLog.d(TAG, "User: Call");
		}
		new RingOutAgent(mActivity).call(num.cpn.phoneNumber.numRaw,
			mCallerName);
	    } else if (mAction == ACTION_ADD_TO_FAVORITE) {
		if (LogSettings.MARKET) {
		    QaLog.d(TAG, "User: Set as Favorite");
		}
		Cont.acts().addToPersonalFavorites(num.id,
			num.cpn.originalNumber, num.cpn.normalizedNumber,
			mActivity);
		if (needToFinishActivityAfterClick) {
		    mActivity.finish();
		}
	    } else if (mAction == ACTION_REMOVE_FROM_FAVORITES) {
		if (LogSettings.MARKET) {
		    MktLog.d(TAG, "User: Remove from Favorites");
		}
		PersonalFavorites.deletePersonalFavorite(mActivity,
			RCMProviderHelper.getCurrentMailboxId(mActivity),
			num.id);
	    }
	} catch (Exception ex) {
	    if (LogSettings.MARKET) {
		MktLog.e(TAG, "onClick failed: " + ex.getMessage());
	    }
	}
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
	mActivity = null;
	mFillMaps = null;
	mList = null;
    }
}
