/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.ActivityNotFoundException;
import android.content.AsyncQueryHandler;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts.People;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.contacts.BaseContactsActivity.ContactListItemCache;
import com.ringcentral.android.contacts.Cont.acts.TaggedContactPhoneNumber;
import com.ringcentral.android.contacts.Projections.Extensions;
import com.ringcentral.android.contacts.Projections.Personal;
import com.ringcentral.android.provider.RCMDataStore.ExtensionsTable;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.ContactsUtils;

public class ContactsListActivity extends BaseContactsActivity {
	/** Show all contacts */
	protected static final int MODE_CONTACTS = 1 | MODE_MASK_CONTACTS | MODE_MASK_SHOW_PHOTOS;

	/** Show all extensions */
	protected static final int MODE_EXTENSIONS = 3 | MODE_MASK_EXTENSION;

	/** Title for names which first letter is none English letter */
	protected static final String NONE_ENGLISH_LETTER_TITLE = "#";

	/**
	 * Regular expression to detect if contact name's first letter is English
	 * letter
	 */
	protected static final String FIRST_ENGLISH_LETTER_PATTERN = "^[A-Za-z]";

	/** Regular expression object */
	protected final Pattern mPattern = Pattern.compile(FIRST_ENGLISH_LETTER_PATTERN);

	/** Add+ button */
	protected Button mBtnAddContact;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contacts_list_content);

		final String action = getIntent().getAction();
		if (RCMConstants.ACTION_LIST_CONTACTS.equals(action)) {
			mMode = MODE_CONTACTS;
		} else if (RCMConstants.ACTION_LIST_EXTENSIONS.equals(action)) {
			mMode = MODE_EXTENSIONS;
		}

		/**
		 * Initial common part of Contacts and Favorite screen
		 */
		initCommon();
		setTitle();
		/**
		 * Initial add contact button
		 */
		mBtnAddContact = (Button) findViewById(R.id.btnTopRight);
		mBtnAddContact.setVisibility(View.VISIBLE);
		mBtnAddContact.setOnClickListener(new View.OnClickListener() {
			/**
			 * Called when a view has been clicked.
			 * 
			 * @param v
			 *            The view that was clicked.
			 */
			public void onClick(View v) {
				addContact();
			}
		});

		/** Init queryHandler */
		mQueryHandler = new QueryHandler(this);

		/**
		 * Set Contacts adapter
		 */
		mAdapter = new ContactsAdapter(this);
		setListAdapter(mAdapter);
		getListView().setOnScrollListener(mAdapter);

		/**
		 * Set Tag
		 */
		TAG = "[RC] ContactsListActivity";
	}

	private void setTitle() {
		TextView title = (TextView) findViewById(R.id.title);
		title.setText(R.string.tab_name_contacts);
	}

	@Override
	protected void onResume() {
		FlurryTypes.onEventScreen(FlurryTypes.EVENT_SCREEN_OPEN,
				(mMode == MODE_EXTENSIONS) ? FlurryTypes.SCR_CONTACTS_EXTENSION : FlurryTypes.SCR_CONTACTS_PERSONAL);
		super.onResume();
	}

	@Override
	public ListView getListView() {
		if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
			ListView listView = super.getListView();
			listView.setOnScrollListener(new OnScrollListener() {

				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
					if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED && mMode == MODE_CONTACTS && totalItemCount > 0) {
						PerformanceMeasurementLogger.getInstance().stop(
								PerformanceMeasurementLogger.CONTACTS_LOG_SWITCH);
					}
				}
			});

			return listView;
		} else {
			return super.getListView();
		}
	}

	private class ContactSectionMapper implements SectionIndexer {

		private String[] mSectionData;

		private HashMap<Integer, Integer> mSectionPositionMap;

		private HashMap<Integer, Integer> mPositionSectionMap;

		public ContactSectionMapper(String[] sections, HashMap<Integer, Integer> sectionPositionMap,
				HashMap<Integer, Integer> positionSectionMap) {
			mSectionData = sections;
			mSectionPositionMap = sectionPositionMap;
			mPositionSectionMap = positionSectionMap;
		}

		@Override
		public Object[] getSections() {
			return mSectionData;
		}

		@Override
		public int getPositionForSection(int section) {
			if (mSectionPositionMap == null || !mSectionPositionMap.containsKey(section))
				return -1;

			return mSectionPositionMap.get(section);
		}

		@Override
		public int getSectionForPosition(int position) {
			if (mPositionSectionMap == null || !mPositionSectionMap.containsKey(position))
				return -1;

			return mPositionSectionMap.get(position);
		}

		public boolean isSection(int position) {
			if (mPositionSectionMap != null && mPositionSectionMap.containsKey(position)) {
				return true;
			}
			return false;
		}

		public String getSection(int sectionIndex) {
			if (mSectionData == null)
				return "";

			return mSectionData[getSectionForPosition(sectionIndex)];
		}

	}

	/**
	 * Asynchronous query handler.
	 */
	protected final class QueryHandler extends AsyncQueryHandler {
		/**
		 * Asynchronous query handler constructor.
		 */
		public QueryHandler(Context context) {
			super(context.getContentResolver());

		}

		/**
		 * On query completion.
		 */
		@Override
		protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
			setLoading(false);
			if (cursor == null || cursor.isClosed()) {
				return;
			}
			if (!isFinishing()) {
				ContactSectionMapper sectionMapper = processCursor(cursor);
				mAdapter.setLoading(false);
				mAdapter.changeCursor(cursor);
				((ContactsAdapter) mAdapter).changeSectionMapper(sectionMapper);
			} else {
				cursor.close();
			}
		}
	}

	/**
	 * Define the common action of grouped mode and ungroup mode.
	 */
	public interface ListDisplayMode {

		public int getItemViewType(int position);

		public int getViewTypeCount();

		public boolean isSection(int position);
	}

	/**
	 * Item view cache holder.
	 */
	protected static final class SectionedContactListItemCache {
		TextView sectionHeader;
		ContactListItemCache contactItemCache;

		public SectionedContactListItemCache() {
			contactItemCache = new ContactListItemCache();
		}
	}

	// ==================================================================================================
	// ContactsAdapter
	// ==================================================================================================
	/**
	 * Cursor adapter.
	 */
	protected final class ContactsAdapter extends BaseContactsAdapter {

		/**
		 * Implement related function when the list need to be displayed in
		 * group.
		 */
		private class GroupMode implements ListDisplayMode {

			/** Total numbers of modes */
			private static final int MODE_NUMBERS = 4;

			@Override
			public int getItemViewType(int position) {
				if (mSectionMapper.isSection(position)) {
					if (mMode == MODE_CONTACTS) {
						return 0;
					} else if (mMode == MODE_EXTENSIONS) {
						return 1;
					}
				} else {
					if (mMode == MODE_CONTACTS) {
						return 2;
					} else if (mMode == MODE_EXTENSIONS) {
						return 3;
					}
				}

				return -1;
			}

			@Override
			public int getViewTypeCount() {
				return MODE_NUMBERS;
			}

			@Override
			public boolean isSection(int position) {
				if (mSectionMapper == null) {
					return false;
				}
				return mSectionMapper.isSection(position);
			}

		}

		/**
		 * Implement related function when the list need't to be displayed in
		 * group.
		 */
		private class UngroupMode implements ListDisplayMode {

			/** Total numbers of modes */
			private static final int MODE_NUMBERS = 2;

			@Override
			public int getItemViewType(int position) {
				if (mMode == MODE_CONTACTS) {
					return 0;
				}
				if (mMode == MODE_EXTENSIONS) {
					return 1;
				}

				return -1;
			}

			@Override
			public int getViewTypeCount() {
				return MODE_NUMBERS;
			}

			@Override
			public boolean isSection(int position) {
				return false;
			}

		}

		private ContactSectionMapper mSectionMapper;

		private ListDisplayMode mDisplayMode = null;

		private final ListDisplayMode mGroupMode = new GroupMode();

		private final ListDisplayMode mUngroupMode = new UngroupMode();

		/**
		 * Default constructor.
		 */
		public ContactsAdapter(Context context) {
			super(context, R.layout.contacts_list_item_photo);
			mDisplayMode = mGroupMode;
		}

		@Override
		public int getItemViewType(int position) {
			return mDisplayMode.getItemViewType(position);
		}

		@Override
		public int getViewTypeCount() {
			return mDisplayMode.getViewTypeCount();
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			LayoutInflater inflatter = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View view = inflatter.inflate(R.layout.contacts_list_item_photo, parent, false);
			View sectionItemLayout = view.findViewById(R.id.section_item);

			final SectionedContactListItemCache cache = new SectionedContactListItemCache();
			cache.sectionHeader = (TextView) view.findViewById(R.id.txtSectionHeader);
			cache.contactItemCache.nameView = (TextView) view.findViewById(R.id.name);
			cache.contactItemCache.typeView = (TextView) view.findViewById(R.id.type);
			cache.contactItemCache.presenceView = (ImageView) view.findViewById(R.id.presence);
			cache.contactItemCache.photoView = (ImageView) view.findViewById(R.id.photo);
			view.setTag(cache);

			if (mDisplayMode.isSection(cursor.getPosition())) {
				sectionItemLayout.setVisibility(View.VISIBLE);
			} else {
				sectionItemLayout.setVisibility(View.GONE);
			}
			if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
				cache.contactItemCache.typeView.setVisibility(View.VISIBLE);
				cache.contactItemCache.photoView.setVisibility(View.GONE);

			} else {
				cache.contactItemCache.typeView.setVisibility(View.GONE);
				cache.contactItemCache.photoView.setVisibility(View.VISIBLE);
			}
			return view;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			final SectionedContactListItemCache cache = (SectionedContactListItemCache) view.getTag();

			if (mDisplayMode.isSection(cursor.getPosition())) {
				cache.sectionHeader.setText(mSectionMapper.getSection(cursor.getPosition()));
				// Set section header unclickable
				cache.sectionHeader.setOnClickListener(null);
			}
			if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
				String name = cursor.getString(Extensions.EXT_NAME_COLUMN_INDEX);
				String number = cursor.getString(Extensions.EXT_PIN_COLUMN_INDEX);

				cache.contactItemCache.nameView.setText(name);
				cache.contactItemCache.typeView.setText(number);
			} else {
				String name = cursor.getString(Personal.NAME_COLUMN_INDEX);
				cache.contactItemCache.nameView.setText(name);

				// Set the photo, if requested
				if (mDisplayPhotos) {
					setContactPhoto(cursor, cache.contactItemCache, Personal.PHOTO_COLUMN_INDEX);
				}
			}
		}

		@Override
		public void changeCursor(Cursor c) {
			if (c == null) {
				mSectionMapper = null;
			}
			super.changeCursor(c);
		}

		public void changeSectionMapper(ContactSectionMapper sectionMapper) {
			mSectionMapper = sectionMapper;
		}

		public void changeDisplayMode(boolean isGroup) {
			if (isGroup)
				mDisplayMode = mGroupMode;
			else
				mDisplayMode = mUngroupMode;
		}

	}

	/**
	 * Process cursor to extract useful information into some data structure
	 */
	private ContactSectionMapper processCursor(Cursor c) {
		if (c == null || c.getCount() == 0 || c.isClosed())
			return null;

		/** define some variables */
		List<String> listData = new ArrayList<String>();
		HashMap<Integer, Integer> sectionPositionMap = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> positionSectionMap = new HashMap<Integer, Integer>();

		String curtitle = "";

		while (c.moveToNext()) {
			int position = c.getPosition();

			String curLetter = getTitle(getDisplayName(c));

			/** check if it is needed to insert a section title */
			if (TextUtils.isEmpty(curtitle) || !TextUtils.equals(curLetter, curtitle)) {
				sectionPositionMap.put(listData.size(), position);
				positionSectionMap.put(position, listData.size());
				curtitle = curLetter;
				listData.add(curtitle);
			}
		}

		ContactSectionMapper sectionMapper = new ContactSectionMapper(listData.toArray(new String[] {}),
				sectionPositionMap, positionSectionMap);
		return sectionMapper;
	}

	protected String getTitle(String displayName) {
		String title;
		/** check if the first letter is English letter */
		Matcher matcher = mPattern.matcher(displayName);
		if (!matcher.find()) {
			title = NONE_ENGLISH_LETTER_TITLE;
		} else {
			title = displayName.trim().substring(0, 1).toUpperCase();
		}
		return title;
	}

	protected String getDisplayName(Cursor c) {
		String displayName;
		/** get displayName */
		if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
			displayName = c.getString(Extensions.EXT_NAME_COLUMN_INDEX).trim();
		} else {
			displayName = c.getString(Personal.NAME_COLUMN_INDEX).trim();
		}
		return displayName;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		try {
			Cursor cursor = (Cursor) getListAdapter().getItem(position);
			if (cursor == null) {
				return;
			}
			if (mMode == MODE_EXTENSIONS) {
				FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, FlurryTypes.SCR_CONTACTS_EXTENSION);
				if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
					PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.CALL_FROM_CONTACTS);
				}
				callExtension(cursor);
				return;
			} else {
				int hasPhone = cursor.getInt(Personal.HAS_PHONE_COLUMN_INDEX);
				if (hasPhone != 0) {
					FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, FlurryTypes.SCR_CONTACTS_PERSONAL);
					List<Cont.acts.TaggedContactPhoneNumber> list = Cont.acts().getPersonalContactPhoneNumbers(this,
							cursor.getLong(Personal.ID_COLUMN_INDEX), false);
					if (list.size() != 0) {
						callContact(cursor, list);
						return;
					}
				}
				openContextMenu(v);
			}
		} catch (java.lang.Throwable error) {
			if (LogSettings.MARKET) {
				QaLog.e(TAG, " onListItemClick error: " + error.getMessage());
			}
		}
	}

	/**
	 * Query under personal mode
	 */
	protected final void queryPersonal() {
		if (LogSettings.ENGINEERING) {
			EngLog.w(TAG, "TODO: Fix this selection if neded");
		}

		mQueryHandler.startQuery(QUERY_TOKEN, null, ContactsUtils.Uri_People, Personal.CONTACTS_SUMMARY_PROJECTION,
				null, null, getSortOrder(People.DISPLAY_NAME));
	}

	/**
	 * Query under extension mode
	 */
	protected final void queryExtension() {
		Uri uri = UriHelper.getUri(RCMProvider.EXTENSIONS, RCMProviderHelper.getCurrentMailboxId(this));

		mQueryHandler.startQuery(QUERY_TOKEN, null, uri, Extensions.EXTENSION_SUMMARY_PROJECTION, null, null,
				getSortOrder(ExtensionsTable.RCM_DISPLAY_NAME));

	}

	/**
	 * switchToPersonal
	 */
	@Override
	protected void switchTab(boolean isToCompany) {
		if (!isToCompany && mMode != MODE_CONTACTS) {
			FlurryTypes.onEventScreen(FlurryTypes.EVENT_SCREEN_OPEN, FlurryTypes.SCR_CONTACTS_PERSONAL);
			mMode = MODE_CONTACTS;
			mBtnAddContact.setEnabled(true);
		} else if (isToCompany && mMode != MODE_EXTENSIONS) {
			mMode = MODE_EXTENSIONS;
			FlurryTypes.onEventScreen(FlurryTypes.EVENT_SCREEN_OPEN, FlurryTypes.SCR_CONTACTS_EXTENSION);
			mBtnAddContact.setEnabled(false);
		} else {
			return;
		}
		/**
		 * Set new Empty text to list
		 */
		setEmptyText(mMode);
		if (mAdapter.getCursor() != null) {
			mAdapter.getCursor().close();
		}

		/**
		 * Clear Photo cache
		 */
		mAdapter.mBitmapCache.clear();

		/**
		 * Get new data
		 */
		startQuery(mMode);
	}

	/**
	 * set image and text when list is empty
	 */
	@Override
	protected void setEmptyText(int mode) {
		int gravity = Gravity.CENTER;
		switch (mode) {
		case MODE_CONTACTS:
			mEmtytext.setText(getText(R.string.noContacts));
			mImgContact.setImageResource(R.drawable.ic_contacts_empty);
			break;
		case MODE_EXTENSIONS:
			mEmtytext.setText(getText(R.string.noExtensions));
			mImgContact.setImageResource(R.drawable.ic_contacts_empty);
			break;
		default:
			mEmtytext.setText(getText(R.string.noContacts));
			break;
		}
		mEmtytext.setGravity(gravity);
	}

	/**
	 * set context menu when personal mode
	 */
	@Override
	protected void setPersonalContextMenu(ContextMenu menu, Cursor cursor) {
		/**
		 * CONTACTS MODE
		 */
		int hasPhone = cursor.getInt(Personal.HAS_PHONE_COLUMN_INDEX);

		/**
		 * Call Contact
		 */
		if (hasPhone != 0) {
			List<Cont.acts.TaggedContactPhoneNumber> list = Cont.acts().getPersonalContactPhoneNumbers(this,
					cursor.getLong(Personal.ID_COLUMN_INDEX), false);
			if (list.size() != 0) {
				menu.add(0, MENU_ITEM_CALL, Menu.NONE, R.string.menu_call);
			}
		}

		/**
		 * View Contact
		 */
		menu.add(Menu.NONE, MENU_ITEM_VIEW, Menu.NONE, R.string.menu_viewContact);

		/**
		 * Add to Favorites/Remove from Favorites
		 */
		if (hasPhone != 0) {
			List<List<Cont.acts.TaggedContactPhoneNumber>> nums = PersonalFavorites.getPersonalFavorites(this,
					cursor.getLong(Personal.ID_COLUMN_INDEX));

			List<Cont.acts.TaggedContactPhoneNumber> favorites = nums.get(PersonalFavorites.NUMBERS_IN_FAVORITES);
			List<Cont.acts.TaggedContactPhoneNumber> not_favorites = nums
					.get(PersonalFavorites.NUMBERS_NOT_IN_FAVORITES);

			if (not_favorites.size() > 0) {
				menu.add(Menu.NONE, MENU_ITEM_TOGGLE_STAR, Menu.NONE, R.string.menu_addStar);
			} else if (favorites.size() > 0) {
				menu.add(Menu.NONE, MENU_ITEM_TOGGLE_STAR, Menu.NONE, R.string.menu_removeStar);
			}
		}

		/**
		 * Send Mail
		 */
		List<Cont.acts.EmailContact> emails = Cont.acts().getEmailAddresses(this,
				cursor.getLong(Personal.ID_COLUMN_INDEX));
		if (emails.size() > 0) {
			menu.add(0, MENU_ITEM_EMAIL, Menu.NONE, R.string.menu_sendEmail);
		}

		/**
		 * Edit Contact
		 */
		menu.add(Menu.NONE, MENU_ITEM_EDIT, Menu.NONE, R.string.menu_editContact);

		/**
		 * Delete Contact
		 */
		menu.add(Menu.NONE, MENU_ITEM_DELETE, Menu.NONE, R.string.menu_deleteContact);
	}

	/**
	 * Get the title of the context menu
	 */
	protected CharSequence getMenuTitle(AdapterView.AdapterContextMenuInfo info){
		final SectionedContactListItemCache views = (SectionedContactListItemCache) info.targetView.getTag();
		return views.contactItemCache.nameView.getText();
	}
	
	/**
	 * On selection context menu item.
	 */
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		try {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
			long mailboxId = RCMProviderHelper.getCurrentMailboxId(this);
			Cursor cursor = null;
			final Uri personUri;
			if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
				personUri = UriHelper.getUri(RCMProvider.EXTENSIONS, mailboxId, info.id);
				cursor = (Cursor) getListAdapter().getItem(info.position);
			} else {
				personUri = ContentUris.withAppendedId(ContactsUtils.Uri_People, info.id);
				cursor = (Cursor) getListAdapter().getItem(info.position);
			}

			if (cursor == null) {
				return false;
			}
			switch (item.getItemId()) {
			case MENU_ITEM_CALL: {
				return callItem(cursor);
			}
			case MENU_ITEM_EMAIL: {
				if (LogSettings.MARKET) {
					QaLog.d(TAG, "User: EMail");
				}
				long personalContactId = cursor.getLong(Personal.ID_COLUMN_INDEX);
				return sendEmail(cursor, personalContactId);
			}
			case MENU_ITEM_VIEW: {
				return viewItem(personUri);
			}
			case MENU_ITEM_TOGGLE_STAR: {
				return toggleItemFavorite(info, mailboxId, cursor);
			}
			case MENU_ITEM_EDIT: {
				return editItem(personUri);
			}
			case MENU_ITEM_DELETE: {
				return deleteItem(personUri);
			}
			}
		} catch (java.lang.Throwable error) {
			if (LogSettings.MARKET) {
				QaLog.e(TAG, " onContextItemSelected error: " + error.getMessage());
			}
		}
		return false;
	}

	/**
	 * call contact
	 */
	private boolean callItem(Cursor cursor) {
		if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
			if (LogSettings.MARKET) {
				QaLog.d(TAG, "User: Call extension");
			}
			FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, FlurryTypes.SCR_CONTACTS_EXTENSION);
			callExtension(cursor);

		} else {
			if (LogSettings.MARKET) {
				QaLog.d(TAG, "User: Call contact");
			}
			FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, FlurryTypes.SCR_CONTACTS_PERSONAL_CONTEXT);
			callContact(cursor, null);
		}
		return true;
	}

	/**
	 * view contact
	 */
	private boolean viewItem(final Uri personUri) {
		if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
			if (LogSettings.MARKET) {
				QaLog.d(TAG, "User: View extension");
			}
			FlurryTypes.onEvent(FlurryTypes.EVENT_CONTACTS_DETAILS_OPEN, FlurryTypes.SCR_CONTACTS_EXTENSION_DETAILS);
			if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
				PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.OPEN_CONTACT_DETAILS);
			}
			Intent intent = new Intent(this, ViewExtensionsActivity.class);
			intent.setData(personUri);

			try {
				startActivity(intent);
			} catch (ActivityNotFoundException e) {
				if (LogSettings.ENGINEERING) {
					EngLog.e(TAG, "viewItem()", e);
				} else if (LogSettings.MARKET) {
					QaLog.e(TAG, "viewItem(): " + e.getMessage());
				}
			}
		} else {
			if (LogSettings.MARKET) {
				QaLog.d(TAG, "User: View Contact");
			}
			Intent intent = new Intent(Intent.ACTION_VIEW, personUri);
			try {
				startActivityForResult(Intent.createChooser(intent, getString(R.string.menu_viewContact)),
						VIEW_CONTACT_REQUEST);
			} catch (android.content.ActivityNotFoundException e) {
				if (LogSettings.MARKET) {
					QaLog.e(TAG, "viewItem(): " + e.getMessage());
				}
			}
		}
		return true;
	}

	/**
	 * delete local contact
	 */
	private boolean deleteItem(final Uri personUri) {
		if (LogSettings.MARKET) {
			QaLog.d(TAG, "User: Delete");
		}
		RcAlertDialog.getBuilder(this).setTitle(R.string.menu_deleteContact)
				.setMessage(R.string.contacts_del_confirmation).setIcon(R.drawable.symbol_exclamation)
				.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						try {
							getContentResolver().delete(personUri, null, null);
						} catch (SQLiteException e) {
							if (LogSettings.QA) {
								QaLog.e(TAG, "Error deleting contact.", e);
							} else if (LogSettings.MARKET) {
								MktLog.e(TAG, "Cannot delete contact: SQLite error.");
							}

							showDialog(ERROR_DELETING_CONTACT_DIALOG_ID);
						}
					}
				}).setNegativeButton(R.string.dialog_btn_cancel, null).create().show();
		return true;
	}

	/**
	 * edit local contact
	 */
	private boolean editItem(final Uri personUri) {
		if (LogSettings.MARKET) {
			QaLog.d(TAG, "User: Edit");
		}
		Intent intent = new Intent(Intent.ACTION_EDIT, personUri);
		try {
			startActivityForResult(Intent.createChooser(intent, getString(R.string.menu_editContact)),
					EDIT_CONTACT_REQUEST);
		} catch (android.content.ActivityNotFoundException e) {
			if (LogSettings.ENGINEERING) {
				EngLog.e(TAG, "editItem()", e);
			} else if (LogSettings.MARKET) {
				QaLog.e(TAG, "editItem(): " + e.getMessage());
			}
		}
		return true;
	}

	protected void callContact(Cursor cursor, List<Cont.acts.TaggedContactPhoneNumber> list) {
		if (LogSettings.MARKET) {
			QaLog.d(TAG, "User: Call");
		}
		if (list == null) {
			if (cursor == null) {
				return;
			}
			boolean hasPhone = cursor.getInt(Personal.HAS_PHONE_COLUMN_INDEX) != 0;
			if (!hasPhone) {
				if (LogSettings.MARKET) {
					MktLog.w(TAG, "Error dialog: no valid phone numbers");
				}
				signalError(getString(R.string.contacts_no_valid_phone_numbers));
				return;
			}
			list = Cont.acts().getPersonalContactPhoneNumbers(this, cursor.getLong(Personal.ID_COLUMN_INDEX), false);
		}
		if (list.isEmpty()) {
			if (LogSettings.MARKET) {
				MktLog.w(TAG, "Error dialog: no valid phone numbers");
			}
			signalError(getString(R.string.contacts_no_valid_phone_numbers));
			return;
		} else if (list.size() == 1) {
			makeRingOut(list.get(0).cpn.phoneNumber, cursor.getString(Personal.NAME_COLUMN_INDEX));
			return;
		}
		String name = cursor.getString(Personal.NAME_COLUMN_INDEX);
		PhoneSelectionDialog dialog = new PhoneSelectionDialog(this, list, name, PhoneSelectionDialog.ACTION_CALL,
				false);
		dialog.show();
		return;
	}

	/**
	 * add contact to favorite or remove contact from favorite
	 */
	private boolean toggleItemFavorite(AdapterContextMenuInfo info, long mailboxId, Cursor cursor) {
		if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
			if (cursor.getInt(Extensions.EXT_STARRED_COLUMN_INDEX) == 0) {
				if (LogSettings.MARKET) {
					QaLog.d(TAG, "User: Extension: Set as Favorite");
				}
				FlurryTypes.onEventScreen(FlurryTypes.EVENT_ADD_FAVORITES, FlurryTypes.SCR_CONTACTS_EXTENSION_CONTEXT);
				CompanyFavorites.setAsFavorite(this, mailboxId, info.id);
			} else {
				if (LogSettings.MARKET) {
					QaLog.d(TAG, "User: Extension: UnSet as Favorite");
				}
				FlurryTypes.onEventScreen(FlurryTypes.EVENT_REMOVE_FAVORITES,
						FlurryTypes.SCR_CONTACTS_EXTENSION_CONTEXT);
				CompanyFavorites.setAsNotFavorite(this, mailboxId, info.id);
			}
		} else {
			if (LogSettings.MARKET) {
				QaLog.d(TAG, "User: Toggle Favorite");
			}
			return togglePersonalFavorites(cursor);
		}
		return true;
	}

	/**
	 * Toggle favorites.
	 * 
	 * @param cursor
	 *            the current cursor pointed to a contact
	 * @return
	 */
	protected boolean togglePersonalFavorites(Cursor cursor) {
		if (cursor == null) {
			return false;
		}

		List<List<TaggedContactPhoneNumber>> nums = PersonalFavorites.getPersonalFavorites(this,
				cursor.getLong(Personal.ID_COLUMN_INDEX));
		List<TaggedContactPhoneNumber> favorites = nums.get(PersonalFavorites.NUMBERS_IN_FAVORITES);
		List<TaggedContactPhoneNumber> not_favorites = nums.get(PersonalFavorites.NUMBERS_NOT_IN_FAVORITES);

		if (!not_favorites.isEmpty()) {
			FlurryTypes.onEventScreen(FlurryTypes.EVENT_ADD_FAVORITES, FlurryTypes.SCR_CONTACTS_PERSONAL_CONTEXT);
			if (not_favorites.size() == 1) {
				addToPersonalFavorites(not_favorites);
			} else {
				showAddSelectionDialog(not_favorites);
			}
			return true;
		} else if (!favorites.isEmpty()) {
			FlurryTypes.onEventScreen(FlurryTypes.EVENT_REMOVE_FAVORITES, FlurryTypes.SCR_CONTACTS_PERSONAL_CONTEXT);
			if (favorites.size() == 1) {
				deletePersonalFavorite(favorites);
			} else {
				showRemoveSelectionDialog(favorites);
			}
			return true;
		}

		signalError(getString(R.string.contacts_no_valid_phone_numbers));
		return false;
	}

	protected void addToPersonalFavorites(List<TaggedContactPhoneNumber> not_favorites) {
		TaggedContactPhoneNumber num = not_favorites.get(0);
		Cont.acts().addToPersonalFavorites(num.id, num.cpn.originalNumber, num.cpn.normalizedNumber, this);
	}

	protected void showAddSelectionDialog(List<TaggedContactPhoneNumber> not_favorites) {
		PhoneSelectionDialog dialog = new PhoneSelectionDialog(this, not_favorites, getString(R.string.menu_addStar),
				PhoneSelectionDialog.ACTION_ADD_TO_FAVORITE, false);
		dialog.show();
	}

	protected void deletePersonalFavorite(List<TaggedContactPhoneNumber> favorites) {
		TaggedContactPhoneNumber num = favorites.get(0);
		PersonalFavorites.deletePersonalFavorite(this, RCMProviderHelper.getCurrentMailboxId(this), num.id);
	}

	protected void showRemoveSelectionDialog(List<TaggedContactPhoneNumber> favorites) {
		PhoneSelectionDialog dialog = new PhoneSelectionDialog(this, favorites, getString(R.string.menu_removeStar),
				PhoneSelectionDialog.ACTION_REMOVE_FROM_FAVORITES, false);
		dialog.show();
	}

	/**
	 * add contact using local address list function
	 */
	private void addContact() {
		if (LogSettings.MARKET) {
			QaLog.d(TAG, "User: Add");
		}
		FlurryTypes.onEvent(FlurryTypes.EVENT_CREATE_NEW_CONTACT,
				(mMode == MODE_CONTACTS) ? FlurryTypes.SCR_CONTACTS_PERSONAL_OPTIONS
						: FlurryTypes.SCR_FAVORITES_PERSONAL_OPTIONS);
		Intent intent = new Intent(Intent.ACTION_INSERT, Cont.acts().getPeopleUri());
		intent.addCategory(Intent.CATEGORY_DEFAULT);
		try {
			startActivityForResult(Intent.createChooser(intent, getString(R.string.menu_newContact)),
					NEW_CONTACT_REQUEST);
		} catch (android.content.ActivityNotFoundException e) {
			if (LogSettings.ENGINEERING) {
				EngLog.e(TAG, "onOptionsItemSelected()", e);
			}
		}
	}

}