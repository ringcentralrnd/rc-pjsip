/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.SimpleAdapter;

import com.ringcentral.android.R;
import com.ringcentral.android.utils.EmailSender;

public class EmailSelectionDialog implements DialogInterface.OnClickListener, DialogInterface.OnDismissListener {

    private Context mContext;
    private AlertDialog mDialog;
    private SimpleAdapter mEmailAdapter;
    private List<HashMap<String, String>> mFillMaps;

    public EmailSelectionDialog(Context context, List<Cont.acts.EmailContact> emails) {
        mContext = context;
        String[] from = new String[] { 
                Mail.EMAIL, 
                Mail.TYPE };
        int[] to = new int[] { 
                android.R.id.text1, 
                android.R.id.text2 };
        
        mFillMaps = new ArrayList<HashMap<String, String>>();

        for (Cont.acts.EmailContact e : emails) {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put(Mail.EMAIL, e.emailAddress);
            map.put(Mail.TYPE, e.emailTag);
            mFillMaps.add(map);
        }
        mEmailAdapter = new SimpleAdapter(mContext, mFillMaps, R.layout.simple_list_item_2, from, to);
        AlertDialog.Builder dialogBuilder = 
            RcAlertDialog.getBuilder(mContext).setAdapter(mEmailAdapter, this).setTitle(
                context.getString(R.string.contacts_send_email_selection_title));
        mDialog = dialogBuilder.create();
    }

    public void show() {
        if (mFillMaps.size() == 0) {
            onClick(mDialog, 0);
            return;
        }
        mDialog.show();
    }

    public void onClick(DialogInterface dialog, int which) {
        EmailSender emailSender = new EmailSender(mContext);
        String[] to = { mFillMaps.get(which).get(Mail.EMAIL) };
        emailSender.sendEmail(to, "", "", null);
    }

    public void onDismiss(DialogInterface dialog) {
        mContext = null;
        mDialog = null;
        mEmailAdapter = null;
        mFillMaps = null;
    }

    private class Mail {
        public static final String EMAIL = "EMAIL";
        public static final String TYPE = "TYPE";
    }
}
