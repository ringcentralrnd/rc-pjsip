/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.phoneparser.PhoneNumberFormatter;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.provider.RCMDataStore.ExtensionsTable;
import com.ringcentral.android.utils.PhoneUtils;

/**
 * Contacts access proxy resolver depending on the underlying platform version.
 */
public class Cont {
    /**
     * Keeps the contacts proxy instance.
     */
    private static ContactProxy s_proxy;

    /**
     * Sync-primitive.
     */
    private static final Object s_lock = new Object();

    /**
     * Returns Contacts proxy depending on the underlying platform version.
     * 
     * @return the Contacts proxy depending on the underlying platform version.
     */
    public static ContactProxy acts() {
        synchronized (s_lock) {
            if (s_proxy != null) {
                return s_proxy;
            }

            String name;

            int sdkVersion = Integer.parseInt(Build.VERSION.SDK);

            if (sdkVersion < 5) { // Build.VERSION_CODES.ECLAIR
                name = "ContactProxy1x";
            } else {
                name = "ContactProxy20";
            }

            if (LogSettings.QA) {
                QaLog.i("[RC] Contacts", "Contacts proxy class: " + name);
            }

            try {
                Class<? extends ContactProxy> clazz = Class.forName(
                        ContactProxy.class.getPackage().getName() + "." + name).asSubclass(ContactProxy.class);
                s_proxy = clazz.newInstance();
            } catch (Exception e) {
                throw new IllegalStateException("Error platform contacts access: " + e.getMessage());
            }
            return s_proxy;
        }
    }

    /**
     * Validate phone number.
     * 
     * @param phoneNumber
     *            the number to be validated
     */
    static void validatePhoneNumber(String phoneNumber) throws IllegalArgumentException {
        if (phoneNumber == null || phoneNumber.trim().length() == 0
                || PhoneNumberFormatter.trimNonNumberSymbols(phoneNumber).trim().length() == 0) {
            throw new IllegalArgumentException("Invalid phone number");
        }
    }

    public static final class acts {
        /**
         * Define logging tag.
         */
        private static final String LOG_TAG = "[RC] Cont.acts";
        
        /**
         * Returns display name for certain phone number.
         * 
         * @param context
         *            the execution context
         * @param number
         *            the phone number to lookup
         * @return the display name
         */
        public static final String bindContactNameByNumber(Context context, final String number) {
            ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumber(number);

            if (!cpn.isValid) {
                return null;
            }

            if (cpn.phoneNumber.isShortNumber) {
                Cursor c = null;
                try {
                    Uri uri = UriHelper.getUri(RCMProvider.EXTENSIONS, RCMProviderHelper.getCurrentMailboxId(context));
                    c = context.getContentResolver().query(uri, FIND_COMPANY_CONTACT_BY_NUMBER_PROJECTION,
                            ExtensionsTable.JEDI_PIN + " = ?", new String[] { cpn.normalizedNumber }, null);
                    if (c != null) {
                        if (c.moveToFirst()) {
                            String name = c.getString(0);
                            c.close();
                            return name;
                        }
                        c.close();
                    }
                } catch (java.lang.Throwable error) {
                    if (LogSettings.MARKET) {
                        MktLog.e(LOG_TAG, "bindContactNameByNumber error: " + error.getMessage());
                    }
                } finally {
                    if (c != null) {
                        c.close();
                    }
                }
            }

            return acts().lookupContactNameByPhoneNumber(cpn.normalizedNumber, context);
        }

        private final static String[] FIND_COMPANY_CONTACT_BY_NUMBER_PROJECTION = { ExtensionsTable.RCM_DISPLAY_NAME };

        /**
         * Bind synchronization.
         * 
         * @param context
         *            the execution context
         * @param mailboxId
         *            the record identifier
         * @param bind
         *            the initial bind
         * @return
         */
        public static BindSync syncBind(Context context, long mailboxId, ContactBinding bind) {
            BindSync sync = new BindSync();
            sync.bind = bind;
            if (!bind.isValid) {
                sync.syncState = BindSync.State.INVALID_RECORD;
                return sync;
            }
            ContactBinding cleanBind = new ContactBinding();
            cleanBind.hasContact = false;
            cleanBind.phoneId = -1;
            cleanBind.isValid = bind.isValid;
            cleanBind.cpn = bind.cpn;
            cleanBind.originalNumber = bind.originalNumber;

            Cursor c = null;
            if (bind.hasContact) {
                if (bind.isPersonalContact) {
                    return Cont.acts().syncPersonalBind(context, mailboxId, sync);
                } else {
                    try {
                        Uri uri = UriHelper.getUri(RCMProvider.EXTENSIONS, mailboxId);
                        c = context.getContentResolver().query(uri, SYNC_COMPANY_CONTACT_BIND_PROJECTION,
                                ExtensionsTable._ID + " = " + bind.phoneId, null, null);
                        boolean found = false;
                        String displayName = null;
                        String bindNumber = null;
                        boolean isFavorite = false;
                        if (c != null) {
                            if (c.moveToFirst()) {
                                displayName = c.getString(SYNC_BIND_RCM_DISPLAY_NAME_INDX);
                                bindNumber = c.getString(SYNC_BIND_JEDI_PIN_INDX);
                                isFavorite = (c.getLong(SYNC_BIND_RCM_STARRED_INDX) > 0);
                                found = true;
                            }
                        }

                        if (found) {
                            if (compareStrings(bindNumber, bind.phoneNumber)) {
                                bind.isDefaultPhoneNumber = true;
                                bind.isPhoneNumberFavorite = isFavorite;
                                bind.phoneNumberTag = context.getString(R.string.phone_tag_extension);
                                if (!compareStrings(displayName, bind.displayName)) {
                                    bind.displayName = displayName;
                                    sync.syncState = BindSync.State.UPDATED;
                                } else {
                                    sync.syncState = BindSync.State.NOT_CHANGED;
                                }
                                return sync;
                            }
                        }
                    } catch (java.lang.Throwable error) {
                        if (LogSettings.MARKET) {
                            MktLog.w(LOG_TAG, "syncBind (company) failed: " + error.getMessage());
                        }
                    } finally {
                        if (c != null) {
                            c.close();
                        }
                    }
                }

                ContactBinding newBind = bindContactByNumber(context, mailboxId, bind.originalNumber);
                if (newBind.isValid) {
                    if (newBind.hasContact) {
                        sync.bind = newBind;
                        sync.syncState = BindSync.State.REBIND;
                        return sync;
                    }
                }

                sync.syncState = BindSync.State.GONE;
                sync.bind = cleanBind;
                return sync;
            }

            /**
             * Has not contact.
             */
            ContactBinding newBind = bindContactByNumber(context, mailboxId, bind.originalNumber);
            if (newBind.isValid) {
                if (newBind.hasContact) {
                    sync.bind = newBind;
                    sync.syncState = BindSync.State.BIND;
                    return sync;
                }
            }

            sync.syncState = BindSync.State.NOT_CHANGED;
            return sync;
        }

        private final static String[] SYNC_COMPANY_CONTACT_BIND_PROJECTION = {
                ExtensionsTable.RCM_DISPLAY_NAME, 
                ExtensionsTable.JEDI_PIN, 
                ExtensionsTable.RCM_STARRED };
        private static final int SYNC_BIND_RCM_DISPLAY_NAME_INDX = 0;
        private static final int SYNC_BIND_JEDI_PIN_INDX = 1;
        private static final int SYNC_BIND_RCM_STARRED_INDX = 2;

        /**
         * Bind synchronization.
         */
        public static class BindSync {
            public static enum State {
                INVALID_RECORD, NOT_CHANGED, UPDATED, BIND, REBIND, GONE
            }

            public State syncState = State.INVALID_RECORD;
            public ContactBinding bind;
        }

        /**
         * Binds company contact or personal contact (phoneId) by the phone
         * number.
         * 
         * @param context
         *            the execution context
         * @param number
         *            the phone number
         * @return the binding
         */
        public static final ContactBinding bindContactByNumber(Context context, long mailboxId, final String number) {
            ContactBinding bind = new ContactBinding();
            bind.originalNumber = number;
            bind.cpn = PhoneUtils.getContactPhoneNumber(number);
            bind.isValid = bind.cpn.isValid;

            if (bind.isValid) {
                if (bind.cpn.phoneNumber.isShortNumber) {
                    Cursor c = null;
                    try {
                        Uri uri = UriHelper.getUri(RCMProvider.EXTENSIONS, mailboxId);
                        c = context.getContentResolver().query(uri, FIND_COMPANY_CONTACT_BY_NUMBER_PROJECTION2,
                                ExtensionsTable.JEDI_PIN + " = ?", new String[] { bind.cpn.normalizedNumber }, null);
                        if (c != null) {
                            if (c.moveToFirst()) {
                                bind.displayName = c.getString(RCM_DISPLAY_NAME_INDX2);
                                bind.isPersonalContact = false;
                                bind.hasContact = true;
                                bind.phoneId = c.getLong(_ID_INX2);
                                bind.phoneNumber = c.getString(JEDI_PIN_INDX2);
                                bind.isDefaultPhoneNumber = true;
                                bind.phoneNumberTag = context.getString(R.string.phone_tag_extension);
                                bind.isPhoneNumberFavorite = (c.getInt(RCM_STARRED_INDX2) != 0);
                                return bind;
                            }
                        }
                    } catch (java.lang.Throwable error) {
                        if (LogSettings.MARKET) {
                            MktLog.e(LOG_TAG, "bindContactByNumber error: " + error.getMessage());
                        }
                    } finally {
                        if (c != null) {
                            c.close();
                        }
                    }
                }
            }
            return acts().bindPersonalContact(context, mailboxId, bind);
        }

        private final static String[] FIND_COMPANY_CONTACT_BY_NUMBER_PROJECTION2 = { 
                ExtensionsTable._ID,
                ExtensionsTable.RCM_DISPLAY_NAME, 
                ExtensionsTable.JEDI_PIN,
                ExtensionsTable.RCM_STARRED};
        private static final int _ID_INX2 = 0;
        private static final int RCM_DISPLAY_NAME_INDX2 = 1;
        private static final int JEDI_PIN_INDX2 = 2;
        private static final int RCM_STARRED_INDX2 = 3;

        public static final class ContactBinding {
            /**
             * Keeps original number.
             */
            public String originalNumber;

            /**
             * Defines if the number is valid.
             */
            public boolean isValid;

            /**
             * Keeps normalized phone number.
             */
            public ContactPhoneNumber cpn;

            /**
             * Defines if the <code>number</code> has binding to a contact.
             */
            public boolean hasContact;

            /**
             * Defines if the binding exists in company contacts otherwise in
             * the personal contacts. Valid value when <code>hasContact</code>
             * is <code>true</code>.
             */
            public boolean isPersonalContact;

            /**
             * Keeps display name for the binding. Valid value when
             * <code>hasContact</code> is <code>true</code>.
             */
            public String displayName;

            /**
             * Keeps bind identifier.
             */
            public long phoneId;

            /**
             * Keeps original number.
             */
            public String phoneNumber;

            /**
             * Keeps phone number tag.
             */
            public String phoneNumberTag;

            /**
             * Defines if the contact number is primary / default.
             */
            public boolean isDefaultPhoneNumber;

            /**
             * Defines if the contact number a favorite.
             */
            public boolean isPhoneNumberFavorite;

            /**
             * Log representation.
             */
            public String toString() {
                try {
                    if (LogSettings.QA) {
                        StringBuffer sb = new StringBuffer("ContactBinding(" + originalNumber + ") ");

                        if (isValid) {
                            if (hasContact) {
                                sb.append("; number=");
                                sb.append(cpn.normalizedNumber);
                                sb.append("company=");
                                sb.append(!isPersonalContact);
                                sb.append("; bindId=");
                                sb.append(phoneId);
                                sb.append("; bindName=");
                                sb.append(displayName);
                                sb.append("; bindNumber=");
                                sb.append(phoneNumber);
                                sb.append(";");
                            } else {
                                sb.append("no contacts");
                            }
                        } else {
                            sb.append("not valid");
                        }
                        return sb.toString();
                    }
                } catch (Exception ex) {
                }
                return originalNumber;
            }
        }

        public static class TaggedContactPhoneNumber {
            /**
             * Keeps id.
             */
            public long id;

            /**
             * Keeps normalized phone number.
             */
            public ContactPhoneNumber cpn;

            /**
             * Keeps phone number tag.
             */
            public String numberTag;

            /**
             * Defines if the contact number is primary / default.
             */
            public boolean isDefault;
        }

        /**
         * Represents contact phone.
         */
        public static final class PhoneContact {
            /**
             * Phone contact id.
             */
            public long phoneId;

            /**
             * Contact id.
             */
            public long contactId;

            /**
             * Photo associated with the contact (-1 if not exists)
             */
            public long photoId;

            /**
             * Displayed label.
             */
            public String displayName;

            /**
             * The number.
             */
            public String number;

            /**
             * The type (tag) of the number.
             */
            public int type;

            /**
             * Keeps label (custom number type)
             */
            public String label;
        }

        
        public final static class EmailContact {
            public long contactId;
            public String displayName;
            public String emailAddress;
            public String emailTag;
        }
        
        
        public static boolean compareStrings(String str1, String str2) {
            if (str1 == null) {
                if (str2 != null) {
                    return false;
                }
                return true;
            } else {
                if (str2 == null) {
                    return false;
                }
                return str1.equalsIgnoreCase(str2);
            }
        }
        
        /**
         * Trace BindSync.
         * 
         * @param context
         *            the execution context
         * @param bsync
         *            the bind sync
         * @param logTag
         *            the logging tag
         * @param recordId
         *            the record identifier
         */
        public static void bindSyncTrace(Context context, BindSync bsync, String logTag, String logTag2, long recordId) {
            if (LogSettings.MARKET) {
                try {
                    BindSync.State state =  bsync.syncState;
                    StringBuffer sb = new StringBuffer(logTag2);
                    sb.append(":BindSync:id=");
                    sb.append(recordId);
                    sb.append(':');
                    if (state == BindSync.State.INVALID_RECORD) {
                        sb.append("INVALID_RECORD");
                    } else if (state == BindSync.State.GONE) {
                        sb.append("GONE:");
                        sb.append(bsync.bind.originalNumber);
                    } else if (state == BindSync.State.BIND) {
                        if (LogSettings.ENGINEERING) {
                            sb.append("BIND:");
                            sb.append(bsync.bind.originalNumber);
                            if (bsync.bind.isPersonalContact) {
                                sb.append(":Personal:");
                            } else {
                                sb.append(":Company:");
                            }
                            sb.append(bsync.bind.phoneId);
                        }
                    } else if (state == BindSync.State.UPDATED) {
                        if (LogSettings.ENGINEERING) {
                            sb.append("UPDATED:");
                            sb.append(bsync.bind.originalNumber);
                            if (bsync.bind.isPersonalContact) {
                                sb.append(":Personal:");
                            } else {
                                sb.append(":Company:");
                            }
                            sb.append(bsync.bind.phoneId);
                        }
                    } else if (state == BindSync.State.REBIND) {
                        sb.append("REBIND:");
                        sb.append(bsync.bind.originalNumber);
                        if (bsync.bind.isPersonalContact) {
                            sb.append(":Personal:");
                        } else {
                            sb.append(":Company:");
                        }
                        sb.append(bsync.bind.phoneId);
                    } 
                    
                    if (LogSettings.MARKET) {
                        MktLog.d(logTag, sb.toString());
                    }
                } catch (Exception ex) {
                }
            }
        }
        
    }
}
