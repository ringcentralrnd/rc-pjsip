/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ActivityNotFoundException;
import android.content.AsyncQueryHandler;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.api.RequestInfoStorage;
import com.ringcentral.android.api.network.NetworkManager;
import com.ringcentral.android.api.network.NetworkManagerNotifier;
import com.ringcentral.android.api.pojo.CallLogPOJO;
import com.ringcentral.android.api.pojo.CallLogsListPOJO;
import com.ringcentral.android.calllog.CallLogQuery;
import com.ringcentral.android.calllog.CallLogSync;
import com.ringcentral.android.contacts.Cont.acts.BindSync;
import com.ringcentral.android.contacts.Cont.acts.ContactBinding;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMDataStore.CallLogTable;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.ringout.RingOutAgent;
import com.ringcentral.android.settings.DNDialog;
import com.ringcentral.android.settings.SettingsLauncher;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.LabelsUtils;
import com.ringcentral.android.utils.LogsUtils;
import com.ringcentral.android.utils.NetworkUtils;
import com.ringcentral.android.utils.UserInfo;

public class CallLogListActivity extends ListActivity implements View.OnCreateContextMenuListener, NetworkManagerNotifier {

	private static final String TAG = "[RC] CallLogListActivity";
	private static final int MENU_ITEM_VIEW = 1;
	private static final int MENU_ITEM_CALL = 2;
	private static final int MENU_ITEM_CREATE_NEW_CONTACT = 3;
	private static final int MENU_ITEM_ADD_TO_EXISTING_CONTACT = 4;
	private static final int MENU_ITEM_ADD_TO_FAVORITES = 5;
	private static final int MENU_ITEM_REFRESH = 6;
	private static final int MENU_ITEM_REMOVE_FROM_FAVORITES = 7;

	private static final int QUERY_TOKEN = 53;
	private int mMode = MODE_DEFAULT;
	private static final int MODE_ALL = 1;
	private static final int MODE_MISSED = 2;
	private static final int MODE_DEFAULT = MODE_ALL;
	private ToggleButton btnLeft;
	private ToggleButton btnRight;
	private Button btnClear;

	    
	
	private LogsUtils util;

	/**
	 * Keeps query handler.
	 */
	private QueryHandler mQueryHandler;

	/**
	 * Keeps the cursor adapter.
	 */
	private CallLogListAdapter mAdapter;

	/**
	 * Flag which indicate first start
	 */
	private boolean mJustCreated;

	/**
	 * Keeps RO agent.
	 */
	private RingOutAgent mRingOutAgent;

	/**
	 * Keeps the local synchronization service handle.
	 */
	private LocalSyncService.LocalSyncBinder syncService;

	    
	/**
	 * Keeps the connection to the local synchronization service.
	 */
	private ServiceConnection syncSvcConn = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder rawBinder) {
			syncService = (LocalSyncService.LocalSyncBinder) rawBinder;
			if (LogSettings.ENGINEERING) {
				EngLog.d(TAG, "Sync service connected.");
			}
			try {
				syncService.syncCallLog(RCMProviderHelper.getCurrentMailboxId(syncService.getService()));
			} catch (java.lang.Throwable error) {
				if (LogSettings.MARKET) {
					QaLog.e(TAG, "onServiceConnected() call sync failed: ", error);
				}
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			syncService = null;
			if (LogSettings.MARKET) {
				EngLog.d(TAG, "Sync service disconnected.");
			}
		}
	};

	/**
	 * Defines if the progress bar is shown.
	 */
	private boolean mCallLogLoadingViewVisiable = true;

	/**
	 * Defines if "Call Log empty" views are shown.
	 */
	private boolean mCallLogEmptyVisiable = false;

	/**
	 * Defines if the activity is active.
	 */
	private boolean mActive = true;

	/**
	 * Keeps loading (progress bar) view.
	 */
	private View mLoadingView;

	/**
	 * Keeps "empty" image view.
	 */
	private View mEmptyImageView;

	/**
	 * Keeps "empty" label view.
	 */
	private View mEmptyTextView;

	@Override
	protected void onCreate(Bundle state) {
		super.onCreate(state);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.call_log);
		initSabTabs();
		initClearButton();
		initTitle();

		util = new LogsUtils(getBaseContext());
		mQueryHandler = new QueryHandler(this);
		mAdapter = new CallLogListAdapter();
		setListAdapter(mAdapter);
		mJustCreated = true;
		registerForContextMenu(getListView());
		mRingOutAgent = new RingOutAgent(this);
		mLoadingView = findViewById(R.id.calllog_loading);
		mEmptyImageView = findViewById(R.id.call_log_list_empty_image);
		mEmptyTextView = findViewById(R.id.call_log_list_empty_text);
		getApplicationContext().bindService(new Intent(this, LocalSyncService.class), syncSvcConn, BIND_AUTO_CREATE);
	}

	private void initTitle() {
		TextView title = (TextView) findViewById(R.id.title);
		title.setText(R.string.tab_name_recents);
	}

	private void initClearButton() {
		this.btnClear = (Button) findViewById(R.id.btnTopRight);
		this.btnClear.setVisibility(View.INVISIBLE);
		this.btnClear.setText(R.string.clear);
	}

	@Override
	protected void onResume() {
		refreshCallLogRequestOnUserActivity();
		super.onResume();

		FlurryTypes.onEventScreen(FlurryTypes.EVENT_SCREEN_OPEN, mMode == MODE_ALL ? FlurryTypes.SCR_CALL_LOG_ALL : FlurryTypes.SCR_CALL_LOG_MISSED);

		if (mJustCreated) {
			startQuery();
		}
		mJustCreated = false;
		if (syncService != null) {
			try {
				syncService.syncCallLog(RCMProviderHelper.getCurrentMailboxId(this));
			} catch (java.lang.Throwable error) {
				if (LogSettings.MARKET) {
					MktLog.e(TAG, "onResume() call sync failed: ", error);
				}
			}
		}
	}

	@Override
	public void onContentChanged() {
		super.onContentChanged();
	}

	@Override
	public ListView getListView() {
		if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
			ListView listView = super.getListView();
			listView.setOnScrollListener(new OnScrollListener() {

				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
					if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED && totalItemCount > 3 * RCMConstants.CALL_LOG_PAGE_SIZE) {
						PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.CALL_LOG_SWITCH);
					}
				}
			});

			return listView;
		} else {
			return super.getListView();
		}
	}

	private void refreshCallLogRequestOnUserActivity() {
		try {
			NetworkManager.getInstance().refreshCallLog(this, RCMProviderHelper.getCurrentMailboxId(this));
		} catch (java.lang.Throwable error) {
		}
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		if (TextUtils.isEmpty(getListView().getTextFilter())) {
			startQuery();
		} else {
			((CallLogListAdapter) getListAdapter()).onContentChanged();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mActive = false;
		try {
			mLoadingHandler.reset();
		} catch (java.lang.Throwable error) {
			if (LogSettings.MARKET) {
				QaLog.e(TAG, "onDestroy() error: " + error.getMessage());
			}
		}
		try {
			Cursor cursor = mAdapter.getCursor();
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
			}
		} catch (java.lang.Throwable error) {
			if (LogSettings.MARKET) {
				QaLog.e(TAG, "onDestroy1() error: " + error.getMessage());
			}
		}
		try {
			if (syncService != null) {
				getApplicationContext().unbindService(syncSvcConn);
				syncService = null;
				syncSvcConn = null;
			}
		} catch (java.lang.Throwable error) {
			if (LogSettings.MARKET) {
				QaLog.e(TAG, "onDestroy2() error: " + error.getMessage());
			}
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryTypes.onStartSession(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryTypes.onEndSession(this);
	}

	/**
	 * Item view holder.
	 */
	private static final class CallLogListItemCache {
		public TextView labelView;
		public TextView timeView;
		public TextView whenView;
		public ImageView iconView;
		public int iconViewType = LogsUtils.INVALID_CALL_LOG_ICON;
		public ProgressBar progressView;
		public TextView get_more_records;
		public TextView refresh;
		public boolean hasMoreRecordsItem;
		public boolean aligned;
		public boolean progressBarVisiable;
		public ContactPhoneNumber cpn;
		public TextView locationView;
		public ImageView detailItemIcon;
	}

	/**
	 * List adapter.
	 */
	private final class CallLogListAdapter extends ResourceCursorAdapter {
		public CallLogListAdapter() {
			super(CallLogListActivity.this, R.layout.call_log_list_item, null);
		}

		@Override
		protected void onContentChanged() {
			startQuery();
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			final View view = super.newView(context, cursor, parent);
			final CallLogListItemCache cache = new CallLogListItemCache();
			cache.labelView = (TextView) view.findViewById(R.id.call_log_list_item_label);
			cache.whenView = (TextView) view.findViewById(R.id.call_log_list_item_when);
			cache.iconView = (ImageView) view.findViewById(R.id.call_log_list_item_icon);
			cache.progressView = (ProgressBar) view.findViewById(R.id.call_log_list_item_progress_small);
			cache.get_more_records = (TextView) view.findViewById(R.id.call_log_list_item_get_more_records);
			cache.refresh = (TextView) view.findViewById(R.id.call_log_list_item_refresh);
			cache.locationView = (TextView) view.findViewById(R.id.call_log_list_item_locationAndTime);
			cache.detailItemIcon = (ImageView) view.findViewById(R.id.call_log_detail_item_icon);			
			view.setTag(cache);
			return view;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			try {
				final CallLogListItemCache itemView = (CallLogListItemCache) view.getTag();
				final Long callLogId = cursor.getLong(CallLogQuery.CALLLOG_ID_INDX);
				final long mailboxId = Long.parseLong(cursor
				.getString(CallLogQuery.CALLLOG_MAILBOX_INDX));
				ContactBinding bind = CallLogQuery.readSyncBind(
				CallLogListActivity.this, cursor);
				final boolean isKnownContact = bind.hasContact;
			
			    final CallLogQuery.CallLogItem ci = CallLogQuery.readCallLogItem(CallLogListActivity.this, cursor);
				if (!itemView.aligned) {
					// itemView.labelView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
					itemView.progressView.setVisibility(View.INVISIBLE);
					itemView.progressBarVisiable = false;
				}

				if (itemView.progressBarVisiable) {
					itemView.progressView.setVisibility(View.INVISIBLE);
					itemView.progressBarVisiable = false;
				}

				if (cursor.getLong(CallLogQuery.CALLLOG_HAS_MORE_RECORD_ITEM_INDX) != 0) {

					if (!itemView.hasMoreRecordsItem || !itemView.aligned) {
						itemView.get_more_records.setText(context.getString(R.string.calllog_get_more_records));
						itemView.get_more_records.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
						itemView.get_more_records.setTextColor(Color.GRAY);
						itemView.get_more_records.setVisibility(View.VISIBLE);
						itemView.refresh.setVisibility(View.INVISIBLE);
						itemView.labelView.setVisibility(View.INVISIBLE);
						itemView.whenView.setVisibility(View.INVISIBLE);
						itemView.iconView.setVisibility(View.INVISIBLE);
						itemView.locationView.setVisibility(View.INVISIBLE);
						itemView.detailItemIcon.setVisibility(View.INVISIBLE);
						itemView.aligned = true;
					}
					itemView.hasMoreRecordsItem = true;
				} else {
					boolean align = false;
					if (itemView.hasMoreRecordsItem || !itemView.aligned) {
						itemView.aligned = true;
						align = true;
					}
					itemView.hasMoreRecordsItem = false;
					//CallLogQuery.CallLogItem ci = CallLogQuery.readCallLogItem(context, cursor);

					// if (align) {
					// itemView.labelView.setGravity(Gravity.CENTER_VERTICAL);
					// }

					if (ci.callStatus == CallLogPOJO.CALLSTATUS_MISSED || ci.callStatus == CallLogPOJO.CALLSTATUS_VOICEMAIL) {
						itemView.labelView.setTextColor(getResources().getColor(R.color.call_log_label_missed));
					} else {
						itemView.labelView.setTextColor(getResources().getColor(R.color.call_log_label_accept));
					}

					itemView.cpn = ci.cpn;

					// if (align) {
					// itemView.timeView.setGravity(Gravity.NO_GRAVITY);
					// }

					itemView.whenView.setText(LabelsUtils.getRelativeDateLabel(ci.date));
					setCallItemIcon(itemView, ci.direction, ci.callType, ci.callStatus);
					itemView.labelView.setText(ci.displayName);
					String time = " (" + LogsUtils.getLengthLabel(ci.length) + ")";
					itemView.locationView.setText(ci.getPhoneTypeOrLocation() + time);

					if (align) {
						itemView.refresh.setVisibility(View.INVISIBLE);
						itemView.get_more_records.setVisibility(View.INVISIBLE);
						itemView.labelView.setVisibility(View.VISIBLE);
						itemView.whenView.setVisibility(View.VISIBLE);
						itemView.iconView.setVisibility(View.VISIBLE);
						itemView.locationView.setVisibility(View.VISIBLE);
						itemView.detailItemIcon.setVisibility(View.VISIBLE);
					}
				}
				/**
				 * Hide loading bar when bind ready view
				 */
				
				
				if (mCallLogLoadingViewVisiable) {
					findViewById(R.id.calllog_loading).setVisibility(View.GONE);
					mCallLogLoadingViewVisiable = false;
				}
				
				/**
				 *  if Unknown/Blocked caller ID
				 */
				
				if (!bind.isValid ) {
					itemView.detailItemIcon.setVisibility(View.INVISIBLE);
	             } else {
	            	 itemView.detailItemIcon.setVisibility(View.VISIBLE);
	             }
				
				itemView.detailItemIcon.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							
					        Uri uri = UriHelper.getUri(RCMProvider.CALL_LOG, RCMProviderHelper.getCurrentMailboxId(CallLogListActivity.this), callLogId);
					        Intent intent = new Intent(CallLogListActivity.this, EventDetailActivity.class);
					        intent.setData(uri);
							intent.putExtra(RCMConstants.ISKNOWNCONTACT, isKnownContact);
							
					        try {
					            startActivity(intent);
					        } catch (ActivityNotFoundException e) {
					            if (LogSettings.ENGINEERING) {
					                EngLog.e(TAG, "showEventDetail()", e);
					            }
					        }
							
						}
					});
			} catch (java.lang.Throwable error) {
				if (LogSettings.MARKET) {
					QaLog.e(TAG, "bindView() error: " + error.getMessage());
				}
			}
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			return super.getView(position, convertView, parent);
		}

		private void setCallItemIcon(CallLogListItemCache cache, int direction, int callType, int callStatus) {
			try {
				cache.iconViewType = util.setCallLogListIconDrawable(direction, callType, callStatus, cache.iconView, cache.iconViewType);
			} catch (java.lang.Throwable error) {
				if (LogSettings.MARKET) {
					QaLog.e(TAG, "setCallItemIcon() error: " + error.getMessage());
				}
			}
		}
	}

	private void startQuery() {
		mQueryHandler.cancelOperation(QUERY_TOKEN);
		Uri uri = UriHelper.getUri(RCMProvider.CALL_LOG, RCMProviderHelper.getCurrentMailboxId(this));
		switch (mMode) {
		case MODE_ALL: {
			mQueryHandler.startQuery(QUERY_TOKEN, null, uri, CallLogQuery.CALLLOG_SUMMARY_PROJECTION, CallLogTable.RCM_FOLDER + "=" + CallLogTable.LOGS_ALL, null, CallLogTable.JEDI_START + " DESC");
			break;
		}
		case MODE_MISSED: {
			mQueryHandler.startQuery(QUERY_TOKEN, null, uri, CallLogQuery.CALLLOG_SUMMARY_PROJECTION, CallLogTable.RCM_FOLDER + "=" + CallLogTable.LOGS_MISSED, null, CallLogTable.JEDI_START + " DESC");
			break;
		}

		}
	}

	public NetworkManagerNotifier getNotifier() {
		return this;
	}

	public void statusChanged(Bundle status, Object result) {
		if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
			try {
				int req_status = status.getInt(NetworkManager.REQUEST_INFO.STATUS);
				if (req_status != RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED) {
					return;
				}

				int request_type = status.getInt(NetworkManager.REQUEST_INFO.TYPE);
				if (LogSettings.ENGINEERING) {
					EngLog.d(TAG, "statusChanged(): request_type = " + request_type);
				}
				if (request_type == RequestInfoStorage.GET_ALL_CALL_LOGS || request_type == RequestInfoStorage.GET_MISSED_CALL_LOGS) {
					if (getNewRecordsCount(result) >= RCMConstants.CALL_LOG_PAGE_SIZE - 1) {
						PerformanceMeasurementLogger.getInstance().stop(PerformanceMeasurementLogger.CALLLOG_NET_DOWNLOAD);
					}
				}
			} catch (Exception e) {
				if (LogSettings.MARKET) {
					MktLog.w(TAG, "statusChanged():ex:" + e);
				}
			}
		}
	}

	private int getNewRecordsCount(Object result) {
		try {
			CallLogsListPOJO logs = (CallLogsListPOJO) result;
			List<CallLogPOJO> newLogs = logs.getLogs();
			return newLogs.size();
		} catch (Exception e) {
			if (LogSettings.MARKET) {
				MktLog.w(TAG, "getNewRecordsCount ex:" + e);
			}
			return 0;
		}
	}

	/**
	 * Sets "loading" state.
	 * 
	 * @param enable
	 *            the state
	 */
	private void setLoadingStateView(boolean enable) {
		if (enable) {
			mLoadingView.setVisibility(View.VISIBLE);
			mCallLogLoadingViewVisiable = true;
		} else {
			mCallLogLoadingViewVisiable = false;
			mLoadingView.setVisibility(View.GONE);
		}
	}

	/**
	 * Sets "empty" state.
	 * 
	 * @param enable
	 *            the state
	 */
	private void setEmptyStateView(boolean enable) {
		if (enable) {
			mEmptyImageView.setVisibility(View.VISIBLE);
			mEmptyTextView.setVisibility(View.VISIBLE);
			mCallLogEmptyVisiable = true;
		} else {
			mCallLogEmptyVisiable = false;
			mEmptyImageView.setVisibility(View.GONE);
			mEmptyTextView.setVisibility(View.GONE);
		}
	}

	private final class QueryHandler extends AsyncQueryHandler {
		private Context mContext;

		public QueryHandler(Context context) {
			super(context.getContentResolver());
			mContext = context;
		}

		@Override
		protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
			if (cursor == null) {
				return;
			}
			if (!isFinishing()) {
				mAdapter.changeCursor(cursor);
				try {
					if (cursor.getCount() > 0) {
						if (mCallLogLoadingViewVisiable) {
							mLoadingHandler.reset();
							setLoadingStateView(false);
							if (LogSettings.MARKET) {
								QaLog.i(TAG, "onQueryComplete (loading->data)");
							}
						}
						if (mCallLogEmptyVisiable) {
							if (LogSettings.MARKET) {
								QaLog.i(TAG, "onQueryComplete (empty->data)");
							}
							setEmptyStateView(false);
						}
					} else {
						/**
						 * No data.
						 */
						if (mCallLogLoadingViewVisiable) {
							int status = NetworkManager.getInstance().getRequestStatus(mContext, mMode == MODE_ALL ? RequestInfoStorage.GET_ALL_CALL_LOGS : RequestInfoStorage.GET_MISSED_CALL_LOGS);
							if (status == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED || status == RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR) {
								mLoadingHandler.reset();
								setLoadingStateView(false);
								setEmptyStateView(true);
								if (LogSettings.MARKET) {
									QaLog.i(TAG, "onQueryComplete (loading->empty)");
								}
							} else {
								if (LogSettings.MARKET) {
									QaLog.w(TAG, "onQueryComplete (loading progress)");
								}
								mLoadingHandler.sendEmptyMessageDelayed(0, LOADING_CHECK_TIMEOUT);
							}
						} else {
							if (!mCallLogEmptyVisiable) {
								if (LogSettings.MARKET) {
									MktLog.w(TAG, "onQueryComplete -> empty)");
								}
								setEmptyStateView(true);
							}
						}
					}
				} catch (java.lang.Throwable error) {
					if (LogSettings.MARKET) {
						MktLog.e(TAG, "onQueryComplete process error: " + error.getMessage());
					}
				}
			} else {
				if (cursor != null && !cursor.isClosed()) {
					cursor.close();
				}
			}
		}
	}

	/**
	 * Defines timeout between call log requests/DB states checks.
	 */
	private static final long LOADING_CHECK_TIMEOUT = 1000;

	/**
	 * Defines the number of call log requests/DB states checks.
	 */
	private static final long LOADING_CHECKS_NUMBER = 10;

	/**
	 * Keeps loading handler.
	 */
	private LoadingHandler mLoadingHandler = new LoadingHandler();

	/**
	 * Loading handler helper class.
	 */
	private final class LoadingHandler extends Handler {

		private int mCounter = 0;

		public void reset() {
			mCounter = 0;
			this.removeMessages(0);
		}

		@Override
		public void handleMessage(Message msg) {
			if (!mActive) {
				return;
			}
			try {
				if (mCallLogLoadingViewVisiable) {
					if (mAdapter != null && !mAdapter.getCursor().isClosed()) {
						if (mAdapter.getCursor().getCount() == 0) {
							int status = NetworkManager.getInstance().getRequestStatus(CallLogListActivity.this, mMode == MODE_ALL ? RequestInfoStorage.GET_ALL_CALL_LOGS : RequestInfoStorage.GET_MISSED_CALL_LOGS);
							if (status == RCMDataStore.SyncStatusEnum.SYNC_STATUS_LOADED || status == RCMDataStore.SyncStatusEnum.SYNC_STATUS_ERROR) {
								setEmptyStateView(true);
								setLoadingStateView(false);
								if (LogSettings.MARKET) {
									QaLog.w(TAG, "Loading (wait->empty)");
								}
								return;
							} else {
								mCounter++;
								if (mCounter < LOADING_CHECKS_NUMBER) {
									if (LogSettings.MARKET) {
										QaLog.i(TAG, "Loading (wait->continue)");
									}
									mLoadingHandler.sendEmptyMessageDelayed(0, LOADING_CHECK_TIMEOUT);
									return;
								}
								setEmptyStateView(true);
								setLoadingStateView(false);
								if (LogSettings.MARKET) {
									MktLog.w(TAG, "Loading (wait->empty)");
								}
								return;
							}
						}
					}
				}
			} catch (java.lang.Throwable error) {
				if (LogSettings.MARKET) {
					MktLog.e(TAG, "mLoadingHandler error: " + error.getMessage());
				}
			}

			if (mCallLogLoadingViewVisiable) {
				if (LogSettings.MARKET) {
					MktLog.w(TAG, "Loading (reset)");
				}
				reset();
				setLoadingStateView(false);
			}
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = this.getMenuInflater();
		inflater.inflate(R.layout.calllog_menu, menu);
		MenuItem dnd = menu.findItem(R.id.calllog_dnd);
		if (dnd != null) {
			dnd.setVisible(UserInfo.dndAllowed());
		}
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.calllog_refresh: {
			if (LogSettings.MARKET) {
				QaLog.i(TAG, "User: Refresh");
			}
			if (!NetworkUtils.isRCAvaliable(this)) {
				if (LogSettings.MARKET) {
					MktLog.i(TAG, "Refresh initiated by the user: Network is not available. Warning is shown to the user.");
				}
				try {
					RcAlertDialog.getBuilder(this).setTitle(R.string.error_code_alert_title).setMessage(R.string.offline_mode_alerttext_getmore).setIcon(R.drawable.symbol_exclamation).setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
						}
					}).create().show();
				} catch (java.lang.Throwable th) {
				}
			} else {
				if (LogSettings.MARKET) {
					MktLog.i(TAG, "Refresh: initiated by the user...");
				}
				try {
					long mailBoxId = RCMProviderHelper.getCurrentMailboxId(this);
					NetworkManager.getInstance().getCallLogByType(this, mailBoxId, RCMConstants.CALL_LOG_PAGE_SIZE, CallLogTable.LOGS_ALL, "Up", true);
					NetworkManager.getInstance().getCallLogByType(this, mailBoxId, RCMConstants.CALL_LOG_PAGE_SIZE, CallLogTable.LOGS_MISSED, "Up", true);
				} catch (java.lang.Throwable th) {
				}
			}
			return true;
		}
		case R.id.calllog_settings:
			if (LogSettings.MARKET) {
				QaLog.i(TAG, "User: Settings");
			}
			SettingsLauncher.launch(this);
			return true;
		case R.id.calllog_dnd:
			if (LogSettings.MARKET) {
				QaLog.i(TAG, "User: Set DND");
			}
			DNDialog.showDNDDialog(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo menuInfo) {

		AdapterView.AdapterContextMenuInfo info;
		try {
			info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		} catch (ClassCastException e) {
			if (LogSettings.ENGINEERING) {
				EngLog.e(TAG, "onCreateContextMenu() menuInfo", e);
			} else if (LogSettings.MARKET) {
				QaLog.e(TAG, "onCreateContextMenu() menuInfo: " + e.getMessage());
			}
			return;
		}

		try {
			final CallLogListItemCache itemView = (CallLogListItemCache) info.targetView.getTag();
			Cursor cursor = (Cursor) getListAdapter().getItem(info.position);
			if (cursor == null) {
				return;
			}
			if (itemView.hasMoreRecordsItem) {
				return;
			}

			long mailboxId = Long.parseLong(cursor.getString(CallLogQuery.CALLLOG_MAILBOX_INDX));
			long recordId = cursor.getLong(CallLogQuery.CALLLOG_ID_INDX);
			ContactBinding bind = CallLogQuery.readSyncBind(this, cursor);
			BindSync bsync = Cont.acts.syncBind(this, mailboxId, bind);

			if (bsync.syncState == BindSync.State.INVALID_RECORD) {
				Cont.acts.bindSyncTrace(this, bsync, TAG, "onCreateContextMenu", recordId);
			} else if (bsync.syncState == BindSync.State.NOT_CHANGED) {
			} else {
				Cont.acts.bindSyncTrace(this, bsync, TAG, "onCreateContextMenu", recordId);
				CallLogSync.updateCallLogBind(this, mailboxId, recordId, bsync);
				if (syncService != null) {
					try {
						syncService.syncCallLog(RCMProviderHelper.getCurrentMailboxId(this));
					} catch (java.lang.Throwable error) {
						if (LogSettings.MARKET) {
							MktLog.e(TAG, "onCreateContextMenu() call sync failed: ", error);
						}
					}
				}
			}

			CallLogQuery.CallLogItem ci = CallLogQuery.readCallLogItem(this, cursor);
			itemView.labelView.setText(ci.displayName);
			itemView.cpn = ci.cpn;

			menu.setHeaderTitle(itemView.labelView.getText());

			if (itemView.cpn != null) {
				menu.add(0, MENU_ITEM_CALL, 0, R.string.calllog_menu_Call);
			}

			if (bsync.bind.hasContact) {
				if (bsync.bind.isPhoneNumberFavorite) {
					menu.add(0, MENU_ITEM_REMOVE_FROM_FAVORITES, 0, R.string.calllog_menu_remove_from_faworites);
				} else {
					menu.add(0, MENU_ITEM_ADD_TO_FAVORITES, 0, R.string.calllog_menu_add_to_faworites);
				}
			} else {
				if (itemView.cpn != null) {
					menu.add(0, MENU_ITEM_CREATE_NEW_CONTACT, 0, R.string.calllog_menu_add_to_contacts_new);
					menu.add(0, MENU_ITEM_ADD_TO_EXISTING_CONTACT, 0, R.string.calllog_menu_add_to_contacts_existing);
					menu.add(0, MENU_ITEM_ADD_TO_FAVORITES, 0, R.string.calllog_menu_add_to_faworites);
				}
			}
		} catch (java.lang.Throwable error) {
			if (LogSettings.MARKET) {
				MktLog.e(TAG, "onCreateContextMenu() error: " + error.getMessage());
			}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterView.AdapterContextMenuInfo info;
		try {
			info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		} catch (ClassCastException e) {
			if (LogSettings.ENGINEERING) {
				EngLog.e(TAG, "onContextItemSelected() menuInfo", e);
			} else if (LogSettings.MARKET) {
				MktLog.e(TAG, "onContextItemSelected() menuInfo: " + e.getMessage());
			}
			return false;
		}

		try {
			Cursor cursor = (Cursor) getListAdapter().getItem(info.position);

			long mailboxId = Long.parseLong(cursor.getString(CallLogQuery.CALLLOG_MAILBOX_INDX));
			long recordId = cursor.getLong(CallLogQuery.CALLLOG_ID_INDX);
			ContactBinding bind = CallLogQuery.readSyncBind(this, cursor);
			BindSync bsync = Cont.acts.syncBind(this, mailboxId, bind);
			if (bsync.syncState == BindSync.State.INVALID_RECORD) {
				Cont.acts.bindSyncTrace(this, bsync, TAG, "onContextItemSelected", recordId);
			} else if (bsync.syncState == BindSync.State.NOT_CHANGED) {

			} else {
				Cont.acts.bindSyncTrace(this, bsync, TAG, "onContextItemSelected", recordId);
				CallLogSync.updateCallLogBind(this, mailboxId, recordId, bsync);
				if (syncService != null) {
					try {
						syncService.syncCallLog(mailboxId);
					} catch (java.lang.Throwable error) {
						if (LogSettings.MARKET) {
							MktLog.e(TAG, "onContextItemSelected() call sync failed: ", error);
						}
					}
				}
				return false;
			}

			CallLogQuery.CallLogItem ci = CallLogQuery.readCallLogItem(this, cursor);
			switch (item.getItemId()) {
			case MENU_ITEM_CALL: {
				if (LogSettings.MARKET) {
					QaLog.i(TAG, "User: Call");
				}
				if (ci.isValidNumber) {
					FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, (mMode == MODE_ALL) ? FlurryTypes.SCR_CALL_LOG_ALL_CONTEXT : FlurryTypes.SCR_CALL_LOG_MISSED_CONTEXT);
					mRingOutAgent.call(ci.cpn.phoneNumber.numRaw, ci.displayName);
				}
				return true;
			}

			case MENU_ITEM_CREATE_NEW_CONTACT: {
				if (LogSettings.MARKET) {
					QaLog.i(TAG, "User: Create New Contact");
				}
				if (ci.isValidNumber) {
					FlurryTypes.onEventScreen(FlurryTypes.EVENT_CREATE_NEW_CONTACT, (mMode == MODE_ALL) ? FlurryTypes.SCR_CALL_LOG_ALL_CONTEXT : FlurryTypes.SCR_CALL_LOG_MISSED_CONTEXT);
					try {
						startActivity(Cont.acts().getCreateNewContactIntent(this, ci.displayName, ci.cpn.normalizedNumber));
					} catch (ActivityNotFoundException e) {
						if (LogSettings.MARKET) {
							MktLog.e(TAG, "create new contact start activity error: " + e.toString());
						}
					}
				}
				return true;
			}
			case MENU_ITEM_ADD_TO_EXISTING_CONTACT: {
				if (LogSettings.MARKET) {
					QaLog.i(TAG, "User: Add to Contact");
				}
				if (ci.isValidNumber) {
					FlurryTypes.onEventScreen(FlurryTypes.EVENT_ADD_EXISTING_CONTACT, (mMode == MODE_ALL) ? FlurryTypes.SCR_CALL_LOG_ALL_CONTEXT : FlurryTypes.SCR_CALL_LOG_MISSED_CONTEXT);
					try {
						startActivity(Cont.acts().getAddToContactIntent(this, ci.cpn.normalizedNumber));
					} catch (ActivityNotFoundException e) {
						if (LogSettings.MARKET) {
							MktLog.e(TAG, "add to existing contact start activity error: " + e.toString());
						}
					}
				}
				return true;
			}
			case MENU_ITEM_ADD_TO_FAVORITES: {
				if (LogSettings.MARKET) {
					QaLog.i(TAG, "User: Add to Favorites");
				}
				if (bsync.bind.hasContact) {
					if (!bsync.bind.isPhoneNumberFavorite) {
						FlurryTypes.onEventScreen(FlurryTypes.EVENT_ADD_FAVORITES, (mMode == MODE_ALL) ? FlurryTypes.SCR_CALL_LOG_ALL_CONTEXT : FlurryTypes.SCR_CALL_LOG_MISSED_CONTEXT);
						if (bsync.bind.isPersonalContact) {
							Cont.acts().addToPersonalFavorites(bsync.bind.phoneId, bsync.bind.phoneNumber, bsync.bind.cpn.normalizedNumber, this);
						} else {
							CompanyFavorites.setAsFavorite(this, mailboxId, bsync.bind.phoneId);
						}
					}
				} else {
					if (!ci.isValidNumber) {
						return true;
					}
					Resources res = getResources();
					final CharSequence[] items = { res.getString(R.string.calllog_menu_add_to_contacts_new), res.getString(R.string.calllog_menu_add_to_contacts_existing) };
					final String contactName = ci.displayName;
					final String contactNumber = ci.cpn.normalizedNumber;
					final Context ctx = this;
					AlertDialog.Builder builder = RcAlertDialog.getBuilder(this);
					builder.setTitle(res.getString(R.string.calllog_first_create_contact_prompt));
					builder.setItems(items, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int item) {
							if (item == 0) {
								try {
									startActivity(Cont.acts().getCreateNewContactIntent(ctx, contactName, contactNumber));
								} catch (ActivityNotFoundException e) {
									if (LogSettings.MARKET) {
										MktLog.e(TAG, "create new contact start activity error: " + e.toString());
									}
								}
							} else {
								try {
									startActivity(Cont.acts().getAddToContactIntent(ctx, contactNumber));
								} catch (ActivityNotFoundException e) {
									if (LogSettings.MARKET) {
										MktLog.e(TAG, "add to existing contact start activity error: " + e.toString());
									}
								}
							}
						}
					});
					AlertDialog alert = builder.create();
					alert.show();
				}
				return true;
			}
			case MENU_ITEM_REMOVE_FROM_FAVORITES: {
				if (LogSettings.MARKET) {
					QaLog.i(TAG, "User: Remove from Favorites");
				}
				if (bsync.bind.hasContact) {
					if (bsync.bind.isPhoneNumberFavorite) {
						FlurryTypes.onEventScreen(FlurryTypes.EVENT_REMOVE_FAVORITES, (mMode == MODE_ALL) ? FlurryTypes.SCR_CALL_LOG_ALL_CONTEXT : FlurryTypes.SCR_CALL_LOG_MISSED_CONTEXT);
						if (bsync.bind.isPersonalContact) {
							PersonalFavorites.deletePersonalFavorite(this, mailboxId, bsync.bind.phoneId);
						} else {
							CompanyFavorites.setAsNotFavorite(this, mailboxId, bsync.bind.phoneId);
						}
					}
				}
				return true;
			}
			}
		} catch (java.lang.Throwable error) {
			if (LogSettings.MARKET) {
				QaLog.e(TAG, "onContextItemSelected() error: " + error.getMessage());
			}
			return false;
		}

		return super.onContextItemSelected(item);
	}

	// @Override
	// public boolean onKeyDown(int keyCode, KeyEvent event) {
	// switch (keyCode) {
	//
	// case KeyEvent.KEYCODE_CALL: {
	// long callPressDiff = SystemClock.uptimeMillis()
	// - event.getDownTime();
	// if (callPressDiff >= ViewConfiguration.getLongPressTimeout()) {
	// // Launch voice dialer
	// Intent intent = new Intent(Intent.ACTION_VOICE_COMMAND);
	// intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	// try {
	// startActivity(intent);
	// } catch (ActivityNotFoundException e) {
	// if (LogSettings.ENGINEERING) {
	// Log.e(TAG, "onKeyDown()", e);
	// }
	// }
	// return true;
	// }
	// }
	// }
	// return super.onKeyDown(keyCode, event);
	// }

	// @Override
	// public boolean onKeyUp(int keyCode, KeyEvent event) {
	// switch (keyCode) {
	// case KeyEvent.KEYCODE_CALL:
	// return true;
	// }
	// return super.onKeyUp(keyCode, event);
	// }

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		try {
			final CallLogListItemCache itemView = (CallLogListItemCache) v.getTag();
			if (itemView.hasMoreRecordsItem) {
				if (LogSettings.MARKET) {
					QaLog.i(TAG, "User: Get More Records");
				}
				if (!itemView.progressBarVisiable) {
					itemView.progressView.setVisibility(View.VISIBLE);
					itemView.progressBarVisiable = true;
				}
				itemView.get_more_records.setText(getString(R.string.calllog_get_more_records_loading));
				try {
					NetworkManager.getInstance().getCallLogByType(this, RCMProviderHelper.getCurrentMailboxId(this), RCMConstants.CALL_LOG_PAGE_SIZE, (mMode == MODE_ALL ? CallLogTable.LOGS_ALL : CallLogTable.LOGS_MISSED), "Down", true);
				} catch (java.lang.Throwable th) {
				}
			} else {
				if (itemView.cpn != null && itemView.cpn.isValid) {
					FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, (mMode == MODE_ALL) ? FlurryTypes.SCR_CALL_LOG_ALL_CONTEXT : FlurryTypes.SCR_CALL_LOG_MISSED_CONTEXT);
					if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
						PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.CALL_FROM_CALLLOG);
					}
					boolean isNonFaxAccout = ((RCMProviderHelper.getTierSettings(CallLogListActivity.this) & RCMConstants.TIERS_PHS_DO_NOT_DISTURB) == 0) ? false : true; 
					if(isNonFaxAccout){
						mRingOutAgent.call(itemView.cpn.phoneNumber.numRaw, itemView.labelView.getText().toString());
					}
				}
			}
		} catch (java.lang.Throwable error) {
			if (LogSettings.MARKET) {
				QaLog.e(TAG, "onListItemClick failed: ", error);
			}
		}
	}

	/**
	 * Initialize sub tabs
	 */
	private void initSabTabs() {

		btnLeft = (ToggleButton) findViewById(R.id.btnLeft);
		String all = getResources().getString(R.string.calllog_tab_all_title);
		btnLeft.setTextOn(all);
		btnLeft.setTextOff(all);
		btnLeft.setText(all);
		btnLeft.setChecked(true);
		btnLeft.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				if (LogSettings.MARKET) {
					QaLog.i(TAG, "User: All");
				}
				if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
					PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.CALL_LOG_SWITCH);
				}
				btnLeft.setChecked(!btnLeft.isChecked());
				if (!btnLeft.isChecked()) {
					btnLeft.setChecked(true);
					btnRight.setChecked(false);
					switchToAll();
					refreshCallLogRequestOnUserActivity();
				}
			}
		});

		String missed = getResources().getString(R.string.calllog_tab_missed_title);
		btnRight = (ToggleButton) findViewById(R.id.btnRight);
		btnRight.setTextOn(missed);
		btnRight.setTextOff(missed);
		btnRight.setText(missed);
		btnRight.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				if (LogSettings.MARKET) {
					QaLog.i(TAG, "User: Missed");
				}
				btnRight.setChecked(!btnRight.isChecked());
				if (!btnRight.isChecked()) {
					btnLeft.setChecked(false);
					btnRight.setChecked(true);
					switchToMissed();
					refreshCallLogRequestOnUserActivity();
				}
			}
		});

	}

	private void switchToAll() {
		FlurryTypes.onEventScreen(FlurryTypes.EVENT_SCREEN_OPEN, FlurryTypes.SCR_CALL_LOG_ALL);
		FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_LOG_OPEN, FlurryTypes.SCR_CALL_LOG_ALL);
		mMode = MODE_ALL;
		Cursor cursor = mAdapter.getCursor();
		if (cursor != null && !cursor.isClosed()) {
			// cursor.close();
		}
		startQuery();
	}

	private void switchToMissed() {
		FlurryTypes.onEventScreen(FlurryTypes.EVENT_SCREEN_OPEN, FlurryTypes.SCR_CALL_LOG_MISSED);
		FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_LOG_OPEN, FlurryTypes.SCR_CALL_LOG_MISSED);
		mMode = MODE_MISSED;
		Cursor cursor = mAdapter.getCursor();
		if (cursor != null && !cursor.isClosed()) {
			// cursor.close();
		}
		startQuery();
	}

}
