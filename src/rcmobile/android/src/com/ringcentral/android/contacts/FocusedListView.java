/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class FocusedListView extends ListView {
    public FocusedListView(Context context) {
        super(context);
    }

    public FocusedListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FocusedListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }
}
