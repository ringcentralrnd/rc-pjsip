/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.ringcentral.android.contacts.Cont.acts.BindSync;
import com.ringcentral.android.contacts.Cont.acts.ContactBinding;
import com.ringcentral.android.search.ContactsSearchAgent;

/**
 * Android Contacts APIs accessor interface.
 */
public class ContactProxy {
    /**
     * Returns People CONTENT_URI.
     * 
     * @return the People CONTENT_URI
     */
    public Uri getPeopleUri() {
        throw new IllegalStateException("ContactProxy access");
    }

    /**
     * Returns the list of tagged e-mail addresses assigned to a personal
     * contact.
     * 
     * @param context
     *            the execution context
     * @param contactId
     *            the personal contact identifier to retrieve addresses from
     * @return the list of addresses (empty array means there are no addresses
     *         in the contact)
     */
    public List<Cont.acts.EmailContact> getEmailAddresses(Context context, long contactId) {
        throw new IllegalStateException("ContactProxy access");
    }
    
    /**
     * Forms and returns an intent to create new contact.
     * 
     * @param context
     *            the execution context
     * @param contactName
     *            the name of the new contact
     * @param contactNumber
     *            the number of the new contact
     * @return the intent
     */
    public Intent getCreateNewContactIntent(Context context, String contactName, String contactNumber) {
        throw new IllegalStateException("ContactProxy access");
    }
    
    /**
     * Forms and returns an intent to add number to an existent contact.
     * 
     * @param context
     *            the execution context
     * @param contactNumber
     *            the number of contact to add to
     * @return the intent
     */
    public Intent getAddToContactIntent(Context context, String contactNumber) {
        throw new IllegalStateException("ContactProxy access");
    }
    
    /**
     * Looks-up contact by phone number (first found).
     * 
     * @param phoneNumber
     *            the phone number to search for
     * @param context
     *            the execution context to execute on
     * 
     * @return the contact id if any found, otherwise <code>-1</code>
     */
    public long lookupContactIDByPhoneNumber(String phoneNumber, Context context) {
        throw new IllegalStateException("ContactProxy access");
    }

    /**
     * Adds a contact/phone to personal favorites.
     * 
     * @param phoneId
     *            the phone identifier
     * 
     * @param originalNumber
     *            the original phone number
     * 
     * @param normalizedNumber
     *            the normalized phone number
     * 
     * @param context
     *            the execution context to execute on
     * 
     * @return the result of operation, {@link #FV_RESULT_ADDED},
     *         {@link #FV_RESULT_CONTACT_RECORD_NOT_FOUND},
     *         {@link #FV_RESULT_OPERATION_FAILED}
     */
    public int addToPersonalFavorites(long phoneId, String originalNumber, String normalizedNumber, Context context) {
        throw new IllegalStateException("ContactProxy access");
    }

    /**
     * Defines if the favorite has been added
     */
    public final static int FV_RESULT_ADDED = 0;

    /**
     * Defines an error when the contact/number has not been found in Android
     * Contacts.
     */
    public final static int FV_RESULT_CONTACT_RECORD_NOT_FOUND = 1;

    /**
     * Defines any operation errors.
     */
    public final static int FV_RESULT_OPERATION_FAILED = 2;
    
    /**
     * Looks-up contact name by phone number (first found).
     * 
     * @param phoneNumber
     *            the phone number to search for
     * @param context
     *            the execution context to execute on
     * 
     * @return the contact display name if any found, otherwise <code>-1</code>
     */
    public String lookupContactNameByPhoneNumber(String phoneNumber, Context context) {
        throw new IllegalStateException("ContactProxy access");
    }
    
    /**
     * Binding to personal contact
     * 
     * @param binding
     *            the binding to fill data by
     * @param context
     *            the execution context to execute on
     * @param mailboxId
     *            the mailbox identifier
     * 
     * @return the filled binding data
     */
    ContactBinding bindPersonalContact(Context context, long mailboxId, ContactBinding binding) {
        throw new IllegalStateException("ContactProxy access");
    }
    
    /**
     * Bind synchronization.
     * 
     * @param context
     *            the execution context
     * @param bind
     *            the personal contact/phone existent bind
     * @param mailboxId
     *            the mailbox identifier
     * @return the sync bind
     */
    public BindSync syncPersonalBind(Context context, long mailboxId, BindSync bind) {
        throw new IllegalStateException("ContactProxy access");
    }
    
    /**
     * Returns phone tag - the value represented to the end-user.
     * 
     * @param context
     *            the execution context
     * @param tag
     *            the numeric type
     * @return user-friendly (readable) phone tag
     */
    public String getPhoneNumberTag(Context context, long tag) {
        throw new IllegalStateException("ContactProxy access");
    }
    
    /**
     * Define if the tag is custom number tag.
     * 
     * @param context
     *            the execution context
     * @param tag
     *            the tag to be checked
     * @return <code>true</code> if the <code>tag</code> is custom, otherwise
     *         <code>false</code>
     */
    public boolean isCustomTagPhoneNumber(Context context, long tag) {
        throw new IllegalStateException("ContactProxy access");
    }
    
    public ContactsSearchAgent getContactsSearchAgent() {
        throw new IllegalStateException("ContactProxy access");
    }
    
    /**
     * Retrieves photo bitmap. 
     *  
     * @param context
     *            the execution context
     * @param photoId
     *            the photo id (contact id for Android below 2.0)
     * @param options
     *            the decoding options
     * @return
     */
    public Bitmap getContactPhoto(Context context, long photoId, BitmapFactory.Options options) {
        throw new IllegalStateException("ContactProxy access");
    }

    /**
     * Find contact phone by phone id in contacts.
     * 
     * @param context
     *            the execution context
     * @param phoneId
     *            the phoned id to look up
     * @return the PhoneContact or <code>null</code> if the <code>phoneId</code>
     *         has not been found in contacts
     */
    public Cont.acts.PhoneContact lookUpPhoneContactById(Context context,
            long phoneId) {
        throw new IllegalStateException("ContactProxy access");
    }
    
    /**
     * Retrieves the phone numbers of a contact.
     * 
     * @param context
     *            the execution context
     * @param contactId
     *            the identifier of the personal contact
     * @param onlyFullValidNumbers
     *            defines if only full valid numbers shall be returned
     * @return all <code>contactId</code> valid phone numbers
     */
    public List<Cont.acts.TaggedContactPhoneNumber> 
        getPersonalContactPhoneNumbers(Context context, long contactId,
            boolean onlyFullValidNumbers) {
        throw new IllegalStateException("ContactProxy access");
    }
    
    /**
     * Find contact phone by number in contacts.
     * 
     * @param context
     *            the execution context
     * @param number
     *            the phone number to look up
     * @return the PhoneContact or <code>null</code> if the <code>number</code>
     *         has not been found in contacts
     */
    public Cont.acts.PhoneContact lookUpPhoneContactByNumber(Context context,
            String number) {
        throw new IllegalStateException("ContactProxy access");
    }
}
