/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import com.ringcentral.android.phoneparser.PhoneNumber;

/**
 * Represents Android Contacts phone number. 
 */
public class ContactPhoneNumber {
    /**
     * Defines if the number is valid.
     */
    public boolean isValid = false;
    
    /**
     * Keeps original number.
     */
    public String originalNumber = null;
    
    /**
     * Keeps normalized form.
     */
    public String normalizedNumber = null;
    
    /**
     * Keeps the phone number.
     */
    public PhoneNumber phoneNumber = null;
}
