/** 
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemClock;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.calllog.CallLogSync;
import com.ringcentral.android.messages.MessagesSync;
import com.ringcentral.android.provider.RCMProviderHelper;

/**
 * Local data synchronization service.
 * TODO: Stop on logout/re-login.
 */
public class LocalSyncService extends Service {
    private static final String LOG_TAG = "[RC] LocalSyncService";
    private static final boolean DEBUG_ENABLED = false && LogSettings.ENGINEERING;
    private final LocalSyncBinder mBinder = new LocalSyncBinder();
    private final Object mLock = new Object();
    private LocalSyncControl mSync;
    private LocalSyncControl mCallLogSync;
    private LocalSyncControl mMessagesSync;
    private boolean mActive = true;
    
// TODO: Clean-up
//    private CallLogChangeObserver mCallLogObserver;
//    
//    private class CallLogChangeObserver extends ContentObserver {
//        public CallLogChangeObserver(Handler handler) {
//            super(handler);
//        }
//
//        @Override
//        public boolean deliverSelfNotifications() {
//            return false;
//        }
//
//        @Override
//        public void onChange(boolean selfChange) {
//            super.onChange(selfChange);
//            if (LogSettings.ENGINEERING) {
//                EngLog.w(LOG_TAG, "Sync service: CALL LOG CHANGED (self=" + selfChange +")");
//                if (selfChange) {
//                    mBinder.syncCallLog(RCMProviderHelper.getCurrentMailboxId(LocalSyncService.this));
//                }
//            }
//        }
//    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        if (LogSettings.ENGINEERING) {
            try {
                EngLog.d(LOG_TAG, "Initialized (" + ((Context) this).toString() + ")");
            } catch (Exception ex) {
            }
        }
//        mCallLogObserver = new CallLogChangeObserver(new Handler());
//        LocalSyncService.this.getContentResolver().registerContentObserver(UriHelper.getUri(RCMProvider.CALL_LOG), true, mCallLogObserver);
    }

    @Override
    public IBinder onBind(Intent intent) {
        if (LogSettings.ENGINEERING) {
            try {
                EngLog.d(LOG_TAG, "Service bind (" + ((Context) this).toString() + ")");
            } catch (Exception ex) {
            }
        }
        return (mBinder);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (LogSettings.ENGINEERING) {
            try {
                EngLog.w(LOG_TAG, "Sync service shutdown ("
                        + ((Context) this).toString() + ")");
            } catch (Exception ex) {
            }
        }
        
        synchronized (mLock) {
            mActive = false;
            if (mSync != null) {
                mSync.cancel();
                mSync = null;
            }
            if (mCallLogSync != null) {
                mCallLogSync.cancel();
                mCallLogSync = null;
            }
            
            if(mMessagesSync != null) {
            	mMessagesSync.cancel();
            	mMessagesSync = null;
            }
            
//            if (mCallLogObserver != null) {
//                try {
//                    getContentResolver().unregisterContentObserver(mCallLogObserver);
//                } catch (Exception ex) {
//                }
//                mCallLogObserver = null;
//            }
        }
    }
    
    @Override
    public void onStart(Intent intent, int startId) {
        if (intent == null) {
            return;
        }
        if (DEBUG_ENABLED) {
            try {
                EngLog.i(LOG_TAG, "onStart (" + ((Context) this).toString() + ")");
            } catch (Exception ex) {
            }
        }

        try {
            int command = intent.getExtras().getInt(COMMAND_TAG);
            if (command == CALL_LOG_SYNC_COMMAND) {
                mBinder.syncCallLog(RCMProviderHelper.getCurrentMailboxId(LocalSyncService.this));
            }
        } catch (Exception ex) {
            if (LogSettings.QA) {
                QaLog.e(LOG_TAG, "onStart error: " + ex.toString());
            }
        }
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (DEBUG_ENABLED) {
            try {
                EngLog.i(LOG_TAG, "Async Call Log request (LVL > 5) (" + ((Context) this).toString() + ")");
            } catch (Exception ex) {
            }
        }
        onStart(intent, startId);
        return START_STICKY;
    }
    
    /**
     * Request to sync Call Log.
     * 
     * @param context
     *            the execution context
     */
    public static final void syncCallLog(Context context) {
        if (LogSettings.ENGINEERING) {
            EngLog.i(LOG_TAG, "Async Call Log sync requested.");
        }
        Intent serviceIntent = new Intent(context, LocalSyncService.class);
        serviceIntent.putExtra(COMMAND_TAG, CALL_LOG_SYNC_COMMAND);
        context.startService(serviceIntent);
    }

    /**
     * Command tag.
     */
    private static final String COMMAND_TAG = "rc_sync_command";
    
    /**
     * Sync Call Log data command.
     */
    private static final int CALL_LOG_SYNC_COMMAND = 0x17;
    
    /**
     * Service binder. 
     */
    public class LocalSyncBinder extends Binder {
    	public LocalSyncService getService() {
            return (LocalSyncService.this);
        }

        /**
         * Force personal favorites synchronization.
         * 
         * @param mailboxId the mailbox identifier
         */
        public void syncPersonalFavorites(long mailboxId) {
            synchronized (mLock) {
                if (mActive) {
                    if (mSync != null) {
                        mSync.cancel();
                    }
                    mSync = new LocalSyncControl(mLock, mailboxId, LocalSyncService.this, getNextSyncId());
                    new PersonalFavoritesSyncTask().execute(mSync);
                }
            }
        }
        
        /**
         * Force Call Log synchronization.
         * 
         * @param mailboxId the mailbox identifier
         */
        public void syncCallLog(long mailboxId) {
            synchronized (mLock) {
                if (mActive) {
                    if (mCallLogSync != null) {
                        mCallLogSync.cancel();
                    }
                    mCallLogSync = new LocalSyncControl(mLock, mailboxId, LocalSyncService.this, getNextSyncId());
                    new CallLogSyncTask().execute(mCallLogSync);
                }
            }
        }
        
        /**
         * Force Messages synchronization.
         * 
         * @param mailboxId the mailbox identifier
         */
        public void syncMessages(long mailboxId) {
            synchronized (mLock) {
                if (mActive) {
                    if (mMessagesSync != null) {
                    	mMessagesSync.cancel();
                    }
                    mMessagesSync = new LocalSyncControl(mLock, mailboxId, LocalSyncService.this, getNextSyncId());
                    new MessagesSyncTask().execute(mMessagesSync);
                }
            }
        }
    }
    
    /**
     * Keeps next sync identifier.
     */
    private static int mNextSyncId = 1;
    
    /**
     * Defines max. sync identifier.
     */
    private static int MAX_SYNC_ID = 0x7FFF;

    /**
     * Returns next sync identifier.
     * 
     * @return next sync identifier
     */
    private static int getNextSyncId() {
        int id = mNextSyncId;
        mNextSyncId++;
        if (mNextSyncId >= MAX_SYNC_ID) {
            mNextSyncId = 1;
        }
        return id;
    }
    
    /**
     * Sync control. 
     */
    public static final class LocalSyncControl {
        public LocalSyncControl(Object lock, long mailboxId, Context context, int id) {
            mLock = lock;
            mMailboxId = mailboxId;
            mContext = context;
            mId = id;
        }

        public long getMailboxId() {
            return mMailboxId;
        }

        public void cancel() {
            synchronized (mLock) {
                mCancel = true;
            }
        }

        public boolean isActive() {
            synchronized (mLock) {
                return mActive;
            }
        }

        public boolean isTerminated() {
            synchronized (mLock) {
                return mCancel;
            }
        }
        
        public Context getContext() {
            return mContext;
        }

        private volatile Object mLock;
        private volatile boolean mActive = true;
        private volatile boolean mCancel = false;
        private volatile long mMailboxId;
        private volatile Context mContext;
        private volatile int mId;
        
        public String toString() {
            try {
                StringBuffer sb = new StringBuffer('(');
                sb.append(mId);
                sb.append(':');
                sb.append(mMailboxId);
                sb.append(":cancel=");
                sb.append(mCancel);
                if (DEBUG_ENABLED) {
                    sb.append(':');
                    sb.append(mContext.toString());
                }
                sb.append(')');
                return sb.toString();
            } catch (java.lang.Throwable error) {
            }
            return "sync(null)";
        }
    }
    
    /**
     * Personal favorites synchronization task.  
     */
    private class PersonalFavoritesSyncTask extends AsyncTask<LocalSyncControl, Void, Void> {
        @Override
        protected Void doInBackground(LocalSyncControl... syncs) {
            LocalSyncControl sync = syncs[0];
            long startTime =  0L;
            if (LogSettings.MARKET) {
                startTime = SystemClock.elapsedRealtime();
                try {
                    QaLog.d(LOG_TAG, "Personal favorites sync started " + sync.toString());
                } catch (Exception ex) {
                }
            }

            try {
                if (PersonalFavorites.sync(sync)) {
                    if (LogSettings.MARKET) {
                        QaLog.i(LOG_TAG, "Personal favorites updated.");
                    }
                }
            } catch (Throwable t) {
                if (LogSettings.MARKET) {
                    MktLog.e(LOG_TAG, "Exception in PersonalFavoritesSyncTask sync " + sync.toString());
                    MktLog.e(LOG_TAG, "Exception in PersonalFavoritesSyncTask:", t);
                }
            }

            if (LogSettings.MARKET) {
                try {
                    MktLog.d(LOG_TAG, "Personal favorites sync finished [time="
                            + (SystemClock.elapsedRealtime() - startTime) + "] "
                            + sync.toString());
                } catch (Exception ex) {
                }
            }

            return (null);
        }

        @Override
        protected void onProgressUpdate(Void... unused) {
        }

        @Override
        protected void onPostExecute(Void unused) {
        }
    }
    
    /**
     * Call Log synchronization task.  
     */
    private class CallLogSyncTask extends AsyncTask<LocalSyncControl, Void, Void> {
        @Override
        protected Void doInBackground(LocalSyncControl... syncs) {
            LocalSyncControl sync = syncs[0];
            long startTime =  0L;
            if (LogSettings.MARKET) {
                startTime = SystemClock.elapsedRealtime();
                try {
                    QaLog.d(LOG_TAG, "Call log sync started " + sync.toString());
                } catch (Exception ex) {
                }
            }

            try {
                if (CallLogSync.sync(sync)) {
                    if (LogSettings.MARKET) {
                        QaLog.i(LOG_TAG, "Call log updated.");
                    }
                }
            } catch (Throwable t) {
                if (LogSettings.MARKET) {
                    MktLog.e(LOG_TAG, "Exception in CallLogSyncTask sync " + sync.toString());
                    MktLog.e(LOG_TAG, "Exception in CallLogSyncTask:", t);
                }
            }

            if (LogSettings.MARKET) {
                try {
                    MktLog.d(LOG_TAG, "Call log sync finished [time="
                            + (SystemClock.elapsedRealtime() - startTime) + "] "
                            + sync.toString());
                } catch (Exception ex) {
                }
            }

            return (null);
        }

        @Override
        protected void onProgressUpdate(Void... unused) {
        }

        @Override
        protected void onPostExecute(Void unused) {
        }
    }
    
    /**
     * Messages synchronization task.  
     */
    private class MessagesSyncTask extends AsyncTask<LocalSyncControl, Void, Void> {
        @Override
        protected Void doInBackground(LocalSyncControl... syncs) {
            LocalSyncControl sync = syncs[0];
            long startTime =  0L;
            if (LogSettings.MARKET) {
                startTime = SystemClock.elapsedRealtime();
                try {
                    QaLog.d(LOG_TAG, "Messages sync started " + sync.toString());
                } catch (Exception ex) {
                }
            }

            try {
                if (MessagesSync.sync(sync)) {
                    if (LogSettings.MARKET) {
                        QaLog.i(LOG_TAG, "Messages updated.");
                    }
                }
            } catch (Throwable t) {
                if (LogSettings.MARKET) {
                    MktLog.e(LOG_TAG, "Exception in MessagesSyncTask sync " + sync.toString());
                    MktLog.e(LOG_TAG, "Exception in MessagesSyncTask:", t);
                }
            }

            if (LogSettings.MARKET) {
                try {
                    MktLog.d(LOG_TAG, "Messages sync finished [time="
                            + (SystemClock.elapsedRealtime() - startTime) + "] "
                            + sync.toString());
                } catch (Exception ex) {
                }
            }

            return (null);
        }

        @Override
        protected void onProgressUpdate(Void... unused) {
        }

        @Override
        protected void onPostExecute(Void unused) {
        }
    }
}
