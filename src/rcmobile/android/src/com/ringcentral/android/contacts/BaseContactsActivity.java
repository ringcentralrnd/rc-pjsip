package com.ringcentral.android.contacts;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.*;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Contacts.People;
import android.text.TextUtils;
import android.view.*;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AbsListView.OnScrollListener;
import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.api.network.NetworkManager;
import com.ringcentral.android.contacts.Projections.Extensions;
import com.ringcentral.android.contacts.Projections.Personal;
import com.ringcentral.android.phoneparser.PhoneNumber;
import com.ringcentral.android.provider.RCMDataStore.FavoritesTable;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.ringout.RingOutAgent;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.EmailSender;
import com.ringcentral.android.utils.UserInfo;
import com.ringcentral.android.utils.ui.widget.MenuCustomDialog;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class BaseContactsActivity extends ListActivity implements View.OnCreateContextMenuListener {

	protected static String TAG;
	protected RingOutAgent mRingOutAgent;
	protected ImageView mImgContact;
	protected TextView mEmtytext;
	protected ProgressBar mLoadingBar;
	protected boolean mShowLoading = false;
	// private View mSubTabsDevider;
	protected LinearLayout mSubTabs;
	protected LocalSyncService.LocalSyncBinder syncService;

	protected ServiceConnection syncSvcConn = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder rawBinder) {
			syncService = (LocalSyncService.LocalSyncBinder) rawBinder;
			if (LogSettings.ENGINEERING) {
				EngLog.d(TAG, "Sync service connected.");
			}
			try {
				syncService.syncPersonalFavorites(RCMProviderHelper.getCurrentMailboxId(syncService.getService()));
			} catch (java.lang.Throwable error) {
				if (LogSettings.MARKET) {
					QaLog.e(TAG, "onServiceConnected() call sync failed: ", error);
				}
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			syncService = null;
			if (LogSettings.ENGINEERING) {
				EngLog.d(TAG, "Sync service disconnected.");
			}
		}
	};

	protected static final int SHOW_PROGRESS_DELAY = 300;

	protected static final int ERROR_DELETING_CONTACT_DIALOG_ID = 1;

	/**
	 * View contact context menu item identifier.
	 */
	protected static final int MENU_ITEM_VIEW = 1;
	/**
	 * Call contact option menu item identifier.
	 */
	protected static final int MENU_ITEM_CALL = 2;
	/**
	 * New contact options menu item identifier.
	 */
	protected static final int MENU_ITEM_NEW = 3;
	/**
	 * Edit contact context option menu item identifier.
	 */
	protected static final int MENU_ITEM_EDIT = 4;
	/**
	 * Delete contact context option menu item identifier.
	 */
	protected static final int MENU_ITEM_DELETE = 5;
	/**
	 * Search contact option menu item identifier.
	 */
	protected static final int MENU_ITEM_SEARCH = 6;
	/**
	 * Add contact to Favorite or remove from it context menu item identifier.
	 */
	protected static final int MENU_ITEM_TOGGLE_STAR = 7;
	/**
	 * Send contact email menu item identifier.
	 */
	protected static final int MENU_ITEM_EMAIL = 8;

	// ======================================================
	// Modes and mask items ID
	// ======================================================

	/**
	 * Mask for indicate work with contacts
	 */
	protected static final int MODE_MASK_CONTACTS = 0x00000000;
	/**
	 * Mask for extension mode
	 */
	protected static final int MODE_MASK_EXTENSION = 0x80000000;
	/**
	 * Mask for photo Show mode
	 */
	protected static final int MODE_MASK_SHOW_PHOTOS = 0x40000000;

	protected int mMode = MODE_DEFAULT;

	/**
	 * Default mode
	 */
	protected static final int MODE_DEFAULT = MODE_MASK_CONTACTS;

	protected int mScrollState;
	protected static ExecutorService sImageFetchThreadPool;

	/**
	 * Keeps the cursor adapter.
	 */
	protected BaseContactsAdapter mAdapter;

	/**
	 * Keeps query handler.
	 */
	protected AsyncQueryHandler mQueryHandler;

	/**
	 * Flag which indicate first start
	 */
	protected boolean mJustCreated;

	/**
	 * The query token.
	 */
	protected static final int QUERY_TOKEN = 54;

	/**
	 * New contact request.
	 */
	protected static final int NEW_CONTACT_REQUEST = 77;

	/**
	 * Edit contact request.
	 */
	protected static final int EDIT_CONTACT_REQUEST = 78;

	/**
	 * View contact request.
	 */
	protected static final int VIEW_CONTACT_REQUEST = 80;

	private static final int EMPTY_MSG = 0;

	private ContentChangeObserver mContentChangeObserver;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (LogSettings.ENGINEERING) {
			EngLog.i(TAG, "Bind to local sync service");
		}
		getApplicationContext().bindService(new Intent(this, LocalSyncService.class), syncSvcConn, BIND_AUTO_CREATE);

	}

	@Override
	protected void onResume() {
		super.onResume();

		NetworkManager.getInstance().refreshExtensionsList(this);

		if (mAdapter.mBitmapCache != null) {
			mAdapter.mBitmapCache.clear();
		}

		mScrollState = OnScrollListener.SCROLL_STATE_IDLE;
		boolean runQuery = true;
		if (mJustCreated) {
			getListView().clearTextFilter();
		}

		if (mJustCreated && runQuery) {
			startQuery(mMode);
		}
		mJustCreated = false;
	}

	/**
	 * Called on Restart
	 */
	@Override
	protected void onRestart() {
		super.onRestart();

		if (TextUtils.isEmpty(getListView().getTextFilter())) {
			startQuery(mMode);
		} else {
			((BaseContactsAdapter) getListAdapter()).onContentChanged();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_CALL: {
			return false;
		}
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryTypes.onStartSession(this);
	}

	/**
	 * Called on stop.
	 */
	@Override
	protected void onStop() {
		super.onStop();
		FlurryTypes.onEndSession(this);
		mAdapter.setLoading(true);
		mAdapter.changeCursor(null);
	}

	/**
	 * Initial common part of Contacts and Favorite screen
	 */
	protected final void initCommon() {
		/**
		 * initialize controls
		 */
		mEmtytext = (TextView) findViewById(R.id.emptyListText);
		mImgContact = (ImageView) findViewById(R.id.emptyListImage);
		mLoadingBar = (ProgressBar) findViewById(R.id.loading);

		/**
		 * Setup Sub menu tabs
		 */
		initSabTabs();

		/**
		 * Set the proper empty string
		 */
		setEmptyText(mMode);

		/**
		 * initialize QueryHandler
		 */
		mJustCreated = true;
		registerForContextMenu(getListView());

		/**
		 * Register for change mailbox id notification
		 */
		Uri uri = UriHelper.getUriMailboxCurrent();
		mContentChangeObserver = new ContentChangeObserver(null);
		getContentResolver().registerContentObserver(uri, true, mContentChangeObserver);

		mRingOutAgent = new RingOutAgent(this);
	}

	/**
	 * Process querying data in multiple modes
	 */
	protected void startQuery(int mode) {
		mAdapter.setLoading(true);
		setLoading(true);
		mQueryHandler.cancelOperation(QUERY_TOKEN);

		if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
			queryExtension();
		} else {
			queryPersonal();
		}
	}

	/**
	 * Free resources.
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (mQueryHandler != null) {
			mQueryHandler.cancelOperation(QUERY_TOKEN);
			mQueryHandler = null;
		}

		if (syncService != null) {
			getApplicationContext().unbindService(syncSvcConn);
			syncService = null;
			syncSvcConn = null;
		}

		// Workaround for Android Issue 8488
		// http://code.google.com/p/android/issues/detail?id=8488
		if (mAdapter.mBitmapCache != null) {
			for (SoftReference<Bitmap> bitmap : mAdapter.mBitmapCache.values()) {
				if (bitmap != null && bitmap.get() != null) {
					bitmap.get().recycle();
					bitmap = null;
				}
			}
			mAdapter.mBitmapCache.clear();
			System.gc();
		}

		try {
			getContentResolver().unregisterContentObserver(mContentChangeObserver);
			mContentChangeObserver = null;
		} catch (java.lang.Throwable th) {
		}

		mRingOutAgent = null;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case ERROR_DELETING_CONTACT_DIALOG_ID:
			return RcAlertDialog.getBuilder(this).setTitle(R.string.dialog_title_error).setMessage(R.string.contacts_del_error).setIcon(R.drawable.symbol_error).setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
				}
			}).create();

		default:
			return null;
		}
	}

	/**
	 * Item view cache holder.
	 */
	protected static final class ContactListItemCache {
		/**
		 * Person name view.
		 */
		public TextView nameView;
		/**
		 * Person phone number view.
		 */
		public TextView typeView;
		public ImageView presenceView;
		/**
		 * Person photo cache holder.
		 */
		public ImageView photoView;
	}

	/**
	 * Item Photo info class
	 */
	final static class PhotoInfo {
		public int position;
		public long photoId;

		public PhotoInfo(int position, long photoId) {
			this.position = position;
			this.photoId = photoId;
		}

		public ImageView photoView;
	}

	/**
	 * Item Photo info class
	 */
	final static class ContactInfo {
		public int position;
		long mFavoriteId;
		long mContactId;
		long mPhoneId;
		public TextView mNameView;
		public TextView mTypeView;
		public TextView mPhoneView;

		public ContactInfo(int position, long favorideID, long contactID, long phoneID, TextView nameView, TextView tymeView, TextView phoneView) {
			this.position = position;
			this.mFavoriteId = favorideID;
			this.mContactId = contactID;
			this.mPhoneId = phoneID;
			this.mNameView = nameView;
			this.mTypeView = tymeView;
			this.mPhoneView = phoneView;
		}
	}

	// ==================================================================================================
	// ContactsAdapter
	// ==================================================================================================

	/**
	 * Cursor adapter.
	 */
	protected abstract class BaseContactsAdapter extends ResourceCursorAdapter implements OnScrollListener {

		protected HashMap<Long, SoftReference<Bitmap>> mBitmapCache = null;
		protected HashSet<ImageView> mItemsMissingImages = null;
		protected ImageLoaderHandler mHandler;
		protected ImageLoader mImageFetcher;
		ViewTreeObserver.OnPreDrawListener mPreDrawListener;
		protected static final int FETCH_IMAGE_MSG = 1;

		protected boolean mDisplayPhotos = false;

		/**
		 * Defines id data are loading.
		 */
		protected boolean mLoading = true;

		/**
		 * Default constructor.
		 */
		public BaseContactsAdapter(Context context, int resourceId) {
			super(context, resourceId, null);
			mHandler = new ImageLoaderHandler();
			if ((mMode & MODE_MASK_SHOW_PHOTOS) == MODE_MASK_SHOW_PHOTOS) {
				mDisplayPhotos = true;
				mBitmapCache = new HashMap<Long, SoftReference<Bitmap>>();
				mItemsMissingImages = new HashSet<ImageView>();
			} else {
				mDisplayPhotos = false;
			}
		}

		/**
		 * Sets data loading state.
		 * 
		 * @param loading
		 *            the loading state
		 */
		protected void setLoading(boolean loading) {
			mLoading = loading;
		}

		/**
		 * Defines if there are data.
		 */
		@Override
		public boolean isEmpty() {
			if (mLoading) {
				return false;
			} else {
				return super.isEmpty();
			}
		}

		@Override
		protected void onContentChanged() {
			CharSequence constraint = getListView().getTextFilter();
			if (!TextUtils.isEmpty(constraint)) {
				Filter filter = getFilter();
				filter.filter(constraint);
			} else {
				startQuery(mMode);
			}
		}

		// =====Photo Loader Helper ==========================================
		private class ImageLoaderHandler extends Handler {

			@Override
			public void handleMessage(Message message) {
				if (isFinishing()) {
					return;
				}
				switch (message.what) {
				case FETCH_IMAGE_MSG: {
					final ImageView imageView = (ImageView) message.obj;
					if (imageView == null) {
						break;
					}

					final PhotoInfo info = (PhotoInfo) imageView.getTag();
					if (info == null) {
						break;
					}

					final long photoId = info.photoId;
					if (photoId == 0) {
						break;
					}

					SoftReference<Bitmap> photoRef = mBitmapCache.get(photoId);
					if (photoRef == null) {
						if (LogSettings.ENGINEERING) {
							EngLog.w(TAG, " SoftReference<Bitmap> photoRef == null");
						}
						break;
					}
					Bitmap photo = photoRef.get();
					if (photo == null) {
						mBitmapCache.remove(photoId);
						break;
					}

					synchronized (imageView) {
						final PhotoInfo updatedInfo = (PhotoInfo) imageView.getTag();
						long currentPhotoId = updatedInfo.photoId;
						if (currentPhotoId == photoId) {
							imageView.setImageBitmap(photo);
							mItemsMissingImages.remove(imageView);
						} else {
						}
					}
					break;
				}
				}
			}

			public void clearImageFecthing() {
				removeMessages(FETCH_IMAGE_MSG);
			}
		}

		private class ImageLoader implements Runnable {
			long mPhotoId;
			private ImageView mImageView;

			public ImageLoader(long photoId, ImageView imageView) {
				this.mPhotoId = photoId;
				this.mImageView = imageView;
			}

			public void run() {
				if (isFinishing()) {
					return;
				}

				if (Thread.interrupted()) {
					return;
				}

				if (mPhotoId < 0) {
					return;
				}

				Bitmap photo = Cont.acts().getContactPhoto(getBaseContext(), mPhotoId, null);
				if (photo == null) {
					return;
				}

				mBitmapCache.put(mPhotoId, new SoftReference<Bitmap>(photo));

				if (Thread.interrupted()) {
					return;
				}

				Message msg = new Message();
				msg.what = FETCH_IMAGE_MSG;
				msg.obj = mImageView;
				mHandler.sendMessage(msg);
			}
		}

		public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {

		}

		public void onScrollStateChanged(AbsListView view, int scrollState) {
			mScrollState = scrollState;
			if ((mMode & MODE_MASK_EXTENSION) != MODE_MASK_EXTENSION) {
				if (scrollState == OnScrollListener.SCROLL_STATE_FLING) {
					clearImageFetching();
				} else if (mDisplayPhotos) {
					processMissingImageItems(view);
				}
			}
		}

		private void processMissingImageItems(AbsListView view) {
			for (ImageView iv : mItemsMissingImages) {
				sendFetchImageMessage(iv);
			}
		}

		protected void sendFetchImageMessage(ImageView view) {
			final PhotoInfo info = (PhotoInfo) view.getTag();
			if (info == null) {
				return;
			}
			final long photoId = info.photoId;
			if (photoId == 0) {
				return;
			}
			mImageFetcher = new ImageLoader(photoId, view);
			synchronized (BaseContactsActivity.this) {
				if (sImageFetchThreadPool == null) {
					sImageFetchThreadPool = Executors.newFixedThreadPool(3);
				}
				sImageFetchThreadPool.execute(mImageFetcher);
			}
		}

		public void clearImageFetching() {
			synchronized (BaseContactsActivity.this) {
				if (sImageFetchThreadPool != null) {
					sImageFetchThreadPool.shutdownNow();
					sImageFetchThreadPool = null;
				}
			}

			mHandler.clearImageFecthing();
		}

		protected void setContactPhoto(Cursor cursor, final ContactListItemCache cache, int column) {
			long photoId = 0;

			if (!cursor.isNull(column)) {
				photoId = cursor.getLong(column);
			}

			ImageView viewToUse = cache.photoView;
			final int position = cursor.getPosition();
			viewToUse.setTag(new PhotoInfo(position, photoId));

			if (photoId == 0) {
				viewToUse.setImageResource(R.drawable.ic_contact_list_picture);
			} else {

				Bitmap photo = null;

				SoftReference<Bitmap> ref = mBitmapCache.get(photoId);
				if (ref != null) {

					photo = ref.get();
					if (photo == null) {
						if (LogSettings.ENGINEERING) {
							EngLog.w(TAG, "photo is null, photoId=" + photoId + "  mBitmapCache.remove(photoId);");
						}
						mBitmapCache.remove(photoId);
					}
				}

				if (photo != null) {
					viewToUse.setImageBitmap(photo);
				} else {
					viewToUse.setImageResource(R.drawable.ic_contact_list_picture);
					mItemsMissingImages.add(viewToUse);
					if (mScrollState != OnScrollListener.SCROLL_STATE_FLING) {
						sendFetchImageMessage(viewToUse);
					}
				}
			}
		}
	}

	/**
	 * On sub-activity result.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case (VIEW_CONTACT_REQUEST): {
		}
			break;
		default:
			break;
		}
	}

	/**
	 * Initialize sub tabs
	 */
	protected void initSabTabs() {
		boolean showBar = UserInfo.companyExtensionsAllowed();
		mSubTabs = (LinearLayout) findViewById(R.id.subtabs);

		if (showBar) {
			mSubTabs.setVisibility(View.VISIBLE);
			final ToggleButton btnLeft = (ToggleButton) findViewById(R.id.btnLeft);
			String strPersonal = getString(R.string.filter_name_personal);
			btnLeft.setTextOn(strPersonal);
			btnLeft.setTextOff(strPersonal);
			btnLeft.setText(strPersonal);
			btnLeft.setChecked(true);

			final ToggleButton btnRight = (ToggleButton) findViewById(R.id.btnRight);
			String strCompany = getString(R.string.filter_name_company);
			btnRight.setTextOn(strCompany);
			btnRight.setTextOff(strCompany);
			btnRight.setText(strCompany);

			btnLeft.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (LogSettings.MARKET) {
						EngLog.d(TAG, "User: Personal");
					}
					if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
						PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.CONTACTS_LOG_SWITCH);
					}
					btnLeft.setChecked(true);
					btnRight.setChecked(false);
					switchTab(false);
				}
			});

			btnRight.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (LogSettings.MARKET) {
						EngLog.d(TAG, "User: Company");
					}
					btnLeft.setChecked(false);
					btnRight.setChecked(true);
					switchTab(true);
				}
			});
		} else {
			mSubTabs.setVisibility(View.GONE);
		}

	}

	/**
	 * Signal an error to the user.
	 */
	protected void signalError(String text) {
		Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
	}

	protected void callExtension(Cursor cursor) {
		String number = cursor.getString(Extensions.EXT_PIN_COLUMN_INDEX);
		String name = cursor.getString(Extensions.EXT_NAME_COLUMN_INDEX);
		mRingOutAgent.call(number, name);
	}

	protected void makeRingOut(PhoneNumber pn, String name) {
		if (LogSettings.MARKET) {
			QaLog.w(TAG, "RingOut");
		}
		try {
			mRingOutAgent.call(pn.numRaw, name);
		} catch (java.lang.Throwable error) {
			if (LogSettings.MARKET) {
				QaLog.e(TAG, " makeRingOut error: " + error.getMessage());
			}
		}
	}

	/**
	 * Get sorting String for Contacts display
	 */
	protected static String getSortOrder(String fieldName) {
		return "CASE WHEN substr(UPPER(" + fieldName + "), 1, 1) BETWEEN 'A' AND 'Z' THEN 1 else 10 END," +
				fieldName + " COLLATE LOCALIZED ASC";
	}

	/**
	 * set image and text when list is empty
	 */
	protected void setEmptyText(int mode) {
		mEmtytext.setText(getText(R.string.noContacts));
		mEmtytext.setGravity(Gravity.CENTER);
	}

	/**
	 * Set Default text if Contacts are empty
	 */
	protected void setLoading(boolean enable) {
		if (enable) {
			mShowLoading = true;
			mEmtytext.setVisibility(View.GONE);
			mImgContact.setVisibility(View.GONE);
			new Thread("ContactsListAct:setLoading") {
				public void run() {
					try {
						sleep(SHOW_PROGRESS_DELAY);
					} catch (InterruptedException e) {
						if (LogSettings.ENGINEERING) {
							EngLog.w(TAG, "setLoading()", e);
						}
					}
					if (mShowLoading) {
						showLoadingHandler.sendEmptyMessage(EMPTY_MSG);
					}
				}

			}.start();
		} else {
			mShowLoading = false;
			mEmtytext.setVisibility(View.VISIBLE);
			mImgContact.setVisibility(View.VISIBLE);
			mLoadingBar.setVisibility(View.GONE);
		}
	}

	/**
	 * Set contextMenu of company's contact
	 */
	protected void setExtensionContextMenu(ContextMenu menu, Cursor cursor) {
		/**
		 * Call
		 */
		menu.add(0, MENU_ITEM_CALL, Menu.NONE, R.string.menu_call);

		String email = cursor.getString(Extensions.EXT_EMAIL_COLUMN_INDEX);
		if (!((email == null) || (email.equals("null")) || (email.equals("")))) {
			menu.add(0, MENU_ITEM_EMAIL, Menu.NONE, R.string.menu_sendEmail);
		}

		/**
		 * View
		 */
		menu.add(Menu.NONE, MENU_ITEM_VIEW, Menu.NONE, R.string.menu_viewContact);

		/**
		 * Add/Remove from favorites (extensions)
		 */
		int starState = cursor.getInt(Extensions.EXT_STARRED_COLUMN_INDEX);
		if (starState == 0) {
			menu.add(Menu.NONE, MENU_ITEM_TOGGLE_STAR, Menu.NONE, R.string.menu_addStar);
		} else {
			menu.add(Menu.NONE, MENU_ITEM_TOGGLE_STAR, Menu.NONE, R.string.menu_removeStar);
		}
	}

	/**
	 * Set contextMenu of personal's contact
	 */
	protected abstract void setPersonalContextMenu(ContextMenu menu, Cursor cursor);

	/**
	 * Get the title of the context menu
	 */
	protected abstract CharSequence getMenuTitle(AdapterView.AdapterContextMenuInfo info);

	/**
	 * switchToPersonal
	 */
	protected abstract void switchTab(boolean isToCompany);

	/**
	 * Query under personal mode
	 */
	protected abstract void queryPersonal();

	/**
	 * Query under extension mode
	 */
	protected abstract void queryExtension();

	/**
	 * Switch to RingOut page and insert Extension Number. If we have not one number -> call Selection
	 */
	protected boolean sendEmail(Cursor cursor, long personalContactId) {
		if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
			return sendExtensionEmail(cursor);
		} else {
			return sendPersonalEmail(cursor, personalContactId);
		}
	}

	private boolean sendExtensionEmail(Cursor cursor) {
		String email = cursor.getString(Extensions.EXT_EMAIL_COLUMN_INDEX);
		EmailSender emailSender = new EmailSender(this);
		String[] to = { email };
		emailSender.sendEmail(to, "", "", null);
		return true;
	}

	private boolean sendPersonalEmail(Cursor cursor, long contactId) {
		if (contactId == -1) {
			return false;
		}

		List<Cont.acts.EmailContact> emails = Cont.acts().getEmailAddresses(this, contactId);
		if (emails.size() == 0) {
			signalError("No email address for this record");
			return false;
		} else if (emails.size() == 1) {
			EmailSender emailSender = new EmailSender(this);
			String[] to = { emails.get(0).emailAddress };
			emailSender.sendEmail(to, "", "", null);
		} else {
			EmailSelectionDialog emailDialog = new EmailSelectionDialog(this, emails);
			emailDialog.show();
		}

		return true;
	}

	/**
	 * On context menu creation.
	 */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		AdapterView.AdapterContextMenuInfo info; // FavoriteMode & ContactMode
		try {
			try {
				info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			} catch (ClassCastException e) {
				if (LogSettings.ENGINEERING) {
					EngLog.e(TAG, "onCreateContextMenu() bad menuInfo", e);
				} else if (LogSettings.MARKET) {
					QaLog.e(TAG, "onCreateContextMenu() bad menuInfo: " + e.getMessage());
				}
				return;
			}

			Cursor cursor = (Cursor) getListAdapter().getItem(info.position);
			if (cursor == null) {
				return;
			}
			menu.setHeaderTitle(getMenuTitle(info));

			/**
			 * set context menu
			 */
			if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
				setExtensionContextMenu(menu, cursor);
			} else {
				setPersonalContextMenu(menu, cursor);
			}

		} catch (java.lang.Throwable error) {
			if (LogSettings.MARKET) {
				MktLog.e(TAG, " onCreateContextMenu error: " + error.getMessage());
			}
		}
	}

	class ContentChangeObserver extends ContentObserver {
		public ContentChangeObserver(Handler handler) {
			super(handler);
		}

		@Override
		public boolean deliverSelfNotifications() {
			return false;
		}

		@Override
		public void onChange(boolean arg0) {
			super.onChange(arg0);
		}
	}

	private Handler showLoadingHandler = new Handler() {

		public void handleMessage(Message msg) {
			if (mShowLoading) {
				mEmtytext.setVisibility(View.GONE);
				mImgContact.setVisibility(View.GONE);
				mLoadingBar.setVisibility(View.VISIBLE);
			}
		}
	};

}