/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.Contacts;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.contacts.Cont.acts.BindSync;
import com.ringcentral.android.contacts.Cont.acts.ContactBinding;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.provider.RCMDataStore.FavoritesTable;
import com.ringcentral.android.search.ContactsSearchAgent;
import com.ringcentral.android.search.ContactsSearchAgent1x;
import com.ringcentral.android.utils.PhoneUtils;

/**
 * Underlying platform Contacts proxy for Android below 2.0
 */
@SuppressWarnings("deprecation")
public class ContactProxy1x extends ContactProxy {
    /**
     * Define logging tag.
     */
    private static final String LOG_TAG = "[RC] ContactProxy1x";
    
    @Override
    public Uri getPeopleUri() {
        return android.provider.Contacts.People.CONTENT_URI;
    }
    
    @Override
    public Intent getCreateNewContactIntent(Context context, String contactName, String contactNumber) {
        Intent i = new Intent(android.provider.Contacts.Intents.Insert.ACTION, android.provider.Contacts.People.CONTENT_URI);
        i.putExtra(android.provider.Contacts.Intents.Insert.NAME, contactName);
        i.putExtra(android.provider.Contacts.Intents.Insert.PHONE, contactNumber);
        return i;
    }
    
    @Override
    public Intent getAddToContactIntent(Context context, String contactNumber) {
        Intent i = new Intent(Intent.ACTION_INSERT_OR_EDIT);
        i.setType(android.provider.Contacts.People.CONTENT_ITEM_TYPE);
        i.putExtra(Contacts.Intents.Insert.PHONE, contactNumber);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        return i;
    }
    
    @Override
    public long lookupContactIDByPhoneNumber(String phoneNumber, Context context) {
        try {
            Uri uri = Uri.withAppendedPath(android.provider.Contacts.Phones.CONTENT_FILTER_URL, Uri.encode(phoneNumber));
            Cursor cursor = context.getContentResolver().query(uri, PROJECTION1, null, null, null);
            if (cursor != null) {
                long id = -1;
                if (cursor.moveToFirst()) {
                    id = cursor.getLong(PERSON_ID_INDX1);
                }
                cursor.close();
                return id;
            }
        } catch (Exception ex) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "Look-up contact by phone number failed: " + ex.getMessage());
            }
        }

        return -1;
    }
    private static final String[] PROJECTION1 = {android.provider.Contacts.Phones.PERSON_ID};
    private static final int PERSON_ID_INDX1 = 0;

    @Override
    public List<Cont.acts.EmailContact> getEmailAddresses(Context context, long contactId) {
        ArrayList<Cont.acts.EmailContact> list = new ArrayList<Cont.acts.EmailContact>();
        if (contactId < 0) {
            if (LogSettings.MARKET) {
                QaLog.w(LOG_TAG, "getEmailAddresses error: id is invalid");
            }
            return list;
        }
        
        Cursor c = null;
        try {
            c = context.getContentResolver().query(android.provider.Contacts.ContactMethods.CONTENT_EMAIL_URI,
                    PROJECTION10, android.provider.Contacts.ContactMethods.PERSON_ID + " = ?",
                    new String[] { String.valueOf(contactId) }, null);
            if (c != null) {
                c.moveToPosition(-1);
                while (c.moveToNext()) {
                    try {
                        Cont.acts.EmailContact e = new Cont.acts.EmailContact();
                        e.displayName = c.getString(NAME_INDX10);
                        e.contactId = contactId;
                        e.emailAddress = c.getString(DATA_INDX10);
                        e.emailTag = "";
                        Cursor c2 = null;
                        Resources res = context.getResources();
                        try {
                            c2 = context.getContentResolver().query(
                                    android.provider.Contacts.ContactMethods.CONTENT_URI, 
                                    PROJECTION11,
                                    android.provider.Contacts.ContactMethods.PERSON_ID + " = ? AND " +
                                    android.provider.Contacts.ContactMethods.DATA + " = ?",
                                    new String[] { String.valueOf(contactId), e.emailAddress }, null);
                            if (c2 != null && c2.moveToFirst()) {
                                int type = c2.getInt(TYPE_INDX11);
                                if (type == android.provider.Contacts.ContactMethods.TYPE_CUSTOM) {
                                    e.emailTag = c2.getString(LABEL_INDX11);
                                } else {
                                    if (type == android.provider.Contacts.ContactMethods.TYPE_HOME) {
                                        e.emailTag = res.getString(R.string.email_tag_home);
                                    } else if (type == android.provider.Contacts.ContactMethods.TYPE_WORK) {
                                        e.emailTag = res.getString(R.string.email_tag_work);
                                    } else if (type == android.provider.Contacts.ContactMethods.TYPE_OTHER) {
                                        e.emailTag = res.getString(R.string.email_tag_other);
                                    }
                                }
                                
                                if (e.displayName == null || e.displayName.trim().length() < 1) {
                                    e.displayName = c2.getString(DISPLAY_NAME_INDX11);
                                }
                                 
                            }
                        } catch (java.lang.Throwable error) {
                            if (LogSettings.MARKET) {
                                QaLog.e(LOG_TAG, "getEmailAddressesType - read error: " + error.getMessage());
                            }
                            e.emailTag = res.getString(R.string.email_tag_other);
                        } finally {
                            if (c2 != null) {
                                c2.close();
                            }
                        }
                        list.add(e);
                    } catch (java.lang.Throwable error) {
                        if (LogSettings.MARKET) {
                            QaLog.e(LOG_TAG, "getEmailAddresses - read error: " + error.getMessage());
                        }
                    }
                }
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "getEmailAddresses: " + error.getMessage());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return list;

    }

    private static final String[] PROJECTION10 = { 
        android.provider.Contacts.ContactMethods.NAME, 
        android.provider.Contacts.ContactMethods.DATA };
    private static final int NAME_INDX10 = 0;
    private static final int DATA_INDX10 = 1;
    private static final String[] PROJECTION11 = { 
        android.provider.Contacts.ContactMethods.TYPE,
        android.provider.Contacts.ContactMethods.LABEL,
        android.provider.Contacts.ContactMethods.DISPLAY_NAME};
    private static final int TYPE_INDX11 = 0;
    private static final int LABEL_INDX11 = 1;
    private static final int DISPLAY_NAME_INDX11 = 2;
   
    
    @Override
    public int addToPersonalFavorites(long phoneId, String originalNumber, String normalizedNumber, Context context) {
        if (PersonalFavorites.isFavorite(phoneId, context)) {
            return FV_RESULT_ADDED;
        }

//        if (PersonalFavorites.isFavorite(normalizedNumber, context)) {
//            return FV_RESULT_ADDED;
//        }

        long contactId = -1;
        long photoId = -1;
        long type = android.provider.Contacts.Phones.TYPE_CUSTOM;
        String number = null;
        Cursor photoCursor = null;
        Cursor cursor = null;
        String displayName = null;
        String label = null;
        try {
            Uri phoneUri = ContentUris.withAppendedId(android.provider.Contacts.Phones.CONTENT_URI, phoneId);
            cursor = context.getContentResolver().query(phoneUri, PROJECTION2, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    contactId = cursor.getLong(PERSON_ID_INDX2);
                    number = cursor.getString(NUMBRER_INDX2);
                    type = cursor.getLong(TYPE_INDX2);
                    displayName = cursor.getString(DISPLAY_NAME_INDX2);
                    label = cursor.getString(LABEL_INDX2);
                    if (contactId != -1) {
                        Uri personUri = ContentUris.withAppendedId(android.provider.Contacts.People.CONTENT_URI, contactId);
                        Uri photoUri = Uri.withAppendedPath(personUri, android.provider.Contacts.Photos.CONTENT_DIRECTORY);

                        photoCursor = context.getContentResolver().query(photoUri, PROJECTION3,
                                null, null, null);

                        if (photoCursor != null) {
                            if (photoCursor.moveToFirst()) {
                                photoId = photoCursor.getLong(ID_INDX3);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "addToPersonalFavorites() failed: " + ex.getMessage());
            }
        }

        if (cursor != null) {
            cursor.close();
        }
        
        if (photoCursor != null) {
            photoCursor.close();
        }
        
        if (contactId == -1 || number == null) {
            if (LogSettings.MARKET) {
                MktLog.w("RingCentral", "Add to favorites: contact not found.");
            }
            return FV_RESULT_CONTACT_RECORD_NOT_FOUND;
        }
        
        if (!number.equals(originalNumber)) {
            ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumber(number);
            if (!cpn.isValid) {
                if (LogSettings.MARKET) {
                    MktLog.w("RingCentral", "Add to favorites: number changed and is not valid.");
                }
                return FV_RESULT_CONTACT_RECORD_NOT_FOUND;
            }
            normalizedNumber = cpn.normalizedNumber;
        }
        
        long mailBoxID = RCMProviderHelper.getCurrentMailboxId(context);
        ContentValues values = new ContentValues();
        values.put(FavoritesTable.MAILBOX_ID, mailBoxID);
        values.put(FavoritesTable.DISPLAY_NAME, displayName);
        values.put(FavoritesTable.ORIGINAL_NUMBER, number);
        values.put(FavoritesTable.NORMALIZED_NUMBER, normalizedNumber);
        values.put(FavoritesTable.TYPE, type);
        
        if (isCustomTagPhoneNumber(context, type)) {
            values.put(FavoritesTable.LABEL, label);
        } else {
            values.put(FavoritesTable.LABEL, (String)null);
        }
        
        values.put(FavoritesTable.CONTACT_ID, contactId );
        values.put(FavoritesTable.PHONE_ID, phoneId );
        
        if ( photoId != -1){
            values.put(FavoritesTable.PHOTO_ID, photoId );
        }
        if (LogSettings.ENGINEERING) {
            EngLog.i(LOG_TAG, "addToPersonalFavorites(): displayName = " + (displayName == null ? "null" : displayName));
            EngLog.i(LOG_TAG, "addToPersonalFavorites(): original    = " + number);
            EngLog.i(LOG_TAG, "addToPersonalFavorites(): number      = " + normalizedNumber);
            EngLog.i(LOG_TAG, "addToPersonalFavorites(): type        = " + type);
            EngLog.i(LOG_TAG, "addToPersonalFavorites(): contactID   = " + contactId);
            EngLog.i(LOG_TAG, "addToPersonalFavorites(): phoneID     = " + phoneId);
            EngLog.i(LOG_TAG, "addToPersonalFavorites(): photoID     = " + photoId);
        }
        
        Uri uri = UriHelper.getUri(RCMProvider.FAVORITES);
        context.getContentResolver().insert(uri, values);
        
        return FV_RESULT_ADDED;
    }
    private static final String[] PROJECTION2 = { 
        android.provider.Contacts.Phones.PERSON_ID, 
        android.provider.Contacts.Phones.NUMBER,
        android.provider.Contacts.Phones.TYPE, 
        android.provider.Contacts.PeopleColumns.DISPLAY_NAME,
        android.provider.Contacts.Phones.LABEL};
    private static final int PERSON_ID_INDX2 = 0;
    private static final int NUMBRER_INDX2 = 1;
    private static final int TYPE_INDX2 = 2;
    private static final int DISPLAY_NAME_INDX2 = 3;
    private static final int LABEL_INDX2 = 4;
    private static final String[] PROJECTION3 = { 
        android.provider.Contacts.Photos._ID };
    private static final int ID_INDX3 = 0;
    
    @Override
    public String lookupContactNameByPhoneNumber(String phoneNumber, Context context) {
        try {
            Uri uri = Uri.withAppendedPath(android.provider.Contacts.Phones.CONTENT_FILTER_URL, Uri
                    .encode(phoneNumber));
            Cursor cursor = context.getContentResolver().query(uri, PROJECTION4, null, null, null);
            if (cursor != null) {
                String name = null;
                if (cursor.moveToFirst()) {
                    name = cursor.getString(DISPLAY_NAME_INDX4);
                }
                cursor.close();
                return name;
            }
        } catch (Exception ex) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "Look-up contact name by phone number failed: " + ex.getMessage());
            }
        }

        return null;
    }
    private static final String[] PROJECTION4 = {android.provider.Contacts.Phones.DISPLAY_NAME };
    private static final int DISPLAY_NAME_INDX4 = 0;
    
    @Override
    public BindSync syncPersonalBind(Context context, long mailboxId, BindSync bindSync) {
        Cursor cursor = null;
        try {
            Uri phoneUri = ContentUris.withAppendedId(android.provider.Contacts.Phones.CONTENT_URI, bindSync.bind.phoneId);
            cursor = context.getContentResolver().query(
                    phoneUri, PROJECTION5,
                    null, null, null);
            boolean found = false;
            String displayName = null;
            String phoneNumber = null;
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    displayName = cursor.getString(DISPLAY_NAME_INDX5);
                    phoneNumber = cursor.getString(NUMBER_INDX5);
                    int type = cursor.getInt(TYPE_INDX5);
                    bindSync.bind.phoneNumberTag = getPhoneNumberTag(context, type);
                    if (isCustomTagPhoneNumber(context, type)) {
                        String label = cursor.getString(LABEL_INDX5);
                        if (label != null) {
                            bindSync.bind.phoneNumberTag = label;
                        }
                    }
                    bindSync.bind.isPhoneNumberFavorite = PersonalFavorites.isFavorite(bindSync.bind.phoneId, context);
                    bindSync.bind.isDefaultPhoneNumber = (cursor.getLong(IS_PRIMARY_INDX5) > 0);
                    found = true;
                }
            }
            
            if (found) {
                boolean success = true;
                if (!Cont.acts.compareStrings(phoneNumber, bindSync.bind.phoneNumber)) {
                    ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumber(phoneNumber);
                    if (!cpn.isValid) {
                        success = false;
                    } else {
                        if (!Cont.acts.compareStrings(bindSync.bind.cpn.phoneNumber.numRaw, 
                                cpn.phoneNumber.numRaw)) {
                            success = false;
                        } else {
                            bindSync.bind.displayName = displayName;
                            bindSync.bind.phoneNumber = phoneNumber;
                            bindSync.syncState = BindSync.State.UPDATED;
                            return bindSync;
                        }
                    }
                }
                
                if (success) {
                    if (!Cont.acts.compareStrings(displayName, bindSync.bind.displayName)) {
                        bindSync.bind.displayName = displayName;
                        bindSync.syncState = BindSync.State.UPDATED;
                    } else {
                        bindSync.syncState = BindSync.State.NOT_CHANGED;
                        return bindSync;
                    }
                }
            }
            
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "syncPersonalBind: " + error.getMessage());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        
        ContactBinding newBind = Cont.acts.bindContactByNumber(context, mailboxId, bindSync.bind.originalNumber);
        if (newBind.isValid) {
            if (newBind.hasContact) {
                bindSync.bind = newBind;
                bindSync.syncState = BindSync.State.REBIND;
                return bindSync;
            }
        }

        ContactBinding cleanBind = new ContactBinding();
        cleanBind.hasContact = false;
        cleanBind.phoneId = -1;
        cleanBind.isValid = bindSync.bind.isValid;
        cleanBind.cpn = bindSync.bind.cpn;
        cleanBind.originalNumber = bindSync.bind.originalNumber;
        
        bindSync.syncState = BindSync.State.GONE;
        bindSync.bind = cleanBind;
        return bindSync;
    }
    
    @Override
    ContactBinding bindPersonalContact(Context context, long mailboxId, ContactBinding binding) {
        Cursor cursor = null;
        try {
            Uri uri = Uri.withAppendedPath(android.provider.Contacts.Phones.CONTENT_FILTER_URL, Uri
                    .encode(binding.cpn.normalizedNumber));
            cursor = context.getContentResolver().query(uri, PROJECTION5, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    binding.displayName = cursor.getString(DISPLAY_NAME_INDX5);
                    binding.phoneId = cursor.getLong(ID_INDX5);
                    binding.hasContact = true;
                    binding.isPersonalContact = true;
                    int type = cursor.getInt(TYPE_INDX5);
                    binding.phoneNumberTag = getPhoneNumberTag(context, type);
                    if (isCustomTagPhoneNumber(context, type)) {
                        String label = cursor.getString(LABEL_INDX5);
                        if (label != null) {
                            binding.phoneNumberTag = label;
                        }
                    }
                    binding.isPhoneNumberFavorite = PersonalFavorites.isFavorite(binding.phoneId, context);
                    binding.phoneNumber = cursor.getString(NUMBER_INDX5);
                    binding.isDefaultPhoneNumber = (cursor.getLong(IS_PRIMARY_INDX5) > 0);
                }
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "Contact binding: " + error.getMessage());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return binding;
    }
    private static final String[] PROJECTION5 = { 
        android.provider.Contacts.Phones._ID, 
        android.provider.Contacts.Phones.DISPLAY_NAME,
        android.provider.Contacts.Phones.TYPE,
        android.provider.Contacts.Phones.LABEL,
        android.provider.Contacts.Phones.ISPRIMARY,
        android.provider.Contacts.Phones.NUMBER};
    private static final int ID_INDX5 = 0;
    private static final int DISPLAY_NAME_INDX5 = 1;
    private static final int TYPE_INDX5 = 2;
    private static final int LABEL_INDX5 = 3;
    private static final int IS_PRIMARY_INDX5 = 4;
    private static final int NUMBER_INDX5 = 5;
    
    @Override
    public boolean isCustomTagPhoneNumber(Context context, long tag) {
        return (android.provider.Contacts.Phones.TYPE_CUSTOM == tag);
    }
    
    @Override
    public String getPhoneNumberTag(Context context, long tag) {
        switch ((int)tag) {
        case (android.provider.Contacts.Phones.TYPE_CUSTOM):
            return context.getString(R.string.phone_tag_custom);
        case (android.provider.Contacts.Phones.TYPE_MOBILE):
            return context.getString(R.string.phone_tag_mobile);        
        case (android.provider.Contacts.Phones.TYPE_HOME): 
            return context.getString(R.string.phone_tag_home);
        case (android.provider.Contacts.Phones.TYPE_WORK):
            return context.getString(R.string.phone_tag_work);
        case (android.provider.Contacts.Phones.TYPE_OTHER): 
            return context.getString(R.string.phone_tag_other);
        case (android.provider.Contacts.Phones.TYPE_FAX_WORK): 
            return context.getString(R.string.phone_tag_fax_work);
        case (android.provider.Contacts.Phones.TYPE_FAX_HOME): 
            return context.getString(R.string.phone_tag_fax_home);
        case (android.provider.Contacts.Phones.TYPE_PAGER):
            return context.getString(R.string.phone_tag_pager);

        }
        return context.getString(R.string.phone_tag_other);
    }
    
    @Override
    public ContactsSearchAgent getContactsSearchAgent() {
        return ContactsSearchAgent1x.getInstance();
    }
    
    @Override
    public Bitmap getContactPhoto(Context context, long photoId, BitmapFactory.Options options) {
        if (photoId < 0) {
            if (LogSettings.MARKET) {
                EngLog.w(LOG_TAG, "Photo retrieving failed. Id is invalid.");
            }
            return null;
        }
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(ContentUris.withAppendedId(android.provider.Contacts.Photos.CONTENT_URI, photoId),
                    new String[] { android.provider.Contacts.Photos.DATA }, null, null, null);

            if (cursor != null && cursor.moveToFirst() && !cursor.isNull(0)) {
                byte[] photoData = cursor.getBlob(0);
                // Workaround for Android Issue 8488 http://code.google.com/p/android/issues/detail?id=8488
                if (options == null) {
                    options = new BitmapFactory.Options();
                }
                options.inTempStorage = new byte[16 * 1024];
                options.inSampleSize = 2;
                return BitmapFactory.decodeByteArray(photoData, 0, photoData.length, options);
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                QaLog.w(LOG_TAG, "Photo retrieving failed (id = " + photoId + "): " + error.getMessage());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    @Override
    public List<Cont.acts.TaggedContactPhoneNumber> getPersonalContactPhoneNumbers(Context context, long contactId,
            boolean onlyFullValidNumbers) {
        ArrayList<Cont.acts.TaggedContactPhoneNumber> list = new ArrayList<Cont.acts.TaggedContactPhoneNumber>();
        if (contactId < 0) {
            if (LogSettings.MARKET) {
                QaLog.w(LOG_TAG, TAG9 + " error: id is invalid");
            }
            return list;
        }
        
        Cursor cursor = null;
        
        try {
            Uri contactUri = ContentUris.withAppendedId(android.provider.Contacts.People.CONTENT_URI, contactId);
            Uri phonesUri = Uri.withAppendedPath(contactUri, android.provider.Contacts.People.Phones.CONTENT_DIRECTORY);
            cursor = context.getContentResolver().query(phonesUri, PROJECTION9, null, null, null);

            if (cursor != null) {
                cursor.moveToPosition(-1);
                while (cursor.moveToNext()) {
                    try {
                        Cont.acts.TaggedContactPhoneNumber num = new Cont.acts.TaggedContactPhoneNumber();
                        num.cpn = PhoneUtils.getContactPhoneNumber(cursor.getString(NUMBER_INDX9));
                        if (!num.cpn.isValid) {
                            continue;
                        }
                        
                        if (onlyFullValidNumbers && !num.cpn.phoneNumber.isFullValid) {
                            continue;
                        }
                        int type = cursor.getInt(TYPE_INDX9);
                        num.numberTag = getPhoneNumberTag(context, type);
                        if (isCustomTagPhoneNumber(context, type)) {
                            String label = cursor.getString(LABEL_INDX9);
                            if (label != null) {
                                num.numberTag = label;
                            }
                        }
                        if (cursor.getInt(IS_PRIMARY_INDX9) != 0) {
                            num.isDefault = true;
                        }
                        num.id = cursor.getLong(ID_INDX9);
                        list.add(num);
                    } catch (java.lang.Throwable error) {
                        if (LogSettings.MARKET) {
                            QaLog.e(LOG_TAG, TAG9 + " read error: " + error.getMessage());
                        }
                    }
                }
            }
        } catch (java.lang.Throwable ex) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, TAG9 + " error failed: " + ex.getMessage());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return list;
    }
    private static final String[] PROJECTION9 = { 
        android.provider.Contacts.Phones.TYPE,
        android.provider.Contacts.Phones.NUMBER,
        android.provider.Contacts.Phones.ISPRIMARY,
        android.provider.Contacts.Phones.LABEL,
        android.provider.Contacts.Phones._ID,
        };
    private static final int TYPE_INDX9 = 0;
    private static final int NUMBER_INDX9 = 1;
    private static final int IS_PRIMARY_INDX9 = 2;
    private static final int LABEL_INDX9 = 3;
    private static final int ID_INDX9 = 4;
    private static final String TAG9 = "Get personal contacts numbers";
    
    
    
    @Override
    public Cont.acts.PhoneContact lookUpPhoneContactById(Context context, long phoneId) {
        Cont.acts.PhoneContact contact = new Cont.acts.PhoneContact();
        Cursor cursor = null;
        Cursor photoCursor = null;
        try {
            Uri phoneUri = ContentUris.withAppendedId(android.provider.Contacts.Phones.CONTENT_URI, phoneId);
            cursor = context.getContentResolver().query(phoneUri, PROJECTION6, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    contact.phoneId = phoneId;
                    contact.contactId = cursor.getLong(PERSON_ID_INDX6);
                    contact.number = cursor.getString(NUMBRER_INDX6);
                    contact.type = cursor.getInt(TYPE_INDX6);
                    contact.displayName = cursor.getString(DISPLAY_NAME_INDX6);
                    contact.label = cursor.getString(LABEL_INDX6);
                    if (contact.contactId != -1) {
                        Uri personUri = ContentUris.withAppendedId(android.provider.Contacts.People.CONTENT_URI,
                                contact.contactId);
                        Uri photoUri = Uri.withAppendedPath(personUri,
                                android.provider.Contacts.Photos.CONTENT_DIRECTORY);

                        photoCursor = context.getContentResolver().query(photoUri, PROJECTION7, null, null, null);

                        if (photoCursor != null) {
                            if (photoCursor.moveToFirst()) {
                                contact.photoId = photoCursor.getLong(ID_INDX7);
                            }
                        }
                    } else {
                        contact.photoId = -1;
                    }
                    return contact;
                }
            }
        } catch (Exception ex) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "lookUpPhoneContactById() failed: " + ex.getMessage());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            if (photoCursor != null) {
                photoCursor.close();
            }
        }
        return null;
    }

    private static final String[] PROJECTION6 = { 
            android.provider.Contacts.Phones.PERSON_ID,
            android.provider.Contacts.Phones.NUMBER, 
            android.provider.Contacts.Phones.TYPE,
            android.provider.Contacts.PeopleColumns.DISPLAY_NAME,
            android.provider.Contacts.Phones.LABEL};
    private static final int PERSON_ID_INDX6 = 0;
    private static final int NUMBRER_INDX6 = 1;
    private static final int TYPE_INDX6 = 2;
    private static final int DISPLAY_NAME_INDX6 = 3;
    private static final int LABEL_INDX6 = 4;
    private static final String[] PROJECTION7 = { android.provider.Contacts.Photos._ID };
    private static final int ID_INDX7 = 0;

    @Override
    public Cont.acts.PhoneContact lookUpPhoneContactByNumber(Context context, String number) {
        Cursor cursor = null;
        try {
            Uri phoneUri = Uri
                    .withAppendedPath(android.provider.Contacts.Phones.CONTENT_FILTER_URL, Uri.encode(number));
            cursor = context.getContentResolver().query(phoneUri, PROJECTION8, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    return lookUpPhoneContactById(context, cursor.getLong(ID_INDX8));
                }
            }
        } catch (java.lang.Throwable ex) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "lookUpPhoneContactByNumber() failed: " + ex.getMessage());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    private static final String[] PROJECTION8 = { android.provider.Contacts.Phones._ID };
    private static final int ID_INDX8 = 0;
}
