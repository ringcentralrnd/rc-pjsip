/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import java.util.List;

import android.content.ActivityNotFoundException;
import android.content.AsyncQueryHandler;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.PerformanceMeasurementLogger;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.contacts.Projections.Extensions;
import com.ringcentral.android.provider.RCMDataStore.ExtensionsTable;
import com.ringcentral.android.provider.RCMDataStore.FavoritesTable;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.statistics.FlurryTypes;
import com.ringcentral.android.utils.ContactsUtils;
import com.ringcentral.android.utils.PhoneUtils;

public class FavoritesListActivity extends BaseContactsActivity {

	/** Show all starred contacts */
	private static final int MODE_FAVORITES = 2 | MODE_MASK_CONTACTS | MODE_MASK_SHOW_PHOTOS;
	/** Show all starred extensions */
	private static final int MODE_EXTENSIONS_FAV = 4 | MODE_MASK_EXTENSION;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.favorites_list_content);

		/**
		 * Resolve the intent
		 */
		final Intent intent = getIntent();

		/**
		 * Select working mode
		 */
		final String action = intent.getAction();
		if (RCMConstants.ACTION_LIST_FAVORITES.equals(action)) {
			mMode = MODE_FAVORITES;
		} else if (RCMConstants.ACTION_LIST_EXTENSIONS_FAV.equals(action)) {
			mMode = MODE_EXTENSIONS_FAV;
		}

		initCommon();

		setTitle();
		initAddButton();

		/** Initial queryHandler */
		mQueryHandler = new QueryHandler(this);

		/** Set Contacts adapter */
		mAdapter = new FavoritesAdapter();
		setListAdapter(mAdapter);
		getListView().setOnScrollListener(mAdapter);

		/**
		 * Set Tag
		 */
		TAG = "[RC] FavoritesListActivity";
	}

	private void initAddButton() {
		Button btnAdd = (Button) findViewById(R.id.btnTopRight);

		btnAdd.setVisibility(View.VISIBLE);
		btnAdd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(FavoritesListActivity.this, AddFavoritesActivity.class);
				if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
					intent.setAction(RCMConstants.ACTION_LIST_EXTENSIONS);
				} else {
					intent.setAction(RCMConstants.ACTION_LIST_CONTACTS);
				}
				startActivity(intent);
			}
		});
	}

	private void setTitle() {
		TextView title = (TextView) findViewById(R.id.title);
		title.setText(R.string.tab_name_favorites);
	}

	@Override
	protected void onResume() {

		FlurryTypes.onEventScreen(FlurryTypes.EVENT_SCREEN_OPEN, ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) ? FlurryTypes.SCR_FAVORITES_EXTENSION : FlurryTypes.SCR_FAVORITES_PERSONAL);

		if ((syncService != null) && (mMode == MODE_FAVORITES)) {
			try {
				syncService.syncPersonalFavorites(RCMProviderHelper.getCurrentMailboxId(this));
			} catch (java.lang.Throwable error) {
				if (LogSettings.MARKET) {
					QaLog.e(TAG, "onResume() call sync failed: ", error);
				}
			}
		}
		super.onResume();
	}

	/**
	 * Free resources.
	 */

	// ==================================================================================================
	// ContactsAdapter
	// ==================================================================================================
	/**
	 * Cursor adapter.
	 */
	protected final class FavoritesAdapter extends BaseContactsAdapter {

		/** Total numbers of modes */
		private static final int MODE_NUMBERS = 3;

		public FavoritesAdapter() {
			super(FavoritesListActivity.this, R.layout.favorites_list_item_photo);
		}

		/**
		 * On new view
		 */
		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			final View view = super.newView(context, cursor, parent);

			final ContactListItemCache cache = new ContactListItemCache();
			cache.nameView = (TextView) view.findViewById(R.id.name);
			cache.typeView = (TextView) view.findViewById(R.id.type);
			cache.presenceView = (ImageView) view.findViewById(R.id.presence);
			cache.photoView = (ImageView) view.findViewById(R.id.photo);
			view.setTag(cache);

			if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
				cache.typeView.setVisibility(View.VISIBLE);
				cache.photoView.setVisibility(View.GONE);

			} else if (mMode == MODE_FAVORITES) {
				cache.typeView.setVisibility(View.VISIBLE);
				cache.photoView.setVisibility(View.VISIBLE);
			}
			return view;
		}

		/**
		 * On bind view.
		 */
		@Override
		public void bindView(View view, Context context, Cursor cursor) {

			final ContactListItemCache cache = (ContactListItemCache) view.getTag();

			if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
				String name = cursor.getString(Extensions.EXT_NAME_COLUMN_INDEX);
				String number = cursor.getString(Extensions.EXT_PIN_COLUMN_INDEX);

				cache.nameView.setText(name);
				cache.typeView.setText(number);
			} else {
				String name;
				String type;

				try {
					String localCanonic;
					String normalizedNumber = cursor.getString(Favorite.NORMALIZAED_NUMBER_INDX);
					ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumber(normalizedNumber);
					if (cpn.isValid) {
						localCanonic = cpn.phoneNumber.localCanonical;
					} else {
						localCanonic = normalizedNumber;
					}

					if (!cursor.isNull(Favorite.DISPLAY_NAME_INDX)) {
						name = cursor.getString(Favorite.DISPLAY_NAME_INDX);
					} else {
						name = localCanonic;
					}

					long typeInt = 0;
					try {
						typeInt = cursor.getLong(Favorite.TYPE_INDX);
					} catch (Exception ex) {
					}
					type = Cont.acts().getPhoneNumberTag(context, typeInt);

					if (Cont.acts().isCustomTagPhoneNumber(context, typeInt)) {
						String label = cursor.getString(Favorite.LABEL_INDX);
						if (label != null) {
							type = label;
						}
					}

				} catch (java.lang.Throwable error) {
					if (LogSettings.MARKET) {
						QaLog.e(TAG, "favorite reading failed: ", error);
					}
					type = "";
					name = "";
				}

				cache.nameView.setText(name);
				cache.typeView.setVisibility(View.VISIBLE);
				cache.typeView.setText(type);
				// Set the photo, if requested
				if (mDisplayPhotos) {
					setContactPhoto(cursor, cache, Favorite.PHOTO_ID_INDX);
				}
			}
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			return super.getView(position, convertView, parent);
		}

		@Override
		public int getItemViewType(int position) {
			switch (mMode) {
			case MODE_FAVORITES:
				return 0;
			case MODE_EXTENSIONS_FAV:
				return 1;
			default:
				return -1;
			}
		}

		@Override
		public int getViewTypeCount() {
			return MODE_NUMBERS; //
		}

	}

	/**
	 * Asynchronous query handler.
	 */
	protected final class QueryHandler extends AsyncQueryHandler {
		/**
		 * Asynchronous query handler constructor.
		 */
		public QueryHandler(Context context) {
			super(context.getContentResolver());

		}

		/**
		 * On query completion.
		 */
		@Override
		protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
			setLoading(false);
			if (cursor == null) {
				return;
			}
			if (!isFinishing()) {
				mAdapter.setLoading(false);
				mAdapter.changeCursor(cursor);
			} else {
				cursor.close();
			}
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		try {
			Cursor cursor = (Cursor) getListAdapter().getItem(position);
			if (cursor == null) {
				return;
			}
			if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
				FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, FlurryTypes.SCR_FAVORITES_EXTENSION);
				if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
					PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.CALL_FROM_CONTACTS);
				}
				callExtension(cursor);
				return;
			} else {
				FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, FlurryTypes.SCR_FAVORITES_PERSONAL);
				callFavorite(cursor);
				return;
			}
		} catch (java.lang.Throwable error) {
			if (LogSettings.MARKET) {
				QaLog.e(TAG, " onListItemClick error: " + error.getMessage());
			}
		}
	}

	protected final void queryPersonal() {
		mQueryHandler.startQuery(QUERY_TOKEN, null, UriHelper.getUri(RCMProvider.FAVORITES, RCMProviderHelper.getCurrentMailboxId(this)), Favorite.PROJECTION, null, null, getSortOrder(FavoritesTable.DISPLAY_NAME));

	}

	protected final void queryExtension() {
		mQueryHandler.startQuery(QUERY_TOKEN, null, UriHelper.getUri(RCMProvider.EXTENSIONS, RCMProviderHelper.getCurrentMailboxId(this)), Extensions.EXTENSION_SUMMARY_PROJECTION, ExtensionsTable.RCM_STARRED + "=1", null, getSortOrder(ExtensionsTable.RCM_DISPLAY_NAME));
	}

	protected void switchTab(boolean isToCompany) {
		if (isToCompany == false && mMode != MODE_FAVORITES) {
			mMode = MODE_FAVORITES;
			FlurryTypes.onEventScreen(FlurryTypes.EVENT_SCREEN_OPEN, FlurryTypes.SCR_FAVORITES_PERSONAL);
			FlurryTypes.onEventScreen(FlurryTypes.EVENT_FAVORITES_OPEN, FlurryTypes.SCR_FAVORITES_PERSONAL);

			if ((syncService != null)) {
				try {
					syncService.syncPersonalFavorites(RCMProviderHelper.getCurrentMailboxId(this));
				} catch (java.lang.Throwable error) {
					if (LogSettings.MARKET) {
						QaLog.e(TAG, "switchToPersonal() call sync failed: ", error);
					}
				}
			}
		} else if (isToCompany == true && mMode != MODE_EXTENSIONS_FAV) {
			mMode = MODE_EXTENSIONS_FAV;
			FlurryTypes.onEventScreen(FlurryTypes.EVENT_SCREEN_OPEN, FlurryTypes.SCR_FAVORITES_EXTENSION);
			FlurryTypes.onEventScreen(FlurryTypes.EVENT_FAVORITES_OPEN, FlurryTypes.SCR_FAVORITES_EXTENSION);
		} else {
			return;
		}

		/**
		 * Set new Empty text to list
		 */
		setEmptyText(mMode);
		if (mAdapter.getCursor() != null) {
			mAdapter.getCursor().close();
		}

		/**
		 * Clear Photo cache
		 */
		mAdapter.mBitmapCache.clear();
		startQuery(mMode);
	}

	@Override
	protected void setEmptyText(int mode) {
		int gravity = Gravity.CENTER;
		switch (mode) {
		case MODE_FAVORITES:
			mEmtytext.setText(getText(R.string.noFavorites));
			mImgContact.setImageResource(R.drawable.ic_favorites_empty);
			break;
		case MODE_EXTENSIONS_FAV:
			mEmtytext.setText(getText(R.string.noExtensionsFav));
			mImgContact.setImageResource(R.drawable.ic_favorites_empty);
			break;
		default:
			break;
		}
		mEmtytext.setGravity(gravity);
	}

	/**
	 * On selection context menu item.
	 */
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		try {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
			long mailboxId = RCMProviderHelper.getCurrentMailboxId(this);
			Cursor cursor = null;
			final Uri personUri;
			if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
				personUri = UriHelper.getUri(RCMProvider.EXTENSIONS, mailboxId, info.id);
				cursor = (Cursor) getListAdapter().getItem(info.position);
			} else {
				cursor = (Cursor) getListAdapter().getItem(info.position);
				long contactId = cursor.getLong(Favorite.CONTACT_ID_INDX);
				personUri = ContentUris.withAppendedId(ContactsUtils.Uri_People, contactId);
			}

			if (cursor == null) {
				return false;
			}

			switch (item.getItemId()) {
			case MENU_ITEM_CALL: {
				return callItem(cursor);
			}
			case MENU_ITEM_EMAIL: {
				if (LogSettings.MARKET) {
					QaLog.d(TAG, "User: EMail");
				}
				long personalContactId = cursor.getLong(Favorite.CONTACT_ID_INDX);
				return sendEmail(cursor, personalContactId);
			}
			case MENU_ITEM_VIEW: {
				return viewItem(personUri);
			}
			case MENU_ITEM_TOGGLE_STAR: {
				return deleteFromFavorite(info, mailboxId, cursor);
			}

			}
		} catch (java.lang.Throwable error) {
			if (LogSettings.MARKET) {
				QaLog.e(TAG, " onContextItemSelected error: " + error.getMessage());
			}
		}
		return false;
	}

	private boolean deleteFromFavorite(AdapterContextMenuInfo info, long mailboxId, Cursor cursor) {
		if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
			if (LogSettings.MARKET) {
				QaLog.d(TAG, "User: Extension: UnSet as Favorite");
			}
			FlurryTypes.onEventScreen(FlurryTypes.EVENT_REMOVE_FAVORITES, FlurryTypes.SCR_CONTACTS_EXTENSION_CONTEXT);
			CompanyFavorites.setAsNotFavorite(this, mailboxId, info.id);
		} else if (mMode == MODE_FAVORITES) {
			if (LogSettings.MARKET) {
				QaLog.d(TAG, "User: Remove Favorite");
			}
			FlurryTypes.onEventScreen(FlurryTypes.EVENT_REMOVE_FAVORITES, FlurryTypes.SCR_FAVORITES_PERSONAL_CONTEXT);
			PersonalFavorites.deletePersonalFavoriteByRecordId(this, mailboxId, info.id);
		}
		return true;
	}

	private boolean viewItem(final Uri personUri) {
		if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
			if (LogSettings.MARKET) {
				QaLog.d(TAG, "User: View extension");
			}
			FlurryTypes.onEvent(FlurryTypes.EVENT_CONTACTS_DETAILS_OPEN, (mMode == MODE_FAVORITES) ? FlurryTypes.SCR_FAVORITES_EXTENSION_DETAILS : FlurryTypes.SCR_CONTACTS_EXTENSION_DETAILS);
			if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
				PerformanceMeasurementLogger.getInstance().start(PerformanceMeasurementLogger.OPEN_CONTACT_DETAILS);
			}
			Intent intent = new Intent(this, ViewExtensionsActivity.class);
			intent.setData(personUri);

			try {
				startActivity(intent);
			} catch (ActivityNotFoundException e) {
				if (LogSettings.ENGINEERING) {
					EngLog.e(TAG, "viewItem()", e);
				} else if (LogSettings.MARKET) {
					QaLog.e(TAG, "viewItem(): " + e.getMessage());
				}
			}
		} else {
			if (LogSettings.MARKET) {
				QaLog.d(TAG, "User: View Contact");
			}
			Intent intent = new Intent(Intent.ACTION_VIEW, personUri);
			try {
				startActivityForResult(Intent.createChooser(intent, getString(R.string.menu_viewContact)), VIEW_CONTACT_REQUEST);
			} catch (android.content.ActivityNotFoundException e) {
				if (LogSettings.MARKET) {
					QaLog.e(TAG, "viewItem(): " + e.getMessage());
				}
			}
		}
		return true;
	}

	private boolean callItem(Cursor cursor) {
		if ((mMode & MODE_MASK_EXTENSION) == MODE_MASK_EXTENSION) {
			if (LogSettings.MARKET) {
				QaLog.d(TAG, "User: Call extension");
			}
			FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, (mMode == MODE_FAVORITES) ? FlurryTypes.SCR_FAVORITES_EXTENSION : FlurryTypes.SCR_CONTACTS_EXTENSION);
			callExtension(cursor);

		} else if (mMode == MODE_FAVORITES) {
			if (LogSettings.MARKET) {
				QaLog.d(TAG, "User: Call favorite");
			}
			FlurryTypes.onEventScreen(FlurryTypes.EVENT_CALL_FROM, FlurryTypes.SCR_FAVORITES_PERSONAL_CONTEXT);
			callFavorite(cursor);
		}
		return true;
	}

	protected void callFavorite(Cursor cursor) {
		String number = cursor.getString(Favorite.NORMALIZAED_NUMBER_INDX);
		String name = cursor.getString(Favorite.DISPLAY_NAME_INDX);
		mRingOutAgent.call(number, name);
	}

	@Override
	protected void setPersonalContextMenu(ContextMenu menu, Cursor cursor) {

		long contactId = cursor.getLong(Favorite.CONTACT_ID_INDX);

		/**
		 * Call
		 */
		menu.add(0, MENU_ITEM_CALL, Menu.NONE, R.string.menu_call);

		/**
		 * View
		 */
		if (contactId != -1) {
			menu.add(Menu.NONE, MENU_ITEM_VIEW, Menu.NONE, R.string.menu_viewContact);
		}

		/**
		 * Remove from Favorites
		 */
		menu.add(Menu.NONE, MENU_ITEM_TOGGLE_STAR, Menu.NONE, R.string.menu_removeStar);

		/**
		 * Send Mail
		 */
		if (contactId != -1) {
			List<Cont.acts.EmailContact> emails = Cont.acts().getEmailAddresses(this, contactId);
			if (emails.size() > 0) {
				menu.add(0, MENU_ITEM_EMAIL, Menu.NONE, R.string.menu_sendEmail);
			}
		}

	}

	/**
	 * Get the title of the context menu
	 */
	protected CharSequence getMenuTitle(AdapterView.AdapterContextMenuInfo info) {
		final ContactListItemCache views = (ContactListItemCache) info.targetView.getTag();
		return views.nameView.getText();
	}

	public static final class Favorite {

		public static final String[] PROJECTION = new String[] { FavoritesTable._ID, FavoritesTable.DISPLAY_NAME, FavoritesTable.ORIGINAL_NUMBER, FavoritesTable.NORMALIZED_NUMBER, FavoritesTable.TYPE, FavoritesTable.CONTACT_ID, FavoritesTable.PHONE_ID, FavoritesTable.PHOTO_ID, FavoritesTable.LABEL };

		public static final int _ID_INDX = 0;
		public static final int DISPLAY_NAME_INDX = 1;
		public static final int ORIGINAL_NUMBER_INDX = 2;
		public static final int NORMALIZAED_NUMBER_INDX = 3;
		public static final int TYPE_INDX = 4;
		public static final int CONTACT_ID_INDX = 5;
		public static final int PHONE_ID_INDX = 6;
		public static final int PHOTO_ID_INDX = 7;
		public static final int LABEL_INDX = 8;
	}
}
