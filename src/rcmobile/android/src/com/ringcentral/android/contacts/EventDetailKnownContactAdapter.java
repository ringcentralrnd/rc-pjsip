package com.ringcentral.android.contacts;

import java.util.ArrayList;
import com.ringcentral.android.R;
import com.ringcentral.android.provider.RCMProviderHelper;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class EventDetailKnownContactAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater mInflater;
	private ArrayList<DetailData> mContactDetailList = null;
	private int mLastItemPosition = 0;

	public EventDetailKnownContactAdapter(Context context,
			ArrayList<DetailData> mContactDetailList) {
		this.mContext = context;
		this.mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mContactDetailList = mContactDetailList;
		mLastItemPosition = mContactDetailList.size() - 1;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return mContactDetailList.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;

		if (convertView == null) {
//			convertView = mInflater.inflate(
//					R.layout.event_detail_known_contact_item, null);
			convertView = mInflater.inflate(R.layout.event_detail_known_contact_item, parent, false);
			holder = new ViewHolder();
			holder.textlayout = (RelativeLayout) convertView
					.findViewById(R.id.textlayout);
			holder.contact_detail_type = (TextView) convertView
					.findViewById(R.id.contact_detail_type);
			holder.contact_detail_phonenum = (TextView) convertView
					.findViewById(R.id.contact_detail_phonenum);

			holder.ic_favoritebtn = (ImageButton) convertView
					.findViewById(R.id.ic_favoritebtn);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

	
		holder.contact_detail_type
				.setText(mContactDetailList.get(position).contactPhoneNumber.numberTag);
		holder.contact_detail_phonenum
				.setText(mContactDetailList.get(position).contactPhoneNumber.cpn.normalizedNumber);
		
		
		holder.ic_favoritebtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub

				if (!mContactDetailList.get(position).isFav) {
					Cont.acts.TaggedContactPhoneNumber num = mContactDetailList
							.get(position).contactPhoneNumber;
					Cont.acts().addToPersonalFavorites(num.id,
							num.cpn.originalNumber, num.cpn.normalizedNumber,
							mContext);

					holder.ic_favoritebtn
							.setImageResource(R.drawable.ic_listbtn_fav_yes);
					mContactDetailList.get(position).isFav = true;

				} else {
					Cont.acts.TaggedContactPhoneNumber num = mContactDetailList
							.get(position).contactPhoneNumber;

					PersonalFavorites.deletePersonalFavorite(mContext,
							RCMProviderHelper.getCurrentMailboxId(mContext),
							num.id);
					holder.ic_favoritebtn
							.setImageResource(R.drawable.ic_listbtn_fav_no);
					mContactDetailList.get(position).isFav = false;

				}

			}
		});
		
		

		if (mContactDetailList.get(position).isFav) {
			holder.ic_favoritebtn
					.setImageResource(R.drawable.ic_listbtn_fav_yes);
		} else {
			holder.ic_favoritebtn
					.setImageResource(R.drawable.ic_listbtn_fav_no);
		}

		if (mContactDetailList.size() == 1) {

			holder.textlayout
					.setBackgroundResource(R.drawable.list_contact_info_one_left_bg);
			holder.ic_favoritebtn
					.setBackgroundResource(R.drawable.list_contact_info_one_right_bg);

		} else {
			if (position == mLastItemPosition) {

				holder.textlayout
						.setBackgroundResource(R.drawable.list_contact_info_bottom_left_bg);
				holder.ic_favoritebtn
						.setBackgroundResource(R.drawable.list_contact_info_bottom_right_bg);
			} else if (position == 0) {
				holder.textlayout
						.setBackgroundResource(R.drawable.list_contact_info_top_left_bg);
				holder.ic_favoritebtn
						.setBackgroundResource(R.drawable.list_contact_info_top_right_bg);
			} else {
				holder.ic_favoritebtn
						.setBackgroundResource(R.drawable.list_contact_info_middle_right_bg);
				holder.textlayout
						.setBackgroundResource(R.drawable.list_contact_info_middle_left_bg);

			}
		}

		return convertView;
	}

	class ViewHolder {
		ImageButton ic_favoritebtn;
		RelativeLayout textlayout;
		RelativeLayout storagelayout;
		TextView contact_detail_type;
		TextView contact_detail_phonenum;
	}

}

class DetailData {
	Cont.acts.TaggedContactPhoneNumber contactPhoneNumber;
	boolean isFav;

	public DetailData(Cont.acts.TaggedContactPhoneNumber contactPhoneNumber,
			boolean isFav) {
		this.contactPhoneNumber = contactPhoneNumber;
		this.isFav = isFav;
	}

	public void setFav(boolean isFav) {
		this.isFav = isFav;
	}

	public boolean getFav() {
		return this.isFav;
	}
}
