/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.QaLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.contacts.LocalSyncService.LocalSyncControl;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.provider.RCMDataStore.FavoritesTable;
import com.ringcentral.android.utils.PhoneUtils;

public class PersonalFavorites {
    private static final String LOG_TAG = "[RC] PersonalFavorites";
    /**
     * Defines if needed to remove duplicated numbers for favorites that have
     * associated contact.
     */
    private static final boolean REMOVE_DUPLICATED_NUMBERS = false;
    
    /**
     * Defines if the personal contact number is a favorite.
     * 
     * @param phoneId
     *            the phone identifier
     * @param context
     *            the execution context
     * @return <code>true</code> if the <code>phoneId</code> exists in the
     *         personal favorites table, otherwise <code>false</code>.
     */
    public static final boolean isFavorite(long phoneId, Context context) {
        Uri uri = UriHelper.getUri(RCMProvider.FAVORITES, RCMProviderHelper.getCurrentMailboxId(context));
        Cursor c = null;
        try {
            c = context.getContentResolver().query(uri, PROJECTION_FV1, FavoritesTable.PHONE_ID + " = ?",
                    new String[] { String.valueOf(phoneId) }, null);
            if (c != null && c.moveToFirst()) {
                return true;
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "isFavorite(phoneId): " + error.getMessage());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return false;
    }
    /**
     * Query projection for {@link #isFavorite(long, Context)}
     */
    private static final String[] PROJECTION_FV1 = { 
        FavoritesTable.CONTACT_ID, 
        FavoritesTable.NORMALIZED_NUMBER };
    
    /**
     * Defines if the number is in personal favorites.
     * 
     * @param normalizedNumber
     *            the phone number
     * @param context
     *            the execution context
     * @return <code>true</code> if the <code>phoneId</code> exists in the
     *         personal favorites table, otherwise <code>false</code>.
     */
    public static final boolean isFavorite(String normalizedNumber, Context context) {
        Uri uri = UriHelper.getUri(RCMProvider.FAVORITES, RCMProviderHelper.getCurrentMailboxId(context));
        Cursor c = null;
        try {
            c = context.getContentResolver().query(uri, PROJECTION_FV1, FavoritesTable.NORMALIZED_NUMBER + " = ?",
                    new String[] { normalizedNumber }, null);
            if (c != null && c.moveToFirst()) {
                return true;
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                QaLog.e(LOG_TAG, "isFavorite(number): " + error.getMessage());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return false;
    }
    
    /**
     * Delete personal favorite by phone identifier.
     * 
     * @param context
     *            the execution context
     * @param mailboxId
     *            the mailboxId
     * @param phoneId
     *            the phone number identifier in the favorites
     */
    public static final void deletePersonalFavorite(Context context, long mailboxId, long phoneId) {
        try {
            Uri uri = UriHelper.getUri(RCMProvider.FAVORITES, mailboxId);
            context.getContentResolver().delete(uri, FavoritesTable.PHONE_ID + " = ?",
                    new String[] { Long.toString(phoneId) });
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                MktLog.e(LOG_TAG, " deletePersonalFavorite error: " + error.getMessage());
            }
        }
    }
    
    /**
     * Delete personal favorite by record identifier.
     * 
     * @param context
     *            the execution context
     * @param mailboxId
     *            the mailboxId
     * @param recordId
     *            the record identifier in the favorites table
     */
    public static final void deletePersonalFavoriteByRecordId(Context context, long mailboxId, long recordId) {
        try {
            Uri uri = UriHelper.getUri(RCMProvider.FAVORITES, mailboxId, recordId);
            context.getContentResolver().delete(uri, null, null);
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                MktLog.e(LOG_TAG, " deletePersonalFavoriteByRecordId error: " + error.getMessage());
            }
        }
    }
    
    /**
     * Retrieves phone numbers of the contact indicating where number is in
     * personal favorites or not.
     * 
     * @param context
     *            the execution context
     * @param contactId
     *            the contact identifier for the phones retrieving
     * @return a list of numbers in favorites at index
     *         {@link #NUMBERS_IN_FAVORITES}, and the numbers that are not in
     *         the personal favorites at index {@link #NUMBERS_NOT_IN_FAVORITES}
     */
    public static List<List<Cont.acts.TaggedContactPhoneNumber>> getPersonalFavorites(Context context, long contactId) {
        List<List<Cont.acts.TaggedContactPhoneNumber>> ret = new ArrayList<List<Cont.acts.TaggedContactPhoneNumber>>();
        List<Cont.acts.TaggedContactPhoneNumber> favorites = new ArrayList<Cont.acts.TaggedContactPhoneNumber>();
        List<Cont.acts.TaggedContactPhoneNumber> not_favorites = new ArrayList<Cont.acts.TaggedContactPhoneNumber>();
        ret.add(favorites);
        ret.add(not_favorites);
        
        List<Cont.acts.TaggedContactPhoneNumber> list = Cont.acts().getPersonalContactPhoneNumbers(context, contactId,
                false);
        
        for (Cont.acts.TaggedContactPhoneNumber item: list) {
            if (isFavorite(item.id, context)) {
                favorites.add(item);
            } else {
                not_favorites.add(item);
            }
        }
        
        return ret;
    }

    /**
     * The index of the phone numbers are in favorites,
     * {@link #getPersonalFavorites(Context, long)}
     */
    public final static int NUMBERS_IN_FAVORITES = 0;

    /**
     * The index of the phone numbers are not in the personal favorites,
     * {@link #getPersonalFavorites(Context, long)}
     */
    public final static int NUMBERS_NOT_IN_FAVORITES = 1;
    
    /**
     * Synchronize personal favorites.
     * 
     * @param sync the sync control
     * 
     * @return the status
     */
    public static final boolean sync(LocalSyncControl sync) {
        Uri uri = UriHelper.getUri(RCMProvider.FAVORITES, sync.getMailboxId());
        Cursor c = null;
        ArrayList<FavoriteRecord> records = new ArrayList<FavoriteRecord>();
        long sequence = 0;
        try {
            c = sync.getContext().getContentResolver().query(uri, PROJECTION_FV2, null, null, null);
            if (c != null) {
                c.moveToPosition(-1);
                while (c.moveToNext()) {
                    try {
                        FavoriteRecord r = new FavoriteRecord();
                        r.state = State.KEEP;
                        r.id = c.getLong(_ID_INDX6);
                        r.contactId = c.getLong(CONTACT_ID_INDX6);
                        r.phoneId = c.getLong(PHONE_ID_INDX6);
                        r.photoId = c.getLong(PHOTO_ID_INDX6);
                        r.displayName = c.getString(DISPLAY_NAME_INDX6);
                        r.originalNumber = c.getString(ORIGINAL_NUMBER_INDX6);
                        r.normalizedNumber = c.getString(NORMILIZED_NUMBER_INDX6);
                        r.fullNumber = r.normalizedNumber;
                        r.type = c.getInt(TYPE_INDX6);
                        r.label = c.getString(LABEL_INDX6);
                        r.sequence = sequence;
                        sequence++;
                        records.add(r);
                        if (sync.isTerminated()) {
                            return false;
                        }
                    } catch (java.lang.Throwable error) {
                        if (LogSettings.MARKET) {
                            MktLog.e(LOG_TAG, "sync (read): ", error);
                        }
                    }
                }
            }
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                MktLog.e(LOG_TAG, "sync: ", error);
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }

        if (sync.isTerminated() || records.isEmpty()) {
            return false;
        }

        Cont.acts.PhoneContact p = null;
        for (FavoriteRecord r : records) {
            if (sync.isTerminated()) {
                return false;
            }
            try {
                if (r.phoneId == -1) {
                    ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumber(r.normalizedNumber);
                    if (!cpn.isValid) {
                        if (LogSettings.MARKET) {
                            MktLog.w(LOG_TAG, "Error. Number is not valid (remove)");
                        }
                        r.state = State.DELETE;
                        continue;
                    }
                    r.fullNumber = cpn.phoneNumber.numRaw;
                    continue;
                }
                p = Cont.acts().lookUpPhoneContactById(sync.getContext(), r.phoneId);
                if (p == null) {
                    if (LogSettings.MARKET) {
                        QaLog.i(LOG_TAG, "Contact of personal favorite removed " + r.normalizedNumber);
                    }
                    ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumber(r.normalizedNumber);
                    if (!cpn.isValid) {
                        if (LogSettings.MARKET) {
                            MktLog.w(LOG_TAG, "Error. Number is not valid (remove)");
                        }
                        r.state = State.DELETE;
                        continue;
                    }
                    r.state = State.UPDATE;
                    r.contactId = -1;
                    r.phoneId = -1;
                    r.photoId = -1;
                    r.displayName = null;
                    r.fullNumber = r.originalNumber = r.normalizedNumber = cpn.phoneNumber.numRaw;
                    continue;
                }

                /**
                 * Phone contact found
                 */
                if (p.number == null) {
                    if (LogSettings.MARKET) {
                        MktLog.i(LOG_TAG, "Contact of personal favorite updated. Number is not valid (remove)");
                    }
                    r.state = State.DELETE;
                    continue;
                }

                ContactPhoneNumber cpn = PhoneUtils.getContactPhoneNumber(p.number);
                if (!p.number.equalsIgnoreCase(r.originalNumber)) {
                    if (LogSettings.MARKET) {
                        try {
                            MktLog.i(LOG_TAG, "Contact number of personal favorite changed (" + r.originalNumber + " -> "
                                    + p.number + ")");
                        } catch (Exception ex) {
                        }
                    }
                    if (!cpn.isValid) {
                        if (LogSettings.MARKET) {
                            MktLog.w(LOG_TAG, "Contact number of personal favorite has now invalid number (remove)");
                        }
                        r.state = State.DELETE;
                        continue;
                    }
                    r.originalNumber = p.number;
                    r.normalizedNumber = cpn.normalizedNumber;
                    r.state = State.UPDATE;
                }
                r.fullNumber = cpn.phoneNumber.numRaw;
                if (p.contactId != r.contactId) {
                    if (LogSettings.MARKET) {
                        QaLog.i(LOG_TAG, "Contact id of personal favorite changed");
                    }
                    r.contactId = p.contactId;
                    r.state = State.UPDATE;
                }
                if (!compareStrings(p.displayName, r.displayName)) {
                    if (LogSettings.MARKET) {
                        QaLog.i(LOG_TAG, "Display name of personal favorite changed");
                    }
                    r.displayName = p.displayName;
                    r.state = State.UPDATE;
                }
                if (p.photoId != r.photoId) {
                    r.photoId = p.photoId;
                    r.state = State.UPDATE;
                    if (LogSettings.MARKET) {
                        QaLog.i(LOG_TAG, "Photo of personal favorite changed");
                    }
                }
                if (p.type != r.type) {
                    r.type = p.type;
                    r.state = State.UPDATE;
                    if (LogSettings.MARKET) {
                        MktLog.i(LOG_TAG, "Number type of personal favorite changed");
                    }
                    r.label = p.label;
                }
                if (r.state == State.UPDATE) {
                    if (LogSettings.MARKET) {
                        MktLog.i(LOG_TAG, "Personal favorite updated (contact updated) " + r.normalizedNumber);
                    }
                }
            } catch (java.lang.Throwable error) {
                if (LogSettings.MARKET) {
                    MktLog.e(LOG_TAG, "sync2: ", error);
                }
            }
        }

        /**
         *  Remove duplications in favorites that have contact
         */
        for (FavoriteRecord r : records) {
            if (sync.isTerminated()) {
                return false;
            }
            if ((r.phoneId != -1) && (r.state != State.DELETE)) {
                for (FavoriteRecord rIn : records) {
                    if ((r.sequence != rIn.sequence) && (rIn.phoneId != -1) && (rIn.state != State.DELETE)
                            && (r.fullNumber.equalsIgnoreCase(rIn.fullNumber))) {

                        if (REMOVE_DUPLICATED_NUMBERS) {
                            if (LogSettings.MARKET) {
                                MktLog.i(LOG_TAG, "Remove favorite duplicate (has contact) " + r.fullNumber);
                            }
                            rIn.state = State.DELETE;
                            continue;
                        }
                        
                        if (rIn.phoneId == r.phoneId) {
                            if (LogSettings.MARKET) {
                                MktLog.i(LOG_TAG, "Remove favorite duplicate (the same contact) " + r.fullNumber);
                            }
                            rIn.state = State.DELETE;
                            continue;
                        }
                    }
                }
            }
        }
        
        /**
         * Lookup contacts for favorites that do not have contact
         */
        for (FavoriteRecord r : records) {
            if (sync.isTerminated()) {
                return false;
            }
            if (r.phoneId == -1 && r.state != State.DELETE) {
                p = Cont.acts().lookUpPhoneContactByNumber(sync.getContext(), r.normalizedNumber);

                if (p != null) {
                    if (LogSettings.MARKET) {
                        MktLog.i(LOG_TAG, "Contact for favorite found: " + r.normalizedNumber);
                    }
                    r.phoneId = p.phoneId;
                    r.contactId = p.contactId;
                    r.displayName = p.displayName;
                    r.type = p.type;
                    r.label = p.label;
                    r.originalNumber = p.number;
                    r.photoId = p.photoId;
                    r.state = State.UPDATE;

                    for (FavoriteRecord rIn : records) {
                        if ((r.sequence != rIn.sequence) && (rIn.phoneId != -1) && (rIn.state != State.DELETE)
                                && (r.fullNumber.equalsIgnoreCase(rIn.fullNumber))) {

                            if (rIn.phoneId == r.phoneId) {
                                if (LogSettings.MARKET) {
                                    MktLog.i(LOG_TAG, "Remove favorite duplicate 2 (the same contact) " + r.fullNumber);
                                }
                                r.state = State.DELETE;
                                break;
                            }

                            if (REMOVE_DUPLICATED_NUMBERS) {
                                if (LogSettings.MARKET) {
                                    MktLog.i(LOG_TAG, "Remove favorite duplicate 2 (has contact) " + r.fullNumber);
                                }
                                r.state = State.DELETE;
                                break;
                            }
                        }
                    }
                }
            }
        }
        
        /**
         *  Remove duplications in favorites that do not have contact
         */
        for (FavoriteRecord r : records) {
            if (sync.isTerminated()) {
                return false;
            }
            if (r.phoneId == -1 && r.state != State.DELETE) {
                for (FavoriteRecord rIn : records) {
                    if ((r.sequence != rIn.sequence) && rIn.state != State.DELETE
                            && (r.fullNumber.equalsIgnoreCase(rIn.fullNumber))) {
                        if (LogSettings.MARKET) {
                            MktLog.i(LOG_TAG, "Remove favorite duplicate (no contact) " + r.fullNumber);
                        }
                        r.state = State.DELETE;
                        break;
                    }
                }
            }
        }

        
        /**
         * Update data-base
         */
        boolean changed = false;
        for (FavoriteRecord r : records) {
            if (sync.isTerminated()) {
                return false;
            }
            if (r.state == State.DELETE) {
                try {
                    uri = UriHelper.getUri(RCMProvider.FAVORITES, sync.getMailboxId(), r.id);
                    sync.getContext().getContentResolver().delete(uri, null, null);
                    changed = true;
                } catch (java.lang.Throwable error) {
                    if (LogSettings.MARKET) {
                        MktLog.e(LOG_TAG, "sync (delete error): ", error);
                    }
                }
                continue;
            }
            if (r.state == State.UPDATE) {
                try {
                    uri = UriHelper.getUri(RCMProvider.FAVORITES, sync.getMailboxId(), r.id);
                    ContentValues values = new ContentValues();
                    values.put(FavoritesTable.PHONE_ID, r.phoneId);
                    values.put(FavoritesTable.CONTACT_ID, r.contactId);
                    values.put(FavoritesTable.DISPLAY_NAME, r.displayName);
                    values.put(FavoritesTable.ORIGINAL_NUMBER, r.originalNumber);
                    values.put(FavoritesTable.NORMALIZED_NUMBER, r.normalizedNumber);
                    values.put(FavoritesTable.TYPE, r.type);
                    values.put(FavoritesTable.LABEL, r.label);
                    values.put(FavoritesTable.PHOTO_ID, r.photoId);
                    sync.getContext().getContentResolver().update(uri, values, null, null);
                    changed = true;
                } catch (java.lang.Throwable error) {
                    if (LogSettings.MARKET) {
                        MktLog.e(LOG_TAG, "sync (update error): ", error);
                    }
                }
                continue;
            }
        }
        return changed;
    }
    private static final String[] PROJECTION_FV2 = { 
        FavoritesTable._ID,
        FavoritesTable.PHONE_ID,
        FavoritesTable.CONTACT_ID,
        FavoritesTable.DISPLAY_NAME,
        FavoritesTable.ORIGINAL_NUMBER,
        FavoritesTable.NORMALIZED_NUMBER,
        FavoritesTable.TYPE,
        FavoritesTable.PHOTO_ID,
        FavoritesTable.LABEL
        };
    private static final int _ID_INDX6 = 0;
    private static final int PHONE_ID_INDX6 = 1;
    private static final int CONTACT_ID_INDX6 = 2;
    private static final int DISPLAY_NAME_INDX6 = 3;
    private static final int ORIGINAL_NUMBER_INDX6 = 4;
    private static final int NORMILIZED_NUMBER_INDX6 = 5;
    private static final int TYPE_INDX6 = 6;
    private static final int PHOTO_ID_INDX6 = 7;
    private static final int LABEL_INDX6 = 8;
    
    private static class FavoriteRecord {
        long sequence;
        long id;
        long phoneId;
        long photoId;
        long contactId;
        String displayName;
        String originalNumber;
        String normalizedNumber;
        String fullNumber;
        int type;
        String label;
        State state;
    }
    
    private enum State {
        KEEP, UPDATE, DELETE 
    }
    
    private static boolean compareStrings(String str1, String str2) {
        if (str1 == null) {
            if (str2 != null) {
                return false;
            }
            return true;
        } else {
            if (str2 == null) {
                return false;
            }
            return str1.equalsIgnoreCase(str2);
        }
    }
}
