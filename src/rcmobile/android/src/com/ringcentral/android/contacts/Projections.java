/** 
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import android.provider.Contacts.People;
import android.provider.Contacts.Phones;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;

import com.ringcentral.android.provider.RCMDataStore.CallerIDsTable;
import com.ringcentral.android.provider.RCMDataStore.ExtensionsTable;
import com.ringcentral.android.utils.ContactsUtils;


/**
 * TODO Re-factor (remove)
 */
public final class Projections {
	
	 /**
	  * This class help us to build projection for ListActivity
	  * to display summary information about personal contacts.
	  * Help to parse Cursor
	  */
	 public static final class Personal {
	        
	        private Personal() {};
	        
	        /**
	         * Contacts projection.
	         */
	        public static final String[] CONTACTS_SUMMARY_PROJECTION = new String[] {
	        	People._ID, 								// 0
	        	People.DISPLAY_NAME, 						// 1
	        	People.STARRED,								// 2
	        	ContactsUtils.People_Has_Phone_Number,		// 3
	        	ContactsUtils.People_PhotoID,				// 4
//	        	StructuredName.GIVEN_NAME,					// 5
//	        	StructuredName.FAMILY_NAME					// 6
	        };
	        
	        /**
	         * Person ID column index.
	         */
	        public static final int ID_COLUMN_INDEX = 0;
	        
	        /**
	         * Person name column index.
	         */
	        public static final int NAME_COLUMN_INDEX = 1;
	        
	        /**
	         * Person stared column index.
	         */
	        public static final int STARRED_COLUMN_INDEX = 2;
	        
	        /**
	         * Person stared column index.
	         */
	        public static final int HAS_PHONE_COLUMN_INDEX = 3;
	        
	        /**
	         * Person photo data column index.
	         */
	        public static final int PHOTO_COLUMN_INDEX = 4;

	        /**
	         * Person phone number column index.
	         */
	        public static final int NUMBER_COLUMN_INDEX = 5;	 
	        
//	        /**
//	         * Person phone number column index.
//	         */
//	        public static final int FIRST_NAME_COLUMN_INDEX = 5;	 
//
//	        /**
//	         * Person name column index.
//	         */
//	        public static final int LAST_NAME_COLUMN_INDEX = 6;
	        
	 }
	 
	 public static final class PersonalPhones {
		 
		   private PersonalPhones() {};
		  
		   /** The projection to use when querying the phones table */
		   public static final String[] PHONES_PROJECTION = new String[] {
			   		"_id",								//			   _id / _id ( phone number id in data/phones table)
		            Phones.DISPLAY_NAME,				//			   PhoneLookup.DISPLAY_NAME =display_name
		            Phones.TYPE,						//			   PhoneLookup.TYPE = type
		            Phones.LABEL,						//			   PhoneLookup.LABEL =label
		            Phones.NUMBER,						//			   PhoneLookup.NUMBER = number
		            ContactsUtils.PhoneLookup_Photo_ID	// 			   person / photo_id
		   };
		   
		   public static final int PHONE_ID_COLUMN_INDEX = 0;
		   public static final int NAME_COLUMN_INDEX = 1;
		   public static final int PHONE_TYPE_COLUMN_INDEX = 2;
		   public static final int LABEL_COLUMN_INDEX = 3;
		   public static final int MATCHED_NUMBER_COLUMN_INDEX = 4;
		   public static final int PHOTO_ID_COLUMN_INDEX = 5;
		   public static final int SDK3_PERSON_ID_COLUMN_INDEX = 5;
	}
	 		 
	 /**
	  * This class help us to build projection for ListActivity
	  * to display summary information about extensions (company contacts)
	  * Help to parse Cursor
	  */
	 public static final class Extensions {
	        
	        private Extensions() {};
	        
	        /**
		     * Extension projection.
		     */
		    static final String[] EXTENSION_SUMMARY_PROJECTION = new String[] {
		    	ExtensionsTable._ID,						// 0 
		    	ExtensionsTable.MAILBOX_ID,					// 1
		    	ExtensionsTable.RCM_DISPLAY_NAME,			// 2
		    	ExtensionsTable.RCM_STARRED, 				// 3
		    	ExtensionsTable.JEDI_PIN, 					// 4
		    	ExtensionsTable.JEDI_EMAIL, 				// 5
		    	ExtensionsTable.JEDI_FIRST_NAME,			// 6
		    	ExtensionsTable.JEDI_LAST_NAME,				// 7
		    };
		    
		    /**
		     * Extension ID column index.
		     * 
		     */
		    static final int EXT_ID_COLUMN_INDEX = 0;
		    
		    /**
		     * Extension mailbox ID column index.
		     * 
		     */
		    static final int EXT_MAILBOX_ID_COLUMN_INDEX = 1;
		    
		    /**
		     *  Extension name column index.
		     */
		    static final int EXT_NAME_COLUMN_INDEX = 2;
		    
		    /**
		     *  Extension stared column index.
		     */
		    static final int EXT_STARRED_COLUMN_INDEX = 3;
		    
		    /**
		     *  Extension pin column index.
		     */
		    static final int EXT_PIN_COLUMN_INDEX = 4;
		    
		    /**
		     *  Extension email column index.
		     */
		    static final int EXT_EMAIL_COLUMN_INDEX = 5;

		    /**
		     *  Extension name column index.
		     */
		    static final int EXT_FIRST_NAME_COLUMN_INDEX = 6;

		    /**
		     *  Extension name column index.
		     */
		    static final int EXT_LAST_NAME_COLUMN_INDEX = 7;
		    
	 }
	 
	 /**
	  * This class help us to build projection for ListActivity
	  * to display summary information about personal contacts.
	  * Help to parse Cursor
	  */
	 public static final class CallerID {
	        
	        private CallerID() {};
	        
	        /**
	         * Contacts projection.
	         */
	        public static final String[] SUMMARY_PROJECTION = new String[] {
	        	CallerIDsTable._ID, 								// 0
	        	CallerIDsTable.JEDI_NUMBER, 						// 1
	        	CallerIDsTable.JEDI_USAGE_TYPE,						// 2
	        };
	        
	        /**
	         * CallerId ID column index.
	         */
	        public static final int ID_COLUMN_INDEX = 0;
	        
	        /**
	         * Number column index.
	         */
	        public static final int NUMBER_COLUMN_INDEX = 1;
	        
	        /**
	         * Type(label) column index.
	         */
	        public static final int TYPE_COLUMN_INDEX = 2;
	        
	        /**
	         * Mark selected column index.
	         */
	        public static final int SELECTED_COLUMN_INDEX = 3;
	        
	    
	        
	        
	 }
	 
	
 	
}
