/** 
 * Copyright (C) 2010, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.ringcentral.android.contacts;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.provider.RCMProvider;
import com.ringcentral.android.provider.UriHelper;
import com.ringcentral.android.provider.RCMDataStore.ExtensionsTable;

/**
 * Company/Extensions favorites staff.
 */
public class CompanyFavorites {
    /**
     * Define tag for logging.
     */
    private static final String LOG_TAG = "[RC] CompanyFavorites";

    /**
     * Set a company contact as a favorite.
     * 
     * @param context
     *            the execution context
     * @param mailboxId
     *            the mailbox identifier (current account)
     * @param id
     *            the company/extension record identifier
     */
    public static final void setAsFavorite(Context context, long mailboxId, long id) {
        setFavoriteState(context, mailboxId, id, true);
    }
    
    /**
     * Set a company contact as a favorite.
     * 
     * @param context
     *            the execution context
     * @param mailboxId
     *            the mailbox identifier (current account)
     * @param id
     *            the company/extension record identifier
     */
    public static final void setAsNotFavorite(Context context, long mailboxId, long id) {
        setFavoriteState(context, mailboxId, id, false);
    }
    
    /**
     * Set/Un-set company favorite.
     * 
     * @param context
     *            the execution context
     * @param mailboxId
     *            the mailbox identifier (current account)
     * @param id
     *            the company/extension record identifier
     * @param set
     *            the toggle value
     */
    private static final void setFavoriteState(Context context, long mailboxId, long id, boolean set) {
        try {
            Uri uri = UriHelper.getUri(RCMProvider.EXTENSIONS, mailboxId, id);
            ContentValues values = new ContentValues(1);
            values.put(ExtensionsTable.RCM_STARRED, set ? 1 : 0);
            context.getContentResolver().update(uri, values, null, null);
        } catch (java.lang.Throwable error) {
            if (LogSettings.MARKET) {
                MktLog.w(LOG_TAG, "setFavoriteState failed:" + error.getMessage());
            }
        }
    }
}
