/**
 * 
 */
package com.rcbase.api.xml;

/**
 * @author Denis Kudja
 *
 */
public interface IXmlParsingObject {

	public String getTagName();
}
