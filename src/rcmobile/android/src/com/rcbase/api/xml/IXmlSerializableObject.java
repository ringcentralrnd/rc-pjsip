package com.rcbase.api.xml;
/**
 * 
 */

/**
 * @author Denis Kudja
 *
 */
public interface IXmlSerializableObject {

	public String getTagName();
}
