/**
 * 
 */
package com.rcbase.api.xml;

import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

/**
 * @author Denis Kudja
 *
 */
public abstract class XmlParsingObjectAbstract extends DefaultHandler 
	implements	IXmlParsingObject {
	
	private XmlParsingObjectAbstract m_active_child = null;
	private Hashtable< String, XmlParsingObjectAbstract> m_child_list = new Hashtable< String, XmlParsingObjectAbstract>();
	private boolean m_failed  = false;
	private boolean m_started = false;
	
	protected XMLReader m_xmlReader = null;
	private XmlParsingObjectAbstract m_parent = null;

	private ContentHandler m_content_handler__previous = null;
	
	public XmlParsingObjectAbstract( XmlParsingObjectAbstract parent ){
		setParent( parent );
	}
	
	public XMLReader getReader(){
		return ( m_parent == null ? m_xmlReader : m_parent.getReader() );
	}
	
	public XmlParsingObjectAbstract getParent() {
		return m_parent;
	}

	public void setParent(XmlParsingObjectAbstract parent) {
		m_parent = parent;
	}

	protected StringBuffer m_buf = new StringBuffer();
	
	public void addChild( String key, XmlParsingObjectAbstract child ){
		if( !m_child_list.containsKey(key)){
			m_child_list.put(key, child );
		}
	}
	
	public boolean isFailed() {
		return m_failed;
	}

	protected void setFailed(boolean failed) {
		m_failed = failed;
	}

	private XmlParsingObjectAbstract getActive_child() {
		return m_active_child;
	}

	private void setActive_child(XmlParsingObjectAbstract activeChild) {
		m_active_child = activeChild;
	}

	public boolean isActive_child(){
		return ( m_active_child != null );
	}
	
	protected boolean isStarted() {
		return m_started;
	}

	protected void setStarted(boolean started) {
		m_started = started;
	}
	
	static public String getElementName( String localName, String qName ){
		return (localName.length() == 0 ? qName : localName );
	}

	protected StringBuffer getBuf() {
		return m_buf;
	}

	protected String getValue() {
		return m_buf.toString();
	}

	protected long getLongValue() {
		final String result = m_buf.toString();
		return Long.decode(result).longValue();
	}

	protected int getIntValue() {
		final String result = m_buf.toString();
		return Integer.decode(result).intValue();
	}

	protected long getLongValue(String value) {
		return Long.decode(value).longValue();
	}
	protected int getIntValue(String value) {
		return Integer.decode(value).intValue();
	}
	
	public boolean isItself( String element_name ){
		return this.getTagName().equalsIgnoreCase(element_name);
	}
	
	/*
	 * 
	 */
	protected void parseAttributes(Attributes attributes){
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
	 */
	public void characters(char chars[], int start, int length){
		m_buf.append(chars, start, length);
	}
	/*
	 * (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	public void  startElement  (String uri, String localName, String qName, Attributes attributes) throws SAXException {
		String elementName = getElementName( localName, qName );

		//DK: clear buffer. it is important because you can have previous value for empty fields.
		m_buf.setLength(0); 
		
		if( !isStarted()){
			if( elementName.equalsIgnoreCase(this.getTagName())){
				setStarted( true );

				XMLReader reader = getReader();
				m_content_handler__previous = reader.getContentHandler();
				reader.setContentHandler(this);
				
				if( attributes.getLength() > 0 )
					this.parseAttributes(attributes);
			}
			else{
				setFailed(true);
				throw new SAXException( "Wrong object hierarchy. First element should be equal object tag name. Current element: " + elementName );
			}
		}
		
//		System.out.println(elementName + " " + elementName.length() );
		
		boolean child_present = ( !m_child_list.isEmpty() && m_child_list.containsKey(elementName));
		
//		Enumeration<String> key = m_child_list.keys();
//		while( key.hasMoreElements()){
//			System.out.println( "Key: " + key.nextElement() );
//		}
		
		//set current object
		if( child_present ){
			
//			System.out.println("Found child: " +  elementName );
			
			if( getActive_child() == null ){
				setActive_child( m_child_list.get(elementName));
				getActive_child().startElement( uri, localName, qName, attributes );
			}
			else{// generate SAX exception: wrong object hierarchy
				setFailed(true);
				throw new SAXException( "Wrong object hierarchy. Current element: " + getActive_child().getTagName() + " new: " + elementName );
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void  endElement  (String uri, String localName, String qName){
		String elementName = getElementName( localName, qName );
		
		if( getTagName().equals(elementName) ){
			
			this.setStarted(false);
			
			if( getParent() != null )
				getParent().setActive_child( null );
			
			XMLReader reader = getReader();
			reader.setContentHandler( m_content_handler__previous );
		}
	}

	/*
	 * 
	 */
	public void parseResponse(InputStream is )
		throws ParserConfigurationException, SAXException, IOException {
		
		SAXParserFactory spf = SAXParserFactory.newInstance();
		SAXParser sp = spf.newSAXParser();
		
		m_xmlReader = sp.getXMLReader();
		
		//m_xmlReader.setFeature("http://xml.org/sax/features/namespaces", false );
		
		m_xmlReader.setContentHandler( this );
		m_xmlReader.parse(new InputSource(is));
	}
	
	public String attributesGetName( Attributes attributes, int index ){
		return ( attributes.getLocalName(index) == null || 
				attributes.getLocalName(index).length() == 0 ? attributes.getQName(index) :
					attributes.getLocalName(index));
	}
}
