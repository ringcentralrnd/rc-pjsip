package com.rcbase.api.xml;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Comparator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.utils.ReflectionHelper;

/**
 * 
 */

/**
 * @author Denis Kudja
 *
 */
public abstract class XmlSerializableObjectAbstract implements IXmlSerializableObject {
    private static final String TAG = "[RC] XmlSerializableObjectAbstract";
	private static final String sf_tag_prefix			= "getTag_";
	private static final String sf_attribute_prefix		= "getAttr_";
	
	private static final String sf_elem_begin_hdr	= "<";
	private static final String sf_elem_end_hdr		= "</";
	private static final String sf_elem_end			= ">";
	
	private static final String sf_XML_hdr_UTF8		= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	
	private Document m_doc = null;
	private static Class<?> xml_root = null;
	
	private boolean m_addEmptyValues = true;
	private boolean m_addXmlHeader	= false;
	
	public boolean isAddXmlHeader() {
		return m_addXmlHeader;
	}

	public void setAddXmlHeader(boolean addXmlHeader) {
		m_addXmlHeader = addXmlHeader;
	}

	public boolean isAddEmptyValues() {
		return m_addEmptyValues;
	}

	public void setAddEmptyValues(boolean addEmptyValues) {
		m_addEmptyValues = addEmptyValues;
	}

	/*
	 * 
	 */
	private Document getDoc(){
		return m_doc;
	}
	
	/*
	 * 
	 */
	public String toString(){
		ByteArrayOutputStream s_out = new ByteArrayOutputStream();
		try {
			this.toStream(s_out);
		} catch (WrongXmlSource e) {
		    if (LogSettings.MARKET) {
                MktLog.e(TAG, "toString():" + e.toString());
            }
			return null;
		}
		return s_out.toString();
	}
	/*
	 * 
	 */
	public boolean toStream(OutputStream s_out) throws WrongXmlSource{
		
		try {
			if( xml_root == null ){
				xml_root = Class.forName("com.rcbase.api.xml.XmlSerializableObjectAbstract");
			}
		} catch (ClassNotFoundException e) {
		    if (LogSettings.MARKET) {
                MktLog.e(TAG, "toStream():" + e.toString());
            }
			return false;
		}
		
		
        if (this.getTagName() == null) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "toStream():getTagName=null");
            }
            return false;
        }
		
		try{
			DocumentBuilderFactory docFact = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFact.newDocumentBuilder();
			m_doc = docBuilder.newDocument();
		}
		catch( ParserConfigurationException e_parse_cfg ){
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "toStream():ParserConfigurationException");
            }
			return false;
		}
			
		Element root = m_doc.createElement(this.getTagName()); //create root node
		
		if( !this.toNode(root, this)){
			throw new WrongXmlSource("Could not serialize object: " + this.getTagName() );
		}
		
		m_doc.appendChild(root);//add root note to tree

		boolean result = false;
		try {
			if( isAddXmlHeader() ) {
				s_out.write(sf_XML_hdr_UTF8.getBytes());
			}
			result = toString( root, s_out );
		} 
		catch (IOException e) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "toStream():IOException", e);
            }
		}
		
		m_doc = null;
		
		return result;
	}
	
	/*
	 * 
	 */
	private String getMethodName(String name){
		String result = name.substring(name.indexOf("_") + 1 );
		int next = result.indexOf("_");
		if( next > 0 ){
			String s_index =  result.substring(0, next);
			int index = Integer.parseInt( s_index );
			if( index > 0 ){
				result = result.substring( next + 1 );
			}
		}
		return result;
	}
	
	private class MethodComparator implements Comparator<Method>{

		@Override
		public int compare(Method object1, Method object2) {
			return object1.getName().compareTo(object2.getName());
		}
		
	}
	
	/*
	 * 
	 */
	private boolean toNode( Element root, Object curr_obj ){
		
		Class<?> cur_class = curr_obj.getClass();
		
		Method[] methods = cur_class.getMethods();
		Arrays.sort(methods, new MethodComparator());
		
		for( int i = 0; i < methods.length; i++ ){

			/*
			 * Select methods are:
			 * 1. Not static
			 * 2. Public
			 * 3. Start from 'getAttr_' or 'getTag_'
			 * 3. Does not have any parameters
			 */
			if( Modifier.isStatic(methods[i].getModifiers()) )
				continue;
			if( !Modifier.isPublic(methods[i].getModifiers()))
				continue;
			if( methods[i].getGenericParameterTypes().length > 0)
				continue;
			if( !(methods[i].getName().startsWith(sf_tag_prefix) || methods[i].getName().startsWith(sf_attribute_prefix)) )
				continue;
				
			Class<?> return_type = methods[i].getReturnType();

			if( return_type.getName().startsWith("[[")) // DK: we do not support multi dimension arrays
				return false;
			
			boolean isArray = return_type.getName().startsWith("[");
			boolean isObjectArray = return_type.getName().startsWith("[L");
			
			if( isArray && !isObjectArray ){
				continue; //DK: We will not support arrays of primitive types
			}
			
			String name = getMethodName( methods[i].getName() );
			String class_name = return_type.getName().substring( return_type.getName().lastIndexOf(".") + 1 ).replace(";", "");
			
//			System.out.print(methods[i].getName());
//			System.out.println( " Return type: " + return_type.getName());
			
			/*
			 * Method return class
			 */
			String value = new String();
			try {
				if( isArray ){
					Object[] result = (Object[])methods[i].invoke(curr_obj, (Object[])null);

					Element node = this.getDoc().createElement(name);
					root.appendChild(node);
					for( int idx = 0; idx < result.length; idx++ ){
						
						Object result_item = result[idx];
						
						String tagName = getTagName( result_item );
						Element array_node = this.getDoc().createElement(( tagName == null ? class_name : tagName ));
						if( result_item != null && xml_root.isInstance(result_item)){
							toNode( array_node, result_item );
						}
						else{
							if( result_item == null ){
								value = "";
							}
							else{
								value = result_item.toString();
								
								Class<?> result_class = result_item.getClass();
								if( value != null && 
										result_class != null && 
										result_class.getName() != null && 
										result_class.getName().equalsIgnoreCase("java.lang.Boolean") ){
									value = ( value.equalsIgnoreCase("true") ? "1" : "0" );
								}
							}
							
							Text node_text = this.getDoc().createTextNode(value);
							array_node.appendChild(node_text);
						}
						
						node.appendChild(array_node);
					}
					root.appendChild(node);
					continue;
				}
				else{
					Object result = methods[i].invoke(curr_obj, (Object[])null);
					
					if( result != null && xml_root.isInstance(result)){
						
						String tagName = getTagName( result );
						Element node = this.getDoc().createElement(( tagName == null ? name : tagName));
						root.appendChild(node);
						toNode( node, result );
						continue;
					}
					else{
						if( result == null ){
							value = "";
						}
						else{
							value = result.toString();
							
							Class<?> result_class = result.getClass();
							if( value != null && 
									result_class != null && 
									result_class.getName() != null && 
									result_class.getName().equalsIgnoreCase("java.lang.Boolean") ){
								value = ( value.equalsIgnoreCase("true") ? "1" : "0" );
							}
						}
					}
				}
				
			} catch (IllegalArgumentException e) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "toNode():IllegalArgumentException()", e);
                }
				return false;
			} catch (IllegalAccessException e) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "toNode():IllegalAccessException()", e);
                }
			    return false;
			} catch (InvocationTargetException e) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "toNode():InvocationTargetException()", e);
                }
			    return false;
			}
			
			if( methods[i].getName().startsWith(sf_tag_prefix) ){
				if( isAddEmptyValues() || (value.length() > 0) ){
					Element node = this.getDoc().createElement(name);
					Text node_text = this.getDoc().createTextNode(value);
					node.appendChild(node_text);
					
					root.appendChild(node);
				}
			}
			else if(methods[i].getName().startsWith(sf_attribute_prefix)){
				if( isAddEmptyValues() || (value.length() > 0) ){
					root.setAttribute(name, value );
				}
			}
		}
		
		return true;
	}
	
	/*
	 * 
	 */
	private boolean toString( Node node, OutputStream s_out ){

		//DK: we will support Element node only for now.
		if( node.getNodeType() != Node.ELEMENT_NODE ){
			return true;
		}
		
		Element elem = (Element)node;
		NamedNodeMap attr = elem.getAttributes();
		try {
			s_out.write(sf_elem_begin_hdr.getBytes());
			s_out.write(elem.getTagName().getBytes());
			
			if( attr.getLength() > 0 ){
				String attribute = new String();
				for( int i = 0; i < attr.getLength(); i++ ){
					attribute += " " + attr.item(i).getNodeName() + "=\"" + decodeSpecXmlChars( attr.item(i).getNodeValue()) + "\" ";
				}
				
				if( attribute.length() > 0)
					s_out.write(attribute.getBytes());
			}
			
			s_out.write(sf_elem_end.getBytes());
			
			NodeList node_list = node.getChildNodes();
			if( node_list.getLength() == 1 && node_list.item(0).getNodeType() == Node.TEXT_NODE ){
				s_out.write(decodeSpecXmlChars( node_list.item(0).getNodeValue()).getBytes());
			}
			else{
				for( int j = 0; j < node_list.getLength(); j++ ){
					if( !toString( node_list.item(j), s_out )) {
					    if (LogSettings.MARKET) {
			                MktLog.e(TAG, "toString():toString()");
			            }
						return false;
					}
				}
			}

			s_out.write(sf_elem_end_hdr.getBytes());
			s_out.write(elem.getTagName().getBytes());
			s_out.write(sf_elem_end.getBytes());
			
		} catch (IOException e) {
		    if (LogSettings.MARKET) {
                MktLog.e(TAG, "toString():IOException", e);
            }
			return false;
		}
		
		return true;
	}
	
	/*
	 * Try to detect tag name using a special function getTagName() 
	 */
	private String getTagName( Object object ){

		if( object == null ) {
		    if (LogSettings.MARKET) {
                MktLog.e(TAG, "getTagName():object=null");
            }
			return null;
		}
		
		Class<?> cur_class = object.getClass();
		try {
			Method mGetTagName = ReflectionHelper.getMethod(cur_class, "getTagName");
			
			try {
				
				String tag_name = (String) mGetTagName.invoke(object, (Object[])null );
				return tag_name;
				
			} catch (IllegalArgumentException e) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "getTagName():IllegalArgumentException()", e);
                }
				return null;
			} catch (IllegalAccessException e) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "getTagName():IllegalAccessException()", e);
                }
				return null;
			} catch (InvocationTargetException e) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "getTagName():InvocationTargetException()", e);
                }
				return null;
			}
		} catch (SecurityException e) {
		    if (LogSettings.MARKET) {
                MktLog.e(TAG, "getTagName():SecurityException()", e);
            }
			return null;
		} catch (NoSuchMethodException e) {
		    if (LogSettings.MARKET) {
                MktLog.e(TAG, "getTagName():NoSuchMethodException()", e);
            }
			return null;
		}
	}
	
	/*
	 * 
	 */
	private String decodeSpecXmlChars( String source ){
		return source.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\"", "&quot;").replaceAll("\'", "&#39;");
	}
}
