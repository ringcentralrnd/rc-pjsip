package com.rcbase.api.xml;
/**
 * Exception is generated on case when object is downloaded to XML is incorrect
 */

/**
 * @author Denis Kudja
 *
 */
public class WrongXmlSource extends Exception {

	/**
	 * 
	 */
	public WrongXmlSource() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public WrongXmlSource(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public WrongXmlSource(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public WrongXmlSource(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
