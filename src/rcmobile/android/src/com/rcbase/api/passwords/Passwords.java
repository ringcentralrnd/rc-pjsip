/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.api.passwords;

/**
 * @author Denis Kudja
 *
 */
public class Passwords {

	private static final byte sf_HttpRegister_Mask	 = (byte)0x96;
	private static final byte sf_Web_Mask			 = (byte)0x0C;
	
	private static final int sf_HttpRegister_Max	= 32;
	private static final int sf_Web_Max				= 10; 
	
	/*
	 * Create password hash (32-bit unsigned) from password
	 */
    public static long getPasswordHash(String password) {
        long pswHash = 0;
        if (password == null) {
            return 0;
        }

        int limit = password.length();
        for (int i = 0; i < limit; i++) {
            int c = (int) password.charAt(i) & 0xff;
            pswHash = (pswHash * 33 + c);
        }

        pswHash &= 0xffffffffL;
        return pswHash;
    }
	
    private static String encryptPassword( String password, byte mask, int max_length ){
    	String result = new String();

    	for( int i = 0; i < password.length(); i++ ){
    		result += String.format("%02X", (byte)((byte)password.charAt(i) ^ mask));
    	}
    	
    	for( int i = password.length(); i < max_length; i++ ){
    		result += String.format("%02X", mask );
    	}
    	
    	return result;
    }
    
    /*
     * Create password is using for HTTP register procedure
     */
    public static String getPasswordHttpReg(String password){
    	return encryptPassword( password, sf_HttpRegister_Mask, sf_HttpRegister_Max );
    }
    
    /*
     * Create password is using for WEB request 
     */
    public static String getPasswordWeb( String password ){
    	return encryptPassword( password, sf_Web_Mask, sf_Web_Max );
    }
    
    /*
     * 
     */
}
