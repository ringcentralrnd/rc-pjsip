/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.api.httpreg;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.xml.sax.SAXException;

import android.os.Build;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.service.ServicePJSIP;
import com.rcbase.android.utils.Compatibility;
import com.rcbase.api.xml.WrongXmlSource;
import com.rcbase.parsers.httpreg.HttpRegisterRequest;
import com.rcbase.parsers.httpreg.HttpRegisterResponse;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConfig;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.api.network.HttpClientFactory;

/**
 * @author Denis Kudja
 *
 */
public class HttpRegister implements IHttpRegister {
	
	final public static String TAG = "HttpRegister";
	
    public enum AgentRole {
        agent_iPhone, agent_SoftPhone, agent_OtherPhone, agent_Android
    };
	
    public static final boolean SIP_EMULATE_IPHONE	= false;
    
	private static final String sf_HTTPRegister_Header = "XMLREQ=";
	
	public static final String sf_OSName_iPhone		= "iPhone"; //DK; Is important for right HTTP registration
	public static final String sf_Version_iPhone	= "0";
	public static final String sf_OSBuild_iPhone	= "3";

	public static final String sf_OSName_SoftPhone	= "2003"; 	//DK; 2003? Vista?
	public static final String sf_Version_SoftPhone	= "4.60.109.99";
	public static final String sf_OSBuild_SoftPhone	= "3790";

	public static final String sf_OSName_Android	= "Android";
	
	public static final int sf_TEditionCode_NoCheck		= -1;
	public static final int sf_TEditionCode_WinDefault	= 0x0;
	public static final int sf_TEditionCode_WinRC		= 0x7;
	public static final int sf_TEditionCode_Win_V92		= 0xB;
	public static final int sf_TEditionCode_Android_Old	= 0xC;
	public static final int sf_TEditionCode_Android		= 0x11;
	public static final int sf_TEditionCode_Mac			= 0x100;
	public static final int sf_TEditionCode_Linux		= 0x200;
	
    public enum HttpRegistrationStatus {
        HTTP_REG_OK(0, "HTTPREG OK"), 
        HTTP_REG_INVALID_CREDENTIAL(1, "HTTPREG FAILED INVALID_CREDENTIAL"), 
        HTTP_REG_FAILED(2, "HTTPREG FAILED"), 
        HTTP_REG_FAILED_ADDING_ACCOUNT(3, "HTTPREG FAILED ADDING_ACCOUNT"), 
        HTTP_REG_UNKNOWN(4, "HTTPREG UNKNOWN STATUS");

        private final String msg;

        public String msg() {
            return msg;
        }

        private final int code;

        public static HttpRegistrationStatus get(int code) {
            for (HttpRegistrationStatus s : HttpRegistrationStatus.values()) {
                if (code == s.code) {
                    return s;
                }
            }
            return HTTP_REG_UNKNOWN;
        }

        public int code() {
            return code;
        }

        HttpRegistrationStatus(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

    };
	
	IHttpRegisterCallback m_callback = null;
	private boolean m_alwaysFromDB = false;
	
	public HttpRegister( boolean alwaysFromDB ){
		m_alwaysFromDB = alwaysFromDB;
	}
	
	public HttpRegister(IHttpRegisterCallback callback, boolean alwaysFromDB ){
		m_alwaysFromDB = alwaysFromDB;
		updateCallbackList( callback, true );
	}
	
	/*
	 * 
	 */
	public static HttpRegisterRequest makeRequest( AgentRole role ){
		HttpRegisterRequest request = new HttpRegisterRequest();
		
        
        if( role == AgentRole.agent_iPhone ){
            request.getTag_2_Bdy().setEdtn(HttpRegister.sf_TEditionCode_Android_Old);
            request.getTag_2_Bdy().setVr(HttpRegister.sf_Version_SoftPhone);
            request.getTag_2_Bdy().setOSVr(HttpRegister.sf_OSName_iPhone);
            request.getTag_2_Bdy().setOSBld(HttpRegister.sf_OSBuild_iPhone);
        }
        else if( role == AgentRole.agent_SoftPhone ){
            request.getTag_2_Bdy().setEdtn(HttpRegister.sf_TEditionCode_Android_Old);
            request.getTag_2_Bdy().setVr(HttpRegister.sf_Version_SoftPhone);
            request.getTag_2_Bdy().setOSVr(HttpRegister.sf_OSName_SoftPhone);
            request.getTag_2_Bdy().setOSBld(HttpRegister.sf_OSBuild_SoftPhone);
        }
        else if( role == AgentRole.agent_Android ){
            request.getTag_2_Bdy().setEdtn(HttpRegister.sf_TEditionCode_Android);
            request.getTag_2_Bdy().setVr(Build.VERSION.RELEASE);
            request.getTag_2_Bdy().setOSVr(HttpRegister.sf_OSName_Android);
            request.getTag_2_Bdy().setOSBld(Integer.toString(Compatibility.getApiLevel()));
        }
        
		request.getTag_2_Bdy().setPC( Build.DEVICE + "-" + Build.ID );
		request.getTag_2_Bdy().setOSUsr( Build.USER );
		request.getTag_2_Bdy().setOSDom("");
		
		return request;
	}
	
	/*
	 * 
	 */
	private void updateCallbackList(IHttpRegisterCallback callback, boolean add ){
		if( !add ){
			m_callback = null;
		}
		else if(callback != null ){
			m_callback = callback;
		}
	}

	/* (non-Javadoc)
	 * @see com.rcbase.api.httpreg.IHttpRegister#addCallback(com.rcbase.api.httpreg.IHttpRegisterCallback)
	 */
	@Override
	public void addCallback(IHttpRegisterCallback callback) {
		updateCallbackList( callback, true );
	}

	/* (non-Javadoc)    		

	 * @see com.rcbase.api.httpreg.IHttpRegister#delCallback(com.rcbase.api.httpreg.IHttpRegisterCallback)
	 */
	@Override
	public void delCallback(IHttpRegisterCallback callback) {
		updateCallbackList( callback, false );
	}

	/* (non-Javadoc)
	 * @see com.rcbase.api.httpreg.IHttpRegister#register(com.rcbase.api.parsers.httpreg.HttpRegisterRequest, com.rcbase.api.httpreg.IHttpRegisterCallback)
	 */
	@Override
	public boolean register(HttpRegisterRequest request ) {
		
		boolean result = false;
		HttpRegisterResponse response = null;
		int http_error = 0;
		if (LogSettings.MARKET) {
            MktLog.e(ServicePJSIP.TAG_EMG, "HttpRegistration:register->");
        }
        if (request != null) {
            ByteArrayOutputStream s_out = new ByteArrayOutputStream();
            try {
                if (request.toStream(s_out)) {
                    final String sRequest = sf_HTTPRegister_Header + s_out.toString();
                    try {
                        if (LogSettings.ENGINEERING) {
                            EngLog.i(TAG, "register():request:" + sRequest);
                        }
                        HttpResponse httpResponse = executeHttpRequest(sRequest, RCMConfig.get_HTTPREG_URL(RingCentralApp.getContextRC(), m_alwaysFromDB ));
                        InputStream is = null;
                        
                        if (httpResponse != null) {
                            if (LogSettings.ENGINEERING) {
                                EngLog.i(TAG, "register():statusLine:" + httpResponse.getStatusLine());
                            }
                            http_error = httpResponse.getStatusLine().getStatusCode();
                            if (http_error == 200) {
                                response = new HttpRegisterResponse(null);
                                try {
                                    is = httpResponse.getEntity().getContent();
                                    response.parseResponse(is);
                                    if (LogSettings.ENGINEERING) {
                                        EngLog.i(TAG, "register():parseResponse: Done");
                                    }
                                    result = true;
                                } catch (IllegalStateException e) {
                                    if (LogSettings.MARKET) {
                                        MktLog.e(TAG, "register() ", e );
                                    }
                                    response = null;
                                } catch (ParserConfigurationException e) {
                                    if (LogSettings.MARKET) {
                                        MktLog.e(TAG, "register() ", e );
                                    }
                                    response = null;
                                } catch (SAXException e) {
                                    if (LogSettings.MARKET) {
                                        MktLog.e(TAG, "register() ", e );
                                    }
                                    response = null;
                                } finally {
                                    if (is != null) {
                                        try {
                                            is.close();
                                            is = null;
                                        } catch (Throwable tr1) {
                                        }
                                    }
                                    
                                    if (httpResponse != null) {
                                        try {
                                            httpResponse.getEntity().consumeContent();
                                            httpResponse = null;
                                        } catch (Throwable tr1) {
                                        }
                                    }
                                }
                            }
                        }
                    } catch (IOException e) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "register(): ", e );
                        }
                    }
                } else {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "register(), request.toStream false");
                    }
                }
            } catch (WrongXmlSource e) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "register(): ", e );
                }
            }
        }
        if (LogSettings.MARKET) {
            MktLog.e(ServicePJSIP.TAG_EMG, "HttpRegistration:register<-");
        }
        
		if( m_callback != null ){
			if( result ){
				m_callback.onFinished((result ? response : null ));
			}
			else{
				m_callback.onFailed(http_error);
			}
		}
		
		return result;
	}

	/*
	 * Temporary solution
	 */
	private static HttpResponse executeHttpRequest(final String body,
			final String urlString )
			throws IOException {

		HttpClient client = HttpClientFactory.getHttpClient();
		HttpEntity entity = new StringEntity(body);

		final HttpPost post = new HttpPost(urlString);
		post.addHeader(CONTENT_TYPE, TEXT_XML_CHARSET_UTF_8);
		post.addHeader(CONNECTION, KEEP_ALIVE);

		post.setEntity(entity);

		HttpResponse response;
		try {
			response = client.execute(post);
		} catch (java.lang.Throwable e) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, " failed client.execute: error:", e );
            }
			return null;
		}
		return response;
	}	
	
}
