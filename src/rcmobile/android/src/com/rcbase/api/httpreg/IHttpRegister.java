/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.api.httpreg;

import com.rcbase.parsers.httpreg.HttpRegisterRequest;

/**
 * @author Denis Kudja
 *
 */
public interface IHttpRegister {
	public static final String KEEP_ALIVE = "Keep-Alive";
	public static final String CONNECTION = "Connection";
	public static final String TEXT_XML_CHARSET_UTF_8 = "text/xml; charset=utf-8";
	public static final String CONTENT_TYPE = "Content-Type";
	
	public boolean register( HttpRegisterRequest request );
	public void addCallback( IHttpRegisterCallback callback );
	public void delCallback( IHttpRegisterCallback callback );
}
