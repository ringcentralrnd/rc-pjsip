/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.api.httpreg;

import com.rcbase.parsers.httpreg.HttpRegisterResponse;

/**
 * @author Denis Kudja
 *
 */
public interface IHttpRegisterCallback {

	public void onFinished( HttpRegisterResponse response );
	
	public void onFailed( int http_error );
}
