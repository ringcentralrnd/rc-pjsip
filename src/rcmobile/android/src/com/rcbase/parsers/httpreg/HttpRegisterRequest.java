/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.parsers.httpreg;

import com.rcbase.api.xml.XmlSerializableObjectAbstract;
import com.rcbase.parsers.sipmessage.SipMessage;
import com.rcbase.parsers.sipmessage.SipMessageHeaderOutput;

/**
 * @author Denis Kudja
 *
 */
public class HttpRegisterRequest extends XmlSerializableObjectAbstract {

	private final static String sf_tag_name		= "Msg";
	
	private SipMessageHeaderOutput m_header = new SipMessageHeaderOutput(SipMessage.sf_CMD_HTTP_AgentInfo);
	private HttpRegisterRequestBody m_body = new HttpRegisterRequestBody();
	
	public boolean isValid(){
		return ( m_header.isValid() && m_body.isValid() );
	}
	
	/*
	 * 
	 */
	public SipMessageHeaderOutput getTag_1_Hdr(){
		return m_header;
	}
	/*
	 * 
	 */
	public HttpRegisterRequestBody getTag_2_Bdy(){
		return m_body;
	}
	
	@Override
	public String getTagName() {
		return sf_tag_name;
	}

}
