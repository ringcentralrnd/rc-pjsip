/**
 * 
 */
package com.rcbase.parsers.httpreg;

import com.rcbase.api.xml.XmlParsingObjectAbstract;

/**
 * @author Denis Kudja
 * 
 * Note: There is support for multiple accounts (?). 
 * If there is true then this feature does not have support on server side and does not use for now
 *
 */
public class HttpRegisterResponseMonAccountHandler extends XmlParsingObjectAbstract {

	private final static String sf_tag_name	= "MonAcc";
	
	/**
	 * @param parent
	 */
	public HttpRegisterResponseMonAccountHandler(XmlParsingObjectAbstract parent) {
		super(parent);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.ringcentral.android.api.IXmlParsingObject#getTagName()
	 */
	@Override
	public String getTagName() {
		return sf_tag_name;
	}

}
