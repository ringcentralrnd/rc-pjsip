/**
 * 
 */
package com.rcbase.parsers.httpreg;

import com.rcbase.api.httpreg.HttpRegister;
import com.rcbase.api.passwords.Passwords;
import com.rcbase.api.xml.XmlSerializableObjectAbstract;

/**
 * @author Denis Kudja
 *
 */
public class HttpRegisterRequestBody extends XmlSerializableObjectAbstract {

	private final static String sf_tag_name		= "Bdy";
	
	private String m_Cln;
	private String m_Inst	= "0";
	private String m_Vr;
	private String m_Ext;
	private String m_Pn;
	private String m_SP;
	private String m_FNm;
	private String m_LNm;
	private String m_Eml;
	private String m_Edtn = String.valueOf(HttpRegister.sf_TEditionCode_Android); //Edition
	private String m_CnTp;
	private String m_OSVr;
	private String m_OSBld;
	private String m_AgnLb;	//Agent label
	private String m_Cntry	= "ENU";
	private String m_LngAgn = "1033";
	private String m_LngSys = "1033";
	private String m_SkSch;
	private String m_SkNm;
	private String m_PC;
	private String m_Cntr	= "1";
	private String m_Area	= "650";
	private String m_OSUsr;
	private String m_OSDom;
	
	/*
	 * 
	 */
	public boolean isValid(){
		if( m_Ext == null || m_Ext.length() == 0 ) 
			return false;
		if( m_SP == null || m_SP.length() == 0 )
			return false;
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.ringcentral.api.xml.IXmlSerializableObject#getTagName()
	 */
	@Override
	public String getTagName() {
		return sf_tag_name;
	}


	public String getAttr_Cln() {
		return m_Cln;
	}


	public void setCln(String cln) {
		m_Cln = cln;
	}


	public String getAttr_Inst() {
		return m_Inst;
	}


	public void setInst(String inst) {
		m_Inst = inst;
	}


	public String getAttr_Vr() {
		return m_Vr;
	}


	public void setVr(String vr) {
		m_Vr = vr;
	}


	public String getAttr_Ext() {
		return m_Ext;
	}


	public void setExt(String ext) {
		m_Ext = ext;
	}


	public String getAttr_Pn() {
		return m_Pn;
	}


	public void setPn(String pn) {
		m_Pn = pn;
	}


	public String getAttr_SP() {
		return ( m_SP == null ? m_SP : Passwords.getPasswordHttpReg(m_SP) );
	}


	public void setSP(String sP) {
		m_SP = sP;
	}


	public String getAttr_FNm() {
		return m_FNm;
	}


	public void setFNm(String fNm) {
		m_FNm = fNm;
	}


	public String getAttr_LNm() {
		return m_LNm;
	}


	public void setLNm(String lNm) {
		m_LNm = lNm;
	}


	public String getAttr_Eml() {
		return m_Eml;
	}


	public void setEml(String eml) {
		m_Eml = eml;
	}


	public String getAttr_Edtn() {
		return m_Edtn;
	}


	public void setEdtn(String edtn) {
		m_Edtn = edtn;
	}

	public void setEdtn(int edtn) {
		m_Edtn = Integer.toString(edtn);
	}

	public String getAttr_CnTp() {
		return m_CnTp;
	}


	public void setCnTp(String cnTp) {
		m_CnTp = cnTp;
	}


	public String getAttr_OSVr() {
		return m_OSVr;
	}


	public void setOSVr(String oSVr) {
		m_OSVr = oSVr;
	}


	public String getAttr_OSBld() {
		return m_OSBld;
	}


	public void setOSBld(String oSBld) {
		m_OSBld = oSBld;
	}


	public String getAttr_AgnLb() {
		return m_AgnLb;
	}


	public void setAgnLb(String agnLb) {
		m_AgnLb = agnLb;
	}


	public String getAttr_Cntry() {
		return m_Cntry;
	}


	public void setCntry(String cntry) {
		m_Cntry = cntry;
	}


	public String getAttr_LngAgn() {
		return m_LngAgn;
	}


	public void setLngAgn(String lngAgn) {
		m_LngAgn = lngAgn;
	}


	public String getAttr_LngSys() {
		return m_LngSys;
	}


	public void setLngSys(String lngSys) {
		m_LngSys = lngSys;
	}


	public String getAttr_SkSch() {
		return m_SkSch;
	}


	public void setSkSch(String skSch) {
		m_SkSch = skSch;
	}


	public String getAttr_SkNm() {
		return m_SkNm;
	}


	public void setSkNm(String skNm) {
		m_SkNm = skNm;
	}


	public String getAttr_PC() {
		return m_PC;
	}


	public void setPC(String pC) {
		m_PC = pC;
	}


	public String getAttr_Cntr() {
		return m_Cntr;
	}


	public void setCntr(String cntr) {
		m_Cntr = cntr;
	}


	public String getAttr_Area() {
		return m_Area;
	}


	public void setArea(String area) {
		m_Area = area;
	}


	public String getAttr_OSUsr() {
		return m_OSUsr;
	}


	public void setOSUsr(String oSUsr) {
		m_OSUsr = oSUsr;
	}


	public String getAttr_OSDom() {
		return m_OSDom;
	}


	public void setOSDom(String oSDom) {
		m_OSDom = oSDom;
	}

}
