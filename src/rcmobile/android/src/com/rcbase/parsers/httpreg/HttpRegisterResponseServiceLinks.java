/**
 * 
 */
package com.rcbase.parsers.httpreg;

import org.xml.sax.Attributes;

import com.rcbase.api.xml.XmlParsingObjectAbstract;

/**
 * @author Denis Kudja
 *
 */
public class HttpRegisterResponseServiceLinks extends XmlParsingObjectAbstract {

	private final static String sf_tag_name	= "SrvAcs";
	
	private final static String sf_SteSrv		= "SteSrv";		//service site
	private final static String sf_SteCoBrnd	= "SteCoBrnd";	//brand site
	private final static String sf_SteCmpn		= "SteCmpn";	//company site
	private final static String sf_PgHlp		= "PgHlp";		//Help page
	private final static String sf_PgLgn		= "PgLgn";		//Login page
	private final static String sf_PgWN			= "PgWN";		//What's New page
	private final static String sf_PgUpgrNw		= "PgUpgrNw";
	private final static String sf_PgCL			= "PgCL";
	private final static String sf_PgDnl		= "PgDnl";
	private final static String sf_PgBM			= "PgBM";
	private final static String sf_PgFAQ		= "PgFAQ";
	private final static String sf_PgDnlSF		= "PgDnlSF";
	private final static String sf_PgAbDst		= "PgABDst";
	private final static String sf_PgPrfs		= "PgPrfs";
	private final static String sf_PgCV			= "PgCV";
	private final static String sf_PGCCC		= "PGCCC";
	private final static String sf_PGPHD		= "PGPHD";
	private final static String sf_PgCDW		= "PgCDW";
	private final static String sf_EmlSupp		= "EmlSupp";	//support email
	private final static String sf_SrvMsgSnc	= "SrvMsgSnc"; 	//message SYNC server
	private final static String sf_SrvABSnc		= "SrvABSnc";	//AB SYNC server
	private final static String sf_SrvReg		= "SrvReg";		//HTTP registration server
	
	private String m_SteSrv;	
	private String m_SteCoBrnd;
	private String m_SteCmpn;	
	private String m_PgHlp;	
	private String m_PgLgn;	
	private String m_PgWN;		
	private String m_PgUpgrNw;	
	private String m_PgCL;		
	private String m_PgDnl;	
	private String m_PgBM;		
	private String m_PgFAQ;	
	private String m_PgDnlSF;	
	private String m_PgAbDst;	
	private String m_PgPrfs;	
	private String m_PgCV;		
	private String m_PGCCC;	
	private String m_PGPHD;	
	private String m_PgCDW;	
	private String m_EmlSupp;	
	private String m_SrvMsgSnc;
	private String m_SrvABSnc;	
	private String m_SrvReg;	

	/**
	 * @param parent
	 */
	public HttpRegisterResponseServiceLinks(XmlParsingObjectAbstract parent) {
		super(parent);
	}

	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void  endElement  (String uri, String localName, String qName){
		super.endElement(uri, localName, qName);
	}	
	
	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#parseAttributes(org.xml.sax.Attributes)
	 */
	protected void parseAttributes(Attributes attributes){
		
		for( int i = 0; i < attributes.getLength(); i++ ){
			String elementName = attributesGetName( attributes, i );
			
			if( elementName.equalsIgnoreCase(sf_SteSrv) ){
				m_SteSrv = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_SteCoBrnd) ){
				m_SteCoBrnd = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_SteCmpn) ){
				m_SteCmpn = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_PgHlp) ){
				m_PgHlp = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_PgLgn) ){
				m_PgLgn = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_PgWN) ){
				m_PgWN = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_PgUpgrNw) ){
				m_PgUpgrNw = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_PgCL) ){
				m_PgCL = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_PgDnl) ){
				m_PgDnl = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_PgBM) ){
				m_PgBM = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_PgFAQ) ){
				m_PgFAQ = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_PgDnlSF) ){
				m_PgDnlSF = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_PgAbDst) ){
				m_PgAbDst = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_PgPrfs) ){
				m_PgPrfs = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_PgCV) ){
				m_PgCV = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_PGCCC) ){
				m_PGCCC = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_PGPHD) ){
				m_PGPHD = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_PgCDW) ){
				m_PgCDW = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_EmlSupp) ){
				m_EmlSupp = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_SrvMsgSnc) ){
				m_SrvMsgSnc = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_SrvABSnc) ){
				m_SrvABSnc = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_SrvReg) ){
				m_SrvReg = attributes.getValue(i);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see com.ringcentral.android.api.IXmlParsingObject#getTagName()
	 */
	@Override
	public String getTagName() {
		return sf_tag_name;
	}

	public String getSteSrv() {
		return m_SteSrv;
	}

	public String getSteCoBrnd() {
		return m_SteCoBrnd;
	}

	public String getSteCmpn() {
		return m_SteCmpn;
	}

	public String getPgHlp() {
		return m_PgHlp;
	}

	public String getPgLgn() {
		return m_PgLgn;
	}

	public String getPgWN() {
		return m_PgWN;
	}

	public String getPgUpgrNw() {
		return m_PgUpgrNw;
	}

	public String getPgCL() {
		return m_PgCL;
	}

	public String getPgDnl() {
		return m_PgDnl;
	}

	public String getPgBM() {
		return m_PgBM;
	}

	public String getPgFAQ() {
		return m_PgFAQ;
	}

	public String getPgDnlSF() {
		return m_PgDnlSF;
	}

	public String getPgAbDst() {
		return m_PgAbDst;
	}

	public String getPgPrfs() {
		return m_PgPrfs;
	}

	public String getPgCV() {
		return m_PgCV;
	}

	public String getPGCCC() {
		return m_PGCCC;
	}

	public String getPGPHD() {
		return m_PGPHD;
	}

	public String getPgCDW() {
		return m_PgCDW;
	}

	public String getEmlSupp() {
		return m_EmlSupp;
	}

	public String getSrvMsgSnc() {
		return m_SrvMsgSnc;
	}

	public String getSrvABSnc() {
		return m_SrvABSnc;
	}

	public String getSrvReg() {
		return m_SrvReg;
	}

}
