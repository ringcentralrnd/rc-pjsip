/**
 * 
 */
package com.rcbase.parsers.httpreg;

import org.xml.sax.Attributes;

import com.rcbase.api.xml.XmlParsingObjectAbstract;

/**
 * @author Denis Kudja
 *
 */
public class HttpRegisterResponseHeaderHandler extends XmlParsingObjectAbstract {

	private final static String sf_tag_name		= "Hdr";
	
	private final static String sf_SID	= 	"SID";
	private final static String sf_Req	=	"Req";
	private final static String sf_From	= 	"From";
	private final static String sf_To	= 	"To";
	private final static String sf_Cmd	= 	"Cmd";
	
	private String m_SID;
	private String m_Req;
	private String m_From;
	private String m_To;
	private String m_Cmd;
	
	/*
	 * 
	 */
	public boolean isValid(){
		if( m_Cmd == null || m_Cmd.length() == 0 )
			return false;
		Integer cmd = Integer.decode(m_Cmd);
		if( cmd == null || cmd.intValue() == 0 )
			return false;
		
		return true;
	}
	
	/**
	 * @param parent
	 */
	public HttpRegisterResponseHeaderHandler(HttpRegisterResponse parent) {
		super(parent);
	}

	/* (non-Javadoc)
	 * @see com.ringcentral.android.api.IXmlParsingObject#getTagName()
	 */
	@Override
	public String getTagName() {
		return sf_tag_name;
	}

	public String getSID() {
		return m_SID;
	}

	public String getReq() {
		return m_Req;
	}

	public String getFrom() {
		return m_From;
	}

	public String getTo() {
		return m_To;
	}

	public String getCmd() {
		return m_Cmd;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#parseAttributes(org.xml.sax.Attributes)
	 */
	protected void parseAttributes(Attributes attributes){
		
		for( int i = 0; i < attributes.getLength(); i++ ){
			String elementName = attributesGetName( attributes, i );
			
			if( elementName.equalsIgnoreCase(sf_SID) ){
				m_SID = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Req) ){
				m_Req = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_From) ){
				m_From = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_To) ){
				m_To = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Cmd) ){
				m_Cmd = attributes.getValue(i);
			}
		}
	}
	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void  endElement  (String uri, String localName, String qName){
		super.endElement(uri, localName, qName);
	}	
}
