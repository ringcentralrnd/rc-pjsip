/**
 * 
 */
package com.rcbase.parsers.httpreg;

import org.xml.sax.Attributes;

import com.rcbase.api.xml.XmlParsingObjectAbstract;

/**
 * @author Denis Kudja
 *
 */
public class HttpRegisterResponseSystemMsgHandler extends XmlParsingObjectAbstract {

	private final static String sf_tag_name		= "SysMsg";
	
	private final static String sf_Id		= "Id";
	private final static String sf_Txt		= "Txt";
	private final static String sf_TTTxt	= "TTTxt";
	private final static String sf_Img		= "Img";
	private final static String sf_Pg		= "Pg";
	private final static String sf_CE		= "CE";
	private final static String sf_Prior	= "Prior";
	
	private String m_Id;	
	private String m_Txt;	
	private String m_TTTxt;
	private String m_Img;	
	private String m_Pg;	
	private String m_CE;	
	private String m_Prior;
	
	/**
	 * @param parent
	 */
	public HttpRegisterResponseSystemMsgHandler(XmlParsingObjectAbstract parent) {
		super(parent);
	}

	/* (non-Javadoc)
	 * @see com.ringcentral.android.api.IXmlParsingObject#getTagName()
	 */
	@Override
	public String getTagName() {
		return sf_tag_name;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void  endElement  (String uri, String localName, String qName){
		super.endElement(uri, localName, qName);
	}	
	
	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#parseAttributes(org.xml.sax.Attributes)
	 */
	protected void parseAttributes(Attributes attributes){
		
		for( int i = 0; i < attributes.getLength(); i++ ){
			String elementName = attributesGetName( attributes, i );
			
			if( elementName.equalsIgnoreCase(sf_Id) ){
				m_Id = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Txt) ){
				m_Txt = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_TTTxt) ){
				m_TTTxt = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Img) ){
				m_Img = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Pg) ){
				m_Pg = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_CE) ){
				m_CE = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Prior) ){
				m_Prior = attributes.getValue(i);
			}
			
		}
	}

	public String getId() {
		return m_Id;
	}

	public String getTxt() {
		return m_Txt;
	}

	public String getTTTxt() {
		return m_TTTxt;
	}

	public String getImg() {
		return m_Img;
	}

	public String getPg() {
		return m_Pg;
	}

	public String getCE() {
		return m_CE;
	}

	public String getPrior() {
		return m_Prior;
	}
}
