/**
 * 
 */
package com.rcbase.parsers.httpreg;

import com.rcbase.api.xml.XmlParsingObjectAbstract;

/**
 * @author Denis Kudja
 *
 */
public class HttpRegisterResponse extends XmlParsingObjectAbstract {

	private final static String sf_tag_name		= "Msg";
	
	/*
	 * List of HTTP registration errors 
	 * see m_Error field of HttpRegisterResponseBodyHandler
	 */
	public final static int sf_REG_ERROR_Unknown			= -1;
	public final static int sf_REG_ERROR_OK					= 0;
	public final static int sf_REG_ERROR_InvalidAuthInfo	= 1;
	public final static int sf_REG_ERROR_Suspended			= 2;
	public final static int sf_REG_ERROR_InvalidAgentType	= 3;
	public final static int sf_REG_ERROR_UpgradeRequired	= 4;
	public final static int sf_REG_ERROR_TooManyAgents		= 5;
	public final static int sf_REG_ERROR_ConnectToServer	= 6;
	public final static int sf_REG_ERROR_ProxyAuthenticationRequired	= 7;
	public final static int sf_REG_ERROR_RegistrationDenied= 8;
	
	private HttpRegisterResponseHeaderHandler m_hdr = null;
	private HttpRegisterResponseBodyHandler m_body = null;
	
	/*
	 * 
	 */
	public HttpRegisterResponse(XmlParsingObjectAbstract parent) {
		super(parent);
		
		m_hdr = new HttpRegisterResponseHeaderHandler(this);
		m_body = new HttpRegisterResponseBodyHandler(this);
		
		
		this.addChild(m_hdr.getTagName(), m_hdr );
		this.addChild(m_body.getTagName(), m_body );
	}

	/*
	 * 
	 */
	public boolean isValid(){
		return ( m_hdr.isValid() && m_body.isValid() );
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.rcbase.api.xml.IXmlParsingObject#getTagName()
	 */
	@Override
	public String getTagName() {
		return sf_tag_name;
	}

	/*
	 * 
	 */
	public HttpRegisterResponseHeaderHandler getHeader(){
		return m_hdr;
	}
	
	public HttpRegisterResponseBodyHandler getBody(){
		return m_body;
	}
	
	public HttpRegisterResponseSystemMsgHandler getSystemMsg(){
		return getBody().getSystemMsg();
	}
	
	public HttpRegisterResponseServiceLinks getServiceLinks(){
		return getBody().getServiceLinks();
	}
}
