/**
 * 
 */
package com.rcbase.parsers.httpreg;

import org.xml.sax.Attributes;

import com.rcbase.api.xml.XmlParsingObjectAbstract;

/**
 * @author Denis Kudja
 *
 */
public class HttpRegisterResponseBodyHandler extends XmlParsingObjectAbstract {
	
	private final static String sf_tag_name		= "Bdy";
	
	private final static String sf_Cln		= "Cln";
	private final static String sf_Inst		= "Inst";
	private final static String sf_Flgs		= "Flgs";
	private final static String sf_VrLst	= "VrLst";
	private final static String sf_OldMsg	= "OldMsg";
	private final static String sf_NewMsg	= "NewMsg";
	private final static String sf_Mny		= "Mny";
	private final static String sf_MB		= "MB";
	private final static String sf_UpgrURL	= "UpgrURL";
	private final static String sf_LogoURL	= "LogoURL";
	private final static String sf_FullNm	= "FullNm";
	private final static String sf_SrvNm	= "SrvNm";
	private final static String sf_BrnNm	= "BrnNm";
	private final static String sf_TierLvl	= "TierLvl";
	private final static String sf_SrvMsk	= "SrvMsk";
	private final static String sf_SrvMskExt= "SrvMskExt";
	private final static String sf_TrlPrd	= "TrlPrd";
	private final static String sf_TrlPrdLeft	= "TrlPrdLeft";
	private final static String sf_STrlWrng	= "STrlWrng";
	private final static String sf_BlngFlrs	= "BlngFlrs";
	private final static String sf_SipFlgs	= "SipFlgs";
	private final static String sf_Prx		= "Prx";
	private final static String sf_ObndPrx	= "ObndPrx";
	private final static String sf_LowBnd	= "LowBnd";
	private final static String sf_LstMsgChngId	= "LstMsgChngId";
	private final static String sf_Err		= "Err";
	private final static String sf_Ext		= "Ext";
	private final static String sf_Pn		= "Pn";
	private final static String sf_MboxId	= "MboxId";
	private final static String sf_PNumId	= "PNumId";
	private final static String sf_AgnLb	= "AgnLb";
	private final static String sf_PrgNm	= "PrgNm";
	private final static String sf_PrgNm2	= "PrgNm2";
	private final static String sf_E911W	= "E911W";
	private final static String sf_ABGlobalModID	= "ABGlobalModID";
	private final static String sf_ExtGlobalModID	= "ExtGlobalModID";
	private final static String sf_PhnDataModID		= "PhnDataModID";
	private final static String sf_AFlgs	= "AFlgs";
	
	private String m_ClientId;
	private String m_InstanceId;

	private int m_MsgNewCount = 0;
	private int m_MsgOldCount = 0;
	
	private long m_ServiceMaskLow	= 0;
	private long m_ServiceMaskHigh	= 0;
	
	private String m_ServiceName;
	private String m_BrandName;
	
	private String m_SipService;
	private String m_SipOutboundProxy;
	
	private String m_Extension;
	private String m_Pin;
	private String m_MailboxId;
	private String m_UserFullName;

	private String m_E911Warning;
	private int m_Error = 0;
	
	private long m_MsgModifCounter = 0;
	private long m_AbSyncModifCounter = 0;
	private long m_ExtModifCounter = 0;
	private long m_PhoneModifCounter = 0;

	private HttpRegisterResponseSystemMsgHandler m_sysMsg = null;
	private HttpRegisterResponseServiceLinks m_svcLinks = null;
	
	private int m_SipFlgs = 0;
	
	/*
	 * 
	 */
	public boolean isValid(){
		if( getInstanceId() == null || getInstanceId().length() == 0  )
			return false; 

		Long instance = Long.decode(getInstanceId());
		if( instance == null || instance.longValue() == 0 )
			return false;
		
		if( getExtension() == null || getExtension().length() <= 0 )
			return false;
		
		return true;
	}
	
	/**
	 * @param parent
	 */
	public HttpRegisterResponseBodyHandler(HttpRegisterResponse parent) {
		super(parent);

		m_sysMsg = new HttpRegisterResponseSystemMsgHandler(this);
		m_svcLinks = new HttpRegisterResponseServiceLinks(this);
		
		this.addChild(m_sysMsg.getTagName(), m_sysMsg );
		this.addChild(m_svcLinks.getTagName(), m_svcLinks );
		
	}

	public HttpRegisterResponseSystemMsgHandler getSystemMsg(){
		return m_sysMsg;
	}
	
	public HttpRegisterResponseServiceLinks getServiceLinks(){
		return m_svcLinks;
	}
	
	
	/* (non-Javadoc)
	 * @see com.ringcentral.android.api.IXmlParsingObject#getTagName()
	 */
	@Override
	public String getTagName() {
		return sf_tag_name;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#endElement(java.lang.String, java.lang.String, java.lang.String)
	 * 
	 * Note: [DK] I have been parsed only useful fields however tags were added for all of them. 
	 * All other fields can be added later.
	 */
	public void  endElement  (String uri, String localName, String qName){
		super.endElement(uri, localName, qName);
	}	

	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#parseAttributes(org.xml.sax.Attributes)
	 */
	protected void parseAttributes(Attributes attributes){
		
		for( int i = 0; i < attributes.getLength(); i++ ){
			String elementName = attributesGetName( attributes, i );
			
			if( elementName.equalsIgnoreCase(sf_Cln) ){
				m_ClientId = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Inst) ){
				m_InstanceId = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_NewMsg) ){
				m_MsgNewCount = getIntValue(attributes.getValue(i));
			}
			else if( elementName.equalsIgnoreCase(sf_OldMsg) ){
				m_MsgOldCount = getIntValue(attributes.getValue(i));
			}
			else if( elementName.equalsIgnoreCase(sf_SrvNm) ){
				m_ServiceName = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_BrnNm) ){
				m_BrandName = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Prx) ){
				m_SipService = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_ObndPrx) ){
				m_SipOutboundProxy = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Ext) ){
				m_Extension = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Pn) ){
				m_Pin = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_MboxId) ){
				m_MailboxId = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_FullNm) ){
				m_UserFullName = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_E911W) ){
				m_E911Warning = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_LstMsgChngId) ){
				m_MsgModifCounter = getLongValue(attributes.getValue(i));
			}
			else if( elementName.equalsIgnoreCase(sf_ABGlobalModID) ){
				m_AbSyncModifCounter = getLongValue(attributes.getValue(i));
			}
			else if( elementName.equalsIgnoreCase(sf_ExtGlobalModID) ){
				m_ExtModifCounter = getLongValue(attributes.getValue(i));
			}
			else if( elementName.equalsIgnoreCase(sf_PhnDataModID) ){
				m_PhoneModifCounter = getLongValue(attributes.getValue(i));
			}
			else if( elementName.equalsIgnoreCase(sf_SrvMsk) ){
				m_ServiceMaskLow = getIntValue(attributes.getValue(i));
			}
			else if( elementName.equalsIgnoreCase(sf_SrvMskExt) ){
				m_ServiceMaskHigh = getIntValue(attributes.getValue(i));
			}
			else if( elementName.equalsIgnoreCase(sf_Err) ){
				m_Error = getIntValue(attributes.getValue(i));
			}
			else if( elementName.equalsIgnoreCase(sf_SipFlgs) ){
				m_SipFlgs = getIntValue(attributes.getValue(i));
			}
			
		}
	}
	
	/*
	 * Service mask is 64 bit integer value. 
	 * However it received as two 32 bit value from server
	 * 
	 * Note: It seems we will have a problem there because low part can have '-149699523' view
	 * and as result we can lost sign during conversion
	 */
	public long getServiceMask(){
		long serviceMask = ((m_ServiceMaskHigh << 32) | m_ServiceMaskLow );
		return serviceMask;
	}
	
	public String getClientId() {
		return m_ClientId;
	}

	public String getInstanceId() {
		return m_InstanceId;
	}

	public int getMsgNewCount() {
		return m_MsgNewCount;
	}

	public int getMsgOldCount() {
		return m_MsgOldCount;
	}

	public String getServiceName() {
		return m_ServiceName;
	}

	public String getBrandName() {
		return m_BrandName;
	}

	public String getSipService() {
		return m_SipService;
	}

	public String getSipOutboundProxy() {
		return m_SipOutboundProxy;
	}

	public String getExtension() {
		return m_Extension;
	}

	public String getPin() {
		return m_Pin;
	}

	public String getMailboxId() {
		return m_MailboxId;
	}

	public String getUserFullName() {
		return m_UserFullName;
	}

	public String getE911Warning() {
		return m_E911Warning;
	}

	public long getMsgModifCounter() {
		return m_MsgModifCounter;
	}

	public long getAbSyncModifCounter() {
		return m_AbSyncModifCounter;
	}

	public long getExtModifCounter() {
		return m_ExtModifCounter;
	}

	public long getPhoneModifCounter() {
		return m_PhoneModifCounter;
	}

	public int getSipFlgs(){
		return m_SipFlgs;
	}
	
	public final int getError() {
		return m_Error;
	}

}
