/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.parsers.sipmessage;

import android.os.Parcel;
import android.os.Parcelable;

import com.rcbase.api.xml.XmlParsingObjectAbstract;


/**
 * @author Denis Kudja
 *
 */
public class SipMessageInput extends XmlParsingObjectAbstract 
	implements Parcelable {

	private final static String sf_tag_name		= "Msg";
	
	private SipMessageHeaderInput m_hdr = null;
	
	public final SipMessageHeaderInput getHdr() {
		return m_hdr;
	}

	public final SipMessageBodyInput getBody() {
		return m_body;
	}

	public final int getCommand(){
		return getHdr().getCmd();
	}
	
	private SipMessageBodyInput m_body	= null;
	
	public SipMessageInput(XmlParsingObjectAbstract parent) {
		super(parent);
		
		m_hdr = new SipMessageHeaderInput( this );
		m_body = new SipMessageBodyInput( this );
		
		this.addChild(m_hdr.getTagName(), m_hdr );
		this.addChild(m_body.getTagName(), m_body );
	}
	
	public SipMessageInput(Parcel in) {
		super(null);
		
		readFromParcel( in );
	}
	

	@Override
	public String getTagName() {
		return sf_tag_name;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void  endElement  (String uri, String localName, String qName){
		super.endElement(uri, localName, qName);
	}
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(m_hdr, flags);
		dest.writeParcelable(m_body, flags);
	}
	
	public void readFromParcel( Parcel in ){
		m_hdr = (SipMessageHeaderInput)in.readParcelable(SipMessageInput.class.getClassLoader());
		m_body = (SipMessageBodyInput)in.readParcelable(SipMessageInput.class.getClassLoader());
	}	
	
	/*
	 * 
	 */
	public static final Parcelable.Creator<SipMessageInput> CREATOR = new Parcelable.Creator<SipMessageInput>() {
		
		public SipMessageInput createFromParcel( Parcel in ){
			return new SipMessageInput(in);
		}
		
		public SipMessageInput[] newArray( int size){
			return new SipMessageInput[size];
		}
	};
	
}
