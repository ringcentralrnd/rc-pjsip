/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.parsers.sipmessage;

import org.xml.sax.Attributes;

import android.os.Parcel;
import android.os.Parcelable;

import com.rcbase.api.xml.XmlParsingObjectAbstract;

/**
 * @author Denis Kudja
 *
 */
public class SipMessageHeaderInput extends XmlParsingObjectAbstract 
		implements Parcelable {

	private final static String sf_tag_name		= "Hdr";
	
	private final static String sf_SID	= 	"SID";
	private final static String sf_Req	=	"Req";
	private final static String sf_From	= 	"From";
	private final static String sf_To	= 	"To";
	private final static String sf_Cmd	= 	"Cmd";
	
	private String m_SID;
	private String m_Req;
	private String m_From;
	private String m_To;
	private int m_Cmd = 0;


	/*
	 * Default constrictor
	 */
	public SipMessageHeaderInput(XmlParsingObjectAbstract parent) {
		super(parent);
	}

	/*
	 * Parcel constructor
	 */
	private SipMessageHeaderInput(Parcel in){
		super(null);
		
		readFromParcel( in );
	}
	
	@Override
	public String getTagName() {
		return sf_tag_name;
	}

	public String getSID() {
		return m_SID;
	}

	public String getReq() {
		return m_Req;
	}

	public String getFrom() {
		return m_From;
	}

	public String getTo() {
		return m_To;
	}

	public int getCmd() {
		return m_Cmd;
	}

	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#parseAttributes(org.xml.sax.Attributes)
	 */
	protected void parseAttributes(Attributes attributes){
		
		for( int i = 0; i < attributes.getLength(); i++ ){
			String elementName = attributesGetName( attributes, i );
			
			if( elementName.equalsIgnoreCase(sf_SID) ){
				m_SID = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Req) ){
				m_Req = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_From) ){
				m_From = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_To) ){
				m_To = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Cmd) ){
				m_Cmd = Integer.parseInt(attributes.getValue(i));
			}
		}
	}
	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void  endElement  (String uri, String localName, String qName){
		super.endElement(uri, localName, qName);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		
		dest.writeString( m_SID ); 
		dest.writeString( m_Req );
		dest.writeString( m_From );
		dest.writeString( m_To );
		dest.writeInt( m_Cmd );
	}	

	public void readFromParcel( Parcel in ){
		 
		m_SID	= in.readString();  
		m_Req	= in.readString();  
		m_From	= in.readString(); 
		m_To	= in.readString();   
		m_Cmd	= in.readInt();
	}	
	
	/*
	 * 
	 */
	public static final Parcelable.Creator<SipMessageHeaderInput> CREATOR = new Parcelable.Creator<SipMessageHeaderInput>() {
		
		public SipMessageHeaderInput createFromParcel( Parcel in ){
			return new SipMessageHeaderInput(in);
		}
		
		public SipMessageHeaderInput[] newArray( int size){
			return new SipMessageHeaderInput[size];
		}
	};
	
}
