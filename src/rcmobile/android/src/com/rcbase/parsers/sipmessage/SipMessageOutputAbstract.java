/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.parsers.sipmessage;

import com.rcbase.api.xml.XmlSerializableObjectAbstract;

/**
 * @author Denis Kudja
 *
 */
public abstract class SipMessageOutputAbstract extends XmlSerializableObjectAbstract {

	private final static String sf_tag_name		= "Msg";
	
	private SipMessageHeaderOutput m_header = null;
	
	/**
	 * 
	 */
	public SipMessageOutputAbstract(int cmd) {
		m_header = new SipMessageHeaderOutput( cmd );
	}
	
	/*
	 * Using for filling fields with data from incoming request.
	 * 
	 * From is used as To and via versa.
	 */
	public void fillHeaderParameters( SipMessageHeaderInput headerIn ){
		if( headerIn != null ){
			m_header.setSID(headerIn.getSID());
			m_header.setFrom(headerIn.getTo());
			m_header.setTo(headerIn.getFrom());
		}
	}

	/* (non-Javadoc)
	 * @see com.rcbase.api.xml.IXmlSerializableObject#getTagName()
	 */
	@Override
	public String getTagName() {
		// TODO Auto-generated method stub
		return sf_tag_name;
	}

	public SipMessageHeaderOutput getTag_1_Hdr(){
		return m_header;
	}
}
