/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */

package com.rcbase.parsers.sipmessage;


/**
 * @author Denis Kudja
 *
 */
public final class SipMessage {
	
	/*
	 * List codes of notification commands are sent through SIP Message.
	 * 
	 * Important: Do not change this codes!
	 */
	public static final int sf_CMD_NONE				= 0;

	//one way messages from server to client
	public static final int sf_CMD_ChangeMsg		= 1;
	public static final int sf_CMD_FreeResources	= 2;
	public static final int sf_CMD_NewMsg			= 3;
	public static final int sf_CMD_ReLogin			= 4;	//no body
	public static final int sf_CMD_ChangePhones		= 5;	//no body
	
	//Call control messages from server to client
	public static final int sf_CMD_IncomingCall		= 6;
	public static final int sf_CMD_AlreadyProcessed	= 7;
	public static final int sf_CMD_ClientMinimize	= 8; //?
	public static final int sf_CMD_SessionClose		= 9;
	
	//Call control messages from client to server
	public static final int sf_CMD_ClientForward		= 10;
	public static final int sf_CMD_ClientVoiceMail		= 11;
	public static final int sf_CMD_ClientReject			= 12;
	public static final int sf_CMD_ClientStartReply		= 13;
	public static final int sf_CMD_ClientReply			= 14;
	public static final int sf_CMD_ClientNotProcessed	= 15;
	public static final int sf_CMD_ClientClosed			= 16;
	public static final int sf_CMD_ClientReceiveConfirm	= 17;
	
	//HTTP register 
	public static final int sf_CMD_HTTP_Registration	= 18; //response from HTTP Register server
	public static final int sf_CMD_HTTP_AgentInfo		= 19; //request on HTTP registration
	
	//Answer call with VM screening
	public static final int sf_CMD_AnswerCallVMScreening	= 26;	//no body
	public static final int sf_CMD_StopVMScreening			= 27;
	
	//one way messages from server to client
	public static final int sf_CMD_ChangeAB				= 28;
	public static final int sf_CMD_ChangeExtension		= 29;
	//Availability status changed
	public static final int sf_CMD_AVLChanged			= 30;

	//Start call recording from client
	public static final int sf_CMD_StartCallRecording	= 31;
	//Stop call recording from client
	public static final int sf_CMD_StopCallRecording	= 32;
	
	
	public final static int MSG_PRIORITY_NORMAL		= 0;
	public final static int MSG_PRIORITY_LOW		= 1;
	public final static int MSG_PRIORITY_HIGH		= 2;
	
	public final static int MSG_TYPE_VOICE			= 0;
	public final static int MSG_TYPE_FAX			= 1;
	public final static int MSG_TYPE_GENERIC		= 2;
	
	public final static int PHONE_TYPE_HOME			= 0;
	public final static int PHONE_TYPE_OFFICE		= 1;
	public final static int PHONE_TYPE_CELL			= 2;
	public final static int PHONE_TYPE_OTHER		= 3;
	
	public final static int CALLER_RESPONSE_TYPE_EMPTY		= 0;
	public final static int CALLER_RESPONSE_TYPE_YES		= 1;
	public final static int CALLER_RESPONSE_TYPE_NO			= 2;
	public final static int CALLER_RESPONSE_TYPE_URGENT		= 3;
	public final static int CALLER_RESPONSE_TYPE_CALLBACK	= 4;
	
	public final static int CALLER_CLOSE_STATUS_OK			= 0;
	public final static int CALLER_CLOSE_STATUS_HANGUP		= 1;
	public final static int CALLER_CLOSE_STATUS_VOICEMAIL	= 2;
	public final static int CALLER_CLOSE_STATUS_FINDME		= 3;
	public final static int CALLER_CLOSE_STATUS_INCOMINGFAX	= 4;
	public final static int CALLER_CLOSE_STATUS_SUBSCRIBER_WENT_TO_MAILBOX		= 5;
	
	/*
	 * Prepare RC Call Control command data structure
	 */
    public static SipMessageOutputAbstract prepareControlCallCommand(final int command, 
    		final String clientId,
    		final SipMessageInput sipMsg ){
    	
    	if( sipMsg == null || sipMsg.getHdr() == null ){
    		return null;
    	}
    	
    	SipMessageOutputAbstract msg = null;

    		switch( command ){
    		case SipMessage.sf_CMD_ClientVoiceMail:		
    		case SipMessage.sf_CMD_ClientReject:			
    		case SipMessage.sf_CMD_ClientStartReply:		
    		case SipMessage.sf_CMD_ClientReply:			
    		case SipMessage.sf_CMD_ClientNotProcessed:	
    		case SipMessage.sf_CMD_ClientClosed:			
    		case SipMessage.sf_CMD_ClientReceiveConfirm:
    		case SipMessage.sf_CMD_AnswerCallVMScreening:
    		case SipMessage.sf_CMD_StopVMScreening:
    			msg = new SipMessageOutputSimple(command, clientId );
    			break;
    		case SipMessage.sf_CMD_ClientForward:
    			msg = new SipMessageOutputForward(clientId);
    			break;
    		}
    		
    		if( msg != null ){
    			msg.fillHeaderParameters( sipMsg.getHdr() );
    		}
    	
    	return msg;
    }

    /*
     * 
     */
    public static String prepareToAddress( final String address ){
    	if( address == null )
    		return null;
    	
    	return "sip:" + address.replaceFirst("#", "%23");
    }
}
