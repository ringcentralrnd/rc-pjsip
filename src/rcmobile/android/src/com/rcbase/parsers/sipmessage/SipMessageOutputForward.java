/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.parsers.sipmessage;

/**
 * SIP message. Client forward.
 * 	sf_CMD_ClientForward (10)
 * 
 * @author Denis Kudja
 *
 */
public class SipMessageOutputForward extends SipMessageOutputAbstract {

	private SipMessageOutputForwardBody m_body = null;
	
	/**
	 * @param cmd
	 */
	public SipMessageOutputForward(String clientId) {
		super(SipMessage.sf_CMD_ClientForward);
	
		m_body = new SipMessageOutputForwardBody( clientId );
	}

	public SipMessageOutputForwardBody getTag_2_Bdy(){
		return m_body;
	}
}
