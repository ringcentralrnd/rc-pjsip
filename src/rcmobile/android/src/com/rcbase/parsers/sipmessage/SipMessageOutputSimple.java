/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.parsers.sipmessage;


/**
 * SIP message. Simple. 
 * Used for:
 *  	sf_CMD_ClientVoiceMail 		(11)
 *  	sf_CMD_ClientReject 		(12)
 *  	sf_CMD_ClientStartReply 	(13)
 *  	sf_CMD_ClientNotProcessed 	(15)
 *  	sf_CMD_ClientClosed 		(16)
 *  	sf_CMD_ClientReceiveConfirm (17)
 *  
 *  	sf_CMD_StartCallRecording 	(31)
 *  	sf_CMD_StopCallRecording	(32)
 *  
 * @author Denis Kudja
 *
 */
public class SipMessageOutputSimple extends SipMessageOutputAbstract {

	private SipMessageOutputSimpleBody m_body = null;
	
	/**
	 * @param cmd
	 */
	public SipMessageOutputSimple(int cmd, String clientId ) {
		super(cmd);
		
		m_body = new SipMessageOutputSimpleBody( clientId );
	}

	public SipMessageOutputSimpleBody getTag_2_Bdy(){
		return m_body;
	}
}
