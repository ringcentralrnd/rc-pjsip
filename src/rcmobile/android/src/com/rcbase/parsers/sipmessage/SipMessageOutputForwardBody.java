/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.parsers.sipmessage;

/**
 * @author Denis Kudja
 *
 */
public class SipMessageOutputForwardBody extends SipMessageOutputSimpleBody {

	private String m_toPhone;
	private int m_phoneType = SipMessage.PHONE_TYPE_OTHER;
	private boolean m_forwardDelay = false;
	
	/**
	 * @param clientId
	 */
	public SipMessageOutputForwardBody(String clientId) {
		super(clientId);
	}

	public String getAttr_Phn() {
		return m_toPhone;
	}

	public void setToPhone(String toPhone) {
		m_toPhone = toPhone;
	}

	public final int getAttr_PhnTp() {
		return m_phoneType;
	}

	public void setPhoneType(int phoneType) {
		m_phoneType = phoneType;
	}

	public final boolean getAttr_FwdDly() {
		return m_forwardDelay;
	}

	public void setForwardDelay(boolean forwardDelay) {
		m_forwardDelay = forwardDelay;
	}
}
