/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.parsers.sipmessage;

import com.rcbase.api.xml.XmlSerializableObjectAbstract;

/**
 * Body for simple SIP message
 * 
 * @author Denis Kudja
 *
 */
public class SipMessageOutputSimpleBody extends XmlSerializableObjectAbstract {

	private final static String sf_tag_name		= "Bdy";
	
	private String m_ClientId;
	
	public SipMessageOutputSimpleBody( String clientId ){
		setClientId(clientId);
	}
	
	@Override
	public String getTagName() {
		return sf_tag_name;
	}
	
	public String getAttr_Cln() {
		return m_ClientId;
	}

	public void setClientId(String clientId) {
		m_ClientId = clientId;
	}

}
