/**
 * 
 */
package com.rcbase.parsers.sipmessage;

import com.rcbase.api.xml.XmlSerializableObjectAbstract;

/**
 * @author Denis Kudja
 *
 */
public class SipMessageHeaderOutput extends XmlSerializableObjectAbstract {

	private final static String sf_tag_name		= "Hdr";
	
	private String m_SID;
	private String m_Req;
	private String m_From;
	private String m_To;
	private String m_Cmd;
	
	/**
	 *	List of commands see on SipMessage (com.rcbase.parsers.sipmessage.SipMessage) 
	 */
	public SipMessageHeaderOutput(int cmd) {
		m_Cmd = Integer.toString(cmd);
	}
	
	public boolean isValid(){
		if( m_Cmd == null || m_Cmd.length() == 0 ) 
			return false;
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.ringcentral.api.xml.IXmlSerializableObject#getTagName()
	 */
	@Override
	public String getTagName() {
		return sf_tag_name;
	}

	public String getAttr_SID() {
		return m_SID;
	}

	public void setSID(String sID) {
		m_SID = sID;
	}

	public String getAttr_Req() {
		return m_Req;
	}

	public void setReq(String req) {
		m_Req = req;
	}

	public String getAttr_From() {
		return m_From;
	}

	public void setFrom(String from) {
		m_From = from;
	}

	public String getAttr_To() {
		return m_To;
	}

	public void setTo(String to) {
		m_To = to;
	}

	public String getAttr_Cmd() {
		return m_Cmd;
	}

	public void setCmd(String cmd) {
		m_Cmd = cmd;
	}

}
