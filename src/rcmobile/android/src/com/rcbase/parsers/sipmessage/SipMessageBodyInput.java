/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.parsers.sipmessage;

import org.xml.sax.Attributes;

import android.os.Parcel;
import android.os.Parcelable;

import com.rcbase.api.xml.XmlParsingObjectAbstract;

/**
 * @author Denis Kudja
 *
 */
public class SipMessageBodyInput extends XmlParsingObjectAbstract 
		implements Parcelable{

	private final static String sf_tag_name		= "Bdy";
	
	private final static String sf_Attr_Cln		= "Cln";
	private final static String sf_Attr_Client		= "Client";
	
	private final static String sf_Attr_OldMsg	= "OldMsg";
	private final static String sf_Attr_NewMsg	= "NewMsg";

	private final static String sf_Attr_Mny		= "Mny";
	private final static String sf_Attr_MB		= "MB";
	
	private final static String sf_Attr_Prior	= "Prior";
	private final static String sf_Attr_Tp		= "Tp";
	private final static String sf_Attr_MsgId	= "MsgId";
	
	
	private final static String sf_Attr_SrvLvl		= "SrvLvl";
	private final static String sf_Attr_SrvLvlExt	= "SrvLvlExt";
	private final static String sf_Attr_Phn			= "Phn";
	private final static String sf_Attr_Pnh			= "Pnh";
	private final static String sf_Attr_Nm			= "Nm";
	private final static String sf_Attr_ToPhn		= "ToPhn";
	private final static String sf_Attr_ToNm		= "ToNm";
	private final static String sf_Attr_RecUrl		= "RecUrl";

	private final static String sf_Attr_IP			= "IP";
	private final static String sf_Attr_Sts			= "Sts";
	private final static String sf_Attr_CtrlCln		= "CtrlCln";
	
	private final static String sf_Attr_ExtNfo		= "ExtNfo";
	private final static String sf_Attr_Resp		= "Resp";

	private final static String sf_Attr_Reason		= "Reason";
	
	/*
	 * Different fields of Body entity set(used) in depend on Cmd field of Header object
	 */
	
	/*
	 * Number of messages in Inbox was changed
	 * sf_CMD_ChangeMsg (1)
	 * 
	 * Attributes Cln, OldMsg, NewMsg
	 */
	private String 	m_Client;			// Client ID
	private int		m_OldMsg 	= 0;	// Number of read messages on Inbox
	private int 	m_NewMsg	= 0;	// Number of unread messages on Inbox
	
	/*
	 * Free Resource Info
	 * sf_CMD_FreeResources (2)
	 * 
	 * Attributes Mny (money), MB (mailbox space), SrvLvl
	 */
	private double m_MoneyLeft = 0.0;
	private int m_MailboxSpaceLeft = 0;
	
	/*
	 * New message was created on Inbox
	 * sf_CMD_NewMsg (3)
	 * 
	 * Attributes Pnh (FromPhone), Nm (FromName), Prior (MsgPriority), Tp (MsgType), OldMsg, NewMsg, MsgId
	 */
	private int  m_MsgPriority 	= 0;	//message priority
	private int  m_MsgType		= 0;	// message type
	private long m_MsgId		= 0;
	
	/*
	 * Incoming call request
	 * sf_CMD_IncomingCall (6)
	 */
	private long   m_SrvLvl		= 0;
	private long   m_SrvLvlExt	= 0;
	private String m_FromPhone;
	private String m_FromName;
	private String m_ToPhone;
	private String m_ToName;
	private String m_RecUrl;
	
	/*
	 * Already Processed - Call was processed by another Agent
	 * sf_CMD_AlreadyProcessed (7)
	 * 
	 * Attributes IP, Sts, CtrlCln
	 */
	private long 	m_IP;
	private int 	m_CallerCloseStatus;	//see CALLER_CLOSE_STATUS_* constants (SipMessage)
	private String  m_ControlClientId;
	
	/*
	 * Session Close 
	 * sf_CMD_SessionClose (9)
	 * 
	 *  Attributes Phn, Nm, ExtNfo, Resp, Sts
	 */
	private String 	m_ExtInfo;
	private int 	m_CallerResponse;	//see CALLER_RESPONSE_TYPE_* constants (SipMessage)
	
	/*
	 * 
	 * Address book was changed sf_CMD_ChangeAB (28)
	 * Extension was changed sf_CMD_ChangeExtension (29)
	 * 
	 * Attributes Client, Reason
	 */
	private String m_Reason;
	
	/**
	 * @param parent
	 */
	public SipMessageBodyInput(XmlParsingObjectAbstract parent) {
		super(parent);
	}

	public SipMessageBodyInput(Parcel in) {
		super(null);
		readFromParcel( in );
	}
	
	/* (non-Javadoc)
	 * @see com.rcbase.api.xml.IXmlParsingObject#getTagName()
	 */
	@Override
	public String getTagName() {
		return sf_tag_name;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#parseAttributes(org.xml.sax.Attributes)
	 */
	protected void parseAttributes(Attributes attributes){
		
		for( int i = 0; i < attributes.getLength(); i++ ){
			String elementName = attributesGetName( attributes, i );
			
			if( elementName.equalsIgnoreCase(sf_Attr_Cln) || 
					elementName.equalsIgnoreCase(sf_Attr_Client) ){
				m_Client = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_OldMsg) ){
				m_OldMsg = Integer.parseInt( attributes.getValue(i) );
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_NewMsg) ){
				m_NewMsg = Integer.parseInt( attributes.getValue(i) );
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_SrvLvl) ){
				m_SrvLvl = Long.parseLong( attributes.getValue(i) );
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_SrvLvlExt) ){
				m_SrvLvlExt = Long.parseLong( attributes.getValue(i) );
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_Phn) || 
					elementName.equalsIgnoreCase(sf_Attr_Pnh)){
				m_FromPhone = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_Nm) ){
				m_FromName = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_ToPhn) ){
				m_ToPhone = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_ToNm) ){
				m_ToName = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_RecUrl) ){
				m_RecUrl = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_Prior) ){
				m_MsgPriority = Integer.parseInt( attributes.getValue(i) );
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_Tp) ){
				m_MsgType = Integer.parseInt( attributes.getValue(i) );
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_MsgId) ){
				m_MsgId = Long.parseLong( attributes.getValue(i) );
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_IP) ){
				m_IP = Long.parseLong( attributes.getValue(i) );
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_Sts) ){
				m_CallerCloseStatus = Integer.parseInt( attributes.getValue(i) );
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_CtrlCln) ){
				m_ControlClientId = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_ExtNfo) ){
				m_ExtInfo = attributes.getValue(i);
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_Resp) ){
				m_CallerResponse = Integer.parseInt( attributes.getValue(i) );
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_Mny) ){
				m_MoneyLeft = Double.parseDouble( attributes.getValue(i) );
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_MB) ){
				m_MailboxSpaceLeft = Integer.parseInt( attributes.getValue(i) );
			}
			else if( elementName.equalsIgnoreCase(sf_Attr_Reason) ){
				m_Reason = attributes.getValue(i);
			}
		}
	}
	

	/*
	 * (non-Javadoc)
	 * @see com.ringcentral.android.api.XmlParsingObjectAbstract#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void  endElement  (String uri, String localName, String qName){
		super.endElement(uri, localName, qName);
	}

	public String getClient() {
		return m_Client;
	}

	public int getOldMsg() {
		return m_OldMsg;
	}

	public int getNewMsg() {
		return m_NewMsg;
	}

	public long getSrvLvl() {
		return m_SrvLvl;
	}

	public long getSrvLvlExt() {
		return m_SrvLvlExt;
	}

	public String getFromPhone() {
		return m_FromPhone;
	}

	public String getFromName() {
		return m_FromName;
	}

	public String getToPhone() {
		return m_ToPhone;
	}

	public String getToName() {
		return m_ToName;
	}

	public String getRecUrl() {
		return m_RecUrl;
	}

	public int getMsgPriority() {
		return m_MsgPriority;
	}

	public int getMsgType() {
		return m_MsgType;
	}

	public long getMsgId() {
		return m_MsgId;
	}

	public long getIP() {
		return m_IP;
	}

	public int getCallerCloseStatus() {
		return m_CallerCloseStatus;
	}

	public String getControlClientId() {
		return m_ControlClientId;
	}

	public String getExtInfo() {
		return m_ExtInfo;
	}

	public int getCallerResponse() {
		return m_CallerResponse;
	}

	public final double getMoneyLeft() {
		return m_MoneyLeft;
	}

	public final int getMailboxSpaceLeft() {
		return m_MailboxSpaceLeft;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(m_Client);	
		dest.writeInt(m_OldMsg);
		dest.writeInt(m_NewMsg);
		dest.writeInt(m_MsgPriority); 	
		dest.writeInt(m_MsgType);		
		dest.writeLong(m_MsgId);		
		dest.writeLong(m_SrvLvl);		
		dest.writeLong(m_SrvLvlExt);	
		dest.writeString(m_FromPhone);
		dest.writeString(m_FromName); 
		dest.writeString(m_ToPhone);  
		dest.writeString(m_ToName);   
		dest.writeString(m_RecUrl);   
		dest.writeLong(m_IP);               
		dest.writeInt(m_CallerCloseStatus);
		dest.writeString(m_ControlClientId);  
		dest.writeString(m_ExtInfo);       
		dest.writeInt(m_CallerResponse);
		dest.writeDouble(m_MoneyLeft);
		dest.writeInt(m_MailboxSpaceLeft);
		dest.writeString(m_Reason);
	}	
	
	public void readFromParcel( Parcel in ){
		
		m_Client			= in.readString();		
		m_OldMsg			= in.readInt();		
		m_NewMsg     		= in.readInt();        
		m_MsgPriority 		= in.readInt();     
		m_MsgType			= in.readInt();     
		m_MsgId				= in.readLong();	         
		m_SrvLvl			= in.readLong();	     
		m_SrvLvlExt	   		= in.readLong();      
		m_FromPhone			= in.readString();         
		m_FromName			= in.readString();          
		m_ToPhone			= in.readString();           
		m_ToName			= in.readString();            
		m_RecUrl			= in.readString();            
		m_IP				= in.readLong();                
		m_CallerCloseStatus	= in.readInt(); 
		m_ControlClientId	= in.readString();   
		m_ExtInfo			= in.readString();           
		m_CallerResponse	= in.readInt();    
		m_MoneyLeft			= in.readDouble();
		m_MailboxSpaceLeft	= in.readInt();
		m_Reason 			= in.readString(); 
	}
	
	/*
	 * 
	 */
	public static final Parcelable.Creator<SipMessageBodyInput> CREATOR = new Parcelable.Creator<SipMessageBodyInput>() {
		
		public SipMessageBodyInput createFromParcel( Parcel in ){
			return new SipMessageBodyInput(in);
		}
		
		public SipMessageBodyInput[] newArray( int size){
			return new SipMessageBodyInput[size];
		}
	};
	
}
