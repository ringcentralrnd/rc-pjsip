package com.rcbase.android.logging.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.LoggingManager;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.LoggingManager.LOGGING_TYPE;
import com.rcbase.android.logging.LoggingManager.LogsFinalizationReason;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.utils.FileUtils;

/**
 * Copyright (C) 2010-2011, RingCentral, Inc.
 * All Rights Reserved.
 */

/**
 * Class necessary for manipulations related with application runtime and crash logs
 */
public class LogFilesUtils {
    private static final String     TAG = "[RC]LogFilesUtils";
    
    public static String getThePathToTheLogsDirectory() {
        String result = null;

        if (LoggingManager.isStorageAccessible()) {
            String pathForLogs = FileUtils.getSDPathLogs();
            File dir = null;

            try {
                if (null != (dir = new File(pathForLogs)) && dir.exists() && dir.isDirectory()) {
                    result = pathForLogs;
                } else {
                    if (checkTheFolder(pathForLogs)) {
                        result = pathForLogs;
                    }
                }
            } catch (Exception ex) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, " getThePathToTheLogsDirectory " + ex.toString());
                }
            }
        } else {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, " getThePathToTheLogsDirectory The storage is not accessible ");
            }
        }

        return result;
    }

    static boolean smartDelete(File path) {
        if (null != path) {
            if (path.exists()) {
                File[] files = path.listFiles();
                
                if (null != files) {
                    for (int i = files.length - 1; i >= 0; i--) {
                        if (files[i].isDirectory()) {
                            smartDelete(files[i]);
                        } else {
                            files[i].delete();
                        }
                    }
                }
            }

            return (path.delete());
        } else {
            return false;
        }
    }
    
    /**
     * function which checks the existence of folder and create abs if it does not exist
     * @param path - folder path
     * @return - true if folder exist or was successfully created
     */
    public static boolean checkTheFolder(String path) {
        if (LoggingManager.isStorageAccessible()) {
            if (null != path) {
                return new File(path).mkdirs();
            }
        }

        return false;
    }

    /**
     * That method deletes files specified in params
     * @param filesToBeDeleted - list of files which should be deleted
     */
    public static void cleanUp(List<String> filesToBeDeleted) {
        if (null != filesToBeDeleted && 0 < filesToBeDeleted.size()) {
            if (LoggingManager.isStorageAccessible()) {
                try {
                    for (String file : filesToBeDeleted) {
                        File ftbd = new File(file);
                        if (ftbd.exists()) {
                            ftbd.delete();
                        }

                        ftbd = null;
                    }
                } catch (Exception ex) {
                    if (LogSettings.ENGINEERING) {
                        EngLog.e(TAG, " cleanUp exception : ", ex);
                    }
                }
            } else {
                if (LogSettings.ENGINEERING) {
                    EngLog.e(TAG, " cleanUp Storage is not accessible");
                }
            }
        }
    }

    /**
     *
     * @return null if there is no such file or full path to the latest crash log
     */
    public static String getTheLatestCrashLog() {
        String result = null;
        
        try {
            File directoryWithLogs = new File(getThePathToTheLogsDirectory());
            File[] files = null;
            File tmpFile = null;

            if( LoggingManager.isStorageAccessible() && null != directoryWithLogs &&
                directoryWithLogs.isDirectory() && null != (files = directoryWithLogs.listFiles()) &&
                0 < files.length)
            {
                // here getting any file with zip extension
                for (File file : files){
                    if( file.getAbsolutePath().endsWith(".zip") && file.getName().contains(LoggingManager.STR_CRASH) && 
                        file.getName().contains("Application")) {
                        tmpFile = file;
                        break;
                    }
                }

                if(null != tmpFile) {
                    for(File file : files) {
                        if( file.lastModified() > tmpFile.lastModified() && file.getAbsolutePath().endsWith(".zip") && 
                            file.getName().contains(LoggingManager.STR_CRASH)) {
                            tmpFile = file;
                        }
                    }

                    result = tmpFile.getAbsolutePath();
                }
            }
        } catch (Exception e) {
            if(LogSettings.MARKET) {
                MktLog.e(TAG, " getTheLatestCrashLog ", e);
            }
        }

        return result;
    }


    /**
     * That method returns list of logs containing in folder
     * @return
     */
    public static String[] getListOfLogs() {
        String[] resultArrayOfFiles = null;

        try {
            // if the storage is accessible
            if (LoggingManager.isStorageAccessible()) {
                String pathToTheLogsDir = getThePathToTheLogsDirectory();
                if (null != pathToTheLogsDir) {
                    File directoryWithLogs = new File(pathToTheLogsDir);
                    File[] files = null;
                    if (null != directoryWithLogs && directoryWithLogs.isDirectory() && null != (files = directoryWithLogs.listFiles())) {
                        try {
                            sortFilesByDate(files);
                        } catch (Exception e) {
                            if (LogSettings.MARKET) {
                                MktLog.e(TAG, "getListOfLogs exception : ", e);
                            }
                        }

                        int countOfLogs = 0;

                        for (int i = 0; i < files.length & i < LoggingManager.INT_MAX_LOGS_COUNT; i++) {                            
                            if (files[i].getAbsolutePath().contains(".zip")) {
                                countOfLogs++;
                            }
                        }

                        resultArrayOfFiles = new String[(countOfLogs > LoggingManager.INT_MAX_LOGS_COUNT) ? LoggingManager.INT_MAX_LOGS_COUNT : countOfLogs];

                        resultArrayOfFiles = new String[countOfLogs];

                        for (int i = files.length - 1, j = 0; j < countOfLogs; i--) {
                            if (files[i].getAbsolutePath().endsWith(".zip")) {
                                resultArrayOfFiles[j] = files[i].getName();
                                j++;
                            }
                        }
                    }
                }
            }
        } catch (Throwable th) {
            if(LogSettings.MARKET) {
                MktLog.e(TAG, " ", th);
            }
        }

        return resultArrayOfFiles;
    }

    /**
     * That method is necessary for getting absolute path to the file stored in ring central logs storage
     * @param fileName - file name of .zip file with logs
     * @return - null if there is no such file or real string with absolute path to the file with logs
     */
    public static String getPathToTheFileBySpecifiedName(String fileName) {
        String result = null;

        if (null != fileName && LoggingManager.isStorageAccessible()) {
            String pathToTheFolderLogs = getThePathToTheLogsDirectory();

            if (null != pathToTheFolderLogs) {
                File _logsDir = new File(pathToTheFolderLogs);

                if (null != _logsDir && _logsDir.isDirectory()) {
                    File[] files = _logsDir.listFiles();

                    for (int i = 0; i < files.length; i++) {
                        if (files[i].getName().equals(fileName)) {
                            result = files[i].getAbsolutePath();
                            break;
                        }
                    }
                }
            }
        }

        return result;
    }


    /**
     * That method is necessary to maintain logs directory
     */
    public static void maintainLogsDir() {
        String pathToTheFolderLogs = getThePathToTheLogsDirectory();

        try {
            if (LoggingManager.isStorageAccessible() && null != pathToTheFolderLogs) {
                File logsDir = new File(pathToTheFolderLogs);

                if (null != logsDir && logsDir.isDirectory()) {
                    File[] files = logsDir.listFiles();

                    // delete folders (not temp dirs)
                    for (int i = 0; i < files.length; i++) {
                        if (files[i].isDirectory() && !files[i].getAbsolutePath().contains("tmp")) {
                            smartDelete(files[i]);
                        }
                    }

                    // if we have files & count of that files is more then max
                    // specified for logs folder
                    // we should detect - that "over" files & delete them
                    if (null != files && LoggingManager.INT_MAX_LOGS_COUNT < files.length) {
                        sortFilesByDate(files);
                        int countOfFilesToBeDeleted = files.length - LoggingManager.INT_MAX_LOGS_COUNT;

                        for (int i = 0; i < countOfFilesToBeDeleted; i++) {
                            // yep, i'm paranoid - if it's not tempdir - delete it
                            if (files[i].isDirectory() && !files[i].getAbsolutePath().contains("tmp")) {
                                smartDelete(files[i]);
                            } else {
                                files[i].delete();
                            }
                        }
                    }
                }
            }
        } catch (Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, " ", th);
            }
        }
    }

    private static void sortFilesByDate(File[] files) {
        if(null != files && 1 < files.length ) {
            quickSortForFiles(files, 0, files.length - 1);
        }
    }

    /**
     * Quick sort for files, sorting using last modified value
     * I remember only quick-sort algorithm from university course
     * @param files - list of files to be sorted
     * @param start - start index in that array
     * @param end - end index index for sorting
     */
    private static void     quickSortForFiles(File[] files, int start, int end) {
        if (start >= end)
            return;
        int i = start, j = end;
        int cur = i - (i - j) / 2;

        while (i < j)
        {
            while (i < cur && (files[i].lastModified() <= files[cur].lastModified())) {
                i++;
            }

            while (j > cur && (files[cur].lastModified() <= files[j].lastModified())) {
                j--;
            }

            if (i < j) {
                File temp = files[i];
                files[i] = files[j];
                files[j] = temp;

                if (i == cur){
                    cur = j;
                }
                else if (j == cur){
                    cur = i;
                }
            }
        }

        quickSortForFiles(files, start,    cur);
        quickSortForFiles(files, cur + 1,  end);
    }

    public static boolean removeLogFile(String fileToBeDeleted) {
        boolean result = false;
        if(LoggingManager.isStorageAccessible() && null != fileToBeDeleted) {
            try {
                File file = new File(fileToBeDeleted);
                if(null != fileToBeDeleted && file.exists()) {
                    if(file.isDirectory()){
                        smartDelete(file);
                    } else {
                        file.delete();
                    }

                    return !file.exists();
                } 
            }   catch (Exception e) {
                if(LogSettings.MARKET) {
                    MktLog.e(TAG, " Ex removeLogFile ", e);
                }
            }
        }
        return result;
    }

    public static String getTheLatestServiceCrashLog() {
        String result = null;
        if (BUILD.VOIP_ENABLED) {
        try {
            File directoryWithLogs = new File(getThePathToTheLogsDirectory());
            File[] files = null;
            File tmpFile = null;

            if( LoggingManager.isStorageAccessible() && null != directoryWithLogs &&
                directoryWithLogs.isDirectory() && null != (files = directoryWithLogs.listFiles()) &&
                0 < files.length){
                // here getting any file with zip extension
                for (File f : files){
                    if(f.getAbsolutePath().endsWith(".zip") && f.getName().contains(LoggingManager.STR_CRASH) && f.getName().contains("Service")) {
                        tmpFile = f;
                        break;
                    }
                }

                if(null != tmpFile) {
                    for(File file : files) {
                        if( file.lastModified() > tmpFile.lastModified() && file.getAbsolutePath().endsWith(".zip") &&
                            file.getName().contains(LoggingManager.STR_CRASH) && file.getName().contains("Service")) {
                            tmpFile = file;
                        }
                    }

                    result = tmpFile.getAbsolutePath();
                }

            }
        }   catch (Exception e) {
            if(LogSettings.ENGINEERING) {
                EngLog.e(TAG, " getTheLatestServiceCrashLog ", e);
            }
        }
        }
        return result;
    }
    
	public static void addToPerformanceReport(Hashtable<String, Vector<Long>> results) throws IOException {
		String performanceLogFile = getThePathToTheLogsDirectory() + "/PerformanceMeasurement.log";
		File file = new File(performanceLogFile);
		if (file == null || !file.exists()) {
			file.createNewFile();
		}
		// Read existing data from file
		Hashtable<String, Vector<Long>> existingResults = getExistingData(file.getAbsolutePath());
		// Merge existing data and new
		mergeHashtables(existingResults, results);

		// Adding new data
		saveData(file.getAbsolutePath(), existingResults);
	}

	private static void saveData(String filePath, Hashtable<String, Vector<Long>> existingResults) throws IOException {
		BufferedWriter bufferedWriter;
		FileWriter fw = new FileWriter(filePath);
		bufferedWriter = new BufferedWriter(fw);

		try {
			if (null != bufferedWriter) {
				for (Enumeration<String> types = existingResults.keys(); types.hasMoreElements();) {
					String type = types.nextElement();
					Vector<Long> values = existingResults.get(type);
					if (values != null){
						bufferedWriter.append(type);
						bufferedWriter.newLine();
						for (Long duration : values) {
							bufferedWriter.append(duration.toString());
							bufferedWriter.newLine();
						}
					}
				}
			    bufferedWriter.close();
			}
		} catch (Exception e) {
            if(null != bufferedWriter) {
                try{
                    bufferedWriter.close();
                } catch (Exception ex){
                }
            }
		}
	}

	private static Hashtable<String, Vector<Long>> getExistingData(String filePath)	throws IOException {
		FileReader fr = new FileReader(filePath);
		BufferedReader bufferedReader = new BufferedReader(fr);
		String line = "";
		String currentType = "Unknown";
		Hashtable<String, Vector<Long>> existingResults = new Hashtable<String, Vector<Long>>();
		try {
			while ((line = bufferedReader.readLine()) != null) {
				try {
					long value = Long.parseLong(line);
					Vector<Long> values = existingResults.get(currentType);
					if (values != null){
						values.add(new Long(value));
					}
				} catch (NumberFormatException e) {
					//It is test name
					currentType = line;
					existingResults.put(line, new Vector<Long>());
				}
			}
			bufferedReader.close();
		} catch (Exception e) {
            if(null != bufferedReader) {
                try{
                	bufferedReader.close();
                } catch (Exception ex){
                }
            }
		}
		return existingResults;
	}
    
    private static void mergeHashtables(
			Hashtable<String, Vector<Long>> oldH,
			Hashtable<String, Vector<Long>> newH) {
    	
		for (Enumeration<String> newE = newH.keys(); newE.hasMoreElements();) {
			String newType = newE.nextElement();
			Vector<Long> oldV = oldH.get(newType);
			if (oldV != null){
				Vector<Long> newV = newH.get(newType);
				for (Long newVal : newV) {
					oldV.add(newVal);
				}
			} else {
				Vector<Long> newV = newH.get(newType);
				oldH.put(newType, newV);
			}
		}
	}

}






























