package com.rcbase.android.logging;

/**
 * Copyright (C) 2010-2011, RingCentral, Inc.
 * All Rights Reserved.
 */

abstract class RCLog
{
    protected static void   log(String tag, String msg, LogItem.logLevel levelL, LogItem.logType logType ) {
        if(null != msg && null != tag && null != levelL && null != logType) {
        	try {
        		Logger.newItem(logType, levelL, tag, msg);
        	} catch (java.lang.Throwable th) {
        	}
        }
    }
}
