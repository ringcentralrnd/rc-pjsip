package com.rcbase.android.logging;

import static android.util.Log.getStackTraceString;

/**
 * Copyright (C) 2010-2011, RingCentral, Inc.
 * All Rights Reserved.
 */
public class QaLog extends RCLog
{
    /**
    * Use that method in case of needs of debug traces
    * @param tag
    * @param msg
    */
    public static void d(String tag, String msg) {
        if (null != tag && null != msg) {
            log(tag, msg, LogItem.logLevel.DEBUG, LogItem.logType.QA);
        }
    }

    public static void d(String tag, String msg, Throwable t) {
        if (null != tag && null != t && null != msg) {
            log(tag, msg + " " + getStackTraceString(t), LogItem.logLevel.DEBUG, LogItem.logType.QA);
        }
    }

    public static void d(String tag, Throwable t) {
        if (null != tag && null != t) {
            log(tag, getStackTraceString(t), LogItem.logLevel.DEBUG, LogItem.logType.QA);
        }
    }

    /**
     * Use that method in case of needs of error traces
     * 
     * @param tag
     * @param msg
     */
    public static void e(String tag, String msg) {
        if (null != tag && null != msg) {
            log(tag, msg, LogItem.logLevel.ERROR, LogItem.logType.QA);
        }
    }

    public static void e(String tag, String msg, Throwable t) {
        if (null != tag && null != t && null != msg) {
            log(tag, msg + " " + getStackTraceString(t), LogItem.logLevel.ERROR, LogItem.logType.QA);
        }
    }

    public static void e(String tag, Throwable t) {
        if (null != tag && null != t) {
            log(tag, getStackTraceString(t), LogItem.logLevel.ERROR, LogItem.logType.QA);
        }
    }

    /**
     * Use that method in case of needs of information traces
     * 
     * @param tag
     * @param msg
     */
    public static void i(String tag, String msg) {
        if (null != tag && null != msg) {
            log(tag, msg, LogItem.logLevel.INFORMATION, LogItem.logType.QA);
        }
    }

    public static void i(String tag, String msg, Throwable t) {
        if (null != tag && null != t && null != msg) {
            log(tag, msg + " " + getStackTraceString(t), LogItem.logLevel.INFORMATION, LogItem.logType.QA);
        }
    }

    public static void i(String tag, Throwable t) {
        if (null != tag && null != t) {
            log(tag, getStackTraceString(t), LogItem.logLevel.INFORMATION, LogItem.logType.QA);
        }
    }

    /**
     * Use that method in case of needs of verbose traces
     * 
     * @param tag
     * @param msg
     */
    public static void v(String tag, String msg) {
        if (null != tag && null != msg) {
            log(tag, msg, LogItem.logLevel.VERBOSE, LogItem.logType.QA);
        }
    }

    public static void v(String tag, String msg, Throwable t) {
        if (null != tag && null != t && null != msg) {
            log(tag, msg + " " + getStackTraceString(t), LogItem.logLevel.VERBOSE, LogItem.logType.QA);
        }
    }

    public static void v(String tag, Throwable t) {
        if (null != tag && null != t) {
            log(tag, getStackTraceString(t), LogItem.logLevel.VERBOSE, LogItem.logType.QA);
        }
    }

    /**
     * Use that method in case of needs of warning traces
     * 
     * @param tag
     * @param msg
     */
    public static void w(String tag, String msg) {
        if (null != tag && null != msg) {
            log(tag, msg, LogItem.logLevel.WARNING, LogItem.logType.QA);
        }
    }

    public static void w(String tag, String msg, Throwable t) {
        if (null != tag && null != t && null != msg) {
            log(tag, msg + " " + getStackTraceString(t), LogItem.logLevel.WARNING, LogItem.logType.QA);
        }
    }

    public static void w(String tag, Throwable t) {
        if (null != tag && null != t) {
            log(tag, getStackTraceString(t), LogItem.logLevel.WARNING, LogItem.logType.QA);
        }
    }
}

