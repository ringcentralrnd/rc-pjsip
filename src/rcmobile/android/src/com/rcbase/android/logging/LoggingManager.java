package com.rcbase.android.logging;

/**
 * Copyright (C) 2011-2012, RingCentral, Inc.
 * All Rights Reserved.
 */

import static android.util.Log.getStackTraceString;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.util.Log;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.utils.AppInfo;
import com.rcbase.android.logging.utils.LogFilesUtils;
import com.rcbase.android.logging.utils.LogcatDumper;
import com.rcbase.android.logging.utils.LogsZipper;
import com.rcbase.android.logging.utils.RCTimestamp;
import com.rcbase.android.logging.utils.RCUncaughtExceptionHandler;
import com.rcbase.android.logging.utils.SystemInfo;
import com.rcbase.android.sip.client.SIProxy;
import com.rcbase.android.sip.client.TimedSIProxyTask;
import com.rcbase.android.sip.service.IServicePJSIP;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.utils.FileUtils;

/**
 * That class is the root for the management of logging and troubleshooting
 * feature
 */
public class LoggingManager {
    /**
     * Object for synchronization
     */
    final static Object syncObject = new Object();
    private static RCUncaughtExceptionHandler myHandler = null;
    private final static String TAG = "[RC]LoggingManager";

    public final static String STR_USER = "Runtime";
    public final static String STR_CRASH = "Crash";
    private final static String STR_RUNTIME_FILE_NAME = "RuntimeLog";
    private final static String STR_KEY_TO_LAUNCH = "*74647464#";
    private final static String STR_KEY_FOR_CRASH = "*31415926#";
    private final static String STR_SEPARATOR = "\n=================================================\n";

    public final static int INT_MAX_LOGS_COUNT = 0xA; // TEN! :)
    private static LOGGING_TYPE iam = null;

    static Object gettingServiceLogSyncObject = new Object();

    static String serviceLg = null;

    /**
     * Enum to specify what kind of LoggingManager we are using: Application &
     * Service - when SIP stack lives his own life in separate process from
     * application process Monolithic - when sip & gui lives in the same thread
     */
    public static enum LOGGING_TYPE {
        SERVICE, APPLICATION, MONOLITHIC
    }

    /**
     * Reasons why the logs collection could be stopped
     */
    public enum LogsFinalizationReason {
        UNCAUGHT_EXCEPTION, USER_INITIATED, REMOTE_INITIALIZED_LOG_COLLECTION
    }

    enum SectionName {
        UNCAUGHT_EXCEPTION, APPLICATION_LOG, DEVICE_INFO, APPLICATION_INFO
    }

    public static LOGGING_TYPE getMySide() {
        return iam;
    }

    //
    public static void setMySide(LOGGING_TYPE side) {
        iam = side;
    }

    /**
     * VERY IMPORTANT! PLEASE READ CAREFULLY! Method which initializes the work
     * of Logging Engine Here we initialize the side of the
     */
    public synchronized static void initializeWork(LOGGING_TYPE setMe) {
        // Working scenario - we are initiating our internal logger
        Logger.initRuntimeLog();
        setMySide(setMe);
        if (LogSettings.QA) {
            QaLog.i(TAG, "initializeWork " + getMySide());
        }

        // Registering uncaught exceptions handler
        if (null == myHandler) {
            myHandler = new RCUncaughtExceptionHandler();

            if (null != myHandler) {
                Thread.setDefaultUncaughtExceptionHandler(myHandler);
            }
        }
    }

    /**
     * Method declared for the finalization of logs and .zip log file formation
     * 
     * @param reason
     *            - to understand the scenario of logs capturing
     * @param objects
     *            - some objects which could be useful during logs capturing
     * @return - returns string with abs path to the captured file, can be null
     *         if something went wrong
     */
    public static synchronized String finalizeLogsCollection(LogsFinalizationReason reason, Object[] objects)
    {
        String result = null;
        
        if(!isStorageAccessible() && null != reason) {
            return null;
        }
        
        String logcatFile = null;
        String latestGeneralCrashLogFile = null;

        String zipFileTitle = (iam.equals(LOGGING_TYPE.APPLICATION) ? "Application" : "Service") + "." + 
                              (reason.equals(LogsFinalizationReason.UNCAUGHT_EXCEPTION) ? STR_CRASH : STR_USER);
        
        
        String runtimeLogFile = null;

        File logsDirFile = null;
        List<String> filesToBeZipped = new ArrayList<String>();
        
        try {
            String logsPath = getThePathToTheLogsDirectory();
            // Last check before final log collect
            if (!(null != logsPath && null != (logsDirFile = new File(logsPath)) && logsDirFile.exists() && logsDirFile.isDirectory())) {
                if (LogSettings.ENGINEERING) {
                    EngLog.e(TAG, " Return, someth. really wrong :( ");
                }

                return null;
            } else {
                LogItem[] serviceLog = null; 
                if (BUILD.VOIP_ENABLED) {
                    if (RingCentralApp.getProcessType() == RingCentralApp.APPLICATION_PROCESS) {
                        serviceLog = getCurrentServiceLog();
                        //if (serviceLog == null) {
                            /**
                             * Getting the fresh SIP service log
                             */
                            String latestSrvLg = getLatestServiceLog();
                            if (null != latestSrvLg) {
                                filesToBeZipped.add(latestSrvLg);
                            }
                        //}
                    }
                }
                /** Getting current runtime log for current collector (side): 
                 *  - if it's application - will bring APPLICATION runtime log;
                 *  - if it's service - will bring SERVICE runtime log;
                 */
                runtimeLogFile = provideAppLog(logsPath, reason, objects, serviceLog);
                if (null != runtimeLogFile) {
                    filesToBeZipped.add(runtimeLogFile);
                }
                
                /** Getting latest crash log for current collector (side): 
                 *  - if it's application - will bring Application crash log;
                 *  - if it's service - will bring Service crash log;
                 */
                if( reason.equals(LoggingManager.LogsFinalizationReason.USER_INITIATED) || 
                    reason.equals(LoggingManager.LogsFinalizationReason.REMOTE_INITIALIZED_LOG_COLLECTION)) {
                    latestGeneralCrashLogFile = LogFilesUtils.getTheLatestCrashLog();
                    
                    if(null != latestGeneralCrashLogFile) { 
                        filesToBeZipped.add(latestGeneralCrashLogFile);
                    }
                }
                
                
                /** LOGCAT dump : 
                 * - if it's application initiated log collection; 
                 * - if it's uncaught exception in the service; 
                 */
                if (    (iam.equals(LOGGING_TYPE.APPLICATION) || iam.equals(LOGGING_TYPE.MONOLITHIC)) || 
                        (iam.equals(LOGGING_TYPE.SERVICE) && reason.equals(LoggingManager.LogsFinalizationReason.UNCAUGHT_EXCEPTION)) ){
                    logcatFile = LogcatDumper.dumpLogcatLogOnStorage(logsPath);
                    if(null != logcatFile) {
                        filesToBeZipped.add(logcatFile);
                    }
                }                

                result = zipLogsAndCleanUp(filesToBeZipped, logsPath, zipFileTitle);
            }
        } catch (Throwable e) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, " exception during log finalization ", e);
            }
        }
        
        return result;
    }

    /**
     * That method checks the entered value- is it magic key or not
     * 
     * @param key
     *            - string value of entered phone number
     * @return - true if it is necessary start logger activity with its logic,
     *         false if not
     */
    public static boolean itIsLoggerCase(String key) {
        return null != key && key.equals(STR_KEY_TO_LAUNCH);
    }

    /**
     * That method checks the entered value- is it magic key for test crash or
     * not
     * 
     * @param key
     *            - string value of entered phone number
     * @return - true if it is necessary start logger activity with its logic,
     *         false if not
     */
    public static boolean isItTestCase(String key) {
        return null != key && key.equals(STR_KEY_FOR_CRASH);
    }

    /**
     * Test function
     * 
     * UNIT TEST!!1
     */
    @SuppressWarnings("null")
    public static void loggingTest() {
        if (LogSettings.ENGINEERING) {
            // TODO: create start of the LoggingManager activity with displaying
            // of list of items
            // now it's just causing uncaught exception
            /*
             * Object noolik = null; noolik.toString(); // right here will be
             * uncaught exception
             */

            for (int i = 0; i < 160; i++) {
                StringBuffer additional = new StringBuffer();

                for (short j = 0; j < (i / 10 + 12); j++)
                    additional.append("a");

                EngLog.e(TAG, "add is " + additional.toString());

                if (i % 2 == 0)
                    EngLog.e("" + i, additional.toString());

                EngLog.e("" + i, additional.toString());
            }

            QaLog.e("7868768768", " 123123123123123123123");
            QaLog.e("7868768768", " 123123123123123123123");
            QaLog.e("7868768768", " JHGJHGH");
            QaLog.e("7868768768", " dfifu983034yb");
            QaLog.e("7868768768", " dfifu983034yb");

            EngLog.e("000000000000", "9879879787987098");

            Object noolik = null;
            noolik.toString(); // right here will be uncaught exception
        }
    }
    
    private static final LogItem[] getCurrentServiceLog() {
        if (BUILD.VOIP_ENABLED) {
            if (RingCentralApp.getProcessType() == RingCentralApp.APPLICATION_PROCESS) {
                try {
                    IServicePJSIP service = SIProxy.getInstance().getService();
                    if (service != null) {
                        return service.getCurrentLog(false);
                    }
                } catch (Throwable th) {
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "getCurrentServiceLog: error: " + th.toString());
                    }
                    return null;
                }
            }
        }
        return null;
    }

     private static class GetServiceLogSIProxyTask extends TimedSIProxyTask {
        public String result = null;
        public GetServiceLogSIProxyTask() {
            super("GetServiceLogSIProxyTask");
        }
        
        @Override
        public void doServiceRequest(IServicePJSIP service) {
            if (BUILD.VOIP_ENABLED) {
                try {
                    result = service.storeCurrentLog(android.os.Process.myPid());
                } catch (Throwable th) {
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "getServiceLog:storeCurrentLog: ", th);
                        Log.w(TAG, "getServiceLog:storeCurrentLog:", th);
                    }
                }
            }
        }
    }
    
    /**
     * Gets from SIP service current runtime log (using service connection)
     * 
     * @return path to captured service log
     */
    private static String getServiceLog() {
        String result = null;
        if (BUILD.VOIP_ENABLED) {
            if (null != iam && iam.equals(LOGGING_TYPE.APPLICATION)) {
                GetServiceLogSIProxyTask task = new GetServiceLogSIProxyTask();
                int state = task.execute(10000, true);
                if (state == TimedSIProxyTask.TIMEOUT) {
                    MktLog.w(TAG, "getServiceLog:TIMEOUT");
                    Log.w(TAG, "getServiceLog:TIMEOUT");
                } else if (state == TimedSIProxyTask.FAILED) {
                    MktLog.w(TAG, "getServiceLog:FAILED");
                    Log.w(TAG, "getServiceLog:FAILED");
                }
                result = task.result;
            }
        }
        return result;
    }

    /**
     * Gets latest SIP service log
     * 
     * @return latest service log
     */
    private static String getLatestServiceLog() {
        String serviceLogFile = null;
        if (BUILD.VOIP_ENABLED) {
            if (iam.equals(LOGGING_TYPE.APPLICATION)) {
                serviceLogFile = getServiceLog(); // gets current SIP service
                                                  // log,
                                                  // & also it contains latest
                                                  // SIP service CRASH LOG

                // if the trying get service log was unsuccessful - will try
                // find latest
                if (null == serviceLogFile) {
                    serviceLogFile = LogFilesUtils.getTheLatestServiceCrashLog();
                }
            } // In case if it's service or monolithic, it will search for
              // latest service crash log file
            else if (iam.equals(LOGGING_TYPE.SERVICE) || iam.equals(LOGGING_TYPE.MONOLITHIC)) {
                serviceLogFile = LogFilesUtils.getTheLatestServiceCrashLog();
            }
        }
        return serviceLogFile;
    }

    public static boolean prepareFilesForDump() {
        boolean result = false;

        return result;
    }

    /**
     * Method for getting runtime log file, including information: - System
     * device info; - Runtime log; - Uncaught exception(optional, if it was
     * captured);
     * 
     * @param logsP
     *            - path to the file, where should be log file stored
     * @param mReason
     *            - reason why log is collecting
     * @param objs
     *            - array of objects, where uncaught exception keeping
     * @return aboulute path with file
     */
    private static String provideAppLog(String logsP, LogsFinalizationReason mReason, Object[] objs, LogItem[] serviceLog) {
        String result = null;

        if (null != logsP) {
            String runtimeLogFile = logsP + "/" + STR_RUNTIME_FILE_NAME + "." + RCTimestamp.getCurrentTimestamp()
                    + (iam.equals(LOGGING_TYPE.APPLICATION) ? ".applog" : ".srvlog");
            BufferedWriter bufferedWriter = null;
            try {
                File runtimeLog = new File(runtimeLogFile);

                if (null != runtimeLog && runtimeLog.exists()) {
                    runtimeLog.delete();
                }

                runtimeLog.createNewFile();
                FileWriter fw = new FileWriter(runtimeLog.getAbsolutePath());
                bufferedWriter = new BufferedWriter(fw);

                if (null != bufferedWriter) {
                    try {
                        storeRuntimeLog(bufferedWriter, serviceLog);
                    } catch (java.lang.Throwable th1) {
                        bufferedWriter.write(STR_SEPARATOR);
                        bufferedWriter.write("ERROR storing runtimelog:" );
                        bufferedWriter.write(getStackTraceString(th1));
                        bufferedWriter.write(STR_SEPARATOR);
                    }
                    try {
                        storeSystemDeviceApplicationInfo(mReason, bufferedWriter);
                    } catch (java.lang.Throwable th2) {
                        bufferedWriter.write(STR_SEPARATOR);
                        bufferedWriter.write("ERROR storing device/application information:" );
                        bufferedWriter.write(getStackTraceString(th2));
                        bufferedWriter.write(STR_SEPARATOR);
                    }
                    if (mReason.equals(LogsFinalizationReason.UNCAUGHT_EXCEPTION) && null != objs) {
                        storeUncaughtException((Throwable) objs[0], bufferedWriter);
                    }

                    bufferedWriter.close();
                }

                result = runtimeLog.getAbsolutePath();
            } catch (Exception e) {
                if (null != bufferedWriter) {
                    try {
                        bufferedWriter.close();
                    } catch (Exception ex) {
                    }
                }
            }
        }

        return result;
    }

    /**
     * Creates .zip file with specified list of files, returns full path to the
     * file with captured logs
     * 
     * @param filesToBeZipped
     *            - list of files which will be added into .zip file
     * @param logsPathStr
     *            - path to the folder where logs are located
     * @param zipFileName
     *            - title of .zip file name, in the method will be added
     *            timestamp
     * @return full path, to the completed .zip file with logs
     */
    private static String zipLogsAndCleanUp(List<String> filesToBeZipped, String logsPathStr, String zipFileName) {
        if (null != filesToBeZipped && 0 < filesToBeZipped.size() && null != logsPathStr) {
            String ZipFile = logsPathStr + "/" + zipFileName + "." + RCTimestamp.getCurrentTimestamp() + ".zip";
            LogsZipper.zipLogs(ZipFile, filesToBeZipped);

            // And the finishing fifth step - clean the data :
            LogFilesUtils.cleanUp(filesToBeZipped); // removing the temporary
                                                    // files
            LogFilesUtils.maintainLogsDir(); // maintain logs folder

            return ZipFile;
        }

        return null;
    }

    /**
     * LogItems comparator.
     */
    private static class LogItemComparator implements Comparator<LogItem> {
        @Override
        public int compare(LogItem l, LogItem r) {

            if (l.timestamp == r.timestamp) {
                return 0;
            }
            return ((l.timestamp < r.timestamp) ? -1 : 1);
        }
    }
    
    /**
     * Stares runtime logs.
     * 
     * @param bufferedWriter
     *            stream for writing log
     * @param serviceLog
     *            service log if exists
     * @throws IOException
     *             an exception occurred
     */
    private static void storeRuntimeLog(BufferedWriter bufferedWriter, LogItem[] serviceLog) throws IOException {
        if (null != bufferedWriter) {
            if (serviceLog != null && (RingCentralApp.getProcessType() == RingCentralApp.APPLICATION_PROCESS)) {
                LogItem[] appLog = getCurrentLog(true);
                List<LogItem> log = new ArrayList<LogItem>();
                if (appLog != null) {
                    for (LogItem l : appLog) {
                        if (l != null) {
                            l.appproc = true;
                            log.add(l);
                        }
                    }
                }
                for (LogItem l : serviceLog) {
                    if (l != null) {
                        l.appproc = false;
                        log.add(l);
                    }
                }
                Collections.sort(log, new LogItemComparator());

                bufferedWriter.write(STR_SEPARATOR);
                bufferedWriter.write(">>> Application and SIP Service Log");
                bufferedWriter.write(STR_SEPARATOR);

                for (LogItem tLogItem : log) {
                    try {
                        bufferedWriter.write(tLogItem.toString2());
                    } catch (java.lang.Throwable e) {
                    }
                }

                bufferedWriter.write(STR_SEPARATOR);
                bufferedWriter.write("<<< Application and SIP Service Log");
                bufferedWriter.write(STR_SEPARATOR);
            } else {
                bufferedWriter.write(STR_SEPARATOR);
                bufferedWriter.write(SectionName.APPLICATION_LOG.toString());
                bufferedWriter.write(STR_SEPARATOR);
                Logger.writeLogIntoBufferedWriter(bufferedWriter);
            }
        }
    }

    /**
     * Writes into buffered writer caught exception
     * 
     * @param objects
     *            - exception
     * @param bufferedWriter
     *            - where to write exception information
     */
    private static void storeUncaughtException(Throwable object, BufferedWriter bufferedWriter) {
        try {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, iam + " UNCAUGHT EXCEPTION HANDLER IN WORK ");
            }

            // getting uncaught exception string output
            if (null != object) {
                bufferedWriter.write(STR_SEPARATOR);
                bufferedWriter.write(SectionName.UNCAUGHT_EXCEPTION.toString());
                bufferedWriter.write(STR_SEPARATOR);
                bufferedWriter.write(getStackTraceString(object));
            }
        } catch (Throwable th) {
        }
    }

    /**
     * Method for storing of application information & system device info
     * 
     * @param reason
     *            - reason why log is collecting
     * @param bufferedWriter
     *            - where to write information
     */
    private static void storeSystemDeviceApplicationInfo(LogsFinalizationReason reason, BufferedWriter bufferedWriter) {
        try {
            if (null != bufferedWriter) {
                bufferedWriter.write(STR_SEPARATOR);
                bufferedWriter.write(SectionName.APPLICATION_INFO.toString());
                bufferedWriter.write(STR_SEPARATOR);
                try {
                    AppInfo.writeAppInfoIntoBufferedWriter(RingCentralApp.getContextRC(), bufferedWriter);
                } catch (java.lang.Throwable th1) {
                    bufferedWriter.write("ERROR storing application iformation:");
                    bufferedWriter.write(getStackTraceString(th1));
                }

                bufferedWriter.write(STR_SEPARATOR);
                bufferedWriter.write(SectionName.DEVICE_INFO.toString());
                bufferedWriter.write(STR_SEPARATOR);
                try {
                    SystemInfo.writeSystemeInformationIntoBufferedWriter(bufferedWriter, RingCentralApp.getContextRC(), reason
                            .equals(LogsFinalizationReason.USER_INITIATED));
                } catch (java.lang.Throwable th1) {
                    bufferedWriter.write("ERROR storing system iformation:");
                    bufferedWriter.write(getStackTraceString(th1));
                }
            }
        } catch (IOException ex) {
        }
    }

    /**
     * This method returns path to the logs directory, if it does not exist it
     * will create this
     */
    private static String getThePathToTheLogsDirectory() {
        return LogFilesUtils.getThePathToTheLogsDirectory();
    }

    /**
     * That method should be used during
     */
    public static synchronized void cleanup() {
        try {
            Logger.cleanup();
        } catch (Exception e) {
        }
    }

    public static final synchronized LogItem[] getCurrentLog(boolean cleanup) {
        try {
            return Logger.getCurrentLog(cleanup);
        } catch (Exception e) {
        }
        return null;
    }
    
    
    /**
     * Use for checking sd card storage availability
     * 
     * @return true if storage is accessible, false if not
     */
    public static boolean isStorageAccessible() {
        return FileUtils.isStorageAccessible();
    }

    /**
     * @return true if it's service side
     */
    public static boolean isService() {
        return getMySide().equals(LoggingManager.LOGGING_TYPE.SERVICE);
    }
}
