package com.rcbase.android.logging;



public class NtvLog {
    static final String TAG = "[RC]Native";

    public static void postLog(int type, int level, String MSG) {
        switch (type) {
        // MKT
        case 0:
            Logger.newItem(LogItem.logType.MARKET, LogItem.logLevel.getLevel(level), TAG, MSG);
            break;
        // QA
        case 1:
            Logger.newItem(LogItem.logType.QA, LogItem.logLevel.getLevel(level), TAG, MSG);
            break;
        default:
            break;
        }
    }
}
