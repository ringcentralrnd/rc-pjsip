package com.rcbase.android.logging.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.LoggingManager;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.LoggingScreen.UnZipThread;

/**
 * Copyright (C) 2010-2011, RingCentral, Inc.
 * All Rights Reserved.
 */

/**
 * Class with static methods for zipping logs
 */
public class LogsZipper {
    final static String     TAG = "[RC]LogsZipper";
    final static boolean    DEBUG = false;

    /**
     * Zip list of specified files(_filesToBeZipped) into specified
     * file(_zipFilePath)
     * 
     * @param zipFilePath
     *            - full path to the future zip
     * @param filesToBeZipped
     *            - array of paths to the files, which should be zipped
     * @return - true if .zip file was created successfully
     */
    public static boolean zipLogs(String zipFilePath, List<String> filesToBeZipped) {        
        boolean result = false;

        if (LoggingManager.isStorageAccessible() && null != zipFilePath && null != filesToBeZipped && 0 < filesToBeZipped.size()) {
            try {
                File zipFile = new File(zipFilePath);

                // what if it's a groundhog day & time? :)
                if (zipFile.exists()) {
                    zipFile.delete();
                }

                zipFile.createNewFile();
                zip(filesToBeZipped, zipFile.getAbsolutePath());

                result = (new File(zipFilePath)).exists();
            } catch (Exception e) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "zipLogs exception: " + e.toString());
                }
            }
        }

        return result;
    }
    
    private static void zip(List<String> files, String zipFile) {
        if(null == files || null == zipFile ) {
            return;
        }
        
        ZipOutputStream out = null;
        FileOutputStream dest = null;
        BufferedInputStream origin = null;
        try {
            final int BUFFER = 2048;
            dest = new FileOutputStream(zipFile);
            out = new ZipOutputStream(new BufferedOutputStream(dest));

            byte data[] = new byte[BUFFER];

            for (int i = 0; i < files.size(); i++) {
                FileInputStream fi = new FileInputStream(files.get(i));
                origin = new BufferedInputStream(fi, BUFFER);
                ZipEntry entry = new ZipEntry(files.get(i).substring(files.get(i).lastIndexOf("/") + 1));
                out.putNextEntry(entry);

                int count;

                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }

        } catch (Exception e) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, " zip exception : ", e);
            }
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ioe) {
                }
            }
            if (origin != null) {
                try {
                    origin.close();
                } catch (IOException ioe) {
                }
            }
            if (dest != null) {
                try {
                    dest.close();
                } catch (IOException ioe) {
                }
            }
        }
    }

    public static String unzipFileAndProvideContentAsString(UnZipThread unzipThreadObj) {        
        if (null == unzipThreadObj) {
            return "Log is empty";
        }

        String fileToBeUnzipped = unzipThreadObj.file;

        File fileWithUnzippedLog = null;
        FileWriter fileStream = null;
        BufferedWriter superBW = null;
        try {
            if (null != fileToBeUnzipped && LoggingManager.isStorageAccessible()) {
                String folderToBeLocated = LogFilesUtils.getThePathToTheLogsDirectory();

                try {
                    fileWithUnzippedLog = new File(folderToBeLocated + "/" + System.currentTimeMillis() + ".dat");
                    if (null != fileWithUnzippedLog) {
                        fileStream = new FileWriter(fileWithUnzippedLog.getAbsolutePath());
                        superBW = new BufferedWriter(fileStream);
                    }
                } catch (IOException th) {
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, " buffered writer exception ", th);
                    }
                }

                if (null != folderToBeLocated && null != fileWithUnzippedLog) {
                    folderToBeLocated += "/tmp" + System.currentTimeMillis() + "/";

                    if (LogFilesUtils.checkTheFolder(folderToBeLocated)) {
                        unzip(fileToBeUnzipped, folderToBeLocated);
                        File folderWithLogs = new File(folderToBeLocated);

                        if (folderWithLogs.exists() && folderWithLogs.isDirectory()) {
                            File[] logs = folderWithLogs.listFiles();

                            if (null != logs && 0 < logs.length) {
                                FileInputStream fis = null;
                                BufferedInputStream bis = null;
                                DataInputStream dis = null;

                                try {
                                    for (int i = 0; i < logs.length && unzipThreadObj.shouldWeContinue(); i++) {

                                        if (!logs[i].getAbsolutePath().contains(".zip") && unzipThreadObj.shouldWeContinue()) {
                                            fis = new FileInputStream(logs[i]);

                                            bis = new BufferedInputStream(fis);
                                            dis = new DataInputStream(bis);
                                            String tmpStr = null;
                                            while (dis.available() != 0 && unzipThreadObj.shouldWeContinue()) {
                                                tmpStr = dis.readLine();

                                                if (null != tmpStr) {
                                                    superBW.write(tmpStr);
                                                    superBW.write("\n");
                                                    tmpStr = null;
                                                }
                                            }
                                            fis.close();
                                            bis.close();
                                            dis.close();
                                        }
                                    }

                                } catch (Throwable e) {
                                    if (LogSettings.MARKET) {
                                        MktLog.e(TAG, " exception ", e);
                                    }
                                } finally {
                                    try {
                                        if (null != fis) {
                                            fis.close();
                                        }

                                        if (null != bis) {
                                            bis.close();
                                        }

                                        if (null != dis) {
                                            dis.close();
                                        }
                                    } catch (Throwable th) {
                                        if (LogSettings.ENGINEERING && DEBUG) {
                                            EngLog.e(TAG, " exception ", th);
                                        }
                                    }
                                }
                            }
                        }

                        LogFilesUtils.smartDelete(folderWithLogs);
                    }
                }

            }
        } catch (Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, " exception ", th);
            }
        } finally {
            try {
                if (null != superBW) {
                    superBW.close();
                }
            } catch (Throwable tth) {
                EngLog.e(TAG, " exception ", tth);
            }
        }

        return (null != fileWithUnzippedLog ? fileWithUnzippedLog.getAbsolutePath() : null);
    }

    private static void unzip(String zipFile, String location) {
        if(null == zipFile || null == location) { 
            return;
        }
        
        try {
            FileInputStream fin = null;
            ZipInputStream zin = null;
            FileOutputStream fout = null;
            ZipEntry ze = null;
            try {
                fin = new FileInputStream(zipFile);
                zin = new ZipInputStream(fin);

                while ((ze = zin.getNextEntry()) != null) {
                    if (ze.isDirectory()) {
                        dirChecker(ze.getName(), location);
                    } else {
                        fout = new FileOutputStream(location + ze.getName());
                        for (int c = zin.read(); c != -1; c = zin.read()) {
                            fout.write(c);
                        }

                        zin.closeEntry();
                        fout.close();
                        fout = null;
                    }
                }

            } catch (Throwable e) {
                if(LogSettings.MARKET) {
                    MktLog.w(TAG, " decompress ", e);                    
                }
            } finally {
                try {
                    if (null != zin) {
                        zin.close();
                    }

                    if (null != fin) {
                        fin.close();
                    }

                    if (null != fout) {
                        fout.close();
                    }
                } catch (Throwable th) {
                }
            }
        } catch (Throwable th) {
            if(LogSettings.MARKET) {
                MktLog.e(TAG, " unzip ", th);                    
            }
        }
    }

    private static void dirChecker(String dir, String location) {
        if(null == dir || null == location) {
            return;
        }
        
        File f = new File(location + dir);

        if (!f.isDirectory()) {
            f.mkdirs();
        }
    }
}
