package com.rcbase.android.logging.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RingCentralApp;

/**
 * Copyright (C) 2010-2011, RingCentral, Inc.
 * All Rights Reserved.
 */
public class SystemInfo {
    private final static String TAG = "[RC]SystemInfo";

    protected static String collectMemInfo() {
		StringBuilder meminfo = new StringBuilder();
		BufferedReader bufferedReader = null;
        try {
			String[] commandLine = {"dumpsys", "meminfo", Integer.toString(android.os.Process.myPid())};			

			Process process = Runtime.getRuntime().exec(commandLine);
			bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				meminfo.append(line);
				meminfo.append("\n");
			}
            process.destroy();
		} catch (java.lang.Throwable e) {
            if(LogSettings.MARKET){
                MktLog.w(TAG, "DumpSysCollector.meminfo could not retrieve data", e);
            }
		} finally {
        	if (bufferedReader != null) {
        		try {
        			bufferedReader.close();
        		} catch (java.lang.Throwable t) {
        		}
        	}
		}

		return meminfo.toString();
	}
    
    public static void writeCPUInfo(BufferedWriter bwr) {
        if (null != bwr) {
            InputStream in = null;

            String[] args = { "/system/bin/cat", "/proc/cpuinfo" };
            ProcessBuilder cmd = new ProcessBuilder(args);
            Process process = null;
            
            try {
                process = cmd.start();
                in = process.getInputStream();
                byte[] re = new byte[1024];
                
                while (in.read(re) != -1) {
                    String tmpStr = new String(re);
                    if (null != tmpStr) {
                        // dirty hack replacement of bad symbol NUL NUL NUL NUL NUL...
                        char n = 0;
                        bwr.newLine();
                        bwr.write(tmpStr.replaceAll(String.valueOf(n), ""));
                        bwr.newLine();
                    }
                    
                }
            } catch (Throwable e) {
                if(LogSettings.ENGINEERING) {
                    EngLog.e(TAG, " CPU INFO ", e);
                }
            } finally {
                try {
                    in.close();
                } catch (Throwable r) {
                }
            }
        }
    }
    
    public static void writeSystemeInformationIntoBufferedWriter(BufferedWriter bwr, Context context, boolean shouldWeSaveMeminfo) {

        if(null != bwr) {
            try {
                boolean isItOldVersionAPI = false;

                int version = Integer.valueOf(Build.VERSION.SDK);

                if( (isItOldVersionAPI = (version < 5))) {
                    bwr.newLine();
                    bwr.write("Android version 1.5 or lower device "+ " " + Build.VERSION.RELEASE + "(" + Build.VERSION.INCREMENTAL + ")" + " SDK " + version);
                    bwr.newLine();
                    
                } else {
                    bwr.write(SystemInfoCollector.getDeviceInfo());
                }
                bwr.newLine();
                bwr.write("#CPU info:");
                writeCPUInfo(bwr);

                Context tmpContext = RingCentralApp.getContextRC();

                if(null != context) {
                    ConnectivityManager connMgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

                    if(null != connMgr) {
                        bwr.newLine();
                        bwr.write("#Network info:");
                        bwr.newLine();
                        android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                        if(null != wifi) {
                            bwr.write(wifi.toString());
                            bwr.newLine();
                        }

                        android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                        if(null != mobile) {
                            bwr.write(mobile.toString());
                            bwr.newLine();
                        }
                    }

                    ActivityManager actvityManager = (ActivityManager)context.getSystemService(Activity.ACTIVITY_SERVICE);

                    if(null != actvityManager) {
                        List<ActivityManager.RunningAppProcessInfo> procInfos = actvityManager.getRunningAppProcesses();
                        bwr.newLine();
                        bwr.write("#Running services");
                        bwr.newLine();
                        for(int i = 0; i < procInfos.size(); i++) {
                            bwr.write(procInfos.get(i).processName);
                            bwr.newLine();
                        }
                    }



                    if(shouldWeSaveMeminfo) {
                        String memInfo = collectMemInfo();

                        if(null != memInfo) {
                            bwr.newLine();
                            bwr.write("#Meminfo :\n");
                            bwr.write(memInfo);
                            bwr.newLine();
                        }
                    }

                    if(!isItOldVersionAPI) {
                        String features = SystemInfoCollector.getFeatures(tmpContext);

                        if(null != features){
                            bwr.newLine();
                            bwr.write("#Features :\n");
                            bwr.write(features);
                            bwr.newLine();
                        }
                    }
                }
            } catch (Exception e) {}
        }
   }
}
