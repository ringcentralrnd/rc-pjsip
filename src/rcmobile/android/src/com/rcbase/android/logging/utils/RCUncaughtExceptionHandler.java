package com.rcbase.android.logging.utils;

/**
 * Copyright (C) 2010-2012, RingCentral, Inc.
 * All Rights Reserved.
 */

import com.rcbase.android.logging.LoggingManager;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;

/**
 * That class is necessary for the handling of uncaught exceptions in application
 */
public class RCUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
    private final static String                 TAG = "[RC]RCUncaughtExceptionHandler";
    private Thread.UncaughtExceptionHandler     myUncaughtExecptionHandler;
    

    public RCUncaughtExceptionHandler() {
        try {
            myUncaughtExecptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        } catch (Throwable E){
            if(LogSettings.MARKET) {
                MktLog.w(TAG, " constructor: " + E.toString());
            }
        }
    }

    /**
     * That method is our handler of uncaught exceptions
     * @param t
     * @param e
     */
    public void uncaughtException(Thread t, Throwable e) {
        if (LogSettings.MARKET) {
            MktLog.e(TAG, " " + LoggingManager.getMySide() + " uncaught exception : ", e);
        }        
        
        try {
            LoggingManager.finalizeLogsCollection(LoggingManager.LogsFinalizationReason.UNCAUGHT_EXCEPTION, new Object[] { e });

            // throw forward to android system
            if(null != myUncaughtExecptionHandler) {
                myUncaughtExecptionHandler.uncaughtException(t, e);
            }
        } catch (java.lang.Throwable th) {}
    }
}
