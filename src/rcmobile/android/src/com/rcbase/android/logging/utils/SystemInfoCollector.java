package com.rcbase.android.logging.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.utils.ReflectionHelper;

/**
 * Copyright (C) 2010-2011, RingCentral, Inc.
 * All Rights Reserved.
 */
public class SystemInfoCollector {
    private final static String TAG ="[RC]SystemInfoCollector";
    private static final String separator = "\n";

    private static int getAPILevel() {
        int apiLevel;
        apiLevel = Integer.parseInt(Build.VERSION.SDK);

        return apiLevel;
    }

    static String getFeatures(Context ctx) {
        String _result = "Data available only with API Level > 5";
        if (getAPILevel() >= 5) {
            StringBuilder result = new StringBuilder();
            PackageManager pm = ctx.getPackageManager();
            try {
            	Method getSystemAvailableFeatures = ReflectionHelper.getMethod(PackageManager.class, "getSystemAvailableFeatures");
                Object[] features = (Object[])getSystemAvailableFeatures.invoke(pm);
                for(Object feature : features) {
                    String featureName = (String)feature.getClass().getField("name").get(feature);
                    if(featureName != null) {
                        result.append(featureName);
                    } else {
                        Method getGlEsVersion = ReflectionHelper.getMethod(feature.getClass(), "getGlEsVersion");
                        String glEsVersion = (String)getGlEsVersion.invoke(feature);
                        result.append("glEsVersion = ");
                        result.append(glEsVersion);
                    }
                    result.append("\n");
                }
            } catch (Throwable e) {
                if(LogSettings.MARKET) {
                    Log.w(TAG, "Error : " + e.toString());
                }
                result.append("Could not retrieve data: ");
                result.append(e.getMessage());
            }

            return result.toString();
        } else {
            return _result;
        }
    }

    public static String getManufacturerName() {
        String result = "Version 1.5 or lower";

        if (Integer.valueOf(Build.VERSION.SDK) > 3) {
            try {
                Class<?> execClass;
                execClass = Class.forName("android.os.Build");
                if (null != execClass) {
                    Field manufacturerF = execClass.getField("MANUFACTURER");

                    if (null != manufacturerF) {
                        String manuf = null;
                        manuf = (String) manufacturerF.get(null);

                        if (null != manuf) {
                            result = manuf;
                        }
                    }
                }
            } catch (Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, " getManufacturerName ", th);
                }
            }
        }
        if (LogSettings.ENGINEERING) {
            EngLog.i(TAG, "getManufacturerName() result " + result);
        }

        return result;
    }
    
    static String getDeviceInfo() {
        StringBuilder systemInfo = new StringBuilder();
        systemInfo.append(separator);
        systemInfo.append("#Model: " + Build.MODEL);
        systemInfo.append(separator);

        systemInfo.append(separator);
        systemInfo.append("#Brand: " + Build.BRAND);
        systemInfo.append(separator);

        systemInfo.append(separator);
        systemInfo.append("#Version: " + Build.VERSION.RELEASE+ "(" + Build.VERSION.INCREMENTAL + ")" + "; SDK: " + Build.VERSION.SDK);
        systemInfo.append(separator);

        systemInfo.append(separator);
        systemInfo.append("#Manufacturer: " + getManufacturerName());
        systemInfo.append(separator);

        return systemInfo.toString();
    }

}
