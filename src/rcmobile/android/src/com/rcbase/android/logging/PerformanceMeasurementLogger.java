package com.rcbase.android.logging;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import android.os.SystemClock;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.utils.LogFilesUtils;

public class PerformanceMeasurementLogger {
	private static final String TAG = "[RC]PerfLogger";
	public static final String CALLLOG_NET_DOWNLOAD = "auto-refresh call log immediately, load min 25 new call log records";
    public static final String CALL_LOG_SWITCH = "auto-load Call Log tab (All), load 100 existing records from call log";
    public static final String CONTACTS_LOG_SWITCH = "Contacts Switch";
    public static final String MSG_SWITCH = "auto-load messages tab, load 100 existing records from messages";
    public static final String VOICEMAIL_OPEN = "tap on voicemail record to message playback start";
    public static final String CALL_DETAILS_OPENING = "tap on a call log record to call info screen";
    public static final String CALL_FROM_CALLLOG = "tap phone number to call status screen";
    public static final String DIALER_OPEN = "tap to dialer from foregrond app";
	public static final String DIALER_KEYTONE = "each keystroke to key tone";
    public static final String DIALER_CALL = "tap call button, transition to call status screen";
    public static final String CALL_INITIZED = "call status screen to ringing tone";
    public static final String INCOMING_VOIP_RINGTONE = "In call to ringtone";
    public static final String INCOMING_TO_INVOIP_ACTIVITY = "In call to in VoIP screen";
    public static final String INCOMING_ANSWER = "Accept to connect";
    public static final String BACK_TO_ACTIVITY = "Reject - back to activity";
    public static final String HOLD = "Hold button response";
    public static final String MUTE = "Mute button response";
    public static final String CALLS_SWITCHING = "Switching between two calls";
    public static final String DIALPAD_OPENING = "Dialpad opening";
    public static final String DIALPAD_DTMF = "Dialpad keypress to DTMF tone";
    public static final String REFRESH_VOICEMAL = "refresh messages log immediately, load min 10 new messages/faxes";
    public static final String FAX_OPEN = "tap on fax message to fax info screen";
    public static final String CALL_FROM_CONTACTS = "Call from company contacts";
    public static final String OPEN_CONTACT_DETAILS = "Open Contact Details";
    public static final String SETTINGS_OPENING = "Settings Activity Opening";
    
	private Hashtable<String, Vector<Long>> mResults;
	private Hashtable<String, Timing> mTimings;
    
	private static PerformanceMeasurementLogger sInstance = null;
        
    public static synchronized PerformanceMeasurementLogger getInstance() {
        if (sInstance == null) {
            sInstance = new PerformanceMeasurementLogger();
        }
        return sInstance;
    }
	
    private PerformanceMeasurementLogger() {
		if (!BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
			MktLog.e(TAG, "Performance measurement is switched off");
		} else {
			mResults = new Hashtable<String, Vector<Long>>();
			mTimings = new Hashtable<String, Timing>();
		}
    }

	public void start(String testName){
		if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
			Timing timing = mTimings.get(testName);
			if (timing == null){
				timing = new Timing(testName);
				mTimings.put(testName, timing);
				timing.start();
			} else {
				timing.start();
			}
		}
	}
	
   public void start(String testName, long start){
        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
            Timing timing = mTimings.get(testName);
            if (timing == null){
                timing = new Timing(testName);
                mTimings.put(testName, timing);
                timing.start(start);
            } else {
                timing.start(start);
            }
        }
    }
	
	public void stop(String testName){
        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
            Timing timing = mTimings.get(testName);
            if (timing != null) {
                if (timing.stop()) {
                    moveToResults(timing);
                }
            }
        }
	}
	
    public void stop(String testName, long time) {
        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
            Timing timing = mTimings.get(testName);
            if (timing != null) {
                if (timing.stop(time)) {
                    moveToResults(timing);
                }
            }
        }
    }
	
	private void moveToResults(Timing timing) {
		mTimings.remove(timing.getType());
		Vector<Long> results = mResults.get(timing.getType());
		if (results == null){
			results = new Vector<Long>();
			addResult(timing, results);
			mResults.put(timing.getType(), results);
		} else {
			addResult(timing, results);
		}
	}

	private void addResult(Timing timing, Vector<Long> results) {
		Long duration = timing.getDuration();
		if (duration != null){
			results.addElement(duration);
		}
	}

	public void printResults(){
		if (!BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
			return;
		}
		try {
			LogFilesUtils.addToPerformanceReport(mResults);
		} catch (IOException e) {
			MktLog.w(TAG, "Can not save data to file. Ex:"+e);
		}
		mResults.clear();
	}
}

class Timing {
	private String mType = "Unknown";
	private long mStart = 0L;
	private long mEnd = 0L;
	
	public Timing(String type){
		mType = type;
	}
	
	public String getType(){
		return mType;
	}
	
	public void start(){
	    start(SystemClock.elapsedRealtime());
    }

    public void start(long time) {
        if (mEnd == 0L) {
            mStart = time;
        }
    }
    
    public boolean stop(long time){
		if (mStart > 0){
			mEnd = time;
			return true;
		}
		return false;	    
	}
	public boolean stop(){
	    return stop(SystemClock.elapsedRealtime());
	}
	
	public Long getDuration() {
		if (mStart > 0 && mEnd > 0) {
		    if (mEnd < mStart) {
		        return 0L;
		    } else {
		        return new Long(mEnd - mStart);
		    }
		} else {
			return null;
		}
	}
}
