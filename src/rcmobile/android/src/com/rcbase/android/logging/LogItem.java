/**
 * Copyright (C) 2010-2012, RingCentral, Inc.
 * All Rights Reserved.
 */
package com.rcbase.android.logging;

import java.text.SimpleDateFormat;
import java.util.Date;
import android.os.Parcel;
import android.os.Parcelable;
/**
 * That class represents log entry
 */
public class LogItem  implements Parcelable {
    public String 			msg = null;
	public String			tag = null;
	public long				timestamp;
	public boolean          appproc;
	public static final String	separator = ":";
	public static final String	separator_space = " ";
    public static final String  TAG = "[RC]LogItem";

    public static short     additionalConst = 2 + 2 + 2 + 8;    // memory costs

    private final static SimpleDateFormat formatter = new SimpleDateFormat("MM-dd HH:mm:ss.SSS");

	public logLevel			level;
	public logType			type;
	
	/**
	 * Keeps size of the log item in bytes.
	 */
	public short			size;

    
    /**
     * Parcelable creator.
     */
    public static final Parcelable.Creator<LogItem> CREATOR = new Parcelable.Creator<LogItem>() {
        public LogItem createFromParcel(Parcel in) {
            return new LogItem(in);
        }
        public LogItem[] newArray(int size) {
            return new LogItem[size];
        }
    };

    public LogItem(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }
    
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(msg);
        dest.writeString(tag);
        dest.writeLong(timestamp);
        dest.writeInt((appproc ? 1 : 0));
        dest.writeInt(level.i);
        dest.writeInt(type.i);
        dest.writeInt(size);
        
    }

    public void readFromParcel(Parcel in) {
        msg = in.readString();
        tag = in.readString();
        timestamp = in.readLong();
        appproc = (0 == in.readInt() ? false : true);
        level = logLevel.getLevel(in.readInt());
        type = logType.getType(in.readInt());
        size = (short) in.readInt();
    }
    
	public static final class logType {
        public static final logType ENGINEERING = new logType("ENG", 0x00);
        public static final logType QA = new logType("QA ", 0x01);
        public static final logType MARKET = new logType("MKT", 0x02);
        
	    public String tag;
        public int    i;
                
        protected logType(String name, int code) {
            tag = name;
            i = code;
        }
        
        public String toString() {
            return tag;
        }
        
        @Override
        public boolean equals(Object obj) {
            try {
                logType l = (logType) obj;
                if (i == l.i) {
                    return true;
                }
                    
            } catch (java.lang.Throwable th) {
                
            }
            return false;
        }
        
        public static logType getType(int id) {
            switch (id) {
            case 0:
                return ENGINEERING;
            case 1:
                return QA;
            case 2:
                return MARKET;
            default:
                return ENGINEERING;
            }
        }
	}
	
	public static class logLevel {
	    public static final logLevel INFORMATION = new logLevel("I", 0x00);
        public static final logLevel VERBOSE = new logLevel("V", 0x01);
        public static final logLevel DEBUG = new logLevel("D", 0x02);
        public static final logLevel WARNING = new logLevel("W", 0x03);
        public static final logLevel ERROR = new logLevel("E", 0x04);
        
	    public String tag;
        public final int i; 
        
        protected logLevel(String n, int id) {
            i = id;
            tag = n;
        }
        
        public String toString() {
            return tag;
        }
	    
        public static logLevel getLevel(logLevel logL) {
            if(null != logL) {
                if(INFORMATION.equals(logL))
                    return INFORMATION;
                
                if(VERBOSE.equals(logL))
                    return VERBOSE;
                
                if(DEBUG.equals(logL))
                    return DEBUG;
                
                if(WARNING.equals(logL))
                    return WARNING;
                
                if(ERROR.equals(logL))
                    return ERROR;
            }
            
            return INFORMATION;
        }
        
	    public static logLevel getLevel(int id) {
            switch (id) {
            case 0:
                return INFORMATION;
            case 1:
                return VERBOSE;
            case 2:
                return DEBUG;
            case 3:
                return WARNING;
            case 4:
                return ERROR;
            default:
                return INFORMATION;
            }
        }

	}
	

	/**
	 * @param itag   - tag
	 * @param msg   - msg to b saved
	 * @param level - from d to w
	 * @param type  - market, qa, engineering
	 */
	public LogItem(String itag, String imsg, logLevel ilevel, logType itype) {
		msg = imsg == null ? "" : imsg;
		tag = itag == null ? "" : itag;

		size = (short)(msg.getBytes().length + itag.getBytes().length + additionalConst);

		level = ilevel;
		type  = itype;

		timestamp = System.currentTimeMillis();
	}

	@Override
    /**
     * Overloaded method, converts logItem into the string
     * with \n in the end.
     */
	public String toString() {
		return new StringBuilder()
			.append(formatter.format(new Date(this.timestamp)))
	        .append(separator_space)
	        .append(type)
	        .append(separator_space)
	        .append(level)
	        .append(separator_space)
			.append(tag)
			.append(separator)
			.append(msg)
	        .append(separator_space)
	        .append("\n")
			.toString();
	}
	
	   public String toString2() {
	        return new StringBuilder()
	            .append(formatter.format(new Date(this.timestamp)))
	            .append(separator_space)
                .append(appproc ? "APP" : "SIP")
	            .append(separator_space)
	            .append(level)
                .append(separator_space)
                .append(tag)
	            .append(separator)
	            .append(msg)
	            .append(separator_space)
	            .append("\n")
	            .toString();
	    }
}
