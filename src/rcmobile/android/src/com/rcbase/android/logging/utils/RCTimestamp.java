package com.rcbase.android.logging.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Copyright (C) 2010-2011, RingCentral, Inc.
 * All Rights Reserved.
 */

/**
 * That class is necessary for the getting of timestamp
 */
public class RCTimestamp {
    /**
     * format string, default : dd.MM.yyyy.hh.mm.ss.SSS
      */
    private final static String TIMESTAMP_FORMAT = "MM-dd-yy.HH.mm.ss.SSS";

    /**
     * Get current time stamp in dd.MM.yyyy.hh.mm.ss.SSS format to change format
     * access to the format string
     * 
     * @return - timestamp as string
     */
    public static String getCurrentTimestamp() {        
        return new SimpleDateFormat(TIMESTAMP_FORMAT).format(new Date(System.currentTimeMillis()));
    }
}
