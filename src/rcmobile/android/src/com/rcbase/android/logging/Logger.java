package com.rcbase.android.logging;

/**
 * Copyright (C) 2010-2011, RingCentral, Inc.
 * All Rights Reserved.
 */
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Vector;

import android.util.Log;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.LogItem.logLevel;
import com.rcbase.android.logging.LogItem.logType;
import com.ringcentral.android.LogSettings;

/**
 * That class is necessary for
 *
 */
class Logger {
    /**
     * Defines the maximum size of log in the bytes
     */
    private static final long        MAX_SIZE_OF_RUNTIME_LOG_IN_BYTES = 256 * 1024;
    
    /**
     * Defines the minimal size of free space in log to stop clean-up (in the bytes)
     */
    private static final long        THRESHOLD_FOR_MIMIMAL_SPACE_OF_RUNTIME_LOG_IN_BYTES = 2 * MAX_SIZE_OF_RUNTIME_LOG_IN_BYTES/3;

    /**
    * Current used space, counter
    */
    private static long              current_size_of_runtime_log = 0;

    /**
     * Here are locating all log traces, this is some kind of cycle buffer
     */
    private static Vector<LogItem>          runtimeLog;

    private final static String             TAG = "[RC]Logger";
    
    /**
     * Sync primitive.
     */
    private static final Object sLock = new Object();

    /*******************************************************************************************************************
    *                                               IMPLEMENTATION
     ******************************************************************************************************************/

    /**
     *  That method is necessary for log initiating of runtime log
     */
    public static void initRuntimeLog() {
        synchronized (sLock) {
            if (null == runtimeLog) {
                runtimeLog = new Vector<LogItem>();
                current_size_of_runtime_log = 0;
            }
        }
    }

    /**
     * This is the one method which should be used in case of adding new log item
     * @param lType - type of log : {market || qa || engineering}
     * @param lLevel - v to e
     * @param tag - tag for log item
     * @param msg - msg for log item
     */
    public static void      newItem(LogItem.logType lType, LogItem.logLevel lLevel, String tag, String msg) {
        if(null != lType && null != lLevel && null != msg && null != tag) {
            if (LogSettings.QA || LogSettings.ENGINEERING || BUILD.TRACES_TO_LOGCAT_ENABLED) {
                if (lLevel.equals(logLevel.VERBOSE)) {
                    Log.v(tag, msg);
                } else if (lLevel.equals(logLevel.INFORMATION)) {
                    Log.i(tag, msg);
                } else if (lLevel.equals(logLevel.DEBUG)) {
                    Log.d(tag, msg);
                } else if (lLevel.equals(logLevel.WARNING)) {
                    Log.w(tag, msg);
                } else if (lLevel.equals(logLevel.ERROR)) {
                    Log.e(tag, msg);
                }
            }

            LogItem logItem = new LogItem(tag, msg, lLevel, lType);
            synchronized (sLock) {
                try {
                    receiveNewItem(logItem);
                } catch (java.lang.Throwable th) {
                    /**
                     * Double consistency
                     */
                    runtimeLog = new Vector<LogItem>();
                    LogItem _it = new LogItem(TAG, "!!! ATTENTION: THERE IS SOMETHING WRONG WITH LOGGER : " + th.toString(), logLevel.ERROR,
                            LogItem.logType.MARKET);
                    runtimeLog.add(_it);
                    current_size_of_runtime_log += _it.size;
                    return;
                }
            }
        }
    }

    /**
     * That method is necessary for the resetting of logs collection
     */
    static void cleanup() {
        synchronized (sLock) {
            if (null != runtimeLog) {
                runtimeLog.removeAllElements();
            }
            current_size_of_runtime_log = 0;
        }
    }

    static LogItem[] getCurrentLog(boolean cleanup) {
        synchronized (sLock) {
            LogItem[] ret = null;
            if (null != runtimeLog) {
                ret = new LogItem[runtimeLog.size()];
                ret = runtimeLog.toArray(ret);
                if (cleanup) {
                    runtimeLog.removeAllElements();
                    current_size_of_runtime_log = 0;
                } 
            } else {
                current_size_of_runtime_log = 0;
            }
            return ret;
        }
    }
    
    private static final long MIN_IN_MS = 1000 * 60;
    private static final long HOUR_IN_MS = MIN_IN_MS * 60;
    private static final long DAY_IN_MS = HOUR_IN_MS * 24;
    
    /**
     * Cleans-up log
     */
    private static void cleanUpLog() {
        int prevSize = 0;
        Vector<LogItem> prev = runtimeLog;
        prevSize = prev.size();
        runtimeLog = new Vector<LogItem>();
        current_size_of_runtime_log = 0;
        long time = System.currentTimeMillis();
        long ONE_DAY_AGO = time - DAY_IN_MS;
        long SIX_HOURS_AGO = time - 6 * HOUR_IN_MS;
        long TWO_HOURS_AGO = time - 2 * HOUR_IN_MS;
        long TEN_MINUTES_AGO = time - 10 * MIN_IN_MS;
        long THIRTY_MINUTES_AGO = time - 30 * MIN_IN_MS;
        
        /**
         * Light clean-up
         */
        for (LogItem item : prev) {
            if (item.type.i == logType.MARKET.i) {
                boolean add = false;
                int level = item.level.i;
                if (level == logLevel.ERROR.i) {
                    if (item.timestamp > ONE_DAY_AGO) {
                        add = true;
                    }
                } else if (level == logLevel.WARNING.i) {
                    if (item.timestamp > SIX_HOURS_AGO) {
                        add = true;
                    }
                } else if (level == logLevel.INFORMATION.i) {
                    if (item.timestamp > TWO_HOURS_AGO) {
                        add = true;
                    }
                } else if (level == logLevel.DEBUG.i) {
                    if (item.timestamp > TEN_MINUTES_AGO) {
                        add = true;
                    }
                } 
                
                if (add) {
                    runtimeLog.add(item);
                    current_size_of_runtime_log += item.size;
                }
            }
        }
        
        if (runtimeLog.isEmpty()) {
            current_size_of_runtime_log = 0;
        }
        
        if (current_size_of_runtime_log < THRESHOLD_FOR_MIMIMAL_SPACE_OF_RUNTIME_LOG_IN_BYTES) {
            LogItem logItem = new LogItem(TAG, "LOG CLEAN-UP (LIGHT) " + prevSize + "/" + runtimeLog.size() + " s=" + current_size_of_runtime_log, logLevel.INFORMATION, LogItem.logType.MARKET);
            runtimeLog.add(logItem);
            current_size_of_runtime_log += logItem.size;
            return;
        }
        
        /**
         * Hard clean-up
         */
        prev = runtimeLog;
        prevSize = prev.size();
        runtimeLog = new Vector<LogItem>();
        current_size_of_runtime_log = 0;
        
        for (LogItem item : prev) {
            if (item.type.i == logType.MARKET.i) {
                boolean add = false;
                int level = item.level.i;
                if (level == logLevel.ERROR.i) {
                    if (item.timestamp > SIX_HOURS_AGO) {
                        add = true;
                    }
                } else if (level == logLevel.WARNING.i) {
                    if (item.timestamp > TWO_HOURS_AGO) {
                        add = true;
                    }
                } else if (level == logLevel.INFORMATION.i) {
                    if (item.timestamp > THIRTY_MINUTES_AGO) {
                        add = true;
                    }
                } 
                
                if (add) {
                    runtimeLog.add(item);
                    current_size_of_runtime_log += item.size;
                }
            }
        }
        
        if (runtimeLog.isEmpty()) {
            current_size_of_runtime_log = 0;
        }
        
        if (current_size_of_runtime_log < THRESHOLD_FOR_MIMIMAL_SPACE_OF_RUNTIME_LOG_IN_BYTES) {
            LogItem logItem = new LogItem(TAG, "LOG CLEAN-UP (HARD) " + prevSize + "/" + runtimeLog.size() + " s=" + current_size_of_runtime_log, logLevel.INFORMATION, LogItem.logType.MARKET);
            runtimeLog.add(logItem);
            current_size_of_runtime_log += logItem.size;
            return;
        }
        
        /**
         * Aggressive clean-up (divide)
         */
        int th = runtimeLog.size()/2;
        int i = 0;
        prev = runtimeLog;
        runtimeLog = new Vector<LogItem>();
        current_size_of_runtime_log = 0;
        for (LogItem item : prev) {
            if (i++ < th) {
                continue;
            }
            runtimeLog.add(item);
            current_size_of_runtime_log += item.size;
        }
        
        if (runtimeLog.isEmpty()) {
            current_size_of_runtime_log = 0;
        }
        
        if (current_size_of_runtime_log < THRESHOLD_FOR_MIMIMAL_SPACE_OF_RUNTIME_LOG_IN_BYTES) {
            LogItem logItem = new LogItem(TAG, "LOG CLEAN-UP (AGGRESSIVE) " + prevSize + "/" + runtimeLog.size() + " s=" + current_size_of_runtime_log, logLevel.INFORMATION, LogItem.logType.MARKET);
            runtimeLog.add(logItem);
            current_size_of_runtime_log += logItem.size;
            return;
        }
        
        /**
         * FULL clean-up
         */
        LogItem logItem = new LogItem(TAG, "LOG CLEAN-UP (FULL) s=" + current_size_of_runtime_log, logLevel.INFORMATION, LogItem.logType.MARKET);
        runtimeLog = new Vector<LogItem>();
        current_size_of_runtime_log = 0;
        
        runtimeLog.add(logItem);
        current_size_of_runtime_log += logItem.size;
    }
    
    /**
     * That method for placement of new log item - if it's not null item, there is possibility store that item:
     * - its not bigger than size of runtime log
     * - there is enough space for log items of such size
     *
     *
     * @param logItem - log item which should be placed in the collection
     */
    private static void receiveNewItem(LogItem  logItem) {
        if (null != logItem && null != runtimeLog && isItCompatibleSizeOfIncomingItem(logItem)) {
            if (!((current_size_of_runtime_log + (long)logItem.size) <= MAX_SIZE_OF_RUNTIME_LOG_IN_BYTES)) {
                cleanUpLog();
            }
             
            runtimeLog.add(logItem);
            current_size_of_runtime_log += logItem.size;
        }
    }

    public static void writeLogIntoBufferedWriter(BufferedWriter bufferedWriter) {
        if (null == bufferedWriter) {
            return;
        }
        Vector<LogItem> tempVect = null;
        synchronized (sLock) {
            if (null != runtimeLog) {
                tempVect = runtimeLog;
                runtimeLog = new Vector<LogItem>();
                current_size_of_runtime_log = 0;
            } else {
                return;
            }
        }

        for (LogItem tLogItem : tempVect) {
            try {
                bufferedWriter.write(tLogItem.toString());
            } catch (IOException e) {
            }
        }
    }
    
    

    /**
     * That method prints to the console current runtime log
     */
//    static void logOutputInLog() {
//        if(LogSettings.ENGINEERING & DEBUG) {
//            if(null != runtimeLog) {
//                int _size = runtimeLog.size();
//
//                Log.e(TAG," logOutputInLog: SIZE IS " + _size + " USED " + current_size_of_runtime_log);
//
//                for(int i = (_size - 1) ; 0 <= i; i-- ) {
//                    Log.e(TAG, "logOutputInLog: " + i + " " + runtimeLog.get(i).toString());
//                }
//            }
//        }
//    }

    /**
     * That method necessary for the checking the size of incoming entry, it could be bigger than log size
     * @param itemToBeSaved
     * @return - true if item is not bigger than , false if it is null or
     */
    private static boolean isItCompatibleSizeOfIncomingItem(LogItem itemToBeSaved) {
        return (null != itemToBeSaved ? MAX_SIZE_OF_RUNTIME_LOG_IN_BYTES >= itemToBeSaved.size : false);
    }
}
