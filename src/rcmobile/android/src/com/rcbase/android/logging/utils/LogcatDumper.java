package com.rcbase.android.logging.utils;

import java.io.File;

import android.util.Log;

import com.rcbase.android.logging.LoggingManager;
import com.ringcentral.android.LogSettings;

/**
 * Copyright (C) 2010-2011, RingCentral, Inc.
 * All Rights Reserved.
 */
public class LogcatDumper
{
    /**
     * This is dump command for the logcat saving in the end should be specified the file where dump file should be located
     */
    private final static String         dumpCommand = "logcat -d -v time -f"; // IA: PP-2511, added key -v time
    private final static String         TAG = "[RC]LogcatDumper";

    /**
     * That method stores logcat dump on sd card storage
     * @return - full path to dump file
     */
    public static String dumpLogcatLogOnStorage(String directoryPathForFileToBeStored) {
        String pathOfStoredFile = null;

        if (null != directoryPathForFileToBeStored) {
            try {
                if (LoggingManager.isStorageAccessible()) {
                    File dir = new File(directoryPathForFileToBeStored);

                    if (null != dir && dir.exists() && dir.isDirectory()) {
                        // here we are creating the descriptor of logcat dump
                        // file
                        File logcatDumpFile = new File(directoryPathForFileToBeStored + "/logcatdump." + RCTimestamp.getCurrentTimestamp() + ".logcat");

                        // if it allready exists => we should delete this
                        if (logcatDumpFile.exists())
                            logcatDumpFile.delete();

                        logcatDumpFile.createNewFile();

                        String cmd = dumpCommand + logcatDumpFile.getAbsolutePath();
                        Runtime myRuntime = Runtime.getRuntime();

                        if (null != myRuntime) {
                            // latest check before run
                            if (LoggingManager.isStorageAccessible()) {
                                Process dumpProcess = myRuntime.exec(cmd);
                                if (null != dumpProcess) {
                                    dumpProcess.waitFor();
                                    dumpProcess.destroy();
                                    dumpProcess = null;
                                    pathOfStoredFile = logcatDumpFile.getAbsolutePath();
                                }
                            }
                        }
                    }
                }

            } catch (Exception e) {
                if (LogSettings.MARKET) {
                    Log.e(TAG, "dumpLogcatLogOnStorage: " + e.toString());
                }
            }
        }

        return pathOfStoredFile;
    }
}
