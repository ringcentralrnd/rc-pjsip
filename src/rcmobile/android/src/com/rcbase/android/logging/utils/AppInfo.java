/**
 * Copyright (C) 2011-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.logging.utils;

import java.io.BufferedWriter;
import java.io.IOException;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.TextUtils;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.sip.client.SIProxy;
import com.rcbase.android.sip.service.CodecInfo;
import com.rcbase.android.sip.service.IServicePJSIP;
import com.rcbase.android.sip.service.RCCallInfo;
import com.rcbase.android.sip.service.ServiceState;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.provider.RCMDataStore;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.provider.RCMDataStore.AccountInfoTable;
import com.ringcentral.android.provider.RCMDataStore.MailboxCurrentTable;
import com.ringcentral.android.utils.DeviceUtils;
import com.ringcentral.android.utils.DndManager;
import com.ringcentral.android.utils.NetworkUtils;
import com.ringcentral.android.utils.PhoneUtils;
import com.ringcentral.android.utils.UserInfo;

public final class AppInfo {
	private static final String TAG = "[RC]AppInfo";

    public static void writeAppInfoIntoBufferedWriter(Context context, BufferedWriter bwr) {
        if (bwr == null) {
            return;
        }
        
        try {
            if (RingCentralApp.numberOfSIPServiceKillsAttempts() > 0) {
                bwr.newLine();
                bwr.write(" SIP Kills (attempts) : " + RingCentralApp.numberOfSIPServiceKills() + "("
                        + RingCentralApp.numberOfSIPServiceKillsAttempts() + ')');
                bwr.newLine();
            }
            
            
            bwr.newLine();
            bwr.write("App Version: " + getBuildVersion(context, true));

            bwr.newLine();
            bwr.write("Brand: " + context.getString(R.string.app_name) + " (" + BUILD.BRAND + ")");
            
            bwr.newLine();
            bwr.write("DB Version: " + RCMDataStore.DB_VERSION);

            bwr.newLine();
            bwr.newLine();
            bwr.write("Device IMSI: " + (TextUtils.isEmpty(RCMProviderHelper.getDeviceIMSI(context)) ? "NOT PRESENT" : "PRESENT"));
            
            bwr.newLine();
            bwr.write("Device Phone Number: " + DeviceUtils.getDeviceNumber(context));
            
            bwr.newLine();
            bwr.newLine();
            bwr.write("#Login Info");

            bwr.newLine();
            bwr.write("Login Number: " + RCMProviderHelper.getLoginNumber(context));
            
            bwr.newLine();
            bwr.write("Login Ext.: " + RCMProviderHelper.getLoginExt(context));
            
            bwr.newLine();
            bwr.write("Login IP Address: " + RCMProviderHelper.getLoginIPAddress(context));
            
            bwr.newLine();
            bwr.write("Login Request ID: " + RCMProviderHelper.getLoginRequestID(context));
            
            bwr.newLine();
            bwr.write("User ID: " + RCMProviderHelper.getUserId(context));
            
            bwr.newLine();
            bwr.write("Mailbox ID: " + RCMProviderHelper.getCurrentMailboxId(context));
            
            bwr.newLine();
            bwr.newLine();
            bwr.write("#Account Info");

            bwr.newLine();
            bwr.write("User ID: " + RCMProviderHelper.getAccountInfoUserId(context));
            
            bwr.newLine();
            bwr.write("Account Number: " + RCMProviderHelper.getAccountNumber(context));
            
            bwr.newLine();
            bwr.write("PIN: " + RCMProviderHelper.getAccountPin(context));
            
            bwr.newLine();
            bwr.write("User First Name: " + RCMProviderHelper.getAccountUserFirstName(context));
            
            bwr.newLine();
            bwr.write("User Last Name: " + RCMProviderHelper.getAccountUserLastName(context));
            
            bwr.newLine();
            bwr.write("Free: " + RCMProviderHelper.getAccountFree(context));
            
            bwr.newLine();
            bwr.write("Phone Parser settings: " + PhoneUtils.getParserSettingsDescription());
            
            try {
                bwr.newLine();
                bwr.write("BrandId: " + RCMProviderHelper.getBrandId(context));

                bwr.newLine();
                bwr.write("User type: " + UserInfo.getUserType(context));
                
                bwr.newLine();
                bwr.write("Tier service type: " + UserInfo.getTierServiceType(context));
                
                bwr.newLine();
                bwr.write("Service API version: " + RCMProviderHelper.getServiceApiVersion(context));
                
            } catch (java.lang.Throwable err) {
            }
            
            
            long tier_settings = RCMProviderHelper.getTierSettings(context);
            
            bwr.newLine();
            bwr.write("TierSettings: 0x" + Long.toHexString(tier_settings));
            
            bwr.newLine();
            bwr.write("TIERS_PHS_DIAL_FROM_CLIENT = " + ((tier_settings & RCMConstants.TIERS_PHS_DIAL_FROM_CLIENT) == 0 ? 0 : 1));
            
            bwr.newLine();
            bwr.write("TIERS_PHS_CAN_VIEW_EXTENSIONS = " + ((tier_settings & RCMConstants.TIERS_PHS_CAN_VIEW_EXTENSIONS) == 0 ? 0 : 1));
            
            bwr.newLine();
            bwr.write("TIERS_PHS_INTERNATIONAL_CALLING = " + ((tier_settings & RCMConstants.TIERS_PHS_INTERNATIONAL_CALLING) == 0 ? 0 : 1));
            
            bwr.newLine();
            bwr.write("TIERS_PHS_DO_NOT_DISTURB = " + ((tier_settings & RCMConstants.TIERS_PHS_DO_NOT_DISTURB) == 0 ? 0 : 1));
            
            bwr.newLine();
            bwr.write("TIERS_PHS_CHANGE_CALLERID = " + ((tier_settings & RCMConstants.TIERS_PHS_CHANGE_CALLERID) == 0 ? 0 : 1));

            bwr.newLine();
            bwr.write("DND: " + DndManager.getInstance().getDndStatus(context));

            if(RCMProviderHelper.getServiceVersion(context) > AccountInfoTable.SERVICE_VERSION_4){
                bwr.newLine();
                bwr.write("Extended DND: " + DndManager.getInstance().getExtendedDndStatus(context));
            }
            
            bwr.newLine();
            bwr.write("ExtModCounter: " + RCMProviderHelper.getExtModCounterTemp(context));
            
            bwr.newLine();
            bwr.write("ExtModCounter (previous): " + RCMProviderHelper.getExtModCounter(context));
            
            bwr.newLine();
            bwr.write("MsgModCounter: " + RCMProviderHelper.getMsgModCounterTemp(context));
            
            bwr.newLine();
            bwr.write("MsgModCounter (previous): " + RCMProviderHelper.getMsgModCounter(context));
            
            bwr.newLine();
            bwr.write("#VoIP: ");
                       
            if (BUILD.VOIP_ENABLED) {
                try {
                    bwr.newLine();
                    final long sipFlags = RCMProviderHelper.getHttpRegSipFlags(context);
                    bwr.write("HTTP REG SIP flags: 0x" + Long.toHexString(sipFlags));

                    bwr.newLine();
                    bwr.write("HTTP REG SIP flag VoIP enabled: " + ((sipFlags & RCMConstants.SIPFLAGS_VOIP_ENABLED) != 0));

                    bwr.newLine();
                    bwr.write(RCMProviderHelper.voipGetCurrentVoipSettingsStatesTrace(context));

                    bwr.newLine();
                    bwr.write("Settings:  isVoipEnabled_Env: " + RCMProviderHelper.isVoipEnabled_Env(context));

                    bwr.newLine();
                    bwr.write("Settings:  isVoipEnabled_Acc: " + RCMProviderHelper.isVoipEnabled_Acc(context));

                    bwr.newLine();
                    bwr.write("Settings:  isVoipEnabled_SipFlagHttpReg: " + RCMProviderHelper.isVoipEnabled_SipFlagHttpReg(context));

                    bwr.newLine();
                    bwr.write("Settings:  isVoipEnabled_Env_Acc_SipFlagHttpReg: " + RCMProviderHelper.isVoipEnabled_Env_Acc_SipFlagHttpReg(context));

                    bwr.newLine();
                    bwr.write("Settings:  isVoipEnabled_Env_Acc_ToS: " + RCMProviderHelper.isVoipEnabled_Env_Acc_ToS(context));

                    bwr.newLine();
                    bwr.write("Settings:  isVoipEnabled_Env_Acc_SipFlagHttpReg_ToS_UserVoIP: "
                            + RCMProviderHelper.isVoipEnabled_Env_Acc_SipFlagHttpReg_ToS_UserVoIP(context));

                    bwr.newLine();
                    bwr.write("Settings:  isVoipSwitchedOn_UserVoip: " + RCMProviderHelper.isVoipSwitchedOn_UserVoip(context));

                    bwr.newLine();
                    bwr.write("Settings:  isVoipSwitchedOn_UserWiFi: " + RCMProviderHelper.isVoipSwitchedOn_UserWiFi(context));

                    bwr.newLine();
                    bwr.write("Settings:  isVoipSwitchedOn_User3g4g: " + RCMProviderHelper.isVoipSwitchedOn_User3g4g(context));

                    bwr.newLine();
                    bwr.write("Settings:  isVoipSwitchedOn_UserIncomingCalls: " + RCMProviderHelper.isVoipSwitchedOn_UserIncomingCalls(context));
                    
                    bwr.newLine();
                    bwr.write("Settings:  isNeedToIncludeEDGEintoMobVoIP: " + RCMProviderHelper.isNeedToIncludeEDGEintoMobVoIP(context));
                    
                    bwr.newLine();
                    bwr.write("Settings:  isAlwaysCPULockForSIPstack: " + RCMProviderHelper.isAlwaysCPULockForSIPstack(context));
                    
                    bwr.newLine();
                    bwr.write("Wide-Band codec set: ");
                    
                    CodecInfo[] codec = RCMProviderHelper.getCodec(context, true);
                    for (CodecInfo ci : codec) {
                        bwr.newLine();
                        bwr.write("  " + ci.toString());
                    }
                    
                    bwr.newLine();
                    bwr.write("Narrow-Band codec set: ");
                    
                    codec = RCMProviderHelper.getCodec(context, false);
                    for (CodecInfo ci : codec) {
                        bwr.newLine();
                        bwr.write("  " + ci.toString());
                    }
                    
                } catch (java.lang.Throwable err) {
                }
            } else {
                bwr.newLine();
                bwr.write("VoIP disabled");
            }
            
            
            bwr.newLine();
            bwr.newLine();
            bwr.write("#Application Settings");

            bwr.newLine();
            bwr.write("Caller ID: " + RCMProviderHelper.getCallerID(context));
            
            int ro_mode = RCMProviderHelper.getRingoutMode(context);
            String ro_mode_label;
            if (ro_mode == RCMConstants.RINGOUT_MODE_MY_ANDROID) {
                ro_mode_label = "My Android";
            } else if (ro_mode == RCMConstants.RINGOUT_MODE_ANOTHER_PHONE) {
                ro_mode_label = "Another Phone";
            } else {
                ro_mode_label = "Unknown (" + ro_mode + ")";
            }
            
            bwr.newLine();
            bwr.write("RingOut Mode: " + ro_mode_label);
            
            bwr.newLine();
            bwr.write("Another Phone: " + RCMProviderHelper.getRingoutAnotherPhone(context));
            
            bwr.newLine();
            bwr.write("Custom Number: " + RCMProviderHelper.getCustomPhoneNumber(context));
            
            bwr.newLine();
            bwr.write("Confirm Connection: " + RCMProviderHelper.isConfirmConnection(context));
            
            bwr.newLine();
            bwr.newLine();
            bwr.write("Last success call number: " + RCMProviderHelper.getLastCallNumber(context));
            
            bwr.newLine();
            bwr.newLine();
            bwr.write("Caller IDs total: " + RCMProviderHelper.getCallerIDsCount(context));
            
            bwr.newLine();
            bwr.write("Forwarding Numbers total: " + RCMProviderHelper.getFwNumbersCount(context));
            
            bwr.newLine();
            bwr.write("Extensions total: " + RCMProviderHelper.getExtensionsCount(context));
            
            bwr.newLine();
            bwr.write("Company Favorites total: " + RCMProviderHelper.getCompanyFavoritesCount(context));
            
            bwr.newLine();
            bwr.write("Personal Favorites total: " + RCMProviderHelper.getPersonalFavoritesCount(context));
            
            bwr.newLine();
            bwr.newLine();
            bwr.write("Call Log: Calls total: " + RCMProviderHelper.getCallLogAllCount(context));
            
            bwr.newLine();
            bwr.write("Call Log: Missed calls: " + RCMProviderHelper.getCallLogMissedCount(context));
            
            bwr.newLine();
            bwr.newLine();
            bwr.write("Messages: total: " + RCMProviderHelper.getMessagesCount(context));
            
            bwr.newLine();
            bwr.write("Messages: Last Loaded Msg ID: " + RCMProviderHelper.getLastLoadedMsgId(context));

            bwr.newLine();
            bwr.write("Messages: Read: " + RCMProviderHelper.getMessagesReadCount(context));
            
            bwr.newLine();
            bwr.write("Messages: Unread: " + RCMProviderHelper.getMessagesUnreadCount(context));
            
            bwr.newLine();
            bwr.write("Messages: Locally Deleted: " + RCMProviderHelper.getMessagesLocallyDeletedCount(context));
            
            bwr.newLine();
            bwr.write("Messages: Loaded: " + RCMProviderHelper.getMessagesLoadedCount(context));
            
            bwr.newLine();
            bwr.write("Messages: Loading: " + RCMProviderHelper.getMessagesLoadingCount(context));
            
            bwr.newLine();
            bwr.write("Messages: Not Loaded: " + RCMProviderHelper.getMessagesNotLoadedCount(context));
            
            bwr.newLine();
            bwr.write("#ToS: ");
                       
            try {
                bwr.newLine();
                bwr.write("EULA Accepted: " + RCMProviderHelper.isTosAccepted(context));

                bwr.newLine();
                bwr.write("911 State (0 not decided/1 yes/2 no): " + RCMProviderHelper.getTos911State(context));
            } catch (java.lang.Throwable err) {
            }
            
            bwr.newLine();
            bwr.write("#Echo delay values: ");
            bwr.newLine();
            int externalSpkr = RCMProviderHelper.getDeviceExternalSpeakerDelay(context);
            bwr.write("external speaker: " + externalSpkr + " samples ~ " + externalSpkr / 8 + "ms"); // now hardcode because of we have just 8kHz TODO: remake next time
            bwr.newLine();
            int internalSpkr = RCMProviderHelper.getDeviceInternalSpeakerDelay(context);
            bwr.write("internal speaker: " + internalSpkr + " samples ~ " + internalSpkr/8 + "ms"); // now hardcode because of we have just 8kHz TODO: remake next time
            
            
            if (BUILD.VOIP_ENABLED) {
                try {
                	bwr.newLine();bwr.write(">>> VoIP state: ====================== ");
                	bwr.newLine();bwr.write(NetworkUtils.getNetworkStatusAsString(context));
                	bwr.newLine();bwr.write("RegisterExpiration:" + NetworkUtils.getForcedExpirationIfRecuired(context));
                	bwr.newLine();bwr.write("ForcedSIPport:" + NetworkUtils.getDedicatedSIPTransportLocalPortIfrequired(context));
                	bwr.newLine();
                	SIProxy proxy = SIProxy.getInstance();
                	bwr.newLine();bwr.write(" SIProxy : ");
                	bwr.newLine();bwr.write("   State      : " + proxy.getServiceConnectionStateAsString());
                	bwr.newLine();bwr.write("   Reconnects : " + proxy.getTotalNumberOfReconnects());
                    IServicePJSIP service = proxy.getService();
                    if (service != null) {
                        ServiceState state = service.getServiceState();
                        bwr.newLine();
                        if (state == null) {
                            bwr.write("Service State: NULL");
                        } else {
                            bwr.write(state.dump());
                        }
                        if (RCMProviderHelper.isAlwaysCPULockForSIPstack(context)) {
                            bwr.newLine();
                            bwr.write("  Always CPU Lock for SIP: TRUE");
                        }
                        /**
                         * Dump current calls.
                         */
                        RCCallInfo[] calls = service.getCurrentCalls();
                        if (calls == null) {
                            bwr.newLine();bwr.write(" Current calls : NULL");
                        } else {
                            bwr.newLine();bwr.write("> Current calls " + calls.length);
                            StringBuffer sb = new StringBuffer();
                            int activeCalls = 0;
                            int endingCalls = 0;
                            int i = 1; 
                            for (RCCallInfo call : calls) {
                                sb.append('\n');
                                sb.append("  Call " + i + "/" + calls.length);
                                sb.append('\n');
                                if (call != null) {
                                    if (call.isAlive()) {
                                        activeCalls++;
                                        if (call.isEnding()) {
                                            endingCalls++;
                                        }
                                    }
                                    sb.append(call.dump());
                                } else {
                                    sb.append("  NULL");
                                }
                                i++;
                                sb.append('\n');
                            }
                            
                            bwr.newLine();bwr.write("   Active(alive): " + activeCalls + " Ending:" + endingCalls);
                            bwr.newLine();bwr.write(sb.toString());
                            bwr.newLine();bwr.write("< Current calls");
                        }
                        
                    	/**
                    	 * Latest finished calls
                    	 */
                    	RCCallInfo[] finishedCalls = service.getLatestFinishedCalls();
                        if (finishedCalls == null) {
                        	bwr.newLine();bwr.write("> Latest finished calls : NULL");
                        } else {
                        	bwr.newLine();bwr.write("> Latest finished calls " + finishedCalls.length);
                            int i = 1;
                            for (RCCallInfo call : finishedCalls) {
                                bwr.newLine();bwr.write("  Call " + i + "/" + finishedCalls.length);
                                if (call != null) {
                                	bwr.newLine();bwr.write(call.dump());
                                } else {
                                    bwr.newLine();bwr.write("  NULL");
                                }
                                i++;
                            }
                            bwr.newLine();bwr.write("< Latest finished calls");
                        }
                    }
                	bwr.newLine();bwr.write("<<< VoIP state: ====================== ");
                } catch (java.lang.Throwable err) {
                }
            }
            
            bwr.newLine();
        } catch (IOException e) {
        } catch (Exception e) {
            try {
                bwr.newLine();
                bwr.write("Error writing app info: " + e.getMessage());
                bwr.newLine();
            } catch (IOException e1) {
            }
        }
    }

    public static String shortAppInfo(Context context) {
        StringBuilder sb = new StringBuilder();
       
        long mailbox_id = RCMProviderHelper.getCurrentMailboxId(context);

        sb.append("Mailbox ID: ");
        if (mailbox_id != MailboxCurrentTable.MAILBOX_ID_NONE) {
            sb.append(mailbox_id);
            sb.append('\n');
        } else {
            sb.append("NONE (User not logged in)\n");
        }
        
        sb.append("User ID: ");
        if (mailbox_id != MailboxCurrentTable.MAILBOX_ID_NONE) {
            sb.append(RCMProviderHelper.getUserId(context) + '\n');
        } else {
            sb.append("NONE\n");
        }

        sb.append("Login Number: ");
        if (mailbox_id != MailboxCurrentTable.MAILBOX_ID_NONE) {
            sb.append(RCMProviderHelper.getLoginNumber(context));
            String ext = RCMProviderHelper.getLoginExt(context);
            if (!TextUtils.isEmpty(ext)) {
                sb.append(" / " + ext);
            }
            sb.append('\n');
        } else {
            sb.append("NONE\n");
        }

        sb.append("Account Number: ");
        if (mailbox_id != MailboxCurrentTable.MAILBOX_ID_NONE) {
            sb.append(RCMProviderHelper.getAccountNumber(context));
            String pin = RCMProviderHelper.getAccountPin(context);
            if (!TextUtils.isEmpty(pin)) {
                sb.append(" / " + pin);
            }
            sb.append('\n');
        } else {
            sb.append("NONE\n");
        }
        
        return sb.toString();
    }
    
    public static String getBuildVersion(Context context, boolean showSvnRevision) {
        String version = null;
        try {
            version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            if (LogSettings.ENGINEERING) {
                EngLog.w(TAG, "getBuildVersion(): " + e.getMessage());
            }
        }
        
        if (version == null) {
            version = "1.7";    
        }

        if (showSvnRevision) {
            StringBuffer versionWithRevision = new StringBuffer(version);
            versionWithRevision.append(" (");
            versionWithRevision.append(context.getString(R.string.svn_revision));
            versionWithRevision.append(")");
            version = versionWithRevision.toString();
        }
            
        return version;
    }
}
