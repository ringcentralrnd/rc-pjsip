/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.utils.bluetooth;

import java.util.HashSet;
import java.util.Set;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothClass.Device;
import android.bluetooth.BluetoothClass.Service;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Handler;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.utils.media.RCMediaManager;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RingCentralApp;

/* 
Implementation notes : 
Bluetooth is normally supported starting API level 8, 2.2 Android version.
After installing an application(SIP service has started) you need at least one iteration of enabling/disabling bluetooth adapter
or connecting/disconnecting headset to device, for Bluetooth button to work in CallScreen,  that's due to the android API 8 limitations,
it's impossible to get a list of connected devices, only events, when they get connected or disconnected.
On HTC, Sense launcher after reboot if bluetooth is enabled, try to connect to audio last used headset, so application might miss audio connected event
and it will need one more switching iteration for bluetooth ;-(there is nothing we can do)

Supports on fly
- enabling/disabling bluetooth adapter
- connecting/disconnecting headset to phone audio
- if you have several devices paired, supports switching between them during call(work on most devices, for some reason work in 50% on FlipSide, 
  poor processor??), if not conencted from the first time, just pull toggle(10% times).

After headset get connected to phone audio(usually get message on the screen) timeout for routing audio to headset is 7 secononds,
that should be enough for underlying bluetooth stack to handle the requests, timeout really depends on device performance and processor load
but that should be enough for ALL devices that we currently have. When switching from phone to BT sound get disappeared for 1-2 seconds, that 
prevents user from hearing strong noise.

Some devices, again with Sence have poor/incorrect/unstable bluetooth support, and perform soft reboot(due to native media manager exceptions not more that 10% times) 
when having HARDCORE ITERATIONS with phone audio, example : make RC VOIP call forward audio to BT device, make native call, connect headset, play with speaker, etc. 
Powerful and mighty Ringcentral application is not responcible for vendor instability ;o)
*/

//TODO marrioz add support for API 11 and later
public class BluetoothAPI8 extends BluetoothAPIWrapper{
    private static final String TAG = "[RC]BluetoothAPI8";
    
    private static final int ACL_CONNECT_DELAY = 7000; // milliseconds, delay to connect after ACL event received to automatically switch to BT device
    private static final int EVENT_ACL_CONNECTED = 1;
            
    private BluetoothReceiver mBluetoothReceiver;
    private BluetoothAdapter mAdapter;
    private Handler mHandler = new Handler();
    
    protected boolean mAudioEnabled;
    
    @Override
    public void init(AudioManager audioManager, RCMediaManager mediaManager) {
    	if(LogSettings.MARKET) {
    		MktLog.d(TAG, "init()");
    	}
    	
    	super.init(audioManager, mediaManager);    	    	
    	
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        
        mBluetoothReceiver = new BluetoothReceiver();        
    	final IntentFilter receiverFilter = new IntentFilter(AudioManager.ACTION_SCO_AUDIO_STATE_CHANGED);
        receiverFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);        
        receiverFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        receiverFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        RingCentralApp.getContextRC().registerReceiver(mBluetoothReceiver , receiverFilter);          
    }
    
    @Override
    public void destroy() {
    	if(LogSettings.MARKET) {
    		MktLog.d(TAG, "destroy()");
    	}
    	
    	setBluetoothOn(false);
    	
    	if (mBluetoothReceiver != null) {    		    		
    		RingCentralApp.getContextRC().unregisterReceiver(mBluetoothReceiver);
    		mBluetoothReceiver = null;
    	}    	    	
    	
    	mAdapter = null;
    	
    	super.destroy();    		
    }

    @Override
    public boolean bluetoothHeadsetConnected() {
        if (mAdapter == null) {
        	if(LogSettings.MARKET) {
        		MktLog.e(TAG, "bluetoothHeadsetConnected() : Bluetooth adapter is null.");
        	}
            return false;
        }
        Boolean hasConnected = mBluetoothReceiver.hasConnectedDevices();//hasPairedDevices();        
        return (hasConnected == null) ? false : hasConnected; 
    }
    
    private boolean isAudioDevice(BluetoothDevice bluetoothDevice) {
    	final BluetoothClass bluetoothClass = bluetoothDevice.getBluetoothClass();
        if (bluetoothClass != null) {                	
            final int deviceClass = bluetoothClass.getDeviceClass();
            if(LogSettings.MARKET) {
            	MktLog.d(TAG, "isAudioDevice() name : " + bluetoothDevice.getName() + ", deviceClass : " + deviceClass);
            }
            return  
            	( bluetoothClass.hasService(Service.RENDER) ||
            	  deviceClass == Device.AUDIO_VIDEO_WEARABLE_HEADSET ||
            	  deviceClass == Device.AUDIO_VIDEO_CAR_AUDIO ||
            	  deviceClass == Device.AUDIO_VIDEO_HANDSFREE );
             
        }
        return false;
    }
    
    //marrioz can be used in future ??
    @SuppressWarnings("unused")
	private boolean hasPairedDevices(){
    	final boolean adapterEnabled = mAdapter.isEnabled();
        boolean pairedDevice = false;                 
        if(adapterEnabled) {
            Set<BluetoothDevice> pairedDevices = mAdapter.getBondedDevices();
            for(BluetoothDevice device : pairedDevices) {                
                if(isAudioDevice(device)){
                	pairedDevice = true;
                	break;
                }
            }
        }        
        final boolean result = pairedDevice && mAudioManager.isBluetoothScoAvailableOffCall();        
        if(LogSettings.MARKET) {
            MktLog.d(TAG, "hasPairedDevices(), adapter enabled : " + adapterEnabled + ", deviceConnected : " + pairedDevice + ", result : " + result);
        }        
        return result;
    }
    

    @Override
    public void setBluetoothOn(boolean enabled) {
        if(LogSettings.MARKET) { 
            MktLog.d(TAG, "setBluetoothOn(), set : " + enabled + ", was mAudioEnabled: " + mAudioEnabled);
        }
        if(enabled != mAudioEnabled) {
        	if(LogSettings.ENGINEERING){
        		EngLog.d(TAG, "setBluetoothOn(), starting...");
        	}
            if (enabled) {            	
                mAudioManager.setBluetoothScoOn(true);
                mAudioManager.startBluetoothSco();
                
            } else {
                mAudioManager.setBluetoothScoOn(false);
                mAudioManager.stopBluetoothSco();                
            }
            
            mAudioEnabled = enabled;
            
            if(LogSettings.ENGINEERING){
        		EngLog.d(TAG, "setBluetoothOn(), ending...");
        	}
        }
    }

    @Override
    public boolean audioConnected() {        
        return mAudioEnabled && mAudioManager.isBluetoothScoOn();    
    }
            
    /*
     * marat.daishev
     * on HTC devices during reboot/restart, if some bluetooth headset was connected, it get automatically reconnected 
     * after reboot during Sence start-up, and usually before we have received ACTION_ACL events.. so we need to rely 
     */
    private class BluetoothReceiver extends BroadcastReceiver {
    	
    	private Set<BluetoothDevice> connectedDevices;
    	private boolean isScoConnected;
    	private boolean isInited;
    	
    	public BluetoothReceiver() {
			super();
			connectedDevices = new HashSet<BluetoothDevice>();  	
			if(mAdapter != null) {	// caused by : java.lang.NullPointerException  - in Android Virtual Device
				isInited = !mAdapter.isEnabled(); 	// if services was started  
			}
		}
    	        	
    	@SuppressWarnings("unused")
		public boolean isSCOConnected() {
    		return isScoConnected;
    	}
    	
    	private Boolean hasConnectedDevices() {    		
    		BluetoothDevice device = null;
    		if(isInited){        			
	    		for(BluetoothDevice btDevice : connectedDevices) {	    			
	    			final boolean audioDevice = isAudioDevice(btDevice);
	    			if(audioDevice){
	    				device = btDevice;
	    				break;
	    			}
	    		}
    		} 
    		if(LogSettings.ENGINEERING) {
    			EngLog.v(TAG, "BluetoothReceiver.hasConnectedDevices() : isInited : " + isInited + ((device == null) ? ", no audio devices" : ", connected : " + device.getName()));
    		}    	
    		return (device == null) ? null : (device != null);     		
    	}
        
        @Override
        public void onReceive(Context context, Intent intent) {                        
            if(AudioManager.ACTION_SCO_AUDIO_STATE_CHANGED.equals(intent.getAction())) {
                final int status = intent.getIntExtra(AudioManager.EXTRA_SCO_AUDIO_STATE, AudioManager.SCO_AUDIO_STATE_ERROR );
                                
                if(LogSettings.ENGINEERING) { 
                    EngLog.d(TAG, "BluetoothReceiver.onReceive(), Bluetooth sco state changed : " + status);
                }
                
                switch (status) {
                // Sco events come and go not on bluetooth audio connecting/disconnecting, but after 
                // changing route from BT to some other so. if you toggle bluetooth on/off they will not come
                // but if you'll switch speaker/bluetooth, you'll get them
                case AudioManager.SCO_AUDIO_STATE_CONNECTED:
                	isScoConnected = true;
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "BluetoothReceiver.onReceive() : SCO_AUDIO_STATE_CONNECTED");
                    }
                    break;
                    
                case AudioManager.SCO_AUDIO_STATE_DISCONNECTED:
                	isScoConnected = false;
                    if (LogSettings.MARKET) { 
                        MktLog.d(TAG, "BluetoothReceiver.onReceive() : SCO_AUDIO_STATE_DISCONNECTED");
                    }
                    break;
                    
                case AudioManager.SCO_AUDIO_STATE_ERROR :
                	isScoConnected = false;
                	if (LogSettings.MARKET) { 
                        MktLog.d(TAG, "BluetoothReceiver.onReceive() : SCO_AUDIO_STATE_ERROR");
                    }
                    break;
                }
                mAudioEnabled = false;
                mMediaManager.notifyBluetoothRouteChanged(isScoConnected);
            } else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(intent.getAction())) {            	
            	if (LogSettings.ENGINEERING) { 
					EngLog.d(TAG, "BluetoothReceiver.onReceive() : ACTION_STATE_CHANGED, extra state : " + intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF));
				}
            	isScoConnected = false;
            	mAudioEnabled = false;
            	connectedDevices.clear();
            	mMediaManager.notifyBluetoothRouteChanged(false);            	
            } else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(intent.getAction())) {            	
            	BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            	if (LogSettings.MARKET) { 
                    MktLog.d(TAG, "BluetoothReceiver.onReceive() : ACTION_ACL_CONNECTED : " + (device == null ? "device : null " : device.getName()));
                }
            	connectedDevices.add(device);
            	if(!isInited) {
            		isInited = true;
            	}
            	
            	mHandler.removeMessages(EVENT_ACL_CONNECTED);
            	mHandler.postDelayed(new Runnable() {
        			
        			@Override
        			public void run() {
        				try {
        					mMediaManager.notifyBluetoothRouteChanged(true);
        				} catch (Throwable t) {
        					if(LogSettings.MARKET){
        						MktLog.e(TAG, "Error notifying media manager" , t);    						
        					}
        				}
        				
        			}
        		}, ACL_CONNECT_DELAY);
            	
            } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(intent.getAction())) {
            	BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            	if (LogSettings.MARKET) { 
                    MktLog.d(TAG, "BluetoothReceiver.onReceive() : ACTION_ACL_DISCONNECTED : " + (device == null ? "device : null " : device.getName()));
                }
            	mAudioEnabled = false;
            	connectedDevices.remove(device);
            	if(!isInited) {
            		isInited = true;
            	}            	
            	mMediaManager.notifyBluetoothRouteChanged(false);               	
            }
        }
    }    
}
