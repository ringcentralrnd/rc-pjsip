/** 
 * Copyright (C) 2010-2011 RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.utils;

public class RCJSONStringBuilder {
    String OperationSystem;
    String OperationSystemVersion;
    String DeviceModel;
    String Locale;
    String Manufacturer;
    String DeviceBrand;
    String UniqueIdOfDevice;
    String Carrier;
    String ApplicationId;
    String ApplicationVersion;
    String ProgrammBuild;
    

    public void setOperationSystem(String operationSystem) {
        this.OperationSystem = operationSystem;
    }

    public void setManufacturer(String manufacturer) {
        this.Manufacturer = manufacturer;
    }

    public void setOperationSystemVersion(String operationSystemVersion) {
        this.OperationSystemVersion = operationSystemVersion;
    }

    public void setDeviceModel(String deviceModel) {
        this.DeviceModel = deviceModel;
    }

    public void setLocale(String locale) {
        this.Locale = locale;

    }

    public void setDeviceBrand(String deviceBrand) {
        this.DeviceBrand = deviceBrand;
    }

    public void setUniqueIdOfDevice(String uniqueIdOfDevice) {
        this.UniqueIdOfDevice = uniqueIdOfDevice;
    }

    public void setCarrier(String carrier) {
        this.Carrier = carrier;
    }

    public void setApplicationId(String applicationId) {
        this.ApplicationId = applicationId;
    }

    public void setApplicationVersion(String applicationVersion) {
        this.ApplicationVersion = applicationVersion;
    }

    public void setProgrammBuild(String programmBuild) {
        if(null != programmBuild) {
            ProgrammBuild = programmBuild;
        }
    }
     
    public RCJSONStringBuilder(){
    }
    
    /**
     * That method processes incomming string, and returns it in json able format
     * @param incommingString - string to be verified
     * @return 
     */
    String processString(String incommingString){
        String resultString = null;
        
        if(null != incommingString) {
            incommingString.replace("\"", "");
            incommingString.replace("\\", "");
            incommingString.replace("\\/", "");
            incommingString.replace("\b", "");
            incommingString.replace("\f", "");
            incommingString.replace("\n", "");
            incommingString.replace("\r", "");
            incommingString.replace("\t", "");
            incommingString.replace("\\u", "");
        }
        
        return incommingString;
    }
    
    
    
    final static String separator = "\n";
    final static String start = "{";
    final static String end = "}";
    final static String quote = "\"";
    final static String colon = ":";
    final static String pairSeparator = ",\n";
    
    void appender(StringBuffer bfr, String _key, String _value) {
        if(null != bfr && null != _key && null != _value) {
            bfr.append(quote);
            bfr.append(_key);
            bfr.append(quote);
            bfr.append(colon);
            
            bfr.append(quote);
            bfr.append(_value);
            bfr.append(quote);
        }
    }
    
    
        
    String getJsonString(){
        StringBuffer resultString = new StringBuffer();
        
        boolean wasAddedAtLeastOnePair = false;
        
        resultString.append(start);
        if(null != this.OperationSystem) {
            if(wasAddedAtLeastOnePair) {
                resultString.append(",");
            }
            appender(resultString, "OS", this.OperationSystem);
            wasAddedAtLeastOnePair = true;
        }
        
        if(null != this.OperationSystemVersion){
            if(wasAddedAtLeastOnePair) {
                resultString.append(",");
            }
            appender(resultString, "OV", this.OperationSystemVersion);
            wasAddedAtLeastOnePair = true;
        }
        
        if(null != this.DeviceModel) {
            if(wasAddedAtLeastOnePair) {
                resultString.append(",");
            }
            appender(resultString, "DM", this.DeviceModel);
            wasAddedAtLeastOnePair = true;
        }
        
        if(null != this.Locale){
            if(wasAddedAtLeastOnePair) {
                resultString.append(",");
            }
            appender(resultString, "LO", this.Locale);
            wasAddedAtLeastOnePair = true;
        }
        
        if(null != this.Manufacturer) {
            if(wasAddedAtLeastOnePair) {
                resultString.append(",");
            }
            appender(resultString, "MF", this.Manufacturer);
            wasAddedAtLeastOnePair = true;            
        }
        
        if(null != this.DeviceBrand) {
            if(wasAddedAtLeastOnePair) {
                resultString.append(",");
            }
            appender(resultString, "DB", this.DeviceBrand);
            wasAddedAtLeastOnePair = true;            
        }
        
        if(null != this.UniqueIdOfDevice) {
            if(wasAddedAtLeastOnePair) {
                resultString.append(",");
            }
            appender(resultString, "ID", this.UniqueIdOfDevice);
            wasAddedAtLeastOnePair = true;            
        }
        
        if(null != this.Carrier) {
            if(wasAddedAtLeastOnePair) {
                resultString.append(",");
            }
            appender(resultString, "CR", this.Carrier);
            wasAddedAtLeastOnePair = true;            
        }
        
        if(null != this.ApplicationId) {
            if(wasAddedAtLeastOnePair) {
                resultString.append(",");
            }
            appender(resultString, "AP", this.ApplicationId);
            wasAddedAtLeastOnePair = true;            
        }
        
        if(null != this.ApplicationVersion) {
            if(wasAddedAtLeastOnePair) {
                resultString.append(",");
            }
            appender(resultString, "AV", this.ApplicationVersion);
            wasAddedAtLeastOnePair = true;            
        }
        
        if(null != this.ProgrammBuild) {
            if(wasAddedAtLeastOnePair) {
                resultString.append(",");
            }
            appender(resultString, "PB", this.ProgrammBuild);
            wasAddedAtLeastOnePair = true;
        }
        
        
        resultString.append(end);
        
        return resultString.toString();
    }
}
