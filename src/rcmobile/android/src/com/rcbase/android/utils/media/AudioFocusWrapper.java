/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.utils.media;

import com.rcbase.android.utils.Compatibility;

import android.media.AudioManager;

public abstract class AudioFocusWrapper {
    private static AudioFocusWrapper instance;
    protected AudioManager audioManager = null;

    public static AudioFocusWrapper getInstance() {
        if (instance == null) {
            String className = "com.rcbase.android.utils.media.AudioFocus";

            if (Compatibility.isCompatibleWith(8)) {
                className += "8";
            } else {
                className += "3";
            }

            try {
                Class<? extends AudioFocusWrapper> wrappedClass = Class.forName(className).asSubclass(AudioFocusWrapper.class);
                instance = wrappedClass.newInstance();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }

        return instance;
    }

    protected AudioFocusWrapper() {
    }

    public abstract void init(AudioManager manager);
    public abstract void focus();
    public abstract void unFocus();
}
