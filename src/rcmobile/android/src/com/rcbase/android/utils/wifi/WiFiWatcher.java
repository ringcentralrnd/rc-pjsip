package com.rcbase.android.utils.wifi;

import java.net.InetAddress;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Handler;
import android.os.Message;
import android.text.format.Formatter;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;

public class WiFiWatcher {
	
	private static final String TAG = "[RC]WiFiWatcher";
	
	//need to examine carefully, subscribe on WiFi events and keep state,
	//so that do not conflict with native WiFi engine and other WiFi restore applications
	private static final boolean PERFORM_WIFI_RESCAN = false;
	
	private static final String WIFI_LOCK = "RingCentral VoIP WiFi lock";
	
	private static final int MSG_WIFI_CHECK = 1;	
	private static final int MSG_ARG_NONE = 0;
	private static final int MSG_ARG_REASSOCIATE = MSG_ARG_NONE + 1;
	private static final int MSG_ARG_RECONNECT = MSG_ARG_REASSOCIATE + 1;
	private static final int MSG_ARG_RESCAN = MSG_ARG_RECONNECT + 1;
	
	private static final int BACKGROUND_DELAY = 30 * 1000; // once a minute
	
	private static final int PING_TIMEOUT = 3 * 1000;
	private static final int PING_TIMEOUT_RECONNECT = PING_TIMEOUT * 2;		
			
	private ScreenStateReceiver mScreenStateReceiver;
	private WifiManager mWiFiManager;
	private WifiLock mWiFiLock;
	private Context mContext;	
	private WatchHandler mWatchHandler;
	private WiFiStatusContentObserver mWiFiStatusContentObserver;
	
	private boolean mWatching = false;
	
	private static long[] mStatistic;
	
	
	public WiFiWatcher(Context context) {
		
		if (LogSettings.ENGINEERING) {
			EngLog.v(TAG, "constructor");
		}
		
		mContext = context;		 		
		mScreenStateReceiver = new ScreenStateReceiver();
		mScreenStateReceiver.register(context);	
		
		mWiFiStatusContentObserver = new WiFiStatusContentObserver();
		
		mWiFiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
			
		try {
			if (mWiFiManager != null) {
				int setWiFiLockMode = WifiManager.WIFI_MODE_FULL;
				if (Integer.parseInt(android.os.Build.VERSION.SDK) > 11) {
					setWiFiLockMode = 3;
				}
				mWiFiLock = mWiFiManager.createWifiLock(setWiFiLockMode, WIFI_LOCK);
				mWiFiLock.setReferenceCounted(false);
			}
		} catch (Throwable t) {
			if(LogSettings.MARKET) {
				MktLog.e(TAG, "Error creating WiFi lock", t);
			}
		}
		
		mWatchHandler = new WatchHandler();
		
		if(LogSettings.MARKET){
			mStatistic = new long[MSG_ARG_RESCAN+1];
		}
		
	}
	
	public void destroy() {
		
		if (LogSettings.ENGINEERING) {
			EngLog.v(TAG, "destroy...");
		}
		
		stopWatching();
		
		if (mWiFiManager != null) {
			mWiFiManager = null;
		}
		
		releaseWiFiLock();
		mWiFiLock = null;
		
		if (mScreenStateReceiver != null) {
			mScreenStateReceiver.unregister(mContext);
			mScreenStateReceiver = null;
		}
		
		if (mWiFiStatusContentObserver != null) {
			mWiFiStatusContentObserver.stopWatching();
			mWiFiStatusContentObserver = null;
		}
		
		if(mWatchHandler != null) {
			mWatchHandler.removeMessages(MSG_WIFI_CHECK);
			mWatchHandler = null;
		}		
		
		mContext = null;	
		
		
	}
	
	/**
	 * Start/Stop watching
	 */
	
	public synchronized void startWatching() {
		
		final boolean wifiEnabled = isWiFiEnabled(mContext);
		final boolean wifiConnected = isWiFiConnected(mContext);
		
		if (LogSettings.MARKET) {
			MktLog.d(TAG, "startWatching(), isWatching : " + isWatching() + ", wifiEnabled : " + wifiEnabled + ", wifiConnected : " + wifiConnected);
		}
		if(!isWatching() && wifiEnabled && wifiConnected){
			accuireWiFiLock();
			mWatching = true;
			sendWatchMessage(MSG_ARG_NONE);
			mWiFiStatusContentObserver.startWatching();
		}
	}
	
	
	
	public synchronized void stopWatching() {
		if (LogSettings.MARKET) {
			MktLog.d(TAG, "stopWatching(), isWatching : " + isWatching());
		}
		if(isWatching()){
			releaseWiFiLock();
			mWatching = false;
			sendWatchMessage(MSG_ARG_NONE);
			mWiFiStatusContentObserver.stopWatching();
		}
	}
	
	public synchronized boolean isWatching() {
		return mWatching;
	}		
	
    public synchronized boolean isLockHeld() {
        if (mWiFiLock != null) {
            return mWiFiLock.isHeld();
        }
        return false;
    }
    
    public synchronized int getLockCounter() {
        return mCounter;
    }
    
	
	private void repairNetwork(Context context, int reason) {
		if (LogSettings.ENGINEERING) {
			EngLog.d(TAG, "repairNetwork, reason : " + reason);
		}
		
		if(reason == MSG_ARG_NONE) {
			//ignoring
		} else if (reason == MSG_ARG_REASSOCIATE) {
		    if(LogSettings.MARKET) {
                MktLog.e(TAG, "WiFi REASSOCIATE");
            }
			mWiFiManager.reassociate();
		} else if (reason == MSG_ARG_RECONNECT) {
		    if(LogSettings.MARKET) {
                MktLog.e(TAG, "WiFi RECONNECT");
            }
			mWiFiManager.reconnect();
		} else if (reason == MSG_ARG_RESCAN) {
			if(PERFORM_WIFI_RESCAN) {
				mWiFiManager.startScan();
			}
		}
		if(LogSettings.MARKET){
			mStatistic[reason]++;
			MktLog.d(TAG, "Statistics, none : " + mStatistic[MSG_ARG_NONE] +
									 " reassosiate : " + mStatistic[MSG_ARG_REASSOCIATE] + 
									 " reconnect : " + mStatistic[MSG_ARG_RECONNECT] + 
									 " rescan : " + mStatistic[MSG_ARG_RESCAN]);
		}
		
	}
	
	private int getNetworkFix(Context context, boolean testReconnect) {
		if (LogSettings.ENGINEERING) {
			EngLog.d(TAG, "getNetworkFix started");
		}
		int networkStatus = MSG_ARG_NONE;
		
		if ((mScreenStateReceiver != null) && mScreenStateReceiver.isScreenOn()){
			if (LogSettings.ENGINEERING) {
				EngLog.d(TAG, "Screen enabled, ignoring watcher state");
			}
			
		} else {
		
			if (isWatching() && isWiFiEnabled(context)){
				if(isWiFiConnected(context)) {
					if(!pingICMPHost(context, PING_TIMEOUT)){
						networkStatus = MSG_ARG_REASSOCIATE;
						if(testReconnect && !pingICMPHost(context, PING_TIMEOUT_RECONNECT)){ // same is done in WiFi reassosicate and WiFi fixer tools
							networkStatus = MSG_ARG_RECONNECT; 
						}
					}
				} else {
					networkStatus = MSG_ARG_RESCAN;
				}
			}
		}
        if (LogSettings.MARKET) {
            if (networkStatus != MSG_ARG_NONE) {
                StringBuffer sb = new StringBuffer("getNetworkFix: network status :");
                switch (networkStatus) {
                case (MSG_ARG_REASSOCIATE):
                    sb.append("REASSOCIATE");
                    break;
                case (MSG_ARG_RECONNECT):
                    sb.append("RECONNECT");
                    break;
                case (MSG_ARG_RESCAN):
                    sb.append("RESCAN");
                    break;
                }

                MktLog.i(TAG, "getNetworkFix: network status : " + sb.toString());
            }
        }
		return networkStatus;
	}
	
	private long getNextDelay(){
		return BACKGROUND_DELAY;
	}
	
	private static final int MAX_ACQUIRES_WHEN_WARN = 5;
	
	/**
	 * Locks
	 */
    private void accuireWiFiLock() {
        if (LogSettings.ENGINEERING) {
            EngLog.d(TAG, "accuireWiFiLock");
        }
        try {
            if ((mWiFiLock != null) && (!mWiFiLock.isHeld())) {
                mWiFiLock.acquire();
                mCounter++;
            }
        } catch (Throwable t) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "Error accuiring WiFi lock", t);
            }
        }
        if (mCounter >= MAX_ACQUIRES_WHEN_WARN) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, " acquire:limit:" + mCounter);
            }
        }
    }
	
	private void releaseWiFiLock() {
		if (LogSettings.ENGINEERING) {
			EngLog.d(TAG, "releaseWiFiLock");
		}
		try {
			if (mWiFiLock != null) {
			    if(mWiFiLock.isHeld()) {
			        mWiFiLock.release();
			        mCounter--;
			    } else {
			        mCounter = 0;
			    }
			}
		} catch (Throwable t) {
			if(LogSettings.MARKET) {
				MktLog.e(TAG, "Error releasing WiFi lock", t);
			}
		}		
	}
	
	private int mCounter = 0;
	
	public static boolean pingICMPHost(Context context, int timeout) {
		if (LogSettings.ENGINEERING) {
			EngLog.d(TAG, "pingICMPHost, timeout : " + timeout);
		}
		
		boolean pingPassed = false;
				
		final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);		
		if (wifiManager != null) {
			try {
				DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
				if (dhcpInfo != null) {
					final InetAddress inetAddress = InetAddress.getByName(Formatter.formatIpAddress(dhcpInfo.gateway));
					pingPassed = inetAddress.isReachable(timeout);					
				}
			} catch (Throwable t) {
				if(LogSettings.MARKET) {
					MktLog.e(TAG, "pingICMPHost", t);
				}
			}
		} 
		
		if (LogSettings.ENGINEERING) {
			EngLog.d(TAG, "pingICMPHost, passed : " + pingPassed);
		}						
		return pingPassed;
	}
	
	
	public static boolean isWiFiEnabled(Context context) {		
		boolean enabled = false;
		
		final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);		
		if (wifiManager != null) {
			enabled = wifiManager.isWifiEnabled();
		} 
		
		if (LogSettings.ENGINEERING) {
			EngLog.d(TAG, "isWiFiEnabled : " + enabled);
		}	
		return enabled;
	}
	
	public static boolean isWiFiConnected(Context context) {
		boolean connected = false;
		
		final ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connManager != null) {
			final NetworkInfo ni =connManager.getActiveNetworkInfo();
			if ( (ni != null) && (ni.getType() == ConnectivityManager.TYPE_WIFI) && (ni.isConnectedOrConnecting()) ) {
				connected = true;
			}
		}
		
		if (LogSettings.ENGINEERING) {
			EngLog.d(TAG, "isWiFiConnected : " + connected);
		}			
		return connected;
	}
	
	private void sendWatchMessage(int arg1){
		if(mWatchHandler != null){
			mWatchHandler.removeMessages(MSG_WIFI_CHECK);
			if(isWatching()){
				Message msg = mWatchHandler.obtainMessage(MSG_WIFI_CHECK, arg1, MSG_ARG_NONE);
				mWatchHandler.sendMessageDelayed(msg, getNextDelay());
			}
		}
	}
	
	public boolean isScreenOn() {
		return mScreenStateReceiver.isScreenOn();
	}
	
	
	private class WatchHandler extends Handler {
				
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if(LogSettings.ENGINEERING) {
				EngLog.d(TAG, "WatchHandler, arg1 : " + msg.arg1);				
			}
			
			if(msg.what == MSG_WIFI_CHECK) {
				final int networkFix = getNetworkFix(mContext, true);				
				repairNetwork(mContext, networkFix);								
				sendWatchMessage(networkFix);
			} else {
				if(LogSettings.MARKET) {
					MktLog.e(TAG, "WatchHandler, received message : " + msg.what + " aborting");
				}
			}
		}
	}
	
	
	private class ScreenStateReceiver extends BroadcastReceiver {
		
		//when start watching WiFi screen state should be on
		private boolean mScreenOn = true; 					
		
		@Override
		public void onReceive(Context context, Intent intent) {			
			if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)){
				setScreenState(true);
			} else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)){
				setScreenState(false);				 
			}			
			
			if (LogSettings.ENGINEERING) {
				EngLog.v(TAG, "ScreenStateReceiver, screenOn : " + mScreenOn);
			}
		}
		
		public synchronized boolean isScreenOn(){
			return mScreenOn;
		}
		
		public synchronized void setScreenState(boolean on){
			mScreenOn = on;
		}
		
		public void register(Context context) {
			IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
		    filter.addAction(Intent.ACTION_SCREEN_ON);
		    context.registerReceiver(this, filter);
		}
		
		public void unregister(Context context) {
			context.unregisterReceiver(this);
		}
		
	}

}
