/**
 * Copyright (C) 2010-2011, RingCentral, Inc.
 * All Rights Reserved.
 */

package com.rcbase.android.utils;

class RCRandomEncryption {
    private final static String prefix = "CDEP1";

    /**
     * 
     * @param originalString - string to be encrypted
     * @param start - use 234
     * @param mult - use 34
     * @param add - use 902
     * @return
     */
    static String RCSPCreateRandomEncryptedString(String plaintext, int start, int mult, int add) {
        String result = null;

        String encodedKeys = null;
        if(null != plaintext) {
            int randStart = randomKey(), randMult = randomKey(), randAdd = randomKey();
            String encodedData = RCSPCreateSimplyEncryptedString(plaintext, randStart, randMult, randAdd);
            String hexKeys = String.format("%3H", randStart) + String.format("%3H",randMult) + String.format("%3H", randAdd);
            if(null != hexKeys && (null != (encodedKeys = RCSPCreateSimplyEncryptedString(hexKeys, start, mult, add))) ) {
                result = prefix + encodedKeys + encodedData;
            }
        }

        return result;
    }

    static String RCSPCreateSimplyEncryptedString(String originalString, int start, int mult, int add) {
        StringBuffer simplyEncryptedString = new StringBuffer();

        if (null != originalString) {
            int originalStringLength = originalString.length();
            char[] originalStringInChar = originalString.toCharArray();

            if (0 < originalStringLength) {
                int index = 0;
                int indexStart = start;

                for (index = 0; index < originalStringLength; ++index) {
                    char encryptedChar = originalStringInChar[index];
                    encryptedChar ^= (indexStart >> 8 & 0xFF);
                    indexStart = ((encryptedChar + indexStart) * mult + add) & 0xFFF;
                    simplyEncryptedString.append(String.format("%2H", encryptedChar));
                }
            }

        }
        return simplyEncryptedString.toString();
    }

    static int randomKey() {
        final int randomRange = 4096 - 4; // all magic numbers where borrowed from iPhone's sources
        return (4 + (int) ( ((double) randomRange) * Math.random()) );
    }
}

