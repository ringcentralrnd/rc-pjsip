/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 * 
 */
package com.rcbase.android.utils.bluetooth;

import android.media.AudioManager;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.utils.Compatibility;
import com.rcbase.android.utils.media.RCMediaManager;
import com.ringcentral.android.LogSettings;

public abstract class BluetoothAPIWrapper {
    private static final String TAG = "[RC]BluetoothAPIWrapper";
    
    private static final String CLASS_NAME = "com.rcbase.android.utils.bluetooth.BluetoothAPI";
    
    private static BluetoothAPIWrapper instance = null;
    protected AudioManager mAudioManager;
    protected RCMediaManager mMediaManager;
    
    
    
    public static synchronized BluetoothAPIWrapper getInstance() {
        if(null == instance) {
            BluetoothAPIWrapper _instance = null;            
            try {
            	final String clazzName = CLASS_NAME + (Compatibility.isCompatibleWith(8) ? "8" : "3"); 
            	Class<? extends BluetoothAPIWrapper> bluetoothAPI = Class.forName(clazzName).asSubclass(BluetoothAPIWrapper.class);
                _instance = bluetoothAPI.newInstance();                
            } catch (Throwable t) {
                if(LogSettings.MARKET) { 
                    MktLog.e(TAG, "getInstance() unable to instaniate BluetoothWrapper ", t);
                }
                _instance = null;
            }    
            instance = _instance;
        }        
        return instance;
    } 

    public void init(AudioManager audioManager, RCMediaManager mediaManager) {
    	this.mAudioManager = audioManager;
    	this.mMediaManager = mediaManager;    	
    }
    
    public void destroy() {
    	mAudioManager = null;
    	mMediaManager = null;    	
    }

    //headset connected
    public abstract boolean bluetoothHeadsetConnected();
    //got event that audio is playing over blueooth
    public abstract boolean audioConnected();    
    public abstract void setBluetoothOn(boolean on);
    

}
