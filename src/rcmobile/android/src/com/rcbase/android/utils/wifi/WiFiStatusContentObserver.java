package com.rcbase.android.utils.wifi;

import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;

import com.rcbase.android.logging.EngLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RingCentralApp;

class WiFiStatusContentObserver extends ContentObserver{
	private static final String TAG = "[RC]WiFiStatusContentObserver";
	
	private Integer mWifiStatus;    
	private static boolean mInited = false;
		
	public WiFiStatusContentObserver() {
		super(new Handler());		
		
		if(LogSettings.ENGINEERING) {
			EngLog.d(TAG, "Constructor");
		}
	}
	
	public synchronized void startWatching(){
		
		if(LogSettings.ENGINEERING) {
			EngLog.d(TAG, "startWatching, inited : " + mInited);
		}
		
		if (!mInited) {
			try {
				mWifiStatus = Settings.System.getInt(RingCentralApp.getContextRC().getContentResolver(), Settings.System.WIFI_SLEEP_POLICY);
				Settings.System.putInt(RingCentralApp.getContextRC().getContentResolver(), Settings.System.WIFI_SLEEP_POLICY, Settings.System.WIFI_SLEEP_POLICY_NEVER);
				if(LogSettings.ENGINEERING){
					EngLog.d(TAG, "startWatching WiFi status : " + mWifiStatus + " setting to " + Settings.System.WIFI_SLEEP_POLICY_NEVER);
				}			
			} catch (Throwable t) {
				if(LogSettings.ENGINEERING){
					EngLog.e(TAG, "startWatching, mWiFiStatus : " + mWifiStatus + " " + t);
				}				
			} finally {				
				if (mWifiStatus !=  null) {
					mInited = true;
					RingCentralApp.getContextRC().getContentResolver().registerContentObserver(Settings.System.getUriFor(Settings.System.WIFI_SLEEP_POLICY), false, this);
				}
			}			
		}		
	}
		
	@Override
 	public void onChange(boolean selfChange) {
		super.onChange(selfChange);
		
		if (LogSettings.ENGINEERING) {
			EngLog.d(TAG, "OnChange selfChange : " + selfChange);
		}
		
		Integer wifiStatus = null;
		try {
			wifiStatus = Settings.System.getInt(RingCentralApp.getContextRC().getContentResolver(), Settings.System.WIFI_SLEEP_POLICY);
			
			if((wifiStatus != null)&&(wifiStatus != Settings.System.WIFI_SLEEP_POLICY_NEVER)){
				
				if(LogSettings.ENGINEERING){
					EngLog.d(TAG, "onChange() user-initiated update for Wifi status : " + wifiStatus);
				}
				
				Settings.System.putInt(RingCentralApp.getContextRC().getContentResolver(), Settings.System.WIFI_SLEEP_POLICY, Settings.System.WIFI_SLEEP_POLICY_NEVER);
				mWifiStatus = wifiStatus;													
			}
		} catch (Throwable t) {
			if(LogSettings.ENGINEERING){
				EngLog.e(TAG, "onChange() Settings not found, wifiStatus : " + wifiStatus + " " + t);
			}
		}
	}

	public synchronized void stopWatching(){	
		
		if (LogSettings.ENGINEERING) {
			EngLog.d(TAG, "stopWatching... mInited : " + mInited);
		}
		
		if (mInited) {
			try {
				RingCentralApp.getContextRC().getContentResolver().unregisterContentObserver(this);				
				Settings.System.putInt(RingCentralApp.getContextRC().getContentResolver(), Settings.System.WIFI_SLEEP_POLICY, mWifiStatus);
				if(LogSettings.ENGINEERING){
					EngLog.d(TAG, "stopWatching reverting Wifi statis back : " + mWifiStatus);
				}			
			} catch (Throwable t) {
				if(LogSettings.ENGINEERING){
					EngLog.e(TAG, "stopWatching " + t);
				}
			} finally {
				mInited = false;
				mWifiStatus = null;				
			}
		}
	}
	
}
