/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.utils.media;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.RemoteException;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.audio.AudioDevice;
import com.rcbase.android.sip.service.IServicePJSIPCallback;
import com.rcbase.android.sip.service.ServicePJSIP;
import com.rcbase.android.sip.service.WrapPJSIP;
import com.rcbase.android.utils.Compatibility;
import com.rcbase.android.utils.bluetooth.BluetoothAPIWrapper;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RingCentralApp;

public class RCMediaManager {
    private static final String TAG = "[RC]RCMediaManager";    
    
    public static final int MODE_DETACHED = 0;    
    public static final int MODE_TEST_SPEAKER = 1;
    public static final int MODE_TEST_INTERNAL = 2;
    public static final int MODE_IN_CALL = 3;
    
    public static final int ROUTE_INTERNAL = 0;
    public static final int ROUTE_SPEAKER = 1;
    public static final int ROUTE_BLUETOOTH = 2;
    
    private static final int API_11_AUDIO_MANAGER_MODE_IN_COMMUNICATION = 3;
    
    private AudioManager mAudioManager;
    private BluetoothAPIWrapper mBluetoothManager;  // necessary for sending commands 
    private AudioFocusWrapper mAudioFocusManager; // manage the                     
    private HeadsetStateReceiver mHeadsetReceiver;
    private WrapPJSIP mWrapperPJSIP;
    private IServicePJSIPCallback mAudioChangedCallback;
    
    private int mMode = MODE_DETACHED; 
    private int mAudioRoute;    
    
	public RCMediaManager(IServicePJSIPCallback audioChangedCallback, WrapPJSIP wrapperPJSIP) {
		if (LogSettings.MARKET) {
			MktLog.i(TAG, "create()");
		}

		mMode = MODE_DETACHED; 
		
		mAudioManager = (AudioManager) RingCentralApp.getContextRC().getSystemService(Context.AUDIO_SERVICE);
		
		if (LogSettings.MARKET) {
		    if (mAudioManager.isSpeakerphoneOn())
		    	MktLog.e(TAG, "Constructor:Speaker is ON");
		}

		mBluetoothManager = BluetoothAPIWrapper.getInstance();
		mBluetoothManager.init(mAudioManager, this);

		mAudioFocusManager = AudioFocusWrapper.getInstance();
		mAudioFocusManager.init(mAudioManager);

		mHeadsetReceiver = new HeadsetStateReceiver();
		RingCentralApp.getContextRC().registerReceiver(mHeadsetReceiver, new IntentFilter(Intent.ACTION_HEADSET_PLUG));
		

		mAudioChangedCallback = audioChangedCallback;
		mWrapperPJSIP = wrapperPJSIP;
	}  
    
    
    public synchronized void destroy(){
    	if (LogSettings.MARKET) {
			MktLog.i(TAG, "destroy()");
		}
    	
    	stopAudio();
    	
    	mBluetoothManager.destroy();
    	mBluetoothManager = null;    	    	
    	
    	if(mHeadsetReceiver != null) {
    		RingCentralApp.getContextRC().unregisterReceiver(mHeadsetReceiver);
    		mHeadsetReceiver = null;    		 
    	}
    	
    	mMode = MODE_DETACHED;    	
    	mAudioFocusManager  = null;
    	mAudioChangedCallback = null;    	
    	mWrapperPJSIP  = null;    	
    }  
    
    
    /**
     * Start managing in-call audio or prepare test mode
     * @param testMode if test mode enabled
     */
	public synchronized void startAudio(final int mode) {
        if (mode != mMode) {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "startAudio(), TRY " + getModeTag(mMode) + "->" + getModeTag(mode));
            }
        }
		if (mode > mMode) {
		    
		    if (LogSettings.MARKET) {
	            MktLog.e(ServicePJSIP.TAG_EMG, TAG + ": startAudio(): " + getModeTag(mode));
	        }
		    
			mMode = mode;								
			
			setAudioMode(true);
			
			if (mMode == MODE_TEST_SPEAKER) {
				mAudioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
				setAudioRoute(ROUTE_SPEAKER, false);
			} else if (mMode == MODE_TEST_INTERNAL) {
				mAudioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
				setAudioRoute(ROUTE_INTERNAL, false);
			} else if (mMode == MODE_IN_CALL) {			
				mAudioRoute = computeAudioRoute();		
				if (LogSettings.MARKET) {
					MktLog.d(TAG, "startAudio(), initial route : " + getRouteTag(mAudioRoute));
				}
				if (mBluetoothManager.bluetoothHeadsetConnected()) {			
					routeBluetooth();				
				} else {
				    setAudioRoute(ROUTE_INTERNAL, false);
				}
			}
			
			mAudioFocusManager.focus();
		}
	}    
    
    /**
     * Stop managing in-call audio or test mode 
     */
    public synchronized void stopAudio() {
    	if (LogSettings.MARKET) {
            MktLog.i(TAG, "stopAudio() mode : " + getModeTag(mMode));
        }
    	
    	if(mMode != MODE_DETACHED) {
            if (LogSettings.MARKET) {
                MktLog.e(ServicePJSIP.TAG_EMG, TAG + ": stopAudio()");
            }
    		mMode = MODE_DETACHED;
    		    		    		
    		setAudioRoute(ROUTE_INTERNAL, true);
    		
    		mAudioFocusManager.unFocus();    		 
    		setAudioMode(false);
    		
    	}    	    	
    }     
    
    /**
     * route audio via internal speaker
     */
    public synchronized void routeInternal() {    	
    	final int currentRoute = computeAudioRoute();
    	
    	if(LogSettings.MARKET) {
            MktLog.d(TAG, "routeInternal(), current : " + getRouteTag(currentRoute));
        }
    	
    	if(currentRoute != ROUTE_INTERNAL) {
    		setAudioRoute(ROUTE_INTERNAL, false);
    	}
    }
    
    /**
     * route audio via external speaker
     */
    public synchronized void routeSpeaker() {
    	final int currentRoute = computeAudioRoute();
    	
    	if(LogSettings.MARKET) {
            MktLog.d(TAG, "routeSpeaker(), current : " + getRouteTag(currentRoute));
        }
    	
    	if((currentRoute != ROUTE_SPEAKER) && (!mHeadsetReceiver.isConnected())) {    	
    		setAudioRoute(ROUTE_SPEAKER, false);
    	}
    }
    
    /**
     * route audio via bluetooth headset, if connected
     */
    public synchronized void routeBluetooth() {
    	final int currentRoute = computeAudioRoute();
    	
    	if(LogSettings.MARKET) {
            MktLog.d(TAG, "routeBluetooth(), current : " + getRouteTag(currentRoute));
        }
    	
    	if(currentRoute != ROUTE_BLUETOOTH) {
    		setAudioRoute(ROUTE_BLUETOOTH, false);
    	}
    }
    
    
    /**
     * toggle routing via speaker on/off
     */
    public synchronized void toggleSpeaker() {
    	if(!mHeadsetReceiver.isConnected()) {
	    	final int currentRoute = computeAudioRoute();
	    	
	    	if(LogSettings.MARKET) {
	            MktLog.d(TAG, "toggleSpeaker(), current : " + getRouteTag(currentRoute));            
	        }
	    	
	    	setAudioRoute((currentRoute != ROUTE_SPEAKER) ? ROUTE_SPEAKER : ROUTE_INTERNAL, false);
    	} else {
    		if(LogSettings.MARKET) {
	            MktLog.w(TAG, "toggleSpeaker(), toggle speaker event when headset connecting, strange issue");            
	        }
    	}
    	
    }    
    
    /**
     * toggle routing via bluetooth on/off
     */
    public synchronized void toggleBluetooth() {    	
    	final int currentRoute = computeAudioRoute();
    	
    	if(LogSettings.MARKET) {
            MktLog.d(TAG, "toggleBluetooth(), current : " + getRouteTag(currentRoute));            
        }
    	
    	setAudioRoute((currentRoute != ROUTE_BLUETOOTH)? ROUTE_BLUETOOTH : ROUTE_INTERNAL, false);    	
    }
    
    /**
     * notification that bluetooth route has changed, ex headset disconnected etc
     */
    public synchronized void notifyBluetoothRouteChanged(boolean forceRoute) {   
    	if(mMode == MODE_IN_CALL) {
    		
	    	notifyRouteChanged();
	    	
	    	if(LogSettings.MARKET) {
	            MktLog.d(TAG, "notifyBluetoothRouteChanged(), current : " + getRouteTag(mAudioRoute));            
	        }
	    	
	    	if(forceRoute && (mAudioRoute != ROUTE_BLUETOOTH) && mBluetoothManager.bluetoothHeadsetConnected()) {
	    		routeBluetooth();
	    	}
    	}
    }
    
    public synchronized void notifyHeadsetRouteChanged(boolean connected) {
    	if(mMode == MODE_IN_CALL) {
    		final int realRoute = computeAudioRoute();		
    		
    		if(connected && realRoute != ROUTE_BLUETOOTH){
    			setAudioRoute(ROUTE_INTERNAL, false);
    		}
    		notifyRouteChanged();
	    	
	    	if(LogSettings.MARKET) {
	            MktLog.d(TAG, "notifyHeadsetRouteChanged(), current : " + getRouteTag(mAudioRoute) + ", connected : " + connected);            
	        }
    	}
    }
    
    public synchronized AudioState getAudioState() {    	    	
    	final int realRoute = computeAudioRoute();		
		if(mAudioRoute != realRoute){   
			mAudioRoute = realRoute;
		}    	     	
				
		AudioState audioState = new AudioState();
    	audioState.setBluetooth(mAudioRoute == ROUTE_BLUETOOTH);
    	audioState.setBluetoothEnabled(mBluetoothManager.bluetoothHeadsetConnected());
    	audioState.setSpeaker(mAudioRoute == ROUTE_SPEAKER);
    	audioState.setSpeakerEnabled(!mHeadsetReceiver.isConnected());
    	
    	return audioState;    	
    }
    

       
    
    private synchronized void setAudioRoute(int route, boolean stop) {
    	if(LogSettings.MARKET) { 
            MktLog.i(TAG, "setAudioRoute() current : " + getRouteTag(mAudioRoute) + " new : " + getRouteTag(route));
        }      
    	
    	if((route == ROUTE_SPEAKER) && mHeadsetReceiver.isConnected()) {
    		if(LogSettings.MARKET) {
    			MktLog.w(TAG, "setAudioRoute(), trying to set route to Speaker, while headset connected, aborting");
    		}
    		route = ROUTE_INTERNAL;
    	}
    	
    	mAudioRoute = route;

    	mWrapperPJSIP.PJSIP_pjsip_set_no_snd_dev("RCMediaManager");

    	switch (route) {
		case ROUTE_INTERNAL :
			mAudioManager.setSpeakerphoneOn(false);
			mAudioManager.setMicrophoneMute(false);			
			mBluetoothManager.setBluetoothOn(false);
			AudioDevice.updateAECDelay(ROUTE_INTERNAL);
			break;
		case ROUTE_SPEAKER :
			mBluetoothManager.setBluetoothOn(false);
			mAudioManager.setSpeakerphoneOn(true);
			mAudioManager.setMicrophoneMute(false);
			AudioDevice.updateAECDelay(ROUTE_SPEAKER);
			break;
		case ROUTE_BLUETOOTH :	
			mAudioManager.setSpeakerphoneOn(false);
			mAudioManager.setMicrophoneMute(false);
			mBluetoothManager.setBluetoothOn(true);		
			AudioDevice.updateAECDelay(ROUTE_BLUETOOTH);
			break;
		}    
    	
    	setAudioMode(true);
    	
    	if(!stop) {
    		mWrapperPJSIP.PJSIP_pjsip_set_snd_dev("RCMediaManager");
    	}
    	
    	notifyRouteChanged();    	    	    	
    }
    
    private void setAudioMode(boolean inCall) {    	    	
    	final int manufacturerCode = Compatibility.getManufacturerCode();
    	final int apiLevel = Compatibility.getApiLevel();
    	
    	if(LogSettings.MARKET){
    		MktLog.d(TAG, "Settings audio mode inCall : " + inCall + ", manufacturerCode : " + manufacturerCode + ", apiLevel : " + apiLevel);    		    		
    	}
    	
    	if(apiLevel < 11) {
	    	if(manufacturerCode == Compatibility.LGE) {
	    		mAudioManager.setMode(AudioManager.MODE_NORMAL);
	    	} else if (manufacturerCode == Compatibility.MOTOROLA) {
	    		mAudioManager.setMode(AudioManager.MODE_NORMAL);
	    	} else if ((manufacturerCode == Compatibility.SONY_ERR)&&(apiLevel < 8)) {
	    		mAudioManager.setMode(inCall ? AudioManager.MODE_IN_CALL : AudioManager.MODE_NORMAL);
	    	} else {
	    		//first is needed to reset audio stack, heavy operation, but it really forces audio on modern devices
	    		mAudioManager.setMode(AudioManager.MODE_IN_CALL);
	    		mAudioManager.setMode(AudioManager.MODE_NORMAL);    		
	    	}
    	} else {
    		mAudioManager.setMode(inCall ? API_11_AUDIO_MANAGER_MODE_IN_COMMUNICATION : AudioManager.MODE_NORMAL);
    	}
    }    
    
    
    private synchronized void notifyRouteChanged() {
    	 
    	final AudioState audioState = getAudioState();
    	  
    	if(LogSettings.MARKET) { 
            MktLog.d(TAG, "notifyRouteChanged(), audioState : " + audioState);
        }

    	try {    			
    		if(mAudioChangedCallback != null) {    			
				mAudioChangedCallback.on_audio_route_changed(audioState);
    		}
		} catch (RemoteException e) {
			if(LogSettings.MARKET) {
				MktLog.e(TAG, "notifyRouteChanged(), remote exception", e);
			}
		} catch (Throwable t) {
			if(LogSettings.MARKET) {
				MktLog.e(TAG, "notifyRouteChanged(), general callback notification exception", t);
			}
		}
    }            
    
    private int computeAudioRoute() {
    	if(mAudioManager.isSpeakerphoneOn()){
    		return ROUTE_SPEAKER;
    	} else if(mBluetoothManager.bluetoothHeadsetConnected() && mBluetoothManager.audioConnected()) {
    		return ROUTE_BLUETOOTH;
    	} else if (mHeadsetReceiver.isConnected()){
    		return ROUTE_INTERNAL;
    	}
    	return ROUTE_INTERNAL;
    }          
   
    
    public static final String getRouteTag(int iRoute) {
    	if(iRoute == ROUTE_INTERNAL) {
    		return "ROUTE_INTERNAL";
    	} else if(iRoute == ROUTE_SPEAKER) {
    		return "ROUTE_SPEAKER";
    	} else if(iRoute == ROUTE_BLUETOOTH) {
    		return "ROUTE_BLUETOOTH";
    	} else {
    		return "ROUTE_UNDEFINED";
    	}
    }
    
    public static final String getModeTag(int iMode) {
    	if (iMode == MODE_DETACHED) {
    		return "MODE_DETACHED";
    	} else if (iMode == MODE_IN_CALL) {
    		return "MODE_IN_CALL";
    	} else if (iMode == MODE_TEST_INTERNAL) {
    		return "MODE_TEST_INTERNAL";
    	} else if (iMode == MODE_TEST_SPEAKER) {
    		return "MODE_TEST_SPEAKER";
    	}
    	return "MODE_UNDEFINED";
    }
    
	private class HeadsetStateReceiver extends BroadcastReceiver {
		private static final String EXTRA_HEADSET_STATE = "state";
    	private static final String EXTRA_HEADSET_NAME = "name";
    	private static final String EXTRA_HEADSET_MIC = "microphone";
    	
    	private boolean mConnected;
    	
    	private boolean isConnected() {
    		return mConnected;
    	}

		@Override
		public void onReceive(Context context, Intent intent) {
			if(Intent.ACTION_HEADSET_PLUG.equals(intent.getAction())) {
				final int headsetState = intent.getIntExtra(EXTRA_HEADSET_STATE, 0);
				final String headsetName = intent.getStringExtra(EXTRA_HEADSET_NAME);
				final int headsetMic = intent.getIntExtra(EXTRA_HEADSET_MIC, 1);

				mConnected = (headsetState == 1); 
				
				notifyHeadsetRouteChanged(mConnected);
				
				if(LogSettings.MARKET) {
					MktLog.i(TAG, "HeadsetStateReceiver(), name : " + headsetName + 
							(headsetState == 0 ? ", disconnected" : ", connected") + 
							(headsetMic == 0 ? ", without mic" : ", has mic"));
				}
			}
		}
	}
}
 