/** 
 * Copyright (C) 2010-2011 RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.utils;

import java.util.Locale;

import android.content.Context;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.utils.AppInfo;
import com.rcbase.android.logging.utils.SystemInfoCollector;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.R;

public class RCClientInfoCollector {
    private static final String TAG = "[RC] RCClientInfoCollector";
    final static String contextIsNull = "error:context is null";
    
    enum attributeId {
        APPLICATION_VERSION,
        DEVICE_INFO_OS,
        DEVICE_INFO_MODEL,
        OS_VERSION,
        PROGRAM_BUILD,
        LOCALE,
        MANUFACTURE,
        MANUFACTURE_BRAND,
        UNIQUE_ID,
        CARRIER, 
        APPLICATION_ID        
    }
    
    public static String getEncryptedPhoneData(Context context){        
        RCJSONStringBuilder st = new RCJSONStringBuilder();
        try {
            if (null != st) {
                String temp = null;

                temp = getAttribute(attributeId.APPLICATION_VERSION, context);
                if (null != temp) {
                    st.setApplicationVersion(temp);
                }

                temp = getAttribute(attributeId.DEVICE_INFO_OS, context);
                if (null != temp) {
                    st.setOperationSystem(temp);
                }

                temp = getAttribute(attributeId.OS_VERSION, context);
                if (null != temp) {
                    st.setOperationSystemVersion(temp);
                }

                temp = getAttribute(attributeId.DEVICE_INFO_MODEL, context);
                if (null != temp) {
                    st.setDeviceModel(temp);
                }

                temp = getAttribute(attributeId.LOCALE, context);
                if (null != temp) {
                    st.setLocale(temp);
                }

                temp = getAttribute(attributeId.MANUFACTURE, context);
                if (null != temp) {
                    st.setManufacturer(temp);
                }

                temp = getAttribute(attributeId.MANUFACTURE_BRAND, context);
                if (null != temp) {
                    st.setDeviceBrand(temp);
                }

                temp = getAttribute(attributeId.UNIQUE_ID, context);
                if (null != temp) {
                    st.setUniqueIdOfDevice(temp);
                }

                temp = getAttribute(attributeId.CARRIER, context);
                if (null != temp) {
                    st.setCarrier(temp);
                }

                temp = getAttribute(attributeId.APPLICATION_ID, context);
                if (null != temp) {
                    st.setApplicationId(temp);
                }

                temp = getAttribute(attributeId.APPLICATION_VERSION, context);
                if (null != temp) {
                    st.setApplicationVersion(temp);
                }

                temp = getAttribute(attributeId.PROGRAM_BUILD, context);
                if (null != temp) {
                    st.setProgrammBuild(temp);
                }
                if(LogSettings.ENGINEERING) {
                    EngLog.e(TAG, st.getJsonString());
                }
            }
        } catch (Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, " ", th);
            }
        }
        String result = null;
        try {
            if (null != st) {
                result = RCRandomEncryption.RCSPCreateRandomEncryptedString(st.getJsonString(), 234, 34, 902);
            }
        } catch (Throwable sthr) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, " ", sthr);
            }
        }
        return result;
    }
    
    static String getAttribute(attributeId interestingAttribute, Context context) {
        
        String result = null;

        if (null != interestingAttribute) {
            try{
            switch (interestingAttribute) {
            case APPLICATION_VERSION:
                if(null != context){
                    result = AppInfo.getBuildVersion(context, false);
                } else {
                    result = contextIsNull;
                }
                break;
            case DEVICE_INFO_MODEL:
                result = Build.MODEL;
                break;
            case DEVICE_INFO_OS:
                result = "Android";
                break;
            case LOCALE:
                result = Locale.getDefault().toString();
                break;
            case MANUFACTURE:
                result = Build.BRAND;
                break;
            case MANUFACTURE_BRAND:
                result = SystemInfoCollector.getManufacturerName();
                break;
            case OS_VERSION:
                result = Build.VERSION.RELEASE;
                break;
            case UNIQUE_ID:
                    try {
                        result = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
                    } catch (Throwable th) {
                        if(LogSettings.MARKET) {
                            MktLog.w(TAG, " ", th);
                        }
                    }
                break;
            case CARRIER:
                if (null != context){
                    result = ((TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE)).getSimOperator(); 
                } else {
                    result = contextIsNull;
                }
                break;
            case APPLICATION_ID:
                result = " " + BUILD.BRAND;
                break;
            case PROGRAM_BUILD:
                if(null != context) {
                    result = context.getString(R.string.svn_revision);
                } else {    
                    result = contextIsNull;
                }
                break;
                
            default:
                break;
            }
            } catch (Throwable th) {
                if(LogSettings.ENGINEERING) {
                    EngLog.e(TAG, " ", th);
                }
            }
            
        }

        return result;
    }
}
