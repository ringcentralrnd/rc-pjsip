/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.utils.media;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RingCentralApp;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;

public class AudioFocus3 extends AudioFocusWrapper {
    private static final String TAG = "[RC]AudioFocus3";
    
    final static String RESTARTAT_MUSIC_ACTION = "com.android.music.musicservicecommand.togglepause";
    final static String PAUSE_MUSIC_ACTION = "com.android.music.musicservicecommand.pause";
	
	private AudioManager audioManager;
	
	private boolean isMusicActive = false;
	private boolean isFocused = false;
	
	public void init(AudioManager manager) {
		audioManager = manager;
	}

	@Override
	public synchronized void focus() {
        if (LogSettings.MARKET) {
            MktLog.w(TAG, " focus, isFocused " + isFocused + " isMusicActive " + audioManager.isMusicActive());
        }
        Context context = RingCentralApp.getContextRC();
        if (!isFocused && null != context) {
            pauseMusic(context);
            isFocused = true;
        }
	}
	
	@Override
	public synchronized void unFocus() {
	    if (LogSettings.MARKET) {
            MktLog.w(TAG, " focus, isFocused " + isFocused + " isMusicActive " + audioManager.isMusicActive());
        }
        
	    Context context = RingCentralApp.getContextRC();
        if (isFocused && null != context) {
            restartMusic(context);
            isFocused = false;
        }
	}

	private synchronized void pauseMusic(Context ctxt) {
	    isMusicActive = audioManager.isMusicActive();
	    EngLog.e(TAG, " pauseMusic, isMusicActive " + isMusicActive);
		if(isMusicActive && null != ctxt) {
			ctxt.sendBroadcast(new Intent(PAUSE_MUSIC_ACTION));
			EngLog.e(TAG, "pause intent is sent");
		}
	}
	
	private synchronized void restartMusic(Context ctxt) {
	    EngLog.e(TAG, "restartMusic, isMusicActive" + isMusicActive);
	    if(isMusicActive && null != ctxt) {
		    ctxt.sendBroadcast(new Intent(RESTARTAT_MUSIC_ACTION));
		    EngLog.e(TAG, "restart intent is sent");
		}
	}
}
