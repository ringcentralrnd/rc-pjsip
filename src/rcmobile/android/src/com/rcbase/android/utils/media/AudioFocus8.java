/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.utils.media;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.utils.Compatibility;
import com.ringcentral.android.LogSettings;

import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;

public class AudioFocus8 extends AudioFocusWrapper{
    final static String RESTARTAT_MUSIC_ACTION = "com.android.music.musicservicecommand.togglepause";
    final static String PAUSE_MUSIC_ACTION = "com.android.music.musicservicecommand.pause";
	
	protected static final String TAG = "[RC]AudioFocus 8";
	
	private boolean isFocused = false;
	
	private OnAudioFocusChangeListener focusChangedListener = new OnAudioFocusChangeListener() {	
		@Override
		public void onAudioFocusChange(int focusChange) {
		   // audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
		    if(LogSettings.MARKET) { 
		        MktLog.d(TAG, "Focus changed:" + focusChange + " isMusicActive:" + audioManager.isMusicActive());
		    }
		}
	};
	
	public void init(AudioManager manager) {
		audioManager = manager;
	}

	@Override
	public synchronized void focus() {
	    if (LogSettings.MARKET) {
            MktLog.w(TAG, " focus, isFocused " + isFocused + " isMusicActive " + audioManager.isMusicActive());
        }
		
		if(!isFocused) {
		    audioManager.requestAudioFocus(focusChangedListener, Compatibility.getInCallStream(), AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
			isFocused = true;
		}
	}
	
	@Override
	public synchronized void unFocus() {
	    if (LogSettings.MARKET) {
            MktLog.i(TAG, " unFocus, isFocused " + isFocused + " isMusicActive " + audioManager.isMusicActive());
        }
	    
	    if(isFocused) {
			audioManager.abandonAudioFocus(focusChangedListener);
			isFocused = false;
		}
	}
}
