package com.rcbase.android.utils.media;

import android.os.Parcel;
import android.os.Parcelable;

public class AudioState implements Parcelable {
	
	private Integer audioMode;
	
	private boolean isMute;	
	private boolean isSpeaker;
	private boolean isSpeakerEnabled;
	private boolean isBluetooth;
	private boolean isBluetoothEnabled;
		
	public AudioState() {			
	}		
	
	public AudioState(Parcel in) {
		readFromParcel(in);
	}
	
	/*package*/ Integer getAudioMode() {
		return audioMode;
	}

	/*package*/ void setAudioMode(Integer audioMode) {
		this.audioMode = audioMode;
	}

	/*package*/ void setMute(boolean isMute) {
		this.isMute = isMute;	
	}
	
	/*package*/ boolean isMute(){
		return isMute;
	}		
	
	public boolean isBluetooth() {
		return isBluetooth;
	}

	public void setBluetooth(boolean isBluetooth) {
		this.isBluetooth = isBluetooth;
	}
	
	public boolean isBluetoothEnabled() {
		return isBluetoothEnabled;
	}

	public void setBluetoothEnabled(boolean isBluetoothEnabled) {
		this.isBluetoothEnabled = isBluetoothEnabled;
	}		

	public boolean isSpeaker() {
		return isSpeaker;
	}	
	
	public void setSpeaker(boolean isSpeaker){
		this.isSpeaker = isSpeaker;
	}

	public boolean isSpeakerEnabled() {
		return isSpeakerEnabled;
	}

	public void setSpeakerEnabled(boolean isSpeakerEnabled) {
		this.isSpeakerEnabled = isSpeakerEnabled;
	}

	public boolean isEquals(AudioState audioState) {
		if (this == audioState)
			return true;
		if (audioState == null)
			return false;
		if (isBluetooth != audioState.isBluetooth)
			return false;
		if (isBluetoothEnabled != audioState.isBluetoothEnabled)
			return false;
		if (isSpeaker != audioState.isSpeaker)
			return false;
		if (isSpeakerEnabled != audioState.isSpeakerEnabled)
			return false;
		return true;
	}



	public static final Parcelable.Creator<AudioState> CREATOR = new Parcelable.Creator<AudioState>() {
		
		public AudioState createFromParcel( Parcel in ){
			return new AudioState(in);
		}
		
		public AudioState[] newArray( int size){
			return new AudioState[size];
		}
	};
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(isSpeaker ? 1 : 0);
		dest.writeInt(isSpeakerEnabled ? 1 : 0);
		dest.writeInt(isBluetooth ? 1 : 0);
		dest.writeInt(isBluetoothEnabled ? 1 : 0);
		
		
	}
	public void readFromParcel( Parcel in ){
		isSpeaker = (in.readInt() == 1);
		isSpeakerEnabled = (in.readInt() == 1);
		isBluetooth = (in.readInt() == 1);
		isBluetoothEnabled = (in.readInt() == 1);
	}
	
	@Override
	public String toString() {		
		return new StringBuilder().append("audioMode : ").append(audioMode)
								  .append(", isMute : ").append(isMute)
								  .append(", isBluetooth : ").append(isBluetooth)
								  .append(", isBluetoothEnabled : ").append(isBluetoothEnabled)
								  .append(", isSpeaker : ").append(isSpeaker)
								  .append(", isSpeakerEnabled : ").append(isSpeakerEnabled)
								  .toString();
	}

	
}
