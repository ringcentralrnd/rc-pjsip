/** 
 * Copyright (C) 2011-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.config;


/**
 * Defines brands identifiers, the names shall be synchronized with
 * brand/<brand-code-name>/brand.build.properties, variable build.brand.variable
 */
public class Brands {

    /**
     * RingCentral Mobile
     */
    public static final int RC_BRAND = 0;
    
    /**
     * Rogers Hosted IP Voice Mobile
     */
    public static final int ROGERS_BRAND = 1;

    /**
     * AT&T OfficeAtHand
     */
    public static final int ATT_BRAND = 2;

}
