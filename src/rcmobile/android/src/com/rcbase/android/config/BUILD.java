/** 
 * Copyright (C) 2011-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.config;

import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.provider.RCMProviderHelper;

/**
 * IMPORTANT NOTES: This file is stub for development purposes only, at brands building (final releases) via "brands_build.bat" the file
 * will be generated according to brand and build properties.  
 */
public class BUILD {
    public static final int BRAND = Brands.RC_BRAND;
    public static final String VERSION_NAME = "3.3";
    /** Market package (for rating) **/
    public static final String PACKAGE_NAME = "com.ringcentral.android";
    public static final String SVN_REVISION = "0";
    public static final int CFG_REVISION = 2;
    public static final String APP_FOLDER_NAME_ON_SDCARD = "rc";
    
    /**
     * VoIP section
     */
    public static final boolean VOIP_ENABLED = true;
    public static final boolean VOIP_ENABLED_BY_DEFAULT = true;
    public static final boolean VOIP_MOBILE_3G_4G_ENABLED = true;
    public static final boolean VOIP_MOBILE_3G_4G_ENABLED_BY_DEFAULT = false;
    public static final boolean VOIP_INCOMING_ENABLED = true;
    public static final boolean VOIP_INCOMING_ENABLED_BY_DEFAULT = false;
    
    
    /**
     * Outbound link will have next view URI.SIGNUP?ol={ENCRYPTED_DATA} - all about encrypted data you can find at: http://jira.dins.ru/browse/RC-34972
     * bellow is flag which rules inclusion of transfering data
     * and parameter data 
     */
    public static final boolean isOutboundLinkWithEncryptedDataTurnedOn = true;
    /**
     * Time interval to reminder user to proceed express setup,
     */
    public static final long SETUP_WIZARD_REMINDER_INTERVAL = (24 * 60 * 60 * 1000); // once in 24 hours
    
    /**
     * Time interval to reminder user about expiration
     */
    public static final long ACCOUNT_EXPIRATION_REMINDER_INTERVAL = (24 * 60 * 60 * 1000); // once in 24 hours
    /**
     * Tune interval in days for starting notification about account expirations 7 days by default
     */
    public static final int ACCOUNT_EXPIRE_REMINDER_START_DAYS = 7;
    /**
     * Android:scheme/appUrlScheme value for mobile web url
     */
    public static final String APP_URL_SCHEME = "rcmobile";
	/**
	 * TOS EULA
	 */ 
    public static final boolean isTosEULAEnabled = true;
	/**
	 * TOS 911
	 */ 
    public static final boolean isTos911Enabled = true;
    /**
     * Settings: Billing section
     */ 
    public static final boolean SETTINGS_BILLING_ENABLED = true;
    /**
     * Settings: Tell A Friend: "new" (Amarosa) Settings screen
     */ 
    public static final boolean SETTINGS_TELLFRIEND_ENABLED = true;    
    /**
     * Settings: Tell A Friend in the About Screen: "old" (pre-Amarosa)Settings screen
     */ 
    public static final boolean SETTINGS_LEGACY_ABOUT_TELLFRIEND_ENABLED = true;    
    /**
     * Enable/Disable splash screen on startup
     */
    public static final boolean SPLASH_SCREEN_ENABLED = false;
    /**
     * Splash screen lifetime in millis
     */
    public static final long SPLASH_SCREEN_TIMEOUT = 2000l;
    
    /**
     * Defines if traces to logcat enabled.
     */
    public static final boolean TRACES_TO_LOGCAT_ENABLED = true;
    
    public static final boolean PERFORMANCE_MEASUREMENT_ENABLED = false;
    
    /**
     * Enable internal configuration screen for URL's
     */    
    public static class CONFIG_URI {
        public static final String LAUNCH_KEY = "*3328433284#";
    }
    public static class FLURRY {
        public static final boolean ENABLE = false;
        public static final String APP_KEY = "UCTY6MWXKG47WYF6P7W8";
    }

    public static class ADMOB {
        public static final boolean ENABLE = false;
        public static final String APP_KEY = "";
    }
    
    public static class GETJAR {
        public static final boolean ENABLE = false;
    }
    
    public static class GANALYT {
    	public static final boolean ENABLE = false;
    	public static final String APP_KEY = "UA-31119-4";
    }
    
    /**
     * Deprecated, use getters from RCMConfig instead of direct use from Build.java
     */
    @Deprecated
    public static class URI {
        /** JEDI **/
    	@Deprecated
        //public static final String JEDI = "https://ws.ringcentral.com/ringcentral/API/";
    	public static final String JEDI ="http://japi.stage.ringcentral.com/ringcentral/API/";
    	/** SOAP **/
    	@Deprecated
        public static final String JEDI_SOAP_XNLNS = "http://service.ringcentral.com/";
    	//public static final String JEDI_SOAP_XNLNS = "http://service.stage.ringcentral.com";
        /** Messaging **/
    	@Deprecated
        public static final String MSGSYNC = "http://sync.stage.ringcentral.com/localstorage/msgmanager.dll";
        /** I'm a new user **/
    	@Deprecated
        public static final String SIGNUP = "http://m.ringcentral.com/";
    	/** Setup URL Overridden by server**/
    	@Deprecated
    	public static final boolean SETUP_URL_OVERRIDING_ENABLE = true;    	
        /** Setup **/
    	@Deprecated
        public static final String SETUP = "https://secure.ringcentral.com/rc-setup/mobile/";
        /** SignUp/Setup settings **/
    	@Deprecated
        public static final String SETUP_SIGNUP_SETTINGS = "https://secure.ringcentral.com/rc-setup/mobile/";
    	/** Base web settings URL **/
    	@Deprecated
    	//public static final String WEB_SETTINGS_BASE = "https://service.ringcentral.com/mobileweb/app.html";
        public static final String WEB_SETTINGS_BASE = "https://service.stage.ringcentral.com/mobileweb/app.html";
        /** HTTP Registration URL **/
    	@Deprecated
        public static final String HTTPREG = "http://agentconnect.stage.ringcentral.com/httpreg/rchttpreg.dll?Register";
    	@Deprecated
        public static final String URI_SIP_PROVIDER = "sip.stage.ringcentral.com";
    	//public static final String URI_SIP_PROVIDER = "10.32.51.31";
    	//public static final String URI_SIP_PROVIDER = "192.168.92.4";
    	//public static final String URI_SIP_PROVIDER = "10.32.51.25";
    	//public static final String URI_SIP_PROVIDER = RCMProviderHelper.getTSCEnalbe(RingCentralApp.getContextRC()) ? "192.168.92.4":"10.32.51.25";
    	
    	@Deprecated
        public static final String URI_SIP_OUTBOUND_PROXY = "sip.stage.ringcentral.com:5090";
    	//public static final String URI_SIP_OUTBOUND_PROXY = "10.32.51.25";
    	//public static final String URI_SIP_OUTBOUND_PROXY = "155.212.214.183:80";
    	//public static final String URI_SIP_OUTBOUND_PROXY = "192.168.92.4";
    	//public static final String URI_SIP_OUTBOUND_PROXY = RCMProviderHelper.getTSCEnalbe(RingCentralApp.getContextRC()) ? "192.168.92.4":"10.32.51.25";
    	
    }
}
