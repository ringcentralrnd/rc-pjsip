/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.audio;

import java.util.Vector;

public class AudioDeviceFrameQueue2  {
	private static final long serialVersionUID = 1L;
	private volatile Vector<AudioDeviceFrame> mQueue;
	private volatile int mCapacity;
	public AudioDeviceFrameQueue2(int capacity) {
	    mCapacity  = capacity;
	    mQueue = new Vector<AudioDeviceFrame>(capacity);
	}
	
	public boolean putFrame(AudioDeviceFrame frame){
		synchronized (mQueue) {
		    if (mQueue.size() >= mCapacity) {
		        return false;
		    }
		    mQueue.addElement(frame);
		    return true;
		}
	}
	
	public boolean isFull(){
	    return (mQueue.size() >= mCapacity);
	}
	
	private volatile long mDiscraded = 0;
	
	public long getDiscardedFrames() {
	    return mDiscraded;
	}
	
    public AudioDeviceFrame getFrame() {
        AudioDeviceFrame frame = null;
        try {
            synchronized (mQueue) {
                if (mQueue.size() > 0) {
                    frame = mQueue.firstElement();
                    mQueue.remove(0);
                } else {
                    return null;
                }
            }
        } catch (java.lang.Throwable th) {
        }
        return frame;
    }
	
    public AudioDeviceFrame getFrame2() {
        AudioDeviceFrame frame = null;
        try {
            synchronized (mQueue) {
                int s = mQueue.size();
                if (s > 0) {
                    frame = mQueue.firstElement();
                    mQueue.remove(0);
                    s--;
                    if (s > 0) {
                        mDiscraded += s;
                        mQueue.removeAllElements();
                    }
                } else {
                    return null;
                }
            }
        } catch (java.lang.Throwable th) {
        }
        return frame;
    }
}
