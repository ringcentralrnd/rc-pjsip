/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.audio;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.SystemClock;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.service.PJSIP;
import com.rcbase.android.utils.Compatibility;
import com.rcbase.android.utils.media.RCMediaManager;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.provider.RCMProviderHelper;

public class AudioDevice {
    public static final String sf_TAG = "[RC]AudioDevice";
    private static final boolean TIME_FRAMES_STATISTICS = true;
    private static final boolean USE_PRIVATE_ELAPSED_TIME = false;
    private static final int SCHEME_1_STARTS_DELAYS_FROM_FRAME = 0;
    
    /**
     * 4xAEC, 2 threads.
     * Record thread: mic->to_rtp, while (writeRTP(to_rtp)/ReadRTP(from_rtp), mic->to_rtp, AEC(to_rtp, from_rtp), send to playback via queue from_rtp)
     * Playback thread: waiting farmes from record thread and plays
     * 
     * Better than previous 4xAEC = 3 threads, but still sometimes "choppy".
     * 
     * Don't use for now. SCHEME_2 is more progressive
     */
    private static final int SCHEME_1 = 0;
    
    /**
     * 3xAEC, 2 threads.
     * Record thread: mic->to_rtp, while (writeRTP(to_rtp)/ReadRTP(from_rtp), mic->to_rtp, AEC(to_rtp, from_rtp), send to playback via queue from_rtp)
     * Playback thread: readFromRTP. send frame to record thread and plays frame
     * Record thread: mic. getting frame from playback, AEC, write to RTP
     */
    private static final int SCHEME_2 = 1;
    private static final int SCHEME = SCHEME_2;
    private static final int PLAYBACK_BUFFER_SIZE_FRAMES = 10;
    private static final int SCHEME2_RTP_FRAMES_BUFFER_SIZE = 8;
    private static final boolean SCHEME_2_ALSA_BEHAVIOR = false;
    static private boolean s_echo_cancelation = true;
    static private boolean isTestMode = false;
    static private int mAECDelay = RCMConstants.DEFAULT_AEC_DELAY_VALUE;
    private final BroadcastReceiver echo_canceler_state_receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (null == intent) {
                if (LogSettings.MARKET) {
                    MktLog.e(sf_TAG, " echo_canceler_state_receiver, intent is null ");
                }
                return;
            }

            final boolean container = intent.getBooleanExtra(RCMConstants.EXTRA_VOIP_ECHO_CANCELATION_STATE_CHANGED,
                    s_echo_cancelation);
            if (LogSettings.MARKET) {
                MktLog.w(sf_TAG, "echo_canceler_state_receiver, s_echo_cancelation new value is " + container + "; was "
                        + s_echo_cancelation);
            }
            s_echo_cancelation = container;
        }
    };
    
	private static AudioDevice s_instance = null;
	private static final Object sLock = new Object();
    static boolean create(int clock_rate, int samples_per_frame, int bits_per_sample, int channel_count, long rc_audio_params) {
        synchronized (sLock) {
            if (s_instance != null) {
                if(LogSettings.MARKET) {
                    MktLog.e(sf_TAG, "create: previous device was not finished");
                }
                try {
                    s_instance.destroyStream();
                } catch (java.lang.Throwable th) {
                }
            }
            s_instance = new AudioDevice();
            if (s_instance != null) {
                return s_instance.createStream(clock_rate, samples_per_frame, bits_per_sample, channel_count, rc_audio_params);
            } else {
                return false;
            }            
        }
    }

    static boolean start() {
        synchronized (sLock) {
            if (s_instance != null) {
                return s_instance.startStream();
            } else {
                if (LogSettings.MARKET) {
                    MktLog.w(sf_TAG, "start() called : no AudioDevice");
                }
                return false;
            }
        }
    }

    static boolean stop() {
        synchronized (sLock) {
            if (s_instance != null) {
                return s_instance.stopStream();
            } else {
                if (LogSettings.MARKET) {
                    MktLog.w(sf_TAG, "stop() called : no AudioDevice");
                }
                return false;
            }
        }
    }

    static boolean destroy() {
        synchronized (sLock) {
            if (s_instance != null) {
                s_instance.destroyStream();
                s_instance = null;
            } else {
                if (LogSettings.MARKET) {
                    MktLog.w(sf_TAG, "destroy() called : no AudioDevice");
                }
                return false;
            }
        }
        return true;
    }
		
	/**
	 * Build.VERSION_CODES.ECLAIR declared from API 5, Build.VERSION_CODES.ECLAIR value is 5 (2.1)
	 */
    private static final int Build_VERSION_CODES_ECLAIR =  5;
    
    /**
     * AudioFormat.CHANNEL_OUT_MONO declared from API 5 (Eclair 2.1) value is 4
     */
    private static final int AudioFormat_CHANNEL_OUT_MONO =  4;
    
    /**
     * AudioFormat.CHANNEL_IN_MONOdeclared from API 5 (Eclair 2.1) value is 4
     */
    private static final int AudioFormat_CHANNEL_IN_MONO =  16;
    
    /***************************************************************************************************************************************
     * 
     ***************************************************************************************************************************************/
    private volatile boolean mActive;
    private volatile boolean mDestroyCalled;
    private volatile AudioDeviceFrameQueue mPlayBackQueue = null;
    private volatile AudioDeviceFrameQueue2 mFromRTPQueue = null;
    
    private volatile AudioDeviceFrameCache mFrameCache = null;
    private volatile AudioRecord mAudioRecord = null;
    private volatile AudioTrack mAudioTrack = null;
    private volatile RecordWorkingThread mRecordThread = null;
    private volatile boolean mRecordThreadActive = false;
    private volatile PlaybackWorkingThread mPlaybackThread = null;
    private volatile boolean mPlayBackThreadActive = false;
    
    private volatile int mClockRate        = 0; 
    private volatile int mSamplesPerFrame  = 0; 
    private volatile int mBitsPerSample    = 0;
    private volatile int mBytesPerFrame    = 0;
    private volatile int mChannelCount     = 1;
    private volatile long mRcAudioParams   = 0;
    
    private volatile int mPlayChannelCfg   = 0;
    private volatile int mRecChannelCfg    = 0;
    private volatile int mAudioFormat      = 0;
    
    private volatile int mDefaultRecMinBuffer = 0;
    private volatile int mRecBufferSize = 0;

    private volatile int mDefaultPlayMinBuffer = 0;
    private volatile int mPlayBufferSize = 0;

    private volatile long mRTPErrors = 0;
    private volatile long mReadRTPErrors = 0;
    private volatile long mWriteRTPErrors = 0;
    private volatile long mPlayQueueFulls = 0;
    private volatile long mAecFrames = 0;
    private volatile long mRTPQueueFull = 0;
    private volatile long mMissedRTPFrames = 0;
    private volatile long mScheme2FromRTPDiscardedFrames = 0;
    
    private volatile long mReadCounter = 0;
    private volatile long mWriteCounter = 0;
    private volatile long mReadWriteCounter = 0;
    
    private volatile long mFrameCounter = 0;
    private volatile long mPlayBackFramesDiscarded = 0;
    
    private volatile long mPlayFrameCounter = 0;
    
    private volatile long mMicLessBytesErrors = 0;
    
    private volatile long mFrameMinTime = 0;
    private volatile long mFrameMaxTime = 0;
    private volatile long mFrameAverageTime = 0;

    private volatile long mMaxWait = 0;
    private volatile long mDelay   = 0;
    private volatile long mDelays  = 0;
    
    private volatile long mFrame20  = 0;
    private volatile long mFrame30  = 0;
    private volatile long mFrame50  = 0;
    private volatile long mFrame100 = 0;
    private volatile long mFrame101above = 0;

    private RecordWav recordWav = new RecordWav();
    
    public AudioDevice() {
        mActive = true;
        mDestroyCalled = false;
    }

    private static final int MIN_AUDIO_REC_BUFFER_SIZE_MIN_IN_FRAMES  = 16;
    private static final int MIN_AUDIO_REC_BUFFER_SIZE = 4096;
    private static final boolean MIC_BUFFER_SCHEME_1 = true;
    private boolean createStream(int clock_rate, int samples_per_frame, int bits_per_sample, int channel_count, long rc_audio_params) {
        if (LogSettings.MARKET) {
            MktLog.d(sf_TAG, "createStream(): clock rate:" + clock_rate + " smp/frame:" + samples_per_frame + " bits/smp:"
                    + bits_per_sample);
        }
        mClockRate = clock_rate;
        mSamplesPerFrame = samples_per_frame;
        mBitsPerSample = bits_per_sample;
        mBytesPerFrame = samples_per_frame * (mBitsPerSample / 8);
        mChannelCount = channel_count;
        mRcAudioParams = rc_audio_params;
		int sdkInt = Compatibility.getApiLevel();
        mPlayChannelCfg = (sdkInt >= Build_VERSION_CODES_ECLAIR ? AudioFormat_CHANNEL_OUT_MONO
                : AudioFormat.CHANNEL_CONFIGURATION_MONO);
        mRecChannelCfg = (sdkInt >= Build_VERSION_CODES_ECLAIR ? AudioFormat_CHANNEL_IN_MONO
                : AudioFormat.CHANNEL_CONFIGURATION_MONO);
        mAudioFormat = (bits_per_sample == 8 ? AudioFormat.ENCODING_PCM_8BIT : AudioFormat.ENCODING_PCM_16BIT);

        /**********************************************************************************************************************************/
        mDefaultRecMinBuffer = AudioRecord.getMinBufferSize(mClockRate, mRecChannelCfg, mAudioFormat );
		if( mDefaultRecMinBuffer <= 0 ){
			if( mDefaultRecMinBuffer == AudioRecord.ERROR_BAD_VALUE ){
				if (LogSettings.MARKET) {
			        MktLog.e(sf_TAG, "AudioRecord.getMinBufferSize failed. Result ERROR_BAD_VALUE" );
			    }
			}
			else if(mDefaultRecMinBuffer == AudioRecord.ERROR ){
				if (LogSettings.MARKET) {
			        MktLog.e(sf_TAG, "AudioRecord.getMinBufferSize failed. Result ERROR" );
			    }
			}
			destroyStream();
			return false;
		}

		mRecBufferSize = mDefaultRecMinBuffer;
		
        if (MIC_BUFFER_SCHEME_1) {
            int minRecSize = MIN_AUDIO_REC_BUFFER_SIZE_MIN_IN_FRAMES * mBytesPerFrame;
            if (mDefaultRecMinBuffer < minRecSize) {
                mRecBufferSize = minRecSize;
            }

            if (mRecBufferSize % mBytesPerFrame != 0) {
                mRecBufferSize = ((mRecBufferSize / mBytesPerFrame) + 1) * mBytesPerFrame;
            }
        } else {
            if (mRecBufferSize <= MIN_AUDIO_REC_BUFFER_SIZE) {
                mRecBufferSize = 4096 * 3 / 2;
            }
            int frameSizeInBytes = (mBitsPerSample == 8) ? 1 : 2;
            if (mRecBufferSize % frameSizeInBytes != 0) {
                mRecBufferSize++;
            }
        }

        /**********************************************************************************************************************************/
        mDefaultPlayMinBuffer = AudioTrack.getMinBufferSize(clock_rate, mPlayChannelCfg, mAudioFormat);
		if( mDefaultPlayMinBuffer <= 0 ){
			if( mDefaultPlayMinBuffer == AudioTrack.ERROR_BAD_VALUE ){
				if (LogSettings.MARKET) {
			        MktLog.e(sf_TAG, "AudioTrack.getMinBufferSize failed. Result ERROR_BAD_VALUE" );
			    }
			}
			if( mDefaultPlayMinBuffer == AudioTrack.ERROR ){
				if (LogSettings.MARKET) {
			        MktLog.e(sf_TAG, "AudioTrack.getMinBufferSize failed. Result ERROR" );
			    }
			}
			destroyStream();
			return false;
		}
		
		mPlayBufferSize = mDefaultPlayMinBuffer;
        if ((mPlayBufferSize % mBytesPerFrame) != 0) {
            mPlayBufferSize = ((mPlayBufferSize / mBytesPerFrame) + 1) * mBytesPerFrame;
        }
		
		/**********************************************************************************************************************************/
        try {
            mAudioTrack = new AudioTrack(AudioManager.STREAM_VOICE_CALL, clock_rate, mPlayChannelCfg, mAudioFormat,
                    mPlayBufferSize, AudioTrack.MODE_STREAM);
        } catch (IllegalArgumentException e_ia) {
            if (LogSettings.MARKET) {
                MktLog.e(sf_TAG, "Could not create AudioTrack. " + e_ia.getMessage());
            }
            destroyStream();
            return false;
        }
        int playerState = mAudioTrack.getState();
        if (playerState != AudioTrack.STATE_INITIALIZED) {
            if (LogSettings.MARKET) {
                MktLog.e(sf_TAG, "AudioTrack is not initialized");
            }
            destroyStream();
            return false;
        }
		
        /**********************************************************************************************************************************/
        int recorderState = AudioRecord.STATE_UNINITIALIZED;
        boolean useMicRecorder = false;
        if (sdkInt >= 10) {
            if (LogSettings.MARKET) {
                MktLog.i(sf_TAG, "Create AudioRecord for VOICE_COMMUNICATION mode");
            }
            try {
                mAudioRecord = new AudioRecord(7, // VOICE_COMMUNICATION
                        clock_rate, mRecChannelCfg, mAudioFormat, mRecBufferSize);
            } catch (java.lang.Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.w(sf_TAG, "Could not create AudioRecord for VOICE_COMMUNICATION mode: " + th.toString());

                }
                useMicRecorder = true;
            }
            if (mAudioRecord == null) {
                useMicRecorder = true;
            }
            if (!useMicRecorder) {
                try {
                    recorderState = mAudioRecord.getState();
                } catch (java.lang.Throwable th) {
                    useMicRecorder = true;
                }
                if ((recorderState != AudioRecord.STATE_INITIALIZED) || useMicRecorder) {
                    if (LogSettings.MARKET) {
                        MktLog.w(sf_TAG, "AudioRecord is not initialized for VOICE_COMMUNICATION mode. Try MIC mode");
                    }
                    useMicRecorder = true;
                    recorderState = AudioRecord.STATE_UNINITIALIZED;
                }
            }
        } else {
            useMicRecorder = true;
        }
		
        if (useMicRecorder) {
            if (LogSettings.MARKET) {
                MktLog.i(sf_TAG, "Create AudioRecord for MIC mode");
            }
            try {
                mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, clock_rate, mRecChannelCfg, mAudioFormat,
                        mRecBufferSize);
            } catch (IllegalArgumentException e_ia) {
                if (LogSettings.MARKET) {
                    MktLog.e(sf_TAG, "Could not create AudioRecord. " + e_ia.getMessage());
                }

                destroyStream();
                return false;
            }
        }
        
        /**********************************************************************************************************************************/
		recorderState = mAudioRecord.getState();
		if( recorderState != AudioRecord.STATE_INITIALIZED ){
			if (LogSettings.MARKET) {
		        MktLog.e(sf_TAG, "AudioRecord is not initialized" );
		    }

			destroyStream();
			return false;
		}
		/**********************************************************************************************************************************/
		if(LogSettings.MARKET) {
            MktLog.d(sf_TAG, "register echo_canceler_state_receiver" );
        }
		
		s_echo_cancelation = RCMProviderHelper.getDeviceEchoState(RingCentralApp.getContextRC());
		if(null != echo_canceler_state_receiver) {
		    IntentFilter intentFilterEchoCancelationState = new IntentFilter(RCMConstants.ACTION_VOIP_ECHO_CANCELATION_STATE_CHANGED);
            RingCentralApp.getContextRC().registerReceiver(echo_canceler_state_receiver, intentFilterEchoCancelationState);
        }
		return true;
	}

	/**********************************************************************************************************************************/
	private volatile Object mLock = new Object(); 
	private static final long MAX_THREADS_WAITING_TIME = 3000;
	private static final long THREADS_WAITING_TIME_STEP = 1000;
	private void waitingFinishing() {
	    synchronized (mLock) {
	        long start = SystemClock.elapsedRealtime();
	        while (mPlayBackThreadActive || mRecordThreadActive) {
	            if ((SystemClock.elapsedRealtime() - start ) >= MAX_THREADS_WAITING_TIME) {
	                if(LogSettings.MARKET) {
	                    MktLog.e(sf_TAG, "Waiting threads completion timeout" );
	                }
                    return;
                }
	            try {
	                mLock.wait(THREADS_WAITING_TIME_STEP);
	            } catch (java.lang.Throwable th) {
	            }
	        }
	    }
	}
	private void onPlayBackThreadFinish() {
	    synchronized (mLock) {
	        mPlayBackThreadActive = false;
	        mLock.notifyAll();
	    }
	}
	private void onRecordThreadFinish() {
        synchronized (mLock) {
            mRecordThreadActive = false;
            mLock.notifyAll();
        }
	}
	

    /**********************************************************************************************************************************/
    private boolean startStream() {
        if (mDestroyCalled) {
            if (LogSettings.MARKET) {
                MktLog.e(sf_TAG, "startStream() called after destroy");
            }
            return false;
        }
        
        if (mActive) {
            if (mRecordThreadActive || mPlayBackThreadActive) {
                if (LogSettings.MARKET) {
                    MktLog.e(sf_TAG, "startStream() called when already active");
                }
                return false;
            }
        } else {
            if (mRecordThreadActive || mPlayBackThreadActive) {
                if (LogSettings.MARKET) {
                    MktLog.e(sf_TAG, "startStream() called : active state : however treands are not finished");
                }
                return false;
            }
        }
        if (LogSettings.MARKET) {
            MktLog.i(sf_TAG, "startStream() starting" );
        }

        mPlayBackQueue = new AudioDeviceFrameQueue(PLAYBACK_BUFFER_SIZE_FRAMES);
        mFromRTPQueue = new AudioDeviceFrameQueue2(SCHEME2_RTP_FRAMES_BUFFER_SIZE);
        mFrameCache = new AudioDeviceFrameCache();
        
        mRecordThread = new RecordWorkingThread(this);
        mRecordThreadActive = true;
        mRecordThread.start();
        
        mPlaybackThread = new PlaybackWorkingThread(this);
        mPlayBackThreadActive = true;
        mPlaybackThread.start();

        if (LogSettings.MARKET) {
            MktLog.i(sf_TAG, "startStream() finished" );
        }
        return true;
    }
    
	/**********************************************************************************************************************************/
	private boolean stopStream(){
        if (!mActive) {
            return true;
        }
        if (LogSettings.MARKET) {
            MktLog.i(sf_TAG, "stopStream() starting");
        }
        mActive = false;		
		AudioDeviceFrameQueue q = mPlayBackQueue;
		if (q != null) {
		    q.stop();
		}
		
        AudioRecord r = mAudioRecord;
        if (r != null) {
            try {
                r.stop();
            } catch (IllegalStateException e_is) {
            }
        }
        
        AudioTrack a = mAudioTrack;
        if (a != null) {
            try {
                a.stop();
            } catch (IllegalStateException e_is) {
            }
        }
        
        waitingFinishing();
		if (LogSettings.MARKET) {
	        MktLog.i(sf_TAG, "stopStream() finished" );
	    }
		return true;
	}
	
	/**********************************************************************************************************************************/
	private boolean destroyStream() {
        if (mDestroyCalled) {
            if (LogSettings.MARKET) {
                MktLog.w(sf_TAG, "destroy() called : when the audio device is marked as destroyed");
            }
            return true;
        }
        mDestroyCalled = true;
		if (LogSettings.MARKET) {
	        MktLog.i(sf_TAG, "destroyStream() starting" );
	    }
		if(null != echo_canceler_state_receiver) {
		    if(LogSettings.MARKET) {
		        MktLog.d(sf_TAG, "unregister echo_canceler_state_receiver" );
		    }
		    try {
		        RingCentralApp.getContextRC().unregisterReceiver(echo_canceler_state_receiver);
		    } catch (java.lang.Throwable th) {
		        if(LogSettings.MARKET) {
	                MktLog.w(sf_TAG, "unregister echo_canceler_state_receiver:error:" + th.toString());
	            }
		    }
		}
		stopStream();
		
        try {
            if (mAudioRecord != null) {
                mAudioRecord.release();
                mAudioTrack = null;
            }
        } catch (java.lang.Throwable th) {
            mAudioTrack = null;
        }

        try {
            if (mAudioTrack != null) {
                mAudioTrack.release();
                mAudioTrack = null;
            }
        } catch (java.lang.Throwable th) {
            mAudioTrack = null;
        }
        
        dump(this);

		if (LogSettings.MARKET) {
	        MktLog.i(sf_TAG, "destroyStream() finished" );
	    }
		return true;
	}
	
	/**********************************************************************************************************************************/
    private void dump(AudioDevice d) {
        if (LogSettings.MARKET) {
            if (d == null) {
                return;
            }

            MktLog.d(sf_TAG, "AudioDevice dump:");
            MktLog.d(sf_TAG, "  Clock: " + mClockRate + " ChannelCount: " + mChannelCount + " AudioFormat: " + mAudioFormat);
            MktLog.d(sf_TAG, "  SamplesPerFrame: " + mSamplesPerFrame + " BitsPerSample: " + mBitsPerSample + " BytesPerFrame: "
                    + mBytesPerFrame);
            MktLog.d(sf_TAG, " Play:   ChannelCfg: " + mPlayChannelCfg + " DefaultMinBuffer: " + mDefaultPlayMinBuffer
                    + " Buffer: " + mPlayBufferSize);
            MktLog.d(sf_TAG, " Record: ChannelCfg: " + mRecChannelCfg + " DefaultMinBuffer: " + mDefaultRecMinBuffer
                    + " Buffer: " + mRecBufferSize);

            if (SCHEME == SCHEME_1) {
                MktLog.d(sf_TAG, "  RTP:");
                MktLog.d(sf_TAG, "    RTPErrors       : " + mRTPErrors);
                MktLog.d(sf_TAG, "    ReadRTPErrors   : " + mReadRTPErrors);
                MktLog.d(sf_TAG, "    WriteRTPErrors  : " + mWriteRTPErrors);
                MktLog.d(sf_TAG, "    PlayQueueFulls  : " + mPlayQueueFulls);
                if (mChannelCount != 0 && mSamplesPerFrame != 0) {
                    MktLog.d(sf_TAG, "    RWCounter       : " + mReadWriteCounter + " ("
                            + (mReadWriteCounter / (mSamplesPerFrame / mChannelCount)) + ")");
                }
                MktLog.d(sf_TAG, "    FrameCounter    : " + mFrameCounter);
                MktLog.d(sf_TAG, "    PlayedFrames    : " + mPlayFrameCounter);
                MktLog.d(sf_TAG, "    MicLessBytesErr : " + mMicLessBytesErrors);
                MktLog.d(sf_TAG, "    FrameMinTime    : " + mFrameMinTime);
                MktLog.d(sf_TAG, "    FrameMaxTime    : " + mFrameMaxTime);
                MktLog.d(sf_TAG, "    FrameAverageTime: " + mFrameAverageTime);
                MktLog.d(sf_TAG, "    Delays          : " + mDelays);
                MktLog.d(sf_TAG, "    Delay total     : " + mDelay);
                MktLog.d(sf_TAG, "    Max. delay      : " + mMaxWait);

                if (TIME_FRAMES_STATISTICS) {
                    MktLog.d(sf_TAG, "    Frame times:");
                    MktLog.d(sf_TAG, "      < 21   : " + mFrame20);
                    MktLog.d(sf_TAG, "      21-30  : " + mFrame30);
                    MktLog.d(sf_TAG, "      31-50  : " + mFrame50);
                    MktLog.d(sf_TAG, "      51-100 : " + mFrame100);
                    MktLog.d(sf_TAG, "      100 >  : " + mFrame101above);
                }

                MktLog.d(sf_TAG, "  Frames cache:");
                MktLog.d(sf_TAG, "    New arrays : " + mFrameCache.getNewFrames());
                MktLog.d(sf_TAG, "    Put/Full   : " + mFrameCache.getPutFrameFull());

            } else if (SCHEME == SCHEME_2) {
                MktLog.d(sf_TAG, "  Play:");
                MktLog.d(sf_TAG, "    Frames         : " + mPlayFrameCounter);
                MktLog.d(sf_TAG, "    ReadRTPErrors  : " + mReadRTPErrors);
                if (mChannelCount != 0 && mSamplesPerFrame != 0) {
                    MktLog.d(sf_TAG, "    ReadRTPCounter : " + mReadCounter + " ("
                            + (mReadCounter / (mSamplesPerFrame / mChannelCount)) + ")");
                }
                MktLog.d(sf_TAG, "    QueueFull       : " + mRTPQueueFull);
                MktLog.d(sf_TAG, "  Record:");
                MktLog.d(sf_TAG, "    Frames          : " + mFrameCounter);
                MktLog.d(sf_TAG, "    MicLessBytesErr : " + mMicLessBytesErrors);
                MktLog.d(sf_TAG, "    FrameMinTime    : " + mFrameMinTime);
                MktLog.d(sf_TAG, "    FrameMaxTime    : " + mFrameMaxTime);
                MktLog.d(sf_TAG, "    FrameAverageTime: " + mFrameAverageTime);
                MktLog.d(sf_TAG, "    Delays          : " + mDelays);
                MktLog.d(sf_TAG, "    Delay total     : " + mDelay);
                MktLog.d(sf_TAG, "    Max. delay      : " + mMaxWait);
                MktLog.d(sf_TAG, "    FromRTP:");
                MktLog.d(sf_TAG, "      Missed        : " + mMissedRTPFrames);
                MktLog.d(sf_TAG, "      Discarded     : " + mScheme2FromRTPDiscardedFrames);
                if (mFrameCounter > 0) {
                    MktLog.d(sf_TAG, "    AecFrames  : " + mAecFrames + " (" + ((100 * mAecFrames) / mFrameCounter) + "%)");
                } else {
                    MktLog.d(sf_TAG, "    AecFrames  : " + mAecFrames);
                }
                MktLog.d(sf_TAG, "    ToRTP:");
                MktLog.d(sf_TAG, "      WriteErrors   : " + mWriteRTPErrors);
                if (mChannelCount != 0 && mSamplesPerFrame != 0) {
                    MktLog.d(sf_TAG, "      WriteCounter  : " + mWriteCounter + " ("
                            + (mWriteCounter / (mSamplesPerFrame / mChannelCount)) + ")");
                }
                if (TIME_FRAMES_STATISTICS) {
                    MktLog.d(sf_TAG, "    Frame times:");
                    MktLog.d(sf_TAG, "      < 21   : " + mFrame20);
                    MktLog.d(sf_TAG, "      21-30  : " + mFrame30);
                    MktLog.d(sf_TAG, "      31-50  : " + mFrame50);
                    MktLog.d(sf_TAG, "      51-100 : " + mFrame100);
                    MktLog.d(sf_TAG, "      100 >  : " + mFrame101above);
                }

                MktLog.d(sf_TAG, "  Frames cache:");
                MktLog.d(sf_TAG, "    New arrays : " + mFrameCache.getNewFrames());
                MktLog.d(sf_TAG, "    Put/Full   : " + mFrameCache.getPutFrameFull());
            }
        }
    }
	
	/**********************************************************************************************************************************/
	/*
	 * Record working thread 
	 */
    private class RecordWorkingThread extends Thread {
        private volatile AudioDevice mAudioDevice;

        public RecordWorkingThread(AudioDevice ad) {
            super("TH_RECORD");
            mAudioDevice = ad;
        }

        @Override
        public void run() {
            try {
                if (SCHEME == SCHEME_1) {
                    runInnerScheme1();
                } else if (SCHEME == SCHEME_2) {
                    if (isTestMode) {
                        runInnerScheme1();
                    } else {
                        runInnerScheme2();
                    }
                }
            } catch (java.lang.Throwable e_is) {
                if (LogSettings.MARKET) {
                    MktLog.e(sf_TAG, "Record thread. runInner : exception: ", e_is);
                }
            }
            
            if (mAudioDevice.mRecordThread == this) {
                mAudioDevice.onRecordThreadFinish();
            }
            mAudioDevice = null;
        }

        /**************************************************************************************************************************************
         **************************************************************************************************************************************/
        
        public void runInnerScheme1() {
            if (LogSettings.MARKET) {MktLog.i(sf_TAG, "Record working thread started");}
            int frameTime = (mAudioDevice.mSamplesPerFrame / (mAudioDevice.mClockRate / 1000));
            long samples_per_cycle = (mAudioDevice.mSamplesPerFrame / mAudioDevice.mChannelCount);
            long frame_counter = 0, playback_frame_discarded = 0;
            long fullFinish = 0;
            int bytes_per_frame = mAudioDevice.mBytesPerFrame;
            byte[] to_rtp_buf = new byte[mAudioDevice.mBytesPerFrame];
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
            AudioRecord r = mAudioDevice.mAudioRecord;
            try {
                r.startRecording();
            } catch (java.lang.Throwable e_is) {
                if (LogSettings.MARKET) {
                    MktLog.e(sf_TAG, "Record thread. s_recorder.startRecording() failed: ", e_is);
                }
                return;
            }
            
            AudioDeviceFrameCache cache = mAudioDevice.mFrameCache;
            AudioDeviceFrameQueue pQueue = mAudioDevice.mPlayBackQueue;
            AudioDeviceFrame from_rtp = cache.getFrame(bytes_per_frame);
            long params = mAudioDevice.mRcAudioParams;
            long rw_counter = 0, RTPErrors = 0, playQueueFulls = 0, micLessBytesErrors = 0, frameStartTime = 0, frameStopTime = 0, frameMaxTime = 0;
            long frameMinTime = 2000;
            long missedTime = frameTime;
            long waitTime = 0, maxWait = 0, delay = 0, delays = 0;
            long fullStart;
            if (USE_PRIVATE_ELAPSED_TIME) {
                fullStart = AudioDevicePJSIP.getTimestamp(params);
            } else {
                fullStart = SystemClock.elapsedRealtime();
            }
            long lastFrameTime = fullStart;
            long deltaTime = 0, frame20 = 0, frame30 = 0, frame50 = 0, frame100 = 0, frame101above  = 0;
            
            /**
             * Get one frame for rec_cb
             */
            byte[] from_rtp_buf;
            int mic_read_bytes = r.read(to_rtp_buf, 0, bytes_per_frame);
            if (mic_read_bytes == AudioRecord.ERROR_BAD_VALUE || mic_read_bytes == AudioRecord.ERROR_INVALID_OPERATION) {
                if (LogSettings.MARKET) {
                    MktLog.e(sf_TAG, "Record thread. Read failed first frame: " + mic_read_bytes);
                }
            }
            
            while (mAudioDevice.mActive) {
                if (USE_PRIVATE_ELAPSED_TIME) {
                    frameStartTime = AudioDevicePJSIP.getTimestamp(params);
                } else {
                    frameStartTime = SystemClock.elapsedRealtime();
                }
                if (frame_counter > SCHEME_1_STARTS_DELAYS_FROM_FRAME) {
                    if (USE_PRIVATE_ELAPSED_TIME) {
                        deltaTime = AudioDevicePJSIP.getElapsedTime(params, lastFrameTime, frameStartTime);
                    } else {
                        deltaTime = frameStartTime - lastFrameTime;
                    }
                    lastFrameTime = frameStartTime; missedTime = missedTime / 2 + deltaTime - frameTime;
                    if (missedTime <= 0) {
                        waitTime = -missedTime - 4;
                        if (waitTime > 0) {
                            if (waitTime > 20) { waitTime = 19;}
                            delays++; delay += waitTime;
                            if (waitTime > maxWait) {maxWait = waitTime;}
                            try {Thread.sleep(waitTime);} catch (java.lang.Throwable th) {}
                            if (!mAudioDevice.mActive) break;
                        }
                    }
                }
                /**
                 * Write mic(raw: first step; aec_mic: next steps) and get frame from RTP; rec_cb and play_cb as one step
                 */
                from_rtp_buf = from_rtp.getFrame();
                int resultRTP = AudioDevicePJSIP.readFromWriteToRTP(to_rtp_buf, from_rtp_buf, bytes_per_frame, rw_counter, params);
                if (resultRTP != PJSIP.sf_PJ_SUCCESS) RTPErrors++;
                rw_counter += samples_per_cycle;
                
                /**
                 * Mic frame to pass to AEC in additional to frame received from RTP at prev. step
                 */
                mic_read_bytes = r.read(to_rtp_buf, 0, bytes_per_frame);
                if (mic_read_bytes == AudioRecord.ERROR_BAD_VALUE || mic_read_bytes == AudioRecord.ERROR_INVALID_OPERATION) {
                    if (LogSettings.MARKET) MktLog.e(sf_TAG, "Record thread. Read failed: " + mic_read_bytes);
                    break;
                }
                if (mic_read_bytes < bytes_per_frame) {
                    micLessBytesErrors++;
                    frame_counter++;
                    continue;
                }

                /**
                 * AEC step + putting play frame for speaker
                 */
                boolean wasPutToPlayback = false;
                boolean isQueueFull = pQueue.isFull();
                if (isQueueFull) playQueueFulls++;
                if (resultRTP == PJSIP.sf_PJ_SUCCESS && !isQueueFull) {
                    if (isTestMode) {AudioSetupEngine.onPlayback(params);}
                    if (s_echo_cancelation || isTestMode) {AudioDevicePJSIP.aec(to_rtp_buf, from_rtp_buf, bytes_per_frame, params);}
                    wasPutToPlayback = pQueue.putFrame(from_rtp);
                } else {
                    playback_frame_discarded++;
                }
                frame_counter++;
                
                /**
                 * Statistics
                 */
                if (wasPutToPlayback) from_rtp = cache.getFrame(bytes_per_frame);
                if (USE_PRIVATE_ELAPSED_TIME) {
                    frameStopTime = AudioDevicePJSIP.getTimestamp(params);
                } else {
                    frameStopTime = SystemClock.elapsedRealtime();
                }
                if (frame_counter > SCHEME_1_STARTS_DELAYS_FROM_FRAME) {
                    long delta;
                    if (USE_PRIVATE_ELAPSED_TIME) {
                        delta = AudioDevicePJSIP.getElapsedTime(params, frameStartTime, frameStopTime);
                    } else {
                        delta = frameStopTime - frameStartTime;
                    }
                    if (delta > frameMaxTime) {frameMaxTime = delta;}if (delta < frameMinTime) {frameMinTime = delta;}
                    if (TIME_FRAMES_STATISTICS) if (delta < 21) {frame20++;} else if (delta < 31) {frame30++;} 
                        else if (delta < 51) {frame50++;} else if (delta < 101) {frame100++;} else {frame101above++;}
                }
            }

            if (USE_PRIVATE_ELAPSED_TIME) {
                fullFinish = AudioDevicePJSIP.getTimestamp(params);
            } else {
                fullFinish = SystemClock.elapsedRealtime();
            }
            mAudioDevice.mRTPErrors            = RTPErrors;
            mAudioDevice.mPlayQueueFulls       = playQueueFulls;
            mAudioDevice.mReadWriteCounter     = rw_counter;
            mAudioDevice.mFrameCounter         = frame_counter;
            mAudioDevice.mPlayBackFramesDiscarded      = playback_frame_discarded;
            mAudioDevice.mMicLessBytesErrors   = micLessBytesErrors;
            mAudioDevice.mFrameMinTime         = frameMinTime;
            mAudioDevice.mFrameMaxTime         = frameMaxTime;
            if (frame_counter > 0) {
                if (USE_PRIVATE_ELAPSED_TIME) {
                    mAudioDevice.mFrameAverageTime = AudioDevicePJSIP.getElapsedTime(params, fullStart, fullFinish)/frame_counter;
                } else {
                    mAudioDevice.mFrameAverageTime = (fullFinish - fullStart)/frame_counter;
                }
            }
            mAudioDevice.mMaxWait = maxWait;
            mAudioDevice.mDelay   = delay;
            mAudioDevice.mDelays  = delays;
            
            if (TIME_FRAMES_STATISTICS) {
                mAudioDevice.mFrame20 = frame20;
                mAudioDevice.mFrame30 = frame30;
                mAudioDevice.mFrame50 = frame50;
                mAudioDevice.mFrame100 = frame100;
                mAudioDevice.mFrame101above = frame101above;
            }
            try {
                r.stop();
            } catch (java.lang.Throwable e_is) {
            }
            if (LogSettings.MARKET) {
                MktLog.i(sf_TAG, "Record working thread finished");
            }
        }
        
        /**************************************************************************************************************************************
         **************************************************************************************************************************************/
        
        public void runInnerScheme2() {
            if (LogSettings.MARKET) {MktLog.i(sf_TAG, "Record working thread started (scheme 2)");}
            int frameTime = (mAudioDevice.mSamplesPerFrame / (mAudioDevice.mClockRate / 1000));
            long samples_per_cycle = (mAudioDevice.mSamplesPerFrame / mAudioDevice.mChannelCount);
            long frame_counter = 0;
            long fullFinish = 0;
            int bytes_per_frame = mAudioDevice.mBytesPerFrame;
            byte[] to_rtp_buf = new byte[mAudioDevice.mBytesPerFrame];
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
            AudioRecord r = mAudioDevice.mAudioRecord;
            try {
                r.startRecording();
            } catch (java.lang.Throwable e_is) {
                if (LogSettings.MARKET) {
                    MktLog.e(sf_TAG, "Record thread (scheme 2). s_recorder.startRecording() failed: ", e_is);
                }
                return;
            }
            
            AudioDeviceFrameCache cache = mAudioDevice.mFrameCache;
            AudioDeviceFrameQueue2 rtpQueue = mAudioDevice.mFromRTPQueue;
            AudioDeviceFrame rtpFrame = null;
            long params = mAudioDevice.mRcAudioParams;
            long write_counter = 0, micLessBytesErrors = 0, frameStartTime = 0, frameStopTime = 0, frameMaxTime = 0;
            long frameMinTime = 2000;
            long missedTime = frameTime;
            long waitTime = 0, maxWait = 0, delay = 0, delays = 0;
            long fullStart;
            if (USE_PRIVATE_ELAPSED_TIME) {
                fullStart = AudioDevicePJSIP.getTimestamp(params);
            } else {
                fullStart = SystemClock.elapsedRealtime();
            }
            long lastFrameTime = fullStart;
            long deltaTime = 0, frame20 = 0, frame30 = 0, frame50 = 0, frame100 = 0, frame101above  = 0;
            long writeRTPErrors = 0, missedRTPFrames = 0, AecFrames = 0;
            while (mAudioDevice.mActive) {
                if (USE_PRIVATE_ELAPSED_TIME) {
                    frameStartTime = AudioDevicePJSIP.getTimestamp(params);
                } else {
                    frameStartTime = SystemClock.elapsedRealtime();
                }
                if (SCHEME_2_ALSA_BEHAVIOR) {
                    if (USE_PRIVATE_ELAPSED_TIME) {
                        deltaTime = AudioDevicePJSIP.getElapsedTime(params, lastFrameTime, frameStartTime);
                    } else {
                        deltaTime = frameStartTime - lastFrameTime;
                    }
                    lastFrameTime = frameStartTime; missedTime = missedTime / 2 + deltaTime - frameTime;
                    if (missedTime <= 0) {
                        waitTime = -missedTime - 4;
                        if (waitTime > 0) {
                            if (waitTime > 20) { waitTime = 19;}
                            delays++; delay += waitTime;
                            if (waitTime > maxWait) {maxWait = waitTime;}
                            try {Thread.sleep(waitTime);} catch (java.lang.Throwable th) {}
                            if (!mAudioDevice.mActive) break;
                        }
                    }
                }
                
                int mic_read_bytes = r.read(to_rtp_buf, 0, bytes_per_frame);
                if (mic_read_bytes == AudioRecord.ERROR_BAD_VALUE || mic_read_bytes == AudioRecord.ERROR_INVALID_OPERATION) {
                    if (LogSettings.MARKET) MktLog.e(sf_TAG, "Record thread. Read failed: " + mic_read_bytes);
                    break;
                }
                if (mic_read_bytes < bytes_per_frame) {
                    micLessBytesErrors++;
                }
                
                rtpFrame = rtpQueue.getFrame();
                
                if (rtpFrame != null) {
                    byte[] from_rtp_buf = rtpFrame.getFrame();
                    if (s_echo_cancelation) {
                        AudioDevicePJSIP.aecScheme2(to_rtp_buf, from_rtp_buf, bytes_per_frame, params);
                        AecFrames++;
                    }
                } else {
                    missedRTPFrames++;
                }
                
                int resultRTP = AudioDevicePJSIP.writeToRTP(to_rtp_buf, 
                        bytes_per_frame, 
                        write_counter, 
                        params );
                write_counter += samples_per_cycle;
                if( resultRTP != PJSIP.sf_PJ_SUCCESS ){
                    writeRTPErrors++;
                }
                frame_counter++;
                if (rtpFrame != null) {
                    cache.putFrame(rtpFrame);
                }

                if (USE_PRIVATE_ELAPSED_TIME) {
                    frameStopTime = AudioDevicePJSIP.getTimestamp(params);
                } else {
                    frameStopTime = SystemClock.elapsedRealtime();
                }
                long delta;
                if (USE_PRIVATE_ELAPSED_TIME) {
                    delta = AudioDevicePJSIP.getElapsedTime(params, frameStartTime, frameStopTime);
                } else {
                    delta = frameStopTime - frameStartTime;
                }
                if (delta > frameMaxTime) {
                    frameMaxTime = delta;
                }
                if (delta < frameMinTime) {
                    frameMinTime = delta;
                }
                if (TIME_FRAMES_STATISTICS)
                    if (delta < 21) {
                        frame20++;
                    } else if (delta < 31) {
                        frame30++;
                    } else if (delta < 51) {
                        frame50++;
                    } else if (delta < 101) {
                        frame100++;
                    } else {
                        frame101above++;
                    }
            }

            if (USE_PRIVATE_ELAPSED_TIME) {
                fullFinish = AudioDevicePJSIP.getTimestamp(params);
            } else {
                fullFinish = SystemClock.elapsedRealtime();
            }
            mAudioDevice.mWriteRTPErrors       = writeRTPErrors;
            mAudioDevice.mWriteCounter         = write_counter;
            mAudioDevice.mMissedRTPFrames      = missedRTPFrames;
            mAudioDevice.mScheme2FromRTPDiscardedFrames = rtpQueue.getDiscardedFrames();
            mAudioDevice.mFrameCounter         = frame_counter;
            mAudioDevice.mMicLessBytesErrors   = micLessBytesErrors;
            mAudioDevice.mFrameMinTime         = frameMinTime;
            mAudioDevice.mFrameMaxTime         = frameMaxTime;
            mAudioDevice.mAecFrames            = AecFrames;
            
            if (frame_counter > 0) {
                if (USE_PRIVATE_ELAPSED_TIME) {
                    mAudioDevice.mFrameAverageTime = AudioDevicePJSIP.getElapsedTime(params, fullStart, fullFinish)/frame_counter;
                } else {
                    mAudioDevice.mFrameAverageTime = (fullFinish - fullStart)/frame_counter;
                }
            }
            mAudioDevice.mMaxWait = maxWait;
            mAudioDevice.mDelay   = delay;
            mAudioDevice.mDelays  = delays;
            
            if (TIME_FRAMES_STATISTICS) {
                mAudioDevice.mFrame20 = frame20;
                mAudioDevice.mFrame30 = frame30;
                mAudioDevice.mFrame50 = frame50;
                mAudioDevice.mFrame100 = frame100;
                mAudioDevice.mFrame101above = frame101above;
            }
            try {
                r.stop();
            } catch (java.lang.Throwable e_is) {
            }
            if (LogSettings.MARKET) {
                MktLog.i(sf_TAG, "Record working thread finished (scheme 2).");
            }
        }
    };
	
    /**
     * Playback working thread
     */
    private class PlaybackWorkingThread extends Thread {
        private volatile AudioDevice mAudioDevice;

        public PlaybackWorkingThread(AudioDevice ad) {
            super("TH_PLAYBACK");
            mAudioDevice = ad;
        }

        @Override
        public void run() {
            try {
                if (SCHEME == SCHEME_1) {
                    runInnerScheme1();
                } else if (SCHEME == SCHEME_2) {
                    if (isTestMode) {
                        runInnerScheme1();
                    } else {
                        runInnerScheme2();
                    }
                }
            } catch (java.lang.Throwable e_is) {
                if (LogSettings.MARKET) {
                    MktLog.e(sf_TAG, "Playback thread. runInner : exception: ", e_is);
                }
            }
            if (mAudioDevice.mPlaybackThread == this) {
                mAudioDevice.onPlayBackThreadFinish();
            }
            mAudioDevice = null;
        }

        /**************************************************************************************************************************************
         **************************************************************************************************************************************/
        public void runInnerScheme1() {
            if (LogSettings.MARKET) {MktLog.i(sf_TAG, "Playback working thread started");}
            long frame_counter = 0;
            AudioTrack a = mAudioDevice.mAudioTrack;
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);
            try {
                a.play();
            } catch (java.lang.Throwable e_is) {
                if (LogSettings.MARKET) {
                    MktLog.e(sf_TAG, "Playback thread. s_player.play() : exception: ", e_is);
                }
                return;
            }
            AudioDeviceFrameQueue q = mAudioDevice.mPlayBackQueue;
            AudioDeviceFrameCache cache = mAudioDevice.mFrameCache;
            while (mAudioDevice.mActive) {
                int writeBytes = 0;
                AudioDeviceFrame frame = q.getFrame();
                if (!mAudioDevice.mActive) break;
                while (frame != null && mAudioDevice.mActive) {
                    byte[] bframe = frame.getFrame();
                    writeBytes = a.write(bframe, 0, bframe.length);
                    if (writeBytes == AudioTrack.ERROR_BAD_VALUE || writeBytes == AudioTrack.ERROR_INVALID_OPERATION
                            || writeBytes <= 0) {
                        if (LogSettings.MARKET) {
                            MktLog.w(sf_TAG, "Playback thread. Write failed: " + writeBytes);
                        }
                        break;
                    }
                    frame_counter++;
                    cache.putFrame(frame);
                    if (!mAudioDevice.mActive) break;
                    frame = q.getFrame();
                }
                if (writeBytes < 0)
                    continue;
            }
            mAudioDevice.mPlayFrameCounter = frame_counter;
            try {
                a.flush();
            } catch (IllegalStateException e_is) {
            }

            try {
                a.stop();
            } catch (IllegalStateException e_is) {
            }

            if (LogSettings.MARKET) {
                MktLog.i(sf_TAG, "Playback working thread finished.");
            }
        }
        
        /**************************************************************************************************************************************
         **************************************************************************************************************************************/
        
        public void runInnerScheme2() {
            if (LogSettings.MARKET) {MktLog.i(sf_TAG, "Playback working thread started (scheme 2)");}
            long frame_counter = 0;
            int bytes_per_frame = mAudioDevice.mBytesPerFrame;
            AudioTrack a = mAudioDevice.mAudioTrack;
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
            try {
                a.play();
            } catch (java.lang.Throwable e_is) {
                if (LogSettings.MARKET) {
                    MktLog.e(sf_TAG, "Playback thread (scheme 2). s_player.play() : exception: ", e_is);
                }
                return;
            }

            AudioDeviceFrameQueue2 fromRTPQueue = mAudioDevice.mFromRTPQueue;
            AudioDeviceFrameCache cache = mAudioDevice.mFrameCache;
            long samples_per_cycle = (mAudioDevice.mSamplesPerFrame / mAudioDevice.mChannelCount); 
            long params = mAudioDevice.mRcAudioParams;
            AudioDeviceFrame frame = cache.getFrame(bytes_per_frame);
            long read_counter = 0, read_errors = 0;
            while (mAudioDevice.mActive) {
                int writeBytes = 0;
                byte[] bframe = frame.getFrame();
                int resultRTP = AudioDevicePJSIP.readFromRTP(bframe, bytes_per_frame, read_counter, params);
                    
                if (resultRTP != PJSIP.sf_PJ_SUCCESS) {
                    read_errors++;
                } else {
                    read_counter += samples_per_cycle;
                }
                recordWav.write(bframe);
                writeBytes = a.write(bframe, 0, bframe.length);
                if (writeBytes == AudioTrack.ERROR_BAD_VALUE || writeBytes == AudioTrack.ERROR_INVALID_OPERATION
                            || writeBytes <= 0) {
                        if (LogSettings.MARKET) {
                            MktLog.w(sf_TAG, "Playback thread. Write failed: " + writeBytes);
                        }
                        break;
                }
                frame_counter++;
                
                if (fromRTPQueue.putFrame(frame)) {
                    frame = cache.getFrame(bytes_per_frame);
                } else {
                    mAudioDevice.mRTPQueueFull++;
                }    
            }
            recordWav.stop();
            mAudioDevice.mPlayFrameCounter = frame_counter;
            mAudioDevice.mReadRTPErrors    = read_errors;
            mAudioDevice.mReadCounter      = read_counter;
            
            try {
                a.flush();
            } catch (IllegalStateException e_is) {
            }

            try {
                a.stop();
            } catch (IllegalStateException e_is) {
            }

            if (LogSettings.MARKET) {
                MktLog.i(sf_TAG, "Playback working thread finished (scheme 2)");
            }
        }
    };
	
    /**************************************************************************************************************************************
     **************************************************************************************************************************************/
	public static boolean isTestMode() {
	    if(LogSettings.MARKET){ 
	        MktLog.d(sf_TAG, " isTestMode " + isTestMode);
	    }
	    return isTestMode;
	}
	
    public static void setTestMode(boolean value) {
        if (LogSettings.MARKET) {
            MktLog.d(sf_TAG, "setTestMode > isTestMode will be " + value);
        }
        isTestMode = value;
    }

    private static final Object mSync = new Object();

    public static int getDelayInternalSpeaker() {
        synchronized (mSync) {
            return mAECDelay;
        }
    }

    public static void updateAECDelay(int audioSource) {
        synchronized (mSync) {
            if (audioSource == RCMediaManager.ROUTE_INTERNAL) {
                mAECDelay = RCMProviderHelper.getDeviceInternalSpeakerDelay(RingCentralApp.getContextRC());
            } else if (audioSource == RCMediaManager.ROUTE_SPEAKER) {
                mAECDelay = RCMProviderHelper.getDeviceExternalSpeakerDelay(RingCentralApp.getContextRC());
            } else if (audioSource == RCMediaManager.ROUTE_BLUETOOTH) {
                mAECDelay = RCMConstants.DEFAULT_AEC_DELAY_VALUE;
            }

            if (LogSettings.MARKET) {
                MktLog.i(sf_TAG, "updateAECDelay(), source : " + RCMediaManager.getRouteTag(audioSource) + ", delayValue : "
                        + mAECDelay + "(samples), " + (mAECDelay / 8) + "(millis)");
            }
        }

    }
}
