/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import com.rcbase.parsers.sipmessage.SipMessageInput;

/**
 * @author Denis Kudja
 *
 */
public interface IPJSIPCallbackListener {

	public void on_reg_state( int accountId );
	public void on_message( long rcCallId, SipMessageInput sipMsg );
	public void on_incoming_call( CallInfo callInfo );
	
	public void on_call_media_state( long rcCallId, int state );
	public void on_stop_call_media( long rcCallId );
	
	public void notify_on_call_state( long rcCallId, int state, int last_status, int ending_reason, int media_sate);
	public void on_call_transfer_status( long rcCallId, int status, String statusProgressText );
	public void on_media_failed( int error );
	
	public void on_call_info_changed( long rcCallId );
	
	public void PJSIPCallbackOnCallState( int pjsipCallId, int state, int last_status, String dump );
}
