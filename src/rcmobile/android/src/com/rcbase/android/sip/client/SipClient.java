/**
 * Copyright (C) 2010-2012 RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.client;

import java.util.Vector;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.telephony.TelephonyManager;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.client.SIProxy.ServiceRequest;
import com.rcbase.android.sip.service.CallInfo;
import com.rcbase.android.sip.service.IServicePJSIP;
import com.rcbase.android.sip.service.IServicePJSIPCallback;
import com.rcbase.android.sip.service.PJSIP;
import com.rcbase.android.sip.service.RCCallInfo;
import com.rcbase.android.utils.execution.TimedProxyTask;
import com.rcbase.android.utils.media.AudioState;
import com.ringcentral.android.LogSettings;

/**
 * RC SIP Client. It is responsible for client functions and 
 * process RC PJSIP Service callbacks. 
 * 
 * @author Denis Kudja
 *
 */
public class SipClient implements SIProxyClient {

    private static final String TAG = "[RC]SipClient";
    
    
    public final static int    	sf_base_event       		= 1000;
    public final static int    	sf_update_controls  		= (sf_base_event);
    public final static int		sf_completion  				= (sf_base_event + 1);
    public final static int    	sf_update_call_state 		= (sf_base_event + 2);
    public final static int    	sf_make_call 				= (sf_base_event + 4);
    public final static int    	sf_switch_view				= (sf_base_event + 5);
    public final static int    	sf_set_mute					= (sf_base_event + 6);
    public final static int    	sf_synchronize_call_state	= (sf_base_event + 7);
    public final static int     sf_update_audio             = (sf_base_event + 10);
    
//    private AccountInfo m_account = null;
    private TelephonyManager m_telefMng = null;
    
    public final static float m_unmute_level = 1;    
    public final static float m_mute_level   = 0;    
    
    public final static int MUTE_ON	= 1;
    public final static int MUTE_OFF = 0;
    
    public static float getMuteLevel(){
    	return m_mute_level;
    }
    
    public static float getUnMuteLevel(){
    	return m_unmute_level;
    }
    
//	protected synchronized AccountInfo getAccount() {
//		return m_account;
//	}
//
//	protected synchronized void setAccount(AccountInfo account) {
//		m_account = account;
//	}

	/**
	 * Process service callback calls
	 */
	private SipClientServiceCallback m_callback = null;
	private SipClientLines m_lines = null;
	
	protected final SipClientLines getLines() {
		return m_lines;
	}

	/**
	 *	RC PJSIP service interface 
	 */
	private IServicePJSIP m_service = null;
	
	/**
	 * Handler is used for sending notification to the UI level
	 */
	private Handler m_handler = null;
	
	protected final Handler getHandler() {
		return m_handler;
	}

	public SipClient( Handler handler, Context context ){
		
	    if( m_telefMng == null ){
	    	m_telefMng = (TelephonyManager)context.getSystemService(android.content.Context.TELEPHONY_SERVICE);
	    }
		
		m_callback = new SipClientServiceCallback( this );
		m_lines = new SipClientLines();
		m_handler = handler;
	}
	
	public synchronized IServicePJSIPCallback getCallback() {
		return ( m_callback == null ? null : m_callback.getCallback());
	}

//	public synchronized void setService(IServicePJSIP service) {
//		m_service = service;
//	}

	public synchronized IServicePJSIP getService() {
		return m_service;
	}

	/***************************************************************************************************************************************
	 */
    private class RegisterCallBackCommand extends TimedSIProxyTask {
        public RegisterCallBackCommand() {
            super("RegisterCallBackCommand");
        }

        @Override
        public void doServiceRequest(IServicePJSIP service) {
            try {
                service.registerCallback(getCallback().hashCode(), getCallback());
            } catch (Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "RegisterCallBackCommand:" + th.toString());
                }
            }
        }
    }
	
    private volatile boolean isCallBackRegistered = false;
    public boolean registerCallback() {
        if (isCallBackRegistered) {
            return true;
        }
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "registerCallback() started");
        }
        if (m_service == null || getCallback() == null) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "registerCallback() failed. Service is null.");
            }
            isCallBackRegistered = false;
            return false;
        }
        RegisterCallBackCommand task = new RegisterCallBackCommand();
        if (task.execute(RCSIP.TIMEOUT_FOR_SERVICE_NOT_CRITICAL_CALLS, true) != TimedSIProxyTask.COMPLETED) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "RegisterCallBackCommand:failed");
            }
            sendMessage(new CompletionInfo(false, false, RCCallInfo.CALL_ENDED_BY_ERROR, CompletionInfo.ERROR_REGISTER_CALLBACK,
                    0, null, false));
            return false;
        }
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "registerCallback(): finished...");
        }
        isCallBackRegistered = true;
        return true;
    }
    
    /***************************************************************************************************************************************
     */
    private class UnRegisterCallBackCommand extends TimedSIProxyTask {
        public UnRegisterCallBackCommand() {
            super("UnRegisterCallBackCommand");
        }

        @Override
        public void doServiceRequest(IServicePJSIP service) {
            try {
                service.unregisterCallback( getCallback().hashCode());
            } catch (Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "UnRegisterCallBackCommand:" + th.toString());
                }
            }
        }
    }

    public void unregisterCallback() {

        if (LogSettings.MARKET) {
            MktLog.d(TAG, "unregisterCallback(): starting...");
        }

        if (m_service != null && getCallback() != null) {
            UnRegisterCallBackCommand task = new UnRegisterCallBackCommand();
            if (task.execute(RCSIP.TIMEOUT_FOR_SERVICE_NOT_CRITICAL_CALLS, true) != TimedSIProxyTask.COMPLETED) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "UnRegisterCallBackCommand:failed");
                }
            }
            isCallBackRegistered = false;
            m_callback = null;
        }

        if (LogSettings.MARKET) {
            MktLog.d(TAG, "unregisterCallback(): finished...");
        }
    }

    /***************************************************************************************************************************************
     */
	public boolean bind( Context context ){
        ServiceRequest sr = SIProxy.getInstance().getService(this);
        
        if (sr.serviceState == SIProxy.SHUTDOWN) {
            sendMessage( new CompletionInfo(false, false, RCCallInfo.CALL_ENDED_BY_ERROR, CompletionInfo.ERROR_BINDING, 0, null, false));
            return false;
        } else if (sr.serviceState == SIProxy.CONNECTED) {
            m_service = sr.serviceHandle;
            onServiceConnected();
        }
        
        return true;
	}

	@Override
	public void onServiceConnectionStateChange() {
	    ServiceRequest service = SIProxy.getInstance().getService(this);
	    if (service.serviceState == SIProxy.SHUTDOWN) {
            if (getLines().isActiveCalls()) {
                sendMessage( new CompletionInfo(true, false, RCCallInfo.CALL_ENDED_BY_ERROR, CompletionInfo.ERROR_SERVICE_CONN_LOST, 0, null, false));
            }
            m_service = null;
            SIProxy.getInstance().removeClient(this);
        } else if (service.serviceState == SIProxy.CONNECTED) {
            m_service = service.serviceHandle;
            onServiceConnected();
        }
	}
	
	
	/**
	 * Unbind RC PJSIP Service
	 */
	public void unBindSipServiceRoutine( Context context ) {
	    SIProxy.getInstance().removeClient(this);
	    
		if( m_telefMng != null ){
			m_telefMng = null;
		}
		
		if( context != null ){
			Thread shoutdownThread = new Thread( new unbindServiceThread( context ), "unBindSipServiceRoutine");
			shoutdownThread.start();
		}
	}

	/*
	 * 
	 */
	private class unbindServiceThread implements Runnable{
//		Context m_context = null;
		
		public unbindServiceThread( Context context ){
//			m_context = context;
		}
		
		@Override
		public void run() {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "unBindSipServiceRoutine():start...");
            }
            
            getLines().remove_all_calls();
		}
	};
	
	/**
	 * 
	 */
//	public void stopSipStack(){
//        if (LogSettings.MARKET) {
//            MktLog.i(TAG, "stopSipStack():start...");
//        }
//		
//		if( m_service != null ){
//			try {
//				m_service.stopSipStack();
//			} catch (RemoteException e) {
//			}
//		}
//		
//        if (LogSettings.MARKET) {
//            MktLog.i(TAG, "stopSipStack(): finished");
//        }
//	}
	
	
	private void onServiceConnected() {
	    if (LogSettings.MARKET) {
            MktLog.i(TAG, "onServiceConnected is starting...");
        }
	    
	    if (m_service == null) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "onServiceConnected(): Failed to get IServicePJSIP");
            }
            sendMessage( new CompletionInfo(false, false, RCCallInfo.CALL_ENDED_BY_ERROR, CompletionInfo.ERROR_ISERVICEPJSIP, 0, null, false));
            return;
        }
	    
        /*
         * Register PJSIP callback
         */
        if( !registerCallback() ){
            return;
        }
        
        try {
            final boolean sipStarted = m_service.isStartedSipStack();
            
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "onServiceConnected(): SIP stack started: " + sipStarted );
            }
            
            if( sipStarted ){
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, " onServiceConnected: Send sf_synchronize_call_state");
                }
                sendMessage( SipClient.sf_synchronize_call_state, 1, null );
            }
            else{
                m_service.startSipStack( false );
            }
            
            new Thread(new Runnable() {
                @Override
                public void run() {
                    AudioState audioState = null;
                    try {
                        audioState = SipClient.this.m_service.getAudioState();
                        sendMessage(sf_update_audio, 0, audioState);
                    } catch (RemoteException e) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "onServiceConnected(): Failed to call isPossibleToUseBluetooth;", e);
                        }
                    }
                }
            }, "SIPClient:onServiceConnected").start();
            
        } catch (Throwable e) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "onServiceConnected(): Failed to start SIP Stack: RemoteException;", e);
            }
            sendMessage( new CompletionInfo(false, false, RCCallInfo.CALL_ENDED_BY_ERROR, CompletionInfo.ERROR_START_SIP, 0, null, false));
            return;
        }
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onServiceConnected finished");
        }
	}
	
	/**
	 * Send notification to UI level through Handler is received 
	 * during SipClient creation.
	 */
	public void sendMessage( CompletionInfo compInfo ){
		
		if( getHandler() != null ){
	          try {
	              Message msg = Message.obtain( getHandler(), sf_completion );
	              msg.arg1	= ( compInfo != null ? compInfo.getErrorCode() : CompletionInfo.ERROR_UNKNOWN );
	              msg.obj 	= compInfo;
	              msg.sendToTarget();
	          } catch (Throwable e) {
	          }
		}
	}

	public void sendMessage( int code, int sub_code, Object obj ){
		
		if( getHandler() != null ){
	          try {
	              Message msg = Message.obtain( getHandler(), code );
	              msg.arg1	= sub_code;
	              msg.obj 	= obj;
	              msg.sendToTarget();
	          } catch (Throwable e) {
	          }
		}
	}
	
//	protected void removeMessages(int code){
//		if( getHandler() != null ){
//			getHandler().removeMessages(code);
//		}		
//	}
	
	/**
	 * Get array of active calls
	 */
	public synchronized final CallInfo[] getCallsArray(){
		return getLines().getCallsArray();
	}
	/**
	 * Get number of active lines
	 */
	public synchronized final int getActiveLines(){
		return getLines().getActiveLines();
	}
	
    /***************************************************************************************************************************************
     */
    private static class GetNumberOfActiveCallsCommand extends TimedSIProxyTask {
        int mLastCallsNumber = 0;

        public GetNumberOfActiveCallsCommand() {
            super("GetNumberOfActiveCallsCommand");
        }

        @Override
        public void doServiceRequest(IServicePJSIP service) {
            try {
                mLastCallsNumber = service.call_get_count();
            } catch (Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "GetNumberOfActiveCallsCommand:call_get_count:" + th.toString());
                }
            }
        }
    }

    /**
     * Returns number of active (alive) calls currently driven in SIP service.
     * 
     * @return number of active (alive) calls currently driven in SIP service.
     */
    public int getNumberOfActiveCalls(long timeout) {
        GetNumberOfActiveCallsCommand task = new GetNumberOfActiveCallsCommand();
        if (task.execute(RCSIP.TIMEOUT_FOR_SERVICE_NOT_CRITICAL_CALLS, false) != TimedSIProxyTask.COMPLETED) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "GetNumberOfActiveCallsCommand:failed");
            }
        }
        return task.mLastCallsNumber;
    }
	
    /***************************************************************************************************************************************
     */
    private class SyncCallInfoCommand extends TimedSIProxyTask {
        int callCount = 0;
        Vector<CallInfo> activeCallsVec = new Vector<CallInfo>();
        float adjust_rx_level;
        AudioState audioState;
        public SyncCallInfoCommand() {
            super("SyncCallInfoCommand");
        }

        @Override
        public void doServiceRequest(IServicePJSIP service) throws RemoteException {
            callCount = service.call_get_count();
            if (callCount <= 0) {
                return;
            }
            long[] activeCalls = service.call_get_active_calls();
            if (activeCalls == null || activeCalls.length == 0) {
                return;
            }
            for (int i = 0; i < activeCalls.length; i++) {
                try {
                    CallInfo callInfo = new CallInfo();
                    if (service.call_get_info(activeCalls[i], callInfo) == PJSIP.sf_PJ_SUCCESS) {
                        activeCallsVec.add(callInfo);
                    }
                }
                catch (Throwable e) {
                }
            }
            /**
             * TODO: Optimize
             */
            audioState = m_service.getAudioState();
            adjust_rx_level = m_service.conf_get_adjust_rx_level(0);
            return;
        }
    }
    
	/**
	 * Synchronize call information with RC SIP service 
	 * 1. Get list of active calls 
	 * 2. Get call info 
	 * 3. Update call and media state
	 */
	public int syncCallInfo() {
    	if (LogSettings.MARKET) {
    	    MktLog.d(TAG, "syncCallInfo is starting...");
    	}

    	int callCount = 0;
    	
    	if( m_service == null ){
        	if (LogSettings.MARKET) {
        	    MktLog.i(TAG, "syncCallInfo finished. Service is not started");
        	}
    		return -1;
    	}
    	
    	SyncCallInfoCommand task = new SyncCallInfoCommand();
        if (task.execute(RCSIP.TIMEOUT_FOR_SERVICE_NOT_CRITICAL_CALLS, false) != TimedSIProxyTask.COMPLETED) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "UnRegisterCallBackCommand:failed");
            }
            return -2;
        }
    	
        callCount = task.callCount;
        if( callCount == 0 ){
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "syncCallInfo() finished No active calls." );
            }
    		sendMessage( sf_switch_view, 0, null );
    		sendMessage( sf_update_call_state, 0, null );
        	return callCount;
        }
		
    	getLines().remove_all_calls();
    	
    	if (task.activeCallsVec == null || task.activeCallsVec.isEmpty()) {
    	    return 0;
    	}
        for (CallInfo callInfo : task.activeCallsVec) {
            getLines().call_add(callInfo, false);
        }
		
		/*
		 * Try to detect current line by Hold status and change current line status if need
		 */
		final int currLineDetected = getLines().detectCurrentLineIndex();
    	getLines().setCurrentLine( ( currLineDetected == SipClientLines.sf_INVALID_LINE_INDEX ? 0 : currLineDetected ) );
		
		/*
		 * Restore Speaker state & mute state
		 */
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "syncCallInfo() restore Mute & Speaker values");
        }

        final AudioState audioState = task.audioState;
        final float adjust_rx_level = task.adjust_rx_level;

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "syncCallInfo audio " + audioState + " AdjustRxLevel :" + adjust_rx_level + " [" + getUnMuteLevel()
                    + "]");
        }

        /*
         * If current call does not have MEDIA (it is on hold) we should not put
         * it on mute
         */
        final CallInfo callInfoCurrent = getLines().getCurrentLineCallInfo();
        if (callInfoCurrent != null) {

            int muteValue = (adjust_rx_level < getUnMuteLevel() ? MUTE_ON : MUTE_OFF);

            if (LogSettings.MARKET) {
                MktLog.d(TAG, "syncCallInfo() Media state for current call is:" + callInfoCurrent.getMedia_state() + " Mute:"
                        + muteValue + " Hold:" + callInfoCurrent.isHold());
            }

            if (muteValue == MUTE_ON && callInfoCurrent.getMedia_state() == RCSIP.PJSUA_CALL_MEDIA_NONE) {

                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "syncCallInfo() Clear MUTE state because current call has no media");
                }

                muteValue = 0;
            }

            if (LogSettings.MARKET) {
                MktLog.d(TAG, "syncCallInfo() set MUTE to:" + muteValue);
            }

            sendMessage(sf_set_mute, muteValue, null);
        }

        sendMessage(sf_update_audio, 0, audioState);

        sendMessage(sf_switch_view, 0, null);
        sendMessage(sf_update_call_state, 0, null);

        if (LogSettings.MARKET) {
            MktLog.w(TAG, "syncCallInfo() finished Calls count:" + callCount);
        }

        return callCount;
	}	

    /***************************************************************************************************************************************
     */
    private class HangupCallCommand extends TimedSIProxyTask {
        long callId;
        int result = PJSIP.sf_PJ_EINVALIDOP;

        public HangupCallCommand(long callId) {
            super("HangupCallCommand");
            this.callId = callId;
        }

        @Override
        public void doServiceRequest(IServicePJSIP service) throws RemoteException {
            result = service.call_hangup(callId, 0, true);
        }
    }

    /**
     * Hangup call.
     * 
     * @param Call
     *            ID. If Call ID is equal sf_INVALID_CALL_ID then hangup call
     *            for current active line
     */
    public void hangup(long callId) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "hangup Call ID: " + callId);
        }

        if (callId == PJSIP.sf_INVALID_RC_CALL_ID) {
            callId = getLines().getCurrentLineCallId();
        }

        if (callId == PJSIP.sf_INVALID_RC_CALL_ID) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "hangup Could not detect call ID");
            }

            return;
        }

        if (m_service != null) {
            HangupCallCommand task = new HangupCallCommand(callId);
            if (task.execute(RCSIP.TIMEOUT_FOR_SERVICE_CRITICAL_CALLS, true) != TimedSIProxyTask.COMPLETED) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "HangupCallCommand:failed");
                }
                return;
            }
            if (task.result != PJSIP.sf_PJ_SUCCESS) {
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "hangup Call ID: " + callId + " Result:" + task.result);
                }
            }
        }
    }
    /***************************************************************************************************************************************
     */	
	/**
	 * Select another line
	 */
	public synchronized void selectLine( long callIdSelectedLine ){
		
        if (LogSettings.ENGINEERING) {
            EngLog.i(TAG, "selectLine new Call Id: " + callIdSelectedLine );
        }
		
		/*
		 * Is there is invalid line - do nothing
		 * Note: Possible we will change this behaviour in the feature when
		 * will be added possibility to make additional outbound call from 
		 * this activity
		 */
		if( callIdSelectedLine == PJSIP.sf_INVALID_RC_CALL_ID ){
            if (LogSettings.MARKET) {
            	MktLog.i(TAG, "selectLine. Invalid Call Id.");
            }
			return;
		}
		
        long callId = getLines().getCurrentLineCallId();
        
        /*
         * Is there is the same line - do nothing
         */
        if( callId == callIdSelectedLine ){
            if (LogSettings.MARKET) {
            	MktLog.i(TAG, "selectLine. This line is active already.");
            }
        	return;
        }
		
		int activeLines = getActiveLines();
        if( activeLines == 0 ){
            if (LogSettings.MARKET) {
            	MktLog.i(TAG, "selectLine. No active lines.");
            }
        	return;
        }
        
        /*
         * Hold other lines 
         */
        holdAll( callIdSelectedLine );
        
		/*
		 * Activate a new line marked as current
		 */
    	final int line = getLines().getLineIndexbyCallId( callIdSelectedLine );
    	final CallInfo currLine = getLineCallInfo( line );
        boolean isOnHold = true;
        if( currLine!= null )
        	isOnHold = currLine.isHold();
        
        if( isOnHold && m_service != null ){
    		try {
				m_service.call_unhold(callIdSelectedLine);
	    		getLines().call_set_hold(callIdSelectedLine, false);
			} catch (RemoteException e) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "selectLine call_unhold failed ", e );
                }
			}
        }
        
    	getLines().setCurrentLine( line );
    	sendMessage( SipClient.sf_update_call_state, 0, null );
        
	}
    /***************************************************************************************************************************************
     */
	private class CallHoldCommand extends TimedSIProxyTask {
        long callId;
        int result = PJSIP.sf_PJ_EINVALIDOP;

        public CallHoldCommand(long callId) {
            super("CallHoldCommand");
            this.callId = callId;
        }

        @Override
        public void doServiceRequest(IServicePJSIP service) throws RemoteException {
            result = service.call_hold(callId);
        }
    }
	
	private class CallUnHoldCommand extends TimedSIProxyTask {
        long callId;
        int result = PJSIP.sf_PJ_EINVALIDOP;

        public CallUnHoldCommand(long callId) {
            super("CallUnHoldCommand");
            this.callId = callId;
        }

        @Override
        public void doServiceRequest(IServicePJSIP service) throws RemoteException {
            result = service.call_unhold(callId);
        }
    }
	
	/**
	 * Hold all calls exclude current line
	 */
	private synchronized void holdAll(final long lineCallId){

        CallInfo[] calls = getLines().getCallsArray();
        if( calls == null ){
            if (LogSettings.MARKET) {
            	MktLog.i(TAG, "holdAll. No active lines.");
            }
        	return;
        }

        if (LogSettings.MARKET) {
        	MktLog.i(TAG, "holdAll. Starting.... calls:" + calls.length );
        }
        
        for (int i = 0; i < calls.length; i++) {
            /*
             * Set on Hold all other lines exclude new active line
             */
            if (calls[i].getRcCallId() != lineCallId) {

                if (!calls[i].isHold()) {
                    if (m_service != null) {
                        if (LogSettings.MARKET) {
                            MktLog.i(TAG, "holdAll. Set hold Call ID:" + calls[i].getRcCallId() + " Remote URI:"
                                    + calls[i].getRemoteURI());
                        }
                        // TODO : Process result
                        new CallHoldCommand(calls[i].getRcCallId()).execute(RCSIP.TIMEOUT_FOR_SERVICE_NOT_CRITICAL_CALLS, true);
                        getLines().call_set_hold(calls[i].getRcCallId(), true);
                    }
                }
            }
        }

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "holdAll. Finished.");
        }
	}

    /***************************************************************************************************************************************
     */

	/**
	 * Hold call on active line 
	 */
	public synchronized void hold(){
        final long callId = getLines().getCurrentLineCallId();
        if( callId != PJSIP.sf_INVALID_RC_CALL_ID ){
            if( m_service != null ){
                try {
                    final CallInfo callInfo = getLines().getCallbyCallId( callId );
                	if( callInfo != null ){
                    	if( callInfo.isHold()){
                    	    // TODO : Process result
                            new CallUnHoldCommand(callId).execute(RCSIP.TIMEOUT_FOR_SERVICE_NOT_CRITICAL_CALLS, true);
                    		getLines().call_set_hold(callId, false);
                    	}
                    	else{
                    	    // TODO : Process result
                    	    new CallHoldCommand(callId).execute(RCSIP.TIMEOUT_FOR_SERVICE_NOT_CRITICAL_CALLS, true);
                    		getLines().call_set_hold(callId, true);
                    	}
                	}
                } catch (java.lang.Throwable e) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "hold failed ", e);
                    }
                }
                
                sendMessage( sf_update_controls, 0, null );
            }
        }
	}
	
    /***************************************************************************************************************************************
     */	
	private class DTMFCommand extends TimedSIProxyTask {
	    long callId;
	    String digit;
        int result = PJSIP.sf_PJ_EINVALIDOP;

        public DTMFCommand(long callId, String digit) {
            super("DTMFCommand");
            this.callId = callId;
            this.digit = digit;
        }

        @Override
        public void doServiceRequest(IServicePJSIP service) throws RemoteException {
            result = service.call_dial_dtmf(callId, digit);
        }
    }
	public synchronized void dialDTMF(String digit){
		final long callId = getLines().getCurrentLineCallId();
		if ( callId != PJSIP.sf_INVALID_RC_CALL_ID ) {
			if( m_service != null){
				try {
					final CallInfo callInfo = getLines().getCallbyCallId( callId );
                	if((callInfo != null)&&!callInfo.isHold()){
                	    DTMFCommand task = new DTMFCommand(callId, digit);
                        if (task.execute(RCSIP.TIMEOUT_FOR_SERVICE_CRITICAL_CALLS, true) != TimedSIProxyTask.COMPLETED) {
                            if (LogSettings.MARKET) {
                                MktLog.w(TAG, "dialDTMF:failed");
                            }
                            return;
                        }
                	}
                } catch (java.lang.Throwable e) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "dialDTMF failed, digit : " + digit, e);
                    }
                }
			}
		}
	}
	
	/**
	 * Make outbound call
	 */
//	public synchronized void makeCall( ){
//		Thread makeCallTh = new Thread( new makeCallThread());
//		makeCallTh.start();
//	}
	
	/*
	 * 
	 */
//	private class makeCallThread implements Runnable {
//		public makeCallThread(){
//		}
//		
//		@Override
//		public void run() {
//			
//			if( m_service == null )
//				return;
//			
//            try {
//
//            	final int line = getLines().getOutboundLineIndex();
//                if (LogSettings.ENGINEERING) {
//                    EngLog.i(TAG, "Make call for line: " + line );
//                }
//                
//            	if( line == SipClientLines.sf_INVALID_LINE_INDEX ){
//                    if (LogSettings.ENGINEERING) {
//                        EngLog.i(TAG, "Make call. No outbound lines. Check active lines." );
//                    }
//                    
//            		sendMessage( SipClient.sf_synchronize_call_state, 0, null );
//            		return;
//            	}
//            	
//            	/*
//            	 * Mark this call as processing started. As result exclude it from outbound calls list.
//            	 */
//            	getLines().setProcessingStarted( line, true );
//            	
//                final CallInfo callInfoOutbound = getLines().getLineCallInfo( line );
//                if( callInfoOutbound == null ){
//                    if (LogSettings.MARKET) {
//                    	MktLog.i(TAG, "Making call failed. Invalid line number.");
//                    }
//                    return;
//                }
//                
//                if( !callInfoOutbound.isOutbound() ){
//                    if (LogSettings.MARKET) {
//                    	MktLog.i(TAG, "Making call failed. Line is not outbound call.");
//                    }
//                    return;
//                }
//                
//                if (LogSettings.MARKET) {
//                    MktLog.i(TAG, "Making call to " + callInfoOutbound.getRemoteURI());
//                }
//                
//                int acc_id = PJSIP.sf_INVALID_ACCOUNT_ID;
//                if( getAccount() == null || !getAccount().isRegistered() ){
//                    if (LogSettings.MARKET) {
//                        MktLog.e(TAG, "Making call failed. SIP Account had not been registered." );
//                    }
//
//                    sendMessage( new CompletionInfo(true, false, CompletionInfo.ERROR_REGISTER_ACCOUNT, 0, null, false));
//                    return;
//                }
//
//                if( m_telefMng != null ){
//                    if (LogSettings.ENGINEERING) {
//                    	EngLog.i(TAG, "makeCall Telephony state:" + m_telefMng.getCallState() );
//                    }
//
//                	if( m_telefMng.getCallState() != TelephonyManager.CALL_STATE_IDLE ){
//                		
//                        if (LogSettings.ENGINEERING) {
//                        	EngLog.i(TAG, "makeCall Active celluar call(attempt) is detected. Do nothing.");
//                        }
//                        sendMessage( new CompletionInfo(true, true, CompletionInfo.ERROR_UNKNOWN, 0, null, true));
//                    	return;
//                	}
//                }
//                
//                /*
//                 * Hold all active lines 
//                 */
//                if (LogSettings.ENGINEERING) {
//                	EngLog.i(TAG, "makeCall Hold All");
//                }
//                m_service.call_hold_all();
//
//				ringOn();
//                
//                acc_id = getAccount().getAccountId();
//                if (LogSettings.MARKET) {
//                    MktLog.i(TAG, "makeCall account ID: " + acc_id);
//                }
//                
//                int callId = m_service.call_make_call(acc_id, callInfoOutbound.getRemoteURI());
//                if (LogSettings.MARKET) {
//                    MktLog.i(TAG, "makeCall New call ID: " + callId);
//                }
//                
//                ringOff();
//                
//                if (callId != PJSIP.sf_INVALID_RC_CALL_ID) {
//
//                    CallInfo callInfo = new CallInfo();
//                    callInfo.setCallId(callId);
//                    if (m_service.call_get_info(callId, callInfo) == PJSIP.sf_PJ_SUCCESS) {
//
//                        callInfo.setRemoteContact(callInfoOutbound.getRemoteContact());
//                        callInfo.setAccountId(acc_id);
//                        
//                        if (!getLines().call_replace( line, callInfo)) {
//                        	
//                            if (LogSettings.MARKET) {
//                                MktLog.e(TAG, "Making call failed. Could not replace call info. Hangup. Call ID:" + callId );
//                            }
//                        	
//                            m_service.call_hangup(callId, 0);
//                            
//                        } else {
//                            if (LogSettings.MARKET) {
//                                    MktLog.i(TAG, "Make [" + line + "] callID [" + callId + "] state [" + callInfo.getCall_state() + "] " + callInfoOutbound.getRemoteContact());
//                            }
//                        }
//                    }
//                } else {
//                	
//                    if (LogSettings.MARKET) {
//                        MktLog.e(TAG, "Make call failed");
//                    }
//                    
//                    sendMessage( new CompletionInfo(true, false, CompletionInfo.ERROR_MAKE_CALL_1, 0, null, false));
//                    return;
//                }
//            } catch (Throwable e) {
//            	
//                if (LogSettings.MARKET) {
//                    MktLog.e(TAG, "Make call failed: RemoteExcepton: ", e );
//                }
//                
//                sendMessage( new CompletionInfo(true, false, CompletionInfo.ERROR_MAKE_CALL_2, 0, null, false));    
//            }
//		}
//		
//	};
	
	/**
	 * Check if we should play Ring tones now
	 */
//    protected void ProcessCheckRing(){
//		Thread processCheckRingTh = new Thread( new CheckRing());
//		processCheckRingTh.start();
//    }
	
	/**
	 * 
	 */
//	private class CheckRing implements Runnable{
//
//		@Override
//		public void run() {
//			
//			final boolean isOutboundCallPresent = isOutboundCallsPresent();
//			
//		    if (LogSettings.ENGINEERING) {
//		    	EngLog.i(TAG, "CheckRing Starting Outbound calls present: " + isOutboundCallPresent );
//		    }
//			
//			if( isOutboundCallPresent ){
//				if( m_service != null ){
//					try {
//						final int active_calls = m_service.call_get_count();
//						
//						/*
//						 * If we have potential outbound call then start Ring
//						 * Other active calls are set on Hold 
//						 */
//						if( active_calls > 0 ){
//			                m_service.call_hold_all();
//						}
//
//						ringOn();
//						
//					    if (LogSettings.ENGINEERING) {
//					    	EngLog.i(TAG, "CheckRing RingOn. Calls: " + active_calls );
//					    }
//						
//					} catch (RemoteException e) {
//			            if (LogSettings.MARKET) {
//			                MktLog.e(TAG, "CheckRing failed ", e);
//			            }
//					}
//				}
//			}
//		}
//		
//	}

	/**
	 * Ring Off
	 */
	public void ringOff(){
		// TDOD : Make asynchronous
	    if (LogSettings.ENGINEERING) {
	    	EngLog.i(TAG, "ringOff Starting" );
	    }
		
		if( m_service == null )
			return;

        try {
			m_service.ring_stop();
		} catch (java.lang.Throwable e) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "ringOff failed ", e);
            }
		}
	}
	
//	/**
//	 * Ring On
//	 */
//	public void ringOn(){
//
//	    if (LogSettings.ENGINEERING) {
//	    	EngLog.i(TAG, "ringOn Starting " );
//	    }
//		
//		if( m_service == null )
//			return;
//		
//        int[] devices;
//		try {
//			
//			devices = m_service.get_snd_dev();
//	        if( devices != null ){
//			    if (LogSettings.ENGINEERING) {
//			    	EngLog.i(TAG, "call_make_call Sound devices Capture:" + devices[0] + 
//			    			" Playback:" + devices[1] );
//			    }
//			    
//			    if( devices[0] < 0 || devices[1] < 0 ){
//			    	
//	                if (LogSettings.ENGINEERING) {
//	                	EngLog.i(TAG, "makeCall Set sound devices");
//	                }
//			    	m_service.set_snd_dev();
//			    }
//	        }
//
//	        m_service.ring_start();
//	        
//		} catch (RemoteException e) {
//            if (LogSettings.MARKET) {
//                MktLog.e(TAG, "ringOn failed ", e);
//            }
//		}
//	}
	
	/**
	 * 
	 */
	public synchronized long getCurrentLineCallId(){
		return getLines().getCurrentLineCallId();
	}
	
	/**
	 * 
	 */
	public synchronized final CallInfo getCurrentLineCallInfo(){
		return getLines().getCurrentLineCallInfo();
	}
	
	/**
	 * 
	 */
//	public synchronized boolean isOutboundCallsPresent(){
//		return ( getLines().getOutboundLineIndex() != SipClientLines.sf_INVALID_LINE_INDEX );
//	}
	/**
	 * 
	 */
	public synchronized final CallInfo getCallbyCallId( int callId ){
		return getLines().getCallbyCallId( callId );
	}
	/**
	 * 
	 */
//	public synchronized final int getLineIndexbyCallId( int callId ){
//		return getLines().getLineIndexbyCallId(callId);
//	}
	
    public synchronized final void setCurrentLine( int line ){
    	getLines().setCurrentLine(line);
    }

    public synchronized final int getCurrentLine(){
    	return getLines().getCurrentLine();
    }
	
    public synchronized final CallInfo getLineCallInfo( int line ){
    	return getLines().getLineCallInfo(line);
    }
    
    public void setRemoteContact( int line, String contact ){
    	getLines().setRemoteContact(line, contact);
    }

    public void setFinishingStatus(){
    	getLines().setFinishing(getLines().getCurrentLine(), true);
    }
    
    public void removeCurrentLine(){
    	if( getLines().call_remove_by_index( getLines().getCurrentLine() )){    	
    		sendMessage( sf_update_call_state, 0, null );
    	}
    }
	/**
	 * 
	 */
	public synchronized final int addCallInfo( final CallInfo callInfo, boolean setCurrent ){
		return getLines().call_add( callInfo, setCurrent );
	}
	
    /***************************************************************************************************************************************
     */
    private class ConfAdjustRXlevelCommand extends TimedSIProxyTask {
        private float level;
        int status = 0;
        public ConfAdjustRXlevelCommand(float level) {
            super("ConfAdjustRXlevelCommand");
            this.level = level;
        }

        @Override
        public void doServiceRequest(IServicePJSIP service) throws RemoteException {
            status = service.conf_adjust_rx_level(0, level);
        }
    }
	/**
	 * Adjust RX level
	 */
	public boolean conf_adjust_rx_level( float level ){

    	if( m_service == null ){
    		return false;
    	}
    	
    	ConfAdjustRXlevelCommand task = new ConfAdjustRXlevelCommand(level);
        if (task.execute(RCSIP.TIMEOUT_FOR_SERVICE_NOT_CRITICAL_CALLS, false) != TimedSIProxyTask.COMPLETED) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "ConfAdjustRXlevelCommand:failed");
            }
            return false;
        }
        
        return (task.status == PJSIP.sf_PJ_SUCCESS);
	}
    /***************************************************************************************************************************************
     */
    private class ChangeSpeakerRouteCommand extends TimedSIProxyTask {
        public ChangeSpeakerRouteCommand() {
            super("ChangeSpeakerRouteCommand");
        }

        @Override
        public void doServiceRequest(IServicePJSIP service) throws RemoteException {
            service.toggleSpeakerState();
        }
    }

    /**
     * Change speaker route from internal to external and via versa.
     */
    public void changeSpeakerRoute() {

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "changeSpeakerRoute()");
        }

        if (null != m_service) {
            ChangeSpeakerRouteCommand task = new ChangeSpeakerRouteCommand();
            if (task.execute(RCSIP.TIMEOUT_FOR_SERVICE_NOT_CRITICAL_CALLS, false) != TimedSIProxyTask.COMPLETED) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "ChangeSpeakerRouteCommand:failed");
                }
            }
        }
    }
    /***************************************************************************************************************************************
     */
    private class ChangeBluetoothRouteCommand extends TimedSIProxyTask {
        public ChangeBluetoothRouteCommand() {
            super("ChangeBluetoothRouteCommand");
        }

        @Override
        public void doServiceRequest(IServicePJSIP service) throws RemoteException {
            service.toggleBluetoothState();
        }
    }
    
    public void changeBluetoothRoute() {
        if (null != m_service) {
            ChangeBluetoothRouteCommand task = new ChangeBluetoothRouteCommand();
            if (task.execute(RCSIP.TIMEOUT_FOR_SERVICE_NOT_CRITICAL_CALLS, false) != TimedSIProxyTask.COMPLETED) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "ChangeBluetoothRouteCommand:failed");
                }
            }
        }
    }
    /***************************************************************************************************************************************
     */
    private class IsPlayBackDeviceCommand extends TimedSIProxyTask {
        boolean isPlayBackDevice;
        public IsPlayBackDeviceCommand() {
            super("IsPlayBackDeviceCommand");
        }

        @Override
        public void doServiceRequest(IServicePJSIP service) throws RemoteException {
            isPlayBackDevice = service.isPlayBackDevice();
        }
    }
    
    /**
     * Detect is Playback device available
     */
    public boolean isPlaybackDevice() {

        if (m_service == null) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "isPlaybackDevice: m_service is null");
            }
            return false;
        }
        
        IsPlayBackDeviceCommand task = new IsPlayBackDeviceCommand();
        if (task.execute(RCSIP.TIMEOUT_FOR_SERVICE_NOT_CRITICAL_CALLS, false) != TimedSIProxyTask.COMPLETED) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "IsPlayBackDeviceCommand:failed");
            }
            return false;
        }
        return task.isPlayBackDevice;
    }
    
//    /**
//     * Due to that PJSIP service function is called during
//     * processing account register state we should execute this
//     * function on separate thread. On other case deadlock can be occurred. 
//     */
//    protected void ProcessAccountChangedRegisterState(int accountId, boolean registered, int code){
//		Thread processAccRegTh = new Thread( new AccountChangedRegisterState(accountId, registered, code), "ProcessAccountChangedRegisterState");
//		processAccRegTh.start();
//    }
//    /*
//     * 
//     */
//    private class AccountChangedRegisterState implements Runnable {
//    	int m_accountId = 0;
//		boolean m_registered = false;
//		int m_code = 0;
//		
//    	public AccountChangedRegisterState(int accountId, boolean registered, int code){
//    		m_accountId = accountId;
//    		m_registered = registered;
//    		m_code = code;
//    	}
//    	
//		@Override
//		public void run() {
//			
//            if (LogSettings.MARKET) {
//                MktLog.d(TAG, "AccountChangedRegisterState Started" );
//            }
//			
//            if( !m_registered ){
//                if (LogSettings.MARKET) {
//                    MktLog.i(TAG, "account_changed_register_state Unregistered." );
//                }
//                
//            	setAccount(null);
//        		//sendMessage( new CompletionInfo(true, false, RCCallInfo.CALL_ENDED_BY_ERROR, CompletionInfo.ERROR_REGISTER_ACCOUNT, 0, null, false));
//            	return;
//            }
//            
//            boolean justRegistered = ( getAccount() == null );
//            if (LogSettings.MARKET) {
//                MktLog.d(TAG, "account_changed_register_state Just registered: " + justRegistered);
//            }
//            
//        	if( getAccount() != null ){
//        		getAccount().setRegistered(m_registered);
//        	}
//        	else{
//        		
//        		if( !loadAccount( m_registered )){
//        			return;
//        		}
//        	}
//        	
//        	/*
//        	 * 
//        	 */
//        	if( justRegistered ){
//
//                if( m_registered ){
//                    if (LogSettings.MARKET) {
//                        MktLog.i(TAG, "account_changed_register_state Just registered. Send notification make_call." );
//                    }
////                	sendMessage( SipClient.sf_make_call, 0, null );
//                }
//                else{
//                    if (LogSettings.MARKET) {
//                        MktLog.i(TAG, "account_changed_register_state Just registered. Send notification ERROR REGISTER." );
//                    }
//                	sendMessage( new CompletionInfo(true, false, RCCallInfo.CALL_ENDED_BY_ERROR, CompletionInfo.ERROR_REGISTER_ACCOUNT, 0, null, false));
//                }
//        	}
//        	
//		}
//    }
    
//    /**
//     * Load Account Info
//     */
//    protected boolean loadAccount( final boolean registered ){
//		AccountInfo accountInfo = new AccountInfo();
//		int status;
//		try {
//			status = getService().acc_get(accountInfo);
//		} catch (RemoteException e) {
//			status = PJSIP.sf_PJ_EUNKNOWN;
//		}
//		
//		if( PJSIP.sf_PJ_SUCCESS == status ){
//            if (LogSettings.MARKET) {
//                MktLog.i(TAG, "loadAccount Add accoint. Registered: " + registered);
//            }
//			accountInfo.setRegistered(registered);
//			setAccount(accountInfo);
//		}
//		else{
//			
//            if (LogSettings.MARKET) {
//                MktLog.e(TAG, "loadAccount  acc_get failed Status:" + status );
//            }
//            return false;
//		}
//    	
//		return true;
//    }
    
    protected void CheckMuteState(){
    	Thread thMuteState = new Thread ( new CheckMuteStatus(), "CheckMuteState");
    	thMuteState.start();
    }
    /*
     * Check mute status
     */
    private class CheckMuteStatus implements Runnable{

		@Override
		public void run() {
			float adjust_rx_level;
			try {
				adjust_rx_level = getService().conf_get_adjust_rx_level(0);
		    	int muteValue = ( adjust_rx_level < getUnMuteLevel() ? MUTE_ON : MUTE_OFF );
		    	
	            if (LogSettings.MARKET) {
	                MktLog.d(TAG, "CheckMuteStatus() set MUTE to:" + muteValue + " level:" + adjust_rx_level );
	            }
		    	
				sendMessage( sf_set_mute, muteValue, null );
            	sendMessage( sf_update_controls, 0, null );
				
			} catch (RemoteException e) {
	            if (LogSettings.MARKET) {
	                MktLog.w(TAG, "CheckMuteStatus() conf_get_signal_rx_level  failed", e );
	            }
			}
		}
    	
    }
    		
}
