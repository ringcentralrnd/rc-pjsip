/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import android.os.SystemClock;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.client.RCSIP;
import com.rcbase.api.xml.XmlParsingObjectAbstract;
import com.rcbase.parsers.sipmessage.SipMessageInput;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.provider.RCMProviderHelper;

/**
 * @author Denis Kudja
 *
 */
public class WrapPJSIP {

    private static final String TAG = "[RC]WrapPJSIP";
	
	public static final int RC_CALL_INFO_VALUES_COUNT	 =  6;
	public static final int RC_CALL_INFO_STATUS_COUNT	  = 5;
	
	
	public WrapPJSIP(){
	}
	
	protected boolean isSipStackCreated(){
		return ServicePJSIP.getInstance().getState().isSipStackCreated();
	}
	
	protected synchronized void PJSIP_pjsip_ring_start(final int pjsipCallId ){
	    if (LogSettings.MARKET) {
	        MktLog.i(TAG, "RING START: PJSIP callId: " + pjsipCallId );
	    }
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_pjsip_ring_start: Failed! SIP Stack is not loaded yet." );
		    }
		    return;
	    }
	    
	    ServicePJSIP.getInstance().startStopRingDelay();
	    
	    PJSIP.pjsip_ring_start( pjsipCallId );
	}
	
	protected synchronized void PJSIP_pjsip_ring_stop( final int pjsipCallId ){
		
	    if (LogSettings.MARKET) {
	        MktLog.i(TAG, "RING STOP: PJSIP callId: " + pjsipCallId );
	    }
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_pjsip_ring_stop: Failed! SIP Stack is not loaded yet." );
		    }
		    return;
	    }
	    
	    ServicePJSIP.getInstance().removeDelayRingStop();
	    
	    PJSIP.pjsip_ring_stop( pjsipCallId );
	}
	
	/*
	 * 
	 */
	protected synchronized int PJSIP_message_send(int accountId, String dstURI, String content){
	    if (LogSettings.MARKET) {
	        MktLog.i(TAG, "PJSIP_message_send URI: " + dstURI );
	    }
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_message_send: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		int result = PJSIP.pjsip_message_send(accountId, dstURI, content);
		
		if( result != PJSIP.sf_PJ_SUCCESS){
		    if (LogSettings.MARKET) {
		        MktLog.e(TAG, "PJSIP_message_send failed Error " + result );
		    }
		}
		
		return result;
	}
	
	protected int PJSIP_call_send_request( int pjsipCallId, String content, String clientId ){
	    if (LogSettings.MARKET) {
	        MktLog.i(TAG, "PJSIP_call_send_request PJSIP callID: " + pjsipCallId + " clientId: " + clientId );
	    }
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_call_send_request: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		int result = PJSIP.pjsip_call_send_request( pjsipCallId, content, clientId );
		
		if( result != PJSIP.sf_PJ_SUCCESS){
		    if (LogSettings.MARKET) {
		        MktLog.e(TAG, "PJSIP_call_send_request failed Error " + result );
		    }
		}
		
		return result;
	}
	
	private String[] prepareHeaders(final String[] headers){
		String[] sheaders;
		
		if( headers == null || headers.length == 0 ) 
			sheaders = new String[0];
		else{
			sheaders = new String[headers.length * 2];

			int shdr_idx = 0;
			for( int i =0; i < headers.length; i++ ){
			    if (LogSettings.MARKET) {
			        MktLog.i(TAG, "PJSIP_call_answer Header " + headers[i] );
			    }
				int off = headers[i].indexOf(":");
				if( off <= 0 ) 
					return null;
				
				sheaders[ shdr_idx++ ] = headers[i].substring(0, off );
				sheaders[ shdr_idx++ ] = headers[i].substring( off + 1 );
			}
		}
		
		return sheaders;
	}
	protected int PJSIP_call_answer(int pjsipCallId, int code, final String[] headers ){
		
	    if (LogSettings.MARKET) {
	        MktLog.i(TAG, "PJSIP_call_answer PJSIP callId: " + pjsipCallId );
	    }
		String[] sheaders = prepareHeaders( headers );
		
		if( sheaders == null )
			return PJSIP.sf_PJ_EINVAL;
		
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_call_answer: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		return PJSIP.pjsip_call_answer(pjsipCallId, code, sheaders );
	}
	
	protected int PJSIP_call_hangup(int pjsipCallId, int code, final String[] headers){
		
	    if (LogSettings.MARKET) {
	        MktLog.i(TAG, "PJSIP_call_hangup PJSIP callId: " + pjsipCallId );
	    }
	    
		String[] sheaders = prepareHeaders( headers );
		
		if( sheaders == null )
			return PJSIP.sf_PJ_EINVAL;
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_call_hangup: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		final int result = PJSIP.pjsip_call_hangup(pjsipCallId, code, sheaders );
		
		/*
		 * Update active calls counter
		 */
    	ServicePJSIP.getInstance().updateActiveCallsCounter();
    	
		return result;
	}
	
	protected void PJSIP_call_hangup_all(){
	    if (LogSettings.MARKET) {
	        MktLog.i(TAG, "PJSIP_call_hangup_all" );
	    }
	    
		PJSIP.pjsip_call_hangup_all();
		
		/*
		 * Update active calls counter
		 */
    	ServicePJSIP.getInstance().updateActiveCallsCounter();
	}
	
	protected int PJSIP_call_hold(int pjsipCallId){
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_call_hold: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		return PJSIP.pjsip_call_hold(pjsipCallId);
	}
	
	
	protected int PJSIP_call_unhold(int pjsipCallId){
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_call_unhold: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		return PJSIP.pjsip_call_unhold(pjsipCallId);
	}
	
	protected synchronized int PJSIP_call_transfer( int pjsipCallId, String remoteURI ){
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_call_transfer: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		return PJSIP.pjsip_call_transfer(pjsipCallId, remoteURI);
	}
	
	protected synchronized int PJSIP_call_make_call(final int accountId, final String remoteURI, final String[] headers){
		
		String[] sheaders = prepareHeaders( headers );
		
		if( sheaders == null ){
			return PJSIP.sf_INVALID_CALL_ID;
		}
		
		int[] callId = new int[1];
		int result = PJSIP.pjsip_call_make( accountId, remoteURI, sheaders, callId );
		
		if( result != PJSIP.sf_PJ_SUCCESS){
		    if (LogSettings.MARKET) {
		        MktLog.e(TAG, "PJSIP_call_make_call failed Error " + result );
		    }
			return PJSIP.sf_INVALID_CALL_ID;
		}
		
		if (LogSettings.MARKET) {
		    MktLog.i(TAG, "PJSIP_call_make_call PJSIP callId " + callId[0] );
		}
		
		return callId[0];
	}

    public static class PJSIPCallInfo {
        public int callId = PJSIP.sf_INVALID_CALL_ID;
        public int callSate = RCSIP.PJSUA_CALL_STATE_INIT;
        public int accountId = PJSIP.sf_INVALID_ACCOUNT_ID;
        public int mediaState   = RCSIP.PJSUA_CALL_MEDIA_NONE;
        public int statusCode = RCSIP.SipCode.PJSIP_SC_OK.code();
        public boolean onHold = false;
        public String localURI = null;
        public String localContact = null;
        public String remoteURI = null;
        public String remoteContact = null;
        public String dialogCallId = null;
        public SipMessageInput sipMsg = null;
        
        @Override
        public String toString() {
            StringBuffer sb = new StringBuffer();
            
            sb.append("PJSIP Call ");
            sb.append(callId);
            
            sb.append(", State:");
            sb.append(RCSIP.getCallStateForLog(callSate));
            
            sb.append(", AccountId:");
            sb.append(accountId);
            
            sb.append(", MediaState:");
            sb.append(RCSIP.getMediaStatusLabel(mediaState));
            
            sb.append(", OnHold:");
            sb.append(onHold);
            
            sb.append(", Party:");
            sb.append(normalizedString(CallInfo.getNumberFromURI(remoteURI)));
            
            if (sipMsg == null) {
                sb.append(", SipMsg:null");
            } else {
                sb.append(", SipMsg:exists");
            }
            
            sb.append("  StatusCode:");
            sb.append(RCSIP.SipCode.get(statusCode).msg());
            

            
            return sb.toString();
        }
        
        public String description() {
            StringBuffer sb = new StringBuffer();
            
            sb.append(toString());
            
            sb.append('\n');
            sb.append("  LocalURI:");
            sb.append(normalizedString(localURI));
            
            sb.append('\n');
            sb.append("  LocalContact:");
            sb.append(normalizedString(localContact));
            
            sb.append('\n');
            sb.append("  RemoteURI:");
            sb.append(normalizedString(remoteURI));
            
            sb.append('\n');
            sb.append("  RemoteContact:");
            sb.append(normalizedString(remoteContact));
            
            sb.append('\n');
            sb.append("  DialogCallId:");
            sb.append(normalizedString(dialogCallId));
            
            return sb.toString();
        }
        
        public String minDescription() {
            StringBuffer sb = new StringBuffer();
            
            sb.append(toString());

            sb.append('\n');
            sb.append("  RemoteURI:");
            sb.append(normalizedString(remoteURI));
            return sb.toString();
        }

    }
    
    private static String normalizedString(String string) {
        if (string == null) {
            return "null";
        } else {
            return string;
        }
    }
    
    /**
     * Returns PJSIP call info.
     * 
     * @param pjsipCallId PJSIP Call Id
     * @param call convert to fill
     * @return PJSIP result
     */
    protected int PJSIP_get_call_info(final int pjsipCallId, PJSIPCallInfo call) {
        if (call == null) return PJSIP.sf_PJ_EIGNORED;
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "PJSIP_get_call_info PJSIP CallId:" + pjsipCallId);
        }
        if( !isSipStackCreated()){
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "PJSIP_get_call_info failed! SIP Stack is not loaded yet." );
            }
            return PJSIP.sf_PJ_EIGNORED;
        }
        
        /**
         * Call values are:
         * 
         * [0]  Local URI
         * [1]  Local Contact
         * [2]  Remote URI
         * [3]  Remote contact
         * [4]  Dialog Call-ID string
         * 
         * Status are:
         * [0]  state - Call state
         * [1]  status code - Last status code heard, which can be used as cause code
         * [2]  media status - Call media status
         * [3]  account ID
         * [4]  status (Hold, Finishing)
         */

        
        String[] values = new String[RC_CALL_INFO_VALUES_COUNT];
        int[] states = new int[RC_CALL_INFO_STATUS_COUNT];
        
        int result = PJSIP.pjsip_call_get_info(pjsipCallId, values, states);
        if( result != PJSIP.sf_PJ_SUCCESS){
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "PJSIP_get_call_info failed. Error " + result );
            }
            return result;
        }
        call.callId = pjsipCallId;
        call.localURI = values[0];
        call.localContact = values[1];
        call.remoteURI = values[2];
        call.remoteContact = values[3];
        call.dialogCallId = values[4];
        
        if( values[5] != null ){
            final String msg = WrapPJSIP.cutMessage( values[5], WrapPJSIP.s_End_Msg );
            SipMessageInput sipMsg = WrapPJSIP.parseRCMessage(msg);
            call.sipMsg = sipMsg;
        }
        
        call.callSate = states[0];
        call.statusCode = states[1];
        call.accountId = states[3];
        call.mediaState = states[2];
        
        if( states[4] >= 0 ){
            call.onHold = ( ( states[4] & PJSIP.RC_CALL_STATUS_HOLD ) > 0 );
        }
        return result;
    }
    
	/*
	 * 
	 */
	protected int PJSIP_call_dial_dtmf(final int pjsipCallId, final String digits ){
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		return PJSIP.pjsip_call_dial_dtmf( pjsipCallId, digits );
	}

    /*
	 * 
	 */
    protected int PJSIP_call_media_disconnect(final int pjsipCallId) {

        if (!isSipStackCreated()) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "PJSIP_call_media_disconnect: Failed! SIP Stack is not loaded yet.");
            }
            return PJSIP.sf_PJ_EIGNORED;
        }

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "call_detach_sound PJSIP Call ID: " + pjsipCallId);
        }
        return PJSIP.pjsip_call_meia_disconnect(pjsipCallId);
    }
	
		
	
	protected synchronized int PJSIP_acc_get_default(){
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_acc_get_default: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		return PJSIP.pjsip_acc_get_default();
	}
	
	protected synchronized boolean PJSIP_pjsip_acc_is_valid( final int accountId ){
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_pjsip_acc_is_valid: Failed! SIP Stack is not loaded yet." );
		    }
		    return false;
	    }
		
		return (  PJSIP.pjsip_acc_is_valid(accountId) > 0 );
	}
	
	protected synchronized boolean PJSIP_pjsip_acc_is_registered( final int accountId ){
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_pjsip_acc_is_registered: Failed! SIP Stack is not loaded yet." );
		    }
		    return false;
	    }
		
		int[] acc_status = new int[1];
		int status = PJSIP.pjsip_acc_get_status( accountId, acc_status);
		if( status != PJSIP.sf_PJ_SUCCESS ){
			return false;
		}

		return ( acc_status[0] == RCSIP.SipCode.PJSIP_SC_OK.code());	    	
	}
	
	protected int PJSIP_call_attach_sound(final int pjsipCallId){
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_call_attach_sound: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		return PJSIP.pjsip_call_attach_sound(pjsipCallId);
	}
	
	protected synchronized int PJSIP_call_get_count(){
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_call_get_count: Failed! SIP Stack is not loaded yet." );
		    }
		    return 0;
	    }
		
		return PJSIP.pjsip_call_get_count();
	}
	protected synchronized int PJSIP_call_get_max_count(){
		return PJSIP.pjsip_call_get_max_count();
	}
	
	protected boolean PJSIP_call_is_active(final int pjsipCallId){
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_call_is_active: Failed! SIP Stack is not loaded yet." );
		    }
		    return false;
	    }
		
		return ( 0 == PJSIP.pjsip_call_is_active(pjsipCallId) ? false : true );
	}
	
	protected synchronized boolean PJSIP_call_has_media(final int pjsipCallId){
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "Failed! SIP Stack is not loaded yet." );
		    }
		    return false;
	    }
		
		return ( 0 == PJSIP.pjsua_call_has_media(pjsipCallId) ? false : true );
	}
	
	protected synchronized long PJSIP_call_get_duration( final int pjsipCallId ){
		long[] duration = new long[2];
		int status = PJSIP.pjsip_call_get_duration(pjsipCallId, duration);
		
		return ( status != PJSIP.sf_PJ_SUCCESS ? -1 : duration[1] );
	}
	
	protected synchronized int PJSIP_acc_del( final int accountId ){
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "PJSIP_acc_del delete accoint:" + accountId );
        }
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_acc_del: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		return PJSIP.pjsip_acc_del(accountId);
	}
	
	protected synchronized int PJSIP_acc_add(final AccountInfo accountInfo, long forcedExpires){
		
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "PJSIP_add_acc is starting...");

            MktLog.i(TAG, "PJSIP_add_acc Account " + accountInfo.getAccount() + " Login " + accountInfo.getLoginId() + " Pwd ***");

            MktLog.i(TAG, "PJSIP_add_acc SrvProvider " + accountInfo.getServiceProvider() + " OutProxy " + accountInfo.getSipOutboundProxy());
        }
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_acc_add: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		String[] params = new String[5];
		params[0] = accountInfo.getAccount(); 
		params[1] = accountInfo.getServiceProvider(); 
		params[2] = accountInfo.getLoginId();
		//params[2] = "18885287464*5732";
		//params[2] = "1011";
		//params[2] = "7040";
		//params[2] = "1019";
		//params[2]=RCMProviderHelper.getTSCEnalbe(RingCentralApp.getContextRC()) ? "7040":"1019";
		params[3] = accountInfo.getPsw(); 
		params[4] = accountInfo.getSipOutboundProxy(); 
		
		int[] accountId = new int[1];
		
		int result = PJSIP.pjsip_acc_add(params, accountId, forcedExpires);
        if (result != PJSIP.sf_PJ_SUCCESS) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "PJSIP_start pjsip_acc_add failed. Status:" + result);
            }
        } else {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "PJSIP_start pjsip_acc_add finished");
            }
        }
		
		return result;
	}

	/*
	 * Return list of active calls ID
	 */
	public int[] PJSIP_getActiveCallsList(){
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_getActiveCallsList: Failed! SIP Stack is not loaded yet." );
		    }
		    return new int[0];
	    }
	    
		int size = PJSIP.pjsip_call_get_max_count();
		int[] calls = new int[size];
		int[] count = new int[1];
		count[0] = size;
		
		int status = PJSIP.pjsip_call_enum_calls(calls, count);
		if( status != PJSIP.sf_PJ_SUCCESS || count[0] <= 0 ){
			return new int[0];
		}
		
		int[] result = new int[ count[0] ];
		for( int i = 0; i < count[0]; i++ ){
			result[i] = calls[i];
		}
		
		return result;
	}
	
	/**
	 * Returns current codec set from PJSIP.
	 * 
	 * @return current codec set
	 */
	protected synchronized CodecInfo[] PJSIP_enum_codecs(){
	    if (LogSettings.MARKET) {
	        MktLog.d(TAG, "PJSIP_enum_codecs is starting...");
	    }
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_enum_codecs: Failed! SIP Stack is not loaded yet." );
		    }
		    return new CodecInfo[0];
	    }
	    
		String[] codecId = new String[32];
		int[] codecPrio = new int[32];
		int[] codecCount = new int[1];
		
		int status = PJSIP.pjsip_enum_codecs( codecId, codecPrio, codecCount );
		//work around to add silk to it
		/*codecId[codecCount[0]]="SILK/8000/1";
		codecPrio[codecCount[0]]=128;
		codecCount[0]=codecCount[0]+1;*/
		
		if( status != PJSIP.sf_PJ_SUCCESS || codecCount[0] == 0 ){
			if( status == PJSIP.sf_PJ_ETOOSMALL ) {
			    if (LogSettings.MARKET) {
			        MktLog.i(TAG, "PJSIP_enum_codecs buffer is too small");
			    }
			}
			
			return new CodecInfo[0];
		}
	
		if (LogSettings.MARKET) {
		    MktLog.i(TAG, "PJSIP_enum_codecs: " + codecCount[0] + " codecs detected:");
		}
		
		CodecInfo[] codecInfo = new CodecInfo[codecCount[0]];
		for( int i = 0; i < codecCount[0]; i++ ){
			codecInfo[i] = new CodecInfo(codecId[i], codecPrio[i]);
			if (LogSettings.MARKET) {
			    MktLog.i(TAG, "== " + codecInfo[i].toString() );
			}
		}
		
		Arrays.sort(codecInfo, new CodecInfoComparator());
		return codecInfo;
	}
	
	/*
	 * Set codec priority
	 */
	protected synchronized int PJSIP_codec_set_priority( final String codecId, final int priority ){
	    if (LogSettings.MARKET) {
	        MktLog.i(TAG, "PJSIP_codec_set_priority is starting...");
	    }
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_codec_set_priority: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		return PJSIP.pjsip_codec_set_priority(codecId, priority );
	}
	
	protected synchronized int PJSIP_conf_adjust_tx_level( int slot, float level ){
	    if (LogSettings.MARKET) {
	        MktLog.i(TAG, "PJSIP_conf_adjust_tx_level is starting...Level:" + level );
	    }
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_conf_adjust_tx_level: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		return PJSIP.pjsip_conf_adjust_tx_level(slot, level);
	}
	
	protected synchronized int PJSIP_conf_adjust_rx_level( int slot, float level ){
	    if (LogSettings.MARKET) {
	        MktLog.i(TAG, "PJSIP_conf_adjust_rx_level is starting...Level:" + level );
	    }
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_conf_adjust_rx_level: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		return PJSIP.pjsip_conf_adjust_rx_level(slot, level);
	}
	
	protected synchronized float PJSIP_conf_get_adjust_tx_level( int slot )
	{
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_conf_get_adjust_tx_level: Failed! SIP Stack is not loaded yet." );
		    }
		    return 128;
	    }
		
		return PJSIP.pjsip_conf_get_adjust_tx_level( slot );
	}

	protected synchronized float PJSIP_conf_get_adjust_rx_level( int slot )
	{
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_conf_get_adjust_rx_level: Failed! SIP Stack is not loaded yet." );
		    }
		    return 128;
	    }
		
		return PJSIP.pjsip_conf_get_adjust_rx_level( slot );
	}
	
	protected synchronized float PJSIP_conf_get_signal_rx_level( int slot ){
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_conf_get_signal_rx_level: Failed! SIP Stack is not loaded yet." );
		    }
		    return 0;
	    }
		
		return PJSIP.pjsip_conf_get_signal_rx_level( slot );
	}
	
	protected synchronized float PJSIP_conf_get_signal_tx_level( int slot ){
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_conf_get_signal_tx_level: Failed! SIP Stack is not loaded yet." );
		    }
		    return 0;
	    }
		
		return PJSIP.pjsip_conf_get_signal_tx_level( slot );
	}
	
	public boolean isPlayBackDevice(String by) {
	    if (!isSipStackCreated()) {
	        return false;
	    }
	    if ((SystemClock.elapsedRealtime() - mLastKnowSndState) > 1000) {
	        PJSIP_pjsip_get_snd_dev(by);
	    }
	    return isPlayBackDevice;
	}
	
	private volatile long mLastKnowSndState = 0;
	private volatile boolean isPlayBackDevice = false;
	
    public int PJSIP_pjsip_set_snd_dev(String by) {
        long injectTime = SystemClock.elapsedRealtime();
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "pjsip_set_snd_dev: " + by);
        }
        isPlayBackDevice = true;
        mLastKnowSndState = SystemClock.elapsedRealtime();
        synchronized (this) {
            long launchTime = SystemClock.elapsedRealtime();
            if (!isSipStackCreated()) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "PJSIP_pjsip_set_snd_dev: Failed! SIP Stack is not loaded yet. Waited:" + (launchTime - injectTime));
                }
                isPlayBackDevice = false;
                mLastKnowSndState = SystemClock.elapsedRealtime();
                return PJSIP.sf_PJ_EIGNORED;
            }

            int result = PJSIP.pjsip_set_snd_dev(0, 0);
            isPlayBackDevice = (result == PJSIP.sf_PJ_SUCCESS);
            mLastKnowSndState = SystemClock.elapsedRealtime();
            long finish = SystemClock.elapsedRealtime();

            if (((finish - launchTime) > 1500) || ((launchTime - injectTime) > 1500)) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "pjsip_set_snd_dev: finished, by " + by + " Waited:" + (launchTime - injectTime) + " Launched:"
                            + (finish - launchTime));
                }
            }

            return result;
        }
    }
	
    public int[] PJSIP_pjsip_get_snd_dev(String by) {
        long injectTime = SystemClock.elapsedRealtime();

        if (LogSettings.MARKET) {
            MktLog.w(TAG, "> pjsip_get_snd_dev: " + by);
        }

        synchronized (this) {
            long launchTime = SystemClock.elapsedRealtime();
            if (!isSipStackCreated()) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "PJSIP_pjsip_get_snd_dev: Failed! SIP Stack is not loaded yet. Waited:" + (launchTime - injectTime));
                }
                isPlayBackDevice = false;
                mLastKnowSndState = SystemClock.elapsedRealtime();
                return null;
            }

            final int[] devices = new int[2];
            final int status = PJSIP.pjsip_get_snd_dev(devices);
            if (status != PJSIP.sf_PJ_SUCCESS) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "PJSIP_pjsip_get_snd_dev: status != PJSIP.sf_PJ_SUCCESS, by " + by + " Waited:" + (launchTime - injectTime));
                }
                isPlayBackDevice = false;
                mLastKnowSndState = SystemClock.elapsedRealtime();
                return null;
            }
            long finish = SystemClock.elapsedRealtime();

            if (((finish - launchTime) > 1500) || ((launchTime - injectTime) > 1500)) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "PJSIP_pjsip_get_snd_dev: finished, by " + by + " Waited:" + (launchTime - injectTime) + " Launched:"
                            + (finish - launchTime));
                }
            }
            mLastKnowSndState = SystemClock.elapsedRealtime();
            if (devices != null) {
                isPlayBackDevice = (devices[1] >= 0);
            }
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "< pjsip_get_snd_dev: " + by + " [" + devices[0] + "][" + devices[1] +"]");
            }
            return devices;
        }
    }
	
	public void PJSIP_pjsip_set_no_snd_dev(String by){
	    long injectTime = SystemClock.elapsedRealtime();
	    if (LogSettings.MARKET) {
	    	MktLog.w(TAG, "PJSIP_pjsip_set_no_snd_dev: by " + by);
	    }
        synchronized (this) {
            long launchTime = SystemClock.elapsedRealtime();
            if (!isSipStackCreated()) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "PJSIP_pjsip_set_no_snd_dev: Failed! SIP Stack is not loaded yet. Waited:" + (launchTime - injectTime));
                }
                mLastKnowSndState = SystemClock.elapsedRealtime();
                isPlayBackDevice = false;
                return;
            }

            PJSIP.pjsip_set_no_snd_dev();
            mLastKnowSndState = SystemClock.elapsedRealtime();
            isPlayBackDevice = false;
            long finish = SystemClock.elapsedRealtime();

            if (((finish - launchTime) > 1500) || ((launchTime - injectTime) > 1500)) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "PJSIP_pjsip_set_no_snd_dev: finished, by " + by + " Waited:" + (launchTime - injectTime) + " Launched:"
                            + (finish - launchTime));
                }
            }
        }
	}
	
	protected synchronized int PJSIP_pjsip_acc_set_registration( final int accountId, final boolean renew ){
	    if (LogSettings.MARKET) {
	        MktLog.d(TAG, "PJSIP_pjsip_acc_set_registration renew: " + renew );
	    }
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_pjsip_acc_set_registration: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		return PJSIP.pjsip_acc_set_registration(accountId, renew );
	}
	protected synchronized int PJSIP_pjsip_call_reinvate_all(){
	    if (LogSettings.MARKET) {
	        MktLog.d(TAG, "PJSIP_pjsip_call_reinvate_all");
	    }
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_pjsip_call_reinvate_all: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		return PJSIP.pjsip_call_reinvate_all();
	}
	protected synchronized int PJSIP_pjsip_media_transports_create( final int port ){
	    if (LogSettings.MARKET) {
	        MktLog.d(TAG, "PJSIP_pjsip_media_transports_create");
	    }
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_pjsip_media_transports_create: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		return PJSIP.pjsip_media_transports_create(port);
	}
	protected synchronized int PJSIP_pjsip_media_transports_update( final int port ){
	    if (LogSettings.MARKET) {
	        MktLog.d(TAG, "PJSIP_pjsip_media_transports_update");
	    }
		
	    if( !isSipStackCreated()){
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "PJSIP_pjsip_media_transports_update: Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
		
		return PJSIP.pjsip_media_transports_update(port);
	}
	
	protected static final String s_End_Msg = "</Msg>";
	protected static String cutMessage( final String msg, final String sEnd ){
		int offEnd = msg.indexOf(sEnd);
		if( offEnd > 0 ){
			return msg.substring(0, offEnd + sEnd.length());
		}		
		
		return null;
	}
	
	/**
	 * Parse RC header received as additional header on INVITE of incoming call
	 */
	protected static SipMessageInput parseRCMessage( final String msg ){
		
		if( msg == null )
			return null;
		
		ByteArrayInputStream bis = new ByteArrayInputStream( msg.getBytes() );
		XmlParsingObjectAbstract parent = null;
		SipMessageInput sipMsg = new SipMessageInput(parent);
		try {
			sipMsg.parseResponse(bis);
		} catch (ParserConfigurationException e) {
			sipMsg = null;
		} catch (SAXException e) {
			sipMsg = null;
		} catch (IOException e) {
			sipMsg = null;
		}
		
		return sipMsg;
	}

    public void PJSIP_pjsip_start_test() {
        PJSIP.pjsip_start_test();
    }

    public void PJSIP_pjsip_stop_test() {
        PJSIP.pjsip_stop_test();
    }
}
