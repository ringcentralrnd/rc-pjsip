/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;


/**
 * @author Denis Kudja
 *
 */
public interface IUserConfigurationInfo {

	public String getExtension();
	public String getPin();
	public String getPassword();
	
	public long getCurrentMailboxId();
	public boolean isValidMailboxId();
	
	public String getInstanceId();
	public void setInstanceId( String instanceId );
	
	public String getCallerID();
}
