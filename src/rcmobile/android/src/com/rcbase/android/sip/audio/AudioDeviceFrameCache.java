/**
 * Copyright (C) 2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.audio;

/**
 * Cache for <code>AudioDeviceFrame</code> to reuse frames.
 */
class AudioDeviceFrameCache {
    private static final int CACHE_SIZE = 10;
    private volatile AudioDeviceFrame[] mCache;
    private volatile int mSize = 0; 
    AudioDeviceFrameCache() {
        mCache = new AudioDeviceFrame[CACHE_SIZE];
        for (int i = 0; i < CACHE_SIZE; i++) {
            mCache[i] = null;
        }
    }
    
    AudioDeviceFrame getFrame(byte[] frameToCopy) {
        AudioDeviceFrame frame = null;
        synchronized (mCache) {
            if (mSize > 0) {
                for (int i = 0; i < CACHE_SIZE; i++) {
                    if (mCache[i] != null) {
                        frame = mCache[i];
                        mCache[i] = null;
                        mSize--;
                    }
                }
            }
        }

        if (frame != null) {
            if (frame.copyFrame(frameToCopy)) {
                return frame;
            }
        }
        return new AudioDeviceFrame(frameToCopy);
    }
    
    AudioDeviceFrame getFrame(int size) {
        AudioDeviceFrame frame = null;
        synchronized (mCache) {
            if (mSize > 0) {
                for (int i = 0; i < CACHE_SIZE; i++) {
                    if (mCache[i] != null) {
                        if (mCache[i].getFrame().length == size) {
                            frame = mCache[i];
                            mCache[i] = null;
                            mSize--;
                            return frame;
                        }
                    }
                }
            }
        }
        mNewFrames++;
        return new AudioDeviceFrame(size);
    }
    
    private volatile long mNewFrames = 0;
    private volatile long mPutFrameFull = 0;
    
    long getNewFrames() {
        return mNewFrames;
    }
    
    long getPutFrameFull() {
        return mPutFrameFull;
    }
    
    void putFrame(AudioDeviceFrame frame) {
        synchronized (mCache) {
            if (mSize >= CACHE_SIZE) {
                mPutFrameFull++;
                return;
            }
            for (int i = 0; i < CACHE_SIZE; i++) {
                if (mCache[i] == null) {
                    mCache[i] = frame;
                    mSize++;
                    return;
                }
            }
        }
    }
}
