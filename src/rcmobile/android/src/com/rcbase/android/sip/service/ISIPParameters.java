/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

/**
 * @author Denis Kudja
 *
 */
public interface ISIPParameters {

	/**
	 * Outbound Calls only flag.
	 * If TRUE then PJSIP service will be stopped after
	 * receiving broadcast notification. 
	 */
	public boolean isOutboundCallsOnly();
	
	/**
	 * Get VoIP enable/disable flag
	 */
	public boolean isVoipEnabled();
	
	/**
	 * Get VoIP Wi-Fi enable/disable flag 
	 */
	public boolean isVoipOverWiFiEnabled();
	
	/**
	 * Get VoIP over 3G/4G enabled/disabled flag
	 */
	public boolean isVoipOver3GEnabled();
	
	/**
	 * Use TCP for SIP registration
	 */
	public  boolean use_TCP_For_SIP_registration();
	
	/**
	 * Return current SIP settings
	 */
	public long getVoipSettings();
	
	public boolean isVoipOnServiceEnabled();
	public void setVoipOnServiceEnabled(final boolean enabled );
	
	public void setHttpRegSipFlags( final long flags );
	public long getHttpRegSipFlags();
	
	/**
	 * Get/Set SIP outbound proxy.
	 * This value will be received during HTTP register procedure.
	 */
	public String getSipOutboundProxy();
	public void setSipOutboundProxy(final String sipProxy );
	
	/**
	 * Get/Set service provider (for example sip.ringcentral.com)
	 */
	public String getServiceProvider();
	public void setServiceProvider(final String sipProvider);
}
