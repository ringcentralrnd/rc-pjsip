/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.client;


import java.util.LinkedList;
import java.util.List;

import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.client.SIProxy.ServiceRequest;
import com.rcbase.android.sip.service.CallInfo;
import com.rcbase.android.sip.service.IServicePJSIP;
import com.rcbase.android.sip.service.IServicePJSIPCallback;
import com.rcbase.android.sip.service.RCCallInfo;
import com.rcbase.android.utils.media.AudioState;
import com.rcbase.android.utils.media.RCMediaManager;
import com.rcbase.parsers.sipmessage.SipMessageInput;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.settings.AudioSetupWizard;

public class WizardClient implements SIProxyClient {
    private static final String TAG = "[RC]WizardClient";
    
    public static final long TEST_EXECUTION_TIMEOUT = 6000; //millis
    private static final long INTER_TEST_DELAY = 1000; //millis
    private static final long UI_UPDATE_DELAY = 50; //millis, to let UI update redraw it's state before heavy procedures for starting stack/routing audio
    
    private static final int MSG_TIMEOUT = 0;
    private static final int MSG_NEXT_TEST = 1;
    private static final int MSG_START_TEST = 2;
    private static final int MSG_RELEASE = 3;
    private static final int MSG_INIT = 4;
         
    private final Handler mNotificationHandler;    
    private final List<WizardClient.AudioTest> mAudioTests;    
    private IServicePJSIP mService = null;
    
	private AudioTest mCurrentTest;
	private Object mLock = new Object();

    private boolean isFinishing = false;
    private boolean mFlagStopStackOnExit = false;
    
    public WizardClient(Handler notificationHandler){
    	if (LogSettings.MARKET) {
			MktLog.v(TAG, "WizardClient.constructor()...");
		}
        mNotificationHandler = notificationHandler;        
        mAudioTests = new LinkedList<WizardClient.AudioTest>();        
        mHandler.sendEmptyMessageDelayed(MSG_INIT, UI_UPDATE_DELAY);
    }    

    @Override
	public void onServiceConnectionStateChange() {
		ServiceRequest service = SIProxy.getInstance().getService(this);
	    if (service.serviceState == SIProxy.SHUTDOWN) {
            mService = null;
            SIProxy.getInstance().removeClient(this);
            onTestFailed();
        } else if (service.serviceState == SIProxy.CONNECTED) {
            mService = service.serviceHandle;
            onServiceConnected();
        }
		
	}
    
	private void onServiceConnected() {
		if (LogSettings.MARKET) {
			MktLog.i(TAG, "onServiceConnected() is starting...");
		}
		
		if (!registerCallback()) {
			return;
		}
		try {			
			if (!mService.isStartedSipStack()) {
				mFlagStopStackOnExit = true;
			}
			mService.startSipStack(true);			
		} catch (Throwable t) {
			if (LogSettings.MARKET) {
				MktLog.e(TAG, "onServiceConnected(): Failed to start SIP Stack", t);
			}
		}
		if (LogSettings.MARKET) {
			MktLog.d(TAG, "onServiceConnected() finished");
		}
	}    
    
    private boolean registerCallback(){
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "registerCallback() starting, mService : " + mService);
        }
        
        if( mService != null){
	        try {
	            mService.registerCallback( mCallback.hashCode(), mCallback );            
	        } catch (Throwable t) {
	            if (LogSettings.MARKET) {
	                MktLog.e(TAG, "registerCallback(): Failed to register calback", t);
	            }            
	            return false;
	        }        
        }
        return true;
    }
    
    private void unregisterCallback(){        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "unregisterCallback(): starting, mService : " + mService);
        }
        
        if( mService != null){            
            try {
                mService.unregisterCallback( mCallback.hashCode());
            } catch (Throwable t) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "unregisterCallback() failed", t);
                }
            }            
        }
    }
    	
	public void runTestsCommand() {
		if (LogSettings.MARKET) {
			MktLog.i(TAG, "runTestsCommand()...");
		}
		mHandler.sendEmptyMessageDelayed(MSG_START_TEST, UI_UPDATE_DELAY);
	}
	
	private void runTestsImpl() {
		synchronized (mLock) {
			if (LogSettings.MARKET) {
				MktLog.i(TAG, "runTestsImpl()");
			}
			wrapInitTest();			
			mAudioTests.clear();			
			mAudioTests.add(new AudioTest(RCMediaManager.MODE_TEST_SPEAKER, "External Speaker") {

				@Override
				protected void saveDelayValue(int delay) {
					super.saveDelayValue(delay);
					RCMProviderHelper.setDeviceExternalSpeakerDelay(RingCentralApp.getContextRC(), delay);
				}
			});			
			mAudioTests.add(new AudioTest(RCMediaManager.MODE_TEST_INTERNAL, "Internal Speaker") {				
				protected void saveDelayValue(int delay) {
					super.saveDelayValue(delay);
					RCMProviderHelper.setDeviceInternalSpeakerDelay(RingCentralApp.getContextRC(), delay);
				}
			});			
			mCurrentTest = mAudioTests.get(0);
			startTest();
		}				
	}
	
	public void releaseTestsCommand() {
		if (LogSettings.MARKET) {
			MktLog.v(TAG, "releaseTestsCommand()...");
		}
		mHandler.sendEmptyMessageDelayed(MSG_RELEASE, UI_UPDATE_DELAY);
	}
	
	 private void releaseTestsImpl() {
	        if(LogSettings.ENGINEERING) {
	            MktLog.d(TAG, "releaseTestsImpl(), mFlagStopStackOnExit : " + mFlagStopStackOnExit);
	        }
	        isFinishing = true;
	        
	        checkFinishing();            
	        unregisterCallback();
	        
	        if(mFlagStopStackOnExit && (mService != null)){
	            try{ 
	                mService.stopSipStack();
	            } catch(Throwable t) {
	            	 if (LogSettings.MARKET) {
	                     MktLog.e(TAG, "release() failed", t);
	                 }
	            }
	        }
	    }

    /**
     * Controls UI, sends messages to handler for UI update 
     */
    private void updateUIRequest(int state) {
        if(isFinishing) {
            if(LogSettings.MARKET) { 
                MktLog.d(TAG,"updateUI(), isFinishing : " + isFinishing);
            }
            return;
        }
        if(mNotificationHandler != null) {
            mNotificationHandler.sendEmptyMessage(state);                       
        }
    }
	
	private final Handler mHandler = new Handler() {				
		public void handleMessage(Message msg) {
			if (LogSettings.MARKET) {
				MktLog.d(TAG, "mHandler.handleMessage(), msg.what : " + getMessageTag(msg.what));
			}
			switch (msg.what) {
				case MSG_START_TEST:					
					runTestsImpl();
					break;
				case MSG_NEXT_TEST:
					startTest();
					break;
				case MSG_TIMEOUT:
					onTestFailed();
					break;
				case MSG_RELEASE:
					releaseTestsImpl();    				    	
					break;		
				case MSG_INIT:
					onServiceConnectionStateChange();
					break;
			}
		};
		
		private String getMessageTag(int message) {
			switch (message) {
			case MSG_START_TEST:
				return "MSG_START_TEST";
			case MSG_NEXT_TEST:
				return "MSG_NEXT_TEST";
			case MSG_TIMEOUT:
				return "MSG_TIMEOUT";
			case MSG_RELEASE:
				return "MSG_RELEASE";
			case MSG_INIT:
				return "MSG_INIT";
			}
			return "UNDEFINED";
		}				
	};
	
    public void onStackStarted(boolean started) {
        if(LogSettings.MARKET) {    
            MktLog.d(TAG, "onStackStarted(), started : " + started);
        }
        if(started) {
        	updateUIRequest((mCurrentTest == null) ? AudioSetupWizard.CONNECTED : AudioSetupWizard.FAILED);
        }
    }
	
	
	
	private void startTest() {
		synchronized (mLock) {
			if (LogSettings.MARKET) {
				MktLog.i(TAG, "startTest(), mCurrentTest : " + (mCurrentTest == null ? "null" : mCurrentTest.dumpState()));
			}
			if(checkFinishing()){				
				return;
			}	
			
			mHandler.removeMessages(MSG_TIMEOUT);
			mHandler.sendEmptyMessageDelayed(MSG_TIMEOUT, TEST_EXECUTION_TIMEOUT);				
			
			if( (mCurrentTest == null) || (!mCurrentTest.startTest()) ) {
				onTestFailed();
			}				
		}
	}
	
	private void onTestStart(int id) {
		synchronized (mLock) {
			if (LogSettings.MARKET) {
				MktLog.i(TAG, "onTestStart(), id : " + id + " mCurrentTest : " + (mCurrentTest == null ? "null" : mCurrentTest.dumpState()));
			}
			if(checkFinishing()) {
				return;
			}			
			if( (mCurrentTest == null) || (!mCurrentTest.onStart(id)) ) {
				onTestFailed();
			}			
		}		
	}
	
	private void onTestComplete(int id, int delay) {
		synchronized (mLock) {
			if (LogSettings.MARKET) {
				MktLog.i(TAG, "onTestComplete(), id : " + id + ", delay : " + delay + " samples, mCurrentTest : " + (mCurrentTest == null ? "null" : mCurrentTest.dumpState()));
			}
			if(checkFinishing()) {
				return;
			}
			if( (mCurrentTest == null) || (!mCurrentTest.onComplete(id, delay)) ) {
				onTestFailed();
			} else {				
				iterateNextTest();
			}
		}
	}
	
	private void onTestFailed() {
		synchronized (mLock) {
			if (LogSettings.MARKET) {
				MktLog.i(TAG, "onTestFailed(), mCurrentTest : " + (mCurrentTest == null ? "null" : mCurrentTest.dumpState()));
			}
			if(checkFinishing()) {
				return;
			}
			if( mCurrentTest != null ){
				mCurrentTest.onFailed();
			}			
			iterateNextTest();
		}
	}
	
	private void iterateNextTest() {
		synchronized (mLock) {
			if (LogSettings.MARKET) {
				MktLog.i(TAG, "iterateNextTest(), mCurrentTest : " + (mCurrentTest == null ? "null" : mCurrentTest.dumpState()));
			}
			if(checkFinishing()) {
				return;
			}			
			
			mHandler.removeMessages(MSG_TIMEOUT);
			
			final int currentIndex = mAudioTests.indexOf(mCurrentTest);
			if((currentIndex >= 0)&&(currentIndex +1 < mAudioTests.size())){
				mCurrentTest = mAudioTests.get(currentIndex +1);
				mHandler.sendEmptyMessageDelayed(MSG_NEXT_TEST, INTER_TEST_DELAY);
			} else {
				mCurrentTest = null;
				boolean testSucceeded = false; // according to previous logic, need at least one passed test to show Completed notificationb
				for(AudioTest audioTest : mAudioTests) {
					if(audioTest.hasCompleted()){
						testSucceeded = true;
						break;
					} 
				}
				updateUIRequest(testSucceeded ? AudioSetupWizard.COMPLETED : AudioSetupWizard.FAILED);
			}
		}
	}
	
	private boolean checkFinishing() {
		synchronized (mLock) {						
			if(isFinishing) {				
				if (LogSettings.MARKET) { 
					MktLog.d(TAG, "checkFinishing(), isFinishing : " + isFinishing + ", mCurrentTest : " + (mCurrentTest == null ? "null" : mCurrentTest.dumpState()));
				}
				mHandler.removeMessages(MSG_TIMEOUT);
				mAudioTests.clear();
				if(mCurrentTest != null) {
					mCurrentTest.onFailed();
					mCurrentTest = null;
				}																 				
				return true;
			}			
			return false;
		}
	}	

	
	/**
	 * Abstract implementation for Audio Test, subclasses should implement saveDelayValue method
	 * to persist detected delay value
	 *
	 */
    private abstract class AudioTest {
    	
    	private static final int NOT_STARTED    = 0;
    	private static final int STARTED 		= 1;
    	private static final int IN_PROGRESS    = 2;
    	private static final int COMPLETED      = 3;
    	private static final int FAILED  		= 4;

    	private int id;
    	private String name;
    	private int state = NOT_STARTED;
    	private int delay = RCMConstants.DEFAULT_AEC_DELAY_VALUE;     	
    	
    	public AudioTest(int id, String name) {
    		this.id = id;    		
    		this.name = name;
    	}    	    	
    	
    	public boolean startTest() {
			if (state == NOT_STARTED) {
				this.state = STARTED;
				return WizardClient.this.wrapStartTest(id);
			}
			return false;
		}
    	
    	public boolean stopTest() {
			return WizardClient.this.wrapStopTest();
		}
    	    		
    	public boolean onStart(int id) {
    		if ((this.id == id) && (state == STARTED)) {
    			this.state = IN_PROGRESS;
    			return true;
    		}
    		return false;
    	}
    	
    	public boolean onComplete(int id, int delay) {    		
    		if ((this.id == id) && (state == IN_PROGRESS)) {
    			saveDelayValue(delay);
        		this.state = COMPLETED;
        		stopTest();
    			return true;
    		}
    		return false;
    	}    	
    	
    	public void onFailed() {
    		if (this.state != COMPLETED) {
    			this.state = FAILED;
    			stopTest();
    		}
    	}
    	
    	public boolean hasCompleted() {
    		return (this.state == COMPLETED);
    	}    	    	
    	    	
    	public String dumpState() {
    		return new StringBuilder()
    					.append("name : ").append(name)
    					.append(", id : ").append(id)    					
    					.append(", state : ").append(getStateTag(state))
    					.append(hasCompleted() ? (", delay : " + delay) : "")
    					.toString();
    	}
    	
    	protected void saveDelayValue(int delay) {
    		this.delay = delay;
    	}    	
    	
    	private String getStateTag(int state) {
    		switch (state) {
			case NOT_STARTED:
				return "NOT_STARTED";
			case STARTED:
				return "STARTED";
			case IN_PROGRESS:
				return "IN_PROGRESS";
			case COMPLETED:
				return "COMPLETED";
			case FAILED:
				return "FAILED";
			}
    		return "UNDEFINED";
    	}    	
    }	
    
    
    /**
     * Wrapper for test initiation
     * @param testId, id of test to be executed
     * @return true if succeeded, false otherwise
     */
    private void wrapInitTest() {
		if (LogSettings.MARKET) {
            MktLog.d(TAG, "wrapInitTest(), mService : " + mService);
        }
		if (mService != null) {
			try {
				final int[] devices = mService.get_snd_dev();
				if (devices != null) {
					if (LogSettings.MARKET) {
						MktLog.d(TAG, "wrapInitTest(), sound devices Capture : "
										+ devices[0] + ", playback : "
										+ devices[1]);
					}
					if (devices[0] < 0 || devices[1] < 0) {
						mService.set_snd_dev_test_mode();
					}
				}
				mService.ase_initialize();
			} catch (Throwable t) {
				if (LogSettings.MARKET) {
					MktLog.e(TAG, "wrapInitTest() failed... ", t);
				}
			}
		}
	}
    
    /**
     * Wrapper for start test method 
     * @param testId, id of test to be executed
     * @return true if succeeded, false otherwise
     */
	private boolean wrapStartTest(int testId) {
		if (LogSettings.MARKET) {
            MktLog.d(TAG, "wrapStartTest(), testId : " + testId);
        }
		boolean result = false;
		if(mService != null) {
	        try {
	            mService.asw_start(testId);
	            result = true;
	        } catch (Throwable t) {
	            if (LogSettings.MARKET) {
	                MktLog.e(TAG, "wrapStartTest(), failed...", t);
	            }
	        }
		}
		return result;
	}
	
	/**
     * Wrapper for stop test method 
     * @param testId, id of test to be executed
     * @return true if succeeded, false otherwise
     */
	private boolean wrapStopTest() {
		if (LogSettings.MARKET) {
            MktLog.d(TAG, "wrapStopTest()");
        }
		boolean result = false;
		if(mService != null) {
	        try {
	            mService.asw_stop();
	            result = true;
	        } catch (Throwable t) {
	            if (LogSettings.MARKET) {
	                MktLog.e(TAG, "wrapStopTest() failed...", t);
	            }
	        }
		}
		return result;
	}

	
    /**
     * Service callback implementation. 
     */
    private final IServicePJSIPCallback mCallback = new IServicePJSIPCallback.Stub(){
    	
        @Override
        public void on_sip_stack_started(boolean started) throws RemoteException {
        	onStackStarted(started);
        }
        
        @Override
        public void on_test_started(int id) throws RemoteException {                  
            onTestStart(id);             
        }

        @Override
        public void on_test_completed(int id, int delaySample) throws RemoteException {
            onTestComplete(id, delaySample);
        }

        @Override
        public void account_changed_register_state(int accountId, boolean registered, int code) throws RemoteException {    
            return; 
        }

        @Override
        public void call_media_state(long callId, int state) throws RemoteException {
            return;
        }

        @Override
        public void call_media_stop(long callId) throws RemoteException {
            return;
        }

        @Override
        public void call_state(long callId, int state, int last_status, int ending_reason, int media_sate) throws RemoteException {
            return;
        }

        @Override
        public void http_registration_state(int code) throws RemoteException {
            return;
        }

        @Override
        public void on_audio_route_changed(AudioState audioState) throws RemoteException {
            return;
        }

        @Override
        public void on_call_transfer_status(long callId, int status, String statusProgressText) throws RemoteException {
            return;
        }

        @Override
        public void on_incoming_call(CallInfo callInfo) throws RemoteException {
            return;
        }

        @Override
        public void on_message(long callId, SipMessageInput sipMsg) throws RemoteException {
        	return;
        }

		@Override
		public void on_call_info_changed(long callId) throws RemoteException {
			return;
		}
		
		@Override
	    public void notifyCallsStateChanged(final RCCallInfo[] calls) throws RemoteException {
			return;
		}
    };
}
