/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.audio;

/**
 * @author Denis Kudja
 *
 */
public class AudioDevicePJSIP {

	public final static native int writeToRTP( byte[] buffer, int size, long sampleCounter, long param );
	public final static native int readFromRTP( byte[] buffer, int size, long sampleCounter, long param );
	public final static native int aec( byte[] micBuff, byte[] spkBuff, int size, long param );
	public final static native int aecScheme2( byte[] micBuff, byte[] spkBuff, int size, long param );
	
	public final static native int aecSetTestMode( long param );
	public final static native int aecGetCurrentTestMode( long param );
	public final static native int aecGetMicDb( long param );
	public final static native int aecGetAudioDelay( long param );
	public final static native long getTimestamp(long param);
	public final static native long getElapsedTime(long param, long start, long stop);
	
	public final static native int readFromWriteToRTP( byte[] buffer_to, byte[] buffer_from, int size, long counter, long param );
}
