/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.client;

import java.util.ArrayList;

import android.os.SystemClock;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.service.CallInfo;
import com.rcbase.android.sip.service.PJSIP;
import com.rcbase.android.sip.service.PJSIPCallback;
import com.ringcentral.android.LogSettings;

/**
 * RC SIP Client. Lines control class. 
 *  
 * @author Denis Kudja
 */
public class SipClientLines {

    private static final String TAG = "[RC]SipClientLines";
	
	/**
	 * Maximum lines number
	 */
    public final static int sf_max_lines = 3;
    
    public final static int sf_INVALID_LINE_INDEX = -1;
    
    private ArrayList<CallInfo> m_calls = null;
    private int m_currentLine = 0;
    
    public synchronized int getActiveLines() {
    	int activeLines = 0;
		for (int i = 0; i < m_calls.size(); i++) {
			final CallInfo callInfo = m_calls.get(i);
			if ( callInfo != null && callInfo.isActive())
				activeLines++;
		}
		
		return activeLines;
	}
    
    /**
     * Find active line that is not On Hold state. 
     * It will be used for restoring current line index. 
     */
    public synchronized int detectCurrentLineIndex(){
    	int currLine = sf_INVALID_LINE_INDEX;
    	long startTime = 0;
    	int timeLineIndex = sf_INVALID_LINE_INDEX;
    	
		for (int i = 0; i < m_calls.size(); i++) {
			final CallInfo callInfo = m_calls.get(i);
			if ( callInfo != null && callInfo.isActive()){
				
				 if( !callInfo.isHold()){
						/*
						 * If there is the first not On Hold line then save index
						 * Otherwise we cannot detect current line index by Hold state 
						 */
						currLine = i;
				 }
				 
				 if( startTime == 0 ){
					 startTime = callInfo.getCallStartTime();
					 timeLineIndex = i;
				 }
				 else{
					 if( startTime > callInfo.getCallStartTime()){
						 startTime = callInfo.getCallStartTime();
						 timeLineIndex = i;
					 }
				 }
			}
		}
		
		if( currLine == sf_INVALID_LINE_INDEX && 
				timeLineIndex != sf_INVALID_LINE_INDEX ){
			currLine = timeLineIndex;
		}
		
		return currLine;
    }

	/**
     * Default constructor. Number of lines is defined on start using sf_max_lines value
     * and MUST not be changed during work.
     */
    public SipClientLines(){
    	m_calls = new ArrayList<CallInfo>(sf_max_lines);
    	
		for( int i = 0; i < sf_max_lines; i++ ){
			m_calls.add(i, null );
		}
    }

    /**
     * Get line index by Call ID
     */
    public synchronized int getLineIndexbyCallId( long callId ){
        for( int i = 0; i < m_calls.size(); i++ ){
            if( m_calls.get(i) != null && m_calls.get(i).getRcCallId() == callId )
                return i;
        }
        
        return sf_INVALID_LINE_INDEX;
    }
    
    /**
     * Detect call by Call ID
     */
    public synchronized final CallInfo getCallbyCallId( long callId ){
    	final int index = getLineIndexbyCallId( callId );
        return ( index == sf_INVALID_LINE_INDEX ? null : m_calls.get(index));
    }
    
    /**
     * Get index of line that can be used for a new call (incoming/outbound)
     */
    private synchronized int getEmptyLine(){

        if( m_calls.get(m_currentLine) == null )
            return m_currentLine;
        
        for( int i = 0; i < m_calls.size(); i++ ){
            if( m_calls.get(i) == null )
                return i;
        }
        
        return sf_INVALID_LINE_INDEX;
    }
    
    /**
     * 
     */
    public synchronized void selectAnotherActiveLine(){

		for (int i = 0; i < m_calls.size(); i++) {
			if ( m_calls.get(i) != null && m_calls.get(i).isActive()){
				setCurrentLine( i );
				return;
			}
		}
		
		/*
		 * If we was not able detect another active line then
		 * set the first line as active line
		 */
		setCurrentLine( 0 );
    }
    
    /**
     *	Detect active calls are present. 
     */
	public synchronized boolean isActiveCalls(){

		for (int i = 0; i < m_calls.size(); i++) {
			if ( m_calls.get(i) != null && m_calls.get(i).isActive())
				return true;
		}
		
		return false;
	}

	/**
	 * Remove Call info for all lines
	 */
	public synchronized void remove_all_calls() {
		for (int i = 0; i < m_calls.size(); i++) {
				m_calls.set(i, null);
		}
	}

	public synchronized long[] getCallIdArray(){

		if( m_calls.size() == 0 )
			return null;
		
		long[] callIdList = new long[ m_calls.size() ];
		for( int i = 0; i < m_calls.size(); i++ ){
			callIdList[ i ] = ( m_calls.get(i) != null ? m_calls.get(i).getRcCallId() : PJSIP.sf_INVALID_RC_CALL_ID );
		}
		
		return callIdList;
	}
	/**
	 * Return calls'a array used for showing on list view 
	 */
	public synchronized CallInfo[] getCallsArray(){

		int activeLines = getActiveLines();
    	if (LogSettings.MARKET) {
    	    MktLog.i(TAG, "getCallsArray array size:" + activeLines );
    	}

		if( activeLines == 0 )
			return null;

		int index = 0;
		CallInfo[] callList = new CallInfo[ activeLines ];
		for( int i = 0; i < m_calls.size(); i++ ){
			
			if( m_calls.get(i) != null && m_calls.get(i).isActive()){

				if( index >= activeLines ){
	                if (LogSettings.MARKET) {
	                    MktLog.e(TAG, "getCallsArray failed. Invalid number of active calls");
	                }
	                break;
				}
				
		    	if (LogSettings.MARKET) {
		    	    MktLog.i(TAG, "getCallsArray [" + index + "] " + m_calls.get(i).toString() );
		    	}
				
				callList[index++] = m_calls.get(i);
			}
		}
	
		return callList;
	}
	
	/**
	 * Remove Call info from lines array
	 */
    public synchronized void call_remove(long callId){
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "Remove call info. RC CallID [" + callId + "]");
        }
        int call_index = getLineIndexbyCallId( callId );
        if( call_index != sf_INVALID_LINE_INDEX ){
            m_calls.set(call_index, null );
        }
    }

	/**
	 * Remove Call info from lines array
	 */
    public synchronized boolean call_remove_by_index(int index){
    	if( isValidLine( index ) ){
                m_calls.set(index, null );
                return true;
    	}    	
    	return false;
    }    
    
    /**
     * Add call info to the lines array
     * 
     * @return index of a new line on success, sf_INVALID_LINE_INDEX is all lines are used already 
     */
    public synchronized int call_add(CallInfo callInfo, boolean markAddedCallAsActive ){
    	
    	if( callInfo == null ){
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "Add call info. Invalid object");
            }
    		return sf_INVALID_LINE_INDEX;
    	}
    	
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "Add call info. Call ID [" + callInfo.getRcCallId() + "] Time:" + callInfo.getCallStartTime());
        }
        
        if( callInfo.getRcCallId() == PJSIP.sf_INVALID_RC_CALL_ID ){
        	return sf_INVALID_LINE_INDEX;
        }
        
        int call_index = sf_INVALID_LINE_INDEX;
        call_index = getLineIndexbyCallId(callInfo.getRcCallId());
        if( call_index != sf_INVALID_LINE_INDEX ){
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "Add call info. Such call is present already on the list [" + callInfo.getRcCallId() + "]");
            }
            return sf_INVALID_LINE_INDEX;
        }
        
        call_index = getEmptyLine(); 
        if( call_index == sf_INVALID_LINE_INDEX ){
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "Add call info failed. All lines are bisy. Could not add call [" + callInfo.getRcCallId() + "]");
            }
            return sf_INVALID_LINE_INDEX;
        }
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "call_add Set call info. Line [" + call_index + "] CallId ["+ callInfo.getRcCallId() + "]");
        }
        
        m_calls.set(call_index, callInfo);
        
        if( markAddedCallAsActive ){
        	this.setCurrentLine(call_index);
        }
        return call_index;
    }
    
    public synchronized void call_set_state( long callId, int state ){
    	
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "Set call state. RC CallID [" + callId + "] state " + RCSIP.getCallStateForLog(state));
        }
        
        int call_index = getLineIndexbyCallId( callId );
        if( call_index != sf_INVALID_LINE_INDEX ){
            m_calls.get(call_index).setCall_state(state);
        }
    }

    public synchronized void call_set_hold( long callId, boolean hold ){
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "Set hold. Call ID [" + callId + "] hold [" + hold + "]");
        }
        
        int call_index = getLineIndexbyCallId( callId );
        if( call_index != sf_INVALID_LINE_INDEX ){
            m_calls.get(call_index).setHold(hold);
        }
    }
    
    public synchronized void call_set_media_state( long callId, int state ){
    
        int call_index = getLineIndexbyCallId( callId );
        if( call_index != sf_INVALID_LINE_INDEX ){
        	
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "Set call media state. Call ID [" + m_calls.get(call_index).getRcCallId() + 
                		"] media state [" + state + 
                		"] prev [" + m_calls.get(call_index).getMedia_state() + "]");
            }
            
            m_calls.get(call_index).setMedia_state(state);
        }
        else{
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "Set call media state. Invalid index. Call ID [" + callId + "] media state [" + state + "]");
            }
        }
    }
    
    public synchronized void setCurrentLine( int line ){
    	if( isValidLine(line) ){
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "Current line changed from:" + m_currentLine + " to:" + line );
            }
    		m_currentLine = line;
    	}
    }

    public synchronized int getCurrentLine(){
    	return m_currentLine;
    }
    
    public synchronized boolean isActiveCall( long callId ){
    	return ( m_calls.get(m_currentLine) != null && m_calls.get(m_currentLine).getRcCallId() == callId );
    }
    
    public synchronized long getCurrentLineCallId(){
        
        CallInfo callInfo = m_calls.get(m_currentLine);
        if( callInfo !=  null )
            return callInfo.getRcCallId();
        
        return PJSIP.sf_INVALID_CALL_ID;
    }

    public synchronized final CallInfo getCurrentLineCallInfo(){
        return m_calls.get(m_currentLine);
    }
    
    public synchronized final CallInfo getLineCallInfo( int line ){
    	if( isValidLine( line ) ){
    		return m_calls.get(line);
    	}
    	
    	return null;
    }
    
    protected synchronized void setRemoteContact( int line, String contact ){
    	if( isValidLine( line ) && m_calls.get(line) != null ){
    		m_calls.get(line).setRemoteContact(contact);
    	}
    }

    protected synchronized void setFinishing( int line, boolean finishing ){
    	if( isValidLine( line ) && m_calls.get(line) != null ){
    		m_calls.get(line).setFinishing(finishing);
    	}
    }
    
    protected synchronized void setProcessingStarted( int line, boolean starting ){
    	if( isValidLine( line ) && m_calls.get(line) != null ){
    		m_calls.get(line).setProcessingStarted(starting);
    	}
    }
    
    protected static boolean isValidLine( int line ){
    	return ( line >= 0 && line < sf_max_lines && line != sf_INVALID_LINE_INDEX );
    }
}
