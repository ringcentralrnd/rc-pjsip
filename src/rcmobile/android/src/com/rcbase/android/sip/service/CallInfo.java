/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.client.RCSIP;
import com.rcbase.parsers.sipmessage.SipMessageInput;
import com.ringcentral.android.LogSettings;

/**
 * @author Denis Kudja
 *
 */
public class CallInfo implements Parcelable {

    private static final String TAG = "[RC]CallInfo";        
	
    public static final String sf_SIP_PREFIX = "sip:";
    public static final String sf_Anonymous  = "Anonymous";    
    public static final long ID_NO_PHOTO = -1;
    
    private long	m_rcCallId 	= PJSIP.sf_INVALID_RC_CALL_ID;
	private int 	m_PjsipCallId 	= PJSIP.sf_INVALID_CALL_ID;
	private int 	m_accountId = PJSIP.sf_INVALID_ACCOUNT_ID;
	
	private int 	m_call_state	= RCSIP.PJSUA_CALL_STATE_CREATED;
	private int 	m_media_state	= RCSIP.PJSUA_CALL_MEDIA_NONE;
	
	private String m_localURI;
	private String m_remoteURI;
	private String m_localContact;
	private String m_remoteContact;
	
	private String m_dialogCallId;
	private String m_rcHeader;
	
	private boolean m_hold = false;
	private String m_clientId;	//DK: Client ID is received after RC HTTP registration procedure
	
	private SipMessageInput m_sipMsg; 
	private boolean m_finishing = false;
	private long m_callStartTime = 0;
	private long m_callAcceptedTime = 0;
	private int m_networkType = -1;
	
	//members do not send over IPC
	private boolean m_recordOnServer = false;
	private boolean m_callScreening = false;
	private boolean m_processingStarted = false; //Processing for this call is started already (used for outbound calls)
	
	private String m_binded_contact = "";
	private Long m_photo_id = ID_NO_PHOTO;
	private int m_endingReason = RCCallInfo.CALL_ENDED_NO;
	
	public int getEndingReason() {
	    return m_endingReason;
	}
	
	public void setEndingReason(int ending_reason) {
        m_endingReason = ending_reason;
    }
	
	public String getBindedContact() {
		return m_binded_contact;
	}

	public void setBindedContact(String m_binded_contact) {
		this.m_binded_contact = m_binded_contact;
	}

	public Long getPhotoID() {
		return m_photo_id;
	}

	public void setPhotoID(Long m_photo_id) {
		this.m_photo_id = m_photo_id;
	}
	
	public int getNetworkType() {
		return m_networkType;
	}
	public void setNetworkType(int networkType) {
		m_networkType = networkType;
	}
	public long getRcCallId() {
		return m_rcCallId;
	}
	public void setRcCallId(long rcCallId) {
		m_rcCallId = rcCallId;
	}
	public boolean isProcessingStarted() {
		return m_processingStarted;
	}
	public void setProcessingStarted(boolean processingStarted) {
		m_processingStarted = processingStarted;
	}
	
	public boolean isFinishing() {
		return m_finishing;
	}
	public void setFinishing(boolean isFinishing) {
		m_finishing = isFinishing;
	}
	public long getCallStartTime() {
		return m_callStartTime;
	}
	public void setCallStartTime(long callStartTime) {
		m_callStartTime = callStartTime;
	}

    public long getCallAcceptedTime() {
        return m_callAcceptedTime;
    }

    public void setCallAcceptedTime(long callAcceptedTime) {
        m_callAcceptedTime = callAcceptedTime;
    }

	
	public final SipMessageInput getSipMsg() {
		return m_sipMsg;
	}
	public final void setSipMsg(SipMessageInput sipMsg) {
		m_sipMsg = sipMsg;
	}
	
	public void copyFrom( CallInfo callInfo ){
		if( callInfo == null ) return;
		this.setCall_state( callInfo.getCall_state() );
		this.setMedia_state( callInfo.getMedia_state());
		this.setHold(callInfo.isHold());
		this.setCallStartTime(callInfo.getCallStartTime());
		this.setCallAcceptedTime(callInfo.getCallAcceptedTime());
		this.setEndingReason(callInfo.getEndingReason());
		this.setLastStatus(callInfo.getLastStatus());
	}

	/**
	 * Return From name or from RC Info or from SIP header
	 */
	public String getFromName(){
		
		if( !CallInfo.isUnknown( getBindedContact()) ){
			return getBindedContact();
		}
		
		if( m_sipMsg != null && 
				m_sipMsg.getBody().getFromName() != null && 
				m_sipMsg.getBody().getFromName().length() > 0 ){
			return m_sipMsg.getBody().getFromName();
		}
		
		return ( m_remoteContact != null && 
				m_remoteContact.indexOf(sf_SIP_PREFIX) < 0 &&
				!CallInfo.isUnknown(m_remoteContact) ? m_remoteContact : "" );
	}

	/**
	 * Return From phone or from RC Info or from SIP header
	 */
	public String getFromNumber(){
		if( m_sipMsg != null && 
				m_sipMsg.getBody().getFromPhone() != null && 
				m_sipMsg.getBody().getFromPhone().length() > 0 ){
			return m_sipMsg.getBody().getFromPhone();
		}
		
		return ( m_remoteURI != null ? getNumberFromURI( m_remoteURI ) : "");
	}
	
	/**
	 * Default constructor
	 */
	public CallInfo(){
		
	}
	
	/**
	 * Constructor Template for Outbound Call
	 */
	public CallInfo(final String number, final String name){

		m_remoteURI = number;
        m_remoteContact = name;
        
        if ( LogSettings.MARKET ) {
            MktLog.i(TAG, "CallInfo Prepare call info for making call to " + number);
        }
	}
	
	/**
	 * Detect if there outbound call template
	 */
	public boolean isOutbound(){
		if(m_PjsipCallId == PJSIP.sf_INVALID_CALL_ID &&
				m_remoteURI != null &&
				m_remoteURI.length() > 0 ){
			return true;
		}
		
		return false;
	}
	
	/**
	 * Get number from URI sip:xxxxxxxxxx@sip.ringcentral.com
	 */
	public static String getNumberFromURI( final String uri ){
		
		String result = uri;
		
		if( uri == null ) 
			return uri;
		
		int index_start = uri.indexOf(":");
		int index_end = uri.indexOf("@");
		
		if( index_end >= 0 ){
			result = ( index_start >= 0 ? uri.substring( index_start + 1, index_end ) : uri.substring(0, index_end ));
		}
		else{
			result = ( index_start >= 0 ? uri.substring( index_start + 1 ) : uri );
		}
		
		return result;
	}

	/**
	 * Detect if there Unknown name or number
	 */
	public static boolean isUnknown( final String name ){
		return ( name == null || name.length() == 0 || sf_Anonymous.equalsIgnoreCase(name));
	}
	
	/**
	 * Parcel constructor
	 */
	private CallInfo(Parcel in){
		readFromParcel( in );
	}
	
	public boolean isRecordOnServer() {
		return m_recordOnServer;
	}
	public void setRecordOnServer(boolean recordOnServer) {
		m_recordOnServer = recordOnServer;
	}
	public boolean isCallScreening() {
		return m_callScreening;
	}
	public void setCallScreening(boolean callScreening) {
		m_callScreening = callScreening;
	}
	public boolean isHold() {
		return m_hold;
	}
	public void setHold(boolean hold) {
		m_hold = hold;
	}
	
	
	private int mLastStatus = RCSIP.SipCode.PJSIP_SC_OK.code();
	public int getLastStatus() {
	    return mLastStatus;
	}
	public void setLastStatus(int status) {
	    mLastStatus = status;
	}
	
	public int getCall_state() {
        return m_call_state;
    }
	
	public void setCall_state(int callState) {

	    if (callState == RCSIP.PJSUA_CALL_STATE_CONFIRMED && m_callAcceptedTime <= 0) {
	        m_callAcceptedTime = SystemClock.elapsedRealtime();
	    }
	    
		m_call_state = callState;
	}
	public int getMedia_state() {
		return m_media_state;
	}
	public void setMedia_state(int mediaState) {
		m_media_state = mediaState;
	}
	
	/**
	 * Active call is or Outbound calls or 
	 * call with valid callId and with state another then NULL & DISCONNECTED
	 */
	public boolean isActive(){
		return ( isOutbound() ||  
				( isValid() && 
						m_call_state != RCSIP.PJSUA_CALL_STATE_DISCONNECTED && 
						!isFinishing() ) );
	}
	
	/**
	 * Valid call has Call ID and local & remote URI
	 */
	public boolean isValid(){
		if(m_rcCallId == PJSIP.sf_INVALID_RC_CALL_ID) return false;
		if(m_localURI == null || m_localURI.length() == 0 ) return false;
		if(m_remoteURI == null || m_remoteURI.length() == 0 ) return false;
		
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return "RC CallID: " + m_rcCallId + " PJSIP CallID: " + m_PjsipCallId + " State: " + RCSIP.getCallStateForLog(m_call_state);
	}
	
	public Integer getPjsipCallIdInteger() {
		return new Integer(m_PjsipCallId);
	}
	
	protected int getPjsipCallId() {
		return m_PjsipCallId;
	}

	public void setPjsipCallId(int pjsipCallId) {
		m_PjsipCallId = pjsipCallId;
	}

	public int getAccountId() {
		return m_accountId;
	}
	public void setAccountId(int accountId) {
		m_accountId = accountId;
	}
	public String getRcHeader() {
		return m_rcHeader;
	}
	public void setRcHeader(String rcHeader) {
		m_rcHeader = rcHeader;
	}
	public String getLocalURI() {
		return m_localURI;
	}

	public void setLocalURI(String localURI) {
		m_localURI = localURI;
	}

	public String getRemoteURI() {
		return m_remoteURI;
	}

	public void setRemoteURI(String remoteURI) {
		m_remoteURI = remoteURI;
	}

	public String getLocalContact() {
		return m_localContact;
	}

	public void setLocalContact(String localContact) {
		m_localContact = localContact;
	}

	public String getRemoteContact() {
		return m_remoteContact;
	}

	public void setRemoteContact(String remoteContact) {
		m_remoteContact = remoteContact;
	}

	public String getDialogCallId() {
		return m_dialogCallId;
	}

	public void setDialogCallId(String dialogCallId) {
		m_dialogCallId = dialogCallId;
	}

	public String getClientId() {
		return m_clientId;
	}
	public void setClientId(String clientId) {
		m_clientId = clientId;
	}	
	
	/* (non-Javadoc)
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		
		dest.writeLong(m_rcCallId);
		dest.writeInt(m_PjsipCallId);
		dest.writeInt(m_accountId);
		dest.writeInt(m_call_state);
		dest.writeInt(m_media_state);
		dest.writeInt(m_endingReason);
		dest.writeInt(mLastStatus);
		
		
		dest.writeString(m_localURI);     
		dest.writeString(m_remoteURI);    
		dest.writeString(m_localContact); 
		dest.writeString(m_remoteContact);
		dest.writeString(m_dialogCallId); 
		dest.writeString(m_rcHeader); 
		dest.writeInt((m_hold ? 1 : 0) );
		dest.writeInt((m_finishing ? 1 : 0) );

		dest.writeParcelable(m_sipMsg, flags);
		dest.writeString(m_clientId); 
		dest.writeLong(m_callStartTime);
		dest.writeLong(m_callAcceptedTime);
		dest.writeInt(m_networkType);
		dest.writeString(m_binded_contact);
		dest.writeLong(m_photo_id);
	}

	public void readFromParcel( Parcel in ){
		 
		 m_rcCallId		= in.readLong();
		 m_PjsipCallId 	= in.readInt();
		 m_accountId	= in.readInt();
		 m_call_state	= in.readInt();
		 m_media_state	= in.readInt();
		 m_endingReason = in.readInt();
		 mLastStatus    = in.readInt();
		     
		 m_localURI		= in.readString();    
		 m_remoteURI	= in.readString();   
		 m_localContact	= in.readString();
		 m_remoteContact= in.readString();
		 m_dialogCallId	= in.readString();
		 m_rcHeader 	= in.readString();
		 m_hold 		= ( 0 == in.readInt() ? false : true );
		 m_finishing  	= ( 0 == in.readInt() ? false : true );
			
		 m_sipMsg 			= (SipMessageInput)in.readParcelable(CallInfo.class.getClassLoader());
		 m_clientId 		= in.readString();
		 m_callStartTime 	= in.readLong();
		 m_callAcceptedTime = in.readLong();
		 m_networkType 		= in.readInt();
		 m_binded_contact   = in.readString();
		 m_photo_id         = in.readLong();
	}	
	
	/*
	 * 
	 */
	public static final Parcelable.Creator<CallInfo> CREATOR = new Parcelable.Creator<CallInfo>() {
		
		public CallInfo createFromParcel( Parcel in ){
			return new CallInfo(in);
		}
		
		public CallInfo[] newArray( int size){
			return new CallInfo[size];
		}
	};
}
