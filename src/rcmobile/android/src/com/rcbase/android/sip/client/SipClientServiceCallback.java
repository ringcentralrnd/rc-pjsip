/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.client;

import android.os.RemoteException;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.service.CallInfo;
import com.rcbase.android.sip.service.IServicePJSIPCallback;
import com.rcbase.android.sip.service.PJSIP;
import com.rcbase.android.sip.service.RCCallInfo;
import com.rcbase.android.sip.client.RCSIP;
import com.rcbase.android.utils.media.AudioState;
//import com.rcbase.api.httpreg.HttpRegister;
import com.rcbase.parsers.sipmessage.SipMessageInput;
import com.ringcentral.android.LogSettings;

/**
 * @author Denis Kudja
 *
 */
public class SipClientServiceCallback {
    private static final String TAG = "[RC]SipClientServiceCallback";
	
	private SipClient m_sipClient = null;
	
	public SipClientServiceCallback( SipClient sipClient ){
		m_sipClient = sipClient;
	}
	
	public final IServicePJSIPCallback getCallback() {
		return m_callback;
	}
	
	private final IServicePJSIPCallback m_callback = new IServicePJSIPCallback.Stub(){

		@Override
		public void account_changed_register_state(int accountId,
				boolean registered, int code) throws RemoteException {    
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "account_changed_register_state registered:" + registered );
            }
//			
//            if( m_sipClient == null )
//            	return;
//
//            m_sipClient.ProcessAccountChangedRegisterState(accountId, registered, code );
		}

		@Override
		public void call_media_state(long callId, int state)
				throws RemoteException {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "call_media_state for RC CallId: " + callId + " as " + RCSIP.getMediaStatusLabel(state));
            }
            
            if( m_sipClient.getLines().getLineIndexbyCallId( callId ) == SipClientLines.sf_INVALID_LINE_INDEX ){
            	
    			if(LogSettings.ENGINEERING) {
                    EngLog.w(TAG, " call_media_state() Ignored. No line with call ID: " + callId );
                }
    			return;
            }
            
            m_sipClient.getLines().call_set_media_state( callId, state );
            
            if( m_sipClient.getLines().isActiveCall( callId )){
            	
            	/*
            	 * Check Mute state when current call is received media 
            	 * (usually when Hold status is switched Off)
            	 */
            	if( RCSIP.PJSUA_CALL_MEDIA_ACTIVE == state ){
            		m_sipClient.CheckMuteState();
            	}
            	
            	m_sipClient.sendMessage( SipClient.sf_update_controls, 0, null );
            }
			
		}

		@Override
		public void call_media_stop(long callId) throws RemoteException {
			
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "call_media_stop  call ID:" + callId );
            }
			
            if( m_sipClient.getLines().getLineIndexbyCallId( callId ) == SipClientLines.sf_INVALID_LINE_INDEX ){
            	
    			if(LogSettings.ENGINEERING) {
                    EngLog.w(TAG, " call_media_stop() Ignored. No line with call ID: " + callId );
                }
    			return;
            }
            
			m_sipClient.getLines().call_set_media_state( callId, RCSIP.PJSUA_CALL_MEDIA_NONE );
			
            if( m_sipClient.getLines().isActiveCall( callId ))
            	m_sipClient.sendMessage( SipClient.sf_update_controls, 0, null );
		}

		@Override
		public void call_state(long callId, int state, int last_status, int ending_reason, int media_state) throws RemoteException {
			
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "call_state  RC CallID:" + callId + 
                		" state: " + state + 
                		" last status:" + last_status );
            }
			
            if( m_sipClient.getLines().getLineIndexbyCallId( callId ) == SipClientLines.sf_INVALID_LINE_INDEX ){
            	
    			if(LogSettings.ENGINEERING) {
                    EngLog.w(TAG, " call_state() Ignored. No line with call ID: " + callId );
                }
    			return;
            }
            
			m_sipClient.getLines().call_set_state(callId, state);
			m_sipClient.getLines().call_set_media_state(callId, media_state);
			
            if ( state == RCSIP.PJSUA_CALL_STATE_CONFIRMED ) {
            	
                if( m_sipClient.getLines().isActiveCall( callId )){
                	
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "sf_update_call_state call_state(): PJSUA_CALL_STATE_CONFIRMED" );
                    }
                
                	m_sipClient.sendMessage( SipClient.sf_update_call_state, 0, null );
                }
            	
            }
            
            if (state == RCSIP.PJSUA_CALL_STATE_DISCONNECTED) {
            	
        		boolean isActiveLine = m_sipClient.getLines().isActiveCall( callId );
        		int activeLines = m_sipClient.getLines().getActiveLines();

        		if( activeLines >= 1 ){
            		m_sipClient.getLines().call_remove( callId );
            		
            		/*
            		 * Select another line 
            		 */
                    if( isActiveLine ){
                    	m_sipClient.getLines().selectAnotherActiveLine();
                    }
        		}
        		
        		activeLines = m_sipClient.getLines().getActiveLines();
        		
            	if( activeLines == 1 ){
                	m_sipClient.sendMessage( SipClient.sf_switch_view, 0, null );
                	m_sipClient.sendMessage( SipClient.sf_update_controls, 0, null );
            	}
            	else if(  activeLines > 1 ){
                	
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "sf_update_call_state call_state(): PJSUA_CALL_STATE_DISCONNECTED 1" );
                    }
                
                	m_sipClient.sendMessage( SipClient.sf_update_call_state, 0, null );
            	}
            	else{ //no more active calls
                	
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "sf_update_call_state call_state(): PJSUA_CALL_STATE_DISCONNECTED 2" );
                    }
                	// from pjsua_call_hangup
                	final boolean validHangup = ((last_status == RCSIP.SipCode.PJSIP_SC_OK.code()) ||
                								(last_status == RCSIP.SipCode.PJSIP_SC_DECLINE.code()) ||
                								(last_status == RCSIP.SipCode.PJSIP_SC_REQUEST_TERMINATED.code()));
                	
               		m_sipClient.sendMessage( new CompletionInfo(true, validHangup, ending_reason, validHangup ? CompletionInfo.ERROR_UNKNOWN : CompletionInfo.ERROR_PJSIP, last_status, null, false));
            	}
            	
            }
		}

		@Override
		public void http_registration_state(int code) throws RemoteException {

			if(LogSettings.MARKET) {
                MktLog.i(TAG, " http_registration_state() Starting code: " + code );
            }
			
//			if( code == HttpRegister.HttpRegistrationStatus.HTTP_REG_INVALID_CREDENTIAL.code()){
//	            if(LogSettings.MARKET) {
//	                MktLog.w(TAG, " http_registration_state() End Call: HTTP_REG_INVALID_CREDENTIAL");
//	            }
//				m_sipClient.sendMessage( new CompletionInfo(true, false, RCCallInfo.CALL_ENDED_BY_ERROR, CompletionInfo.ERROR_HTTPREG_1, 0, null, false));
//			}
//			else if( code == HttpRegister.HttpRegistrationStatus.HTTP_REG_FAILED.code()){
//			    if(LogSettings.MARKET) {
//                    MktLog.w(TAG, " http_registration_state() End Call: HTTP_REG_FAILED");
//                }
//				m_sipClient.sendMessage( new CompletionInfo(true, false, RCCallInfo.CALL_ENDED_BY_ERROR, CompletionInfo.ERROR_HTTPREG_2, 0, null, false));
//			}
		}

		@Override
		public void on_audio_route_changed(AudioState audioState) throws RemoteException {
        	if(LogSettings.ENGINEERING) {
                EngLog.i(TAG, " on_audio_route_changed: " + audioState);
            }
        	
        	m_sipClient.sendMessage( SipClient.sf_update_audio, 0, audioState);
        	
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "sf_update_call_state on_audio_route_changed():" );
            }
		}

		@Override
		public void on_call_transfer_status(long callId, int status,
				String statusProgressText) throws RemoteException {
			
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "Call transfer. callID[" + callId + "] [" + status + "] Info:"
                        + (statusProgressText == null ? "" : statusProgressText.trim()));
            }
		}

		@Override
		public void on_incoming_call(CallInfo callInfo) throws RemoteException {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "on_icoming_call URI " + callInfo.getRemoteURI() + " ID " + callInfo.getDialogCallId());
            }
		}

		@Override
		public void on_message(long callId, SipMessageInput sipMsg)
				throws RemoteException {
			
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "Message Call ID[" + callId + "] Cmd [" + ( sipMsg == null ? -1 : sipMsg.getCommand()) + "]" );
            }
		}

		@Override
		public void on_sip_stack_started(boolean result) throws RemoteException {
			
	    	if (LogSettings.MARKET) {
	    	    MktLog.i(TAG, "on_sip_stack_started() result: " + result );
	    	}
			
	    	if( result == false ){
		    	if (LogSettings.MARKET) {
		    	    MktLog.i(TAG, "on_sip_stack_started() finished due to SIP started false flag" );
		    	}
		    	return;
	    	}
	    	
            if (LogSettings.MARKET) {
                MktLog.w(TAG, " on_sip_stack_started: Send sf_synchronize_call_state");
            }
    		m_sipClient.sendMessage( SipClient.sf_synchronize_call_state, 1, null );
        	
			if( result && m_sipClient != null ){
				
	            if (LogSettings.MARKET) {
	                MktLog.w(TAG, "sf_update_call_state on_sip_stack_started():" );
	            }
				
	        	m_sipClient.sendMessage( SipClient.sf_update_call_state, 0, null );
			}
			
	    	if (LogSettings.MARKET) {
	    	    MktLog.i(TAG, "on_sip_stack_started() finished" );
	    	}
			
		}

        @Override
        public void on_test_started(int id) throws RemoteException {
            return;
        }

        @Override
        public void on_test_completed(int id, int value) throws RemoteException {
            return;
        }

		@Override
		public void on_call_info_changed(long callId) throws RemoteException {

			if (LogSettings.MARKET) {
	    	    MktLog.d(TAG, "on_call_info_changed() Call ID:" + callId );
	    	}
			/*
			 * Update calls list
			 */
			if( callId == PJSIP.sf_INVALID_RC_CALL_ID ){
				
	            if (LogSettings.MARKET) {
	                MktLog.w(TAG, " on_call_info_changed: Send sf_synchronize_call_state");
	            }
				m_sipClient.sendMessage( SipClient.sf_synchronize_call_state, 1, null );
			}
			else{
				/*
				 * Update call info with call ID 
				 */
				m_sipClient.sendMessage( SipClient.sf_synchronize_call_state, 0, null );
			}
		}
		
		/**
	     * NEW "CENTRIC" SCHEME ============================================================================================================
	     */
	    
	    /**
	     * SIProxy notifications
	     */
		@Override
	    public void notifyCallsStateChanged(final RCCallInfo[] calls) throws RemoteException {
		}
	};
}
