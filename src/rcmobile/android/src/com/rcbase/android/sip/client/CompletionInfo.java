/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.client;

import com.rcbase.android.sip.service.RCCallInfo;

/**
 * @author Denis Kudja
 *
 */
public class CompletionInfo {

    public static final int ERROR_UNKNOWN 				= 0;
    public static final int ERROR_BINDING 				= 1;
    public static final int ERROR_ISERVICEPJSIP 		= 2;
    public static final int ERROR_HTTPREG_1 			= 3;
    public static final int ERROR_HTTPREG_2 			= 4;
    public static final int ERROR_REGISTER_CALLBACK 	= 5;
    public static final int ERROR_START_SIP 			= 6;
    public static final int ERROR_ADD_ACCOUNT_1 		= 7;
    public static final int ERROR_ADD_ACCOUNT_2 		= 8;
    public static final int ERROR_REGISTER_ACCOUNT 		= 9;
    public static final int ERROR_MAKE_CALL_1 			= 10;
    public static final int ERROR_MAKE_CALL_2	 		= 11;
    public static final int ERROR_SERVICE_CONN_LOST 	= 12;
    public static final int ERROR_HTTPREG_3 			= 13;
    public static final int ERROR_PJSIP	     			= 14;
	
	boolean m_asyncCaller 	= false; 
	boolean m_success	 	= false; 
	int m_errorCode	 		= ERROR_UNKNOWN; 
	int m_errorSubCode 		= ERROR_UNKNOWN; 
	String m_errorPrompt 	= null; 
	boolean m_autoCloaseOnSuccess = false;
	int m_endingReason = RCCallInfo.CALL_ENDED_BY_ERROR;
	
	public final boolean isAsyncCaller() {
		return m_asyncCaller;
	}


	public final boolean isSuccess() {
		return m_success;
	}


	public final int getErrorCode() {
		return m_errorCode;
	}


	public final int getErrorSubCode() {
		return m_errorSubCode;
	}


	public final String getErrorPrompt() {
		return m_errorPrompt;
	}


	public final boolean isAutoCloaseOnSuccess() {
		return m_autoCloaseOnSuccess;
	}

    /**
     * Returns the reason of ending: {@link RCCallInfo#CALL_ENDED_BY_USER},
     * {@link RCCallInfo#CALL_ENDED_BY_PARTY},
     * {@link RCCallInfo#CALL_ENDED_BY_SYSTEM},
     * {@link RCCallInfo#CALL_ENDED_BY_ERROR}
     * 
     * @return the reason of ending
     */
    public final int getEndingReason() {
        return m_endingReason;
    }
	
	public CompletionInfo(	boolean asyncCaller, 
			boolean success, 
			int endingReason,
			int errorCode, 
			int errorSubCode, 
			String errorPrompt, 
			boolean autoCloaseOnSuccess){
	
	    m_endingReason = endingReason;
		m_asyncCaller = asyncCaller; 
		m_success = success;
		m_errorCode = errorCode;
		m_errorSubCode = errorSubCode; 
		m_errorPrompt = errorPrompt;
		m_autoCloaseOnSuccess = autoCloaseOnSuccess;		
	}
	
	@Override
	public String toString() {
		return new StringBuffer().append(" m_asyncCaller:").append(m_asyncCaller)
								 .append(" m_success:").append(m_success)
								 .append(" m_errorCode:").append(m_errorCode)
								 .append(" m_errorSubCode:").append(m_errorSubCode)
								 .append(" m_errorPrompt:").append(m_errorPrompt)
								 .toString();
	} 			
}
