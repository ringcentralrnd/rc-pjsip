/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.os.PowerManager;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.utils.execution.ExecutionWakeLock;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.utils.ReflectionHelper;

/**
 * Wake lock for SIP commands execution. 
 */
public class ServicePJSIPWakeLock implements ExecutionWakeLock {
    /**
     * Synchronization primitive.
     */
    private Object _lock = new Object();
    
    /**
     * Defines logging tag.
     */
    private static final String LOG_TAG = "[RC]SrvcPJSIPwakeLock";
    
    /**
     * Keeps Android Power manager.
     */
    private PowerManager mPowerManager;
    
    /**
     * Keeps partial wake lock.
     */
    private PowerManager.WakeLock mPartialWakeLock;
    
    /**
     * acquire/release counter.
     */
    private long mCounter;

    
    private static final int MAX_ACQUIRES_WHEN_WARN = 9;
    
    private Method m_isScreenOn = null;
    /**
     * Constructs wake lock for SIP commands execution.
     * 
     * @param powerManager the power manager instance
     */
    public ServicePJSIPWakeLock(PowerManager powerManager) {
        mPowerManager = powerManager;
        
        try {
        	if( mPowerManager != null ){
        		m_isScreenOn = ReflectionHelper.getMethod(mPowerManager.getClass(), "isScreenOn");
        	}
        	
		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
		}

        if (LogSettings.MARKET) {
            MktLog.d(LOG_TAG, " ServicePJSIPWakeLock Method isScreenOn " + (m_isScreenOn == null ? "Not present" : "Present"));
        }
    }

    /*
     * 
     */
    public boolean isScreenOn(){
    	boolean result = true;
    	if( mPowerManager != null && m_isScreenOn != null ){
    		try {
				result =  (Boolean) m_isScreenOn.invoke(mPowerManager, (Object[])null );
				
		        if (LogSettings.MARKET) {
		            MktLog.d(LOG_TAG, " ServicePJSIPWakeLock Method isScreenOn is present result: " + result );
		        }
		        
			} catch (IllegalArgumentException e) {
			} catch (IllegalAccessException e) {
			} catch (InvocationTargetException e) {
			}
    	}
    	
    	return result;
    }
    
    @Override
    public void acquire() {
        synchronized (_lock) {
            try {
                if (mPartialWakeLock == null) {
                    mPartialWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "RingCentral VoIP Service");
                    mPartialWakeLock.setReferenceCounted(false);
                }
            } catch (Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.w(LOG_TAG, " acquire:newWakeLock:exception:" + th.toString());
                }
                return;
            }

            try {
                if (!mPartialWakeLock.isHeld()) {
                    mPartialWakeLock.acquire();
                }
            } catch (Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.w(LOG_TAG, " acquire:acquire:exception:" + th.toString());
                }
                return;
            }
            mCounter++;
        }
        
        if (mCounter >= MAX_ACQUIRES_WHEN_WARN) {
            if (LogSettings.MARKET) {
                MktLog.w(LOG_TAG, " acquire:limit:" + mCounter);
            }
        }
    }

    @Override
    public void release() {
        synchronized (_lock) {
            boolean released = false;
            mCounter--;
            if (mCounter < 0) {
                if (LogSettings.MARKET) {
                    MktLog.w(LOG_TAG, " relase:negative counter:");
                }
                mCounter = 0;
            }
            try {
                if ((mCounter == 0) && (mPartialWakeLock != null) && (mPartialWakeLock.isHeld())) {
                    released = true;
                    mPartialWakeLock.release();
                }
            } catch (Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.w(LOG_TAG, " release:release:exception:" + th.toString());
                }
                return;
            }
            if (released) {
                if (LogSettings.MARKET) {
                    MktLog.d(LOG_TAG, "Released");
                }
            }
        }
    }

    public long getLockCounter() {
        synchronized (_lock) {
            return mCounter;
        }
    }
    
    @Override
    public void releaseAll() {
        synchronized (_lock) {
            if (mCounter > 0) {
                if (LogSettings.MARKET) {
                    MktLog.d(LOG_TAG, " releaseAll:counter is " + mCounter);
                }
            }
            
            mCounter = 0;
            try {
                if ((mPartialWakeLock != null) && (mPartialWakeLock.isHeld())) {
                    mPartialWakeLock.release();
                }
            } catch (Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.w(LOG_TAG, " releaseAll:release:exception:" + th.toString());
                }
                return;
            }
        }
    }
    
    /**
     * Returns if the lock is acquired.
     * 
     * @return if the lock is acquired
     */
    public boolean isHeld() {
        synchronized (_lock) {
            try {
                if (mPartialWakeLock != null) {
                    return mPartialWakeLock.isHeld();
                }
            } catch (Throwable th) {
            }
        }
        return false;
    }
}
