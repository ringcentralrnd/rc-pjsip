/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import com.rcbase.android.sip.service.CallInfo;
import com.rcbase.android.sip.service.RCCallInfo;
import com.rcbase.parsers.sipmessage.SipMessageInput;
import com.rcbase.android.utils.media.AudioState;

/**
 * @author Denis Kudja
 *
 */
interface IServicePJSIPCallback {

    void account_changed_register_state( int accountId, boolean registered, int code );
    void on_incoming_call( in CallInfo callInfo );
    void on_message( long rcCallId, in SipMessageInput sipMsg );
    
    void call_media_state( long rcCallId, int state );
    void call_media_stop( long rcCallId );
    
    void call_state( long rcCallId, int state, int last_status, int ending_reason, int media_state);
    void on_call_transfer_status( long rcCallId, int status, String statusProgressText );
    void on_audio_route_changed( in AudioState audioState );
    
    void http_registration_state( int code );
    void on_sip_stack_started( boolean result );
    void on_call_info_changed( long rcCallId );
        
    void on_test_started(int id);
    void on_test_completed(int id, int value);
    
    /**
     * NEW "CENTRIC" SCHEME ================================================================================================================
     */
    
    /**
     * SIProxy notifications
     */    
    void notifyCallsStateChanged(in RCCallInfo[] calls);
}
