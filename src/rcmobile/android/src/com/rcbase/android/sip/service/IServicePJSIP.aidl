/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import com.rcbase.android.sip.service.IServicePJSIPCallback;
import com.rcbase.android.sip.service.ServiceState;
import com.rcbase.android.sip.service.CallInfo;
import com.rcbase.android.sip.service.RCCallInfo;
import com.rcbase.android.sip.service.AccountInfo;
import com.rcbase.android.sip.service.CodecInfo;
import com.rcbase.android.utils.media.AudioState;
import com.rcbase.android.logging.LogItem;

/**
 * @author Denis Kudja
 *
 */
interface IServicePJSIP {
    
    int startSipStack( boolean testmode );
    int stopSipStack();
    int getSipServicePID();
    void testMode_emualateCrash(long delay);
//    int restartSipStack();
    boolean isStartedSipStack();
    
    void registerCallback( int hash, IServicePJSIPCallback callback );
    void unregisterCallback( int hash );
    
//    boolean isHttpRegistrationDone();
//    boolean isOutboundCallsOnly();
    
//    void outboundCallsOnlyFinish();
    
    void call_answer( long callId, long delayMillis );
    void call_answer_and_hold( long callId, long delayMillis );
    void call_answer_and_hangup( long callId, long delayMillis );
    int call_hangup( long callId, int code, boolean detachSound );
    int call_hold( long callId );
//    int call_hold_all();
    int call_unhold( long callId ); 
//    int call_make_call( int accountId, String remoteURI, String remoteName ); 
    int call_make_call_init( int accountId, String remoteURI, String remoteName ); 
    void call_make_call_complete( long callId ); 
//    int call_attach_sound( long callId );
//    int call_send_request( long callId, String method, String clientId ); 
//    int call_transfer( long callId, String remoteURI );
    int call_dial_dtmf( long callId, String digits );
//    int call_detach_sound( long callId );
    int call_mark_as_ending( long callId );
    
    int call_get_info( long callId, inout CallInfo callInfo );
    boolean call_is_active( long callId );
    boolean call_has_media( long callId );
    int call_get_count();
    int call_get_pjsip_count();
    long[] call_get_active_calls(); 
//    long call_get_duration( long callId );
    
//    int acc_get_default();
      int acc_get( inout AccountInfo accountInfo );
//    int acc_del( int accountId );
//    int acc_set_registration( int accountId, boolean renew );
    boolean acc_is_registered( int accountId );
    
    int message_send( int accountId, String dstURI, String content );
    
//    CodecInfo[] enum_codecs();
//    int codec_set_priority( String codecId, int priority );
    
    int[] get_snd_dev();
//    void set_snd_dev();
    
//    void ring_start();
    void ring_stop();
    void ringtone_stop();
    
    int conf_adjust_tx_level(int slot, float level);
    int conf_adjust_rx_level(int slot, float level);

    float conf_get_adjust_rx_level( int slot );
    float conf_get_adjust_tx_level( int slot );
    
    float conf_get_signal_rx_level( int slot );
    float conf_get_signal_tx_level( int slot );
    
    
    AudioState getAudioState();
    void toggleBluetoothState();
    void toggleSpeakerState();
    String storeCurrentLog(int applicationPID);
    
    void ase_initialize();
    void asw_start(int testId);
    void asw_stop();
    void asw_release();
    
    void set_snd_dev_test_mode(); 
    
    /**
     * Returns latest finished RC VoIP calls for dumping.
     */
    RCCallInfo[] getLatestFinishedCalls();
    
    /**
     * Returns current RC VoIP calls for dumping.
     */
    RCCallInfo[] getCurrentCalls();
    
    /**
     * Returns current service state. 
     */
    com.rcbase.android.sip.service.ServiceState getServiceState();
    
    /**
     * Cleans-up Log on SIP service side and latest finished calls.
     */
    void cleanupLogAndLatestFinishedCalls();
    
    /**
     * Cleans-up Log.
     */
    void cleanupLog();
    
    /**
     * Returns current log.
     * 
     */
    LogItem[] getCurrentLog(boolean cleanup_after_getting);
    
    /**
     * Returns if outbound call can be made.
     */ 
    boolean canMakeVoIPOutboundCall();
    
    /**
     * Returns if device is in playback mode.
     */ 
    boolean isPlayBackDevice();
    
    /**
     * Commands consistency check.
     */
    void testEnqueedCommandsMaxNumberExceeded();
    
    /**
     * Commands consistency check.
     */
    void testCommandExecutionTimeExceeded();
}
