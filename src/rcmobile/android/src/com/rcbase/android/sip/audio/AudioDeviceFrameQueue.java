/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.audio;

import java.util.Vector;

public class AudioDeviceFrameQueue  {
	private static final long serialVersionUID = 1L;
	private volatile Vector<AudioDeviceFrame> mQueue;
	private volatile int mCapacity;
	private volatile boolean active = true;
	public AudioDeviceFrameQueue(int capacity) {
	    mCapacity  = capacity;
	    mQueue = new Vector<AudioDeviceFrame>(capacity);
	}

	public void stop() {
	    synchronized (mQueue) {
	        active = false;
	        if (mWaiting) {
                mQueue.notifyAll();
            }
	    }
	}
	
	public boolean putFrame(AudioDeviceFrame frame){
		synchronized (mQueue) {
		    if (!active) {
		        return false;
		    }
		    if (mQueue.size() >= mCapacity) {
		        if (mWaiting) {
		            mQueue.notifyAll();
		        }
		        return false;
		    }
		    mQueue.addElement(frame);
		    if (mWaiting) {
                mQueue.notifyAll();
            }
		    return true;
		}
	}
	
	public boolean isFull(){
	    return (mQueue.size() >= mCapacity);
	}
	
	private volatile boolean mWaiting = false;
	private static final long MAX_WAITING_TIME = 2000;
    public AudioDeviceFrame getFrame() {
        AudioDeviceFrame frame = null;
        try {
            synchronized (mQueue) {
                if (!active) {
                    return null;
                }
                if (mQueue.size() > 0) {
                    frame = mQueue.firstElement();
                    mQueue.remove(0);
                } else {
                    mWaiting = true;
                    try {
                        mQueue.wait(MAX_WAITING_TIME);
                    } catch (java.lang.Throwable th) {
                    }
                    mWaiting = false;
                    if (mQueue.size() > 0 && active) {
                        frame = mQueue.firstElement();
                        mQueue.remove(0);
                        return frame;
                    } else {
                        return null;
                    }
                }
            }
        } catch (java.lang.Throwable th) {

        }
        return frame;
    }
}
