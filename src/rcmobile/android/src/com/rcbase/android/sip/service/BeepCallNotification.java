/**
 * Copyright (C) 2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import android.content.Context;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Handler;
import android.os.Message;

import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.utils.ToneGeneratorUtils;

class BeepCallNotification {
    private static final String TAG = "[RC]BeepCallNotification";
    private static final int    PLAY_NOTIFICATION = 0x00;
    private static final int    INT_DELAY_BETWEEN_BEEPS = 2500; 
    private static final int    INT_SHORT_KEY_TONE_LENGTH_MS = 100;
    private static final int    INT_BEEP_VOLUME = (int)(42 * 1.2); // nice digit :)
    private static final int    BEEP_TONE = ToneGenerator.TONE_PROP_ACK;
    
    
    private ToneGeneratorUtils  m_toneGenerator = null; // generates tones
    private Handler             m_notificator = null; // 
    private boolean             isUsed = false; // flag, is beep engine used or not
    
    
    protected BeepCallNotification(Context context) {
        m_toneGenerator = new ToneGeneratorUtils(context, AudioManager.STREAM_VOICE_CALL, INT_BEEP_VOLUME);
        
        m_notificator = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (LogSettings.MARKET) {
                	MktLog.w(TAG, "m_notificator : BEEP");
                }
                
                if (msg.what == PLAY_NOTIFICATION) {
                    beepMaker();
                }
            }
        };
    }
    
    /**
     * method which should be called for second call notification
     */
    public synchronized void startBeep() {
        if (LogSettings.MARKET) {
        	MktLog.w(TAG, "startBeep : isUsed: " + isUsed);
        }
    	
        if( !isUsed ) {
            isUsed = true;
            m_notificator.sendEmptyMessageDelayed(PLAY_NOTIFICATION, 0);
        }
    }

    public synchronized void stopBeep(){
        if (LogSettings.MARKET) {
        	MktLog.w(TAG, "stopBeep : isUsed: " + isUsed);
        }
    	
        isUsed = false;
    }    
    
    public synchronized boolean isBeeping() {
    	return isUsed;
    }


    // M.D. make love..sex..beeps ? !
    /**
     * Engine method, necessary for making beeps during call
     */
    private void beepMaker() {
    	if( isUsed ){
    		playTone();
    		m_notificator.sendEmptyMessageDelayed(PLAY_NOTIFICATION, INT_DELAY_BETWEEN_BEEPS);
    	}
    }
    
    /**
     * Plays beeps 
     */
    protected void playTone() {  
        if(null != m_toneGenerator) {
            m_toneGenerator.playTone(BEEP_TONE, INT_SHORT_KEY_TONE_LENGTH_MS);
        }
    }
}