/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.audio;


/**
 * @author Denis Kudja
 *
 */
public class AudioDeviceFrame {

    private byte[] m_frame = null;

    public AudioDeviceFrame(byte[] frame) {
        m_frame = frame.clone();
    }

    public AudioDeviceFrame(int size) {
        m_frame = new byte[size];
    }

    public byte[] getFrame() {
        return m_frame;
    }

    public boolean copyFrame(byte[] frame) {
        if (frame != null && (frame.length == m_frame.length)) {
            System.arraycopy(frame, 0, m_frame, 0, m_frame.length);
            return true;
        }
        return false;
    }
}
