/**
 * Copyright (C) 2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.client;

/**
 * <code>RCSIP</code> provides "one point" defines/constants for SIP/VoIP
 */
public final class RCSIP {
    /**
     * Defines timeout for execution non critical service routine on main task to exclude ANRs.
     */
    public static final long TIMEOUT_FOR_SERVICE_NOT_CRITICAL_CALLS = 2000;
    
    /**
     * Defines timeout for execution critical service routines on main task to exclude ANRs.
     */
    public static final long TIMEOUT_FOR_SERVICE_CRITICAL_CALLS = 3500;
    
    public static final int ALWAYS_CPU_LOCK_FOR_SIP_INITAL_VALUE  = 0;
    
    /**
     * Defines Call Media states prompts.
     */
    private static final String[] MEDIA_STATES_PROMPTS = { 
        "Call currently has no media",
        "The media is active",
        "The media is currently put on hold by local endpoint",
        "The media is currently put on hold by remote endpoint",
        "The media has reported error (e.g. ICE negotiation)"
    }; 
    
    /**
     * Defines Call Media states labels.
     */
    private static final String[] MEDIA_STATES_LABELS = { 
        "NO",
        "ACTIVE",
        "LOCAL_HOLD",
        "REMOTE_HOLD",
        "MEDIA_ERROR)"
    }; 
    
    /** Call currently has no media */
    public static final int PJSUA_CALL_MEDIA_NONE = 0;

    /** The media is active */
    public static final int PJSUA_CALL_MEDIA_ACTIVE = 1;

    /** The media is currently put on hold by local end-point */
    public static final int  PJSUA_CALL_MEDIA_LOCAL_HOLD = 2;

    /** The media is currently put on hold by remote end-point */
    public static final int PJSUA_CALL_MEDIA_REMOTE_HOLD = 3;

    /** The media has reported error (e.g. ICE negotiation) */
    public static final int PJSUA_CALL_MEDIA_ERROR = 4;
    
    /**
     * Defines short descriptions for call states.
     */
    private static final String[] CALL_STATE_PROMPTS = {
        "Unknown. Before INVITE is sent or received. Iterations with PJSIP started.",
        "Calling. After INVITE is sent",
        "Incoming. After INVITE is received",
        "Early. After response with To tag.",
        "Connecting. After 2xx is sent/received.",
        "Confirmed. After ACK is sent/received.",
        "Disconnected. Session is terminated.",
        "Created. No works with PJSIP yet."
    };
    
    /**< Before INVITE is sent or received PJSIP : PJSUA_CALL_STATE_NULL */
    public static final int PJSUA_CALL_STATE_INIT       = 0;        
    /**< After INVITE is sent           */
    public static final int PJSUA_CALL_STATE_CALLING    = 1;        
    /**< After INVITE is received.      */
    public static final int PJSUA_CALL_STATE_INCOMING   = 2;     
    /**< After response with To tag.        */
    public static final int PJSUA_CALL_STATE_EARLY      = 3;        
    /**< After 2xx is sent/received.        */
    public static final int PJSUA_CALL_STATE_CONNECTING = 4;       
    /**< After ACK is sent/received.        */
    public static final int PJSUA_CALL_STATE_CONFIRMED  = 5;        
    /**< Session is terminated.         */
    public static final int PJSUA_CALL_STATE_DISCONNECTED   = 6;    
    /**< Call created, but there are no works with PJSIP yet  */
    public static final int PJSUA_CALL_STATE_CREATED       = 7;     

    /**
     * Returns label for call state
     * 
     * @param response
     *            call state
     * @return label
     */
    public static final String getCallStateForLog(int response) {
        switch (response) {
        case (PJSUA_CALL_STATE_INIT): 
            return "PJSUA_CALL_STATE_INIT";
        case (PJSUA_CALL_STATE_CALLING): 
            return "PJSUA_CALL_STATE_CALLING";
        case (PJSUA_CALL_STATE_INCOMING): 
            return "PJSUA_CALL_STATE_INCOMING";
        case (PJSUA_CALL_STATE_EARLY): 
            return "PJSUA_CALL_STATE_EARLY";
        case (PJSUA_CALL_STATE_CONNECTING): 
            return "PJSUA_CALL_STATE_CONNECTING";
        case (PJSUA_CALL_STATE_CONFIRMED): 
            return "PJSUA_CALL_STATE_CONFIRMED";
        case (PJSUA_CALL_STATE_DISCONNECTED): 
            return "PJSUA_CALL_STATE_DISCONNECTED";
        case (PJSUA_CALL_STATE_CREATED): 
            return "PJSUA_CALL_STATE_CREATED";
        default:
            return "" + response;
        }
    }
    
    /**
     * Returns short description for media status.
     * 
     * @param media_status
     *            the media status
     * @return short description for the media status
     */
    public static String getMediaStatus(int media_status) {
        if (media_status < PJSUA_CALL_MEDIA_NONE || media_status > PJSUA_CALL_MEDIA_ERROR)
            return new String("Wrong status");

        return MEDIA_STATES_PROMPTS[media_status];
    }

    /**
     * Returns label for media status.
     * 
     * @param media_status
     *            the media status
     * @return label for the media status
     */
    public static String getMediaStatusLabel(int media_status) {
        if (media_status < PJSUA_CALL_MEDIA_NONE || media_status > PJSUA_CALL_MEDIA_ERROR)
            return new String("Wrong status");

        return MEDIA_STATES_LABELS[media_status];
    }
    
    /**
     * Returns short description for call state.
     * 
     * @param state
     *            the call state
     * @return short description for call state
     */
    public static String getCallStatus(int state ){
        if( state < PJSUA_CALL_STATE_INIT || state > PJSUA_CALL_STATE_CREATED )
            return new String("Wrong status");
        
        return CALL_STATE_PROMPTS[state];
    }

    public static enum SipCode {
        
        PJSIP_SC_TRYING(100, "100 (Trying: extended search being performed may take a significant time so a forking proxy must send a 100 Trying response)"),
        PJSIP_SC_RINGING(180, "180 (Ringing)"),
        PJSIP_SC_CALL_BEING_FORWARDED(181, "181 (Call Is Being Forwarded)"),
        PJSIP_SC_QUEUED(182, "182 (Queued)"),
        PJSIP_SC_PROGRESS(183, "183 Session Progress"),

        PJSIP_SC_OK(200, "200 OK"),
        PJSIP_SC_ACCEPTED(202, "202 accepted: It Indicates that the request has been understood but actually can't be processed"),

        PJSIP_SC_ERROR(300, "300 Error"),
        
        PJSIP_SC_MULTIPLE_CHOICES(300, "300 Multiple Choices"),
        PJSIP_SC_MOVED_PERMANENTLY(301, "301 Moved Permanently"),
        PJSIP_SC_MOVED_TEMPORARILY(302, "302 Moved Temporarily"),
        PJSIP_SC_USE_PROXY(305, "305 Use Proxy"),
        PJSIP_SC_ALTERNATIVE_SERVICE(380, "380 Alternative Service"),

        PJSIP_SC_BAD_REQUEST(400, "400 Bad Request"),
        PJSIP_SC_UNAUTHORIZED(401, "401 Unauthorized (Used only by registrars or user agents. Proxies should use proxy authorization 407)"),
        PJSIP_SC_PAYMENT_REQUIRED(402,"402 Payment Required (Reserved for future use)"),
        PJSIP_SC_FORBIDDEN(403, "403 Forbidden"),
        PJSIP_SC_NOT_FOUND(404, "404 Not Found (User not found)"),
        PJSIP_SC_METHOD_NOT_ALLOWED(405, "405 Method Not Allowed"),
        PJSIP_SC_NOT_ACCEPTABLE(406, "406 Not Acceptable"),
        PJSIP_SC_PROXY_AUTHENTICATION_REQUIRED(407, "407 Proxy Authentication Required"),
        PJSIP_SC_REQUEST_TIMEOUT(408, "408 Request Timeout (Couldn't find the user in time)"),
        PJSIP_SC_GONE(410, "410 Gone (The user existed once, but is not available here any more.)"),
        PJSIP_SC_REQUEST_ENTITY_TOO_LARGE(413, "413 Request Entity Too Large"),
        PJSIP_SC_REQUEST_URI_TOO_LONG(414, "414 Request-URI Too Long"),
        PJSIP_SC_UNSUPPORTED_MEDIA_TYPE(415, "415 Unsupported Media Type"),
        PJSIP_SC_UNSUPPORTED_URI_SCHEME(416, "416 Unsupported URI Scheme"),
        PJSIP_SC_BAD_EXTENSION(420, "420 Bad Extension (Bad SIP Protocol Extension used, not understood by the server)"),
        PJSIP_SC_EXTENSION_REQUIRED(421, "421 Extension Required"),
        PJSIP_SC_SESSION_TIMER_TOO_SMALL(422, "422 Session Interval Too Small"),
        PJSIP_SC_INTERVAL_TOO_BRIEF(423, "423 Interval Too Brief"),
        PJSIP_SC_TEMPORARILY_UNAVAILABLE(480, "480 Temporarily Unavailable"),
        PJSIP_SC_CALL_TSX_DOES_NOT_EXIST(481, "481 Call/Transaction Does Not Exist"),
        PJSIP_SC_LOOP_DETECTED(482, "482 Loop Detected"),
        PJSIP_SC_TOO_MANY_HOPS(483, "483 Too Many Hops"),
        PJSIP_SC_ADDRESS_INCOMPLETE(484, "484 Address Incomplete"),
        PJSIP_AC_AMBIGUOUS(485, "485 Ambiguous"),
        PJSIP_SC_BUSY_HERE(486, "486 Busy Here"),
        PJSIP_SC_REQUEST_TERMINATED(487, "487 Request Terminated"),
        PJSIP_SC_NOT_ACCEPTABLE_HERE(488, "488 Not Acceptable Here"),
        PJSIP_SC_BAD_EVENT(489, "489 Bad Event"),
        PJSIP_SC_REQUEST_UPDATED(490, "490 Request Updated"),
        PJSIP_SC_REQUEST_PENDING(491, "491 Request Pending"),
        PJSIP_SC_UNDECIPHERABLE(493, "493 Undecipherable (Could not decrypt S/MIME body part)"),

        PJSIP_SC_INTERNAL_SERVER_ERROR(500, "500 Server Internal Error"),
        PJSIP_SC_NOT_IMPLEMENTED(501, "501 Not Implemented: The SIP request method is not implemented here"),
        PJSIP_SC_BAD_GATEWAY(502, "502 Bad Gateway"),
        PJSIP_SC_SERVICE_UNAVAILABLE(503, "503 Service Unavailable"),
        PJSIP_SC_SERVER_TIMEOUT(504, "504 Server Time-out"),
        PJSIP_SC_VERSION_NOT_SUPPORTED(505, "505 Version Not Supported: The server does not support this version of the SIP protocol"),
        PJSIP_SC_MESSAGE_TOO_LARGE(513, "513 Message Too Large"),
        PJSIP_SC_PRECONDITION_FAILURE(580, "580 Precondition Failure"),

        PJSIP_SC_BUSY_EVERYWHERE(600, "600 Busy Everywhere"),
        PJSIP_SC_DECLINE(603, "603 Decline"),
        PJSIP_SC_DOES_NOT_EXIST_ANYWHERE(604, "604 Does Not Exist Anywhere"),
        PJSIP_SC_NOT_ACCEPTABLE_ANYWHERE(606, "606 Not Acceptable"),
        
        /**
         * EXTENDED
         */
        PJSIP_SC_UNKNOWN_STATUS_CODE_EXTENDED                     (4801, "4801 UNKNOWN STATUS"),

        PJSIP_SC_IP_ADDRESS_CHANGED_STATUS_CODE_EXTENDED          (4803, "4803 IP ADDRESS CHANGED"),
        PJSIP_SC_LOGOUT_STATUS_CODE_EXTENDED                      (4804, "4804 LOGOUT"),
        PJSIP_SC_SIP_REGISTRATION_FAILED_STATUS_CODE_EXTENDED     (4805, "4805 SIP REGISTRATION FAILED"),
        PJSIP_SC_SIP_CALL_FAILED_STATUS_CODE_EXTENDED             (4806, "4806 SIP CALL FAILED"),
        PJSIP_SC_CALLS_EXISTS_WHEN_SIP_FALSE_STATUS_CODE_EXTENDED (4807, "4807 CALL WHEN STACK IS SHUTDOWN"),
        PJSIP_SC_NO_LOGIN_STATUS_CODE_EXTENDED                    (4808, "4808 NO LOGIN"),
        PJSIP_SC_SIP_STACK_CREATION_FAILED_EXTENDED               (4809, "4809 SIP STACK CREATION FAILED"),
        PJSIP_SC_SIP_LIBS_LOADING_FAILED_EXTENDED                 (4810, "4810 SIP LIBS LOADING FAILED"),
        PJSIP_SC_HTTP_REG_FAILED_EXTENDED                         (4811, "4811 HTTP REG FAILED"),
        PJSIP_SC_DISABLED_VOIP_EXTENDED                           (4812, "4812 VOIP DISABLED"),
        PJSIP_SC_NETWORK_CHANGED_EXTENDED                         (4813, "4813 NETWORK CHANGED"),
        PJSIP_SC_NETWORK_DISCONNECTED_EXTENDED                    (4814, "4814 NETWORK DISCONNECTED"),
        PJSIP_SC_NETWORK_DISCONNECTED_UNKNOWN_EXTENDED            (4815, "4815 UNKNOWN/UNSUPPORTED NETWORK"),
        PJSIP_SC_MEDIA_ERROR_EXTENDED                             (4816, "4816 MEDIA_ERROR"),
        PJSIP_SC_HTTP_REG_FAILED_CREDS_STATUS_CODE_EXTENDED       (4817, "4817 HTTP REG-ON FAILED: CREDENTIALS"),
        PJSIP_SC_HTTP_REG_FAILED_STATUS_CODE_EXTENDED             (4818, "4818 HTTP REG-ON FAILED: RESPONSE"),
        PJSIP_SC_HTTP_REG_FAILED_ACCOUNT_STATUS_CODE_EXTENDED     (4819, "4819 HTTP REG-ON FAILED: ADDING ACCOUNT"),
        PJSIP_SC_DISABLED_VOIP_HTTPREG_EXTENDED                   (4820, "4820 VOIP DISABLED HTTPREG"),
        PJSIP_SC_DISABLED_VOIP_REG_NOINC_EXTENDED                 (4821, "4821 VOIP DISABLED REG NO INCOMING"),
        PJSIP_SC_DISABLED_VOIP_AFTER_SETTIGS_EXTENDED             (4822, "4822 VOIP DISABLED AFTER SETTINGS"),
        PJSIP_SC_DISABLED_VOIP_AT_NET_NOTIF_EXTENDED              (4823, "4823 VOIP DISABLED AT NET NOTIFICATION"),
        PJSIP_SC_DISABLED_VOIP_AT_LOGIN_DETECTION_EXTENDED        (4824, "4824 VOIP DISABLED AT LOGIN DETECTION"),
        PJSIP_SC_HTTP_REG_FAILED_TEST_CODE_EXTENDED               (4825, "4825 HTTP REG-ON FAILED: TEST");
        
        
        /**
         * Keeps code identifier.
         */
        private final int code;
        
        /**
         * Keeps description.
         */
        private final String msg;
        
        /**
         * Returns code identifier.
         * 
         * @return code identifier.
         */
        public int code() { return code; }

        /**
         * Returns <code>SiPCode</code> instance by code
         * 
         * @param code the code
         * @return <code>SiPCode</code> instance by code
         */
        public static SipCode get(int code) {
            for (SipCode s : SipCode.values()) {
                if (code == s.code) {
                    return s;
                }
            }
            return PJSIP_SC_UNKNOWN_STATUS_CODE_EXTENDED;
        }
        
        /**
         * Returns code description.
         * 
         * @return code description.
         */
        public String msg() { return msg; }
        
        /**
         * Constructs code.
         * 
         * @param code
         *            code identifier
         * @param msg
         *            code description
         */
        SipCode(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }
    };

    /**
     * Returns code description
     * 
     * @param response the code identifier
     * @return status code description
     */
    public static final String getSipResponseForLog(int response) {
        return SipCode.get(response).msg();
    }
}
