/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import java.io.File;
import java.util.ConcurrentModificationException;
import java.util.Vector;

import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.os.SystemClock;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.audio.AudioSetupEngine;
import com.rcbase.android.sip.client.RCSIP;
import com.rcbase.android.sip.client.RCSIP.SipCode;
import com.rcbase.android.utils.Compatibility;
import com.rcbase.android.utils.execution.CommandProcessor;
import com.rcbase.android.utils.execution.TimedProxyTask;
import com.rcbase.android.utils.execution.CommandProcessor.Command;
import com.rcbase.android.utils.media.RCMediaManager;
import com.rcbase.android.utils.wifi.WiFiWatcher;
import com.rcbase.api.httpreg.HttpRegister;
import com.rcbase.api.httpreg.IHttpRegisterCallback;
import com.rcbase.api.xml.XmlSerializableObjectAbstract;
import com.rcbase.parsers.httpreg.HttpRegisterRequest;
import com.rcbase.parsers.httpreg.HttpRegisterResponse;
import com.rcbase.parsers.sipmessage.SipMessageInput;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.contacts.Cont;
import com.ringcentral.android.contacts.Cont.acts.ContactBinding;
import com.ringcentral.android.contacts.Cont.acts.PhoneContact;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.utils.NetworkUtils;
import com.ringcentral.android.utils.PhoneUtils;
import com.ringcentral.android.voip.RCSIPParameters;
import com.ringcentral.android.voip.RCUserConfigurationInfo;
import com.ringcentral.android.voip.VoipCallStatusActivity;
import com.ringcentral.android.voip.VoipInCall;

/**
 * PJSIP Service.
 */
public class ServicePJSIP extends Service implements IPJSIPCallbackListener {
    /**
     * Conditional variables.
     */
    private static final boolean HANG_UP_ON_NETWORK_CHANGE = true;

    
    
	public static final int PJ_SUCCESS	= 0;
    static final String TAG = "[RC]ServicePJSIP";
    public static final String TAG_EMG = "[RC]Validation";

    //public static final String sf_RC_SIP_Prefix = "sip.ringcentral.com"; 
   // public static final String sf_RC_SIP_Prefix = "192.168.92.4";
    //public static final String sf_RC_SIP_Prefix = "10.32.51.25";
    //public static final String sf_RC_SIP_Prefix =RCMProviderHelper.getTSCEnalbe(RingCentralApp.getContextRC()) ? "192.168.92.4":"10.32.51.25";
    public static final String sf_RC_SIP_Prefix="sip.stage.ringcentral.com";
    private int UNKNOWN_NETWORK_TYPE = -1;
    
	public static final long sf_RING_STOP_DELAY_INTERVAL	= 30; //on seconds
	
    public static final long sf_VIBRATE_DURATION		= 400;
    public static final long sf_VIBRATE_PAUSE_DURATION	= 2500;
    
    public static final long[] sf_VIBRATE_PATTERN = { 0, sf_VIBRATE_DURATION, sf_VIBRATE_PAUSE_DURATION };
    
    public static final int MSG_CALL_ANSWER = 1;
    public static final int MSG_CALL_ANSWER_AND_HOLD = 2;
    public static final int MSG_CALL_ANSWER_AND_HANGUP = 3;
    public static final int MSG_CALL_MAKE_CALL_COMPLETE = 4;
    public static final int MSG_CALL_CONSISTENCY_CHECK  = 5;
    public static final int MSG_CALLS_NOTIFICATION_CONSISTENCY_CHECK  = 6;
    public static final int MSG_LOCKERS_CONSISTENCY_CHECK  = 7;
    public static final int MSG_COMMANDS_CONSISTENCY_CHECK  = 8;
    
    /**
     * Timeout for checking lockers.
     */
    public static final long MSG_LOCKERS_CONSISTENCY_CHECK_DELAY  = 30000;

    private volatile ServiceState mState = new ServiceState(); 
    public ServiceState getState() {
        return mState;
    }
    
    /**
     * Used for detection If login occurred when SIP stack was not started yet
     */
    private boolean m_loginDetected = false;
    

    private LoginInfo m_logInfo = new LoginInfo();
    private  LoginInfo getLogInfo(){
    	return m_logInfo;
    }


    private HttpRegisterResponse m_httpregInfo = null;

	private WrapPJSIP m_pjsip_wrapper = null;
//    private boolean m_foreground = false;
    
    private static ServicePJSIP s_serivice = null;
    
    private CallbackWrapper m_callback = new CallbackWrapper();
    
//    private static final Class<?>[] m_SetForegroundSignature = new Class[] {  boolean.class};
//    private static final Class<?>[] m_StartForegroundSignature = new Class[] { int.class, Notification.class};
//    private static final Class<?>[] m_StopForegroundSignature = new Class[] { boolean.class};

//    private Method m_SetForeground		= null;
//    private Method m_StartForeground	= null;
//    private Method m_StopForeground 	= null;

    private LoginReceiver 	m_loginReceiver 					= null;
    private LogoutReceiver 	m_logoutReceiver 					= null;
    private WifiStateChangedReceiver 	m_wifistateReceiver 	= null;
    private PhoneStateChangedReceiver 	m_phstateReceiver 		= null;
    private NetworkConnectivityActionReceiver m_netconnReceiver = null;
    private PhoneStateListener mPhoneStateListener              = null;
    private RCApplicationStartedReceiver mRCAppStarted 			= null;
    private VoipConfigurationChangedReceiver m_voipConfChangedReceiver 		= null;
    private UserActionReceiver m_userActionReceiver 			= null;
    private WiFiWatcher mWiFiWatcher 							= null;
    
    /**
     * Keeps broadcast receiver for {@link RCMConstants#ACTION_STATION_LOCATION_CHANGE}
     */
    private OnStationChangedReceiver mOnStationChangedReceiver  = null;
    
    private TelephonyManager m_telefMng 		= null;
    private NotificationManager m_notifMng 		= null;
    private ConnectivityManager m_connManager 	= null;
    private AudioManager m_audioManager			= null;
    
    
    private long m_incomingNotificationId = PJSIP.sf_INVALID_RC_CALL_ID;
    private Intent m_incomingCallIntent = null;

    AudioSetupEngine mAudioSetupEngine = null;
    
    RCMediaManager mMediaManager;
    
    private IncomingCallNotification mIncomingNotification;

    /**
     * Current type of network. Currently is supported TYPE_MOBILE (0) 
     * and WIFI (1)
     */
    private int m_networkType = UNKNOWN_NETWORK_TYPE;
    

    
	/**
     * This variable is used for save current number of active SIP calls
     * (there are calls that have states another then UNKNOWN & DISCONNECTED)
     */
    private int m_activeCallsCounter = 0;

	/**
     * This object is used for receiving user info 
     * are used for HTTP registration 
     */
    private IUserConfigurationInfo m_userInfo = null;
    
    /**
     * SIP configuration parameters
     */
    private ISIPParameters m_sipParameters = null;
    
//    private IExternalResources m_extResource = new SipResources();
    
    /**
     * Keeps wake lock for SIP commands execution.
     */
    private ServicePJSIPWakeLock mWakeLock;
    
    /**
     * Keeps wake lock for incoming calls.
     */
    private ServicePJSIPIncomingCallsLock mWakeLockForIncomingCalls;
    
    /**
     * Keeps SIP commands asynchronous executor (<code>Command</code> processor).
     */
    private CommandProcessor mCommandProcessor;
    
    /**
     * Keeps low priority commands asynchronous executor (<code>Command</code> processor).
     */
    //private CommandProcessor mLowPrioCommandProcessor;
    
    WifiManager mWiFiManager;

	private Handler m_handler = new ServicePJSIPHandler();
	
	private CallManager m_callManager = new CallManager();
	
    /*
     * Functions
     */
	
	CallManager getCallMng(){
		return m_callManager;
	}
	
	WrapPJSIP getWrapper(){
		return m_pjsip_wrapper;
	}
	
	Handler getHandler(){
		return m_handler;
	}
	
	void injectCommand(Command cmd) {
	    if (mCommandProcessor != null) {
            mCommandProcessor.execute(cmd);
        }
	}
	
	private volatile int m_radioNetworkType = TelephonyManager.NETWORK_TYPE_UNKNOWN;
	protected int getRadioNetworkType() {
        return m_radioNetworkType;
    }
	protected void setRadioNetworkType(int networkType) {
        if (m_radioNetworkType != networkType) {
            if (LogSettings.MARKET) {
                MktLog.i(
                        TAG,
                        "setRadioNetworkType from:" + NetworkUtils.getRadioNetworkTypeLabel(m_networkType) + " to:"
                                + NetworkUtils.getRadioNetworkTypeLabel(networkType));
            }
            m_radioNetworkType = networkType;
        }
    }
	
    protected int getNetworkType() {
		return m_networkType;
	}

    protected void setNetworkType(int networkType) {
        if (m_networkType != networkType) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "setNetworkType from:" + NetworkUtils.getNetworkTypeLabel(m_networkType) + " to:"
                        + NetworkUtils.getNetworkTypeLabel(networkType));
            }
            m_networkType = networkType;
        }
    }
    
	/*
	 * 
	 */
    private class LoginInfo {
    	private String m_ext;
    	private String m_pin;
    	private String m_psw;
    	private long m_mbox = 0;
    	
//    	public void clear(){
//    		m_ext = null;
//    		m_pin = null;
//    		m_psw = null;
//    		m_mbox = 0;
//    	}
    	
    	public boolean isValid(){
    		return ( m_mbox > 0 );
    	}
		public String getExt() {
			return m_ext;
		}
		public void setExt(String ext) {
			m_ext = ext;
		}
		public String getPin() {
			return m_pin;
		}
		public void setPin(String pin) {
			m_pin = pin;
		}
		public String getPsw() {
			return m_psw;
		}
		public void setPsw(String psw) {
			m_psw = psw;
		}
		public long getMbox() {
			return m_mbox;
		}
		public void setMbox(long mbox) {
			m_mbox = mbox;
		}
    };
    
	protected synchronized int getActiveCallsCounter() {
		return m_activeCallsCounter;
	}
	protected synchronized void setActiveCallsCounter(int activeCallsCounter) {
		m_activeCallsCounter = activeCallsCounter;
	}
	
	protected void updateActiveCallsCounter(){
		
    	mCommandProcessor.execute(new Command("updateCallsCounter"){

			@Override
			public void run() {
				int activeCalls = 0;
				int callsOnHold = 0;
				long start = SystemClock.elapsedRealtime();
				if( !getState().isSipStackCreated()){
					setActiveCallsCounter(0);
				}
				else{
					final int calls = getCallMng().call_get_count();
					setActiveCallsCounter(calls);
					activeCalls = calls;
					
					/*
					 * If we have active calls try to detect number calls on hold 
					 */
					if( activeCalls > 0 ){
						
		                final long[] callIds = getCallMng().call_get_active_calls();
		                if( callIds != null ){
	                	
		                	CallInfo callInfo = new CallInfo();
		                	for( int i = 0; i < callIds.length; i++ ){
		                		int result = getCallMng().call_get_info( callIds[i], callInfo );
		                		if( result == PJSIP.sf_PJ_SUCCESS && callInfo.isHold()){
		                			callsOnHold++;
		                		}
		                	}
		                }
					}
				}

    	        if (LogSettings.MARKET) {
    	        	MktLog.w(TAG, "updateActiveCallsCounter Detected calls Active: " +
    	        			"" + activeCalls + " onHold:" + callsOnHold + " Command executed: " + (SystemClock.elapsedRealtime() - start));
    	        }
    	        ensureAudioRouteSet(false);
    	        sendCallsCountNotification( activeCalls, callsOnHold );
			}
    	});		
	}

	/**
	 * Send calls number notification
	 */
	protected void sendCallsCountNotification( final int activeCalls, final int callsOnHold ){
        if (LogSettings.MARKET) {
        	MktLog.d(TAG, "sendCallsCountNotification Detected calls Active: " + activeCalls + " onHold:" + callsOnHold );
        }
		
        boolean start = false;
        synchronized(lastCallCountNotificationLock) {
            if (activeCalls == 0 && callsOnHold == 0) {
                lastCallCountNotificationHasCallsSent = false;
            } else {
                lastCallCountNotificationHasCallsSent = true;
            }
            if (!lastCallCountNotificationCheckInjected) {
                lastCallCountNotificationCheckInjected = true;
                start = true;
            }
        }
        if (start) {
            m_handler.sendMessageDelayed(m_handler.obtainMessage(MSG_CALLS_NOTIFICATION_CONSISTENCY_CHECK), 
                    TIMEOUT_FOR_CALLS_COUNT_CONSISTENCY_CHECK);
        }
        
        Intent iVoipNumberCallsChanged = new Intent(RCMConstants.ACTION_VOIP_CALLS_NUMBER_CHANGED);
        iVoipNumberCallsChanged.putExtra(RCMConstants.EXTRA_VOIP_CALLS_NUMBER_CHANGED_TOTAL, activeCalls );
        iVoipNumberCallsChanged.putExtra(RCMConstants.EXTRA_VOIP_CALLS_NUMBER_CHANGED_ON_HOLD, callsOnHold );
        getApplicationContext().sendBroadcast(iVoipNumberCallsChanged);
	}

    private volatile boolean lastCallCountNotificationHasCallsSent = false;
    private volatile Object lastCallCountNotificationLock = new Object();
    private volatile boolean lastCallCountNotificationCheckInjected = false;
    private static final long TIMEOUT_FOR_CALLS_COUNT_CONSISTENCY_CHECK = 3000;
    
    private void onCallsNotificationConsistencyCheck() {
        int calls = getCallMng().call_get_count();
        int state = getState().state();
        if (LogSettings.MARKET) {
            MktLog.d(TAG_EMG, "onCallsNotificationConsistencyCheck");
        }
        boolean update = false;
        synchronized (lastCallCountNotificationLock) {
            lastCallCountNotificationCheckInjected = false;
            if (lastCallCountNotificationHasCallsSent) {
                if (calls == 0 || state != ServiceState.ON) {
                    update = true;
                }
            } else {
                if (calls > 0) {
                    update = true;
                }
            }
        }
        if (update) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG_EMG, "onCallsNotificationConsistencyCheck->updateActiveCallsCounter");
            }
            updateActiveCallsCounter();
        }
    }
    /**
     * Set SIP registration state
     */
	protected void setSIP_registration_state(
			boolean sIPRegistrationState) {
		
		if( getState().isSIPRegistrationState() != sIPRegistrationState ){
			
		    if (LogSettings.ENGINEERING) {
		        EngLog.i(TAG, "SIP registration state has been changed from:" + 
		                getState().isSIPRegistrationState() + 
		        		" To:" + sIPRegistrationState );
		    }
		    
            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "setSIP_registration_state changed to:" + sIPRegistrationState );
            }
		    
		    synchronized(this) {
				getState().setSIPRegistrationState(sIPRegistrationState);
			}
//		    /*
//		     * If incoming calls are enabled send SIP state changed notification
//		     */
//		    if( !getSipParameters().isOutboundCallsOnly() ){
//		    	updateVoipState();
//		    }
		}
	}

    protected synchronized boolean isLoginDetected() {
		return m_loginDetected;
	}
    
	protected synchronized void setLoginDetected(boolean loginDetected) {
        if (LogSettings.ENGINEERING) {
        	EngLog.i(TAG, "setLoginDetected: Login flag changed from:" + m_loginDetected + " To:" + loginDetected );
        }
		
		m_loginDetected = loginDetected;
	}

    protected synchronized void setIncomingCallIntent( Intent incomingCallIntent ){
    	m_incomingCallIntent = incomingCallIntent;
    }
    protected synchronized Intent getIncomingCallIntent(){
    	return m_incomingCallIntent;
    }
    
    protected synchronized long getIncomingNotificationId() {
		return m_incomingNotificationId;
	}
	protected void setIncomingNotificationId(long incomingNotificationId) {
		
		synchronized( this ){
			m_incomingNotificationId = incomingNotificationId;
		}
		
		if( m_incomingNotificationId == PJSIP.sf_INVALID_RC_CALL_ID ){
			setIncomingCallIntent( null );
		}
	}

    protected IUserConfigurationInfo getUserInfo(){
    	return m_userInfo;
    }
    
	protected ISIPParameters getSipParameters() {
		return m_sipParameters;
	}
//	protected void setSipParameters(ISIPParameters sipParameters) {
//		m_sipParameters = sipParameters;
//	}

	/**
     * Result of call HTTP registration procedure 
     * is executed on base user's credentials. 
     * 
     * Data is expired only when user make logout operation   
     */
    
    protected  void setHttpRegInfo( HttpRegisterResponse httpregInfo ){
        boolean cleared = false;
        boolean set = false;
        synchronized (this) {
            if (httpregInfo == null && m_httpregInfo != null) {
                cleared = true;
            }
            m_httpregInfo = httpregInfo;
            if (null != m_httpregInfo) {
                set = true;
            }
        }
    	if (cleared) {
    	    if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "setHttpRegInfo: Clear HttpRegisterResponse");
            }
    	}
        if (set) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG_EMG, "setHttpRegInfo: New instance");
            }
        }
    }
    protected synchronized HttpRegisterResponse getHttpRegInfo(){
    	return m_httpregInfo;
    }
    
    public void PJSIP_addCallback(int hash, IServicePJSIPCallback callback) {
    	
    	if( m_callback != null ){
    		m_callback.add( new Integer(hash), callback );
    	}
	}
    
    public void PJSIP_removeCallback(int hash) {
    	
    	if( isCallback() ){
    		m_callback.remove( hash );
    	}
	}
    
    public boolean isCallback(){
    	return ( m_callback != null && !m_callback.isEmpty() );
    }

    /**
     * Constructs PJSIP Service.
     */
    public ServicePJSIP(){
    	s_serivice = this;
    	m_pjsip_wrapper = new WrapPJSIP();
    }
    
    
    public static ServicePJSIP getInstance(){
    	return s_serivice;
    }
	
    /**
     * Return current instance of PJSIP callback listener
     * We will use ServicePJSIP object for now.
     * However possible the separate class will be used for it. 
     */
    public IPJSIPCallbackListener getPJSIPCallbackListener(){
    	return this;
    }
    




//	public boolean isForeground() {
//		return m_foreground;
//	}
//	
//	private void setForegroundState( boolean foreground){
//		m_foreground = foreground;
//	}

	void CPUlock() {
	    if (mWakeLock != null) {
            mWakeLock.acquire();
        }    
	}
	
	void CPUunlock() {
	    if (mWakeLock != null) {
            mWakeLock.release();
        }
	}

//	private void cpuUnLockAll() {
//	    if (mWakeLock != null) {
//            mWakeLock.releaseAll();
//        }
//	}
	
    @Override
    public void onCreate(){
        super.onCreate();

        if (LogSettings.MARKET) {
            MktLog.e(TAG, "onCreate: Service starting...");
        }
        mMediaManager = new RCMediaManager(m_callback, getPJSIPWrapper());        
        
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = new ServicePJSIPWakeLock(pm);
        mWakeLockForIncomingCalls = new ServicePJSIPIncomingCallsLock(pm);
        
        mCommandProcessor = new CommandProcessor("SrvcPJSIP", android.os.Process.THREAD_PRIORITY_URGENT_DISPLAY, mWakeLock);
        //mLowPrioCommandProcessor = new CommandProcessor("LowSrvcPJSIP", android.os.Process.THREAD_PRIORITY_LOWEST, null);
            
        m_userInfo = new RCUserConfigurationInfo( getApplicationContext() );
        m_sipParameters = new RCSIPParameters( getApplicationContext() );

        m_notifMng = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        
//        try {
//        	m_StartForeground = ReflectionHelper.getMethod(getClass(), "startForeground", m_StartForegroundSignature);
//        	m_StopForeground = ReflectionHelper.getMethod(getClass(), "stopForeground", m_StopForegroundSignature);
//        } catch (NoSuchMethodException e) {
//            // Running on an older platform.
//            m_StartForeground = m_StopForeground = null;
//        }
//        
//        if( m_StartForeground == null ){
//            try {
//            	m_SetForeground = ReflectionHelper.getMethod(getClass(), "setForeground", m_SetForegroundSignature);
//
//            } catch (NoSuchMethodException e) {
//                throw new IllegalStateException(
//                        "OS doesn't have Service.startForeground OR Service.setForeground!");
//            }
//        }
        
        if( m_telefMng == null ){
        	m_telefMng = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        }

        if( mWiFiManager == null )
            mWiFiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        
        if( m_connManager == null )
        	m_connManager = (ConnectivityManager) getSystemService( Context.CONNECTIVITY_SERVICE );
        
        final NetworkInfo activeNet = m_connManager.getActiveNetworkInfo();
        if( activeNet != null ){
        	setNetworkType( activeNet.getType() );
        }
        
        if( m_telefMng != null ){
            setRadioNetworkType(m_telefMng.getNetworkType());
        }
        
        if( m_audioManager == null )
        	m_audioManager = (AudioManager) getSystemService( Context.AUDIO_SERVICE );        
        
        m_phstateReceiver = new PhoneStateChangedReceiver();
        IntentFilter intentFilterPhoneState = new IntentFilter(	android.telephony.TelephonyManager.ACTION_PHONE_STATE_CHANGED );
        registerReceiver(m_phstateReceiver, intentFilterPhoneState);
        
        m_wifistateReceiver = new WifiStateChangedReceiver();
        IntentFilter intentFilterWifiState = new IntentFilter( android.net.wifi.WifiManager.WIFI_STATE_CHANGED_ACTION );
        registerReceiver(m_wifistateReceiver, intentFilterWifiState);
		
        m_netconnReceiver = new NetworkConnectivityActionReceiver();
        IntentFilter intentFilterNetConnectivity = new IntentFilter( android.net.ConnectivityManager.CONNECTIVITY_ACTION );
        registerReceiver(m_netconnReceiver, intentFilterNetConnectivity);
        
        if (m_telefMng != null) {
            mPhoneStateListener = new ServicePhoneStateListener();
            try {
                m_telefMng.listen(mPhoneStateListener, PhoneStateListener.LISTEN_SERVICE_STATE);
            } catch (java.lang.Throwable t) {

            }
        }
        
        mRCAppStarted = new RCApplicationStartedReceiver();
        IntentFilter intentFilterRCAppClosed = new IntentFilter(RCMConstants.ACTION_RC_APP_STARTED);
        registerReceiver(mRCAppStarted, intentFilterRCAppClosed);
        
        m_userActionReceiver = new UserActionReceiver();
        IntentFilter intentFilterActionUserPresent = new IntentFilter( android.content.Intent.ACTION_USER_PRESENT );
        registerReceiver(m_userActionReceiver, intentFilterActionUserPresent);
        
        try {
            mOnStationChangedReceiver = new OnStationChangedReceiver();
            IntentFilter intentFilterOnStationChangedReceiver = new IntentFilter(RCMConstants.ACTION_STATION_LOCATION_CHANGE);
            registerReceiver(mOnStationChangedReceiver, intentFilterOnStationChangedReceiver, RCMConstants.INTERNAL_PERMISSION_NAME, null);
        } catch (java.lang.Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "OnStationChangedReceiver registration failed: " + th.toString());
            }
            mOnStationChangedReceiver = null;
        }
        
		/*
		 *	Create Login & Logout notification receivers 
		 */
	    m_loginReceiver = new LoginReceiver();
        IntentFilter intentFilterLogin = new IntentFilter(RCMConstants.ACTION_LOGIN_FINISHED);
        registerReceiver(m_loginReceiver, intentFilterLogin);
        
	    m_logoutReceiver = new LogoutReceiver();
        IntentFilter intentFilterLogout = new IntentFilter(RCMConstants.ACTION_RESTART_APP);
        registerReceiver(m_logoutReceiver, intentFilterLogout);

        m_voipConfChangedReceiver = new VoipConfigurationChangedReceiver();
        IntentFilter intentFilterIncomingCalls = new IntentFilter(RCMConstants.ACTION_VOIP_CONFIGURATION_CHANGED);
        registerReceiver(m_voipConfChangedReceiver, intentFilterIncomingCalls);        
        
        mWiFiWatcher = new WiFiWatcher(this);
        
        mIncomingNotification = new IncomingCallNotification();
        
        /*
         * Update VoIP state 
         */
        //updateVoipState();
        
        /*
         * Initialize calls number notification
         */
        sendCallsCountNotification( 0, 0 );
        
        /**
         * Starts commands consistency checking.
         */
        ingectCommandsConcistencyCheck(new CommandsConcistencyCheck());
        
        /*
         * Check if we have correct login already
         */
        if( m_userInfo.isValidMailboxId() ){
        	
		    if (LogSettings.MARKET) {
		        MktLog.i(TAG, "onCreate Set login detected set to TRUE");
		    }
        	
        	setLoginDetected( true );
        }
    }
    
    WiFiWatcher getWiFiWatcher() {
        return mWiFiWatcher;
    }
    
    ServicePJSIPWakeLock getWakeLock() {
        return mWakeLock;
    }
    
    ServicePJSIPIncomingCallsLock getWakeLockForIncomingCalls() {
        return mWakeLockForIncomingCalls;
    }
    
	/**
	 * Check if we have active (valid) account.
	 * We should do it because PJSIP is always return an account 
	 * after call acc_get_default() even if It is not correct
	 */
	private int CheckCurrentAccount(){
		int accountId = PJSIP.sf_INVALID_ACCOUNT_ID;
		
		if( getState().isSipStackStarted()){
			
			accountId = getWrapper().PJSIP_acc_get_default();
			if( accountId >= 0 ){
			    boolean accValid = getWrapper().PJSIP_pjsip_acc_is_valid(accountId);
				
			    if (LogSettings.MARKET) {
			        MktLog.i(TAG, "Current default account ID: " + accountId + " valid: " + accValid );
			    }
			    
			    if( !accValid )
			    	accountId = PJSIP.sf_INVALID_ACCOUNT_ID;
			}
			else
				accountId = PJSIP.sf_INVALID_ACCOUNT_ID;				
		}			
		return accountId;
	}
	
	/**
	 * 	Service control interface
	 */

	/*
	 * 
	 */
	protected void RingtoneStop(){
    	mCommandProcessor.execute(new Command("ringtoneOff"){
			@Override
			public void run() {
				 ringtoneOff();
			}
    	});
	}
	
	/**
	 * Start ring
	 */
	protected void ringStart(String by){

        if (!getState().isSipStackStarted()) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "ringStart Failed! SIP Stack is not loaded yet. By:" + by);
            }
            return;
        }
   		int[] devices = getWrapper().PJSIP_pjsip_get_snd_dev(by);
		if( devices[0] == -1 || devices[1] == -1 ) {
		    //TODO: Check mobile call 
		    mMediaManager.startAudio(RCMediaManager.MODE_IN_CALL);
		}

        devices = getWrapper().PJSIP_pjsip_get_snd_dev(by + " 2");
        if (devices[0] == -1 || devices[1] == -1) {
            getWrapper().PJSIP_pjsip_set_snd_dev(by);
        }
		
		getWrapper().PJSIP_pjsip_ring_start(PJSIP.sf_INVALID_CALL_ID);
	}
	
	/**
	 * 
	 */
	protected void ringStop(String by){
	    if (!getState().isSipStackStarted()) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "ringStop Failed! SIP Stack is not loaded yet. By:" + by);
            }
            return; 
        }
		getWrapper().PJSIP_pjsip_ring_stop(PJSIP.sf_INVALID_CALL_ID);
	}
	
	/*
	 * 
	 */
//	private void startForegroundService(){
//		
//		if( isForeground())
//			return;
//
//		final int id = m_extResource.getServiceForegroundTitle();
//		
//		if (m_StartForeground != null){
//			
//			final Object[] m_StartForegroundArgs = new Object[2];
//	        m_StartForegroundArgs[0] = Integer.valueOf(id);
//	        m_StartForegroundArgs[1] = null;
//	        try {
//				m_StartForeground.invoke(this, m_StartForegroundArgs);
//				setForegroundState( true );
//			} catch (IllegalArgumentException e) {
//			} catch (IllegalAccessException e) {
//			} catch (InvocationTargetException e) {
//			}
//		}
//		else{
//			
//			final Object[] m_SetForegroundArgs = new Object[1];
//			try {
//				m_SetForegroundArgs[0] = Boolean.TRUE;
//				m_SetForeground.invoke(this, m_SetForegroundArgs );
//				
//				setForegroundState( true );
//				
//			} catch (IllegalArgumentException e) {
//			} catch (IllegalAccessException e) {
//			} catch (InvocationTargetException e) {
//			}
//		}
//	}
	
//	private void stopForegroundService(){
//		
//	    if ( m_StopForeground != null) {
//			final Object[] m_StopForegroundArgs = new Object[1];
//	        m_StopForegroundArgs[0] = Boolean.TRUE;
//	        try {
//				m_StopForeground.invoke(this, m_StopForegroundArgs );
//				setForegroundState( false );
//			} catch (IllegalArgumentException e) {
//			} catch (IllegalAccessException e) {
//			} catch (InvocationTargetException e) {
//			}
//	    }
//	    else{
//			final int id = m_extResource.getServiceForegroundTitle();
//
//	    	final Object[] m_SetForegroundArgs = new Object[1];
//	        m_SetForegroundArgs[0] = Boolean.FALSE;
//			try {
//				m_SetForeground.invoke(this, m_SetForegroundArgs );
//				setForegroundState( false );
//			} catch (IllegalArgumentException e) {
//			} catch (IllegalAccessException e) {
//			} catch (InvocationTargetException e) {
//			}
//	    }
//	}
	
	
	/*
	 * 
	 */
	protected int PJSIP_stop(){
        if (LogSettings.MARKET) {
            MktLog.e(TAG_EMG, "PJSIP_stop starting");
        }

        if (getState().state() != ServiceState.TO_OFF) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "PJSIP_stop: invalid state: " + getState());
            }
        }
        
	    /*
	     * Finish play Ringtone, Rings and remove HTTP registration sheduler
	     * 
	     */
	    ringtoneOff();
	    removeDelayHttpRegistration();	        
	    
		if( getState().isSipStackCreated() ){
		    if (LogSettings.MARKET) {
	            MktLog.e(TAG_EMG, "> PJSIP Stopping: stack has been created, destroying...");
	        }
			
			/*
			 * Stop ring
			 */
		    ringStop("PJSIP stop");
			
		    getState().setSipStackCreated( false );
		    getState().setSipStackStarted( false );
			
			int result;
			synchronized( this ){
				result = PJSIP.pjsip_destroy();
			}
			
            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "> PJSIP Stopping: stack has been created, destroyed with result=" + result);
            }

			if( result != PJ_SUCCESS ){
			    if (LogSettings.MARKET) {
			        MktLog.e(TAG, "PJSIP_stop pjsip_destroy failed. Status:" + result );
			    }
			}
		}
		else{
			if (LogSettings.ENGINEERING) {
			    EngLog.w(TAG, "PJSIP_stop SIP stack is not created.");
			}
			
		}
		
		if(mWiFiWatcher != null) {
			mWiFiWatcher.stopWatching();
		}

        /*
         * Send calls number notification
         */
		// TODO: if not?
		ensureAudioRouteSet(false);
        sendCallsCountNotification( 0, 0 );        
		
		if (LogSettings.MARKET) {
		    MktLog.i(TAG, "PJSIP_stop finished");
		}
		if (getState().state() == ServiceState.TO_ON) {
		    if (LogSettings.MARKET) {
	            MktLog.e(TAG, "PJSIP_stop ignore setting OFF:" + getState());
	        }
		} else {
		    getState().set(ServiceState.OFF);    
		}

        if (mWakeLockForIncomingCalls != null) {
            if (mWakeLockForIncomingCalls.isHeld()) {
                if (mWakeLockForIncomingCalls.getLockCounter() > 1) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "PJSIP_stop: WakeLockForIncomingCalls is held (count="
                                + mWakeLockForIncomingCalls.getLockCounter() + ")");
                    }
                }
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "PJSIP_stop: WakeLockForIncomingCalls release");
                }
				mWakeLockForIncomingCalls.releaseAll();
            } else {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "PJSIP_stop: WakeLockForIncomingCalls was not held");
                }
            }
        }

        m_handler.removeMessages(MSG_LOCKERS_CONSISTENCY_CHECK);
        m_handler.sendMessageDelayed(m_handler.obtainMessage(MSG_LOCKERS_CONSISTENCY_CHECK),
                MSG_LOCKERS_CONSISTENCY_CHECK_DELAY);
		return PJSIP.sf_PJ_SUCCESS;
	}
	
	/*
	 * 
	 */
	protected boolean PJSIP_init(final String sipOutboundProxy, final int SIPTransportLocalPort){
        int result;
        
        if (LogSettings.MARKET) {
        	MktLog.i(TAG, "PJSIP_init Initialize PJSIP stack");
        }
        getState().set(ServiceState.TO_ON_SIP_STACK_CREATION);
	    if( !getState().isSipStackCreated()){
	        
		    if (LogSettings.MARKET) {
	            MktLog.i(TAG, "Loading PJSIP librarys...");
	        }
		
	    	if( !loadSipStackLibrary()){
			    if (LogSettings.MARKET) {
			    	MktLog.e(TAG, "PJSIP_init Failed. PJSIP stack is not detected");
		        }
			    getState().set(ServiceState.OFF, RCSIP.SipCode.PJSIP_SC_SIP_LIBS_LOADING_FAILED_EXTENDED);
				return false;
			}
	    	
    		result = PJSIP.pjsip_create();
	    	
	    	if( result != PJ_SUCCESS ){
			    if (LogSettings.MARKET) {
			    	MktLog.e(TAG, "PJSIP_init Failed. pjsip_create failed");
		        }
			    getState().set(ServiceState.OFF, RCSIP.SipCode.PJSIP_SC_SIP_STACK_CREATION_FAILED_EXTENDED);
			    return false;	
	    	}
		    if (LogSettings.MARKET) {
	            MktLog.i(TAG, "PJSIP_init. pjsip_create finished successfully");
	        }
		    
	    	int t1=RCMProviderHelper.getTSCEnalbe(ServicePJSIP.this) ? 1 : 0;
	    	String t2=RCMProviderHelper.getTSCTunnelAddress(ServicePJSIP.this);
	    	int t3=RCMProviderHelper.getTSCTunnelPort_int(ServicePJSIP.this);
	    	int t4=RCMProviderHelper.getTSCRedundancy(ServicePJSIP.this);
	    	
	    	
		    result=PJSIP.pjsip_tsc_tunnel_set(t1,  t2,t3,  t4);
	    	result = PJSIP.pjsip_init(sipOutboundProxy, SIPTransportLocalPort);
		    
	    	if( result != PJ_SUCCESS ){
			    if (LogSettings.MARKET) {
			    	MktLog.e(TAG, "PJSIP_init Failed. pjsip_init failed");
		        }
			    getState().set(ServiceState.OFF, RCSIP.SipCode.PJSIP_SC_SIP_STACK_CREATION_FAILED_EXTENDED);
			    return false;	
	    	}

	    	synchronized (getState()) {
	    	    getState().setSipStackCreated(true);
	    	}
			
		    if (LogSettings.MARKET) {
	            MktLog.i(TAG, "PJSIP_init. pjsip_init finished successfully");
	        }
		    
	    }
	    else{
		    if (LogSettings.MARKET) {
	            MktLog.i(TAG, "PJSIP_init. PJSIP stack is initilized already");
	        }
	    }
		
	    return true;
	}
	/*
	 * 
	 */
	protected boolean PJSIP_start( ){
		
	    if (LogSettings.MARKET) {
            MktLog.i(TAG_EMG, "> PJSIP starting...");
        }

        
        if( !getState().isSipStackStarted() ){

            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "> PJSIP starting (stack has not been created)...");
            }
        	
            int result;
           	result = PJSIP.pjsip_start();
            
            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "< PJSIP starting finished with result=" + result);
            }
            if( result != PJ_SUCCESS ){
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "PJSIP_start pjsip_start failed. Status:" + result );
                }
                return false;
            }
            
            setPJSIPCodecPriorities();		    
            getState().setSipStackStarted( true );
        }
        else{
		    if (LogSettings.MARKET) {
	            MktLog.i(TAG, "PJSIP_start. PJSIP stack is started already");
	        }
        }
		
		return true;
	}
	
	/**
	 * Make call
	 */
	private class MakeCallCommand extends Command {
		long m_rcCallId = PJSIP.sf_INVALID_RC_CALL_ID;
		
		public MakeCallCommand( final long rcCallId ) {
			super("MakeCall");
			
			m_rcCallId = rcCallId;
		}

        @Override
        public void run() {
            RCCallInfo rcCallInfo = null;
            String uri = null;

            rcCallInfo = getCallMng().getCallByRCCallId(m_rcCallId);
            if (rcCallInfo == null) {
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "MakeCallCommand Ignored for RC CallId : " + m_rcCallId + " : Not found");
                }
                ServicePJSIP.getInstance().updateActiveCallsCounter();
                stopRings();
                return;
            } else if (rcCallInfo.isEnding() || !rcCallInfo.isAlive() || rcCallInfo.isIncoming()) {
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "MakeCallCommand Ignored: " + rcCallInfo.toString());
                }
                ServicePJSIP.getInstance().updateActiveCallsCounter();
                stopRings();
                return;
            }

            uri = rcCallInfo.getPartyUri();
            if (!uri.startsWith(CallInfo.sf_SIP_PREFIX)) {
                uri = CallInfo.sf_SIP_PREFIX + rcCallInfo.getPartyUri() + "@" + getSipParameters().getServiceProvider();
            }

            if (LogSettings.MARKET) {
                MktLog.i(TAG, "MakeCallCommand for RC CallId : " + m_rcCallId + " [" + rcCallInfo.getPartyUri() + "] URI [" + uri + "]");
            }
            /*
             * Check if service is ready to make call
             */
            if (getState().state() != ServiceState.ON) {
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "MakeCallCommand Failed for RC CallId : " + m_rcCallId + ". Call is disabled. accountId ["
                            + rcCallInfo.getPartyUri() + "] URI [" + uri + "]");
                    MktLog.d(TAG, rcCallInfo.toString());
                }
                synchronized (getCallMng()) {
                    rcCallInfo.setEnding(RCCallInfo.CALL_ENDED_BY_ERROR);
                    rcCallInfo.setMainError(RCSIP.SipCode.PJSIP_SC_CALLS_EXISTS_WHEN_SIP_FALSE_STATUS_CODE_EXTENDED);
                    rcCallInfo.onCallStatusChange(false, RCSIP.PJSUA_CALL_STATE_DISCONNECTED, RCSIP.SipCode.PJSIP_SC_SERVICE_UNAVAILABLE.code(), null);
                    getCallMng().deleteCall(m_rcCallId);
                }
                getCallMng().ensureClientNotified(rcCallInfo, true, true);
                updateActiveCallsCounter();
                stopRings();
            } else {
                /*
                 * Add custom Caller-ID through P-Asserted-Identity parameter
                 */
                final String callerId = getUserInfo().getCallerID();
                final String[] headers = new String[1];
                headers[0] = String.format("%s:<%s%s@%s>", PJSIP.sf_SIP_header_P_Asserted_Identity, CallInfo.sf_SIP_PREFIX, callerId,
                        getSipParameters().getServiceProvider());
                getCallMng().call_make_call(m_rcCallId, 0, uri, headers);
            }
        }
    }	
	
	
	/**
	 * 
	 * StartPJSIPCommand
	 */
	class StartPJSIPCommand extends Command {
	    private boolean m_testMode = false;
	    
	    public StartPJSIPCommand( boolean testMode ) {
	        super("PJSIP Start");
	        
	        m_testMode = testMode;
	    }
	    
	    // TODO: CLEAN-UP CALLS on failed result
	    
	    @Override
	    public void run() {

            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "StartPJSIPCommand: Start SIP: Current state: " + getState());
            }
	    	
            if (getState().state() != ServiceState.TO_ON) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG_EMG, "StartPJSIPCommand: State Error: " + getState());
                    getState().set(ServiceState.TO_ON);
                }
            }
            
	    	/*
	    	 * Skip all tests on test mode
	    	 */
	    	if( !m_testMode ){
	    		
		    	final boolean voipEnabled = getSipParameters().isVoipEnabled();
		    	final boolean voipOver3GEnabled = getSipParameters().isVoipOver3GEnabled();
	    		
			    if (LogSettings.MARKET) {
			        MktLog.i(TAG, "StartPJSIPCommand is starting... VoIPEnabled: " + voipEnabled + 
			        		" voipOver3GEnabled:" + voipOver3GEnabled );
			    }
		    	
		    	/*
		    	 * If VoIP is disabled. Do nothing
		    	 */
		    	if( !voipEnabled ){
	                if (LogSettings.MARKET) {
	                    MktLog.i(TAG_EMG, "StartPJSIPCommand: VoIP is disabled. Do nothing");
	                }
	                // TODO:
	                getState().set(ServiceState.TO_OFF);
                    return;
		    	}
		    	
		        if(  !isLoginDetected() ){
	                if (LogSettings.MARKET) {
	                    MktLog.i(TAG, "StartPJSIPCommand: Login was not detected. Do nothing");
	                }
	                // TODO:
	                getState().set(ServiceState.OFF);
                    return;
		        }	    	
		    	
		    	if( m_connManager != null ){
		    		final NetworkInfo netInfo = m_connManager.getActiveNetworkInfo();
		    		
		    		/*
		    		 * If active network is not connected the do nothing
		    		 */
		    		if( netInfo != null && !netInfo.isConnected()){
		                if (LogSettings.MARKET) {
		                    MktLog.i(TAG, "StartPJSIPCommand: Active network is not connected. Do nothing");
		                }
		                // TODO :
		                getState().set(ServiceState.OFF);
	                    return;
		    		}
		    		
		    		/*
		    		 * If current active network is mobile and VoIP over Wi-Fi is disabled
		    		 * then do nothing
		    		 */
		    		if( netInfo != null && netInfo.getType() == ConnectivityManager.TYPE_MOBILE ){
		    			if( !voipOver3GEnabled ){
		                    if (LogSettings.MARKET) {
		                        MktLog.i(TAG, "StartPJSIPCommand: Network type is Mobile. VoIP over 3G/4G is disabled. Do nothing");
		                    }
		                    getState().set(ServiceState.OFF);
		                    // TODO:
	                        return;
		    			}
		    		}
		    	}
	    	}	    	
	    	
	    	if(mWiFiWatcher != null) {
	    		mWiFiWatcher.startWatching();
	    	}
	    	
	    	/*
             * Initialize SIP stack
             */
	    	PJSIP_init( null, NetworkUtils.getDedicatedSIPTransportLocalPortIfrequired(ServicePJSIP.this) );
	    	
	    	if (getState().detailedState() != ServiceState.TO_ON_SIP_STACK_CREATION) {
	    	    if (LogSettings.MARKET) {
                    MktLog.e(TAG, "StartPJSIPCommand: PJSIP_init failed");
                }
	    	    return;
	    	}
	    	
	    	getState().set(ServiceState.TO_ON_SIP_STACK_STARTING);
	    	/*
             * Start SIP stack
             */
	    	if( !PJSIP_start() ){
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "StartPJSIPCommand: PJSIP_start failed");
                }
                
                getState().set(ServiceState.OFF, RCSIP.SipCode.PJSIP_SC_SIP_STACK_CREATION_FAILED_EXTENDED);
                return;
	    	}

	    	try{
	        	if( m_callback != null ){
	                if (LogSettings.MARKET) {
	                    MktLog.i(TAG, "StartPJSIPCommand Callback: SIP started");
	                }
					m_callback.on_sip_stack_started( getState().isSipStackStarted() );
	        	}	    	
	    	}
	    	catch( ConcurrentModificationException e_cm ){
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "StartPJSIPCommand Callback: SIP started", e_cm );
                }
	    	}
	    	
	    	/*
	    	 * On test mode we only start SIP stack and do nothing anymore
	    	 */
	    	if( m_testMode ){
	    	    getState().set(ServiceState.ON);
		        if (LogSettings.ENGINEERING) {
		            EngLog.i(TAG, "StartPJSIPCommand: Test mode is detected. Do nothing after start SIP stack.");
		        }
	    		return;
	    	}
	    	
	    	/*
	    	 * If SIP stack has been created and 
	    	 * we have an incoming call in the queue then start ring 
	    	 */
	        if( getState().isSipStackStarted() ){
	        	
	    		RCCallInfo call = getCallMng().getOutboundOnInitiatingState(false);
	    		if(call != null){
	                if (LogSettings.MARKET) {
	                    MktLog.w(TAG, "StartPJSIPCommand: Start ring for: " + call);
	                }
	                ensureAudioRouteSet(false);
	                if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
	                    Intent ringStartIntent = new Intent(RCMConstants.ACTION_VOIP_RING_STARTED);
	                    ringStartIntent.putExtra(RCMConstants.EXTRA_VOIP_RING_STARTED, SystemClock.elapsedRealtime() );
	                    getApplicationContext().sendBroadcast(ringStartIntent);
	                }
	    			ringStart("StartPJSIPCommand");
	    		}
	        }	    	
	        
            /*
             * Check if Login request was received during start stack operation
             */
	    	if( getHttpRegInfo() == null ){
	    	    getState().set(ServiceState.TO_ON_HTTP_REGISTRATION);	    	    
	    	    
		    	HttpRegistration httpReg = new HttpRegistration();
	        	
		        if( !getState().isHttpRegistrationOnProgress() ){
	    	        getState().setHttpRegistrationOnProgress( true );
	            	httpReg.run();
	            	// TODO: Make asynchronous
		        }
		        else{
	    	        if (LogSettings.MARKET) {
	    	            MktLog.d(TAG, "StartPJSIPCommand: HTTP registration is on progress. Ignore.");
	    	        }
		        }
	    	}
	    	else{
	    	    boolean isValid = getWrapper().PJSIP_pjsip_acc_is_valid(0);
	    	    if (!isValid) {
	    	        if (LogSettings.MARKET) {
	                    MktLog.w(TAG_EMG, "StartPJSIPCommand: HTTP registration was executed already. However accound is not valid.");
	                }
	    	        getState().set(ServiceState.TO_ON_HTTP_REGISTRATION);             
	                HttpRegistration httpReg = new HttpRegistration();
	                
	                if( !getState().isHttpRegistrationOnProgress() ){
	                    getState().setHttpRegistrationOnProgress( true );
	                    httpReg.run();
	                }
	                else{
	                    if (LogSettings.MARKET) {
	                        MktLog.d(TAG, "StartPJSIPCommand: HTTP registration is on progress. Ignore.");
	                    }
	                }
	                return;
	    	    }
    	        if (LogSettings.MARKET) {
    	            MktLog.d(TAG, "StartPJSIPCommand: HTTP registration was executed already. Ignore.");
    	        }
    	        getState().set(ServiceState.ON);
	    	}
	    	
            if (RCMProviderHelper.isAlwaysCPULockForSIPstack(ServicePJSIP.this)) {
                if (mWakeLockForIncomingCalls != null) {
                    if (mWakeLockForIncomingCalls.isHeld()) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "StartPJSIPCommand: WakeLockForIncomingCalls is held (count="
                                    + mWakeLockForIncomingCalls.getLockCounter() + ")");
                        }
                    } else {
                        if (LogSettings.MARKET) {
                            MktLog.w(TAG, "StartPJSIPCommand: WakeLockForIncomingCalls acquire");
                        }
                        mWakeLockForIncomingCalls.acquire();
                    }
                }
            }
	    }
	}

    /**
     * Lockers consistency check.
     */
    private void lockersConsistencyCheck() {
        if (getState().state() == ServiceState.OFF) {
            if (mWakeLockForIncomingCalls != null) {
                if (mWakeLockForIncomingCalls.isHeld()) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "lockersConsistencyCheck: WakeLockForIncomingCalls is held (count="
                                + mWakeLockForIncomingCalls.getLockCounter() + "). Force release.");
                    }
                    mWakeLockForIncomingCalls.releaseAll();
                } else {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "lockersConsistencyCheck: WakeLockForIncomingCalls is not held.");
                    }
                }
            }

            if (mWakeLock != null) {
                if (mWakeLock.isHeld()) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "lockersConsistencyCheck: WakeLock is held (count="
                                + mWakeLockForIncomingCalls.getLockCounter() + "). Force release.");
                    }
                    mWakeLock.releaseAll();
                } else {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "lockersConsistencyCheck: WakeLock is not held.");
                    }
                }
            }
        } else {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "lockersConsistencyCheck: State is not OFF. Ignore.");
            }
        }
    }
	
	/*
	 * 
	 */
	private boolean loadSipStackLibrary(){
	    if (LogSettings.MARKET) {
            MktLog.d(TAG, "loadSipStackLibrary is starting...");
        }
		
		if( getState().isSipStackCreated() ){
		    if (LogSettings.MARKET) {
	            MktLog.i(TAG, "loadSipStackLibrary SIP stack library is already loaded. Nothing to do.");
	        }
			return true;
		}
		
		String sipLibrary = this.getFilesDir().getParent() + File.separator + "lib" + File.separator + PJSIP.sf_PJSIP_LIB_NAME;
		
		File fSipLib = new File( sipLibrary );
		if( !fSipLib.exists() ){
		    if (LogSettings.MARKET) {
                MktLog.e(TAG, "SIP stack library is not present " + sipLibrary );
            }
			return false;
		}
		
		try {
			System.load(fSipLib.getAbsolutePath());
		}
		catch ( UnsatisfiedLinkError e_utf ){
		    if (LogSettings.MARKET) {
                MktLog.e(TAG, "Could not load SIP stack library (ULE):" + e_utf.getMessage() );
            }
			return false;
		}
		catch( SecurityException e_se ){
		    if (LogSettings.MARKET) {
		        MktLog.e(TAG, "Could not load SIP stack library (SE):" + e_se.getMessage() );
		    }
			return false;
		}
		
		if (LogSettings.MARKET) {
		    MktLog.i(TAG, "loadSipStackLibrary finished successfuly");
		}
		return true;
	}
	

	/*
	 * (non-Javadoc)
	 * @see android.app.Service#onLowMemory()
	 */
	public void onLowMemory (){
		super.onLowMemory();
		if (LogSettings.MARKET) {
            MktLog.i(TAG, "onLowMemory is starting...");
        }
	}
	
	
    /*
     * (non-Javadoc)
     * 
     * @see android.app.Service#onDestroy()
     */
    public void onDestroy() {
        super.onDestroy();

        if (LogSettings.MARKET) {
            MktLog.e(TAG, "onDestroy is starting...");
        }

        if (mMediaManager != null) {
            mMediaManager.destroy();
            mMediaManager = null;
        }
        ringtoneOff();

        if (m_handler != null) {
            m_handler = null;
        }

        if (mCommandProcessor != null) {
            mCommandProcessor.execute(new StopPJSIP());
        }

        if (m_notifMng != null) {
            m_notifMng = null;
        }

        if (m_telefMng != null) {
            if (mPhoneStateListener != null) {
                try {
                    m_telefMng.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
                } catch (java.lang.Throwable t) {
                }
                mPhoneStateListener = null;
            }
            m_telefMng = null;
        }

        if (m_connManager != null) {
            m_connManager = null;
        }

        if (m_audioManager != null) {
            m_audioManager = null;
        }


        /*
         * Unregister notifications
         */
        try {
            unregisterReceiver(m_phstateReceiver);
        } catch (java.lang.Throwable t) {
        }
        m_phstateReceiver = null;

        try {
        unregisterReceiver(m_wifistateReceiver);
        } catch (java.lang.Throwable t) {
        }
        m_wifistateReceiver = null;

        try {
        unregisterReceiver(m_loginReceiver);
        } catch (java.lang.Throwable t) {
        }
        m_loginReceiver = null;

        try {
        unregisterReceiver(m_logoutReceiver);
        } catch (java.lang.Throwable t) {
        }
        m_logoutReceiver = null;

        try {
        unregisterReceiver(m_voipConfChangedReceiver);
        } catch (java.lang.Throwable t) {
        }
        m_voipConfChangedReceiver = null;
        try {
        unregisterReceiver(mRCAppStarted);
        } catch (java.lang.Throwable t) {
        }
        mRCAppStarted = null;

        try {
        unregisterReceiver(m_userActionReceiver);
        } catch (java.lang.Throwable t) {
        }
        m_userActionReceiver = null;

        try {
        unregisterReceiver(m_netconnReceiver);
        } catch (java.lang.Throwable t) {
        }
        m_netconnReceiver = null;

        if (mWiFiWatcher != null) {
            mWiFiWatcher.destroy();
            mWiFiWatcher = null;
        }
        
        if(mIncomingNotification != null) {
        	mIncomingNotification.destroy();
        	mIncomingNotification = null;
        }

        try {
            if (mOnStationChangedReceiver != null) {
                unregisterReceiver(mOnStationChangedReceiver);
            }
            mOnStationChangedReceiver = null;
        } catch (java.lang.Throwable th) {
        }

        if (mWakeLock != null) {
            mWakeLock.releaseAll();
        }

        if (mWakeLockForIncomingCalls != null) {
            mWakeLockForIncomingCalls.releaseAll();
        }
        
        if (mCommandProcessor != null) {
            mCommandProcessor.destroy(false);
            mCommandProcessor = null;
        }

//        if (mLowPrioCommandProcessor != null) {
//            mLowPrioCommandProcessor.destroy(false);
//            mLowPrioCommandProcessor = null;
//        }

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onDestroy finished");
        }
    }
	
	/*
	 * PJSIP Stop
	 */
	class StopPJSIP extends Command {

		public StopPJSIP() {
			super("PJSIP_Stop");
		}
		
        @Override
        public void run() {
            PJSIP_stop();
        }
	};

	
	/* (non-Javadoc)
	 * @see android.app.Service#onBind(android.content.Intent)
	 */
	@Override
	public IBinder onBind(Intent intent) {
	    if (LogSettings.MARKET) {
	        MktLog.d(TAG, "onBind is starting...");
	    }
		return m_binder;
	}
	private ServicePJSIPbinder m_binder = new ServicePJSIPbinder(this);
	
	
	/*
	 * (non-Javadoc)
	 * @see android.app.Service#onUnbind(android.content.Intent)
	 */
	@Override
	public boolean onUnbind(Intent intent){
	    if (LogSettings.MARKET) {
	        MktLog.d(TAG, "onUnbind is starting...");
	    }
		return false;
	}


	@Override
	public void on_reg_state(int accountId) {
		
	    if (LogSettings.MARKET) {
	        MktLog.w(TAG, "on_reg_state is starting");
	    }
	    
        mCommandProcessor.execute(new OnRegState(accountId));
	}

	@Override
    public void on_message(long rcCallId, final SipMessageInput sipMsg) {
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "on_message RC CallId [" + rcCallId + "] Cmd [" + (sipMsg == null ? -1 : sipMsg.getHdr().getCmd())
                    + "]");
        }

        CPUlock();
        try {
            if (m_callback != null) {
                m_callback.on_message(rcCallId, sipMsg);
            }
        } catch (java.lang.Throwable th) {

        }

        CPUunlock();
    }

    @Override
    public void on_incoming_call(final CallInfo callInfo) {
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "on_icoming_call RC CallId:" + callInfo.getRcCallId() + " URI:" + callInfo.getRemoteURI());
        }

        CPUlock();
        try {
            if (m_callback != null) {
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "on_icoming_call From " + callInfo.getRemoteURI() + " RC[" + callInfo.getRcHeader() + "]");
                }
                m_callback.on_incoming_call(callInfo);
                // mLowPrioCommandProcessor.execute(new
                // BindContactCommand(callInfo.getRcCallId()));
                bindContactCommand(callInfo.getRcCallId());
            }
        } catch (java.lang.Throwable th) {

        }
        CPUunlock();
    }

    @Override
    public void on_call_media_state(long rcCallId, int state) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "on_start_call_media call ID" + rcCallId);
        }

        CPUlock();
        try {
            if (m_callback != null) {
                m_callback.call_media_state(rcCallId, state);
            }
        } catch (java.lang.Throwable th) {

        }
        CPUunlock();
    }

    @Override
    public void on_stop_call_media(long rcCallId) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "on_stop_call_media RC CallId" + rcCallId);
        }

        CPUlock();
        try {
            if (m_callback != null) {
                m_callback.call_media_stop(rcCallId);
            }
        } catch (java.lang.Throwable th) {

        }
        CPUunlock();
    }

    @Override
    public void notify_on_call_state(long rcCallId, int state, int last_status, int ending_reason, int media_sate) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "notify_on_call_state RC call ID:" + rcCallId + " state:" + RCSIP.getCallStatus(state)
                    + " last status:" + last_status + "/" + RCSIP.getSipResponseForLog(last_status));
        }

        CPUlock();
        try {
            if (m_callback != null) {
                m_callback.call_state(rcCallId, state, last_status, ending_reason, media_sate);
            }
        } catch (java.lang.Throwable th) {

        }
        CPUunlock();
    }	
	
	@Override
    public void PJSIPCallbackOnCallState(int pjsipCallId, int state, int last_status, String dump) {
        if (mCommandProcessor != null) {
            mCommandProcessor.execute(new PJSIPCallbackOnCallState(pjsipCallId, state, last_status, dump));
        }
    }

    /**
     * PJSIPCallbackOnCallState
     */
    private class PJSIPCallbackOnCallState extends Command {
        private int m_pjsipCallId;
        private int m_state;
        private int m_last_status;
        private String m_dump;

        public PJSIPCallbackOnCallState(int pjsipCallId, int state, int last_status, String dump) {
            super("PJSIPCallbackOnCallState");
            m_pjsipCallId = pjsipCallId;
            m_state = state;
            m_last_status = last_status;
            m_dump = dump;
        }

        @Override
        public void run() {
            if (getState().isSipStackStarted()) {
                getCallMng().OnCallState(m_pjsipCallId, m_state, m_last_status, m_dump);
            }
        }
    };
	
    @Override
    public void on_call_transfer_status(long rcCallId, int status, String statusProgressText) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "on_call_transfer_status RC CallId " + rcCallId + " status [" + status + "] Info:"
                    + (statusProgressText == null ? "" : statusProgressText));
        }

        CPUlock();
        try {
            if (m_callback != null) {
                m_callback.on_call_transfer_status(rcCallId, status, statusProgressText);
            }
        } catch (java.lang.Throwable th) {

        }
        CPUunlock();
    }

	@Override
	public void on_media_failed(int error) {
	    if (LogSettings.MARKET) {
	        MktLog.i(TAG, "on_media_failed " + error );
	    }
		
		if (mCommandProcessor != null) {
            mCommandProcessor.execute(new HangupAll(RCCallInfo.CALL_ENDED_BY_ERROR, RCSIP.SipCode.PJSIP_SC_MEDIA_ERROR_EXTENDED));
        }
	}

	/**
     * Execute HTTP registration with some interval
     */
    private Runnable httpRegDelay = new Runnable() {
        @Override
        public void run() {
            boolean stopped = false;
            synchronized (getState()) {
                boolean isDelayedHTTPregIsInProgress = getState().isHttpDelayedRegistrationOnProgress();
                if (isDelayedHTTPregIsInProgress) {
                    getState().setHttpDelayedRegistrationOnProgress(false);
                    stopped = true;
                }
            }
            
            if (stopped) {
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "HttpReg on delay (stopped)");
                }
            } else {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "HttpReg on delay (was not is in progress)");
                }
            }
            final boolean isStackStarted = getState().isSipStackStarted();

            if (LogSettings.MARKET) {
                MktLog.w(TAG, "HttpRegDelay stack started: " + isStackStarted);
            }

            if (isStackStarted) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "HttpRegDelay done, stack started, make HTTPregistration");
                }

                if (mCommandProcessor != null) {
                    mCommandProcessor.execute(new HttpRegistration());
                }
            } else {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "HttpRegDelay done, stack is not started, postpone HTTPregistration");
                }
                startDelayHttpRegistration();
            }
        }
    };

	/**
	 * Starts delayed HTTP registration if required.
	 */
    protected void startDelayHttpRegistration() {
        long timeout = 0;
        boolean start = false;
        synchronized (getState()) {
            boolean isDelayedHTTPregIsInProgress = getState().isHttpDelayedRegistrationOnProgress();
            if (!isDelayedHTTPregIsInProgress) {
                timeout = getState().getHTTPRegDelayedTimeout();
                start = true;
                getState().setHttpDelayedRegistrationOnProgress(true);
            }
        }

        if (start) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "startDelayHttpRegistration HTTP Registration delay start Interval(sec):" + timeout);
            }
            if (getHandler() != null) {
                getHandler().removeCallbacks(httpRegDelay);
                getHandler().postDelayed(httpRegDelay, timeout);
            }
        } else {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "startDelayHttpRegistration HTTP Registration delay is in progress.");
            }
        }
    }
	
	protected void removeDelayHttpRegistration(){
	    boolean remove = false;
	    synchronized (getState()) {
	        if (getState().isHttpDelayedRegistrationOnProgress()) {
	            remove = true;
	            getState().setHttpDelayedRegistrationOnProgress(false);
	        }
	    }
	    
        if (remove) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "removeDelayHttpRegistration: was is in progress");
            }
            getHandler().removeCallbacks(httpRegDelay);
        } else {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "removeDelayHttpRegistration: was not is in progress");
            }
        }
	}
	
	/**
	 * Start delay Ring stop.
	 * If Rings were not finished during sf_RING_STOP_DELAY_INTERVAL
	 * then stop it on anyway
	 */
	protected void startStopRingDelay(){

		if (LogSettings.MARKET) {
		    MktLog.w(TAG, "startStopRingDelay Stop Ring delay start Interval(sec):" + sf_RING_STOP_DELAY_INTERVAL );
		}
		
		if( getHandler() != null)
			getHandler().postDelayed( stopRingsDelay , sf_RING_STOP_DELAY_INTERVAL * 1000 ); 
	}

	
	protected void removeDelayRingStop(){
		getHandler().removeCallbacks( stopRingsDelay );
	}
	
	/**
	 * Stop Rings
	 */
	private Runnable stopRingsDelay = new Runnable(){

		@Override
		public void run() {
			final boolean isStackStarted = getState().isSipStackStarted();
			
	        if( isStackStarted ){
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "stopRingsDelay");
                }

		        if (mCommandProcessor != null) {
		            mCommandProcessor.execute(new StopRings());
		        }
	        } else {
	            if (LogSettings.MARKET) {
	                MktLog.i(TAG, "stopRingsDelay stack started(stack not started)");
	            }
	        }
		}
	};
	
	
	/**
	 * Stop/Start RING
	 */
	
	protected void stopRings(){
        mCommandProcessor.execute(new StopRings());
	}
	protected void startRings(){
        if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED) {
            Intent ringStartIntent = new Intent(RCMConstants.ACTION_VOIP_RING_STARTED);
            ringStartIntent.putExtra(RCMConstants.EXTRA_VOIP_RING_STARTED, SystemClock.elapsedRealtime() );
            getApplicationContext().sendBroadcast(ringStartIntent);
        }
        mCommandProcessor.execute(new StartRings());
	}
	
	/**
	 * StopRings
	 */
	private class StopRings extends Command {

		public StopRings( ) {
			super("StopRings");
		}

		@Override
		public void run() {
			ringStop("StopRingsCommand");
		}
	}
	
	/**
	 * StartRings
	 * 
	 */
	private class StartRings extends Command {

		public StartRings( ) {
			super("StartRings");
		}

		@Override
		public void run() {
			
			ringStart("StartRingsCommand");
		}
	}

	/*
	 * on_reg_state Implementation
	 */
	private class OnRegState extends Command{
		
		private int m_accountId = 0;
		
		public OnRegState(final int accountId) {
			super("OnRegState");
			m_accountId = accountId;
		}

		@Override
		public void run() {
		    /**
		     * [0] - status
		     * [1] - expires
		     */
			int[] acc_status = new int[2];
			int status = PJSIP.pjsip_acc_get_status(m_accountId, acc_status);
			if( status != PJSIP.sf_PJ_SUCCESS ){
			    // TODO: Clean-up calls
			    if (LogSettings.MARKET) {
			        MktLog.e(TAG, "SIP REG: pjsip_acc_get_status failed Error: " + status );
			    }
				return;
			}

            if (LogSettings.MARKET) {
                MktLog.w(TAG, "SIP REG: Account state changed to " + acc_status[0] + " from :" + getState().isSIPRegistrationState());
            }

//			final boolean regStateCurrent = isSIP_registration_state();
			
			/*
			 * Change state of SIP registration
			 */
			setSIP_registration_state( acc_status[0] == RCSIP.SipCode.PJSIP_SC_OK.code() );
			
            if (m_callback != null) {
                m_callback.account_changed_register_state(m_accountId, (acc_status[0] != RCSIP.SipCode.PJSIP_SC_OK.code() ? false
                        : true), acc_status[0]);
            }

			/*
			 * Check outbound calls
			 * 
			 * 1. If registration state was changed from FALSE to TRUE
			 */
            if (getState().isSIPRegistrationState()) {
                getState().setLatestSIPRegStatus(true, RCSIP.SipCode.PJSIP_SC_OK, acc_status[1]);
                if (getState().detailedState() == ServiceState.TO_ON_SIP_REGISTRATION) {
                    getState().set(ServiceState.ON);
                }
                // TODO: what about disabled
                
                RCCallInfo call = getCallMng().getOutboundOnInitiatingState(true);
                if (call != null) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG_EMG, "SIP REG: Outbound call was detected CallId:" + 
                                call.getRcCallId() + " Initiate and inject (HoldAll/StartRing/MakeCall)");
                    }
                    injectHoldAllRingCall(call);
                    getCallMng().ensureClientNotified(call, true, true);
                    updateActiveCallsCounter();
                } else {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG_EMG, "SIP REG: Outbound call has not been detected");
                    }
                    if(isOutboundCallsOnly()) {
                        CallManager.CallsState callsState = getCallMng().getCallsState();
                        if (callsState.active_with_pjsip_and_media == 0 &&
                                callsState.active_with_pjsip == 0 && callsState.active_wo_pjsip == 0) {
                            stackStop(RCCallInfo.CALL_ENDED_BY_SYSTEM, RCSIP.SipCode.PJSIP_SC_DISABLED_VOIP_REG_NOINC_EXTENDED);
                            if (LogSettings.MARKET) {
                                MktLog.e(TAG_EMG, "SIP REG: Outbound call has not been detected. Shutdown.");
                            }
                            return;
                        } else {
                            if (LogSettings.MARKET) {
                                MktLog.d(TAG_EMG, "SIP REG: Outbound only: There are active PJSIP calls stays here");
                            }
                        }
                    }
                }
            }

			if( acc_status[0] >= RCSIP.SipCode.PJSIP_SC_ERROR.code() ){
	            if (LogSettings.MARKET) {
	                MktLog.e(TAG_EMG, "SIP REGISTRATION FAILED; " + NetworkUtils.getNetworkStatusAsString(getInstance()));
	            }
	            getState().setLatestSIPRegStatus(false, RCSIP.SipCode.get(acc_status[0]), 0);
			    /*
			     * DK: If SIP registration is failed than stop it and restart HTTP registration
			     * 
			     * Note: Exclude moment when Account is not valid already. 
			     * Usually it means that SIP stack is restarting.   
			     */
			    final boolean isValid = getWrapper().PJSIP_pjsip_acc_is_valid( 0 );
				
			    if (LogSettings.MARKET) {
			        MktLog.e(TAG, "OnRegState SIP registration failed Error: " + acc_status[0] + " is Valid:" + isValid );
			    }
			    
			    CallManager.CallsState callsState = getCallMng().getCallsState();
			    
                if (callsState.active_with_pjsip_and_media > 0 ||
                        callsState.active_with_pjsip > 0) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG_EMG, "SIP REG: There are active PJSIP calls stays here");
                    }
                } else {
                    if (isOutboundCallsOnly()) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG_EMG, "SIP REG: Failed. No calls. Outboud only. Shutdown.");
                        }
                        stackStop(RCCallInfo.CALL_ENDED_BY_ERROR, RCSIP.SipCode.PJSIP_SC_SIP_REGISTRATION_FAILED_STATUS_CODE_EXTENDED);
                        return;
                    } else {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG_EMG, "SIP REG: Failed. No active PJSIP calls. Hangup All.");
                        }
                        getCallMng().call_hangup_all_forced(RCCallInfo.CALL_ENDED_BY_ERROR,
                                RCSIP.SipCode.PJSIP_SC_SIP_REGISTRATION_FAILED_STATUS_CODE_EXTENDED,
                                RCSIP.SipCode.get(acc_status[0]));
                    }
                }
			    
                if (LogSettings.MARKET) {
                    MktLog.e(TAG_EMG, "SIP REG: DelAcc/RingStop/HTTPReg(null/delay)");
                }
			    RingtoneStop();
			    setHttpRegInfo( null );
			    if( isValid ){
			        new DeleteAccount().run();
			    }
			    getState().set(ServiceState.TO_ON_HTTP_REGISTRATION);
				startDelayHttpRegistration();			    
			}
		}
	}
	
	
	/**
	 * CheckStackStop
	 * 
	 * This function is detect should we stop SIP stack on case when 
	 * is Outbound only mode.
	 * 
	 * It checked if no more active calls and then Stop SIP stack 
	 * 
	 */
	
	protected void checkForStop(){
        mCommandProcessor.execute(new CheckStackStop());
	}
	
	private class CheckStackStop extends Command {

		public CheckStackStop( ) {
			super("CheckStackStop");
		}

		@Override
		public void run() {
			
			if( isOutboundCallsOnly() ){
				final int callsCounter = getCallMng().call_get_count();
				if (LogSettings.MARKET) {
				    MktLog.w(TAG, "CheckStackStop Active calls count:" + callsCounter );
				}
				
				if( callsCounter == 0 ){
					stackStop(RCCallInfo.CALL_ENDED_BY_SYSTEM, RCSIP.SipCode.PJSIP_SC_OK);
				}
			}
		}
	}	
	
	
    private volatile boolean endCallBeepInjected = false;

	protected void endCallBeep(){
        if (endCallBeepInjected) {
            return;
        }
        endCallBeepInjected = true;
		mCommandProcessor.execute( new Command("EndCallBeep"){

			@Override
			public void run() {
                if (getCallMng().call_get_count() == 0) {
	                if (LogSettings.MARKET) {
	                    MktLog.i( TAG, "ChangeAudioRoute Start TONE");
	                }
	                
	                
	            	ToneGenerator tone = new ToneGenerator( AudioManager.STREAM_VOICE_CALL, 50 );
	            	tone.startTone(ToneGenerator.TONE_CDMA_ONE_MIN_BEEP );
	            	
	                if (LogSettings.MARKET) {
	                    MktLog.i( TAG, "ChangeAudioRoute Stop TONE");
	                }
	                
	                if( getState().isRestartStack()){
		                if (LogSettings.MARKET) {
		                    MktLog.w( TAG, "endCllBp: Restart SIP stack");
		                }
		                getState().setRestartStack(false);
		                restartStack();
	                }
	            }
                endCallBeepInjected = false;
			}
		});
	}
	
    public void ensureAudioRouteSet(boolean async) {
        if (async) {
            if(null != mCommandProcessor) {
            	mCommandProcessor.execute(new EnsureAudioRouteSet());
            }
        } else {
            if (LogSettings.MARKET) {
                MktLog.w( TAG, "ensureAudioRouteSet: Sync call");
            }
            new EnsureAudioRouteSet().run();
        }
    }
    
    private class EnsureAudioRouteSet extends Command {
        public EnsureAudioRouteSet() {
            super("EnsureAudioRouteSet");
        }

        @Override
        public void run() {

            long[] calls = getCallMng().call_get_active_calls();
            int callsCount = 0;
            boolean start = false;
            boolean stop = false;
            
            if (calls != null) {
                callsCount = calls.length;
                if (callsCount == 1) {                	
                    RCCallInfo callInfo = getCallMng().getCallByRCCallId(calls[0]);
                    if (callInfo != null && callInfo.isAlive() && !callInfo.isEnding()) {
                    	if(callInfo.isIncoming()) {
                            start = (callInfo.getState() == RCSIP.PJSUA_CALL_STATE_CONFIRMED);
                        } else {
                        	start = true;
                        }
                    }
                } else if (callsCount > 1) {
                    start = true;
                } else {
                    stop = true;
                }
            } else {
                stop = true;
            }
            
            if (start) {
                mMediaManager.startAudio(RCMediaManager.MODE_IN_CALL);
            }

            if (stop) {
                mMediaManager.stopAudio();
                /*
                 * If we do not have any active calls restore RX level & Set
                 * External Speaker to Off
                 */
                getWrapper().PJSIP_conf_adjust_rx_level(0, 1);
            }
        }
    };
    
    /*
     * 
     */
    protected boolean isOutboundCallsOnly(){
    	return getSipParameters().isOutboundCallsOnly();
    }
    
    
    
    private void muteOff(){
        confAdjustRxLevel(0, 1);        
    }

    
    private int confAdjustRxLevel(int slot, float level) {
        int ret = getWrapper().PJSIP_conf_adjust_rx_level(slot, level);
        return ret;
    }


    void callMakeCallComplete(long rcCallId, boolean bySIPClient) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "callMakeCallComplete(): rcCallId: " + rcCallId + " bySIPClient:" + bySIPClient);
        }

        int state = getState().state();
        boolean startStack = false;
        synchronized (getCallMng()) {
            RCCallInfo rcCallInfo = getCallMng().getCallByRCCallId(rcCallId);
            if (rcCallInfo == null) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "callMakeCallComplete(): RC CallId: " + rcCallId + " has not been found.");
                }
                return;
            } else if (!rcCallInfo.isMakeCallDelayed()
                    || !(rcCallInfo.isAlive() && !rcCallInfo.isIncoming() && !rcCallInfo.isEnding() && (rcCallInfo.getState() == RCSIP.PJSUA_CALL_STATE_CREATED))) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "callMakeCallComplete(): Ignored:  " + rcCallInfo.toString());
                }
                return;
            }
            rcCallInfo.setMakeCallDelayed(false);
            if (state == ServiceState.ON) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "callMakeCallComplete(): RC CallId: " + rcCallId + " Initiate and inject (HoldAll/StartRing/MakeCall)");
                }
                injectHoldAllRingCall(rcCallInfo);
            } else {
                if (state != ServiceState.TO_ON) {
                    startStack = true;
                }
            }
            
        }
        if (startStack) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "callMakeCallComplete(): RC CallId: " + rcCallId + " StartStack");
            }
            getState().set(ServiceState.TO_ON);
            prepareSip();
        }
        updateActiveCallsCounter();
    }

    private void injectHoldAllRingCall(RCCallInfo call) {
        call.onInitiatedState();
        mCommandProcessor.execute(new HoldAll());
        startRings();
        mCommandProcessor.execute(new MakeCallCommand(call.getRcCallId()));
    }
    
    protected void callAnswer(long rcCallId) {
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "callAnswer(): rcCallId = " + rcCallId);
        }
        
        if( !getState().isSipStackCreated() ){
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "SIP stack is not created. Cancel operation.");
            }
            return;
        }

        if (checkDelayedCallAnswered(rcCallId) == true) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "callAnswer(): already answered; return.");
            }
            return;
        }
        
        doCallAnswer(rcCallId);
    }
 
    
    protected void callAnswerAndHold(long rcCallId) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "callAnswerAndHold(): rcCallId = " + rcCallId);
        }

        if( !getState().isSipStackCreated() ){
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "callAnswerAndHold(): SIP stack is not created. Cancel operation.");
            }
            return;
        }
        
        if (checkDelayedCallAnswered(rcCallId) == true) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "callAnswerAndHold(): already answered; return.");
            }
            return;
        }

        
        long[] calls = getCallMng().call_get_active_calls();
        if (calls == null || calls.length == 0) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "callAnswerAndHold(): No active calls" );
            }
            return;
        }    
        
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "callAnswerAndHold(): " + calls.length + " calls");
        }
        
        for (int i = 0; i < calls.length; i++) {
            if (calls[i] != rcCallId) {
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "callAnswerAndHold(): Hold CallId " + calls[i]);
                }
                m_callManager.call_hold(calls[i]);
            }
        }
        
        //update calls list in UI 
        getPJSIPCallbackListener().on_call_info_changed(PJSIP.sf_INVALID_RC_CALL_ID);

        doCallAnswer(rcCallId);
    }
        
    protected void callAnswerAndHangup(long rcCallId) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "callAnswerAndHangup(): Answer rcCallId = " + rcCallId);
        }

        if( !getState().isSipStackCreated() ){
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "callAnswerAndHangup(): SIP stack is not created. Cancel operation.");
            }
            return;
        }
        
        if (checkDelayedCallAnswered(rcCallId) == true) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "callAnswerAndHangup(): already answered; return.");
            }
            return;
        }

        
        long[] calls = getCallMng().call_get_active_calls();
        if (calls == null || calls.length == 0) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "callAnswerAndHangup(): No active calls" );
            }
            return;
        }    
        
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "callAnswerAndHangup(): " + calls.length + " calls");
        }
        
        for (int i = 0; i < calls.length; i++) {
            if (calls[i] != rcCallId) {
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "callAnswerAndHangup(): Hangup RC CallId " + calls[i]);
                }
                m_callManager.call_hangup(calls[i], RCCallInfo.CALL_ENDED_BY_USER, 0, null);
            }
        }
        
        doCallAnswer(rcCallId);
    }
        
    
    private boolean checkDelayedCallAnswered(long rcCallId) {
        RCCallInfo rcCallInfo = getCallMng().getCallByRCCallId(rcCallId);
        if (rcCallInfo == null || rcCallInfo.isCallAnswered()) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "checkDelayedCallAnswered(): already answered; return true");
            }
            return true;
        } else {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "checkDelayedCallAnswered(): not answered yet; set as answered, return false");
            }

            rcCallInfo.setCallAnswered(true);
            return false;
        }
    }
    
    private void doCallAnswer(long rcCallId) {
        RingtoneStop();
        muteOff();
        getCallMng().call_answer(rcCallId, 200, null);
        ensureAudioRouteSet(true);
    }
    
    
    /**
     * Hungup call and add custom headers
     * 
     * @param reason
     *            {@link #CALL_ENDED_BY_USER}, {@link #CALL_ENDED_BY_PARTY},
     *            {@link #CALL_ENDED_BY_SYSTEM}, {@link #CALL_ENDED_BY_ERROR}
     */
    protected void doCallHangupAsync( long rcCallId, int reason, final int code, final String[] headers ){
		if (LogSettings.MARKET) {
	        MktLog.i( TAG, "doCallHangupAsync RC callId: " + rcCallId + " code:" + code + " reason: " + RCCallInfo.getEndingReasonAsString(reason));
	    }
		
		if( mCommandProcessor != null ) {
			mCommandProcessor.execute( new CallHangupCommand( rcCallId, reason, code, headers));
		}
    }
    
    /**
     * Send answer
     */
    private class CallHangupCommand extends Command{
    	
    	private long m_RCCallId; 
    	private int m_code;
    	private String[] m_headers;
    	private int m_reason;
    	
		public CallHangupCommand( final long rcCallId, final int reason, final int code, final String[] headers ) {
			super("CallHangupCommand");
			m_RCCallId = rcCallId;
			m_code = code;
			m_reason = reason; 
			m_headers = ( headers == null ? headers : headers.clone());
		}

		@Override
		public void run() {
		    if (LogSettings.MARKET) {
		        MktLog.i(TAG, "CallHangupCommand RC CallId: " + m_RCCallId + "  code:" + m_code );
		    }
            getCallMng().call_hangup(m_RCCallId, m_reason, m_code, m_headers );
		} 
    };
    
    /**
     * Send RC Call Control SIP message
     */
    protected void sendSipMessage( final String To, XmlSerializableObjectAbstract msg ){
    	
		final String sContent = msg.toString();
		if( sContent != null ){

			if (LogSettings.MARKET) {
		        MktLog.i( TAG, "Send SIP message: " + sContent );
		    }
			
			if( mCommandProcessor != null )
				mCommandProcessor.execute( new SendSipMessage( To, sContent ));
		}
    }
    
    private class SendSipMessage extends Command {
    	private String m_To;
    	private String m_content;
		public SendSipMessage( String To, String content) {
			super("SendSipMessage");
			m_To = To;
			m_content = content;
		}
		@Override
		public void run() {
		    if (LogSettings.MARKET) {
		        MktLog.i(TAG, "SendSipMessage [" + 0 + "] To [" + m_To + "]" );
		    }
            getWrapper().PJSIP_message_send( 0, m_To, m_content);
		}
    };

    
    /**
     * Start Incoming Call Activity
     */
    protected void startIncomingCallActivity( final CallInfo callInfo, final int activeCalls ){
    	
		if (LogSettings.MARKET) {
	        MktLog.i( TAG, "Start Incoming Call Activity" );
	    }

		mCommandProcessor.execute( new Command("StartIncomingCallActivity"){
			@Override
			public void run() {
				Intent i_calls = new Intent( getBaseContext(), VoipInCall.class);
				/*
				 * DK: FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS added as temporary solution
				 * I have not find the solution for situation when VoipInCall activity
				 * is run when RC Application is started from recent list.
				 */
				setIncomingCallActivityFlags( i_calls );
				
				int calls = activeCalls;
				if( activeCalls == 0 ){
					calls = getCallMng().call_get_count();
				}
				
		        if (LogSettings.MARKET) {
		        	MktLog.w(TAG, "Start Incoming Call Activity call Id: " + callInfo.getRcCallId() + " calls: " + calls );
		        }
				
				i_calls.putExtra(RCMConstants.EXTRA_VOIP_INCOMING_CALL_INFO, callInfo );
				i_calls.putExtra(RCMConstants.EXTRA_VOIP_INCOMING_CALL_INFO_COUNT, calls );
				
				
				boolean screenOn = true;
				
				if(Compatibility.getApiLevel() >= 7) {
					screenOn = mWakeLock.isScreenOn();
				} else {
					screenOn = mWiFiWatcher.isScreenOn();
				}				

				if(screenOn){
					
			        if (LogSettings.MARKET) {
			        	MktLog.i(TAG, "Screen is unlocked Start activity." );
			        }
			        
					getApplication().startActivity( i_calls );
				}
				else{
					
			        if (LogSettings.MARKET) {
			        	MktLog.i(TAG, "Screen is locked Start activity when screen wiil be unlocked." );
			        }
					
		        	setIncomingCallIntent( i_calls );
				}
			
			}
		});
    }
    
    void setIncomingCallActivityFlags( Intent intent ){
    	
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | 
				Intent.FLAG_FROM_BACKGROUND  |
				Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS );
    }
    
    /**
     * Change IP address is used on PJSIP
     */
    private class ChangeIPAddress extends Command{

		public ChangeIPAddress() {
			super("ChangeSipIp");
		}

		@Override
		public void run() {
		    if (LogSettings.MARKET) {
		    	MktLog.w(TAG, "ChangeIPAddress is starting" );
		    }
//		    int status;
			if( m_pjsip_wrapper != null ){
                if (!HANG_UP_ON_NETWORK_CHANGE) {
					if( getCallMng().call_get_count() > 0 ){
						
//						status = m_pjsip_wrapper.PJSIP_pjsip_acc_set_registration( 0, false );
//						
//						status = m_pjsip_wrapper.PJSIP_pjsip_media_transports_update( 0 );
//						if( status != PJSIP.sf_PJ_SUCCESS ){
//						    if (LogSettings.ENGINEERING) {
//						        MktLog.w(TAG, "ChangeIPAddress PJSIP_pjsip_media_transports_update failed " + status );
//						    }
//						}
//						
//					    if (LogSettings.ENGINEERING) {
//					        EngLog.w(TAG, "ChangeIPAddress is starting PJSIP_pjsip_call_reinvate_all() starting" );
//					    }
//					    
//						status = m_pjsip_wrapper.PJSIP_pjsip_call_reinvate_all();
//						if( status != PJSIP.sf_PJ_SUCCESS ){
//						    if (LogSettings.ENGINEERING) {
//						        MktLog.w(TAG, "ChangeIPAddress PJSIP_pjsip_media_transports_update failed " + status );
//						    }
//						}
						
			            if (LogSettings.MARKET) {
			                MktLog.e(TAG_EMG, "Change IP Address:HangUpAll");
			            }
						
						/*
						 * Restart stack when all call will be finished 
						 */
		                getCallMng().call_hangup_all_forced(RCCallInfo.CALL_ENDED_BY_SYSTEM, 
		                        RCSIP.SipCode.PJSIP_SC_IP_ADDRESS_CHANGED_STATUS_CODE_EXTENDED,
		                        RCSIP.SipCode.PJSIP_SC_SERVICE_UNAVAILABLE);
					} else {
					    if (LogSettings.MARKET) {
					        MktLog.w(TAG, "ChangeIPAddress: No active calls: restartStack" );
					    }
					    restartStack();
					}
                } else {
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "ChangeIPAddress: No active calls: restartStack");
			}		    
                    restartStack();
		    }
		}
        }
    };
    

	/*
	 * 
	 */
    public void http_registration_state(HttpRegister.HttpRegistrationStatus status) {
        if (m_callback != null) {
            m_callback.http_registration_state(status.code());
        }
    }	
	
    /**
     * HTTP Reg delayed algorithm testing.
     */
    private static final boolean HTTP_REG_DELAY_ALG_TEST = false;
    private static int HTTP_REG_DELAY_ALG_TEST_ITER = 0;
    private static int HTTP_REG_DELAY_ALG_TEST_FINAL = 8;
    
    private static final long HTTP_REG_NET_REQUEST_TIMEOUT = 15000; 
        
    public class HttpRegistrationAsyncTask extends TimedProxyTask {
        private HttpRegisterRequest request = null;
        private HttpRegister httpReg = null;
        public HttpRegistrationAsyncTask(HttpRegister httpReg, HttpRegisterRequest request) {
            super("HttpRegistrationAsyncTask");
            this.httpReg = httpReg;
            this.request = request;
        }
        @Override
        public void doTask() {
            httpReg.register(request);
        }
    }
    
    /**
     * HttpRegistration
     */
	private class HttpRegistration extends Command implements IHttpRegisterCallback {
	    private boolean valid = true;
	    public HttpRegistration( ) {
	        super("RC HTTP registration");
	    }
	    
	    @Override
        public void run() {
            CPUlock();
            HttpRegisterRequest request = null;
            HttpRegister httpReg = new HttpRegister(this, true);
            boolean error = false;
            try {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG_EMG, "HttpRegistration: Starting Instance ID:" + getUserInfo().getInstanceId());
                }

                removeDelayHttpRegistration();
                setHttpRegInfo(null);
                request = HttpRegister.makeRequest(HttpRegister.AgentRole.agent_Android);

                request.getTag_2_Bdy().setExt(getUserInfo().getExtension());
                request.getTag_2_Bdy().setPn(getUserInfo().getPin());
                request.getTag_2_Bdy().setSP(getUserInfo().getPassword());
                request.getTag_2_Bdy().setInst(getUserInfo().getInstanceId());
                request.getTag_2_Bdy().setCln(getUserInfo().getInstanceId());
            } catch (java.lang.Throwable th1) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG_EMG, "HttpRegistration:run:error1:", th1);
                }
                error = true;
                valid = false;
            }
            
            if (!error) {
                try {
                    if (request == null || !request.isValid()) {
                        error = true;
                        valid = false;
                    }
                } catch (java.lang.Throwable th2) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG_EMG, "HttpRegistration:run:error2:", th2);
                    }
                    error = true;
                    valid = false;
                }
            }
             
            if (!error) {
                HttpRegistrationAsyncTask task = new HttpRegistrationAsyncTask(httpReg, request);
                if (LogSettings.MARKET) {
                    MktLog.e(TAG_EMG, "HttpRegistration: HttpRegistrationAsyncTask...");
                }
                int state = task.execute(HTTP_REG_NET_REQUEST_TIMEOUT, false);
                if (state == TimedProxyTask.COMPLETED) {
                    if (LogSettings.MARKET) {
                        MktLog.i(TAG_EMG, "HttpRegistrationAsyncTask:completed");
                    }
                } else if (state == TimedProxyTask.TIMEOUT) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG_EMG, "HttpRegistrationAsyncTask:TIMEOUT");
                    }
                    valid = false;
                    error = true;
                } else {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG_EMG, "HttpRegistrationAsyncTask:FAILED");
                    }
                    valid = false;
                    error = true;
                }
            }
            
            if (error) {
                getState().setHttpRegistrationOnProgress(false);
                onHTTPregFail(HttpRegister.HttpRegistrationStatus.HTTP_REG_INVALID_CREDENTIAL,
                        RCSIP.SipCode.PJSIP_SC_HTTP_REG_FAILED_CREDS_STATUS_CODE_EXTENDED);
            }
            
            CPUunlock();
	    }

		@Override
        public void onFailed(int httpError) {
		    if (!valid) {
		        if (LogSettings.MARKET) {
	                MktLog.e(TAG_EMG, "> HttpRegistration: onFailed. But it's not valid: error:" + httpError);
	            }
		        return;
		    }
		    if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "> HttpRegistration: onFailed: error:" + httpError);
            }
            getState().setHttpRegistrationOnProgress(false);
            onHTTPregFail(HttpRegister.HttpRegistrationStatus.HTTP_REG_FAILED, RCSIP.SipCode.PJSIP_SC_HTTP_REG_FAILED_STATUS_CODE_EXTENDED);
        }

        @Override
        public void onFinished(HttpRegisterResponse response) {
            if (!valid) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG_EMG, "> HttpRegistration: onFinished. But it was marked as invalid.");
                }
                return;
            } else {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG_EMG, "> HttpRegistration: onFinished.");
                }
            }
            getState().setHttpRegistrationOnProgress(false);

            if (!response.isValid()) {
                onHTTPregFail(HttpRegister.HttpRegistrationStatus.HTTP_REG_FAILED, RCSIP.SipCode.PJSIP_SC_HTTP_REG_FAILED_STATUS_CODE_EXTENDED);
                return;
            }
            
            if (HTTP_REG_DELAY_ALG_TEST) {
                if (HTTP_REG_DELAY_ALG_TEST_ITER <= HTTP_REG_DELAY_ALG_TEST_FINAL) {
                    switch (HTTP_REG_DELAY_ALG_TEST_ITER){
                    case 0:
                        getState().setLatestSIPRegStatus(false, RCSIP.SipCode.PJSIP_SC_BAD_GATEWAY, 0);
                        break;
                    case 1:
                        getState().setLatestSIPRegStatus(false, RCSIP.SipCode.PJSIP_AC_AMBIGUOUS, 0);
                        break;
                    case 2:
                        getState().setLatestSIPRegStatus(false, RCSIP.SipCode.PJSIP_SC_PAYMENT_REQUIRED, 0);
                        break;
                    case 3:
                        getState().setLatestSIPRegStatus(false, RCSIP.SipCode.PJSIP_SC_BAD_REQUEST, 0);
                        break;
                    case 4:
                        getState().setLatestSIPRegStatus(false, RCSIP.SipCode.PJSIP_SC_SESSION_TIMER_TOO_SMALL, 0);
                        break;
                    case 5:
                        getState().setLatestSIPRegStatus(false, RCSIP.SipCode.PJSIP_SC_ERROR, 0);
                        break;
                    case 6:
                        getState().setLatestSIPRegStatus(false, RCSIP.SipCode.PJSIP_SC_INTERNAL_SERVER_ERROR, 0);
                        break;
                    case 7:
                        getState().setLatestSIPRegStatus(false, RCSIP.SipCode.PJSIP_SC_SERVICE_UNAVAILABLE, 0);
                        break;
                    }
                    HTTP_REG_DELAY_ALG_TEST_ITER++;
                    onHTTPregFail(HttpRegister.HttpRegistrationStatus.HTTP_REG_FAILED,
                            RCSIP.SipCode.PJSIP_SC_HTTP_REG_FAILED_TEST_CODE_EXTENDED);
                    return;
                }
            }

            setHttpRegInfo(response);

            String instanceId = getUserInfo().getInstanceId();
            String instanceIdNew = getHttpRegInfo().getBody().getInstanceId();

            try {
                String prev = getUserInfo().getInstanceId();
                if (!prev.equals(instanceId)) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG_EMG, "HttpRegistration:Prev.InstanceID=" + prev + " New.InstanceID=" + instanceId);
                    }
                }
            } catch (java.lang.Throwable th) {
            }

            /*
             * Check and update Instance ID
             */
            if (instanceId == null || instanceId.length() == 0 || instanceId != instanceIdNew) {
                getUserInfo().setInstanceId(instanceIdNew);
            }

            final long sipFlags = response.getBody().getSipFlgs();

            if (LogSettings.MARKET) {
                MktLog.i(TAG_EMG, "HTTP-REG:SIP Flags: 0x" + Long.toHexString(sipFlags));
            }

            /*
             * If VoIP on service was disabled save this flag and stop SIP stack
             */
            final boolean isVoipOnServiceEnabled = ((sipFlags & RCMConstants.SIPFLAGS_VOIP_ENABLED) != 0);
            if (!isVoipOnServiceEnabled) {

                getSipParameters().setHttpRegSipFlags(sipFlags);
                getSipParameters().setVoipOnServiceEnabled(false);

                if (LogSettings.MARKET) {
                    MktLog.e(TAG_EMG, "HTTPRegister. VoIP disabled on service. Stop SIP stack. ");
                }
                stackStop(RCCallInfo.CALL_ENDED_BY_SYSTEM, RCSIP.SipCode.PJSIP_SC_DISABLED_VOIP_HTTPREG_EXTENDED);
                return;
            }
            getSipParameters().setServiceProvider(getHttpRegInfo().getBody().getSipService());
            
            String proxy = getHttpRegInfo().getBody().getSipOutboundProxy();
            getSipParameters().setSipOutboundProxy(proxy);
            if (LogSettings.MARKET) {
                if (proxy == null) {
                    MktLog.e(TAG_EMG, "< !!!!!!!!!!!!!!!    ***********************   !!!!!!!!!!!!!!!!!!!!!");
                    MktLog.e(TAG_EMG, "< HttpRegistration: Successfull PROXY IS NULL ");
                    MktLog.e(TAG_EMG, "< !!!!!!!!!!!!!!!    ***********************   !!!!!!!!!!!!!!!!!!!!!");
                } else {
                    MktLog.e(TAG_EMG, "< HttpRegistration: Successfull (delete/add account) proxy: " + proxy);
                }
            }
            // deleteAccount(); addAccount will delete previous
            http_registration_state(HttpRegister.HttpRegistrationStatus.HTTP_REG_OK);
            getState().setLatestHTTPRegStatus(true, HttpRegister.HttpRegistrationStatus.HTTP_REG_OK);
            setSIP_registration_state(true);
            getState().set(ServiceState.TO_ON_SIP_REGISTRATION);
            
            if (!addAccount()) {
                onHTTPregFail(HttpRegister.HttpRegistrationStatus.HTTP_REG_FAILED_ADDING_ACCOUNT,
                        RCSIP.SipCode.PJSIP_SC_HTTP_REG_FAILED_ACCOUNT_STATUS_CODE_EXTENDED);
            }
        }
	}

    private void onHTTPregFail(HttpRegister.HttpRegistrationStatus http_reg_status, RCSIP.SipCode error) {
        if (LogSettings.MARKET) {
            MktLog.e(TAG_EMG, "HTTP Reg Failed:code:" + error.msg() + "; " + NetworkUtils.getNetworkStatusAsString(this));
        }
        http_registration_state(http_reg_status);
        setHttpRegInfo( null );
        setSIP_registration_state(false);
        getState().setLatestHTTPRegStatus(false, http_reg_status);
        CallManager.CallsState callsState = getCallMng().getCallsState();
        if (callsState.active_with_pjsip_and_media > 0 || callsState.active_with_pjsip > 0) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "HTTP Reg Failed: There are active PJSIP calls stays here");
            }
        } else {
            if (isOutboundCallsOnly()) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG_EMG, "HTTP Reg Failed: No calls. Outboud only. Shutdown.");
                }
                stackStop(RCCallInfo.CALL_ENDED_BY_ERROR, error);
                return;
            } else {
                if (getCallMng().call_get_count() > 0) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG_EMG, "HTTP Reg Failed: No calls. Hangup All.");
                    }
                    if (mCommandProcessor != null) {
                        mCommandProcessor.execute(new HangupAll(RCCallInfo.CALL_ENDED_BY_ERROR, error));
                    }
                }
            }
        }
        if (isValidCurrentNetworkForCalls() && getSipParameters().isVoipEnabled()) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "HTTP Reg Failed: Failed. Delete account and delay HTTP Reg.");
            }
            deleteAccount();
            startDelayHttpRegistration();
        } else {
            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "HTTP Reg Failed: Failed. Stop stack.");
            }
            stackStop(RCCallInfo.CALL_ENDED_BY_ERROR, error);
        }
    }
	
//	/**
//	 * SIP Logout. 
//	 * 1. Clear HTTP registration information
//	 * 2. Delete current active SIP account
//	 * 
//	 */
//	private class LogoutSIP extends Command {
//
//		public LogoutSIP() {
//			super("Logout SIP");
//		}
//
//		@Override
//		public void run() {
//			
//            if (LogSettings.MARKET) {
//                MktLog.e(TAG_EMG, "Logout Started");
//            }
//			
//	        if (LogSettings.ENGINEERING) {
//	            EngLog.i(TAG, "LogoutSIP Started...." );
//	        }
//	        
//            cpuLock();
//			setHttpRegInfo( null );
//
//			if (LogSettings.ENGINEERING) {
//	            EngLog.i(TAG, "LogoutSIP DeleteAccount" );
//	        }
//			deleteAccount();
//			
//            /*
//             * Hangup all active calls
//             */
//	        if (LogSettings.ENGINEERING) {
//	            EngLog.i(TAG, "LogoutSIP Hangup all calls" );
//	        }
//	        
//	        // TODO : wait PJSIP callbacks about DISCONNECTIONs (for calls that have PJSIP calls)
//	        
//          	getCallMng().call_hangup_all_forced(RCCallInfo.CALL_ENDED_BY_SYSTEM, 
//            	        RCSIP.SipCode.PJSIP_SC_LOGOUT_STATUS_CODE_EXTENDED,
//            	        RCSIP.SipCode.PJSIP_SC_LOGOUT_STATUS_CODE_EXTENDED);
//          	
//          	
//            cpuUnLock();
//		}
//	};
	
	/**
	 * Delete SIP account 
	 */
	private class DeleteAccount extends Command {
		public DeleteAccount() {
	        super("RC Delete Account");
		}
		@Override
		public void run() {
			deleteAccount();
		}
	};
	
    /**
     * Add SIP Account
     */
    private boolean addAccount() {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "AddAccount: Starting");
        }

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "AddAccount: Delete current current account");
        }
        deleteAccount();

        final AccountInfo accInfo = getAccountInfo();
        if (accInfo == null) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "getAccountInfo() returned NULL.");
            }
            return false;
        }
        
        long forcedExpires = NetworkUtils.getForcedExpirationIfRecuired(this);
        
        if (forcedExpires > 0) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "AddAccount: ForcedExpires(sec): " + forcedExpires);
            }
        } else {
            forcedExpires = 0;
        }
        
        
        if (PJSIP.sf_PJ_SUCCESS != m_pjsip_wrapper.PJSIP_acc_add(accInfo, forcedExpires)) {
            return false;
        }
        return true;
    }

	
	/**
	 * Hangup all calls
	 */
    private class HangupAll extends Command {
    	private int hangupReason;
    	private SipCode error;
    	public HangupAll(int reason, SipCode error){
    		super("PJSIP_hangup_all");
    		hangupReason = reason;
    		this.error = error;
    	}
    	
        @Override
        public void run() {
            getCallMng().call_hangup_all(hangupReason, error);
        }
    };

	/**
	 * Set HOLD state for all active calls
	 */
    private class HoldAll extends Command {
    	
    	public HoldAll(){
    		super("PJSIP_hold_all");
    	}
    	
        @Override
        public void run() {
        	
	        if (LogSettings.ENGINEERING) {
	        	EngLog.i(TAG, "HoldAll Starting..." );
	        }
            /*
             * Set HOLD state for all active calls
             */
            if( getState().isSipStackStarted() ){
            	// TODO: Move to CallManager
                final long[] calls = getCallMng().call_get_active_calls();
                if( calls != null ){
                	
        	        if (LogSettings.ENGINEERING) {
        	        	EngLog.i(TAG, "HoldAll Detected active calls: " + calls.length );
        	        }
                	
                	CallInfo callInfo = new CallInfo();
                	for( int i = 0; i < calls.length; i++ ){
                        if(LogSettings.MARKET) {
                            MktLog.w(TAG, "call_get_info HoldAll");
                        }
                		
                		int result = getCallMng().call_get_info( calls[i], callInfo );
                		if( result == PJSIP.sf_PJ_SUCCESS ){
                			
                			if( callInfo.isValid() && !callInfo.isHold()){
                				
                    	        if (LogSettings.MARKET) {
                    	        	MktLog.i(TAG, "HoldAll Set HOLD state for call: " + calls[i] );
                    	        }
                    	        
                    	        getCallMng().call_hold(calls[i]);
                    	        getPJSIPCallbackListener().on_call_info_changed(calls[i]);
                			}
                		}
                	}
                	
        	    	updateActiveCallsCounter();
                }
            }
        }
    };

    
    /**
     * Invalidate Incoming Call Notification
     */
    protected void invalidateIncomingCallNotification( final long rcCallId ){

	    final long incomingCallNtfId = getIncomingNotificationId();
		if( PJSIP.sf_INVALID_RC_CALL_ID != incomingCallNtfId ){
			
		    if (LogSettings.MARKET) {
		        MktLog.i(TAG, "invalidateIncomingCallNotification callId " + rcCallId + " NtfId " + incomingCallNtfId );
		    }
			
			if( incomingCallNtfId == rcCallId ){
			    if (LogSettings.MARKET) {
			        MktLog.i(TAG, "invalidateIncomingCallNotification callId Delete notification for " + rcCallId + " NtfId " + incomingCallNtfId );
			    }

				ServicePJSIP.getInstance().deleteIncomingCallNotification(rcCallId);
			}
			
		    if (LogSettings.MARKET) {
		        MktLog.i(TAG, "invalidateIncomingCallNotification callId " + rcCallId + " checkIncomingCalls " );
		    }
			ServicePJSIP.getInstance().checkIncomingCalls();
		}
    }
    
    /**
     * Check Incoming call notification
     */
    protected void checkIncomingCalls(){
		if( mCommandProcessor != null ){
			mCommandProcessor.execute( new CheckIncomingCalls());
		}
    }
	
	/**
	 * Delete Incoming call notification
	 */
	protected void deleteIncomingCallNotification(long rcCallId){

        if (LogSettings.MARKET) {
            MktLog.w(TAG, "deleteIncomingCallNotification id:" + rcCallId );
        }
        ringtoneOff();
        
		if (rcCallId == PJSIP.sf_INVALID_RC_CALL_ID || rcCallId == getIncomingNotificationId()) {
			setIncomingNotificationId( PJSIP.sf_INVALID_CALL_ID );
		}
		getApplicationContext().sendBroadcast(new Intent(RCMConstants.ACTION_VOIP_STOP_NOTIFY_INCOMING_CALL));
	}
    
    /**
     * Check if we have calls on CALLING state and 
     * create VoipInCall activity for it.
     */
    private class CheckIncomingCalls extends Command {
    	
    	public CheckIncomingCalls(){
    		super("CheckIncomingCalls");
    	}

		@Override
		public void run() {
	        if( getState().isSipStackStarted() ){
                final long[] calls = getCallMng().call_get_active_calls();
                if( calls != null ) {
                    final int activeCallNumber = calls.length;
                	CallInfo callInfo = new CallInfo();
                	for( int i = 0; i < calls.length; i++ ){
                        if(LogSettings.MARKET) {
                            MktLog.w(TAG, "call_get_info CheckIncomingCalls");
                        }
                		int result = getCallMng().call_get_info( calls[i], callInfo );
                		if( result == PJSIP.sf_PJ_SUCCESS ){
                			
                			if( callInfo.isValid() && callInfo.getCall_state() == RCSIP.PJSUA_CALL_STATE_INCOMING){
                    	        if (LogSettings.MARKET) {
                    	        	MktLog.i(TAG, "Add Notification for Call ID: " + calls[i] );
                    	        }
                    	        
                    	        /* Add notification on notification bar*/
                    			ServicePJSIP.getInstance().setIncomingCallNotification(callInfo, activeCallNumber );
                    			/* Start Incoming Call Activity */
                    			ServicePJSIP.getInstance().startIncomingCallActivity( callInfo, activeCallNumber );
                    			break;
                			}
                		}
                	}
                	
                } else {
                    if(LogSettings.MARKET) {
                        MktLog.i(TAG, "CheckIncomingCalls. No Active calls (null returned)");
                    }
                    long notification = ServicePJSIP.getInstance().getIncomingNotificationId();
                    if (notification != PJSIP.sf_INVALID_RC_CALL_ID) {
                        deleteIncomingCallNotification(notification);
                    }
                }
            }
		}
    };   
    
    protected void bindContactCommand(long rcCallId) {
        new BindContactCommand(rcCallId).start();
    }
    
    
    /**
     * Bind contact info to RC Call
     */
    private class BindContactCommand extends Thread {
        private long rc_call_id;

        public BindContactCommand(long rcCallId) {
            super("BindContactCmd");
            rc_call_id = rcCallId;
        }

        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_LOWEST);
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "BindContactCmd Starting...");
            }

            try {
                String name = null;
                Long photoId = CallInfo.ID_NO_PHOTO;

                final RCCallInfo rcCallInfo = getCallMng().getCallByRCCallId(rc_call_id);

                if (rcCallInfo != null && rcCallInfo.isAlive() && !rcCallInfo.isEnding()) {
                    final String number = rcCallInfo.getPartyNumber();
                    if (!TextUtils.isEmpty(number)) {
                        final ContactBinding cb = Cont.acts.bindContactByNumber(getApplicationContext(), RCMProviderHelper
                                .getCurrentMailboxId(getApplicationContext()), number);

                        if ((cb != null) && cb.isValid && cb.hasContact) {
                            name = cb.displayName;
                            if (cb.isPersonalContact) {
                                final PhoneContact phoneContact = Cont.acts().lookUpPhoneContactById(getApplicationContext(), cb.phoneId);
                                if (phoneContact != null && phoneContact.photoId >= 0) {
                                    photoId = phoneContact.photoId;
                                }
                            }
                        }
                    }

                    if (LogSettings.MARKET) {
                        MktLog.i(TAG, "BindContactCommand, name : " + name + " photoId : " + photoId);
                    }

                    rcCallInfo.setBindedContact(name);
                    rcCallInfo.setPhotoID(photoId);
                    // TDOD: New calls scheme notification
                    getPJSIPCallbackListener().on_call_info_changed(rc_call_id);
                }
            } catch (java.lang.Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "BindContactCommand: error", th);
                }
            }
        }
    }
    
    
    /**
     * Delete current active account
     */
    private void deleteAccount() {
        int accointId = CheckCurrentAccount();
        if (accointId != PJSIP.sf_INVALID_ACCOUNT_ID) {
            final int status = m_pjsip_wrapper.PJSIP_acc_del(accointId);
            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "Delete current account status: " + status);
            }
        }
    }
    
    /**
     * Process RCMConstants.ACTION_RC_APP_STARTED broadcast notification.
     */
    private class RCApplicationStartedReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			if (LogSettings.MARKET) {
	        	MktLog.d(TAG, "RCApplicationStartedReceiver: UI process either started first time or crashed");
	        }
			try {
				m_binder.asw_release();
			} catch (Throwable t) {
				if(LogSettings.MARKET) {
					MktLog.e(TAG, "RCApplicationStartedReceiver.onReceive() : ", t);				
				}
			}
		}
    };
    
	/**
	 * Login receiver. Process RCMConstants.ACTION_LOGIN_FINISHED broadcast notification
	 */
	private class LoginReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
            final boolean voipEnabled = getSipParameters().isVoipEnabled();            
			
	        if (LogSettings.ENGINEERING) {
	        	EngLog.w(TAG, "LoginReceiver: Login notification received : VoIP enabled:" + voipEnabled );
	        }

            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "Login receiver Started");
            }

            
        	final boolean isLoginDetected = isLoginDetected();
        	if( isLoginDetected ){
        		if( m_logInfo.isValid() ){
        			final String ext = getUserInfo().getExtension();
        			final String pin = getUserInfo().getPin();
        			final String psw = getUserInfo().getPassword();
        			final long mbox = getUserInfo().getCurrentMailboxId();

        			if( ext != null && ext.equals(getLogInfo().getExt())){
            			if( psw != null && psw.equals(getLogInfo().getPsw())){
            				if( mbox == getLogInfo().getMbox() ){
            					if( (pin == null && getLogInfo().getPin() == null) || 
            							(pin != null && pin.equals(getLogInfo().getPin()) )){
            						
            						/*
            						 * The same account parameters - do nothing
            						 */
            				        if (LogSettings.ENGINEERING) {
            				        	EngLog.w(TAG, "LoginReceiver: Current account info the same as on previos time. Do nothing");
            				        }
            				        return;
            					}
            				}
            			}        				
        			}
        		}
        	}
        	
	        if (LogSettings.ENGINEERING) {
	        	EngLog.w(TAG, "LoginReceiver: Update local account info");
	        }
        	
        	getLogInfo().setExt(getUserInfo().getExtension());
        	getLogInfo().setPin(getUserInfo().getPin());
        	getLogInfo().setPsw(getUserInfo().getPassword());
        	getLogInfo().setMbox(getUserInfo().getCurrentMailboxId());
        	
        	setLoginDetected( true );
			setHttpRegInfo( null );

            /*
             * If VoIP is disabled then stop SIP stack.
             */
            if (!voipEnabled) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG_EMG, "LoginReceiver: Received LOGIN notification but VoIP is disabled.");
                }
                stackStop(RCCallInfo.CALL_ENDED_BY_ERROR, RCSIP.SipCode.PJSIP_SC_DISABLED_VOIP_AT_LOGIN_DETECTION_EXTENDED);
            } else {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG_EMG, "LoginReceiver: VoipEnabled->stackRun");
                }
                stackRun();
            }
		}
	};

	/*
	 * 
	 */
    protected void prepareSip() {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "prepareSip");
        }
        setHttpRegInfo(null);

        if (mCommandProcessor != null) {
            mCommandProcessor.execute(new StartPJSIPCommand(false));
        }
    }
	
	/**
	 * Logout receiver. Process RCMConstants.ACTION_RESTART_APP broadcast notification
	 */
	private class LogoutReceiver extends BroadcastReceiver{
		@Override
		public void onReceive(Context context, Intent intent) {
			
            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "Received LOGOUT notification");
            }
	        setLoginDetected( false );
            stackStop(RCCallInfo.CALL_ENDED_BY_USER, RCSIP.SipCode.PJSIP_SC_LOGOUT_STATUS_CODE_EXTENDED);
		}
	};

	/**
	 * Analyze type of mobile network. 
	 */
	protected boolean isValidMobileNetwork(){
		
		if( m_telefMng != null ){
			final int netType = m_telefMng.getNetworkType();
			if( netType == TelephonyManager.NETWORK_TYPE_GPRS ) {
			    if (LogSettings.MARKET) {
                    MktLog.i(TAG, "isValidMobileNetwork:No:GPRS" );
                }
				return false;
			}
			if( netType == TelephonyManager.NETWORK_TYPE_EDGE) {
			    if (RCMProviderHelper.isNeedToIncludeEDGEintoMobVoIP(this)) {
		             if (LogSettings.MARKET) {
		                    MktLog.i(TAG, "isValidMobileNetwork:Yes:EDGE included");
		                }
			        return true;
			    }
			    if (LogSettings.MARKET) {
                    MktLog.i(TAG, "isValidMobileNetwork:No:EDGE" );
                }
                return false;
            }
		}
		
		return true;
	}
	
			
	protected boolean is3G4GNetwork(NetworkInfo netInfo) {
	    int activeType = netInfo.getType();
	    if (activeType == ConnectivityManager.TYPE_MOBILE ||
                activeType == 4  || //TYPE_MOBILE_DUN 
                activeType == 5  || //TYPE_MOBILE_HIPRI
                activeType == 2  || //TYPE_MOBILE_MMS 
                activeType == 3     // TYPE_MOBILE_SUPL 
                ) {
	        return true;
	    }
	    return false;
	}
	
    protected boolean isValidNetworkForCalls(NetworkInfo netInfo) {
	    		if( netInfo != null ){
		    		if( !netInfo.isConnected()){
                return false;
		    	        }
            if (is3G4GNetwork(netInfo)) {
                if (!getSipParameters().isVoipOver3GEnabled()) {
		    			return false;
		    		}
		    		
                if (!isValidMobileNetwork()) {
		    			return false;
		    		}
	    		}
	    	}
        return true;
    }
	    	
    protected boolean isValidCurrentNetworkForCalls() {
        if( m_connManager != null ){
            return isValidNetworkForCalls(m_connManager.getActiveNetworkInfo());
        }
	    	return true;
		}
		
	/**
	 * VoIP On/Off or VoIP Incoming Call On/Off receiver. 
	 * Process RCMConstants.ACTION_VOIP_CONFIGURATION_CHANGED broadcast notification
	 */
	private class VoipConfigurationChangedReceiver extends BroadcastReceiver{
	    /**
	     * Defines logging tag.
	     */
	    private static final String TAG = "[RC]VoIPCfgChngRcv";
		
		@Override
		public void onReceive(Context context, Intent intent) {
			
			/*
			 * This flag is define if VoIP was enabled/disabled or 
			 * other VoIP configuration parameters (active networks, incoming calls) were changed.
			 */
			final long voipConfigurationStateChanged = intent.getLongExtra(RCMConstants.EXTRA_VOIP_CONFIGURATION_STATE_CHANGED, 0 );
			
	        final boolean isOutboundOnly= getSipParameters().isOutboundCallsOnly();
	        final boolean isVoipEnabled = getSipParameters().isVoipEnabled();
	        
	        if (LogSettings.MARKET) {
	        	MktLog.i(TAG, "Voip Cfg changed VoipEnabled:" + isVoipEnabled + 
	        			" OutboundOnly:" + isOutboundOnly + 
	        			" VoipStateChanged: " + voipConfigurationStateChanged );
	        }

	        if( voipConfigurationStateChanged == 0 ){
	        	return;
	        }
	        /*
	         * Changed VoIP state 
	         */
	        if (((voipConfigurationStateChanged & RCMConstants.SIP_SETTINGS_USER_VOIP_ENABLED) != 0) ||
	            ((voipConfigurationStateChanged & RCMConstants.SIP_SETTINGS_ON_ENVIRONMENT_ENABLED) != 0) ||
	            ((voipConfigurationStateChanged & RCMConstants.SIP_SETTINGS_ON_ACCOUNT_ENABLED) != 0)) {
	        	
    	        if (LogSettings.MARKET) {
    	        	MktLog.w(TAG, "VoIP configuration has been changed." );
    	        }
	        	
	        	/*
	        	* If VoIP is disabled - shutdown SIP stack
	        	*/
	        	if( isVoipEnabled ){
	        		
	        		if( isValidCurrentNetworkForCalls() ){
	        		    if (LogSettings.MARKET) {
                            MktLog.e(TAG_EMG, "VoipCfgChnRcvr:isVoipEnbld&isVldNet: ->stackRun");
                        }
	        			stackRun();
	        		}
	        	}
	        	else{
	        		
	    	        if (LogSettings.MARKET) {
	    	        	MktLog.i(TAG, "Shutdown SIP stack" );
	    	        }
	    	        
        			if( isOutboundOnly ){
				        if (LogSettings.MARKET) {
				        	MktLog.d(TAG, "Update VoIP Configuration STATE because VoIP was DISABLED");
				        }
						
						//updateVoipState();
        			}
        			else{
    	    			mCommandProcessor.execute(new DeleteAccount());
        			}
        			// TODO:
    				setHttpRegInfo( null ); 
	                mCommandProcessor.execute(new HangupAll(RCCallInfo.CALL_ENDED_BY_SYSTEM, RCSIP.SipCode.PJSIP_SC_DISABLED_VOIP_AFTER_SETTIGS_EXTENDED));
	    		    mCommandProcessor.execute( new StopPJSIP());
	        	}
	        }
	        else{
	        	
	        	/*
	        	 * Changed network configuration
	        	 */
	        	if( (voipConfigurationStateChanged & RCMConstants.SIP_SETTINGS_USER_3G_4G_ENABLED) != 0 ){

	    	        if (LogSettings.MARKET) {
	    	        	MktLog.d(TAG, "VoIP Network configuration has been changed." );
	    	        }
	        		
	        		if( !isValidCurrentNetworkForCalls() ){
		    	        if (LogSettings.MARKET) {
		    	        	MktLog.i(TAG, "No valid network. Shutdown SIP stack" );
		    	        }
		    	        
		    	        // TODO:
		    	        if( getState().isSipStackStarted() ){
		    	        	
	        				setHttpRegInfo( null );
	        				
		        			if( !isOutboundOnly ){
				    			mCommandProcessor.execute(new DeleteAccount());
		        			}
		        			
			                mCommandProcessor.execute(new HangupAll(RCCallInfo.CALL_ENDED_BY_SYSTEM, RCSIP.SipCode.PJSIP_SC_NETWORK_CHANGED_EXTENDED));
			    		    mCommandProcessor.execute( new StopPJSIP());
		    	        }

	        			if( isOutboundOnly ){
	        				
					        if (LogSettings.MARKET) {
					        	MktLog.i(TAG, "Update VoIP Configuration STATE because are no valid network");
					        }
							
							//updateVoipState();
	        			}
		    	        
		    		    return;
                    } else {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG_EMG, "VoipCfgChngdRcvr:3G_4G_ENBLD:VldNet:->stackRun");
                        }
                        stackRun();
                    }
	        	}
	        	
	        	if( (voipConfigurationStateChanged & RCMConstants.SIP_SETTINGS_USER_INCOMING_CALLS_ENABLED) != 0 ){
	        		/*
	        		 * Changed Incoming call configuration
	        		 */
	        		
			        /*
			         * Incoming calls have been disabled then clear HTTP registration and
			         * disable SIP registration.
			         */
	    	        if (LogSettings.MARKET) {
	    	        	EngLog.i(TAG, "Configuration of Incoming calls was changed." );
	    	        }
		        	
			        if( isOutboundOnly ){
			        	
		                mCommandProcessor.execute(new Command("IncomingCallsSwitchedOff") {
		                    @Override
		                    public void run() {
		                        // TODO :
		                    	if( getState().isSipStackStarted() ){
			        				setHttpRegInfo( null ); 
                                    CallManager.CallsState callsState = getCallMng().getCallsState();
                                    if (callsState.active_with_pjsip_and_media > 0 || callsState.active_with_pjsip > 0) {
                                        if (LogSettings.MARKET) {
                                            MktLog.e(TAG_EMG, "IncomingCallsSwitchedOff: There are active PJSIP calls stays here");
                                        }
                                        m_pjsip_wrapper.PJSIP_pjsip_acc_set_registration( 0, false );
                                    } else {
                                        if (LogSettings.MARKET) {
                                            MktLog.e(TAG_EMG, "IncomingCallsSwitchedOff: Shutdown.");
                                        }
                                        stackStop(RCCallInfo.CALL_ENDED_BY_SYSTEM, RCSIP.SipCode.PJSIP_SC_NETWORK_CHANGED_EXTENDED);
                                    }
		                    	}
						        if (LogSettings.MARKET) {
						        	MktLog.i(TAG, "Update VoIP Configuration STATE because incoming calls was DISABLED");
						        }
		                    }
		                });
			        }
			        else
			        {
				        if (LogSettings.MARKET) {
				        	MktLog.i(TAG, "Update VoIP Configuration STATE because incoming calls was ENABLED");
				        }
						
						//updateVoipState();
						
			        	/*
			        	 * Incoming calls have been enabled. Execute HTTP registration and 
			        	 * start SIP stack if needed 
			        	 */
		    	        if (LogSettings.MARKET) {
		    	        	MktLog.i(TAG, "Start SIP stack" );
		    	        }
		    	        if( isValidCurrentNetworkForCalls() ){
		    	            if (LogSettings.MARKET) {
	                            MktLog.e(TAG_EMG, "VoipCfgChgdRcvr:INC_CLLS_ENBLD::isVldNet:->stackRun");
	                        }
		    	        	stackRun();
		    	        }
			        }
	        		
	        	}
	        }
		}
	};
	
	/**
	 * Get state of telephony
	 */
	protected int getPhoneState(){
		if( m_telefMng != null ){
			return m_telefMng.getCallState();
		}
		
		return TelephonyManager.CALL_STATE_IDLE;
	}
	
	/**
	 * Process ACTION_PHONE_STATE_CHANGED broadcast notification
	 */
	private class PhoneStateChangedReceiver extends BroadcastReceiver {
		private final static String sf_CALL_STATE_IDLE  	= "Idle"; 
		private final static String sf_CALL_STATE_RINGING  	= "Ringing"; 
		private final static String sf_CALL_STATE_OFFHOOK  	= "OffHook"; 
		
		private String getPhoneState(int state){
			
			switch( state ){
			case TelephonyManager.CALL_STATE_IDLE:
				return sf_CALL_STATE_IDLE;
			case TelephonyManager.CALL_STATE_RINGING:
				return sf_CALL_STATE_RINGING;
			case TelephonyManager.CALL_STATE_OFFHOOK:
				return sf_CALL_STATE_OFFHOOK;
			}
			return "Unknown";
		}
		
		int m_prevState = -1;
		
		public PhoneStateChangedReceiver(){
			
			if( m_telefMng != null ){
				m_prevState = m_telefMng.getCallState();
			}
			
	        if (LogSettings.MARKET) {
	        	MktLog.i(TAG, "PhoneStateChangedReceiver: Started state: " + getPhoneState( m_prevState ) );
	        }
			
		}
		
		@Override
		public void onReceive(Context context, Intent intent) {
			
			int state = -1;
			if( m_telefMng != null ){
				state = m_telefMng.getCallState();
			}
			
			final boolean haveActiveCall = (getActiveCallsCounter() > 0);
			
	        if (LogSettings.MARKET) {
	        	MktLog.d(TAG, "PhoneStateChangedReceiver: State: " + getPhoneState( state ) + ", have active calls : " + haveActiveCall);
	        }
	        
	        if(( state != m_prevState ) && haveActiveCall ){
		        if( state == TelephonyManager.CALL_STATE_OFFHOOK || state ==  TelephonyManager.CALL_STATE_RINGING ){

			        if (LogSettings.MARKET) {
			        	MktLog.i(TAG, "PhoneStateChanged(OFFHOOK|RINGING) call PJSIP_pjsip_set_no_snd_dev SIP stack started:" + 
			        	        getState().isSipStackStarted() + " created:" + getState().isSipStackCreated() );
			        }
		        	
			        if( getState().isSipStackStarted()){
			        	if( m_pjsip_wrapper != null ){
			        		m_pjsip_wrapper.PJSIP_pjsip_set_no_snd_dev("PhnStChngdRcvr:MobileCall");
			        	}
			        	
						if (mCommandProcessor != null) {
				            mCommandProcessor.execute(new HoldAll());
				        }
			        }
		        }
		        else if ( state == TelephonyManager.CALL_STATE_IDLE ){		            
		        	
	                mCommandProcessor.execute(new Command("SetSoundDevice") {
	                    @Override
	                    public void run() {

	    			        if( getState().isSipStackStarted()){
		    			        if (LogSettings.MARKET) {
		    			        	MktLog.i(TAG, "PhoneStateChanged(IDLE): call PJSIP_pjsip_set_snd_dev SIP stack started:" + 
		    			        	        getState().isSipStackStarted() + " created:" + getState().isSipStackCreated() );
		    			        }					
		    			        mMediaManager.routeInternal();
	    		        		m_pjsip_wrapper.PJSIP_pjsip_set_snd_dev("PhnStChngdRcvr:NoMobileCall");
	    			        }
	                    }
	                });
		        }
		        
		        if (LogSettings.MARKET) {
		        	MktLog.i(TAG, "PhoneStateChangedReceiver: State changed from: " + getPhoneState( m_prevState ) + " to: " + getPhoneState( state ) );
		        }
		        
		        m_prevState = state;
	        }
		}
	};
	
	/**
	 * Process WIFI_STATE_CHANGED_ACTION broadcast notification
	 */
	private class WifiStateChangedReceiver extends BroadcastReceiver
	{
		@Override
		public void onReceive(Context context, Intent intent) {
			
			int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN );
			if( mWiFiManager != null )
				state = mWiFiManager.getWifiState();			
			final int prevState = intent.getIntExtra(WifiManager.EXTRA_PREVIOUS_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN );
			
			final boolean isOutboundOnly = getSipParameters().isOutboundCallsOnly();
			final int activeCallsNumber = getActiveCallsCounter();
			
			if( !isOutboundOnly || 
			    (isOutboundOnly && (activeCallsNumber > 0)) ) {
				
				if(state == WifiManager.WIFI_STATE_ENABLED) {				
					mWiFiWatcher.startWatching();
				} else if (state == WifiManager.WIFI_STATE_DISABLED ) {
					mWiFiWatcher.stopWatching();
				}
			}
			
	        if (LogSettings.MARKET) {
	        	MktLog.d(TAG, "WifiStateChangedReceiver: State: " + getWiFiState( state ) + " Previous: " + getWiFiState( prevState ) + 
	        			", isOutboundOnly : " + isOutboundOnly + ", activeCallsNumber : " + activeCallsNumber);
	        }
		}
	};
	
    /**
     * Process RCMConstants.ACTION_STATION_LOCATION_CHANGE broadcast notification
     */
    private class OnStationChangedReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            PhoneUtils.onSetStationLocation(intent);
        }
    };
    
    private class ServicePhoneStateListener extends PhoneStateListener {
        private static final String INTAG = "[RC]TelephonyState";
	
//        @Override
//        public void onDataConnectionStateChanged(int state) {
//            
//        }
        
        @Override
        public void onServiceStateChanged(android.telephony.ServiceState serviceState) {
            String d = null;
            if (serviceState != null) {
               d = serviceState.toString();
            }
            if (LogSettings.MARKET) {
                MktLog.w(INTAG, "TelephonyStateChanged: " + ((d == null) ? "NULL" : d));
            }
            
            if (m_connManager != null) {
                NetworkInfo activeNet = m_connManager.getActiveNetworkInfo();
                if (activeNet != null) {
                    if (LogSettings.MARKET) {
                        MktLog.d(INTAG, "TelephonyStateChanged: Active Network : " + activeNet.toString());
                    }
                }
                final boolean isStackRun = getState().isSipStackStarted();
                final int state = getState().state();
                if (isValidNetworkForCalls(activeNet)) {
                    if( activeNet != null ){
                        setNetworkType( activeNet.getType() );
                    }
                    
                    if( m_telefMng != null ){
                        setRadioNetworkType(m_telefMng.getNetworkType());
                    }
                    if (getSipParameters().isVoipEnabled() && !getSipParameters().isOutboundCallsOnly()) {
                        if (!(isStackRun || state == ServiceState.ON || state == ServiceState.TO_ON)) {
                            if (LogSettings.MARKET) {
                                MktLog.w(TAG, "NetStateReceiver: Valid network. Start stack");
                            }
                            stackRun();
                        }
                        return;
                    }
                }
            }
        }
    }
    
	
	/**
	 * Process android.net.ConnectivityManager.CONNECTIVITY_ACTION broadcast notification
	 */
	private class NetworkConnectivityActionReceiver extends BroadcastReceiver {

		private int m_ip = 0;
		
		public String getNetState(final NetworkInfo.State state){
			
			if( state == NetworkInfo.State.CONNECTED )
				return "Connected";
			else if( state == NetworkInfo.State.CONNECTING )
				return "Connecting";
			else if( state == NetworkInfo.State.DISCONNECTED )
				return "Disconnected";
			else if( state == NetworkInfo.State.DISCONNECTING )
				return "Disconecting";
			else if( state == NetworkInfo.State.SUSPENDED )
				return "Suspended";
				
			return "Unknown";
		}

		public String getDetailState(final NetworkInfo.DetailedState state){
			
			if( state == NetworkInfo.DetailedState.AUTHENTICATING )
				return "Authenticating";
			else if( state == NetworkInfo.DetailedState.CONNECTED )
				return "Connected";
			else if( state == NetworkInfo.DetailedState.CONNECTING )
				return "Connecting";
			else if( state == NetworkInfo.DetailedState.DISCONNECTED )
				return "Disconnected";
			else if( state == NetworkInfo.DetailedState.DISCONNECTING )
				return "Disconecting";
			else if( state == NetworkInfo.DetailedState.FAILED )
				return "Failed";
			else if( state == NetworkInfo.DetailedState.IDLE )
				return "Idle";
			else if( state == NetworkInfo.DetailedState.OBTAINING_IPADDR )
				return "Obtaining IP";
			else if( state == NetworkInfo.DetailedState.SCANNING )
				return "Scanning";
			else if( state == NetworkInfo.DetailedState.SUSPENDED )
				return "Suspended";
				
			return "Unknown";
		}
		
		
		private Runnable m_checkIP = new Runnable() {

			@Override
			public void run() {
				
				if( mWiFiManager != null ){
				
					int state = mWiFiManager.getWifiState();
					final WifiInfo wifiInfo = mWiFiManager.getConnectionInfo();
					int ip = wifiInfo.getIpAddress();
					
			        if (LogSettings.ENGINEERING) {
			        	EngLog.w(TAG, "WifiStateChangedReceiver Check IP: WifiInfo State:" + getWiFiState(state) + " IP: " + 
			            		android.text.format.Formatter.formatIpAddress( ip ) );
			        }
			        
			        if( state == WifiManager.WIFI_STATE_ENABLED ){
			        	if( ip != 0 ){
			        	    if (LogSettings.MARKET) {
			                    MktLog.e(TAG_EMG, "m_checkIP:run:WiFiEnabled:ip!=0: ->stackRun");
			                }
			        		stackRun();
			        	}
			        	else
							getHandler().postDelayed( this, 1000 );
			        }
				}
			}
		};
		
		@Override
		public void onReceive(Context context, Intent intent) {
            final NetworkInfo networkInfo = (NetworkInfo) intent
                    .getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
			final boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false );			
	        final boolean isStackRun  = getState().isSipStackStarted();
			int activeType = UNKNOWN_NETWORK_TYPE;
    		final NetworkInfo networkActiveInfo = m_connManager.getActiveNetworkInfo();
    		final boolean isOutboundCallsOnly = getSipParameters().isOutboundCallsOnly(); 
    		
			if( networkActiveInfo != null ){
				activeType = networkActiveInfo.getType();
			}
    		
            final boolean activeNetworkChanged = (getNetworkType() != UNKNOWN_NETWORK_TYPE ? (getNetworkType() != activeType)
                    : false);
			setNetworkType( activeType );
            if( m_telefMng != null ){
                setRadioNetworkType(m_telefMng.getNetworkType());
            }
			
            if (LogSettings.MARKET) {
                MktLog.e(
                        TAG,
                        "NetStateReceiver"
                                + " Now [Type:"
                                + networkInfo.getTypeName()
                                + " State:"
                                + getNetState(networkInfo.getState())
                                + " Detail:"
                                + getDetailState(networkInfo.getDetailedState())
                                + "] Active [Type:"
                                + (networkActiveInfo != null ? networkActiveInfo.getTypeName() : "Unknown")
                                + " State:"
                                + (networkActiveInfo != null ? getNetState(networkActiveInfo.getState()) : "Unknown")
                                + " Detail:"
                                + (networkActiveInfo != null ? getDetailState(networkInfo.getDetailedState())
                                        : "Unknown") + "]");

                MktLog.e(TAG, "NetStateReceiver" + " SIP_run:" + isStackRun + " 3G/4G_Enabled:"
                        + getSipParameters().isVoipOver3GEnabled() + " VoIP_enabled:"
                        + getSipParameters().isVoipEnabled() + " No_Connectivity:" + noConnectivity
                        + " Active_Network_changed:" + activeNetworkChanged);
                MktLog.e(TAG, "Current network state dump: " + NetworkUtils.getNetworkStatusAsString(getInstance()));
            }
	        getHandler().removeCallbacks( m_checkIP );
	        
	        if( !getSipParameters().isVoipEnabled() ){
	        	
	        	if( isStackRun ){
	        	    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "NetStateReceiver: VoIP is disabled. Stop stack");
	                }
					stopStack(RCSIP.SipCode.PJSIP_SC_DISABLED_VOIP_AT_NET_NOTIF_EXTENDED);
	        	}
        		return;
	        }
	        
	        if( networkInfo.getState() == NetworkInfo.State.DISCONNECTED  ){
	        	
		        if (LogSettings.MARKET) {
                    MktLog.w(TAG, "NetStateReceiver: DISCONNECTED");
		        }
	        	
                if (activeType == networkInfo.getType()) {
                    if (LogSettings.MARKET) {
                        MktLog.w(
                                TAG,
                                "NetStateReceiver:Network Disconnected:NetType:"
                                + NetworkUtils.getNetworkTypeLabel(activeType) + " stopStack()");
                    }
                    stopStack(RCSIP.SipCode.PJSIP_SC_NETWORK_DISCONNECTED_EXTENDED);
                } else if (activeType == UNKNOWN_NETWORK_TYPE) {
	        	    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "NetStateReceiver:Network Disconnected:NetType:UNKNOWN_NETWORK_TYPE stopStack()");
                    }
					stopStack(RCSIP.SipCode.PJSIP_SC_NETWORK_DISCONNECTED_UNKNOWN_EXTENDED);
	        	}
            } else if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
	        	if( activeType == networkInfo.getType() ){
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "NetStateReceiver CONNECTED. Process state of active network");
			        }
                    if (is3G4GNetwork(networkInfo)) {
                        if (!getSipParameters().isVoipOver3GEnabled()) {

                            if (LogSettings.MARKET) {
                                try {
                                    MktLog.i(TAG, "NetStateReceiver Type:" + networkInfo.getTypeName()
                                            + " VoIP over 3G/4G disabled. Do nothing");
                                } catch (java.lang.Throwable t) {
                                }
                            }
                            return;
                        }

                        if (!isValidMobileNetwork()) {
                            if (LogSettings.MARKET) {
                                try {
                                    MktLog.i(TAG, "NetStateReceiver Type:" + networkInfo.getTypeName()
                                            + " We do not support this mobile network. Do nothing");
                                } catch (java.lang.Throwable t) {
                                }
                            }
                            return;
                        }
	    			}	        		
	        		
					if( activeNetworkChanged && isStackRun ){
                        if (!HANG_UP_ON_NETWORK_CHANGE) {
				        /*
                             * If active network was changed when an active call
                             * is present Set flag Restart needed and finish
                             * processing
				         * 
                             * This flag will be checked on
                             * on_call_state_changed callback when a call has
                             * been disconnected
				         */
                            if (getCallMng().call_get_count() > 0) {
					        if (LogSettings.MARKET) {
                                    MktLog.w(TAG,
                                            "NetStateReceiver Active network has been changed but active calls are present. Restart Stack later.");
					        }
					        getState().setRestartStack( true );
							return;
				        }
				        if (LogSettings.MARKET) {
                                MktLog.w(TAG, "NetStateReceiver: activeNetworkChanged && isStackRun : stopStack()");
                        }
                        } else {
                            if (LogSettings.MARKET) {
                                MktLog.w(TAG, "NetStateReceiver:(activeNetworkChanged && isStackRun) stopStack()");
                            }
                        }
						stopStack(RCSIP.SipCode.PJSIP_SC_NETWORK_CHANGED_EXTENDED);
					}
	    			
					if( !isOutboundCallsOnly ){
						
						if(  networkInfo.getType() != ConnectivityManager.TYPE_WIFI ){
                            if (LogSettings.MARKET) {
                                MktLog.e(TAG_EMG, "NetStateReceiver :incoming_true:not_wifi: ->stackRun");
                            }
                            stackRun();
                        } else {
							final WifiInfo wifiInfo = mWiFiManager.getConnectionInfo();
							int ip = wifiInfo.getIpAddress();
							
					        if (LogSettings.ENGINEERING) {
                                EngLog.w(
                                        TAG,
                                        "NetStateReceiver: IP address:"
                                                + android.text.format.Formatter.formatIpAddress(m_ip));
					        }							
							/*
                             * If IP address for Wi-Fi network is not detected
                             * yet wait for a time and try to start SIP stack
							 */
							if( ip == 0 ){
								getHandler().postDelayed( m_checkIP, 1000 );
                            } else {
							    if (LogSettings.MARKET) {
                                    MktLog.e(TAG_EMG,
                                            "NetStateReceiver:!isOutboundCallsOnly:TYPE_WIFI:ip!=0: ->stackRun");
	                            }
								stackRun();
								
								if( m_ip == 0 ){
									m_ip = ip;
								}

								if( ip != m_ip ){
                                    if (LogSettings.MARKET) {
                                        try {
                                            MktLog.w(TAG, "NetStateReceiver: IP address was changed Old:"
                                                    + android.text.format.Formatter.formatIpAddress(m_ip) + " New:"
                                                    + android.text.format.Formatter.formatIpAddress(ip));
                                        } catch (java.lang.Throwable t) {
                                        }
                                    }
							        
							        m_ip = ip;

							        if (mCommandProcessor != null) {
							            mCommandProcessor.execute(new ChangeIPAddress());
							        }
								}
							}
						}
					}
                } else {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "NetStateReceiver: CONNECTED. Process state  of inactive network");
	        	}
			        }
	        	}
	        }
	        
		/*
		 * 
		 */
		private void stopStack(SipCode reason){
		    if (LogSettings.MARKET) {
                MktLog.w(TAG, "NetStateReceiver:Stop stack" ); 
            }
	        m_ip = 0;
	        stackStop(RCCallInfo.CALL_ENDED_BY_SYSTEM, RCSIP.SipCode.PJSIP_SC_NETWORK_CHANGED_EXTENDED);
		}
	}
	

	private void restartStack() {
	    boolean doStop = false;
	    boolean doStart = false;
	    boolean isLoginDetected = isLoginDetected();
	    boolean isOutboundCallsOnly = getSipParameters().isOutboundCallsOnly();
	    synchronized (getState()) {
            if(!( getState().state() ==  ServiceState.OFF || getState().state() ==  ServiceState.TO_OFF)) {
                doStop = true;
                getState().set(ServiceState.TO_OFF);
            }
            if(!(!isLoginDetected || isOutboundCallsOnly)) {
                doStart = true;
                getState().set(ServiceState.TO_ON);
            }
        }
	    
        if (doStop) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG_EMG, "restartStack: doStop->HngAll/DltAcc/SatpPJSIP");
            }
            setHttpRegInfo(null);
            if (mCommandProcessor != null) {
                mCommandProcessor.execute(new HangupAll(RCCallInfo.CALL_ENDED_BY_SYSTEM, RCSIP.SipCode.PJSIP_SC_OK));
                mCommandProcessor.execute(new DeleteAccount());
                mCommandProcessor.execute(new StopPJSIP());
            }
        }
        
        if (doStart) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG_EMG, "restartStack: doStart->prepareSip");
            }
            prepareSip();
        }
        
	}
	

	/**
	 * Stops stack.
	 * 
	 * @param callEndReason
	 * 
	 * @param error
	 */
    private void stackStop(int callEndReason, SipCode error) {
        boolean stop = false;
        int state = getState().state();
        if (state != ServiceState.OFF && state != ServiceState.TO_OFF) {
            stop = true;
        } else {
            /* Double-check */
            if (getState().isSipStackCreated() || getState().isSipStackStarted() || getCallMng().call_get_count() > 0) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG_EMG, "stackStop(called) at no active state, but double-check caused error. Force stop.");
                }
                stop = true;
            } else {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG_EMG, "stackStop(called) ignored - stack is not runned");
                }
                return;
            }
        }

        if (stop) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "stackStop(HangupAll/DeleteAccount/StopPJSIP) Started");
            }
            getState().set(ServiceState.TO_OFF);
            setHttpRegInfo(null);
            mCommandProcessor.execute(new HangupAll(callEndReason, error));
            mCommandProcessor.execute(new DeleteAccount());
            mCommandProcessor.execute(new StopPJSIP());
        }
    }
	
	/*
	 * 
	 */
	protected void stackRun() {
        if( !isLoginDetected()){
	        if (LogSettings.MARKET) {
	        	MktLog.i(TAG, "stackRun(). Login is not detected. Do nothing" );
	        }
	        return;
        }
        
        if( getSipParameters().isOutboundCallsOnly() ){
	        if (LogSettings.MARKET) {
	        	MktLog.i(TAG, "stackRun(). Do nothing due to Outbound calls only." );
	        }
	        return;
        }
        if (LogSettings.MARKET) {
            MktLog.w(TAG_EMG, "stackRun: prepareSip");
        }
        getState().set(ServiceState.TO_ON);
        prepareSip();
	}
	
	/**
	 * Create Incoming call notification
	 */
	protected void setIncomingCallNotification( final CallInfo callInfo, final int activeCalls ){
		
        if( callInfo == null || callInfo.getRcCallId() == PJSIP.sf_INVALID_RC_CALL_ID ){
            return;
        }
        
		mCommandProcessor.execute( new Command("CreateIncomingCallNotification"){
			@Override
			public void run() {
				
				int calls = activeCalls;
				if( activeCalls == 0 ){
					calls = getCallMng().call_get_count();
				}								
				
				setIncomingNotificationId( callInfo.getRcCallId() );
                
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "Sending Incoming Call notification broadcast: call Id: " + callInfo.getRcCallId() + "; calls: " + calls );
                }                                
                
				Intent notification_intent = new Intent(RCMConstants.ACTION_VOIP_NOTIFY_INCOMING_CALL);
				notification_intent.putExtra(RCMConstants.EXTRA_VOIP_INCOMING_CALL_INFO, callInfo);
				notification_intent.putExtra(RCMConstants.EXTRA_VOIP_INCOMING_CALL_INFO_COUNT, calls);
				notification_intent.putExtra(RCMConstants.EXTRA_VOIP_INCOMING_CALL_START_TIME, SystemClock.elapsedRealtime());
				getApplicationContext().sendBroadcast(notification_intent);
				
				if( calls > 0 ){
					mIncomingNotification.startRingtone(calls > 1, getCallMng().getCallByRCCallId(callInfo.getRcCallId()));
				}								
			}
		});
		
	}
	
	/**
	 * Switch off Incoming ringtone
	 */
	protected void ringtoneOff(){		
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "ringtoneOff" );
        }
        
        mIncomingNotification.stopRingtone();
	}	
	
	
	public AccountInfo getAccountInfo() {
		
		if( getUserInfo() == null ){
	        if (LogSettings.MARKET) {
	            MktLog.d(TAG, "User Info parameters is not present!!");
	        }
			return null;
		}
		
		if( getSipParameters() == null ){
	        if (LogSettings.MARKET) {
	            MktLog.d(TAG, "SIP parameters is not present!!");
	        }
			return null;
		}
		
		AccountInfo accInfo = new AccountInfo();
		
        String smsg = new String("Register Ext:" + 
        		getUserInfo().getExtension() + 
                "[" + getUserInfo().getPin() + 
                "] inst [" + getUserInfo().getInstanceId() + "]" );

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "getAccountInfo " + smsg );
        }
        
        accInfo.setExtension( getUserInfo().getExtension() );
        accInfo.setPin(getUserInfo().getPin());
        accInfo.setPsw(getUserInfo().getPassword());
        accInfo.setLoginId(getUserInfo().getInstanceId());
        accInfo.setServiceProvider(getSipParameters().getServiceProvider());
        
        String uriProxy = getSipParameters().getSipOutboundProxy();
		if( m_sipParameters.use_TCP_For_SIP_registration() ){
			uriProxy += ";transport=tcp";
		}
        accInfo.setSipOutboundProxy(uriProxy);

        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "getAccountInfo SIP service: " + 
            		accInfo.getServiceProvider() + 
            		" SIP proxy: " + 
            		accInfo.getSipOutboundProxy() );
        }
        
		return accInfo;
	}
	
	/**
	 * Sets codec priority in PJSP.
	 * 
	 * @param codec PJSIP codec
	 * 
	 * @param priority prioritity to be set
	 */
	private void setCodecPriority(CodecInfo codec, final int priority) {
	    if (codec.getPjsipValue() == null) {
	        if (LogSettings.MARKET) {
                MktLog.e(TAG, "setCodecPriority: PJSIP value is NULL");
            }
	        return;
	    }
	    int prevPriority = codec.getPriority();
	    int newPriority = codec.setPriority(priority);
	    
	    if (newPriority == prevPriority) {
	        return;
	    }
	    
	    if (LogSettings.MARKET) {
	        StringBuffer sb = new StringBuffer("setCodecPriority: ");
	        sb.append(codec.toString2());
	        sb.append(" ");
	        sb.append(prevPriority);
	        sb.append(" -> ");
	        sb.append(newPriority);

	        if (prevPriority == CodecInfo.Codec.DISABLED) {
	            MktLog.i(TAG, sb.toString() + " (enabled)");
	        } else if (newPriority == CodecInfo.Codec.DISABLED) {
	            MktLog.i(TAG, sb.toString() + " (disabled)"); 
	        } else if (newPriority > prevPriority){
	            MktLog.i(TAG, sb.toString() + " (increased)");
	        } else {
	            MktLog.i(TAG, sb.toString() + " (decreased)");
	        }
        }
	    m_pjsip_wrapper.PJSIP_codec_set_priority(codec.getPjsipValue(), priority);
	}
	
	/**
	 * Audio codec management function
	 * Use only PCMU codec for now due to problem with media
	 * stream after re-invite (unhold) operation.
	 * We try to avoid UPDATE command sending. 
	 */
	private void setPJSIPCodecPriorities(){
		boolean isWiFi = (getNetworkType() == ConnectivityManager.TYPE_WIFI);
		
		
		CodecInfo[] codecToSet = RCMProviderHelper.getCodec(this, isWiFi);
		CodecInfo[] codec = m_pjsip_wrapper.PJSIP_enum_codecs();
		
		if((codec != null) && (codec.length > 0)){
		    for (CodecInfo curCodec: codec) {
		        boolean found = false;
		        
		        for (CodecInfo setCodec: codecToSet) {
		            if (curCodec.getCodec().getCodecIdentifier() == setCodec.getCodec().getCodecIdentifier()) {
		                setCodecPriority(curCodec, setCodec.getPriority());
		                found = true;
		                break;
		            }
		        }
		        
		        if (!found) {
		            if (LogSettings.MARKET) {
                        MktLog.i(TAG, "setPJSIPCodecPriorities: Not found. Disable: " + curCodec.toString());
                    }
		            setCodecPriority(curCodec, CodecInfo.Codec.DISABLED);
		        }
		    }
		} else {
		    if (LogSettings.MARKET) {
		        MktLog.e(TAG, "setPJSIPCodecPriorities: Retrieving current codec set failed.");
		    }
		}
		
		if (LogSettings.MARKET) {
            MktLog.d(TAG, "setPJSIPCodecPriorities: Codec after set: ");
            MktLog.d(TAG, "========================================>");
            m_pjsip_wrapper.PJSIP_enum_codecs();
            MktLog.d(TAG, "========================================<");
        }
	}
	
	/*
	 * 
	 */
	public static String getWiFiState( int state ){
		
		switch( state ){
		case WifiManager.WIFI_STATE_DISABLING:
			return "Disabling";
		case WifiManager.WIFI_STATE_DISABLED:
			return "Disabled";
		case WifiManager.WIFI_STATE_ENABLING:
			return "Enabling";
		case WifiManager.WIFI_STATE_ENABLED:
			return "Enabled";
		case WifiManager.WIFI_STATE_UNKNOWN:
			return "Unknown";
		}
		
		return "Unknown";
	}

	/**
	 * Update VoIP state 
	 * 0 - VoIP is available
	 * 1 - VoIP Wi-Fi is available
	 * 2 - VoIP 3G/4G is available
	 * 3 - VoIP network is available
	 * 
	 * 4 - VoIP incoming calls are available
	 */
//	protected void updateVoipState(){
//		
//		final long voipSettings = getSipParameters().getVoipSettings();
//		final boolean isIncomingCallsEnabled = (( voipSettings & RCMConstants.SIP_SETTINGS_USER_INCOMING_CALLS_ENABLED ) != 0 );
//		long flags = 0;
//		
//		/*
//		 * Check VoIP configuration options
//		 */
//		boolean isVoIPAvailable 	= (( voipSettings & RCMConstants.SIP_SETTINGS_USER_VOIP_ENABLED ) != 0 ) &&
//				(( voipSettings & RCMConstants.SIP_SETTINGS_SIP_FLAG_HTTPREG_ENABLED ) != 0 );
//		
//		boolean isWiFiAvailable = ( ( voipSettings & RCMConstants.SIP_SETTINGS_USER_WIFI_ENABLED ) != 0 );
//		boolean is3G4GAvailable = ( ( voipSettings & RCMConstants.SIP_SETTINGS_USER_3G_4G_ENABLED ) != 0 );
//		
//        if (LogSettings.MARKET) {
//            MktLog.i(TAG, "updateVoipState VoIP configuration settings VoIP:" + isVoIPAvailable +
//            		" Wi-Fi:" + isWiFiAvailable + 
//            		" 3G/4G:" + is3G4GAvailable );
//        }
//		
//		/*
//		 * Detect current state of networks
//		 */
//		if( m_connManager != null ){
//			
//			NetworkInfo[] infoNets = m_connManager.getAllNetworkInfo();
//			for( int i = 0 ; i < infoNets.length; i++ ){
//				if( infoNets[i].getType() == ConnectivityManager.TYPE_WIFI && isWiFiAvailable ){
//			        if (LogSettings.ENGINEERING) {
//			            EngLog.i(TAG, "updateVoipState Network Wi-Fi connected:" + infoNets[i].isConnected() );
//			        }
//					isWiFiAvailable 	= infoNets[i].isConnected();
//				}
//				else if( infoNets[i].getType() == ConnectivityManager.TYPE_MOBILE && is3G4GAvailable ){
//					boolean validMobile = isValidMobileNetwork();
//					
//			        if (LogSettings.ENGINEERING) {
//			            EngLog.i(TAG, "updateVoipState Network MOBILE connected:" + infoNets[i].isConnected() + 
//			            		" valid mobile:" + validMobile );
//			        }
//					is3G4GAvailable 	= ( validMobile ? infoNets[i].isConnected() : false );
//				}
//			}
//		}
//
//		final boolean isNetAvailable = ( isWiFiAvailable || is3G4GAvailable );
//		
//        if (LogSettings.ENGINEERING) {
//            EngLog.i(TAG, "updateVoipState VoIP network state Net:" + isNetAvailable +
//            		" Wi-Fi:" + isWiFiAvailable + 
//            		" 3G/4G:" + is3G4GAvailable );
//        }
//
//		isVoIPAvailable = ( isVoIPAvailable && isNetAvailable );
//        
//		if( isIncomingCallsEnabled ){
//			/*
//			 * Additional checks for Incoming calls
//			 * 1. Is SIP stack started?
//			 * 2. Is HTTP registration finished successfully?
//			 * 3. Is SIP registration finished successfully?
//			 */
//			
//	        if (LogSettings.ENGINEERING) {
//	            EngLog.i(TAG, "updateVoipState check Incoming. Before VoIP:" + isVoIPAvailable +
//	            		" ready to receive:" + getState().isCallEnabled() );
//	        }
//			
//			isVoIPAvailable = ( isVoIPAvailable && getState().isCallEnabled() );
//		}
//		
//		/*
//		 * Set result bits
//		 */
//		if( isVoIPAvailable )
//			flags |= RCMConstants.VOIP_STATUS_VOIP_AVAILABLE;
//			
//		if( isNetAvailable ) 
//			flags |= RCMConstants.VOIP_STATUS_VOIP_NEW_AVAILABLE;
//		
//		if( isWiFiAvailable )
//			flags |= RCMConstants.VOIP_STATUS_VOIP_WIFI_AVAILABLE;
//
//		if( is3G4GAvailable )
//			flags |= RCMConstants.VOIP_STATUS_VOIP_3G_4G_AVAILABLE;
//		
//		if( isIncomingCallsEnabled )
//			flags |= RCMConstants.VOIP_STATUS_VOIP_INCOMING_AVAILABLE;
//		
//        if (LogSettings.ENGINEERING) {
//            EngLog.i(TAG, "updateVoipState VoIP:" + isVoIPAvailable + 
//            		" Net:" + isNetAvailable +
//            		" Wi-Fi:" + isWiFiAvailable + 
//            		" 3G/4G:" + is3G4GAvailable + 
//            		" Incoming:" + isIncomingCallsEnabled );
//        }
//		
//        Intent iVoipStateChanged = new Intent(RCMConstants.ACTION_VOIP_STATE_CHANGED);
//        iVoipStateChanged.putExtra(RCMConstants.EXTRA_VOIP_STATE_CHANGED, flags );
//        getApplicationContext().sendBroadcast(iVoipStateChanged);
//        
//	}
	
	/*
	 * Process broadcast notification ACTION_USER_PRESENT
	 */
    private class UserActionReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {        	

            if (LogSettings.MARKET) {
                MktLog.d(TAG, "UserActionReceiver ACTION_USER_PRESENT is received.");
            }

            long notification = getIncomingNotificationId();
            if (notification != PJSIP.sf_INVALID_CALL_ID) {
                RCCallInfo call = getCallMng().getCallByRCCallId(notification);

                if (call == null || !call.isAlive() || call.isEnding() || call.getState() != RCSIP.PJSUA_CALL_STATE_INCOMING) {
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "UserActionReceiver: RC Call ID " + notification + " is not active. Invalidate.");
                    }
                    invalidateIncomingCallNotification(notification);
                } else {

                    final Intent i_calls = getIncomingCallIntent();
                    if (i_calls != null) {
                        if (LogSettings.MARKET) {
                            MktLog.i(TAG, "UserActionReceiver Start activity.");
                        }
                        getApplication().startActivity(i_calls);
                    }
                }
            } else if (getActiveCallsCounter() > 0) {
            	if(m_telefMng != null) {
            		final int callState = m_telefMng.getCallState();
            		if(callState == TelephonyManager.CALL_STATE_IDLE) {
            			if(LogSettings.MARKET) {
            				MktLog.d(TAG, "User unlocked, has active calls, not GSM, launching VoipStatusActivity");
            			}
            			final Intent activeCallsIntent = new Intent(getApplicationContext(), VoipCallStatusActivity.class);
            			setIncomingCallActivityFlags(activeCallsIntent);
            			getApplication().startActivity(activeCallsIntent);            			
            		}
            	}
            }
            
            
        }
    }
	
	public WrapPJSIP getPJSIPWrapper() {
        return m_pjsip_wrapper;
    }
	
	/**
     * Callback from AudioSetupEngine that audio test has started 
     * @param testID test id
     */
    public void onTestStarted(final int testID) {               
        if(mCommandProcessor != null) {
            mCommandProcessor.execute(
                new Command("onTestStarted()"){
                    @Override
                    public void run() {
                        if (LogSettings.MARKET) {
                            MktLog.d(TAG, "onTestStarted(), testID : " + RCMediaManager.getModeTag(testID));
                        }
                        try {
                            if (m_callback != null) {
                                m_callback.on_test_started(testID);
                            }
                        } catch (Throwable t) {
                            if (LogSettings.MARKET) {
                                MktLog.e(TAG, "onTestStarted(), failed : ", t);
                            }
                        }                   
                    }               
                }
            );
        }
    }
    
    /**
     * Callback from AudioSetupEngine that audio test has completed 
     * @param testID test id
     * @param delaySample delay in samples
     */
    public void onTestCompleted(final int testID, final int delaySample) {      
        if(mCommandProcessor != null) {
            mCommandProcessor.execute(
                new Command("onTestCompleted()"){
                    @Override
                    public void run() {
                        if (LogSettings.MARKET) {
                            MktLog.d(TAG, "onTestCompleted(), testID : " + RCMediaManager.getModeTag(testID) + ", delaySample : " + delaySample);
                        }
                        try {
                            if (m_callback != null) {
                                m_callback.on_test_completed(testID, delaySample);
                            }
                        } catch (Throwable t) {
                            if (LogSettings.MARKET) {
                                MktLog.e(TAG, "onTestCompleted(), failed : ", t);
                            }
                        }               
                    }               
                }
            );
        }
    }
    

	@Override
	public void on_call_info_changed(long rcCallId) {
        CPUlock();
        try {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "on_call_info_changed RC CallId " + rcCallId);
            }

            if (m_callback != null) {
                m_callback.on_call_info_changed(rcCallId);
            }
        } catch (java.lang.Throwable th) {

        }
        CPUunlock();
	}


//=====================================================================
	
	private class ServicePJSIPHandler extends Handler {

	    @Override
        public void handleMessage(Message msg) {
            switch(msg.what) { 
            case MSG_CALL_ANSWER:
                callAnswer((Long) msg.obj);
                return;

            case MSG_CALL_ANSWER_AND_HOLD:
                callAnswerAndHold((Long) msg.obj);
                return;

            case MSG_CALL_ANSWER_AND_HANGUP:
                callAnswerAndHangup((Long) msg.obj);
                return;

            case MSG_CALL_MAKE_CALL_COMPLETE:
                callMakeCallComplete((Long) msg.obj, false);
                return;
            case MSG_CALLS_NOTIFICATION_CONSISTENCY_CHECK:
                onCallsNotificationConsistencyCheck();
                return;
            case MSG_CALL_CONSISTENCY_CHECK:
                synchronized (PeriodicalCallsConcistencyCheckLock) {
                    isPeriodicalCallsConcistencyCheckInjected = false;
                }
                injectCallsConsistencyCommand();
                return;
            case MSG_LOCKERS_CONSISTENCY_CHECK:
                lockersConsistencyCheck();
                return;
            case MSG_COMMANDS_CONSISTENCY_CHECK:
                try {
                    commandsConcistencyCheck((CommandsConcistencyCheck) msg.obj);
                } catch (java.lang.Throwable th) {
                }
                return;
            }
	    }
	}

	/**
	 * NEW "CENTRIC" SCHEME ================================================================================================================
	 */
    
	
	/**************************************************************************************************************************************
     * Calls consistency check.
     *************************************************************************************************************************************/
	
	/**
	 * Keeps sync primitive for calls consistency periodical checks.
	 */
	private volatile Object PeriodicalCallsConcistencyCheckLock = new Object();
	
	/**
     * Defines if calls consistency periodical checks runned.
     */
	private volatile boolean isPeriodicalCallsConcistencyCheckRunned = false;
	private volatile boolean isPeriodicalCallsConcistencyCheckInjected = false;
	/**
	 * Defines timeout for periodical calls consistency check in milliseconds.
	 */
	private static final long TIMEOUT_FOR_CALLS_CONSISTENCY_CHECK = 5000;
	private static final boolean CALLS_CONSISTENCY_CHECK_ENABLE = false;
	
	
	private volatile boolean isSetNetworkPreference = false;  
	private static final boolean ENABLE_KEEPING_CURRENT_NETWORK = false; // Switched off - not correctly work on all devices - can case invalid network state on device
	/**
	 * Ensures that calls consistency checks is periodically runned
	 */
    public void ensurePeriodicalCallsConcistencyCheckRunnedStopped(boolean unconditional) {
        if (CALLS_CONSISTENCY_CHECK_ENABLE) {
            int calls = 0;
            if (unconditional) {
                calls = 1;
            } else {
                calls = getCallMng().getCurrentDrivenCallsNumber();
                if (calls == 0 && getState().isSipStackCreated()) {
                    calls = getWrapper().PJSIP_call_get_count();
                }
            }
            boolean start = false;
            boolean started = false;
            boolean stopped = false;
            synchronized (PeriodicalCallsConcistencyCheckLock) {
                if (calls > 0) {
                    if (ENABLE_KEEPING_CURRENT_NETWORK) {
                        if (!isSetNetworkPreference) {
                            isSetNetworkPreference = true;
                            int networkType = ServicePJSIP.getInstance().getNetworkType();
                            if (networkType != ConnectivityManager.TYPE_WIFI) {
                                if (LogSettings.MARKET) {
                                    MktLog.e(TAG, ">> SET NETWORK PREFERENCE: " + NetworkUtils.getNetworkTypeLabel(networkType));
                                }
                                try {
                                    m_connManager.setNetworkPreference(networkType);
                                } catch (java.lang.Throwable th) {
                                    if (LogSettings.MARKET) {
                                        MktLog.e(TAG, ">> SET NETWORK PREFERENCE ERROR " + th.toString());
                                    }
                                    isSetNetworkPreference = false;
                                }
                            }
                        }
                    }
                    if (!isPeriodicalCallsConcistencyCheckRunned) {
                        isPeriodicalCallsConcistencyCheckRunned = true;
                        started = true;
                    }
                    if (!isPeriodicalCallsConcistencyCheckInjected) {
                        isPeriodicalCallsConcistencyCheckInjected = true;
                        start = true;
                    }
                } else {
                    if (isPeriodicalCallsConcistencyCheckRunned) {
                        isPeriodicalCallsConcistencyCheckInjected = false;
                        isPeriodicalCallsConcistencyCheckRunned = false;
                        stopped = true;
                    }
                    
                    if (ENABLE_KEEPING_CURRENT_NETWORK) {
                        if (isSetNetworkPreference) {
                            isSetNetworkPreference = false;
                            if (LogSettings.MARKET) {
                                MktLog.e(TAG, "<< SET DEFAULT_NETWORK_PREFERENCE");
                            }
                        }
                        try {
                            m_connManager.setNetworkPreference(ConnectivityManager.DEFAULT_NETWORK_PREFERENCE);
                        } catch (java.lang.Throwable th) {
                            if (LogSettings.MARKET) {
                                MktLog.e(TAG, ">> SET NETWORK PREFERENCE ERROR " + th.toString());
                            }
                        }
                    }
                }
            }

            if (start) {
                m_handler.sendMessageDelayed(m_handler.obtainMessage(MSG_CALL_CONSISTENCY_CHECK), TIMEOUT_FOR_CALLS_CONSISTENCY_CHECK);
            }

            if (LogSettings.MARKET) {
                if (!unconditional) {
                    if (started) {
                        MktLog.i(TAG, "CallsConcistencyCheck Started...");
                    } else if (stopped) {
                        MktLog.w(TAG, "CallsConcistencyCheck Stopped.");
                    }
                }
            }
        }
    }
    
    /**
     * Request to make calls consistency check as something wrong has been happened.
     */
    public void CallsConcistencyCheckRequest() {
        if (CALLS_CONSISTENCY_CHECK_ENABLE) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "CallsConcistencyCheckRequest");
            }
            injectCallsConsistencyCommand();
        }
    }
    
    /**
     * Injects <code>CallsConsistencyCommand</code> to main command processor.
     */
    private void injectCallsConsistencyCommand() {
        if (CALLS_CONSISTENCY_CHECK_ENABLE) {
            if (mCommandProcessor != null) {
                mCommandProcessor.execute(new CallsConsistencyCommand());
            }
        }
    }
    
    /**
     * Call consistency check command.
     */
    private class CallsConsistencyCommand extends Command {
        public CallsConsistencyCommand() {
            super("CallsConsistencyCommand");
        }

        @Override
        public void run() {
            if (CALLS_CONSISTENCY_CHECK_ENABLE) {
                if (getCallMng().checkCallsConsistency()) {
                    ensurePeriodicalCallsConcistencyCheckRunnedStopped(true);
                } else {
                    ensurePeriodicalCallsConcistencyCheckRunnedStopped(false);
                }
            }
        }
    }
    
	/**************************************************************************************************************************************
	 * SIProxy notifications
	 */

    
	/**
	 * Notifies on the same thread that <code>calls</code> set has been changed.
	 */
    public void notifyCallsStateChanged_Sync(RCCallInfo[] calls) {
        if (calls == null) {
            return;
        }
        Vector<RCCallInfo> callsForNotification = new Vector<RCCallInfo>();
        synchronized (getCallMng()) {
            for (RCCallInfo call : calls) {
                if (call != null && call.needClientNotification()) {
                    call.setLastStateSeqNumClientNotification();
                    callsForNotification.add(call);
                }
            }
        }
        
        if (callsForNotification.isEmpty()) {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "notifyCallStateChanged_Sync : latest state change notification has been sent");
            }
            return;
        }

        if (m_callback != null) {
            RCCallInfo[] in = new RCCallInfo[callsForNotification.size()];
            in = callsForNotification.toArray(in);
            m_callback.notifyCallsStateChanged(in);
        }
    }
    
    /**
     * Notifies asynchronously that <code>calls</code> set has been changed.
     */
    public void notifyCallsStateChanged_Async(RCCallInfo[] calls) {
        if (calls == null) {
            return;
        }
        if (mCommandProcessor != null) {
            mCommandProcessor.execute(new NotifyCallsStateChangedCommand(calls));
        }
    }

    /**
     * Notifies that <code>calls</code> set has been changed.
     */
    private class NotifyCallsStateChangedCommand extends Command {
        private RCCallInfo[] m_calls = null;

        public NotifyCallsStateChangedCommand(RCCallInfo[] calls) {
            super("NotifyCallsStateChangedCommand");
            m_calls = calls;
        }

        @Override
        public void run() {
            notifyCallsStateChanged_Sync(m_calls);
        }
    }
    
    /***************************************************************************************************************************************
     **************************************************************************************************************************************/
    
    /**
     * CommandsConcistencyCheck
     */
    private static class CommandsConcistencyCheck {
        /**
         * Keeps emergency for exceed max. number of pending commands.
         */
        boolean emergencyQueue = false;

        /**
         * Keeps emergency for exceed max. time for command execution.
         */
        boolean emergencyExecution = false;

        /**
         * Number of pending commands last checked.
         */
        int enqueued = 0;
        
        /**
         * Command identifier last executed.
         */
        int underExecution = 0;
    }
    
    /**
     * Timeout for normal commands consistency checking.
     */
    private static final long COMMANDS_CONSISTENCY_CHECK_NORMAL_TIMEOUT = 120000; /* 2min */
    
    /**
     * Timeout for emergency commands consistency checking.
     */
    private static final long COMMANDS_CONSISTENCY_CHECK_EMERGENCY_TIMEOUT = 45000; /* 45sec */
    
    /**
     * Defines max. enqueued commands for raising emergency check.
     */
    private static final int MAX_ENQUEUED_COMMANDS_FOR_EMERGECY_CHECK = 25;
    
    /**
     * Injects (delay) commands consistency check.
     *  
     * @param cmd the command
     */
    private void ingectCommandsConcistencyCheck(CommandsConcistencyCheck cmd) {
        Handler h = m_handler;
        if (h == null) {
            return;
        }
        h.removeMessages(MSG_COMMANDS_CONSISTENCY_CHECK);
        if (cmd.emergencyQueue || cmd.emergencyExecution) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "EmergencyCommandsConcistencyCheck postponed");
            }
            h.sendMessageDelayed(h.obtainMessage(MSG_COMMANDS_CONSISTENCY_CHECK, cmd), COMMANDS_CONSISTENCY_CHECK_EMERGENCY_TIMEOUT);
        } else {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "CommandsConcistencyCheck postponed");
            }
            h.sendMessageDelayed(h.obtainMessage(MSG_COMMANDS_CONSISTENCY_CHECK, cmd), COMMANDS_CONSISTENCY_CHECK_NORMAL_TIMEOUT);
        }
    }

    /**
     * Process commands consistency check.
     * 
     * @param cmd previous check
     */
    private void commandsConcistencyCheck(CommandsConcistencyCheck cmd) {
        CommandProcessor proc = mCommandProcessor;
        if (proc == null) {
            return;
        }
        int currentEnqueued = proc.getNumberOfEnqueuedCommands();
        int currentExecuted = proc.getCommandIdUnderExecution();
        if (cmd.emergencyQueue || cmd.emergencyExecution) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG_EMG, "EmergencyCommandsConcistencyCheck processing");
            }
            
            if (cmd.emergencyQueue) {
                if (currentEnqueued > MAX_ENQUEUED_COMMANDS_FOR_EMERGECY_CHECK) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG_EMG, "EmergencyCommandsConcistencyCheck(restart): Number of enqueued: " + currentEnqueued
                                + " previous: " + cmd.enqueued);
                    }
                } else {
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG_EMG, "EmergencyCommandsConcistencyCheck: Number of enqueued commands is normal.");
                    }
                    cmd.emergencyQueue = false;
                }
            }
            
            if (cmd.emergencyExecution) {
                if (currentExecuted != 0 && currentExecuted == cmd.underExecution) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG_EMG, "EmergencyCommandsConcistencyCheck(restart): Command " + currentExecuted + " is executed for long time.");
                    }
                } else {
                    cmd.emergencyExecution = false;
                }
            }
            
            if (cmd.emergencyQueue || cmd.emergencyExecution) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG_EMG, "EmergencyCommandsConcistencyCheck(KILL SIP SERVICE)");
                }
                android.os.Process.killProcess(android.os.Process.myPid());
                return;
            } else {
                if (LogSettings.MARKET) {
                    MktLog.i(TAG_EMG, "EmergencyCommandsConcistencyCheck: NORMAL");
                }
            }
        } else {
            if (currentEnqueued > MAX_ENQUEUED_COMMANDS_FOR_EMERGECY_CHECK) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG_EMG, "commandsConcistencyCheck: Number of enqueued: " + currentEnqueued);
                }
                cmd.emergencyQueue = true;
            }

            if (currentExecuted != 0) {
                if (currentExecuted == cmd.underExecution) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG_EMG, "commandsConcistencyCheck: Command " + currentExecuted + " is executed for long time.");
                    }
                    cmd.emergencyExecution = true;
                }
            }
        }
        cmd.enqueued = currentEnqueued;
        cmd.underExecution = currentExecuted;
        ingectCommandsConcistencyCheck(cmd);
    }
    
    /**
     * Defines commands consistency check test timeout for EnqueedCommandsMaxNumberExceeded.
     */
    private static final long MAX_ENQUEUED_COMMANDS_FOR_EMERGECY_CHECK_TEST_TIMEOUT = 10000; 
    
    /**
     * Defines commands consistency check test timeout for EnqueedCommandsMaxNumberExceeded.
     */
    private static final long COMMANDS_CONSISTENCY_CHECK_EMERGENCY_TIMEOUT_TEST_TIMEOUT = (COMMANDS_CONSISTENCY_CHECK_NORMAL_TIMEOUT
            + COMMANDS_CONSISTENCY_CHECK_EMERGENCY_TIMEOUT) * 2;
    
    /**
     * Commands consistency check test command.
     */
    private class CommandsConcistencyCheckTestCommand extends Command {
        private long timeout;

        public CommandsConcistencyCheckTestCommand(long timeout) {
            super("CommandsConcistencyCheckTestCommand");
            this.timeout = timeout;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(timeout);
            } catch (java.lang.Throwable th) {
            }
        }
    };
    
    /**
     * Test: EnqueedCommandsMaxNumberExceeded
     */
    void testEnqueedCommandsMaxNumberExceeded() {
        int number = (int) ((COMMANDS_CONSISTENCY_CHECK_NORMAL_TIMEOUT + 
                COMMANDS_CONSISTENCY_CHECK_EMERGENCY_TIMEOUT)/MAX_ENQUEUED_COMMANDS_FOR_EMERGECY_CHECK_TEST_TIMEOUT) + 
                MAX_ENQUEUED_COMMANDS_FOR_EMERGECY_CHECK;
        for (int i = 0; i < number; i++) {
            injectCommand(new CommandsConcistencyCheckTestCommand(MAX_ENQUEUED_COMMANDS_FOR_EMERGECY_CHECK_TEST_TIMEOUT));
        }
    }

    /**
     * Test: CommandExecutionTimeExceeded
     */
    void testCommandExecutionTimeExceeded() {
        injectCommand(new CommandsConcistencyCheckTestCommand(COMMANDS_CONSISTENCY_CHECK_EMERGENCY_TIMEOUT_TEST_TIMEOUT));
    }
}
