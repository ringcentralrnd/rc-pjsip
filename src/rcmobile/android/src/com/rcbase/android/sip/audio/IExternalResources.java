/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.audio;

/**
 * @author Denis Kudja
 *
 */
public interface IExternalResources {

	/*
	 * Service foreground resources
	 */
	public int 		getServiceForegroundIcon();
	public int 	    getServiceForegroundText();
	public int 	    getServiceForegroundDescription();
	public int 		getServiceForegroundTitle();
	
	/*
	 * Incoming Call notification resources
	 */
	public int 		getIncomingCallIcon();
	public int  	getIncomingCallText();
	
}
