/**
 * Copyright (C) 2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import java.util.Vector;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.client.RCSIP;
import com.rcbase.android.sip.client.RCSIP.SipCode;
import com.rcbase.android.utils.wifi.WiFiWatcher;
import com.rcbase.api.httpreg.HttpRegister.HttpRegistrationStatus;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.utils.DateUtils;

/**
 * Represents SIP service state
 */
public class ServiceState  implements Parcelable {
    /**
     * Defines logging tag
     */
    private static final String TAG = "[RC]ServiceState";
    
    /**
     * Keeps current detailed service state.
     */
    private volatile int mState = OFF;
    
    /**
     * Keeps service start time.
     */
    private long mStartedElapsedTime = SystemClock.elapsedRealtime();
    
    /**
     * Defines OFF state.
     */
    public static final int OFF            = 1;
    
    /**
     * Defines transition to OFF state.
     */
    public static final int TO_OFF         = 50;
    
    /**
     * Defines ON state.
     */
    public static final int ON             = 40;
    
    /**
     * Defines transition to ON state.
     */
    public static final int TO_ON          = 2;
   
    /**
     * Defines transition to ON state when SIP is creating (next {@link #TO_ON_SIP_STACK_STARTING})
     */
    public static final int TO_ON_SIP_STACK_CREATION   = 7;
    
    /**
     * Defines transition to ON state when SIP is staring (next {@link #TO_ON_HTTP_REGISTRATION})
     */
    public static final int TO_ON_SIP_STACK_STARTING   = 10;
    
    /**
     * Defines transition to ON state when HTTP Reg is in progress (next {@link #TO_ON_SIP_REGISTRATION})
     */
    public static final int TO_ON_HTTP_REGISTRATION    = 14;
    
    /**
     * Defines ON state when SIP is in progress (next {@link #TO_ON_SIP_REGISTRATION})
     */
    public static final int TO_ON_SIP_REGISTRATION     = 17;
   
    /**
     * Returns state label.
     * 
     * @param state
     *            service state
     * @return label for the <code>state</code>
     */
    public static String getStateLabel(int state) {
        switch(state) {
        case (OFF):
            return "OFF";
        case (ON):
            return "ON";
        case (TO_ON):
            return "TO_ON";
        case (TO_OFF):
            return "TO_OFF";
        case (TO_ON_SIP_STACK_CREATION):
            return "TO_ON_SIP_STACK_CREATION";
        case (TO_ON_SIP_STACK_STARTING):
            return "TO_ON_SIP_STACK_STARTING";
        case (TO_ON_HTTP_REGISTRATION):
            return "TO_ON_HTTP_REGISTRATION";
        case (TO_ON_SIP_REGISTRATION):
            return "TO_ON_SIP_REGISTRATION";
        default:
            return "UNKNOWN";
        }
    }
    
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(getStateLabel(mState));
        sb.append(" LatestStatus:");
        sb.append(mLatestStatus.msg());
        return sb.toString();
    }
    
    /**
     * Keeps last error status.
     */
    private volatile RCSIP.SipCode mLatestStatus = RCSIP.SipCode.PJSIP_SC_OK;
    
    /**
     * Sets state.
     * 
     * @param state new service state
     */
    public synchronized void set(int state) {
        if (!isValidState(state)) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "setState:Invalid: " + state);
            }
            return;
        }

        if (mState != state) {
            stateChanged();
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "" + getStateLabel(mState) + "->" + getStateLabel(state));
            }
            mState = state;
        }
    }
    
    /**
     * Sets new service state indicating error code.
     * 
     * @param state new service state.
     * @param latestStatus latest status (error)
     */
    public synchronized void set(int state, RCSIP.SipCode latestStatus) {
        set(state);
        setLatestStatus(latestStatus);
    }
    
    /**
     * Defines if <code>state</code> parameter is valid state.
     * 
     * @param state
     *            the state value to be checked
     * @return if <code>state</code> parameter is valid state
     */
    public static final boolean isValidState(int state) {
        switch(state) {
        case (OFF):
        case (ON):
        case (TO_ON):
        case (TO_OFF):
        case (TO_ON_SIP_STACK_CREATION):
        case (TO_ON_SIP_STACK_STARTING):
        case (TO_ON_HTTP_REGISTRATION):
        case (TO_ON_SIP_REGISTRATION):
            return true;
        default:
            return false;
        }
    }
    
    /**
     * Sets latest service status (error)
     * 
     * @param latestStatus
     *            latest service status (error)
     */
    public synchronized void setLatestStatus(RCSIP.SipCode latestStatus) {
        if (mLatestStatus != latestStatus) {
            mLatestStatus = latestStatus;
            stateChanged();
        }
    }
    
    /**
     * Returns latest service status (error)
     * 
     * @return latest service status (error)
     */
    public synchronized RCSIP.SipCode getLatesStatus() {
        return mLatestStatus;
    }

    /**
     * Returns current service state (not detailed)
     * 
     * @return current service state (not detailed)
     */
    public synchronized int state() {
        switch(mState) {
        case (TO_ON_SIP_STACK_CREATION):
        case (TO_ON_SIP_STACK_STARTING):
        case (TO_ON_HTTP_REGISTRATION):
        case (TO_ON_SIP_REGISTRATION):
            return TO_ON;
        }
        return mState;
    }
    
    /**
     * Returns detailed current state of service.
     * 
     * @return detailed current state of service.
     */
    public synchronized int detailedState() {
        return mState;
    }
    
    
    /**
     * Sets SIP stack creation value.
     * 
     * @param sIPStackCreated SIP stack creation value
     */
    public synchronized void setSipStackCreated(boolean sIPStackCreated) {
        m_SIP_stack_created = sIPStackCreated;
    }
    
    /**
     * Returns if SIP stack is created.
     * 
     * @return <code>true</code> if SIP stack is created.
     */
    public synchronized boolean isSipStackCreated() {
        return m_SIP_stack_created;
    }
    
    /**
     * Keeps SIP stack creation status.
     */
    private volatile boolean m_SIP_stack_created = false;
    
    /**
     * Returns if SIP stack is started.
     * 
     * @return <code>true</code> if SIP stack is started.
     */
    public synchronized boolean isSipStackStarted() {
        return m_SIP_stack_started;
    }

    /**
     * Sets SIP staring status.
     * 
     * @param SIP_stack_started
     *            SIP staring status
     */
    protected void setSipStackStarted(boolean SIP_stack_started) {
        m_SIP_stack_started = SIP_stack_started;
    }

    /**
     * Keeps SIP stack starting status.
     */
    private boolean m_SIP_stack_started = false;

    /**
     * Get SIP registration state.
     */
    public synchronized boolean isSIPRegistrationState() {
        return m_SIP_registration_state;
    }

    /**
     * Sets SIP Registration status
     * 
     * @param SIPRegistrationState SIP Registration status
     */
    protected synchronized void setSIPRegistrationState(boolean SIPRegistrationState) {
        m_SIP_registration_state = SIPRegistrationState;
    }

    /**
     * Flag for detection SIP registration state.
     */
    private volatile boolean m_SIP_registration_state = false;

    /**
     * Returns mark if the stack is in restarting mode.
     * 
     * @return mark if the stack is in restarting mode
     */
    public synchronized boolean isRestartStack() {
        return m_restartStack;
    }
    protected synchronized void setRestartStack(boolean restartStack) {
        m_restartStack = restartStack;
    }
    
    /**
     * Flag for detection situation when SIP stack should be restarted when 
     * last call is finished
     */
    private volatile boolean m_restartStack = false;

    
    /**
     * Returns if HTTP Reg is in progress.
     * 
     * @return is HTTP Reg is in progress
     */
    public synchronized boolean isHttpRegistrationOnProgress() {
        return m_httpRegistrationOnProgress;
    }
    
    /**
     * Sets HTTP reg progress status
     * 
     * @param httpRegistrationOnProgress status to be set
     */
    protected synchronized void setHttpRegistrationOnProgress(
            boolean httpRegistrationOnProgress) {
        m_httpRegistrationOnProgress = httpRegistrationOnProgress;
    }
    
    /**
     * Returns if HTTP Reg delayed (postponed)
     * 
     * @return if HTTP Reg delayed (postponed)
     */
    public synchronized boolean isHttpDelayedRegistrationOnProgress() {
        return m_httpDelayedOnProgress;
    }
    
    /**
     * Set HTTP Reg delayed status.
     * 
     * @param httpDelayedRegistrationOnProgress HTTP Reg delayed status to be set
     */
    protected synchronized void setHttpDelayedRegistrationOnProgress(
            boolean httpDelayedRegistrationOnProgress) {
        if (httpDelayedRegistrationOnProgress) {
            mHttpDelayStarted = SystemClock.elapsedRealtime();   
        } else {
            mHttpDelayStarted = 0;
        }
        m_httpDelayedOnProgress = httpDelayedRegistrationOnProgress;
        
    }
    
    /**
     * Defines initial timeout for HTTP Reg re-registration delay.
     */
    private static final long INITAIL_HTTPREG_DELAYED_TIMEOUT = 5000;
    
    /**
     * Defines minimal timeout for HTTP Reg re-registration delay.
     */
    private static final long MIN_HTTPREG_DELAYED_TIMEOUT = 160000;

    /**
     * Keeps next HTTP Reg delay for unsuccessful cases.
     */
    private volatile long mCurrentHTTPRegDelayTimeout = INITAIL_HTTPREG_DELAYED_TIMEOUT;

    public synchronized long getHTTPRegDelayedTimeout() {
        long timeout = INITAIL_HTTPREG_DELAYED_TIMEOUT;
        if (mCurrentHTTPRegDelayTimeout >= MIN_HTTPREG_DELAYED_TIMEOUT) {
            mCurrentHTTPRegDelayTimeout = MIN_HTTPREG_DELAYED_TIMEOUT;
            timeout = MIN_HTTPREG_DELAYED_TIMEOUT;
        } else {
            timeout = mCurrentHTTPRegDelayTimeout;
            mCurrentHTTPRegDelayTimeout = 2 * mCurrentHTTPRegDelayTimeout;
        }

        if (LogSettings.MARKET) {
            MktLog.e(ServicePJSIP.TAG_EMG, "getHTTPRegDelayedTimeout:" + timeout + " next " + mCurrentHTTPRegDelayTimeout);
        }
        return timeout;
    }
    
    /**
     * Used for detection if HTTP registration is on progress.
     * It allows to avoid not useful SIP account deleting.   
     */
    private volatile boolean m_httpRegistrationOnProgress = false;
    
    /**
     * Defines if HTTP Reg delayed (postponed). 
     */
    private volatile boolean m_httpDelayedOnProgress = false;
    
    /**
     * Keeps time when HTTP Reg delay started. 
     */
    private volatile long mHttpDelayStarted = 0;
    
    /**
     * HTTP Reg monitoring values.
     */
    private volatile long mLastSuccessfulHTTPReg = 0;
    private volatile long mLastUnSuccessfulHTTPReg = 0;
    private volatile long mNumberSuccessfulHTTPReg = 0;
    private volatile long mNumberUnSuccessfulHTTPReg = 0;
    private volatile HttpRegistrationStatus mLastUnSuccessfulHTTPRegStatus = HttpRegistrationStatus.HTTP_REG_UNKNOWN;
    private volatile boolean mIsLastHTTPRegSuccessful = false;
    
    /**
     * SIP Reg monitoring values.
     */
    private volatile long mLastSuccessfulSIPReg = 0;
    private volatile long mLastUnSuccessfulSIPReg = 0;
    private volatile SipCode mLastUnSuccessfulSIPRegStatus = SipCode.PJSIP_SC_UNKNOWN_STATUS_CODE_EXTENDED;
    private volatile boolean mIsLastSIPRegSuccessful = false;
    private volatile long mNumberSuccessfulSIPReg = 0;
    private volatile long mNumberUnSuccessfulSIPReg = 0;
    
    /**
     * Keeps latest statuses of failed SIP regs.
     */
    private volatile Vector<String> mLatestFailedSIPRegs = new Vector<String>();
    /**
     * Defines max number of keeping latest failed SIP reg statuses.
     */
    private final static int MAX_LATEST_FAILED_SIP_REG = 10;
    
    /**
     * Keeps latest statuses of SIP regs.
     */
    private volatile Vector<String> mLatestSIPRegs = new Vector<String>();
    /**
     * Defines max number of keeping latest SIP reg statuses.
     */
    private final static int MAX_LATEST_SIP_REG = 30;
    
    /**
     * Adds latest SIP Reg Info
     * 
     * @param info
     *            the info
     */
    private void addLatestSIPRegInfo(String info) {
        try {
            if (mLatestSIPRegs == null) {
                mLatestSIPRegs = new Vector<String>();
            }
            if (mLatestSIPRegs.size() >= MAX_LATEST_SIP_REG) {
                mLatestSIPRegs.remove(mLatestSIPRegs.firstElement());
            }
            if (mLatestSIPRegs.size() >= MAX_LATEST_SIP_REG) {
                mLatestSIPRegs = new Vector<String>();
            }
            mLatestSIPRegs.add(info);
        } catch (java.lang.Throwable th) {
        }
    }
    
    /**
     * Sets latest SIP Reg status
     * 
     * @param success
     *            defines status of latest registration
     * @param unsuccessStatus
     *            error code for failed latest operation
     */
    public void setLatestSIPRegStatus(boolean success, SipCode unsuccessStatus, int expires) {
        synchronized (this) {
            if (success) {
                mLastSuccessfulSIPReg = SystemClock.elapsedRealtime();
                mIsLastSIPRegSuccessful = true;
                mCurrentHTTPRegDelayTimeout = INITAIL_HTTPREG_DELAYED_TIMEOUT;
                mNumberSuccessfulSIPReg++;
                StringBuffer sb = new StringBuffer(DateUtils.currentTimeLabel() + " : success");
                if (expires > 30) {
                    sb.append("; next in ");
                    sb.append(expires);
                    sb.append("s");
                }
                addLatestSIPRegInfo(sb.toString());
            } else {
                mIsLastSIPRegSuccessful = false;
                mNumberUnSuccessfulSIPReg++;
                mLastUnSuccessfulSIPReg = SystemClock.elapsedRealtime();
                if (unsuccessStatus == null) {
                    mLastUnSuccessfulSIPRegStatus = SipCode.PJSIP_SC_UNKNOWN_STATUS_CODE_EXTENDED;
                } else {
                    String sipreg = DateUtils.currentTimeLabel() + " : " + unsuccessStatus.msg();
                    try {
                        if (mLatestFailedSIPRegs == null) {
                            mLatestFailedSIPRegs = new Vector<String>();
                        }
                        if (mLatestFailedSIPRegs.size() >= MAX_LATEST_FAILED_SIP_REG) {
                            mLatestFailedSIPRegs.remove(mLatestFailedSIPRegs.firstElement());
                        } 
                        if (mLatestFailedSIPRegs.size() >= MAX_LATEST_FAILED_SIP_REG) {
                            mLatestFailedSIPRegs = new Vector<String>();
                        }
                        mLatestFailedSIPRegs.add(sipreg);
                    } catch (java.lang.Throwable th) {
                    }
                    addLatestSIPRegInfo(sipreg);
                    mLastUnSuccessfulSIPRegStatus = unsuccessStatus;
                }
            }
        }
    }
    
    /**
     * Sets latest HTTP Reg status
     * 
     * @param success
     *            defines status of latest registration
     * @param unsuccessStatus
     *            error code for failed latest operation
     */
    public void setLatestHTTPRegStatus(boolean success, HttpRegistrationStatus unsuccessStatus) {
        synchronized (this) {
            if (success) {
                mLastSuccessfulHTTPReg = SystemClock.elapsedRealtime();
                mIsLastHTTPRegSuccessful = true;
                mCurrentHTTPRegDelayTimeout = INITAIL_HTTPREG_DELAYED_TIMEOUT;
                mNumberSuccessfulHTTPReg++;
            } else {
                mIsLastHTTPRegSuccessful = false;
                mNumberUnSuccessfulHTTPReg++;
                mLastUnSuccessfulHTTPReg = SystemClock.elapsedRealtime();
                if (unsuccessStatus == null) {
                    mLastUnSuccessfulHTTPRegStatus = HttpRegistrationStatus.HTTP_REG_UNKNOWN;
                } else {
                    mLastUnSuccessfulHTTPRegStatus = unsuccessStatus;
                }
            }
        }
    }
    
    /**
     * Keeps instance state sequence number for synchronization.
     */
    private volatile int mStateSeqNum = 0;
    
    /**
     * Keeps latest state sequence number when the clients (SIProxy on application side) was notified
     * 
     * See {@link #mStateSeqNum}
     * 
     */
    private volatile int mLastStateSeqNumClientNotification = 0;
    
    /**
     * Returns latest state sequence number when the service clients (SIProxy on
     * application side) was notified
     * 
     * @return latest state sequence number when the service clients (SIProxy on
     *         application side) was notified
     */
    public int getLastStateSeqNumClientNotification() {
        synchronized (this) {
            return mLastStateSeqNumClientNotification;
        }
    }
    
    /**
     * Sets latest state sequence number when the service clients (SIProxy on
     * application side) was notified
     */
    void setLastStateSeqNumClientNotification() {
        synchronized (this) {
            mLastStateSeqNumClientNotification = mStateSeqNum;
        }
    }
    
    /**
     * Defines if needed notification client about state/info change
     * @return
     */
    boolean needClientNotification() {
        synchronized (this) {
            return mLastStateSeqNumClientNotification != mStateSeqNum;
        }
    }
    
    /**
     * If state has been changed.
     */
    private void stateChanged() {
        synchronized (this) {
            if (mStateSeqNum >= Integer.MAX_VALUE) {
                mStateSeqNum = 0;
            }
            mStateSeqNum++;
        }
    }
    
    /**
     * Default constructor.
     */
    public ServiceState() {
        
    }
    
    /** 
     *  Generates instances of your Parcelable class from a Parcel
     */
    public static final Parcelable.Creator<ServiceState> CREATOR = new Parcelable.Creator<ServiceState>() {
        
        public ServiceState createFromParcel( Parcel in ){
            return new ServiceState(in);
        }
        
        public ServiceState[] newArray( int size){
            return new ServiceState[size];
        }
    };
    
    /**
     * Constructor from serialized data.
     * 
     * @param in the parcel with serialized data
     */
    private ServiceState(Parcel in) {
        readFromParcel(in);
    }
    
    @Override
    public int describeContents() {
        return 0;
    }
    
    /**
     * Snaps additional statistics and return state.
     * 
     * @return state with additional statistics
     */
    public ServiceState snapStatistics() {
        WiFiWatcher w = ServicePJSIP.getInstance().getWiFiWatcher();
        
        if (w != null) {
            isWiFiWatcherExists = true;
            isWiFiWatcherWatching = w.isWatching();
            isWiFiWatcherLockHeld = w.isLockHeld();
            WiFiWatcherLockCounter = w.getLockCounter();
        } else {
            isWiFiWatcherExists = false;
        }
        
        
        ServicePJSIPWakeLock l = ServicePJSIP.getInstance().getWakeLock();
        if (l != null) {
            isWakeLockExists = true;
            isWakeLockHeld = l.isHeld();
            WakeLockCounter = l.getLockCounter();
        } else {
            isWakeLockExists = false;
        }
        
        ServicePJSIPIncomingCallsLock il = ServicePJSIP.getInstance().getWakeLockForIncomingCalls();
        if (il != null) {
            isWakeLockForIncomingCallsExists = true;
            isWakeLockForIncomingCallsHeld = il.isHeld();
            WakeLockForIncomingCallsCounter = il.getLockCounter();
        } else {
            isWakeLockForIncomingCallsExists = false;
        }
        
        return this;
    }
    
    /**
     * WiFi Watcher related additional statistics. 
     */
    private volatile boolean isWiFiWatcherExists         = false;
    private volatile boolean isWiFiWatcherWatching       = false;
    private volatile boolean isWiFiWatcherLockHeld       = false;
    private volatile int     WiFiWatcherLockCounter      = 0;
    
    /**
     * WakeLock related additional statistics. 
     */
    private volatile boolean isWakeLockExists            = false;
    private volatile boolean isWakeLockHeld              = false;
    private volatile long    WakeLockCounter             = 0;
    
    /**
     * WakeLock for incoming calls related additional statistics. 
     */
    private volatile boolean isWakeLockForIncomingCallsExists            = false;
    private volatile boolean isWakeLockForIncomingCallsHeld              = false;
    private volatile long    WakeLockForIncomingCallsCounter             = 0;
    
    
    /**
     * Reading serialized data.
     * 
     * @param in the parser with serialized data.
     */
    public void readFromParcel(Parcel in) {
        mState = in.readInt();
        m_httpRegistrationOnProgress = (0 == in.readInt() ? false : true);
        m_restartStack = (0 == in.readInt() ? false : true);
        m_SIP_registration_state = (0 == in.readInt() ? false : true);
        m_SIP_stack_started = (0 == in.readInt() ? false : true);
        m_SIP_stack_created = (0 == in.readInt() ? false : true);
        mLatestStatus = RCSIP.SipCode.get(in.readInt());
        mStartedElapsedTime = in.readLong();
        
        isWiFiWatcherExists = (0 == in.readInt() ? false : true);
        isWiFiWatcherWatching = (0 == in.readInt() ? false : true);
        isWiFiWatcherLockHeld = (0 == in.readInt() ? false : true);
        WiFiWatcherLockCounter = in.readInt();
        
        isWakeLockExists = (0 == in.readInt() ? false : true);
        isWakeLockHeld = (0 == in.readInt() ? false : true);
        WakeLockCounter = in.readLong();

        isWakeLockForIncomingCallsExists = (0 == in.readInt() ? false : true);
        isWakeLockForIncomingCallsHeld = (0 == in.readInt() ? false : true);
        WakeLockForIncomingCallsCounter = in.readLong();

        mLastSuccessfulHTTPReg = in.readLong();
        mLastUnSuccessfulHTTPReg = in.readLong();
        mLastUnSuccessfulHTTPRegStatus = HttpRegistrationStatus.get(in.readInt());
        mIsLastHTTPRegSuccessful = (0 == in.readInt() ? false : true);
        mNumberUnSuccessfulHTTPReg = in.readLong();
        mNumberSuccessfulHTTPReg = in.readLong();
        
        mLastSuccessfulSIPReg = in.readLong();
        mLastUnSuccessfulSIPReg = in.readLong();
        mLastUnSuccessfulSIPRegStatus = SipCode.get(in.readInt());
        mIsLastSIPRegSuccessful = (0 == in.readInt() ? false : true);
        mNumberUnSuccessfulSIPReg = in.readLong();
        mNumberSuccessfulSIPReg = in.readLong();
        
        mCurrentHTTPRegDelayTimeout = in.readLong();
        m_httpDelayedOnProgress = (0 == in.readInt() ? false : true);
        mHttpDelayStarted = in.readLong();
        
        mLatestFailedSIPRegs = new Vector<String>();
        mLatestSIPRegs = new Vector<String>();
        
        try {
            Vector<String> v = new Vector<String>();
            int n = in.readInt();
            if (n > 0) {
                for (int i = 0; (i < n); i++) {
                    v.add(in.readString());
                }
            }
            mLatestFailedSIPRegs = v;

            v = new Vector<String>();
            n = in.readInt();
            if (n > 0) {
                for (int i = 0; (i < n); i++) {
                    v.add(in.readString());
                }
            }
            mLatestSIPRegs = v;
        } catch (java.lang.Throwable th) {
        }
    }
    
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mState);
        dest.writeInt((m_httpRegistrationOnProgress ? 1 : 0));
        dest.writeInt((m_restartStack ? 1 : 0));
        dest.writeInt((m_SIP_registration_state ? 1 : 0));
        dest.writeInt((m_SIP_stack_started ? 1 : 0));
        dest.writeInt((m_SIP_stack_created ? 1 : 0));
        dest.writeInt(mLatestStatus.code());
        dest.writeLong(mStartedElapsedTime);
        
        dest.writeInt((isWiFiWatcherExists ? 1 : 0));
        dest.writeInt((isWiFiWatcherWatching ? 1 : 0));
        dest.writeInt((isWiFiWatcherLockHeld ? 1 : 0));
        dest.writeInt(WiFiWatcherLockCounter);
        
        dest.writeInt((isWakeLockExists ? 1 : 0));
        dest.writeInt((isWakeLockHeld ? 1 : 0));
        dest.writeLong(WakeLockCounter);

        dest.writeInt((isWakeLockForIncomingCallsExists ? 1 : 0));
        dest.writeInt((isWakeLockForIncomingCallsHeld ? 1 : 0));
        dest.writeLong(WakeLockForIncomingCallsCounter);
        
        dest.writeLong(mLastSuccessfulHTTPReg);
        dest.writeLong(mLastUnSuccessfulHTTPReg);
        dest.writeInt(mLastUnSuccessfulHTTPRegStatus.code());
        dest.writeInt((mIsLastHTTPRegSuccessful ? 1 : 0));
        dest.writeLong(mNumberUnSuccessfulHTTPReg);
        dest.writeLong(mNumberSuccessfulHTTPReg);

        dest.writeLong(mLastSuccessfulSIPReg);
        dest.writeLong(mLastUnSuccessfulSIPReg);
        dest.writeInt(mLastUnSuccessfulSIPRegStatus.code());
        dest.writeInt((mIsLastSIPRegSuccessful ? 1 : 0));
        dest.writeLong(mNumberUnSuccessfulSIPReg);
        dest.writeLong(mNumberSuccessfulSIPReg);
        
        dest.writeLong(mCurrentHTTPRegDelayTimeout);
        dest.writeInt((m_httpDelayedOnProgress ? 1 : 0));
        dest.writeLong(mHttpDelayStarted);

        String[] latestSipRegs = null;
        synchronized (this) {
            if (mLatestFailedSIPRegs != null) {
                latestSipRegs = new String[mLatestFailedSIPRegs.size()];
                latestSipRegs = mLatestFailedSIPRegs.toArray(latestSipRegs);
            }
        }
        if (latestSipRegs == null || latestSipRegs.length == 0) {
            dest.writeInt(0);
        } else {
            dest.writeInt(latestSipRegs.length);
            for (String s : latestSipRegs) {
                dest.writeString(s);
            }
        }
        latestSipRegs = null;
        synchronized (this) {
            if (mLatestSIPRegs != null) {
                latestSipRegs = new String[mLatestSIPRegs.size()];
                latestSipRegs = mLatestSIPRegs.toArray(latestSipRegs);
            }
        }
        if (latestSipRegs == null || latestSipRegs.length == 0) {
            dest.writeInt(0);
        } else {
            dest.writeInt(latestSipRegs.length);
            for (String s : latestSipRegs) {
                dest.writeString(s);
            }
        }
    }
    
    /**
     * Returns call dump for tracing.
     * 
     * @return call dump for tracing.
     */
    public String dump() {
        StringBuffer sb = new StringBuffer("Service: ");
        sb.append('\n');sb.append("  Started: ");sb.append(DateUtils.getUTCandRelativeDateFromElapsedTime(mStartedElapsedTime));
        
        sb.append('\n');sb.append("  State  : "); sb.append(getStateLabel(mState));sb.append(" / "); sb.append(mLatestStatus.msg());
        
        sb.append('\n');sb.append("  HTTP/SIP Reg Progress: "); 
        sb.append(m_httpRegistrationOnProgress);sb.append("/"); sb.append(m_SIP_registration_state);
        
        sb.append('\n');sb.append("  Stack Created/Started/Restart: "); 
        sb.append(m_SIP_stack_created);sb.append('/'); sb.append(m_SIP_stack_started);sb.append('/'); sb.append(m_restartStack);

        sb.append('\n');sb.append("  HTTP REG: Last:");sb.append(mIsLastHTTPRegSuccessful);sb.append(" +/-: ");sb.append(mNumberSuccessfulHTTPReg);sb.append('/'); sb.append(mNumberUnSuccessfulHTTPReg);
        
        if (mLastSuccessfulHTTPReg != 0) {
            sb.append('\n');sb.append("   LastSuccessful: ");sb.append(DateUtils.getUTCandRelativeDateFromElapsedTime(mLastSuccessfulHTTPReg));    
        } 
        
        if (mLastUnSuccessfulHTTPReg != 0) {
            sb.append('\n');sb.append("   LastUnSuccessful: ");sb.append(DateUtils.getUTCandRelativeDateFromElapsedTime(mLastUnSuccessfulHTTPReg));
            sb.append('\n');sb.append("   LastUnSuccess: ");sb.append(mLastUnSuccessfulHTTPRegStatus.msg());
        }
        sb.append('\n');sb.append("   DelayIsInProgress: ");sb.append(m_httpDelayedOnProgress);
        
        if (mHttpDelayStarted != 0) {
            sb.append('\n');sb.append("   DelayStartedMsAgo: ");sb.append(SystemClock.elapsedRealtime() - mHttpDelayStarted);
        }
        sb.append('\n');sb.append("   NextDelay: ");sb.append(mCurrentHTTPRegDelayTimeout);
        
        sb.append('\n');sb.append("  SIP REG: Last:");sb.append(mIsLastSIPRegSuccessful);        
        sb.append("   +/-: ");sb.append(mNumberSuccessfulSIPReg);sb.append('/'); sb.append(mNumberUnSuccessfulSIPReg);
        if (mLastSuccessfulSIPReg != 0) {
            sb.append('\n');sb.append("   LastSuccessful: ");sb.append(DateUtils.getUTCandRelativeDateFromElapsedTime(mLastSuccessfulSIPReg));    
        } 
        
        if (mLastUnSuccessfulSIPReg != 0) {
            sb.append('\n');sb.append("   LastUnSuccessful: ");sb.append(DateUtils.getUTCandRelativeDateFromElapsedTime(mLastUnSuccessfulSIPReg));
            sb.append('\n');sb.append("   LastUnSuccess: ");sb.append(mLastUnSuccessfulSIPRegStatus.msg());
        }
        
        if (mLatestSIPRegs != null && !mLatestSIPRegs.isEmpty()) {
            sb.append('\n');sb.append("  SIP REG Latest: ");
            sb.append('\n');sb.append("  =======================:");
            for (String s : mLatestSIPRegs) {
                 sb.append('\n');sb.append(s);    
            }
            sb.append('\n');sb.append("  =======================:");
        }
        
        if (mLatestFailedSIPRegs != null && !mLatestFailedSIPRegs.isEmpty()) {
            sb.append('\n');sb.append("  SIP REG Latest Failures:");
            sb.append('\n');sb.append("  =======================:");
            for (String s : mLatestFailedSIPRegs) {
                 sb.append('\n');sb.append(s);    
            }
            sb.append('\n');sb.append("  =======================:");
        }
        
        if (isWiFiWatcherExists) {
            sb.append('\n');sb.append("  WiFi Watching:");sb.append(isWiFiWatcherWatching);
                            sb.append(" Held:");sb.append(isWiFiWatcherLockHeld);sb.append(" Count:");sb.append(WiFiWatcherLockCounter);
        } else {
            sb.append('\n');sb.append("  WiFi Watcher : NONE");
        }
        
        if (isWakeLockExists) {
            sb.append('\n');sb.append("  SIP Lock Held:");sb.append(isWakeLockHeld);
                            sb.append(" Count:");sb.append(WakeLockCounter);
        } else {
            sb.append('\n');sb.append("  SIP Lock : NONE");
        }

        if (isWakeLockForIncomingCallsExists) {
            sb.append('\n');sb.append("  SIP IncomingLock Held:");sb.append(isWakeLockForIncomingCallsHeld);
                            sb.append(" Count:");sb.append(WakeLockForIncomingCallsCounter);
        } else {
            sb.append('\n');sb.append("  SIP IncomingLock : NONE");
        }
        
        return sb.toString();
    }
}
