/**
 * Copyright (C) 2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import java.lang.reflect.Method;
import android.os.PowerManager;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.utils.execution.ExecutionWakeLock;
import com.ringcentral.android.LogSettings;

/**
 * Wake lock for SIP commands execution. 
 */
public class ServicePJSIPIncomingCallsLock implements ExecutionWakeLock {
    /**
     * Synchronization primitive.
     */
    private Object _lock = new Object();
    
    /**
     * Defines logging tag.
     */
    private static final String LOG_TAG = "[RC]IncomingCallsLock";
    
    /**
     * Keeps Android Power manager.
     */
    private PowerManager mPowerManager;
    
    /**
     * Keeps partial wake lock.
     */
    private PowerManager.WakeLock mPartialWakeLock;
    
    /**
     * acquire/release counter.
     */
    private long mCounter;
    
    private static final int MAX_ACQUIRES_WHEN_WARN = 2;
    
    /**
     * Constructs wake lock for SIP commands execution.
     * 
     * @param powerManager the power manager instance
     */
    public ServicePJSIPIncomingCallsLock(PowerManager powerManager) {
        mPowerManager = powerManager;
    }

    
    @Override
    public void acquire() {
        synchronized (_lock) {
            try {
                if (mPartialWakeLock == null) {
                    mPartialWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "RingCentral VoIP Incoming");
                    mPartialWakeLock.setReferenceCounted(false);
                }
            } catch (Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.w(LOG_TAG, " acquire:newWakeLock:exception:" + th.toString());
                }
                return;
            }

            try {
                if (!mPartialWakeLock.isHeld()) {
                    mPartialWakeLock.acquire();
                }
            } catch (Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.w(LOG_TAG, " acquire:acquire:exception:" + th.toString());
                }
                return;
            }
            mCounter++;
        }
        
        if (mCounter >= MAX_ACQUIRES_WHEN_WARN) {
            if (LogSettings.MARKET) {
                MktLog.w(LOG_TAG, " acquire:limit:" + mCounter);
            }
        }
    }

    @Override
    public void release() {
        synchronized (_lock) {
            boolean released = false;
            mCounter--;
            if (mCounter < 0) {
                if (LogSettings.MARKET) {
                    MktLog.w(LOG_TAG, " relase:negative counter:");
                }
                mCounter = 0;
            }
            try {
                if ((mCounter == 0) && (mPartialWakeLock != null) && (mPartialWakeLock.isHeld())) {
                    released = true;
                    mPartialWakeLock.release();
                }
            } catch (Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.w(LOG_TAG, " release:release:exception:" + th.toString());
                }
                return;
            }
            if (released) {
                if (LogSettings.MARKET) {
                    MktLog.d(LOG_TAG, "Released");
                }
            }
        }
    }

    public long getLockCounter() {
        synchronized (_lock) {
            return mCounter;
        }
    }
    
    @Override
    public void releaseAll() {
        synchronized (_lock) {
            if (mCounter > 0) {
                if (LogSettings.MARKET) {
                    MktLog.d(LOG_TAG, " releaseAll:counter is " + mCounter);
                }
            }
            
            mCounter = 0;
            try {
                if ((mPartialWakeLock != null) && (mPartialWakeLock.isHeld())) {
                    mPartialWakeLock.release();
                }
            } catch (Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.w(LOG_TAG, " releaseAll:release:exception:" + th.toString());
                }
                return;
            }
        }
    }
    
    /**
     * Returns if the lock is acquired.
     * 
     * @return if the lock is acquired
     */
    public boolean isHeld() {
        synchronized (_lock) {
            try {
                if (mPartialWakeLock != null) {
                    return mPartialWakeLock.isHeld();
                }
            } catch (Throwable th) {
            }
        }
        return false;
    }
}
