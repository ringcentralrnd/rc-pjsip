/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.media.AudioManager;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.client.RCSIP;
import com.rcbase.android.sip.client.RCSIP.SipCode;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.provider.RCMProviderHelper;
import com.ringcentral.android.utils.NetworkUtils;

/**
 * <code>RCCallInfo</code> represents RC VoIP call.
 */
public class RCCallInfo implements Parcelable {
    /**
     * Defines logging tag
     */
    private static final String TAG = "[RC]RCCallInfo";
    
    /**
     * Defines ending reason : no ending
     * 
     * @see #getEndingReason()
     */
    public static final int CALL_ENDED_NO = 0;
    
    /**
     * Defines ending reason : by the end-user
     * 
     * @see #setEnding(int, boolean, String)
     * @see #isEnding()
     * @see #getEndingReason()
     */
    public static final int CALL_ENDED_BY_USER = 1;
    
    /**
     * Defines ending reason : by party (recipient)
     * 
     * @see #setEnding(int, boolean, String)
     * @see #isEnding()
     * @see #getEndingReason()
     */
    public static final int CALL_ENDED_BY_PARTY = 2;
    
    /**
     * Defines ending reason : by the system like shutdown
     * 
     * @see #setEnding(int, boolean, String)
     * @see #isEnding()
     * @see #getEndingReason()
     */
    public static final int CALL_ENDED_BY_SYSTEM = 3;
    
    /**
     * Defines ending reason : by an error occurred
     * 
     * @see #setEnding(int, boolean, String)
     * @see #isEnding()
     * @see #getEndingReason()
     */
    public static final int CALL_ENDED_BY_ERROR = 4;
    
    /**
     * Defines if the call marked for ending.
     */
    private int mCallEnded = CALL_ENDED_NO;  
    
    /**
     * Returns if the call marked for ending
     * 
     * @return if the call marked for ending
     */
    public boolean isEnding() {
        return mCallEnded != CALL_ENDED_NO;
    }

    /**
     * Returns reason of ending: {@link #CALL_ENDED_BY_USER}, {@link #CALL_ENDED_BY_PARTY}, {@link #CALL_ENDED_BY_SYSTEM}, {@link #CALL_ENDED_BY_ERROR}
     * 
     * @see #setEnding(int, boolean, String)
     * @see #isEnding()
     * @return
     */
    public int  getEndingReason() {
        return mCallEnded;
    }
    
    /**
     * Returns ending state as string.
     * 
     * @see #setEnding(int, boolean, String)
     * @see #getEndingReason()
     * 
     * @return ending state as string.
     */
    public String  getEndingReasonAsString() {
        return getEndingReasonAsString(mCallEnded);
    }
    
    /**
     * Returns ending state as string.
     * 
     * @see #setEnding(int, boolean, String)
     * @see #getEndingReason()
     * @param reason
     *            {@link #CALL_ENDED_BY_USER}, {@link #CALL_ENDED_BY_PARTY},
     *            {@link #CALL_ENDED_BY_SYSTEM}, {@link #CALL_ENDED_BY_ERROR}
     * @return ending state as string.
     */
    public static final String getEndingReasonAsString(int reason) {
        switch (reason) {
        case (CALL_ENDED_NO):
            return "No";
        case (CALL_ENDED_BY_USER):
            return "By User";
        case (CALL_ENDED_BY_PARTY):
            return "By party";
        case (CALL_ENDED_BY_SYSTEM):
            return "By system";
        case (CALL_ENDED_BY_ERROR):
            return "By error";
        }

        return "UNKNOWN " + reason;
    }
    
    
    /**
     * Set ending state.
     * 
     * @param reason
     *            {@link #CALL_ENDED_BY_USER}, {@link #CALL_ENDED_BY_PARTY},
     *            {@link #CALL_ENDED_BY_SYSTEM}, {@link #CALL_ENDED_BY_ERROR}
     * @param ending
     */
    public void setEnding(int reason) {
        synchronized (mLock) {
            if (mCallEnded == CALL_ENDED_NO) {
                mCallEnded = reason;
                m_timeEnding = getTime();
                stateChanged();
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "setEnding RC CallID: " + mRCallId + " PJSIP Call ID: " + mPJSIPcallId + " reason: "
                            + getEndingReasonAsString());
                }
            }
        }
    }
    
    /** 
     * Keeps call state 
     */
    private volatile int mState = RCSIP.PJSUA_CALL_STATE_CREATED; 
    
    /**
     * Returns current state: {@link RCSIP#PJSUA_CALL_STATE_CREATED} or
     * {@link RCSIP#PJSUA_CALL_STATE_INIT} or [
     * {@link RCSIP#PJSUA_CALL_STATE_INCOMING} or
     * {@link RCSIP#PJSUA_CALL_STATE_CALLING}] or
     * {@link RCSIP#PJSUA_CALL_STATE_EARLY}] or
     * {@link RCSIP#PJSUA_CALL_STATE_CONNECTING} or
     * {@link RCSIP#PJSUA_CALL_STATE_CONFIRMED} or
     * {@link RCSIP#PJSUA_CALL_STATE_DISCONNECTED}
     * 
     * @return current state.
     */
    public int getState() {
        synchronized (mLock) {
            return mState;
        }
    }
    
    /**
     * Keeps latest call status
     */
    private volatile SipCode mLastStatus = SipCode.PJSIP_SC_OK;
    
    public SipCode getLastSatus() {
        return mLastStatus;
    }
    
    /**
     * Keeps main error reason
     */
    private volatile SipCode mMainError = SipCode.PJSIP_SC_UNKNOWN_STATUS_CODE_EXTENDED;
    
    /** 
     * Defines contact without photo.
     */
    public static final long ID_NO_PHOTO = -1;
    
    /**
     * Keeps RC VoIP call identifier.
     */
	private volatile long mRCallId = PJSIP.sf_INVALID_RC_CALL_ID;
	
	/**
	 * Keeps formatter for logging times.
	 */
	private final static SimpleDateFormat formatter = new SimpleDateFormat("MM-dd HH:mm:ss.SSS");
    
    /**
     * Call ID received from PJSIP 
     */
    private int mPJSIPcallId = PJSIP.sf_INVALID_CALL_ID;
    
    /**
     * Keeps network type when the call has been initiated.
     */
    private int m_networkType = -1; //Will be used constants defined on ConnectivityManager
    private int m_radioNetworkType = -1;
    
    /**
     * Defines if the call incoming.
     */
    private boolean m_incoming  = false;
    
    /**
     * Keeps time when call has been created (elapsed) in ms
     */
    private long m_timeCreation   = 0;
    
    /**
     * Keeps time when call has been market as ending (elapsed) in ms
     */
    private long m_timeEnding   = 0;
    
    
    /**
     * Keeps time when call has been initiated (elapsed) in ms
     */
    private long m_timeInitiated    = 0;
    
    /**
     * Keeps time when call has been confirmed (elapsed) in ms
     */
    private long m_timeConfirmed = 0;
    
    /**
     * Keeps time when call has been disconnected (elapsed) in ms
     */
    private long m_timeDisconnected = 0;
    
    /**
     * Keeps time when call has been turned to CALLING state (elapsed) in ms
     */
    private long m_timeCalling = 0;
    
    /**
     * Keeps time when call has been turned to EARLY state (elapsed) in ms
     */
    private long m_timeEarly = 0;
    
    /**
     * Keeps time when call has been turned to CONNECTING state (elapsed) in ms
     */
    private long m_timeConnecting = 0;
    
    /**
     * Keeps time when call has been deleted (elapsed) in ms
     */
    private long m_deletedEnded        = 0;
    
    /**
     * Keeps instance state sequence number for synchronization.
     */
    private volatile int mStateSeqNum = 0;
    
    /**
     * Keeps latest state sequence number when the call clients (SIProxy on application side) was notified
     * 
     * See {@link #mStateSeqNum}
     * 
     */
    private volatile int mLastStateSeqNumClientNotification = 0;
    
    /**
     * Returns latest state sequence number when the call clients (SIProxy on
     * application side) was notified
     * 
     * @return latest state sequence number when the call clients (SIProxy on
     *         application side) was notified
     */
    public int getLastStateSeqNumClientNotification() {
        synchronized (mLock) {
            return mLastStateSeqNumClientNotification;
        }
    }
    
    /**
     * Sets latest state sequence number when the call clients (SIProxy on
     * application side) was notified
     */
    void setLastStateSeqNumClientNotification() {
        synchronized (mLock) {
            mLastStateSeqNumClientNotification = mStateSeqNum;
        }
    }
    
    /**
     * Defines if needed notification client about state/info change
     * @return
     */
    boolean needClientNotification() {
        synchronized (mLock) {
            return mLastStateSeqNumClientNotification != mStateSeqNum;
        }
    }
    
    /**
     * If state has been changed.
     */
    private void stateChanged() {
        synchronized (mLock) {
            if (mStateSeqNum >= Integer.MAX_VALUE) {
                mStateSeqNum = 0;
            }
            mStateSeqNum++;
        }
    }
    
    /**
     * Returns state sequence number be used for synchronization.
     * @return
     */
    public int getStateSeqNumber() {
        synchronized (mLock) {
            return mStateSeqNum;
        }
    }
    
    
    private String m_party_uri = null;
    
    /**
     * Keeps party's number
     */
    private String m_party_number = null;
    
    private String m_contact = null;        
    
    private String m_rcFrom = null;
    private String m_binded_contact = null;
    
    /**
     * Keeps photo identifier 
     */
    private volatile Long m_photo_id = ID_NO_PHOTO;
    
    /**
     * Keeps PJSIP call dump after the call disconnection. 
     */
    private volatile String mEndCallDump = null;
    
    /**
     * Keeps lock for sync operations.
     */
    private Object mLock = new Object();
    
    private boolean m_makeCallDelayed = false;
    private boolean m_callAnswered = false;
    
    /**
     * Current media state.
     */
    private int  m_media_state   = RCSIP.PJSUA_CALL_MEDIA_NONE;
    
    /**
     * Defines if was in active state
     */
    private boolean m_hadActiveMedia = false;
    
    /**
     * Returns media state.
     * 
     * @return media state {@link RCSIP#PJSUA_CALL_MEDIA_NONE},
     *         {@link RCSIP#PJSUA_CALL_MEDIA_ACTIVE},
     *         {@link RCSIP#PJSUA_CALL_MEDIA_ERROR},
     *         {@link RCSIP#PJSUA_CALL_MEDIA_LOCAL_HOLD},
     *         {@link RCSIP#PJSUA_CALL_MEDIA_REMOTE_HOLD}
     */
    public int getMediaState() {
        return m_media_state;
    }
    
    /**
     * Sets media state.
     * 
     * @param media_state
     *            the media state to be set
     *            {@link RCSIP#PJSUA_CALL_MEDIA_NONE},
     *            {@link RCSIP#PJSUA_CALL_MEDIA_ACTIVE},
     *            {@link RCSIP#PJSUA_CALL_MEDIA_ERROR},
     *            {@link RCSIP#PJSUA_CALL_MEDIA_LOCAL_HOLD},
     *            {@link RCSIP#PJSUA_CALL_MEDIA_REMOTE_HOLD}
     */
    public void setMediaState(int media_state) {
        switch (media_state) {
        case (RCSIP.PJSUA_CALL_MEDIA_NONE):
        case (RCSIP.PJSUA_CALL_MEDIA_ACTIVE):
        case (RCSIP.PJSUA_CALL_MEDIA_ERROR):
        case (RCSIP.PJSUA_CALL_MEDIA_LOCAL_HOLD):
        case (RCSIP.PJSUA_CALL_MEDIA_REMOTE_HOLD):
            break;
        default:
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "setMediaState for RC CallId: " + mRCallId + " as " + media_state);
            }
            return;
        }
        synchronized ( mLock ) {
            if (m_media_state != media_state) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "setMediaState for RC CallId: " + mRCallId + " " + 
                            RCSIP.getMediaStatusLabel(m_media_state) + "->" + RCSIP.getMediaStatusLabel(media_state));
                }
                m_media_state = media_state;
                if (m_media_state == RCSIP.PJSUA_CALL_MEDIA_ACTIVE) {
                    m_hadActiveMedia = true;
                }
                stateChanged();
            }
        }
    }
    
    private int m_ringer_mode = -1;
    
    /**
     * Sets ringer mode for particular incoming call.
     * 
     * @param ringer_mode
     *            the ringer mode to be set
     *            {@link AudioManager#RINGER_MODE_NORMAL},
     *            {@link AudioManager#RINGER_MODE_SILENT},
     *            {@link AudioManager#RINGER_MODE_VIBRATE},
     *            {@link IncomingCallNotification#RINGER_MODE_SECOND_CALL}
     */
    public void setRingerMode(int ringer_mode) {
    	synchronized (mLock) {
    		this.m_ringer_mode = ringer_mode;
		}		
	}
    
    private int m_ringer_volume = -1;

    /**
     * Sets ringer volume for particular incoming call.
     * 
     * @param ringer_mode
     *            the volume for {@link AudioManager#STREAM_RING} stream
     */
    public void setRingerVolume(int ringer_volume) {
    	synchronized (mLock) {
    		this.m_ringer_volume = ringer_volume;			
		}
    }

	private volatile String mCallerID; 
    
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        synchronized (mLock) {
            dest.writeLong(mRCallId);
            dest.writeInt(mPJSIPcallId);
            dest.writeInt(m_networkType);
            dest.writeInt(m_radioNetworkType);
            dest.writeInt(mState);
            dest.writeInt(mLastStatus.code());
            dest.writeInt(mMainError.code());
            dest.writeInt(m_media_state);
            dest.writeInt((m_hadActiveMedia ? 1 : 0));
            dest.writeInt((mPJSIPIdUnbound ? 1 : 0));
            dest.writeInt(mStateSeqNum);
            
            dest.writeInt((m_incoming ? 1 : 0));
            dest.writeInt(mCallEnded);
            dest.writeString(mCallerID);
            
            
            dest.writeLong(m_timeCreation);
            dest.writeLong(m_timeInitiated);

            dest.writeLong(m_timeConfirmed);
            dest.writeLong(m_timeDisconnected);
            dest.writeLong(m_deletedEnded);            
            dest.writeLong(m_timeCalling);
            dest.writeLong(m_timeEarly);
            dest.writeLong(m_timeConnecting);
            dest.writeLong(m_timeEnding);
            

            dest.writeString(m_party_uri);
            dest.writeString(m_party_number);
            dest.writeString(m_contact);
            dest.writeString(m_rcFrom);
            dest.writeString(m_binded_contact);

            dest.writeLong(m_photo_id);
            dest.writeInt(m_ringer_mode);
            dest.writeInt(m_ringer_volume);

            dest.writeString(mEndCallDump);

            dest.writeInt((m_makeCallDelayed ? 1 : 0));
            dest.writeInt((m_callAnswered ? 1 : 0));
        }
    }
		
    @Override
    public int describeContents() {
        return 0;
    }
    
    /**
     * Reading serialized data.
     * 
     * @param in the parser with serialized data.
     */
    public void readFromParcel(Parcel in) {
        synchronized (mLock) {
            mRCallId = in.readLong();
            mPJSIPcallId = in.readInt();
            m_networkType = in.readInt();
            m_radioNetworkType = in.readInt();
            mState = in.readInt();
            mLastStatus = RCSIP.SipCode.get(in.readInt());
            mMainError = RCSIP.SipCode.get(in.readInt());
            m_media_state = in.readInt();
            m_hadActiveMedia = (0 == in.readInt() ? false : true);
            mPJSIPIdUnbound = (0 == in.readInt() ? false : true);
            mStateSeqNum = in.readInt();
            
            m_incoming = (0 == in.readInt() ? false : true);
            mCallEnded = in.readInt();
            mCallerID = in.readString();
            
            m_timeCreation = in.readLong();
            m_timeInitiated = in.readLong();

            m_timeConfirmed = in.readLong();
            m_timeDisconnected = in.readLong();
            m_deletedEnded = in.readLong();
            m_timeCalling = in.readLong();
            m_timeEarly = in.readLong();
            m_timeConnecting = in.readLong();
            m_timeEnding = in.readLong();
            
            m_party_uri = in.readString();
            m_party_number = in.readString();
            m_contact = in.readString();
            m_rcFrom = in.readString();
            m_binded_contact = in.readString();

            m_photo_id = in.readLong();
            m_ringer_mode = in.readInt();
            m_ringer_volume = in.readInt();

            mEndCallDump = in.readString();
            
            m_makeCallDelayed = (0 == in.readInt() ? false : true);
            m_callAnswered = (0 == in.readInt() ? false : true);
        }
    }
    
    /**
     * Sets main error.
     * 
     * @param error main error to be set.
     */
    public final void setMainError(SipCode error) {
        synchronized (mLock) {
            if (error == null) {
                return;
            }
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "setMainError for RC CallId: " + mRCallId + " as " + error.msg());
            }
            
            mMainError = error;
            
            if (mMainError.code() > RCSIP.SipCode.PJSIP_SC_UNKNOWN_STATUS_CODE_EXTENDED.code()) {
                mLastStatus = error;
            }
        }
    }
    
    /** 
     *  Generates instances of your Parcelable class from a Parcel
     */
    public static final Parcelable.Creator<RCCallInfo> CREATOR = new Parcelable.Creator<RCCallInfo>() {
        
        public RCCallInfo createFromParcel( Parcel in ){
            return new RCCallInfo(in);
        }
        
        public RCCallInfo[] newArray( int size){
            return new RCCallInfo[size];
        }
    };
    
    /**
     * Constructor from serialized data.
     * 
     * @param in the parcel with serialized data
     */
    private RCCallInfo(Parcel in) {
        readFromParcel(in);
    }
    
    /**
     * Defines if the call is alive not in {@link RCSIP#PJSUA_CALL_STATE_DISCONNECTED}
     * @return
     */
    public boolean isAlive() {
        synchronized (mLock) {
            return mState != RCSIP.PJSUA_CALL_STATE_DISCONNECTED;
        }
    }
    
    @Override
    public String toString() {
        synchronized (mLock) {
            StringBuffer sb = new StringBuffer("Call: RC/PJSIP Ids: ");
            sb.append(mRCallId); sb.append('/'); sb.append(mPJSIPcallId);
            
            if (mPJSIPIdUnbound) {
                sb.append("(unbound)");
            }
            
            if (m_incoming) {
                sb.append(" Inbound:");
            } else {
                sb.append(" Outbound:");
            }
            sb.append(normalizedStringForDump(m_party_number));
            sb.append(" State:"); sb.append(RCSIP.getCallStateForLog(mState));
            if (isEnding()) {
                sb.append(" Ending("); sb.append(getEndingReasonAsString());sb.append(")");
            }
            sb.append(" Time:");
            if (isAlive()) {
                sb.append(getTime() - m_timeCreation);
            } else {
                sb.append(normalizedStringForElapsedTimeDump(m_timeCreation, m_timeDisconnected));
            }
            
            sb.append(" Media:");sb.append(RCSIP.getMediaStatusLabel(m_media_state));
            
            
            sb.append(" LastStatus: "); sb.append(mLastStatus.msg());
            
            return sb.toString();
        }
    }
    
    /**
     * Returns call dump for tracing.
     * 
     * @return call dump for tracing.
     */
    public String dump() {
        synchronized (mLock) {
            StringBuffer sb = new StringBuffer("VoIP Call: ");
            sb.append(formatter.format(new Date(System.currentTimeMillis() - (SystemClock.elapsedRealtime() - m_timeCreation))));

            sb.append('\n');sb.append("  RC/PJSIP Ids: "); sb.append(mRCallId); sb.append('/'); sb.append(mPJSIPcallId);
            if (mPJSIPIdUnbound) {
                sb.append("(unbound)");
            }
            if (m_incoming) {
                sb.append("    Inbound:");
            } else {
                sb.append("    Outbound:");
            }
            sb.append(normalizedStringForDump(m_party_number));
            if (!m_incoming) {
                sb.append('\n');sb.append("    CallerID: ");sb.append((mCallerID == null ? "NULL" : mCallerID));
            }
            sb.append('\n');sb.append("    State: "); sb.append(RCSIP.getCallStateForLog(mState));
            sb.append('\n');sb.append("    Last Status: "); sb.append(mLastStatus.msg());
            
            sb.append('\n');sb.append("    Media:");sb.append(RCSIP.getMediaStatusLabel(m_media_state));
            sb.append(" HadMedia:");sb.append(m_hadActiveMedia);
            
            
            if (mMainError != null && (mMainError.code() != SipCode.PJSIP_SC_UNKNOWN_STATUS_CODE_EXTENDED.code())) {
                sb.append('\n');sb.append("    Main error: "); sb.append(mMainError.msg());
            }
            
            if (!isAlive()) {
                sb.append('\n');sb.append("    Ending:");sb.append(getEndingReasonAsString());
            }
            sb.append('\n');sb.append("    Network: ");sb.append(NetworkUtils.getNetworkTypeLabel(m_networkType));
            sb.append('\n');sb.append("    Radio  : ");sb.append(NetworkUtils.getRadioNetworkTypeLabel(m_radioNetworkType));
            sb.append('\n');
            sb.append("    Total time: ");
            if (!isAlive()) {
                if (m_deletedEnded == 0) {
                    sb.append(m_deletedEnded - m_timeCreation);
                } else {
                    sb.append(m_timeDisconnected - m_timeCreation);
                }
            } else {
                sb.append(SystemClock.elapsedRealtime() - m_timeCreation);
            }
            if (isEnding()) {
                sb.append('\n');
                sb.append("      Ending at: ");
                sb.append(normalizedStringForElapsedTimeDump(m_timeCreation, m_timeEnding));
            }
            
            sb.append('\n');
            sb.append("      Initiated at: ");
            sb.append(normalizedStringForElapsedTimeDump(m_timeCreation, m_timeInitiated));
            
            if (!m_incoming) {
                sb.append(" Calling at: ");
                sb.append(normalizedStringForElapsedTimeDump(m_timeCreation, m_timeCalling));
            }

            sb.append('\n');
            sb.append("      Early at: ");
            sb.append(normalizedStringForElapsedTimeDump(m_timeCreation, m_timeEarly));
            sb.append(" Connecting at: ");
            sb.append(normalizedStringForElapsedTimeDump(m_timeCreation, m_timeConnecting));
            
            sb.append('\n');
            sb.append("      Confirmed at: ");
            sb.append(normalizedStringForElapsedTimeDump(m_timeCreation, m_timeConfirmed));
            
            if (!isAlive()) {
                sb.append(" Disconnected at: ");
                sb.append(normalizedStringForElapsedTimeDump(m_timeCreation, m_timeDisconnected));
            }
            
            sb.append('\n');
            sb.append("    PartyURI: ");
            sb.append(normalizedStringForDump(m_party_uri));

            sb.append('\n');
            sb.append("    From: ");
            sb.append(normalizedStringForDump(m_rcFrom));

            sb.append('\n');
            sb.append("    Contact: ");
            sb.append(normalizedStringForDump(m_contact));

            sb.append('\n');
            sb.append("    BoundContact: ");
            sb.append(normalizedStringForDump(m_binded_contact));

            sb.append('\n');
            sb.append("    PhotoId: ");
            sb.append(m_photo_id);
            
            if(m_incoming) {
            	sb.append('\n');
            	sb.append("    Notification type :").append(IncomingCallNotification.getRingerModeAsString(m_ringer_mode));
            	if(m_ringer_volume > 0) {
            		sb.append(", volume:").append(m_ringer_volume);
            	}
            }

            sb.append('\n');
            sb.append("    Call delayed: ");sb.append(m_makeCallDelayed);sb.append(" Call answered: ");sb.append(m_callAnswered);

            sb.append('\n');
            if (mEndCallDump != null) {
                sb.append("    Dump: ");
                sb.append(mEndCallDump);
                sb.append('\n');
            }
            return sb.toString();
        }
    }
    
    /**
     * Returns normalized string for elapsed time logging.
     */
    private static String normalizedStringForElapsedTimeDump(long start, long end) {
        if (end == 0) {
            return "?";
        } else {
            return (end - start) + " ms";
        }
    }

    
    /**
     * Returns normalized string for logging.
     */
    private static String normalizedStringForDump(String string) {
        if (string == null) {
            return "null";
        } else {
            return string;
        }
    }

	public String getRcFrom() {
		return m_rcFrom;
	}

	public void setRcFrom(String rcFrom) {
	    synchronized (mLock) {
	        if (m_rcFrom == null) {
	            if (rcFrom != null) {
	                stateChanged();
	            }
	        } else if (rcFrom == null) {
	            stateChanged();
	        } else if (!m_rcFrom.equals(rcFrom)) {
	            stateChanged();
	        }
	        m_rcFrom = rcFrom;
	    }
	}

	public String getBindedContact() {
		return m_binded_contact;
	}

    public void setBindedContact(String binded_contact) {
        synchronized (mLock) {
            if (m_binded_contact == null) {
                if (binded_contact != null) {
                    stateChanged();
                }
            } else if (binded_contact == null) {
                stateChanged();
            } else if (!m_binded_contact.equals(binded_contact)) {
                stateChanged();
            }
            m_binded_contact = binded_contact;
        }
    }

	/**
	 * Returns Photo identifier for the contact, set by {@link #setPhotoID(Long)}
	 * 
	 * @return the photo identifier or {@link #ID_NO_PHOTO}
	 */
	public Long getPhotoID() {
		return m_photo_id;
	}

	/**
	 * Sets photo identifier for the contact.
	 * 
	 * @param photo_id the photo identifier
	 */
	public void setPhotoID(Long photo_id) {
	    synchronized (mLock) {
	        if (m_photo_id != photo_id) {
	            m_photo_id = photo_id;
	            stateChanged();
	        }
	    }
	}
	
	/**
	 * Return RC Call identifier.
	 * @return call identifier
	 */
	public long getRcCallId() {
		return mRCallId;
	}

    /**  
     * Returns PJSIP Call identifier
     * 
     * @return PJSIP Call identifier or PJSIP#sf_INVALID_CALL_ID
     */
	public int getPJSIPCallId() {
		return mPJSIPcallId;
	}

	/**
	 * Sets PJSIP call identifier
	 * 
	 * @param pjsipCallId call identifier to be set
	 */
    public void setPJSIPCallId(int pjsipCallId) {
        synchronized (mLock) {
            if (mPJSIPcallId == pjsipCallId) {
                return;
            } else if (mPJSIPcallId != PJSIP.sf_INVALID_CALL_ID) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "setPJSIPCallId : previous CallId: " + mPJSIPcallId + " trying to set : " + pjsipCallId);
                }
            }

            if (pjsipCallId == PJSIP.sf_INVALID_CALL_ID) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "setPJSIPCallId : setting INVALID identifier for CallId: " + mPJSIPcallId);
                }
            }
            mPJSIPcallId = pjsipCallId;
            stateChanged();
        }
    }

    /**
     * Returns network type when the call has been initiated.
     * 
     * @return network type when the call has been initiated.
     */
	public int getNetworkType() {
		return m_networkType;
	}

	public void setNetworkType(int networkType) {
	    synchronized (mLock) {
	        if (m_networkType != networkType) {
	            stateChanged();
	        }
	        m_networkType = networkType;
	    }
	}
    public void setRadioNetworkType(int networkType) {
        synchronized (mLock) {
            if (m_radioNetworkType != networkType) {
                stateChanged();
            }
            m_radioNetworkType = networkType;
        }
    }

	/**
	 * Defines if it is incoming call.
	 * 
	 * @return is this incoming call, otherwise outgoing
	 */
	public boolean isIncoming() {
		return m_incoming;
	}

	public long getCreationTime() {
		return m_timeCreation;
	}

    private void setCreationTime(long timeInitiating) {
        synchronized (mLock) {
            m_timeCreation = timeInitiating;
        }
    }

	/**
	 * Returns time when the call was turned to RCSIP#PJSUA_CALL_STATE_INIT state.
	 * 
	 * @return
	 */
	public long getTimeInitiated() {
		return m_timeInitiated;
	}

	/**
	 * Turns the call into INIT state, defining that work with PJSIP is started
	 */
	public void onInitiatedState() {
        synchronized (mLock) {
            if (mState != RCSIP.PJSUA_CALL_STATE_CREATED) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "onInitiatedState : state error : " + "RC CallId: " + mRCallId + " current state : "
                            + RCSIP.getCallStateForLog(mState));
                }
                return;
            }
            mState = RCSIP.PJSUA_CALL_STATE_INIT;
            stateChanged();
            m_timeInitiated = getTime();
        }
	}
	
    /**
     * Change call state.
     * 
     * @param async
     *            <code>true</code> defines if this is PJSIP callback,
     *            <code>true</code> defines this is synchronous set, for
     *            example, just after receiving state from PJSIP stack by sync
     *            call.
     * @param state the state to turn to
     * @param last_status lats status
     * @param dump end call dump (for {@link RCSIP#PJSUA_CALL_STATE_DISCONNECTED} state
     */
    public void onCallStatusChange(boolean async, int state, int last_status, String dump) {
        if (!isValidStateValue(state)) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "onCallStatusChange:" + "RC CallId:" + mRCallId + ": Not valid state value:" + state);
            }
            return;
        }

        synchronized (mLock) {
            int prevState = mState;
            _onCallStatusChange(async, state, last_status, dump);
            if (prevState != mState) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "onCallStatusChange:" + "RC CallId:" + mRCallId + ": " 
                            + RCSIP.getCallStateForLog(prevState)  + "->" 
                            + RCSIP.getCallStateForLog(mState));
                }
            }
        }
    }
	private void _onCallStatusChange(boolean async, int state, int last_status, String dump) {
	        if (LogSettings.MARKET) {
	            MktLog.d(TAG, "onCallStatusChange:" + "RC CallId:" + mRCallId + ": TRY (" 
	                    + RCSIP.getCallStateForLog(mState)  + "->" 
	                    + RCSIP.getCallStateForLog(state) + ") async:" + async);
	        }
	        
	        if (mMainError.code() > RCSIP.SipCode.PJSIP_SC_UNKNOWN_STATUS_CODE_EXTENDED.code()) {
	            if (mLastStatus !=  mMainError) {
	                mLastStatus = mMainError;
	            }
	            last_status = mLastStatus.code();
	        }
	        
	        /**
	         * To DISCONNECTED state. 
	         */
	        if (state ==  RCSIP.PJSUA_CALL_STATE_DISCONNECTED) {
	            if (mState == RCSIP.PJSUA_CALL_STATE_DISCONNECTED) {
	                if (async) {
	                    if (dump != null && mEndCallDump == null) {
	                        mEndCallDump = dump;
	                    }
	                    return;
	                }
	                if (LogSettings.MARKET) {
	                    MktLog.w(TAG, "onDisconnectState : already in disconnected state : " + "RC CallId: " + mRCallId);
	                }
	                return;
	            }
	            if (!isEnding()) {
	                setEnding(RCCallInfo.CALL_ENDED_BY_SYSTEM);
	            }
	            mState = RCSIP.PJSUA_CALL_STATE_DISCONNECTED;
	            m_timeDisconnected = getTime();
	            mLastStatus = RCSIP.SipCode.get(last_status);
	            stateChanged();
	            if (dump != null && mEndCallDump == null) {
	                mEndCallDump = dump;
	            }
	            return;
	        } 
	        
            /**
             * To CALLING state.
             */
            if (state == RCSIP.PJSUA_CALL_STATE_CALLING) {
                if (m_incoming) {
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : Inbound, but CALLING state received.");
                    }
                    return;
                }

                switch (mState) {
                case (RCSIP.PJSUA_CALL_STATE_DISCONNECTED):
                    if (m_timeCalling == 0) m_timeCalling = getTime();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_INIT):
                    mState = RCSIP.PJSUA_CALL_STATE_CALLING;
                    m_timeCalling = getTime();
                    mLastStatus = RCSIP.SipCode.get(last_status);
                    stateChanged();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CALLING):
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : is already in CALLING state");
                    }
                    if (!async) {
                        if (mLastStatus.code() != last_status) {
                            mLastStatus = RCSIP.SipCode.get(last_status);
                            stateChanged();
                        }
                    }
                    return;
                case (RCSIP.PJSUA_CALL_STATE_INCOMING):
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : INCOMING->CALLING");
                    }
                    return;
                case (RCSIP.PJSUA_CALL_STATE_EARLY):
                    if (!async) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : EARLY->CALLING");
                        }
                        return;
                    }
                    if (m_timeCalling == 0) m_timeCalling = m_timeEarly;
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CONNECTING):
                    if (!async) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : CONNECTING->CALLING");
                        }
                        return;
                    }
                    if (m_timeEarly == 0) m_timeEarly = m_timeConnecting;
                    if (m_timeCalling == 0) m_timeCalling = m_timeEarly;
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CONFIRMED):
                    if (!async) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : CONFIRMED->CALLING");
                        }
                        return;
                    }
                    if (m_timeConnecting == 0) m_timeConnecting = m_timeConfirmed;
                    if (m_timeEarly == 0) m_timeEarly = m_timeConnecting;
                    if (m_timeCalling == 0) m_timeCalling = m_timeEarly;
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CREATED):
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : CREATED->CALLING");
                    }
                    m_timeInitiated = getTime();
                    mState = RCSIP.PJSUA_CALL_STATE_CALLING;
                    m_timeCalling = m_timeInitiated;
                    stateChanged();
                    return;
                default:
                    return;
                }
            }
            
            /**
             * To EARLY state.
             */
            if (state == RCSIP.PJSUA_CALL_STATE_EARLY) {
                switch (mState) {
                case (RCSIP.PJSUA_CALL_STATE_DISCONNECTED):
                    if (m_timeEarly == 0) m_timeEarly = getTime();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_INIT):
                    if (async) {
                        if (LogSettings.MARKET) {
                            MktLog.w(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : Async INIT->EARLY wo CALLING");
                        }
                    }
                    mState = RCSIP.PJSUA_CALL_STATE_EARLY;
                    m_timeEarly = getTime();
                    if (mLastStatus.code() != last_status) {
                        mLastStatus = RCSIP.SipCode.get(last_status);
                    }
                    if (m_timeCalling == 0) m_timeCalling = m_timeEarly;
                    stateChanged();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CALLING):
                case (RCSIP.PJSUA_CALL_STATE_INCOMING):
                    mState = RCSIP.PJSUA_CALL_STATE_EARLY;
                    m_timeEarly = getTime();
                    if (mLastStatus.code() != last_status) {
                        mLastStatus = RCSIP.SipCode.get(last_status);
                    }
                    stateChanged();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_EARLY):
                    if (!async) {
                        if (mLastStatus.code() != last_status) {
                            mLastStatus = RCSIP.SipCode.get(last_status);
                            stateChanged();
                        }
                        return;
                    }
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CONNECTING):
                    if (!async) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : CONNECTING->EARLY");
                        }
                        return;
                    }
                    if (m_timeEarly == 0) m_timeEarly = m_timeConnecting;
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CONFIRMED):
                    if (!async) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : CONFIRMED->EARLY");
                        }
                        return;
                    }
                    if (m_timeConnecting == 0) m_timeConnecting = m_timeConfirmed;
                    if (m_timeEarly == 0) m_timeEarly = m_timeConnecting;
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CREATED):
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : CREATED->EARLY");
                    }
                    m_timeInitiated = getTime();
                    mState = RCSIP.PJSUA_CALL_STATE_EARLY;
                    m_timeCalling = m_timeInitiated;
                    m_timeEarly = m_timeInitiated;
                    if (mLastStatus.code() != last_status) {
                        mLastStatus = RCSIP.SipCode.get(last_status);
                    }
                    stateChanged();
                    return;
                default:
                    return;
                }
            }
            
            /**
             * To CONNECTING state.
             */
            if (state == RCSIP.PJSUA_CALL_STATE_CONNECTING) {
                switch (mState) {
                case (RCSIP.PJSUA_CALL_STATE_DISCONNECTED):
                    if (m_timeConnecting == 0) m_timeConnecting = getTime();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_INIT):
                    if (async) {
                        if (LogSettings.MARKET) {
                            MktLog.w(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : Async INIT->CONNECTING wo CALLING/EARLY");
                        }
                    }
                    mState = RCSIP.PJSUA_CALL_STATE_CONNECTING;
                    m_timeConnecting = getTime();
                    if (m_timeCalling == 0) m_timeCalling = m_timeConnecting;
                    if (m_timeEarly == 0) m_timeEarly = m_timeCalling;
                    if (mLastStatus.code() != last_status) {
                        mLastStatus = RCSIP.SipCode.get(last_status);
                    }
                    stateChanged();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CALLING):
                    if (async) {
                        if (LogSettings.MARKET) {
                            MktLog.w(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : Async CALLING->CONNECTING wo EARLY");
                        }
                    }
                    mState = RCSIP.PJSUA_CALL_STATE_CONNECTING;
                    m_timeConnecting = getTime();
                    if (mLastStatus.code() != last_status) {
                        mLastStatus = RCSIP.SipCode.get(last_status);
                    }
                    if (m_timeEarly == 0) m_timeEarly = m_timeCalling;
                    stateChanged();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_INCOMING):
                    if (async) {
                        if (LogSettings.MARKET) {
                            MktLog.w(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : Async INCOMING->CONNECTING wo EARLY");
                        }
                    }
                    mState = RCSIP.PJSUA_CALL_STATE_CONNECTING;
                    m_timeConnecting = getTime();
                    if (m_timeEarly == 0) m_timeEarly = m_timeInitiated;
                    if (mLastStatus.code() != last_status) {
                        mLastStatus = RCSIP.SipCode.get(last_status);
                    }
                    stateChanged();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_EARLY):
                    mState = RCSIP.PJSUA_CALL_STATE_CONNECTING;
                    m_timeConnecting = getTime();
                    if (mLastStatus.code() != last_status) {
                        mLastStatus = RCSIP.SipCode.get(last_status);
                    }
                    stateChanged();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CONNECTING):
                    if (!async) {
                        if (mLastStatus.code() != last_status) {
                            mLastStatus = RCSIP.SipCode.get(last_status);
                            stateChanged();
                        }
                        return;
                    }
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CONFIRMED):
                    if (!async) {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : CONFIRMED->CONNECTING");
                        }
                        return;
                    }
                    if (m_timeConnecting == 0) m_timeConnecting = m_timeConfirmed;
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CREATED):
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : CREATED->CONNECTING");
                    }
                    m_timeInitiated = getTime();
                    mState = RCSIP.PJSUA_CALL_STATE_CONNECTING;
                    m_timeCalling = m_timeInitiated;
                    m_timeEarly = m_timeInitiated;
                    m_timeConnecting = m_timeInitiated;
                    if (mLastStatus.code() != last_status) {
                        mLastStatus = RCSIP.SipCode.get(last_status);
                    }
                    stateChanged();
                    return;
                default:
                    return;
                }
            }
            
            /**
             * To CONFIRMED state.
             */
            if (state == RCSIP.PJSUA_CALL_STATE_CONFIRMED) {
                switch (mState) {
                case (RCSIP.PJSUA_CALL_STATE_DISCONNECTED):
                    if (m_timeConfirmed == 0) m_timeConfirmed = getTime();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_INIT):
                    if (async) {
                        if (LogSettings.MARKET) {
                            MktLog.w(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : Async INIT->CONFIRMED wo CALLING/EARLY/CONNECTING");
                        }
                    }
                    m_timeConfirmed = getTime();
                    mState = RCSIP.PJSUA_CALL_STATE_CONFIRMED;
                    if (m_timeConnecting == 0) m_timeConnecting = m_timeConfirmed;
                    if (m_timeCalling == 0) m_timeCalling = m_timeConnecting;
                    if (m_timeEarly == 0) m_timeEarly = m_timeCalling;
                    if (mLastStatus.code() != last_status) {
                        mLastStatus = RCSIP.SipCode.get(last_status);
                    }
                    stateChanged();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CALLING):
                    if (async) {
                        if (LogSettings.MARKET) {
                            MktLog.w(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : Async CALLING->CONFIRMED wo EARLY/CONNECTING");
                        }
                    }
                    mState = RCSIP.PJSUA_CALL_STATE_CONFIRMED;
                    m_timeConfirmed = getTime();
                    if (m_timeConnecting == 0) m_timeConnecting = m_timeConfirmed;
                    if (m_timeEarly == 0) m_timeEarly = m_timeCalling;
                    if (mLastStatus.code() != last_status) {
                        mLastStatus = RCSIP.SipCode.get(last_status);
                    }
                    stateChanged();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_INCOMING):
                    if (async) {
                        if (LogSettings.MARKET) {
                            MktLog.w(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : Async INCOMING->CONFIRMED wo EARLY/CONNECTING");
                        }
                    }
                    mState = RCSIP.PJSUA_CALL_STATE_CONFIRMED;
                    m_timeConfirmed = getTime();
                    if (m_timeConnecting == 0) m_timeConnecting = m_timeInitiated;
                    if (m_timeEarly == 0) m_timeEarly = m_timeConnecting;
                    if (mLastStatus.code() != last_status) {
                        mLastStatus = RCSIP.SipCode.get(last_status);
                    }
                    stateChanged();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_EARLY):
                    if (async) {
                        if (LogSettings.MARKET) {
                            MktLog.w(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : Async EARLY->CONFIRMED wo CONNECTING");
                        }
                    }
                    mState = RCSIP.PJSUA_CALL_STATE_CONFIRMED;
                    m_timeConfirmed = getTime();
                    if (m_timeConnecting == 0) m_timeConnecting = m_timeEarly;
                    if (mLastStatus.code() != last_status) {
                        mLastStatus = RCSIP.SipCode.get(last_status);
                    }
                    stateChanged();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CONNECTING):
                    mState = RCSIP.PJSUA_CALL_STATE_CONFIRMED;
                    m_timeConfirmed = getTime();
                    if (mLastStatus.code() != last_status) {
                        mLastStatus = RCSIP.SipCode.get(last_status);
                    }
                    stateChanged();
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CONFIRMED):
                    if (!async) {
                        if (mLastStatus.code() != last_status) {
                            mLastStatus = RCSIP.SipCode.get(last_status);
                            stateChanged();
                        }
                        return;
                    }
                    return;
                case (RCSIP.PJSUA_CALL_STATE_CREATED):
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "onCallStatusChange : " + "RC CallId: " + mRCallId + " : CREATED->CONFIRMED");
                    }
                    m_timeInitiated = getTime();
                    mState = RCSIP.PJSUA_CALL_STATE_CONNECTING;
                    m_timeCalling = m_timeInitiated;
                    m_timeEarly = m_timeInitiated;
                    m_timeConnecting = m_timeInitiated;
                    m_timeConfirmed = m_timeInitiated;
                    if (mLastStatus.code() != last_status) {
                        mLastStatus = RCSIP.SipCode.get(last_status);
                    }
                    stateChanged();
                    return;
                default:
                    return;
                }
            }
            
            
            /**
             * To INCOMING state.
             */
            if (state == RCSIP.PJSUA_CALL_STATE_INCOMING) {
                switch (mState) {
                case (RCSIP.PJSUA_CALL_STATE_DISCONNECTED):
                    return;
                case (RCSIP.PJSUA_CALL_STATE_INCOMING):
                    if (mLastStatus.code() != last_status) {
                        mLastStatus = RCSIP.SipCode.get(last_status);
                        stateChanged();    
                    }
                    return;
                }
            }
            
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "onCallStatusChange:ERROR:" + "RC CallId:" + mRCallId + ": " 
                        + RCSIP.getCallStateForLog(mState)  + "->" 
                        + RCSIP.getCallStateForLog(state));
            }

	}
	
	/**
	 * Check if <code>state</code> is valid
	 * @param state the state to be checked
	 * @return check state validity result
	 */
	private boolean isValidStateValue(int state) {
	    switch (state) {
	    case (RCSIP.PJSUA_CALL_STATE_DISCONNECTED):
	    case (RCSIP.PJSUA_CALL_STATE_INIT):
	    case (RCSIP.PJSUA_CALL_STATE_CALLING):
	    case (RCSIP.PJSUA_CALL_STATE_INCOMING):
	    case (RCSIP.PJSUA_CALL_STATE_EARLY):
	    case (RCSIP.PJSUA_CALL_STATE_CONNECTING):
	    case (RCSIP.PJSUA_CALL_STATE_CONFIRMED):
	    case (RCSIP.PJSUA_CALL_STATE_CREATED):
	        return true;
	        default:
	            return false;
	    }
	}
	
	/**
	 * Get disconnection time.
	 * 
	 * @return disconnection time
	 */
	public long getDisconnectedTime() {
        return m_timeDisconnected;
    }
    
    /**
     * Get time when call turned to CALLING state.
     * 
     * @return CALLING time
     */
    public long getCallingTime() {
        return m_timeCalling;
    }

    /**
     * Get time when call turned to EARLY state.
     * 
     * @return EARLY time
     */
    public long getEarlyTime() {
        return m_timeEarly;
    }
    
    /**
     * Get time when call turned to CONNECTING state.
     * 
     * @return CONNECTING time
     */
    public long getConnectingTime() {
        return m_timeConnecting;
    }
    
    /**
     * Returns confirmation time (elapsed)
     * 
     * @return confirmation time (elapsed)
     */
    public long getConfirmationTime() {
        return m_timeConfirmed;
    }

    /**
     * Returns time when the call has been deleted.
     * 
     * @return time when the call has been deleted
     */
	public long getDeletedTime() {
		return m_deletedEnded;
	}

	/**
	 * Sets deletion time.
	 */
	public void setDeletedTime() {
	    synchronized (mLock) {
            if (m_deletedEnded == 0)
                m_deletedEnded = getTime();
	    }
	}

	public String getPartyUri() {
	    synchronized (mLock) {
	        return m_party_uri;
	    }
	}
	
	/**
	 * Returns party number.
	 * 
	 * @return party number
	 */
	public String getPartyNumber() {
	    synchronized (mLock) {
	        return m_party_number;
	    }
    }

	public void setPartyUri(String uri) {
        synchronized (mLock) {
            if (m_party_uri == null) {
                if (uri != null) {
                    stateChanged();
                }
            } else if (uri == null) {
                stateChanged();
            } else if (!m_party_uri.equals(uri)) {
                stateChanged();
            }
            m_party_uri = uri;
            m_party_number = CallInfo.getNumberFromURI(m_party_uri);
        }
	}

	public String getContact() {
		return m_contact;
	}

	public void setContact(String contact) {
        synchronized (mLock) {
            if (m_contact == null) {
                if (contact != null) {
                    stateChanged();
                }
            } else if (contact == null) {
                stateChanged();
            } else if (!m_contact.equals(contact)) {
                stateChanged();
            }
            m_contact = contact;
        }
	}

	public boolean isMakeCallDelayed() {
	    return m_makeCallDelayed;
	}
	
    public void setMakeCallDelayed(boolean delayed) {
        synchronized (mLock) {
            m_makeCallDelayed = delayed;
        }
    }
	
    public boolean isCallAnswered() {
        return m_callAnswered;
    }
    
    public void setCallAnswered(boolean answered) {
        synchronized (mLock) {
            m_callAnswered = answered;
        }
    }
    
	private static long getTime(){		
		return SystemClock.elapsedRealtime();
	}			
	
	
	/**
	 * Creates incoming call
	 * 
	 * @param RC Call ID
	 */
	public RCCallInfo( final long rcCallId, final int pjsipCallId, final String partyUri) {
	    mRCallId = rcCallId;
	    mPJSIPcallId = pjsipCallId;
	    m_incoming = true;
	    m_party_uri = partyUri;
	    m_party_number = CallInfo.getNumberFromURI(partyUri);
	    mState = RCSIP.PJSUA_CALL_STATE_INCOMING;
	    long time = getTime();
	    setCreationTime(time);
	    m_timeInitiated = time;
	    m_networkType = ServicePJSIP.getInstance().getNetworkType();
	    m_radioNetworkType = ServicePJSIP.getInstance().getRadioNetworkType();
	}

	/**
	 * @param RC Call ID
	 * @param number
	 * @param name
	 */
	public RCCallInfo( final long rcCallId, String number, String name ) {
		mRCallId = rcCallId;
        m_party_uri = number;
        m_party_number = number;
        m_contact = name;
        mCallerID = RCMProviderHelper.getCallerID(RingCentralApp.getContextRC());
        setCreationTime(getTime());
	}
	
	/**
	 * TODO : REVIEW
	 */
    public boolean isSipNotInitiatedState() {
        return (mPJSIPcallId == PJSIP.sf_INVALID_CALL_ID);
    }
    
    public void unBindPSIPCall() {
        synchronized (mLock) {
            mPJSIPIdUnbound = true;
        }
    }
    public boolean unBoundPSIPCall() {
        synchronized (mLock) {
            return mPJSIPIdUnbound;
        }
    }
    private volatile boolean mPJSIPIdUnbound = false;
}
