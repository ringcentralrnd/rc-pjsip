/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.LogItem;
import com.rcbase.android.logging.LoggingManager;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.logging.LoggingManager.LogsFinalizationReason;
import com.rcbase.android.sip.audio.AudioSetupEngine;
import com.rcbase.android.utils.execution.CommandProcessor.Command;
import com.rcbase.android.utils.media.AudioState;
import com.rcbase.android.utils.media.RCMediaManager;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.voip.VoipCallStatusActivity;

class ServicePJSIPbinder extends IServicePJSIP.Stub {
    private static final String TAG = ServicePJSIP.TAG;
    public static final String TAG_EMG = "[RC]Validation";
    private volatile ServicePJSIP service;

    @Override
    public void cleanupLogAndLatestFinishedCalls() {
        try {
            LoggingManager.cleanup();
        } catch (java.lang.Throwable th) {
        }
        getCallMng().cleanupLatestFinishedCalls();
    }

    @Override
    public void cleanupLog() {
        try {
            LoggingManager.cleanup();
        } catch (java.lang.Throwable th) {
        }
    }

    @Override
    public LogItem[] getCurrentLog(boolean cleanup) {
        try {
            return LoggingManager.getCurrentLog(cleanup);
        } catch (java.lang.Throwable th) {
        }
        return null;
    }

    @Override
    public int call_get_pjsip_count() {
        WrapPJSIP w = getWrapper();
        if (w != null) {
            return getWrapper().PJSIP_call_get_count();
        }
        return 0;
    }

    @Override
    public int getSipServicePID() {
        return android.os.Process.myPid();
    }

    @Override
    public void testMode_emualateCrash(long delay) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "testMode_emualateCrash in " + delay + "ms");
        }
        getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "testMode_emualateCrash");
                }
                testMode_emualateCrash();
            }
        }, delay);
    }

    private void testMode_emualateCrash() {
        throw new InternalError("SIP Service Emulation Crash");
    }

    @Override
    public RCCallInfo[] getLatestFinishedCalls() {
        try {
            return getCallMng().getLatestFinishedCalls();
        } catch (java.lang.Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "getLatestFinishedCalls error:" + th.toString());
            }
        }
        return null;
    }

    @Override
    public RCCallInfo[] getCurrentCalls() {
        try {
            return getCallMng().getCurrentCalls();
        } catch (java.lang.Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "getCurrentCalls error:" + th.toString());
            }
        }
        return null;
    }

    @Override
    public com.rcbase.android.sip.service.ServiceState getServiceState() {
        return getState().snapStatistics();
    }

    /**
     * register parameter is used for starting SIP registration (if needed)
     * after SIP stack start procedure. It is very important for case when
     * Incoming Calls is switched off.
     * 
     * @param testMode
     *            TRUE - start SIP stack on test mode. For Voice Wizard ONLY! On
     *            any other cases FALSE should be used
     * 
     *            (non-Javadoc)
     * @see com.rcbase.android.sip.service.IServicePJSIP#startSipStack(java.lang.String,
     *      boolean)
     */
    @Override
    public int startSipStack(boolean testmode) throws RemoteException {
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "startSipStack is starting... By client.");
        }
        // TODO:
        injectCommand(service.new StartPJSIPCommand(testmode));
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "startSipStack finished");
        }
        return PJSIP.sf_PJ_SUCCESS;
    }

    @Override
    public int stopSipStack() throws RemoteException {
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "stopSipStack started by client");
        }
        injectCommand(service.new StopPJSIP());
        return PJSIP.sf_PJ_SUCCESS;
    }

    @Override
    public void registerCallback(int hash, IServicePJSIPCallback callback) throws RemoteException {
        service.PJSIP_addCallback(hash, callback);
    }

    @Override
    public void unregisterCallback(int hash) throws RemoteException {
        service.PJSIP_removeCallback(hash);
    }

    @Override
    public void call_answer(final long rcCallId, final long delayMillis) throws RemoteException {
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "call_answer(): rcCallId = " + rcCallId + ", delay = " + delayMillis + " msec");
        }

        if (delayMillis > 0) {
            Message msg = getHandler().obtainMessage(ServicePJSIP.MSG_CALL_ANSWER, Long.valueOf(rcCallId));
            getHandler().sendMessageDelayed(msg, delayMillis);
        } else {
            service.callAnswer(rcCallId);
        }
    }

    @Override
    public void call_answer_and_hold(final long rcCallId, final long delayMillis) throws android.os.RemoteException {
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "call_answer_and_hold(): rcCallId = " + rcCallId + ", delay = " + delayMillis + " msec");
        }

        if (delayMillis > 0) {
            Message msg = getHandler().obtainMessage(ServicePJSIP.MSG_CALL_ANSWER_AND_HOLD, Long.valueOf(rcCallId));
            getHandler().sendMessageDelayed(msg, delayMillis);
        } else {
            service.callAnswerAndHold(rcCallId);
        }
    }

    @Override
    public void call_answer_and_hangup(final long rcCallId, final long delayMillis) throws android.os.RemoteException {
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "call_answer_and_hangup(): rcCallId = " + rcCallId + ", delay = " + delayMillis + " msec");
        }

        if (delayMillis > 0) {
            Message msg = getHandler().obtainMessage(ServicePJSIP.MSG_CALL_ANSWER_AND_HANGUP, Long.valueOf(rcCallId));
            getHandler().sendMessageDelayed(msg, delayMillis);
        } else {
            service.callAnswerAndHangup(rcCallId);
        }
    }

    @Override
    public int call_hangup(final long rcCallId, final int code, final boolean detachSound) throws RemoteException {
        // TDOD: Optimize
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "call_hangup RC callId " + rcCallId);
        }

        if (detachSound) {
            getCallMng().call_detach_sound(rcCallId);
        }
        if (!getState().isSipStackCreated()) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "SIP stack is not created. Cancel operation.");
            }
            return PJSIP.sf_PJ_EUNKNOWN;
        }

        int ret = getCallMng().call_hangup(rcCallId, RCCallInfo.CALL_ENDED_BY_USER, code, null);
        return ret;
    }

    @Override
    public int call_hold(final long rcCallId) throws RemoteException {
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "ITF call_hold callId " + rcCallId);
        }

        if (!getState().isSipStackCreated()) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "SIP stack is not created. Cancel operation.");
            }
            return PJSIP.sf_PJ_EUNKNOWN;
        }

        int ret = getCallMng().call_hold(rcCallId);
        service.updateActiveCallsCounter();
        return ret;
    }

    @Override
    public int call_unhold(final long rcCallId) throws RemoteException {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "call_unhold callId " + rcCallId);
        }

        if (!getState().isSipStackCreated()) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "SIP stack is not created. Cancel operation.");
            }
            return PJSIP.sf_PJ_EUNKNOWN;
        }

        int ret = getCallMng().call_unhold(rcCallId);
        service.updateActiveCallsCounter();
        return ret;
    }

    // @Override
    // public int call_make_call( final int accountId,
    // final String remoteURI,
    // final String remoteName )
    // throws RemoteException {
    //          
    // if( remoteURI == null ){
    // if (LogSettings.MARKET) {
    // MktLog.i(TAG, "call_make_call Failed. Invalid Remote URI" );
    // }
    // return PJSIP.sf_PJ_EINVAL;
    // }
    //
    // /*
    // * Create a new record
    // */
    // final long rcCallId = getCallMng().createCall( remoteURI, remoteName );
    // final RCCallInfo rcCallInfo =
    // ServicePJSIP.getInstance().getCallMng().getCallByRCCallId( rcCallId );
    // rcCallInfo.setNetworkType(ServicePJSIP.getInstance().getNetworkType());
    //          
    // mCommandProcessor.execute(new BindContactCommand(rcCallId));
    //
    // Intent i_callList = new Intent( getBaseContext(),
    // VoipCallStatusActivity.class);
    // setIncomingCallActivityFlags( i_callList );
    // getApplication().startActivity( i_callList );
    //          
    // updateActiveCallsCounter();
    //          
    // if (getState().state() == ServiceState.ON) {
    // mCommandProcessor.execute(new HoldAll());
    // startRings();
    // mCommandProcessor.execute(new MakeCallCommand(rcCallId));
    // } else {
    // if (getState().state() != ServiceState.TO_ON) {
    // getState().set(ServiceState.TO_ON);
    // prepareSip();
    // }
    // }
    //
    // return PJSIP.sf_PJ_SUCCESS;
    //          
    // }

    @Override
    public boolean canMakeVoIPOutboundCall() throws RemoteException {
        if (!service.getSipParameters().isVoipEnabled()) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "FALSE canMakeVoIPOutboundCall(). VoIP not enabled.");
            }
            return false;
        }
        if (!service.isLoginDetected()) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG_EMG, "FALSE canMakeVoIPOutboundCall(). Login is not detected.");
            }
            return false;
        }

        // TODO : Number of calls

        int sipState = getState().state();
        boolean sipStarted = (getState().isSipStackStarted() || sipState == ServiceState.ON || sipState == ServiceState.TO_ON);

        if (sipStarted) {
            if (!service.getSipParameters().isOutboundCallsOnly()) {
                if (service.getHttpRegInfo() != null) {
                    if (getState().isSIPRegistrationState()) {
                        return true;
                    } else {
                        if (LogSettings.MARKET) {
                            MktLog.e(TAG_EMG, "FALSE canMakeVoIPOutboundCall(). SIP Registration false.");
                        }
                        return false;
                    }
                } else {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG_EMG, "FALSE canMakeVoIPOutboundCall(). HTTP Registration is in progress.");
                    }
                    return false;
                }
            }
            return true;
        }

        if (!service.isValidCurrentNetworkForCalls()) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "FALSE canMakeVoIPOutboundCall(). Network not available.");
            }
            return false;
        }

        return true;
    }

    @Override
    public int call_make_call_init(final int accountId, final String remoteURI, final String remoteName) throws RemoteException {

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "call_make_call_init(): START");
        }

        if (remoteURI == null) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "call_make_call_init() failed. Invalid Remote URI");
            }
            return PJSIP.sf_PJ_EINVAL;
        }

        /*
         * Create a new record
         */
        final long rcCallId = getCallMng().createCall(remoteURI, remoteName);
        final RCCallInfo rcCallInfo = ServicePJSIP.getInstance().getCallMng().getCallByRCCallId(rcCallId);
        rcCallInfo.setMakeCallDelayed(true);
        rcCallInfo.setNetworkType(ServicePJSIP.getInstance().getNetworkType());
        rcCallInfo.setRadioNetworkType(ServicePJSIP.getInstance().getRadioNetworkType());

        // mLowPrioCommandProcessor.execute(new BindContactCommand(rcCallId));
        service.bindContactCommand(rcCallId);

        Intent intent = new Intent(service.getBaseContext(), VoipCallStatusActivity.class);
        service.setIncomingCallActivityFlags(intent);
        intent.putExtra(RCMConstants.EXTRA_VOIP_CALL_TYPE, RCMConstants.VOIP_CALL_TYPE_OUTGOING);
        intent.putExtra(RCMConstants.EXTRA_CALL_TO_NUMBER, remoteURI);
        intent.putExtra(RCMConstants.EXTRA_CALL_TO_NAME, remoteName);
        intent.putExtra(RCMConstants.EXTRA_VOIP_OUTGOING_CALL_ID, rcCallId);
        service.getApplication().startActivity(intent);

        Message msg = getHandler().obtainMessage(ServicePJSIP.MSG_CALL_MAKE_CALL_COMPLETE, Long.valueOf(rcCallId));
        getHandler().sendMessageDelayed(msg, RCMConstants.VOIP_START_CALL_DELAY);

        return PJSIP.sf_PJ_SUCCESS;
    }

    @Override
    public void call_make_call_complete(long rcCallId) throws android.os.RemoteException {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "call_make_call_complete(): START by SIPClient");
        }
        service.callMakeCallComplete(rcCallId, true);
    }

    @Override
    public int call_get_info(final long rcCallId, CallInfo callInfo) throws RemoteException {
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "ITF call_get_info callId " + rcCallId);
        }

        if (!getState().isSipStackCreated()) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "SIP stack is not created. Cancel operation.");
            }
            return PJSIP.sf_PJ_EUNKNOWN;
        }
        int ret = getCallMng().call_get_info(rcCallId, callInfo);
        return ret;
    }

    // @Override
    // public int acc_get_default() throws RemoteException {
    // if (LogSettings.MARKET) {
    // MktLog.i(TAG, "acc_get_default" );
    // }
    //          
    // if( !getState().isSipStackCreated() ){
    // if (LogSettings.ENGINEERING) {
    // EngLog.e(TAG, "SIP stack is not created. Cancel operation.");
    // }
    // return PJSIP.sf_PJ_EUNKNOWN;
    // }
    //          
    // cpuLock();
    // int ret = getWrapper().PJSIP_acc_get_default();
    // cpuUnLock();
    // return ret;
    // }

    // @Override
    // public int call_attach_sound(final long rcCallId) throws RemoteException
    // {
    // if (LogSettings.MARKET) {
    // MktLog.i(TAG, "pjsip_call_attach_sound " + rcCallId );
    // }
    //          
    // if( !getState().isSipStackCreated() ){
    // if (LogSettings.ENGINEERING) {
    // EngLog.e(TAG, "SIP stack is not created. Cancel operation.");
    // }
    // return PJSIP.sf_PJ_EUNKNOWN;
    // }
    //          
    // cpuLock();
    // int ret = getCallMng().call_attach_sound( rcCallId );
    // cpuUnLock();
    // return ret;
    //
    // }

    @Override
    public int call_get_count() throws RemoteException {
        int ret = getCallMng().call_get_count();
        return ret;
    }

    @Override
    public boolean call_is_active(final long rcCallId) throws RemoteException {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "call_is_active " + rcCallId);
        }

        if (!getState().isSipStackCreated()) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "SIP stack is not created. Cancel operation.");
            }
            return false;
        }

        boolean ret = getCallMng().call_is_active(rcCallId);
        return ret;
    }

    @Override
    public boolean call_has_media(final long rcCallId) throws RemoteException {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "call_has_media " + rcCallId);
        }

        if (!getState().isSipStackCreated()) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "SIP stack is not created. Cancel operation.");
            }
            return false;
        }

        boolean ret = getCallMng().call_has_media(rcCallId);
        return ret;
    }

    // @Override
    // public int acc_del(int accountId) throws RemoteException {
    // if (LogSettings.MARKET) {
    // MktLog.i(TAG, "Delete current account acc_del " + accountId );
    // }
    //          
    // /*
    // * We are not allow to delete account if incoming calls are allowed
    // */
    // if( !getSipParameters().isOutboundCallsOnly() ){
    // return PJSIP.sf_PJ_EINVALIDOP;
    // }
    //          
    // if( !getState().isSipStackCreated() ){
    // if (LogSettings.ENGINEERING) {
    // EngLog.e(TAG, "SIP stack is not created. Cancel operation.");
    // }
    // return PJSIP.sf_PJ_EUNKNOWN;
    // }
    //          
    // cpuLock();
    // int ret = getWrapper().PJSIP_acc_del( accountId );
    // cpuUnLock();
    // return ret;
    // }

    @Override
    public int message_send(final int accountId, final String dstURI, final String content) throws RemoteException {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "message_send [" + accountId + "] To [" + dstURI + "]");
        }

        if (!getState().isSipStackCreated()) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "SIP stack is not created. Cancel operation.");
            }
            return PJSIP.sf_PJ_EUNKNOWN;
        }

        int ret = getWrapper().PJSIP_message_send(accountId, dstURI, content);
        return ret;
    }

    // @Override
    // public int call_send_request(final long rcCallId, final String content,
    // final String clientId)
    // throws RemoteException {
    // if (LogSettings.MARKET) {
    // MktLog.i(TAG, "call_send_request [" + rcCallId + "] Client ID: " +
    // clientId + " Content: " + content );
    // }
    //          
    // if( !getState().isSipStackCreated() ){
    // if (LogSettings.ENGINEERING) {
    // EngLog.e(TAG, "SIP stack is not created. Cancel operation.");
    // }
    // return PJSIP.sf_PJ_EUNKNOWN;
    // }
    //          
    // cpuLock();
    // int ret = getCallMng().call_send_request( rcCallId, content, clientId );
    // cpuUnLock();
    // return ret;
    // }

    // @Override
    // public int call_transfer(final long rcCallId, String remoteURI)
    // throws RemoteException {
    // if (LogSettings.MARKET) {
    // MktLog.i(TAG, "call_make_call callId [" + rcCallId + "] URI [" +
    // remoteURI + "]" );
    // }
    //          
    // if( !getState().isSipStackCreated() ){
    // if (LogSettings.ENGINEERING) {
    // EngLog.e(TAG, "SIP stack is not created. Cancel operation.");
    // }
    // return PJSIP.sf_PJ_EUNKNOWN;
    // }
    //          
    // cpuLock();
    // int ret = getCallMng().call_transfer( rcCallId, remoteURI );
    // cpuUnLock();
    // return ret;
    // }
//
//    @Override
//    public int codec_set_priority(String codecId, int priority) throws RemoteException {
//        return getWrapper().PJSIP_codec_set_priority(codecId, priority);
//    }
//
//    @Override
//    public CodecInfo[] enum_codecs() throws RemoteException {
//        return getWrapper().PJSIP_enum_codecs();
//    }

    @Override
    public int conf_adjust_rx_level(int slot, float level) throws RemoteException {
        int ret = getWrapper().PJSIP_conf_adjust_rx_level(slot, level);
        return ret;
    }

    @Override
    public int conf_adjust_tx_level(int slot, float level) throws RemoteException {
        int ret = getWrapper().PJSIP_conf_adjust_tx_level(slot, level);
        return ret;
    }

    @Override
    public String storeCurrentLog(int applicationPID) {
        // paranoid check, of course we are maintaining "iam" variable in
        // LoggingManager,
        // but : "..but nobody knows, what gonna happen tomorrow.." (C) A-Ha
        // just for JIC
        if (applicationPID != android.os.Process.myPid()) {
            return LoggingManager.finalizeLogsCollection(LogsFinalizationReason.REMOTE_INITIALIZED_LOG_COLLECTION, null);
        } else {
            return null;
        }
    }

    @Override
    public long[] call_get_active_calls() throws RemoteException {

        if (!getState().isSipStackCreated()) {
            if (LogSettings.ENGINEERING) {
                EngLog.e(TAG, "SIP stack is not created. Cancel operation.");
            }
            return null;
        }

        return getCallMng().call_get_active_calls();
    }

    // @Override
    // public long call_get_duration(final long rcCallId) throws RemoteException
    // {
    // return getCallMng().call_get_duration( rcCallId );
    // }

    @Override
    public float conf_get_signal_rx_level(int slot) throws RemoteException {
        return getWrapper().PJSIP_conf_get_signal_rx_level(slot);
    }

    @Override
    public float conf_get_signal_tx_level(int slot) throws RemoteException {
        return getWrapper().PJSIP_conf_get_signal_tx_level(slot);
    }

    @Override
    public int acc_get(AccountInfo accountInfo) throws RemoteException {
        final AccountInfo accInfo = service.getAccountInfo();
        int status = PJSIP.sf_INVALID_ACCOUNT_ID;

        if (accInfo != null && accountInfo != null) {
            accountInfo.copyFrom(accInfo);
            accountInfo.setRegistered(getWrapper().PJSIP_pjsip_acc_is_valid(0));
            accountInfo.getAccountId();
            status = PJSIP.sf_PJ_SUCCESS;
        }
        return status;
    }

    @Override
    public boolean isStartedSipStack() throws RemoteException {
        return (getState().isSipStackStarted() || getState().state() == ServiceState.TO_ON || getState().state() == ServiceState.ON);
    }

    // @Override
    // public int acc_set_registration(int accountId, boolean renew )
    // throws RemoteException {
    // return getWrapper().PJSIP_pjsip_acc_set_registration( accountId, renew );
    // }

    // @Override
    // public void outboundCallsOnlyFinish() throws RemoteException {
    //
    // if( m_sipParameters.isOutboundCallsOnly() ){
    //              
    // if (LogSettings.MARKET) {
    // MktLog.i(TAG, "outboundCallsOnlyFinish started" );
    // }
    //              
    // mCommandProcessor.execute(new Command("stopSipStack"){
    //
    // @Override
    // public void run() {
    // setHttpRegInfo( null );
    // stopSipStackInternal();
    // }
    //                  
    // });
    // }
    // }

    @Override
    public float conf_get_adjust_rx_level(int slot) throws RemoteException {
        float ret = 0;
        ret = getWrapper().PJSIP_conf_get_adjust_rx_level(slot);
        return ret;
    }

    @Override
    public float conf_get_adjust_tx_level(int slot) throws RemoteException {
        float ret = 0;
        ret = getWrapper().PJSIP_conf_get_adjust_tx_level(slot);
        return ret;
    }

    // @Override
    // public int call_hold_all() throws RemoteException {
    // if (LogSettings.ENGINEERING) {
    // EngLog.i(TAG, "HoldAll Starting..." );
    // }
    //          
    // cpuLock();
    //          
    // /*
    // * Set HOLD state for all active calls
    // */
    // if( getState().isSipStackStarted() ){
    //              
    // final long[] calls = getCallMng().call_get_active_calls();
    // if( calls != null ){
    // if (LogSettings.ENGINEERING) {
    // EngLog.i(TAG, "HoldAll Detected active calls: " + calls.length );
    // }
    //                  
    // for( int i = 0; i < calls.length; i++ ){
    //                      
    // if (LogSettings.ENGINEERING) {
    // EngLog.i(TAG, "HoldAll Set HOLD state for call: " + calls[i] );
    // }
    //                      
    // getCallMng().call_hold( calls[i] );
    // }
    //                  
    // updateActiveCallsCounter();
    // }
    // }
    //            
    // cpuUnLock();
    //            
    // return PJSIP.sf_PJ_SUCCESS;
    // }

    @Override
    public int call_dial_dtmf(final long rcCallId, String digits) throws RemoteException {
        int ret = getCallMng().call_dial_dtmf(rcCallId, digits);
        return ret;
    }

    @Override
    public AudioState getAudioState() throws RemoteException {
        return service.mMediaManager.getAudioState();
    }

    @Override
    public void toggleBluetoothState() throws RemoteException {
        service.mMediaManager.toggleBluetooth();
    }

    @Override
    public void toggleSpeakerState() throws RemoteException {
        service.mMediaManager.toggleSpeaker();
    }

    @Override
    public int[] get_snd_dev() throws RemoteException {
        return getWrapper().PJSIP_pjsip_get_snd_dev("SIPClient");
    }
    
    @Override
    public boolean isPlayBackDevice() throws RemoteException {
        // TODO : Optimize
        int[] devices = getWrapper().PJSIP_pjsip_get_snd_dev("SIPClient"); 
        if (devices != null) {
            return (devices[1] >= 0);
        }
        return false; 
    }

    // @Override
    // public void set_snd_dev()
    // throws RemoteException {
    //          
    // /*
    // * Initialize RC Audio settings
    // */
    // mCommandProcessor.execute(new Command("initializeCallSession"){
    //
    // @Override
    // public void run() {
    // mMediaManager.startAudio(RCMediaManager.MODE_IN_CALL);
    // }
    // });
    //          
    // getWrapper().PJSIP_pjsip_set_snd_dev();
    //          
    // }
    //
    // @Override
    // public void ring_start() throws RemoteException {
    // ringStart("SIPClient");
    // }

    @Override
    public void ring_stop() throws RemoteException {
        service.ringStop("SIPClient");
    }

    @Override
    public boolean acc_is_registered(int accountId) throws RemoteException {
        return getWrapper().PJSIP_pjsip_acc_is_registered(accountId);
    }

    /**
     * Initialization
     */
    @Override        
    public void ase_initialize() throws RemoteException {
        if(LogSettings.MARKET) {
            MktLog.d(TAG, "ase_initialize()");
        }            
        service.mAudioSetupEngine = new AudioSetupEngine(service, getWrapper(), service.mMediaManager);
    }
    
    /**
     * Release
     */
    @Override
    public void asw_release() throws RemoteException {
        if(LogSettings.MARKET) {
            MktLog.d(TAG, "asw_release()");
        }
        if(service.mAudioSetupEngine != null) {
            service.mAudioSetupEngine.release();
            service.mAudioSetupEngine = null;
        }
    }
    
    /**
     * use that method only if the SIP stack is started
     */
    @Override
    public void asw_start(int testId) throws RemoteException {
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "asw_start(), testId : " + RCMediaManager.getModeTag(testId));
        }
        if (service.mAudioSetupEngine != null) {
            service.mAudioSetupEngine.startTest(testId);
        }
    }

    @Override
    public void asw_stop() throws RemoteException {
        if(LogSettings.MARKET) {
            MktLog.d(TAG, "asw_stop()");
        }            
        if(service.mAudioSetupEngine != null) {
            service.mAudioSetupEngine.stopTest();
        }
    } 

    @Override
    public void set_snd_dev_test_mode() throws RemoteException {
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "set_snd_dev_test_mode");
        }

        getWrapper().PJSIP_pjsip_set_snd_dev("SIPClientTestMode");
    }

//    @Override
//    public int call_detach_sound(final long rcCallId) throws RemoteException {
//        return getCallMng().call_detach_sound(rcCallId);
//    }

    @Override
    public void ringtone_stop() throws RemoteException {
        service.RingtoneStop();
    }

    @Override
    public int call_mark_as_ending(final long rcCallId) throws RemoteException {
        RCCallInfo rcCall = null;
        synchronized (getCallMng()) {
            rcCall = getCallMng().getCallByRCCallId(rcCallId);
            if (rcCall != null) {
                rcCall.setEnding(RCCallInfo.CALL_ENDED_BY_USER);
            }
        }

        if (rcCall == null) {
            return PJSIP.sf_INVALID_CALL_ID;
        } else {
            getCallMng().ensureClientNotified(rcCall, true, true);
        }

        service.updateActiveCallsCounter();
        service.invalidateIncomingCallNotification(rcCallId);
        return PJSIP.sf_PJ_SUCCESS;
    }
    
    @Override
    public void testEnqueedCommandsMaxNumberExceeded() {
        service.testEnqueedCommandsMaxNumberExceeded();
    }

    @Override
    public void testCommandExecutionTimeExceeded() {
        service.testCommandExecutionTimeExceeded();
    }
    
    /***************************************************************************************************************************************
     * Internal routines. 
     **************************************************************************************************************************************/
    /**
     * 
     */
    ServicePJSIPbinder(ServicePJSIP service) {
        this.service = service;
    }

    private CallManager getCallMng() {
        return service.getCallMng();
    }

    private WrapPJSIP getWrapper() {
        return service.getWrapper();
    }

    private Handler getHandler() {
        return service.getHandler();
    }
    private ServiceState getState() {
        return service.getState();
    }
    private void injectCommand(Command cmd) {
        service.injectCommand(cmd);
    }
}
