/**
 * Copyright (C) 2011-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.client;

import java.lang.ref.WeakReference;
import java.util.Vector;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.service.IServicePJSIP;
import com.rcbase.android.sip.service.ServicePJSIP;
import com.rcbase.android.utils.execution.CommandProcessor;
import com.rcbase.android.utils.execution.ExecutionWakeLock;
import com.rcbase.android.utils.execution.CommandProcessor.Command;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RingCentralApp;

public final class SIProxy {
    /**
     * Initial state of connection to SIP service.
     * 
     * @see #getServiceConnectioState()
     */
    public static final int CONNECTING = 0;

    /**
     * Connection to SIP service is established.
     * 
     * @see #getServiceConnectioState()
     */
    public static final int CONNECTED = 1;

    /**
     * Connection to SIP service has been lost re-connecting state.
     * 
     * @see #getServiceConnectioState()
     */
    public static final int RECONNECTING = 2;

    /**
     * Connection to SIP service closed.
     * 
     * @see #getServiceConnectioState()
     */
    public static final int SHUTDOWN = 3;

    /**
     * Returns state in user-friendly form.
     * 
     * @return state
     */
    public final String getServiceConnectionStateAsString() {
        if (mServiceConnectionState == CONNECTED) {
            return "CONNECTED";
        } else if (mServiceConnectionState == RECONNECTING) {
            return "RECONNECTING";
        } else if (mServiceConnectionState == CONNECTING) {
            return "CONNECTING";
        }
        return "SHUTDOWN";
    }

    /**
     * Returns service. <code>android.os.RemoteException</code> shall be used
     * for working with the handle.
     * 
     * @return service if the connection is in {@link #CONNECTED}, otherwise
     *         <code>null</code>
     */
    public final IServicePJSIP getService() {
        synchronized (sLock) {
            if (mServiceConnectionState != CONNECTED || !mBound) {
                return null;
            }
        }
        return mService;
    }

    /**
     * The envelope is used for synchronized retrieving service by
     * {@link SIProxy#getService(SIProxyClient)}
     */
    public static class ServiceRequest {
        /**
         * Keeps service connection state, see
         * {@link SIProxy#getServiceConnectionState()}
         */
        public int serviceState;

        /**
         * Keeps service handle if {@link #serviceState} is
         * {@link SIProxy#CONNECTED}
         */
        public IServicePJSIP serviceHandle = null;

        /**
         * Defines if client has been added for notification, see
         * {@link SIProxy#getService(SIProxyClient)}
         * 
         * It will be added if {@link #serviceState} is not
         * {@link SIProxy#SHUTDOWN} and client is not <code>null</code> when
         * {@link SIProxy#getService(SIProxyClient)} was called
         */
        public boolean clientAdded = false;
        
        /**
         * Latest known service PID.
         */
        public int servicePID = 0;
    }

    /**
     * Synchronized service retrieving (atomic operation).
     * 
     * If service state is #CONNECTED {@link ServiceRequest#serviceHandle} will
     * keep the service connection.
     * 
     * If service state is not #SHUTDOWN and <code>client</code> is not
     * <code>null</code> the client to be added for notification and shall be
     * removed later via {@link SIProxy#removeClient(SIProxyClient)}
     * 
     * @param client
     *            proxy client to be added
     */
    public final ServiceRequest getService(SIProxyClient client) {
        synchronized (sLock) {
            ServiceRequest r = new ServiceRequest();
            r.serviceState = mServiceConnectionState;
            r.servicePID = mLatestServicePID;
            if (mServiceConnectionState == CONNECTED) {
                r.serviceHandle = mService;
            }

            if (mServiceConnectionState != SHUTDOWN && client != null) {
                r.clientAdded = addClient(client);
            }

            return r;
        }
    }

    /**
     * Returns service connection state.
     * 
     * @return service connection state {@link #CONNECTING}, {@link #CONNECTED},
     *         {@link #RECONNECTING}, {@link #SHUTDOWN}
     */
    public final int getServiceConnectionState() {
        return mServiceConnectionState;
    }

    /**
     * Adds SIProxyClient for receiving notifications.
     * 
     * @see #removeClient(SIProxyClient)
     * @param client
     *            the client to be added
     * @return <code>true</code> if the client has been added, otherwise
     *         <code>false</code> if {@link #SHUTDOWN} state.
     */
    public final boolean addClient(final SIProxyClient client) {
        if (client == null) {
            throw new IllegalArgumentException(TAG + "addClient:null");
        }
        synchronized (sLock) {
            if (mServiceConnectionState != SHUTDOWN) {
                Vector<SIProxyClient> activeClients = cleanupAndGetActiveClients();
                if (!activeClients.contains(client)) {
                    mClients.add(new WeakReference<SIProxyClient>(client));
                }
                return true;
            }
            return false;
        }
    }

    /**
     * Remove SIProxyClient from receiving notifications.
     * 
     * @see #addClient(SIProxyClient)
     * @param client
     *            the client to be removed
     */
    public final void removeClient(final SIProxyClient client) {
        if (client == null) {
            return;
        }
        synchronized (sLock) {
            Vector<WeakReference<SIProxyClient>> newList = new Vector<WeakReference<SIProxyClient>>();
            for (WeakReference<SIProxyClient> ref : mClients) {
                SIProxyClient c = ref.get();
                if (c != null && c != client) {
                    newList.add(new WeakReference<SIProxyClient>(c));
                }
            }
            mClients = newList;
        }
    }

    /**
     * Returns <code>SIProxy</code> instance.
     * 
     * @return the proxy instance
     */
    public static final SIProxy getInstance() {
        synchronized (sLock) {
            if (sInstance == null) {
                sInstance = new SIProxy();
            }
            return sInstance;
        }
    }

    /**
     * Force binding.
     */
    public static final void ensureBindingStarted(int processType) {
        synchronized (sLock) {
            if (sInstance != null) {
                if (m_PID != android.os.Process.myPid()) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "ensureBindingStarted: Instance exists : PID " + m_PID + "->" + android.os.Process.myPid());
                    }
                    sInstance.destroy(false);
                    sInstance = null;
                }
            }
            m_PID = android.os.Process.myPid();
            getInstance();
        }
    }
    
    private static int m_PID = 0;
    
    /**
     * Keeps current clients;
     */
    private Vector<WeakReference<SIProxyClient>> mClients = new Vector<WeakReference<SIProxyClient>>();

    /**
     * Cleans-up clients and returns active clients.
     * 
     * Shall be synchronized by #sLock.
     */
    private Vector<SIProxyClient> cleanupAndGetActiveClients() {
        Vector<WeakReference<SIProxyClient>> newList = new Vector<WeakReference<SIProxyClient>>();
        Vector<SIProxyClient> activeClients = new Vector<SIProxyClient>();
        for (WeakReference<SIProxyClient> ref : mClients) {
            SIProxyClient c = ref.get();
            if (c != null) {
                activeClients.add(c);
                newList.add(new WeakReference<SIProxyClient>(c));
            }
        }
        mClients = newList;
        return activeClients;
    }

    /**
     * Default hidden constructor.
     */
    private SIProxy() {
        TAG = LTAG_PREFIX + android.os.Process.myPid() + ')';
        if (mWakeLock == null) {
            mWakeLock = new SIPProxyWakeLock((PowerManager) RingCentralApp.getContextRC().getSystemService(Context.POWER_SERVICE));
        } else {
            mWakeLock.releaseAll();
        }
        if (mCommandProcessor == null) {
            mCommandProcessor = new CommandProcessor(TAG, android.os.Process.THREAD_PRIORITY_URGENT_DISPLAY, mWakeLock);
        }
        onReconnectAttempt();
    }

    /**
     * Sync primitive.
     */
    private static final Object sLock = new Object();

    /**
     * Keeps instance of the proxy.
     */
    private static SIProxy sInstance = null;

    /**
     * Keeps service connection state.
     */
    private volatile int mServiceConnectionState = CONNECTING;

    /**
     * Keeps command processor.
     */
    private static CommandProcessor mCommandProcessor = null;

    /**
     * Keeps WakeLock
     */
    private static SIPProxyWakeLock mWakeLock = null;

    /**
     * Defines prefix for logging tag.
     */
    private static final String LTAG_PREFIX = "SIProxy(";

    /**
     * Keeps logging tag.
     */
    private static String TAG = LTAG_PREFIX;
    
    /**
     * Kill service test.
     */
    private static final boolean KILL_SERVICE_TEST = false;
    
    /**
     * Defines maximum attempts to reconnect to the service.
     */
    private static final int MAX_RE_CONECTIONS_ATTEMPTS = 15;
    
    /**
     * Defines attempts to reconnect to the service when ServiceSIp will be killed (tried).
     */
    private static final int RE_CONECTIONS_ATTEMPTS_WHEN_TRY_TO_KILL_SERVICE = 7;
    
    /**
     * Defines timeout between re-connections.
     */
    private static final long RE_CONECTION_TIMEOUT_IN_MS = (KILL_SERVICE_TEST ? 10000 : 1000);
    
    private boolean KILL_TEST_CASE_INPROGRESS = KILL_SERVICE_TEST;
    
    /**
     * Re-connection attempt.
     */
    private void onReconnectAttempt() {
        synchronized (sLock) {
            if (mServiceConnectionState == SHUTDOWN || mServiceConnectionState == CONNECTED || mBound) {
                return;
            }
        }

        if (LogSettings.MARKET) {
            MktLog.i(TAG, "onRebindAttempt: " + mLastReconnectionsAttempts + "/" + MAX_RE_CONECTIONS_ATTEMPTS + ", total:"
                    + mTotalReconnectionsAttempts);
        }

        Context ctx = RingCentralApp.getContextRC();
        if (ctx != null) {
            try {
                synchronized (sLock) {
                    mLastReconnectionsAttempts++;
                    mTotalReconnectionsAttempts++;
                }
                boolean bound = false;
                if (!KILL_SERVICE_TEST) {
                    bound = ctx.bindService(new Intent(ctx, ServicePJSIP.class), mConnection, Context.BIND_AUTO_CREATE);
                } else {
                    if ((mLastReconnectionsAttempts <= RE_CONECTIONS_ATTEMPTS_WHEN_TRY_TO_KILL_SERVICE && KILL_TEST_CASE_INPROGRESS)
                            && (RingCentralApp.isApplicationProcess())) {
                        if (mLastReconnectionsAttempts == 1) {
                            ctx.bindService(new Intent(ctx, ServicePJSIP.class), mConnection, Context.BIND_AUTO_CREATE);
                        }
                        bound = false;
                    } else {
                        KILL_TEST_CASE_INPROGRESS = false;
                        bound = ctx.bindService(new Intent(ctx, ServicePJSIP.class), mConnection, Context.BIND_AUTO_CREATE);
                    }
                }
                
                if (bound) {
                    if (LogSettings.MARKET) {
                        MktLog.i(TAG, "ServicePJSIP was bound successfuly");
                    }
                    synchronized (sLock) {
                        mLastReconnectionsAttempts = 0;
                    }
                    return;
                } else {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "Failed to bind to SIP service");
                    }
                    synchronized (sLock) {
                        if (RE_CONECTIONS_ATTEMPTS_WHEN_TRY_TO_KILL_SERVICE == mLastReconnectionsAttempts) {
                            RingCentralApp.tryToKillSIPService();
                        }
                        if (mLastReconnectionsAttempts > MAX_RE_CONECTIONS_ATTEMPTS) {
                            if (LogSettings.MARKET) {
                                MktLog.e(TAG, "Attemts to bind SIP service exceeded. Terminating...");
                            }
                        } else {
                            if (LogSettings.MARKET) {
                                MktLog.w(TAG, "Attempt to bind SIP service postponed.");
                            }
                            mInternalHandler.removeMessages(INT_CMD_RECONNECT);
                            mInternalHandler.sendMessageDelayed(mInternalHandler.obtainMessage(INT_CMD_RECONNECT), RE_CONECTION_TIMEOUT_IN_MS);
                            return;
                        }
                    }
                }
            } catch (java.lang.Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "onReconnectAttempt:error:" + th.toString());
                }
            }
            return;
        }

        destroy(true);
    }

    /**
     * Notifies clients about state changing.
     */
    private void notifyOnServiceConnectionStateChange() {
        synchronized (sLock) {
            if (mServiceConnectionState == SHUTDOWN) {
                return;
            }
            if (cleanupAndGetActiveClients().isEmpty()) {
                return;
            }
        }
        try {
            mCommandProcessor.execute(new Command("onServiceConnectionStateChange", MAX_TIME_FOR_COMMAND_EXECUTION,
                    MAX_TIME_FOR_COMMAND_TOTAL_EXECUTION) {
                @Override
                public void run() {
                    Vector<SIProxyClient> clients = cleanupAndGetActiveClients();
                    if (!clients.isEmpty()) {
                        for (SIProxyClient client : clients) {
                            try {
                                client.onServiceConnectionStateChange();
                            } catch (java.lang.Throwable th) {
                            }
                        }
                    }
                }
            });
        } catch (java.lang.Throwable th) {
        }
    }
    
    /**
     * Defines maximum time for a command (notification) execution
     */
    private static final long MAX_TIME_FOR_COMMAND_EXECUTION = 1000;

    /**
     * Defines maximum time for a command (notification) pending and execution
     */
    private static final long MAX_TIME_FOR_COMMAND_TOTAL_EXECUTION = 2000;

    /**
     * Destroys SIProxy.
     */
    public void destroy(final boolean withCommandProcessor) {
        synchronized (sLock) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "destroy()");
            }
            if (mServiceConnectionState == SHUTDOWN) {
                return;
            }
            mServiceConnectionState = SHUTDOWN;

            try {
                if (mBound) {
                    mBound = false;
                    RingCentralApp.getContextRC().unbindService(mConnection);
                }
            } catch (java.lang.Throwable th) {
            }
            mLastReconnectionsAttempts = 0;
        }

        if (!cleanupAndGetActiveClients().isEmpty()) {
            mCommandProcessor.execute(new Command("SHUTDOWN") {
                @Override
                public void run() {
                    Vector<SIProxyClient> clients = cleanupAndGetActiveClients();

                    if (!clients.isEmpty()) {
                        for (SIProxyClient client : clients) {
                            try {
                                client.onServiceConnectionStateChange();
                            } catch (java.lang.Throwable th) {
                            }
                        }
                    }
                    if (withCommandProcessor) {
                        mCommandProcessor.destroy(true);
                        mCommandProcessor = null;
                    }
                }
            });
        }

        mService = null;
        mConnection = null;
    }

    /**
     * Keeps service interface.
     */
    private IServicePJSIP mService = null;

    /**
     * Keeps connection to SIP service.
     */
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "onServiceConnected");
            }
            IServicePJSIP srvc = null;
            synchronized (sLock) {
                if (mServiceConnectionState == SHUTDOWN) {
                    forceUnbind();
                    return;
                }
                mBound = true;
            }
            srvc = IServicePJSIP.Stub.asInterface(service);
            if (KILL_SERVICE_TEST) {
                if ((KILL_TEST_CASE_INPROGRESS)
                        && (RingCentralApp.isApplicationProcess())) {
                    srvc = null;
                }
            }

            if (srvc == null) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "onServiceConnected : service interface is null:");
                }
                forceUnbind();
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "Attempt to bind SIP service postponed.");
                }
                mInternalHandler.removeMessages(INT_CMD_RECONNECT);
                mInternalHandler.sendMessageDelayed(mInternalHandler.obtainMessage(INT_CMD_RECONNECT), RE_CONECTION_TIMEOUT_IN_MS);
                return;
            }
            
            try {
                int pid = srvc.getSipServicePID();
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "onServiceConnected : PID " + pid);
                }
            } catch (java.lang.Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "onServiceConnected : error obtaining service PID");
                }
            }
            
            synchronized (sLock) {
                mService = srvc;
                mServiceConnectionState = CONNECTED;
                mBound = true;
            }
            notifyOnServiceConnectionStateChange();
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            synchronized (sLock) {
                if (mServiceConnectionState == SHUTDOWN) {
                    return;
                }
            }

            if (LogSettings.MARKET) {
                MktLog.w(TAG, "onServiceDisconnected");
            }

            forceUnbind();
            boolean notify = false;
            synchronized (sLock) {
                mService = null;
                mBound = false;
                if (mServiceConnectionState == CONNECTED) {
                    notify = true;
                    mServiceConnectionState = RECONNECTING;
                }
            }

            if (notify) {
                notifyOnServiceConnectionStateChange();
            }

            if (LogSettings.MARKET) {
                MktLog.w(TAG, "Attempt to bind SIP service postponed.");
            }
            mInternalHandler.removeMessages(INT_CMD_RECONNECT);
            mInternalHandler.sendMessageDelayed(mInternalHandler.obtainMessage(INT_CMD_RECONNECT), RE_CONECTION_TIMEOUT_IN_MS);
        }
    };

    /**
     * Keeps latest service PID.
     */
    private volatile int mLatestServicePID = 0;  
    
    /**
     * Returns latest known service PID
     * 
     * @return latest known service PID
     */
    public int getServicePID() {
        return mLatestServicePID;
    }
    
    /**
     * Makes service un-bind.
     */
    private void forceUnbind() {
        try {
            if (mBound) {
                mBound = false;
                RingCentralApp.getContextRC().unbindService(mConnection);
            }
        } catch (java.lang.Throwable th) {
        } finally {
            mService = null;
        }
    }

    /**
     * Defines if the service is bound.
     */
    private boolean mBound = false;

    /**
     * Keeps the number of last re-connections attempts
     */
    private int mLastReconnectionsAttempts = 0;

    /**
     * Returns total number of reconnects.
     * 
     * @return total number of reconnects.
     */
    public int getTotalNumberOfReconnects() {
        return mTotalReconnectionsAttempts;
    }

    /**
     * Keeps the number of total re-connections attempts
     */
    private int mTotalReconnectionsAttempts = 0;

    /**
     * Makes reconnect to the service
     */
    private final static int INT_CMD_RECONNECT = 0;

    /**
     * Keeps internal handler.
     */
    private Handler mInternalHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (null == msg) {
                return;
            }
            switch (msg.what) {
            case INT_CMD_RECONNECT:
                onReconnectAttempt();
                break;
            default:
                break;
            }
        }
    };

    /**
     * Wake lock for SIP commands execution.
     */
    private class SIPProxyWakeLock implements ExecutionWakeLock {
        /**
         * Synchronization primitive.
         */
        private Object _lock = new Object();

        /**
         * Defines logging tag.
         */
        private static final String LOG_TAG = "[RC]SIProxyWakeLock";

        /**
         * Keeps Android Power manager.
         */
        private PowerManager mPowerManager;

        /**
         * Keeps partial wake lock.
         */
        private PowerManager.WakeLock mPartialWakeLock;

        private static final int MAX_ACQUIRES_WHEN_WARN = 5;
        
        /**
         * acquire/release counter.
         */
        private long mCounter;

        /**
         * Constructs wake lock for SIP commands execution.
         * 
         * @param powerManager
         *            the power manager instance
         */
        public SIPProxyWakeLock(PowerManager powerManager) {
            mPowerManager = powerManager;
        }

        @Override
        public void acquire() {
            synchronized (_lock) {
                try {
                    if (mPartialWakeLock == null) {
                        mPartialWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "RingCenral VoIP Proxy");
                        mPartialWakeLock.setReferenceCounted(false);
                    }
                } catch (Throwable th) {
                    if (LogSettings.MARKET) {
                        MktLog.w(LOG_TAG, " acquire:newWakeLock:exception:" + th.toString());
                    }
                    return;
                }

                try {
                    if (!mPartialWakeLock.isHeld()) {
                        mPartialWakeLock.acquire();
                    }
                } catch (Throwable th) {
                    if (LogSettings.MARKET) {
                        MktLog.w(LOG_TAG, " acquire:acquire:exception:" + th.toString());
                    }
                    return;
                }
                mCounter++;
            }
            
            if (mCounter >= MAX_ACQUIRES_WHEN_WARN) {
                if (LogSettings.MARKET) {
                    MktLog.w(LOG_TAG, " acquire:limit:" + mCounter);
                }
            }
        }

        @Override
        public void release() {
            synchronized (_lock) {
                mCounter--;
                if (mCounter < 0) {
                    if (LogSettings.MARKET) {
                        MktLog.w(LOG_TAG, " relase:negative counter:");
                    }
                    mCounter = 0;
                }
                try {
                    if ((mCounter == 0) && (mPartialWakeLock != null) && (mPartialWakeLock.isHeld())) {
                        mPartialWakeLock.release();
                    }
                } catch (Throwable th) {
                    if (LogSettings.MARKET) {
                        MktLog.w(LOG_TAG, " release:release:exception:" + th.toString());
                    }
                    return;
                }
            }
        }

        @Override
        public void releaseAll() {
            synchronized (_lock) {
                if (mCounter > 0) {
                    if (LogSettings.MARKET) {
                        MktLog.d(LOG_TAG, " releaseAll:counter is " + mCounter);
                    }
                }

                mCounter = 0;
                try {
                    if ((mPartialWakeLock != null) && (mPartialWakeLock.isHeld())) {
                        mPartialWakeLock.release();
                    }
                } catch (Throwable th) {
                    if (LogSettings.MARKET) {
                        MktLog.w(LOG_TAG, " releaseAll:release:exception:" + th.toString());
                    }
                    return;
                }
            }
        }
        
        /**
         * Returns if the lock is acquired.
         * 
         * @return if the lock is acquired
         */
        public boolean isHeld() {
            synchronized (_lock) {
                try {
                    if (mPartialWakeLock != null) {
                        return mPartialWakeLock.isHeld();
                    }
                } catch (Throwable th) {
                }
            }
            return false;
        }
    }
}
