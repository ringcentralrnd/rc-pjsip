/**
 * Copyright (C) 2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import java.util.Comparator;

/**
 * Codec info comparator.
 */
public class CodecInfoComparator implements Comparator<CodecInfo> {
    @Override
    public int compare(CodecInfo object1, CodecInfo object2) {

        if (object1.getPriority() == object2.getPriority())
            return 0;

        return (object1.getPriority() > object2.getPriority() ? -1 : 1);
    }

}
