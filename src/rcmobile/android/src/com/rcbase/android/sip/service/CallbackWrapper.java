/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.RemoteException;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.utils.execution.TimedProxyTask;
import com.rcbase.android.utils.media.AudioState;
import com.rcbase.parsers.sipmessage.SipMessageInput;
import com.ringcentral.android.LogSettings;

/**
 * Service calbacks notifications.
 */
public class CallbackWrapper implements IServicePJSIPCallback {
    private static final String TAG = "[RC]CallbackWrapper";

    private HashMap<Integer, IServicePJSIPCallback> m_callback = new HashMap<Integer, IServicePJSIPCallback>();
    public synchronized boolean isEmpty() {
        return (m_callback.size() == 0);
    }

    public synchronized void add(Integer hash, IServicePJSIPCallback callback) {
        if (callback != null && !m_callback.containsKey(hash)) {
            m_callback.put(hash, callback);
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "Register callback Added: " + hash + " size: " + m_callback.size());
            }
        }
    }

    public synchronized void remove(Integer hash) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "Remove callback: " + hash + " callbacks: " + m_callback.size());
        }

        if (m_callback != null) {
            Iterator<Integer> it = m_callback.keySet().iterator();
            while (it.hasNext()) {
                Integer i_hash = it.next();
                if (i_hash.equals(hash)) {
                    it.remove();
                    break;
                }
            }

            if (LogSettings.MARKET) {
                MktLog.i(TAG, "Removed callback: " + hash + " callbacks: " + m_callback.size());
            }
        }
    }

    /**
     * Defines default timeout for notifications (blocking caller).
     */
    private static final long CALLBACK_TIMEOUT = 1000;

    /**
     * Asynchronous callback notification: implement <code>doNotification</code>
     * and call <code>executeNotification</code>
     */
    public abstract class AsyncNotificatonTask extends TimedProxyTask {
        private CallbackWrapper wrapper;

        public AsyncNotificatonTask(String name, CallbackWrapper wrapper) {
            super(name);
            this.wrapper = wrapper;
        }

        /**
         * The task will be executed for notification.
         */
        public abstract void doNotification(IServicePJSIPCallback callback) throws RemoteException;

        /**
         * Executes notification.
         */
        public void executeNotification() {
            int state = execute(CALLBACK_TIMEOUT, true);
            if (state == TimedProxyTask.TIMEOUT) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, getTaskName() + ":TIMEOUT");
                }
            }
        }

        @Override
        public void doTask() {
            Set<Integer> clientsHash = null;
            synchronized (wrapper) {
                if (wrapper.m_callback != null) {
                    clientsHash = m_callback.keySet();
                }
            }

            if (clientsHash == null) {
                return;
            }

            for (Integer hash : clientsHash) {
                IServicePJSIPCallback callback = null;
                synchronized (wrapper) {
                    callback = wrapper.m_callback.get(hash);
                }
                if (callback == null) {
                    continue;
                }
                try {
                    doNotification(callback);
                } catch (DeadObjectException e_do) {
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, getTaskName() + ":Callback removed. DeadObjectException: hash " + hash);
                    }
                    synchronized (wrapper) {
                        wrapper.m_callback.remove(hash);
                    }
                } catch (RemoteException re) {
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, getTaskName() + ":RemoteException:", re);
                    }
                }
            }

        }
    }

    /************************************************************************************************************************************
     */
    private class AccountChangedRegisterStateTask extends AsyncNotificatonTask {
        private int accountId;
        private boolean registered;
        private int code;

        public AccountChangedRegisterStateTask(CallbackWrapper wrapper, int accountId, boolean registered, int code) {
            super("AccChgRegStateNotifTask", wrapper);
            this.accountId = accountId;
            this.registered = registered;
            this.code = code;
        }

        @Override
        public void doNotification(IServicePJSIPCallback callback) throws RemoteException {
            callback.account_changed_register_state(accountId, registered, code);
        }
    }

    @Override
    public void account_changed_register_state(int accountId, boolean registered, int code) {
        synchronized (this) {
            if (m_callback == null || m_callback.isEmpty()) {
                return;
            }
        }
        new AccountChangedRegisterStateTask(this, accountId, registered, code).executeNotification();

    }

    /************************************************************************************************************************************
     */

    private class CallStateTask extends AsyncNotificatonTask {
        private long callId;
        private int state;
        private int last_status;
        private int ending_reason;
        private int media_sate;

        public CallStateTask(CallbackWrapper wrapper, long callId, int state, int last_status, int ending_reason, int media_sate) {
            super("CallStateNotifTask", wrapper);
            this.callId = callId;
            this.state = state;
            this.last_status = last_status;
            this.ending_reason = ending_reason;
            this.media_sate = media_sate;
        }

        @Override
        public void doNotification(IServicePJSIPCallback callback) throws RemoteException {
            callback.call_state(callId, state, last_status, ending_reason, media_sate);
        }
    }

    @Override
    public void call_state(long callId, int state, int last_status, int ending_reason, int media_sate) {
        synchronized (this) {
            if (m_callback == null || m_callback.isEmpty()) {
                return;
            }
        }
        new CallStateTask(this, callId, state, last_status, ending_reason, media_sate).executeNotification();
    }

    /************************************************************************************************************************************
     */
    private class CallMediaStateTask extends AsyncNotificatonTask {
        private long rcCallId;
        private int state;

        public CallMediaStateTask(CallbackWrapper wrapper, long rcCallId, int state) {
            super("CallMediaStateNotifTask", wrapper);
            this.rcCallId = rcCallId;
            this.state = state;
        }

        @Override
        public void doNotification(IServicePJSIPCallback callback) throws RemoteException {
            callback.call_media_state(rcCallId, state);
        }
    }

    @Override
    public void call_media_state(long rcCallId, int state) {
        synchronized (this) {
            if (m_callback == null || m_callback.isEmpty()) {
                return;
            }
        }
        new CallMediaStateTask(this, rcCallId, state).executeNotification();
    }

    /************************************************************************************************************************************
     */

    private class CallMediaStopTask extends AsyncNotificatonTask {
        private long callId;
        public CallMediaStopTask(CallbackWrapper wrapper, long callId) {
            super("CallMediaStopNotifTask", wrapper);
            this.callId = callId;
        }

        @Override
        public void doNotification(IServicePJSIPCallback callback) throws RemoteException {
            callback.call_media_stop(callId);
        }
    }

    @Override
    public void call_media_stop(long callId) {
        synchronized (this) {
            if (m_callback == null || m_callback.isEmpty()) {
                return;
            }
        }
        new CallMediaStopTask(this, callId).executeNotification();
    }

    /************************************************************************************************************************************
     */
    private class OnCallTransferStatusTask extends AsyncNotificatonTask {
        private long rcCallId;
        private int status;
        private String statusProgressText;

        public OnCallTransferStatusTask(CallbackWrapper wrapper, long rcCallId, int status, String statusProgressText) {
            super("OnCallTransfStatusNotifTask", wrapper);
            this.rcCallId = rcCallId;
            this.status = status;
            this.statusProgressText = statusProgressText;
        }

        @Override
        public void doNotification(IServicePJSIPCallback callback) throws RemoteException {
            callback.on_call_transfer_status(rcCallId, status, statusProgressText);
        }
    }

    @Override
    public void on_call_transfer_status(long rcCallId, int status, String statusProgressText) {
        synchronized (this) {
            if (m_callback == null || m_callback.isEmpty()) {
                return;
            }
        }
        new OnCallTransferStatusTask(this, rcCallId, status, statusProgressText).executeNotification();
    }

    /************************************************************************************************************************************
     */
    private class OnIncomingCallTask extends AsyncNotificatonTask {
        private CallInfo callInfo;
        public OnIncomingCallTask(CallbackWrapper wrapper, CallInfo callInfo) {
            super("OnCallTransfStatusNotifTask", wrapper);
            this.callInfo = callInfo;

        }

        @Override
        public void doNotification(IServicePJSIPCallback callback) throws RemoteException {
            callback.on_incoming_call(callInfo);
        }
    }
    @Override
    public void on_incoming_call(CallInfo callInfo) {
        synchronized (this) {
            if (m_callback == null || m_callback.isEmpty()) {
                return;
            }
        }
        new OnIncomingCallTask(this, callInfo).executeNotification();
    }

    /************************************************************************************************************************************
    */
    private class OnMessageTask extends AsyncNotificatonTask {
        private long callId;
        private SipMessageInput sipMsg;
        public OnMessageTask(CallbackWrapper wrapper, long callId, SipMessageInput sipMsg) {
            super("OnMessageNotifTask", wrapper);
            this.callId = callId;
            this.sipMsg = sipMsg;
        }
        @Override
        public void doNotification(IServicePJSIPCallback callback) throws RemoteException {
            callback.on_message(callId, sipMsg);
        }
    }
    @Override
    public void on_message(long callId, SipMessageInput sipMsg) {
        synchronized (this) {
            if (m_callback == null || m_callback.isEmpty()) {
                return;
            }
        }
        new OnMessageTask(this, callId, sipMsg).executeNotification();
    }

    /************************************************************************************************************************************
     */
    
    @Override
    public IBinder asBinder() {
        // TODO Auto-generated method stub
        return null;
    }

    /************************************************************************************************************************************
     */
    private class OnAudioRouteChangedTask extends AsyncNotificatonTask {
        private AudioState audioState;
        public OnAudioRouteChangedTask(CallbackWrapper wrapper, AudioState audioState) {
            super("OnAudioRouteChgNotifTask", wrapper);
            this.audioState = audioState;
        }
        @Override
        public void doNotification(IServicePJSIPCallback callback) throws RemoteException {
            callback.on_audio_route_changed(audioState);
        }
    }

    @Override
    public void on_audio_route_changed(AudioState audioState) {
        synchronized (this) {
            if (m_callback == null || m_callback.isEmpty()) {
                return;
            }
        }
        new OnAudioRouteChangedTask(this, audioState).executeNotification();
    }
    /************************************************************************************************************************************
     */
    private class HTTPRegStateTask extends AsyncNotificatonTask {
        private int code;

        public HTTPRegStateTask(CallbackWrapper wrapper, int code) {
            super("HTTPRegStateNotifTask", wrapper);
            this.code = code;
        }

        @Override
        public void doNotification(IServicePJSIPCallback callback) throws RemoteException {
            callback.http_registration_state(code);
        }
    }

    @Override
    public void http_registration_state(int code) {
        synchronized (this) {
            if (m_callback == null || m_callback.isEmpty()) {
                return;
            }
        }
        new HTTPRegStateTask(this, code).executeNotification();
    }
    
    /************************************************************************************************************************************
     */
    private class OnSipStackStartedTask extends AsyncNotificatonTask {
        private boolean result;

        public OnSipStackStartedTask(CallbackWrapper wrapper, boolean result) {
            super("OnSipStackStartedNotifTask", wrapper);
            this.result = result;
        }

        @Override
        public void doNotification(IServicePJSIPCallback callback) throws RemoteException {
            callback.on_sip_stack_started(result);
        }
    }

    @Override
    public void on_sip_stack_started(boolean result) {
        synchronized (this) {
            if (m_callback == null || m_callback.isEmpty()) {
                return;
            }
        }
        new OnSipStackStartedTask(this, result).executeNotification();
    }

    /************************************************************************************************************************************
     */
    private class OnTestStartedTask extends AsyncNotificatonTask {
        private int id;

        public OnTestStartedTask(CallbackWrapper wrapper, int id) {
            super("OnTestStartedNotifTask", wrapper);
            this.id = id;
        }

        @Override
        public void doNotification(IServicePJSIPCallback callback) throws RemoteException {
                callback.on_test_started(id);
        }
    }
        
        
    @Override
    public void on_test_started(int id) {
        synchronized (this) {
            if (m_callback == null || m_callback.isEmpty()) {
                return;
            }
        }
        new OnTestStartedTask(this, id).executeNotification();
    }

    /************************************************************************************************************************************
     */
    private class OnTestCompletedTask extends AsyncNotificatonTask {
        private int id;
        private int value;

        public OnTestCompletedTask(CallbackWrapper wrapper, int id, int value) {
            super("OnTestCompletedTask", wrapper);
            this.id = id;
            this.value = value;
        }

        @Override
        public void doNotification(IServicePJSIPCallback callback) throws RemoteException {
            callback.on_test_completed(id, value);
        }
    }

    @Override
    public void on_test_completed(int id, int value) {
        synchronized (this) {
            if (m_callback == null || m_callback.isEmpty()) {
                return;
            }
        }
        new OnTestCompletedTask(this, id, value).executeNotification();
    }

    /************************************************************************************************************************************
     */
    private class OnCallInfoChangedTask extends AsyncNotificatonTask {
        private long callId;

        public OnCallInfoChangedTask(CallbackWrapper wrapper, long callId) {
            super("OnCallInfoChangedTask", wrapper);
            this.callId = callId;
        }

        @Override
        public void doNotification(IServicePJSIPCallback callback) throws RemoteException {
            callback.on_call_info_changed(callId);
        }
    }

    @Override
    public void on_call_info_changed(long callId) throws RemoteException {
        synchronized (this) {
            if (m_callback == null || m_callback.isEmpty()) {
                return;
            }
        }
        new OnCallInfoChangedTask(this, callId).executeNotification();
    }

    /**
     * NEW "CENTRIC" SCHEME
     * ======================================================
     * ==========================================================
     */

    /**
     * SIProxy notifications
     */
    private class NotifyCallsStateChangedTask extends AsyncNotificatonTask {
        private RCCallInfo[] calls;

        public NotifyCallsStateChangedTask(CallbackWrapper wrapper, RCCallInfo[] calls) {
            super("NotifyCallsStateChangedTask", wrapper);
            this.calls = calls;
        }

        @Override
        public void doNotification(IServicePJSIPCallback callback) throws RemoteException {
            callback.notifyCallsStateChanged(calls);
        }
    }
    @Override
    public void notifyCallsStateChanged(final RCCallInfo[] calls) {
        synchronized (this) {
            if (m_callback == null || m_callback.isEmpty() || calls == null || calls.length == 0) {
                return;
            }
        }
        new NotifyCallsStateChangedTask(this, calls).executeNotification();
    }
};
