package com.rcbase.android.sip.audio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RecordWav {
    // 频率
    private int sampleRateInHz = 8000;
    // 缓冲区字节大小
    private int bufferSizeInBytes = 320;
    // 录制的声道数 一般为1或者2
    private int channels = 1;

    // AudioName裸音频数据文件
    private static final String RECORD_DIR = "mnt/sdcard/rc/record";
    private static final String TEMP_FILE = "mnt/sdcard/rc/record/record_wav_temp.raw";
    // 可播放的音频文件
    private String recordWavFile;
    private FileOutputStream fos;

    public RecordWav() {
        prepare();
    }

    public void prepare() {
        if (fos == null)
            try {
                File dir = new File(RECORD_DIR);

                if (!dir.exists()) {
                    dir.mkdir();
                }
                
                File file = new File(TEMP_FILE);
                if (file.exists()) {
                    file.delete();
                }
                fos = new FileOutputStream(file);// 建立一个可存取字节的文件
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    /**
     * 循环调用该方法，将byte[]连续写入文件中
     * 
     * @param audiodata
     */
    public void write(byte[] audiodata) {
        if (fos == null)
            prepare();
        try {
            fos.write(audiodata);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     * 关闭文件流，并将写入的文件转化成wav格式的文件
     */
    public void stop() {
        if (fos != null)
            try {
                fos.close();// 关闭写入流
                fos = null;
                copyWaveFile(TEMP_FILE, recordWavFile);
                File file = new File(TEMP_FILE);
                if (file.exists())
                    file.delete();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    /**
     * 这里得到可播放的音频文件
     * 
     * @param inFilename
     * @param outFilename
     */
    private void copyWaveFile(String inFilename, String outFilename) {

        if (outFilename == null) {
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm");
            outFilename = RECORD_DIR+"/" + formatter.format(date) + ".wav";
        }

        FileInputStream in = null;
        FileOutputStream out = null;
        long totalAudioLen = 0;
        long totalDataLen = totalAudioLen + 36;
        long longSampleRate = sampleRateInHz;
        long byteRate = 16 * sampleRateInHz * channels / 8;
        byte[] data = new byte[bufferSizeInBytes];
        try {
            in = new FileInputStream(inFilename);
            out = new FileOutputStream(outFilename);
            totalAudioLen = in.getChannel().size();
            totalDataLen = totalAudioLen + 36;
            WriteWaveFileHeader(out, totalAudioLen, totalDataLen, longSampleRate, channels, byteRate);
            while (in.read(data) != -1) {
                out.write(data);
            }
            in.close();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 这里提供一个头信息。插入这些信息就可以得到可以播放的文件。 为我为啥插入这44个字节，这个还真没深入研究，不过你随便打开一个wav 音频的文件，可以发现前面的头文件可以说基本一样哦。每种格式的文件都有 自己特有的头文件。
     */
    private void WriteWaveFileHeader(FileOutputStream out, long totalAudioLen, long totalDataLen, long longSampleRate,
            int channels, long byteRate) throws IOException {
        byte[] header = new byte[44];
        header[0] = 'R'; // RIFF/WAVE header
        header[1] = 'I';
        header[2] = 'F';
        header[3] = 'F';
        header[4] = (byte) (totalDataLen & 0xff);
        header[5] = (byte) ((totalDataLen >> 8) & 0xff);
        header[6] = (byte) ((totalDataLen >> 16) & 0xff);
        header[7] = (byte) ((totalDataLen >> 24) & 0xff);
        header[8] = 'W';
        header[9] = 'A';
        header[10] = 'V';
        header[11] = 'E';
        header[12] = 'f'; // 'fmt ' chunk
        header[13] = 'm';
        header[14] = 't';
        header[15] = ' ';
        header[16] = 16; // 4 bytes: size of 'fmt ' chunk
        header[17] = 0;
        header[18] = 0;
        header[19] = 0;
        header[20] = 1; // format = 1
        header[21] = 0;
        header[22] = (byte) channels;
        header[23] = 0;
        header[24] = (byte) (longSampleRate & 0xff);
        header[25] = (byte) ((longSampleRate >> 8) & 0xff);
        header[26] = (byte) ((longSampleRate >> 16) & 0xff);
        header[27] = (byte) ((longSampleRate >> 24) & 0xff);
        header[28] = (byte) (byteRate & 0xff);
        header[29] = (byte) ((byteRate >> 8) & 0xff);
        header[30] = (byte) ((byteRate >> 16) & 0xff);
        header[31] = (byte) ((byteRate >> 24) & 0xff);
        header[32] = (byte) (2 * 16 / 8); // block align
        header[33] = 0;
        header[34] = 16; // bits per sample
        header[35] = 0;
        header[36] = 'd';
        header[37] = 'a';
        header[38] = 't';
        header[39] = 'a';
        header[40] = (byte) (totalAudioLen & 0xff);
        header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
        header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
        header[43] = (byte) ((totalAudioLen >> 24) & 0xff);
        out.write(header, 0, 44);
    }

    public int getSampleRateInHz() {
        return sampleRateInHz;
    }

    public void setSampleRateInHz(int sampleRateInHz) {
        this.sampleRateInHz = sampleRateInHz;
    }

    public int getBufferSizeInBytes() {
        return bufferSizeInBytes;
    }

    public void setBufferSizeInBytes(int bufferSizeInBytes) {
        this.bufferSizeInBytes = bufferSizeInBytes;
    }

    public String getRecordWavFile() {
        return recordWavFile;
    }

    public void setRecordWavFile(String recordWavFile) {
        this.recordWavFile = recordWavFile;
    }

    public int getChannels() {
        return channels;
    }

    public void setChannels(int channels) {
        this.channels = channels;
    }

}
