/**
 * Copyright (C) 2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import java.util.Arrays;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * <code>CodecInfo</code> provides convert for codec description
 */
public class CodecInfo implements Parcelable {
    /**
     * Defines PJSIP codec.
     */
    public static enum Codec {
        UNKNOWN(0, "Unknown", 0,   0,   "----", "Unknown"     ),
        ILBC   (1, "iLBC",    50,  200, "iLBC", "iLBC/8000/1" ),
        PCMU   (2, "PCMU",    200, 100, "PCMU", "PCMU/8000/1" ),
        PCMA   (3, "PCMA",    150, 50,  "PCMA", "PCMA/8000/1" ),
        GSM    (4, "GSM",     100, 150, "GSM",  "GSM/8000/1"  ),
        
        G722   (5, "G.722",   245,   5,   "G722", "G722/16000/1"),
        SILK (6, "SILK", 245, 245, "SILK",  "SILK/8000/1");
        /*SILKNB   (6, "SILK_NB",    240,  240,"SILK",  "SILK/8000/1"),
        SILKMB (7, "SILK_MB",     210, 210, "SILK",  "SILK/12000/1"),
        SILKWB (8, "SILK_WB",     220, 220, "SILK",  "SILK/16000/1"),
        SILKUWB (9, "SILK_UWB",     230, 230, "SILK",  "SILK/24000/1"); 
        */
        /**
         * HIGHEST = 255, NEXT_HIGHER = 254, NORMAL = 128, LOWEST = 1, DISABLED = 0 
         */
        public static final int MIN_PRIORITY = 0;
        public static final int DISABLED     = MIN_PRIORITY;
        public static final int MAX_PRIORITY = 255;
        
        /**
         * Private constructor
         */
        private Codec(int codecId, String label, int wideBandPriority, int narrowBandPriority, String pjsipPrefix, String pjsipValue) {
            this.label = label;
            this.pjsipPrefix = pjsipPrefix;
            this.pjsipValue  = pjsipValue;
            this.narrowBandPriority = narrowBandPriority;
            this.wideBandPriority = wideBandPriority;
            this.codecId = codecId;
        }
        
        /**
         * Returns unique codec identifier.
         * 
         * @return unique codec identifier
         */
        public int getCodecIdentifier() {
            return codecId;
        }
        
        /**
         * Find codec by identifier (see {@link #getCodecIdentifier()}
         * 
         * @param codec identifier
         * 
         * @return found codec or {@link #UNKNOWN}
         */
        public static Codec findByIdentifier(int identifier) {
            for (Codec codec : Codec.values()) {
                if (identifier == codec.codecId) {
                    return codec;
                }
            }
            return UNKNOWN;
        }
        
        /**
         * Keeps unique codec identifier.
         */
        private int codecId;
        
        /**
         * Keeps codec name for logging
         */
        private String label;

        /**
         * Keeps narrow band priority.
         */
        private int narrowBandPriority;
        
        /**
         * Keeps wide band priority.
         */
        private int wideBandPriority;

        /**
         * Returns default priority for wide-band.
         * 
         * @return Returns default priority for wide-band
         */
        public int getWideBandDefaultPriority() {
            return wideBandPriority;
        }
        
        /**
         * Returns default priority for narrow-band.
         * 
         * @return Returns default priority for narrow-band
         */
        public int getNarrowBandDefaultPriority() {
            return narrowBandPriority;
        }
        
        /**
         * Keeps prefix to identify codec by description returned by PJSIP.
         */
        private String pjsipPrefix;
        
        /**
         * Keeps description returned by PJSIP.
         */
        private String pjsipValue;
        
        /**
         * Returns PJSIP value.
         * 
         * @return PJSIP value
         */
        public String getPjsipValue() {
            return pjsipValue;
        }
        
        /**
         * Find codec by description returned from PJSIP.
         * 
         * @param pjsipValue description returned from PJSIP
         * 
         * @return found codec or {@link #UNKNOWN}
         */
        public static Codec findByPjsipValue(String pjsipValue) {
            if (pjsipValue != null && (pjsipValue.trim().length() > 0)) {
                for (Codec codec : Codec.values()) {
                    if (codec == UNKNOWN) {
                        continue;
                    }
                    if (pjsipValue.startsWith(codec.pjsipPrefix)) {
                        return codec;
                    }
                }
            }
            return UNKNOWN;
        }
        
        /**
         * Returns default codec set for wide-band usage.
         * 
         * @return default codec set for wide-band usage.
         */
        public static CodecInfo[] getDefaultsForWideBand() {
           CodecInfo[] codec = new CodecInfo[6];
           codec[0] = new CodecInfo(ILBC, ILBC.getWideBandDefaultPriority());
           codec[1] = new CodecInfo(PCMU, PCMU.getWideBandDefaultPriority());
           codec[2] = new CodecInfo(PCMA, PCMA.getWideBandDefaultPriority());
           codec[3] = new CodecInfo(GSM,  GSM.getWideBandDefaultPriority());
           codec[4] = new CodecInfo(SILK,  SILK.getWideBandDefaultPriority());
           /*
           codec[4] = new CodecInfo(SILKNB,  SILKNB.getWideBandDefaultPriority());
           codec[5] = new CodecInfo(SILKMB,  SILKMB.getWideBandDefaultPriority());
           codec[6] = new CodecInfo(SILKWB,  SILKWB.getWideBandDefaultPriority());
           codec[7] = new CodecInfo(SILKUWB,  SILKUWB.getWideBandDefaultPriority());
           */
           codec[5] = new CodecInfo(G722,  G722.getWideBandDefaultPriority());
           Arrays.sort(codec, new CodecInfoComparator());
           return codec;
        }
        
        /**
         * Returns default codec set for narrow-band usage.
         * 
         * @return default codec set for narrow-band usage.
         */
        public static CodecInfo[] getDefaultsForNarrowBand() {
           CodecInfo[] codec = new CodecInfo[6];
           codec[0] = new CodecInfo(ILBC, ILBC.getNarrowBandDefaultPriority());
           codec[1] = new CodecInfo(PCMU, PCMU.getNarrowBandDefaultPriority());
           codec[2] = new CodecInfo(PCMA, PCMA.getNarrowBandDefaultPriority());
           codec[3] = new CodecInfo(GSM,  GSM.getNarrowBandDefaultPriority());
           codec[4] = new CodecInfo(SILK,  SILK.getWideBandDefaultPriority());
           /*
           codec[4] = new CodecInfo(SILKNB,  SILKNB.getNarrowBandDefaultPriority());
           codec[5] = new CodecInfo(SILKMB,  SILKMB.getNarrowBandDefaultPriority());
           codec[6] = new CodecInfo(SILKWB,  SILKWB.getNarrowBandDefaultPriority());
           codec[7] = new CodecInfo(SILKUWB,  SILKUWB.getNarrowBandDefaultPriority());*/
           codec[5] = new CodecInfo(G722,  G722.getNarrowBandDefaultPriority());
           Arrays.sort(codec, new CodecInfoComparator());
           return codec;
        }
        
        @Override public String toString() {
            return "Codec(" + label + "[wb:" + wideBandPriority + "][nb:" + narrowBandPriority + "] " + pjsipValue + ")";
        }
        
        /**
         * Returns "user-friendly" codec name for logging
         * 
         * @return "user-friendly" codec name for logging
         */
        public String getName() {
            return label;
        }
    }
    
    /**
     * Keeps codec.
     */
    private Codec mCodec;

    /**
     * Keeps PJSIP description codec value.
     */
    private String mPjsipValue;

    /**
     * Returns PJSIP value.
     * 
     * @return PJSIP value
     */
    public String getPjsipValue() {
        return mPjsipValue;
    }

    /**
     * Keeps current/new priority.
     */
    private int mPriority = Codec.DISABLED;

    /**
     * Returns codec priority.
     * 
     * @return codec priority
     */
    public int getPriority() {
        return mPriority;
    }
    
    /**
     * Returns codec.
     * 
     * @return codec
     */
    public Codec getCodec() {
        return mCodec;
    }
    

    /**
     * Sets codec priority.
     * 
     * @param priority to be set
     * 
     * @return priority was set
     */
    public int setPriority(int priority) {
        if (priority < Codec.MIN_PRIORITY) {
            mPriority = Codec.MIN_PRIORITY;
        } else if (priority > Codec.MAX_PRIORITY) {
            mPriority = Codec.MAX_PRIORITY;
        } else {
            mPriority = priority;
        }
        return mPriority;
    }

    /**
     * Parcelable creator.
     */
    public static final Parcelable.Creator<CodecInfo> CREATOR = new Parcelable.Creator<CodecInfo>() {

        public CodecInfo createFromParcel(Parcel in) {
            return new CodecInfo(in);
        }

        public CodecInfo[] newArray(int size) {
            return new CodecInfo[size];
        }
    };

    /**
     * Create codec info by PJSIP value and set priority.
     * 
     * @param pjsipValue
     *            PJSIP description codec value
     * @param priority
     *            priority to be set [0..255]
     */
    public CodecInfo(String pjsipValue, int priority) {
        mCodec = Codec.findByPjsipValue(pjsipValue);
        mPjsipValue = pjsipValue;
        setPriority(priority);
    }

    /**
     * Default constructor.
     * 
     * @param codecId
     *            {@link SIPConstants.Codec} codec
     * 
     * @param priority
     *            priority to be set [0..255]
     */
    public CodecInfo(Codec codecId, int priority) {
        mCodec = codecId;
        mPjsipValue = mCodec.getPjsipValue();
        setPriority(priority);
    }

    public CodecInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mCodec.getCodecIdentifier());
        dest.writeString(mPjsipValue);
        dest.writeInt(mPriority);
    }

    public void readFromParcel(Parcel in) {
        mCodec = Codec.findByIdentifier(in.readInt());
        mPjsipValue = in.readString();
        mPriority = in.readInt();
    }

    @Override
    public String toString() {
        return toString2() + ", Priority: " + getPriority();
    }
    
    public String toString2() {
        return mCodec.getName() + " PJSIP:" + (mPjsipValue == null ? "NULL" : mPjsipValue);
    }
}
