/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.client.RCSIP;
import com.rcbase.android.sip.client.RCSIP.SipCode;
import com.rcbase.android.sip.service.WrapPJSIP.PJSIPCallInfo;
import com.ringcentral.android.LogSettings;

/**
 * RC Call Manager.
 */
public class CallManager {
    /**
     * Tag for logging.
     */
	private final static String TAG = "[RC]CallManager";
	
	/**
	 * Keeps current calls
	 */
	private Map<Long, RCCallInfo> m_calls = new HashMap<Long, RCCallInfo>();
	
	/**
	 * Keeps latest call identifier.
	 */
	private long m_lastRCCallId = 0; 
	
	/**
	 * Defines the number of latest VoIP calls to be kept.
	 */
	private static final int MAX_NUMBER_OF_LATEST_CALLS = 20;
	
	/**
	 * Keeps latest finished VoIP calls.
	 */
	private Vector<RCCallInfo> mLatestFinishedCalls = new Vector<RCCallInfo>();
	
	/**
	 * Returns latest finished RC VoIP calls.
	 * 
	 * @return finished RC VoIP calls
	 */
	public RCCallInfo[] getLatestFinishedCalls() {
		synchronized (this) {
			if (mLatestFinishedCalls == null
					|| mLatestFinishedCalls.size() == 0) {
				return null;
			}
			RCCallInfo[] ret = new RCCallInfo[mLatestFinishedCalls.size()];
			return mLatestFinishedCalls.toArray(ret);
		}
	}
	/**
	 * Cleans-up latest finished calls.
	 */
	public void cleanupLatestFinishedCalls() {
	    synchronized (this) {
	        if (mLatestFinishedCalls == null
                    || mLatestFinishedCalls.size() == 0) {
                return;
            }
	        mLatestFinishedCalls = new Vector<RCCallInfo>();
	        System.gc();
	    }
	}
	
	/**
	 * Returns next unique call identifier.
	 * 
	 * @return next unique call identifier.
	 */
	private long getNextRCCallId(){
		synchronized (this) {
			if (m_lastRCCallId >= Long.MAX_VALUE) {
				m_lastRCCallId = 0;
			}
			return ++m_lastRCCallId;
		}
	}
	
	public CallManager(){
		
	}
	
	/**
	 * Creates incoming call and and add to active.
	 * 
	 * @param pjsipCallId pjsip call identifier
	 * @param number the party URI
	 * @return RC Call instance
	 */
    public RCCallInfo createIncomingCall(final int pjsipCallId, final String partyURI) {
        RCCallInfo rcCallInfo = null;
        long rcCallId = PJSIP.sf_INVALID_RC_CALL_ID;
        synchronized (this) {
            rcCallId = getNextRCCallId();
            rcCallInfo = new RCCallInfo(rcCallId, pjsipCallId, partyURI);
            m_calls.put(new Long(rcCallId), rcCallInfo);
        }
        if (LogSettings.MARKET) {
            MktLog.e(TAG, "INCOMING CALL:" + rcCallInfo);
        }
        ServicePJSIP.getInstance().ensurePeriodicalCallsConcistencyCheckRunnedStopped(true);
        return rcCallInfo;
    }
    
    /**
     * Returns normalized string for logging.
     */
    private static String normalizedStringForDump(String string) {
        if (string == null) {
            return "null";
        } else {
            return string;
        }
    }
	
	/**
	 * Creates an outgoing call.
	 * 
	 * @param number called number
	 * @param name name if exists
	 * @return the call identifier
	 */
    public long createCall(String number, String name) {
        long rcCallId = PJSIP.sf_INVALID_RC_CALL_ID;
        RCCallInfo call = null;
        synchronized (this) {
            rcCallId = getNextRCCallId();
            call = new RCCallInfo(rcCallId, number, name);
            m_calls.put(new Long(rcCallId), call);
        }
        if (LogSettings.MARKET) {
            MktLog.e(TAG, "OUTBOUND CALL: " + call);
        }
        ServicePJSIP.getInstance().ensurePeriodicalCallsConcistencyCheckRunnedStopped(true);
        return rcCallId;
    }
	
	/**
	 * Remove call from list
	 */
	void deleteCall( final long rcCallId ){
        if (LogSettings.MARKET) {
        	MktLog.w(TAG, "deleteCall RC CallId " + rcCallId );
        }
        synchronized (this) {
            Long callId = new Long(rcCallId);
            if (m_calls.containsKey(callId)) {
                RCCallInfo call = m_calls.get(callId);
                if (call != null) {
                    if (call.isAlive()) {
                        call.onCallStatusChange(false, RCSIP.PJSUA_CALL_STATE_DISCONNECTED, RCSIP.SipCode.PJSIP_SC_ERROR.code(), null);
                        ensureClientNotified(call, true, true);
                    }
                    call.setDeletedTime();
                    if (mLatestFinishedCalls.size() >= MAX_NUMBER_OF_LATEST_CALLS) {
                        try {
                            mLatestFinishedCalls.remove(mLatestFinishedCalls.firstElement());
                        } catch (java.lang.Throwable th) {
                        }
                    }
                    if (mLatestFinishedCalls.size() >= MAX_NUMBER_OF_LATEST_CALLS) {
                        mLatestFinishedCalls = new Vector<RCCallInfo>();
                    }
                    mLatestFinishedCalls.add(call);
                    if (LogSettings.MARKET) {
                        MktLog.d(TAG, "Deleted dump RC CallId:" + rcCallId);
                        MktLog.d(TAG, call.dump());
                    }
                }
                m_calls.remove(callId);
            }
        }
    }
	
//	/**
//	 * Set call RC From field is received as custom header on INVITE
//	 */
//	public synchronized void setRCFrom( final long rcCallId, final String rcFrom ){
//		
//        if (LogSettings.MARKET) {
//        	MktLog.w(TAG, "setRCFrom Call ID: " + rcCallId + " RCFrom:" + (rcFrom == null ? "" : rcFrom ));
//        }
//        
//		if( m_calls.containsKey(new Long(rcCallId))){
//			m_calls.get(new Long(rcCallId)).setRcFrom(rcFrom);
//		}
//	}
	
//	/*
//	 * Mark call as initiated
//	 */
//	public synchronized void markAsInitiated( final long rcCallId ){
//		
//        if (LogSettings.MARKET) {
//        	MktLog.i(TAG, "markAsInitiated RC Call ID: " + rcCallId );
//        }
//        
//		if( m_calls.containsKey(new Long(rcCallId))){
//			m_calls.get(new Long(rcCallId)).onInitiatedState();
//		}
//	}
    
//	private synchronized void setCallId(final long rcCallId, final int pjsipCallId ){
//        if (LogSettings.MARKET) {
//        	MktLog.w(TAG, "setCallId RC Call ID: " + rcCallId + " SIP Call ID: " + pjsipCallId );
//        }
//        
//		if( m_calls.containsKey(new Long(rcCallId))){
//			m_calls.get(new Long(rcCallId)).setPJSIPCallId(pjsipCallId);
//		}
//	}
	
	/**
	 * Detect RC Call ID by PJSIP Call ID
	 */
	protected final RCCallInfo getCallByRCCallId( final long rcCallId ){
		
		final Long key = new Long( rcCallId );
		
		synchronized( this ){
			if( m_calls.containsKey(key)){
				return m_calls.get( key );
			}
		}
		
		return null;
	}
	
	/**
	 * Get call information by RC Call ID
	 */
	public int call_get_info( final long rcCallId, CallInfo callInfo ){
		final RCCallInfo rcCallInfo = getCallByRCCallId( rcCallId );
		if( rcCallInfo == null ){
			return PJSIP.sf_PJ_EINVAL;
		}

		if (LogSettings.MARKET) {
            MktLog.d(TAG, "call_get_info: " + rcCallInfo);
        }
		
		PJSIPCallInfo pjsipCallInfo = null;
		
		/*
		 * If call is not in initiated state (PJSIP call ID is present)
		 * then load call information from PJSIP level
		 */
		
		int result = PJSIP.sf_PJ_SUCCESS;
		if( !rcCallInfo.isSipNotInitiatedState()) {
		    pjsipCallInfo = new PJSIPCallInfo();
		    result = ServicePJSIP.getInstance().getWrapper().PJSIP_get_call_info(rcCallInfo.getPJSIPCallId(), pjsipCallInfo);
			if( result != PJSIP.sf_PJ_SUCCESS){
		        if (LogSettings.MARKET) {
		        	MktLog.e(TAG, "Failed call_get_info RC CallId: " + rcCallId + " Result:" + result);
		        }
			}
		}
		
		if (result != PJSIP.sf_PJ_SUCCESS) {
		    rcCallInfo.setMainError(RCSIP.SipCode.PJSIP_SC_SIP_CALL_FAILED_STATUS_CODE_EXTENDED);
            rcCallInfo.setEnding(RCCallInfo.CALL_ENDED_BY_ERROR);
            rcCallInfo.setMediaState(RCSIP.PJSUA_CALL_MEDIA_NONE);
            rcCallInfo.onCallStatusChange(false, RCSIP.PJSUA_CALL_STATE_DISCONNECTED, 
                    RCSIP.SipCode.PJSIP_SC_CALL_TSX_DOES_NOT_EXIST.code(), null);
            pjsipCallInfo = null;
		}
		
		
		if (pjsipCallInfo != null) {
		    if (LogSettings.MARKET) {
                MktLog.d(TAG, "call_get_info RC CallId: " + pjsipCallInfo.minDescription());
            }
		    synchronized (this) {
	            rcCallInfo.onCallStatusChange(false, pjsipCallInfo.callSate, pjsipCallInfo.statusCode, null);
	            rcCallInfo.setMediaState(pjsipCallInfo.mediaState);
	        }
            callInfo.setPjsipCallId(pjsipCallInfo.callId);
            callInfo.setAccountId(pjsipCallInfo.accountId);
            callInfo.setLocalURI(pjsipCallInfo.localURI);
            callInfo.setLocalContact(pjsipCallInfo.localContact);
            callInfo.setRemoteURI(pjsipCallInfo.remoteURI);
            callInfo.setRemoteContact(pjsipCallInfo.remoteContact);
            callInfo.setDialogCallId(pjsipCallInfo.dialogCallId);
            callInfo.setSipMsg(pjsipCallInfo.sipMsg);
            callInfo.setCall_state(pjsipCallInfo.callSate);
            callInfo.setMedia_state(pjsipCallInfo.mediaState);
            callInfo.setLastStatus(pjsipCallInfo.statusCode);
            callInfo.setHold(pjsipCallInfo.onHold);
		}
        
        callInfo.setClientId(ServicePJSIP.getInstance().getUserInfo().getInstanceId());
        callInfo.setRcCallId(rcCallId);
		callInfo.setFinishing(rcCallInfo.isEnding());
		callInfo.setEndingReason(rcCallInfo.getEndingReason());
		callInfo.setCallStartTime(  rcCallInfo.getCreationTime());
		callInfo.setCallAcceptedTime(  rcCallInfo.getConfirmationTime());
		callInfo.setNetworkType(rcCallInfo.getNetworkType());		
		callInfo.setBindedContact(rcCallInfo.getBindedContact());
		callInfo.setPhotoID(rcCallInfo.getPhotoID());
		
		if( rcCallInfo.getContact() != null ){
			callInfo.setRemoteContact(rcCallInfo.getContact());
		}
		if( rcCallInfo.getPartyUri() != null ){
			callInfo.setRemoteURI(rcCallInfo.getPartyUri());
		}
		
		ensureClientNotified(rcCallInfo, true, true);

        if (result != PJSIP.sf_PJ_SUCCESS) {
            ServicePJSIP.getInstance().updateActiveCallsCounter();
            ServicePJSIP.getInstance().getPJSIPCallbackListener().on_call_info_changed(PJSIP.sf_INVALID_RC_CALL_ID);
        }
		return result;
	}
	
	private synchronized Object[] getKeys(){
		return m_calls.keySet().toArray();
	}
	
	/**
	 * Find outbound call on initiating state
	 */ 
	public RCCallInfo getOutboundOnInitiatingState(boolean onlyInCreatedState){
		synchronized( this ){
		    final Object[] keys = getKeys();
			for( int i = 0; i < keys.length; i++ ){
				final Long value = (Long)keys[i];
				final RCCallInfo rcCallInfo = m_calls.get( value.longValue() );
				if( rcCallInfo != null ){
				    if (LogSettings.MARKET) {
				    	MktLog.d(TAG, "getOutboundOnInitiatingState: " + rcCallInfo);
				    }
					
					/*
					 * Without SIP call ID  & Not incoming & Not Ending & Not initiated 
					 */
					if( rcCallInfo.isAlive() && 
					        !rcCallInfo.isIncoming() && 
							rcCallInfo.isSipNotInitiatedState() && 
							!rcCallInfo.isEnding() &&
							((rcCallInfo.getState() == RCSIP.PJSUA_CALL_STATE_CREATED) ||
							(rcCallInfo.getState() == RCSIP.PJSUA_CALL_STATE_INIT && !onlyInCreatedState))){
                        if (LogSettings.MARKET) {
                            MktLog.d(TAG, "getOutboundOnInitiatingState:Found:" + rcCallInfo);
                        }
						return rcCallInfo;
					}
				}
			}
		}
		if (LogSettings.MARKET) {
            MktLog.d(TAG, "getOutboundOnInitiatingState:NONE");
        }
		return null;
	}
	
	/**
	 * 
	 */
	public long getCallByRCFrom( final String rcFrom ){
		
		if( rcFrom == null )
			return PJSIP.sf_INVALID_RC_CALL_ID;
		
		final Object[] keys = getKeys();
		
		synchronized( this ){
			for( int i = 0; i < keys.length; i++ ){
				final Long value = (Long)keys[i];
				final RCCallInfo rcCallInfo = m_calls.get( value.longValue() );
				if( rcCallInfo != null ){
					if( rcCallInfo.isIncoming() && 
							rcCallInfo.getRcFrom() != null &&
							rcCallInfo.getRcFrom().equals(rcFrom)){
						
						return value.longValue();
					}
				}
			}
		}

		return PJSIP.sf_INVALID_RC_CALL_ID;
	}
	
	/**
	 * Hang-ups all active calls.
	 * 
	 * @param reason
     *            the reason of ending {@link RCCallInfo#CALL_ENDED_BY_USER},
     *            {@link RCCallInfo#CALL_ENDED_BY_PARTY},
     *            {@link RCCallInfo#CALL_ENDED_BY_SYSTEM},
     *            {@link RCCallInfo#CALL_ENDED_BY_ERROR}
	 */
	public void call_hangup_all(int reason, SipCode error){
	    if (LogSettings.MARKET) {
            MktLog.w(TAG, "HANGUP ALL CALLS");
        }

        Object[] keys = null;
        Vector<RCCallInfo> sipClientNotifications = new Vector<RCCallInfo>();
        synchronized (this) {
            keys = getKeys();
            for (int i = 0; i < keys.length; i++) {
                final Long value = (Long) keys[i];
                final RCCallInfo rcCallInfo = m_calls.get(value.longValue());
                if (rcCallInfo != null) {
                    rcCallInfo.setEnding(reason);
                    rcCallInfo.setMainError(error);
                    rcCallInfo.unBindPSIPCall();
                    int last_status = RCSIP.SipCode.PJSIP_SC_UNKNOWN_STATUS_CODE_EXTENDED.code();
                    if (error != null) {
                        last_status = error.code();
                    }
                    rcCallInfo.onCallStatusChange(false, RCSIP.PJSUA_CALL_STATE_DISCONNECTED, last_status, null);
                    deleteCall(rcCallInfo.getRcCallId());
                    sipClientNotifications.add(rcCallInfo);
                }
            }
        }
		
		for (RCCallInfo call : sipClientNotifications) {
		    ensureClientNotified(call, true, true);
		}
		
		if( keys.length > 0 ) {
	    	ServicePJSIP.getInstance().updateActiveCallsCounter();
			ServicePJSIP.getInstance().getPJSIPCallbackListener().on_call_info_changed(PJSIP.sf_INVALID_RC_CALL_ID);
			ServicePJSIP.getInstance().endCallBeep();
		}

        if (!ServicePJSIP.getInstance().getState().isSipStackCreated()) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "Failed! SIP Stack is not loaded yet.");
            }
            return;
        }
        ServicePJSIP.getInstance().getWrapper().PJSIP_call_hangup_all();
	}
	
	/**
	 * Forced hang-ups all active calls.
	 * 
	 * @param reason
     *            the reason of ending {@link RCCallInfo#CALL_ENDED_BY_USER},
     *            {@link RCCallInfo#CALL_ENDED_BY_PARTY},
     *            {@link RCCallInfo#CALL_ENDED_BY_SYSTEM},
     *            {@link RCCallInfo#CALL_ENDED_BY_ERROR}
	 */
    public void call_hangup_all_forced(int reason, SipCode mainError, SipCode disconectCode) {
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "HANGUP FORCED ALL CALLS ");
        }

        Object[] keys = null;
        Vector<RCCallInfo> sipClientNotifications = new Vector<RCCallInfo>();
        synchronized (this) {
            keys = getKeys();
            for (int i = 0; i < keys.length; i++) {
                final Long value = (Long) keys[i];
                final RCCallInfo rcCallInfo = m_calls.get(value.longValue());
                if (rcCallInfo != null) {
                    rcCallInfo.setEnding(reason);
                    rcCallInfo.setMainError(mainError);
                    rcCallInfo.onCallStatusChange(false, RCSIP.PJSUA_CALL_STATE_DISCONNECTED, disconectCode.code(), null);
                    sipClientNotifications.add(rcCallInfo);
                    rcCallInfo.unBindPSIPCall();
                    deleteCall(rcCallInfo.getRcCallId());
                }
            }
        }
        
        for (RCCallInfo call : sipClientNotifications) {
            ensureClientNotified(call, true, true);
        }

        if (keys.length > 0) {
            ServicePJSIP.getInstance().updateActiveCallsCounter();
            ServicePJSIP.getInstance().getPJSIPCallbackListener()
                    .on_call_info_changed(PJSIP.sf_INVALID_RC_CALL_ID);
            ServicePJSIP.getInstance().endCallBeep();
        }
        
        if (ServicePJSIP.getInstance().getState().isSipStackCreated()) {
            ServicePJSIP.getInstance().getWrapper().PJSIP_call_hangup_all();
        }
        
    }
	
	/**
	 * Hang-up call
	 * 
	 * @param reason
     *            {@link #CALL_ENDED_BY_USER}, {@link #CALL_ENDED_BY_PARTY},
     *            {@link #CALL_ENDED_BY_SYSTEM}, {@link #CALL_ENDED_BY_ERROR}
	 */
	public int call_hangup(final long rcCallId, final int reason, final int code, final String[] headers ){
		
        
        
        int result = PJSIP.sf_PJ_SUCCESS;
		
		final RCCallInfo rcCallInfo = getCallByRCCallId( rcCallId );
		if( rcCallInfo == null ){
			return PJSIP.sf_PJ_EINVAL;
		}

		if (LogSettings.MARKET) {
            MktLog.i(TAG, "call_hangup: " + rcCallInfo);
        }
		
		rcCallInfo.setEnding(reason);

		if( !rcCallInfo.isIncoming()){
			ServicePJSIP.getInstance().stopRings();
		}
		
		if( rcCallInfo.isSipNotInitiatedState() || rcCallInfo.unBoundPSIPCall()){
            rcCallInfo.onCallStatusChange(false, RCSIP.PJSUA_CALL_STATE_DISCONNECTED, code, null);
			deleteCall( rcCallId );
			ensureClientNotified(rcCallInfo, true, true);
	    	ServicePJSIP.getInstance().updateActiveCallsCounter();
		}
		else{
		    rcCallInfo.unBindPSIPCall();
		    ensureClientNotified(rcCallInfo, true, true);
			result =  ServicePJSIP.getInstance().getWrapper().PJSIP_call_hangup(rcCallInfo.getPJSIPCallId(), code, headers );
		}
		
		ServicePJSIP.getInstance().getPJSIPCallbackListener().on_call_info_changed(rcCallId);
		return result;
	}
	
	/**
	 * Call answer
	 */
	public int call_answer( final long rcCallId, final int code, final String[] headers ){
		
        if (LogSettings.MARKET) {
        	MktLog.i(TAG, "call_answer RC Call ID: " + rcCallId );
        }
        
		final RCCallInfo rcCallInfo = getCallByRCCallId( rcCallId );
		if( rcCallInfo == null ){
			return PJSIP.sf_PJ_EINVAL;
		}
		
		int pjsipCall = PJSIP.sf_INVALID_CALL_ID;
		synchronized (this) {
		    if (LogSettings.MARKET) {
	            MktLog.d(TAG, "call_answer " + rcCallInfo.toString());
	        }    
		    if (rcCallInfo.isAlive() && !rcCallInfo.isEnding()) {
		        if (rcCallInfo.getTimeInitiated() == 0) {
		            rcCallInfo.onInitiatedState();
		        }
		        if( !rcCallInfo.isSipNotInitiatedState()){
		            pjsipCall = rcCallInfo.getPJSIPCallId();
		        }
		    }
		}
		
		if( pjsipCall != PJSIP.sf_INVALID_CALL_ID){
		    // TODO: Error processing
		    return ServicePJSIP.getInstance().getWrapper().PJSIP_call_answer(pjsipCall, code, headers );
		} else {
		    if (LogSettings.MARKET) {
                MktLog.e(TAG, "call_answer RC Call ID: " + rcCallId + " no PJSIP handle");
            }
		}
		
		return PJSIP.sf_PJ_EINVALIDOP;
	}
	
	/**
	 * Hold call
	 */
	public int call_hold( final long rcCallId ){
		
        if (LogSettings.MARKET) {
        	MktLog.i(TAG, "call_hold RC Call ID: " + rcCallId );
        }
        
		final RCCallInfo rcCallInfo = getCallByRCCallId( rcCallId );
		if( rcCallInfo == null ){
			return PJSIP.sf_PJ_EINVAL;
		}
		
		if( !rcCallInfo.isSipNotInitiatedState()){
			return ServicePJSIP.getInstance().getWrapper().PJSIP_call_hold(rcCallInfo.getPJSIPCallId());
		}
		
		return PJSIP.sf_PJ_EINVALIDOP;
	}

	/**
	 * UnHold call
	 */
	public int call_unhold( final long rcCallId ){
		
        if (LogSettings.MARKET) {
        	MktLog.i(TAG, "call_unhold Call ID: " + rcCallId );
        }
        
		final RCCallInfo rcCallInfo = getCallByRCCallId( rcCallId );
		if( rcCallInfo == null ){
			return PJSIP.sf_PJ_EINVAL;
		}
		
		if( !rcCallInfo.isSipNotInitiatedState()){
			return ServicePJSIP.getInstance().getWrapper().PJSIP_call_unhold(rcCallInfo.getPJSIPCallId());
		}
		
		return PJSIP.sf_PJ_EINVALIDOP;
	}

	/**
	 * Attach sound call
	 */
	public int call_attach_sound( final long rcCallId ){
		
		final RCCallInfo rcCallInfo = getCallByRCCallId( rcCallId );
		if( rcCallInfo == null || !rcCallInfo.isAlive()){
			return PJSIP.sf_PJ_EINVAL;
		}
		
		if( !rcCallInfo.isSipNotInitiatedState()){
			return ServicePJSIP.getInstance().getWrapper().PJSIP_call_attach_sound(rcCallInfo.getPJSIPCallId());
		}
		
		return PJSIP.sf_PJ_EINVALIDOP;
	}

	/**
	 * Detach sound call
	 */
	public int call_detach_sound( final long rcCallId ){
		if (LogSettings.MARKET) {
            MktLog.i(TAG, "call_detach_sound RC Call ID: " + rcCallId );
        }
		final RCCallInfo rcCallInfo = getCallByRCCallId( rcCallId );
		if( rcCallInfo == null ){
			return PJSIP.sf_PJ_EINVAL;
		}
		
		if( !rcCallInfo.isSipNotInitiatedState()){
			return ServicePJSIP.getInstance().getWrapper().PJSIP_call_media_disconnect(rcCallInfo.getPJSIPCallId());
		}
		
		return PJSIP.sf_PJ_EINVALIDOP;
	}
	
	/**
	 * Find RC Call ID by PJSIP call ID
	 */
	public synchronized long getRcCallIdByPjsipCallId( final int pjsipCallId ){
	
		if( !m_calls.isEmpty()){
			Set<Long> keys = m_calls.keySet();
			Iterator<Long> itr = keys.iterator();
			while( itr.hasNext()){
				final RCCallInfo rcCallInfo = m_calls.get( itr.next() );
				if(rcCallInfo.getPJSIPCallId() == pjsipCallId ){
					return rcCallInfo.getRcCallId();
				}
			}
		}
		
		return PJSIP.sf_INVALID_RC_CALL_ID;
	}

	/**
	 * Returns RC Call by PJSIP CallId
	 * 
	 * @param pjsipCallId the PJSIP call
	 * 
	 * @return RC Call or null if not found
	 */
    public synchronized RCCallInfo getRcCallByPjsipCallId( final int pjsipCallId ){
        if( !m_calls.isEmpty()){
            Set<Long> keys = m_calls.keySet();
            Iterator<Long> itr = keys.iterator();
            while( itr.hasNext()){
                final RCCallInfo rcCallInfo = m_calls.get( itr.next() );
                if(rcCallInfo.getPJSIPCallId() == pjsipCallId ){
                    return rcCallInfo;
                }
            }
        }
        return null;
    }

    /**
     * Returns current RC VoIP calls.
     * 
     */
    public RCCallInfo[] getCurrentCalls() {
        synchronized (this) {
            if (m_calls == null || m_calls.isEmpty()) {
                return null;
            }

            Collection<RCCallInfo> cls = m_calls.values();
            RCCallInfo[] ret = new RCCallInfo[cls.size()];
            return cls.toArray(ret);
        }
    }
    
    /**
     * Represents current calls state. 
     */
    public static class CallsState {
        /**
         * Number of calls with PJSIP and active media.
         */
        int active_with_pjsip_and_media = 0;
        
        /**
         * Number of calls with PJSIP with not active media.
         */
        int active_with_pjsip = 0;
        
        /**
         * Number of calls with PJSIP but in ending/disconnected state
         */
        int not_active_with_pjsip = 0;
        
        /**
         * Number of active calls without PJSIP (CREATED/INIT)
         */
        int active_wo_pjsip = 0;
        
        /**
         * Number of not active calls without PJSIP
         */
        int not_active_wo_pjsip = 0;
        
        @Override
        public String toString() {
            StringBuffer sb = new StringBuffer("CallsState: [PJSIP: with_media:");
            sb.append(active_with_pjsip_and_media);
            sb.append(" wo_media:");
            sb.append(active_with_pjsip);
            sb.append(" not_active:");
            sb.append(not_active_with_pjsip);
            sb.append("] [WoPJSIP: active:");
            sb.append(active_wo_pjsip);
            sb.append(" not_active:");
            sb.append(not_active_wo_pjsip);
            sb.append("]");
            return sb.toString();
        }
    }
    
    /**
     * Get arrays of active calls ID
     */
    public CallsState getCallsState(){
        CallsState state = new CallsState();
        synchronized (this) {
            if (m_calls.isEmpty()) {
                return state;
            }

            Collection<RCCallInfo> cls = m_calls.values();
            for (RCCallInfo rcCallInfo : cls) {
                if (rcCallInfo != null) {
                    if (!rcCallInfo.isAlive()) {
                        if (rcCallInfo.isSipNotInitiatedState()) {
                            state.not_active_wo_pjsip++;
                        } else {
                            state.not_active_with_pjsip++;
                        }
                    } else {
                        if (rcCallInfo.isSipNotInitiatedState()) {
                            if (rcCallInfo.isEnding()) {
                                state.not_active_wo_pjsip++;
                            } else {
                                state.active_wo_pjsip++;
                            }
                        } else {
                            if (rcCallInfo.isEnding()) {
                                state.not_active_with_pjsip++;
                            } else {
                                if (rcCallInfo.getMediaState() == RCSIP.PJSUA_CALL_MEDIA_ACTIVE) {
                                    state.active_with_pjsip_and_media++;
                                } else {
                                    state.active_with_pjsip++;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if (LogSettings.MARKET) {
            MktLog.i(TAG, state.toString());
        }
        return state;
    }
    
    /**
     * Get arrays of active calls ID
     */
    public long[] call_get_active_calls(){
        
        Set<Long> keys = null;
        Object[] calls = null;
        int i;
        long[] result = null;
        int totalCalls = 0;
        int deletedCalls = 0;
        int endingCalls = 0;
        Vector<RCCallInfo> activeCalls = new Vector<RCCallInfo>() ;

        synchronized (this) {
            if (m_calls.isEmpty()) {
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "call_get_active_calls: 0");
                }
                return null;
            }

            keys = m_calls.keySet();
            calls = keys.toArray();

            /*
             * Remove calls on Ending state
             */
            for (i = 0; i < calls.length; i++) {
                final Long value = (Long) calls[i];
                final RCCallInfo rcCallInfo = getCallByRCCallId(value.longValue());
                totalCalls++;
                if (LogSettings.MARKET) {
                    MktLog.d(TAG, "call_get_active_calls: " + rcCallInfo.toString());
                }
                if (!rcCallInfo.isAlive()) {
                    deletedCalls++;
                    deleteCall(rcCallInfo.getRcCallId());
                } else if (rcCallInfo.isEnding()) {
                    endingCalls++;
                } else {
                    activeCalls.add(rcCallInfo);
                }
            }
            
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "call_get_active_calls: Total:" + totalCalls + "; Active: " + activeCalls.size() + "; Deleted:" + deletedCalls
                        + "; Ending:" + endingCalls);
            }
            
            if (!activeCalls.isEmpty()) {
                result = new long[activeCalls.size()];
                i = 0;
                for (RCCallInfo r : activeCalls) {
                    if (i < result.length) {
                        result[i++] = r.getRcCallId();
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns the number of current calls driven by the manager in any states
     * 
     * @return the number of current calls driven by the manager in any states
     */
    public synchronized int getCurrentDrivenCallsNumber() {
        if (m_calls == null) {
            return 0;
        }
        return m_calls.size();
    }
    
	/**
	 * Get active calls number
	 */
	public int call_get_count(){
		
		int result = 0;
		
        Set<Long> keys = null;
        Object[] calls = null;
		int i;
        
        synchronized( this ){
        	
    		if( m_calls.isEmpty()){
    		    if (LogSettings.MARKET) {
    	            MktLog.i(TAG, "call_get_count: 0");
    	        }
    			return result;
    		}
    		
    		keys = m_calls.keySet();
    		calls = keys.toArray();
        }
		
		
		for( i = 0; i < calls.length; i++){
			final Long value = (Long)calls[i];
			final RCCallInfo rcCallInfo = getCallByRCCallId( value.longValue() );
			if(rcCallInfo.isAlive() && !rcCallInfo.isEnding()){
				result++;
			}
		}
		return result;
	}
	
	/**
	 * Check if call is active
	 */
	public boolean call_is_active(final long rcCallId){
	    RCCallInfo rcCallInfo = null;
	    synchronized (this) {
            rcCallInfo = getCallByRCCallId(rcCallId);
            if (rcCallInfo == null) {
                return false;
            }

            if (rcCallInfo.isEnding()) {
                return false;
            }

            if (rcCallInfo.isAlive()) {
                return false;
            }
        }
        if (!rcCallInfo.isSipNotInitiatedState()) {
            return ServicePJSIP.getInstance().getWrapper().PJSIP_call_is_active(rcCallInfo.getPJSIPCallId());
        }
        
        return true;
	}
	
	/**
	 * Check if call has media
	 */
	public boolean call_has_media(final long rcCallId){
		
		final RCCallInfo rcCallInfo = getCallByRCCallId( rcCallId );
		if( rcCallInfo == null ){
			return false;
		}
		
		if( rcCallInfo.isEnding() ){
			return false;
		}
		
		if( !rcCallInfo.isSipNotInitiatedState()){
			return ServicePJSIP.getInstance().getWrapper().PJSIP_call_has_media(rcCallInfo.getPJSIPCallId());
		}
		
		return false;
	}

	/**
	 * Send request to call
	 */
	public int call_send_request( final long rcCallId, final String content, final String clientId ){
		
		final RCCallInfo rcCallInfo = getCallByRCCallId( rcCallId );
		if( rcCallInfo == null ){
			return PJSIP.sf_PJ_EINVAL;
		}
		
		if( !rcCallInfo.isSipNotInitiatedState()){
			return ServicePJSIP.getInstance().getWrapper().PJSIP_call_send_request(rcCallInfo.getPJSIPCallId(), content, clientId );
		}
		
		return PJSIP.sf_PJ_EINVALIDOP;
	}
	
	/**
	 * Call transfer
	 */
//	public int call_transfer(final long rcCallId, final String remoteURI){
//		
//		final RCCallInfo rcCallInfo = getCallByRCCallId( rcCallId );
//		if( rcCallInfo == null ){
//			return PJSIP.sf_PJ_EINVAL;
//		}
//		
//		if( !rcCallInfo.isSipNotInitiatedState()){
//			return ServicePJSIP.getInstance().getWrapper().PJSIP_call_transfer(rcCallInfo.getPJSIPCallId(), remoteURI );
//		}
//		
//		return PJSIP.sf_PJ_EINVALIDOP;
//	}

	/**
	 * Play DTMF
	 */
	public  int call_dial_dtmf(final long rcCallId, final String digits){
		
		final RCCallInfo rcCallInfo = getCallByRCCallId( rcCallId );
		if( rcCallInfo == null ){
			return PJSIP.sf_PJ_EINVAL;
		}
		
		if( !rcCallInfo.isSipNotInitiatedState()){
			return ServicePJSIP.getInstance().getWrapper().PJSIP_call_dial_dtmf(rcCallInfo.getPJSIPCallId(), digits);
		}
		
		return PJSIP.sf_PJ_EINVALIDOP;
	}
	
	/**
	 * Get call duration
	 */
//	protected long call_get_duration( final long rcCallId ){
//		
//		final RCCallInfo rcCallInfo = getCallByRCCallId( rcCallId );
//		if( rcCallInfo == null ){
//			return 0;
//		}
//		return (SystemClock.elapsedRealtime() - rcCallInfo.getCreationTime())/1000; //sec
//	}
	
	/**
	 * Make call
	 */
	public int call_make_call(final long rcCallId,
			final int accountId, 
			final String remoteURI, 
			final String[] headers)
	{
	
	    if( !ServicePJSIP.getInstance().getState().isSipStackCreated()){
		    if (LogSettings.MARKET) {
		    	MktLog.e(TAG, "Failed! SIP Stack is not loaded yet." );
		    }
		    return PJSIP.sf_PJ_EIGNORED;
	    }
	    RCCallInfo rcCallInfo = null;
        synchronized (this) {
            rcCallInfo = getCallByRCCallId(rcCallId);
            if (rcCallInfo == null) {
                return PJSIP.sf_PJ_EINVAL;
            }
        }
		
		if (LogSettings.MARKET) {
            MktLog.d(TAG, "call_make_call: " + rcCallInfo.toString());
        }
		
		int accId = accountId;
		if( accountId == PJSIP.sf_INVALID_ACCOUNT_ID ){
			accId = PJSIP.pjsip_acc_get_default();
			if (LogSettings.MARKET) {
			    MktLog.i(TAG, "call_make_call Default account ID " + accId + " for RC CallId : " + rcCallId);
			}
		}
		
		/*
		 * Mark call as initiated
		 */
		final int pjsipCallId = ServicePJSIP.getInstance().getWrapper().PJSIP_call_make_call(accId, remoteURI, headers);
		if( pjsipCallId != PJSIP.sf_INVALID_CALL_ID ){
            synchronized (this) {
                int old_pjsip_call_id = rcCallInfo.getPJSIPCallId();
                if (old_pjsip_call_id != PJSIP.sf_INVALID_CALL_ID) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, "call_make_call for RC CallId : " + rcCallId + " Rewrite SIP call ID Current:" + old_pjsip_call_id + " New:"
                                + pjsipCallId);
                    }
                }
                rcCallInfo.setPJSIPCallId(pjsipCallId);
            }
		    if (LogSettings.MARKET) {
		    	MktLog.i(TAG, "call_make_call Finished. RC Call ID: " + rcCallId + 
		        		" accountId: " + accountId + 
		        		" URI: " + remoteURI + 
		        		" PJSIP CallId: " + pjsipCallId );
		    }
            if (LogSettings.MARKET) {
                MktLog.d(TAG, rcCallInfo.toString());
            }
			return PJSIP.sf_PJ_SUCCESS;
		} else {
		    synchronized (this) {
		        if (rcCallInfo.isAlive()) {
		            rcCallInfo.setEnding(RCCallInfo.CALL_ENDED_BY_ERROR);
                    rcCallInfo.onCallStatusChange(false, RCSIP.PJSUA_CALL_STATE_DISCONNECTED,
                            RCSIP.SipCode.PJSIP_SC_SIP_CALL_FAILED_STATUS_CODE_EXTENDED.code(), null);
		        }
		    }
            ensureClientNotified(rcCallInfo, true, true);
		    ServicePJSIP.getInstance().updateActiveCallsCounter();
		    ServicePJSIP.getInstance().stopRings();
		}
	    
	    return PJSIP.sf_PJ_EUNKNOWN;
	}
	
	/**
	 * On call state change (notification from PJSIP)
	 * 
	 * @param pjsipCallId PJSIP call identifier
	 * @param state current call state
	 * @param last_status latest status
	 * @param dump call end dump or null
	 */
    public void OnCallState(int pjsipCallId, int state, int last_status, String dump) {
        final long incomingCallNtfId = ServicePJSIP.getInstance().getIncomingNotificationId();
        final RCCallInfo rcCallInfo = getRcCallByPjsipCallId(pjsipCallId);
        long rcCallId = PJSIP.sf_INVALID_RC_CALL_ID;
        if (rcCallInfo != null) {
            rcCallId = rcCallInfo.getRcCallId();
        }
        
        if (state == RCSIP.PJSUA_CALL_STATE_CALLING && rcCallId == PJSIP.sf_INVALID_RC_CALL_ID) {
            RCCallInfo call = getOutboundOnInitiatingState(false);
            if (call == null) {
                // TODO: 
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, "OnCallState Call=NULL");
                }
            } else {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "OnCallState Find line w/o PJSIP call ID. RC call ID:" + rcCallId);
                }
                call.setPJSIPCallId(pjsipCallId);
            }
        }
        

        if (LogSettings.MARKET) {
            MktLog.w(TAG, "OnCallState RC/PJSIP Ids:" + rcCallId + "/" + pjsipCallId +"  State:" + state + "/"
                    + RCSIP.getCallStateForLog(state) + "; IncCallId:" + incomingCallNtfId + " LastStatus:" + last_status + "/"
                    + RCSIP.getSipResponseForLog(last_status));
        }

        if (rcCallInfo != null && !rcCallInfo.isIncoming() && !rcCallInfo.isEnding()) {

            if (state == RCSIP.PJSUA_CALL_STATE_DISCONNECTED || state == RCSIP.PJSUA_CALL_STATE_CONFIRMED) {

                //ServicePJSIP.getInstance().stopRings();
                ServicePJSIP.getInstance().ringStop("CallMgr");
            }
        }

        if (state == RCSIP.PJSUA_CALL_STATE_DISCONNECTED) {
            if (rcCallInfo != null) {
                synchronized (this) {
                    if (!rcCallInfo.isEnding()) {
                        rcCallInfo.setEnding(RCCallInfo.CALL_ENDED_BY_PARTY);
                    }
                    rcCallInfo.onCallStatusChange(true, RCSIP.PJSUA_CALL_STATE_DISCONNECTED, last_status, dump);
                }
            }
            if (dump != null) {
                try {
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, "OnCallState RC CallId:" + rcCallId + " PJSIP Call Id:" + pjsipCallId + " Dump:");
                        MktLog.i(TAG, dump);
                    }
                } catch (java.lang.Throwable th) {

                }
            }
        }

        if (PJSIP.sf_INVALID_RC_CALL_ID != incomingCallNtfId) {

            if (LogSettings.MARKET) {
                MktLog.i(TAG, "OnCallState RC CallId " + rcCallId + " NtfId " + incomingCallNtfId);
            }

            if (incomingCallNtfId == rcCallId && state != RCSIP.PJSUA_CALL_STATE_INCOMING) {
                if (LogSettings.MARKET) {
                    MktLog.i(TAG, "OnCallState RC callId Delete notification for RC CallId" + rcCallId + " NtfId " + incomingCallNtfId);
                }

                ServicePJSIP.getInstance().deleteIncomingCallNotification(rcCallId);
            }

            if (LogSettings.MARKET) {
                MktLog.i(TAG, "OnCallState RC callId " + rcCallId + " checkIncomingCalls ");
            }
            ServicePJSIP.getInstance().checkIncomingCalls();
        }

        // Audio route management
        if( state == RCSIP.PJSUA_CALL_STATE_CALLING ) {
            ServicePJSIP.getInstance().ensureAudioRouteSet(false);
            if (rcCallInfo != null) {
                synchronized (this) {
                    rcCallInfo.onCallStatusChange(true, RCSIP.PJSUA_CALL_STATE_CALLING, last_status, dump);
                }
            }
        } else if (state == RCSIP.PJSUA_CALL_STATE_CONFIRMED) {
            if (LogSettings.MARKET) {
                MktLog.i(TAG, "OnCallState call confirmed");
            }

            if (rcCallInfo != null) {
                synchronized (this) {
                    rcCallInfo.onCallStatusChange(true, RCSIP.PJSUA_CALL_STATE_CONFIRMED, last_status, dump);
                }
            }
            ServicePJSIP.getInstance().ensureAudioRouteSet(false);
        } else if (state == RCSIP.PJSUA_CALL_STATE_DISCONNECTED) {
            ServicePJSIP.getInstance().endCallBeep();
            ServicePJSIP.getInstance().ensureAudioRouteSet(false);
        } else if (state == RCSIP.PJSUA_CALL_STATE_EARLY) {
            if (rcCallInfo != null) {
                synchronized (this) {
                    rcCallInfo.onCallStatusChange(true, RCSIP.PJSUA_CALL_STATE_EARLY, last_status, dump);
                }
            }
        } else if (state == RCSIP.PJSUA_CALL_STATE_CONNECTING) {
            if (rcCallInfo != null) {
                synchronized (this) {
                    rcCallInfo.onCallStatusChange(true, RCSIP.PJSUA_CALL_STATE_CONNECTING, last_status, dump);
                }
            }
        }

        /*
         * Call was disconnected - remove from list
         */
        if (state == RCSIP.PJSUA_CALL_STATE_DISCONNECTED) {
            deleteCall(rcCallId);
            ServicePJSIP.getInstance().getPJSIPCallbackListener().on_call_info_changed(PJSIP.sf_INVALID_RC_CALL_ID);
        }
        /*
         * Update active calls counter
         */
        if (state == RCSIP.PJSUA_CALL_STATE_DISCONNECTED || state == RCSIP.PJSUA_CALL_STATE_INCOMING
                || state == RCSIP.PJSUA_CALL_STATE_CALLING) {

            ServicePJSIP.getInstance().updateActiveCallsCounter();
        }

        if (rcCallInfo != null && !rcCallInfo.isIncoming() && state == RCSIP.PJSUA_CALL_STATE_DISCONNECTED) {
            ServicePJSIP.getInstance().checkForStop();
        }
        
        if (rcCallInfo == null) {
            ServicePJSIP.getInstance().CallsConcistencyCheckRequest();
        } else {
            if (LogSettings.MARKET) {
                MktLog.d(TAG, "OnCallState Notify: " + rcCallInfo.toString());
            }
            ensureClientNotified(rcCallInfo, true, true);
        }
    }
    
    /**
     * Ensure that application SIProxy client notified about call state change.
     * @param call the call
     * @param async defines if it shall be done asynchronously
     */
    public void ensureClientNotified(RCCallInfo call, boolean async, boolean sendOldScheme) {
        if (call == null) {
            return;
        }
        if (call.needClientNotification()) {
            RCCallInfo[] calls = new RCCallInfo[1];
            calls[0] = call;
            if (async) {
                ServicePJSIP.getInstance().notifyCallsStateChanged_Async(calls);
            } else {
                ServicePJSIP.getInstance().notifyCallsStateChanged_Sync(calls);
            }
        }
        if (sendOldScheme) {
            ServicePJSIP.getInstance().getPJSIPCallbackListener().notify_on_call_state(call.getRcCallId(), call.getState(),
                    call.getLastSatus().code(), call.getEndingReason(), call.getMediaState());
        }
    }

    /**
     * Checks calls consistency.
     * 
     * Returns true if there is active calls with media.
     */
    boolean checkCallsConsistency() {
        synchronized (this) {
            if (m_calls != null && !m_calls.isEmpty()) {
                Collection<RCCallInfo> cls = m_calls.values();
                for (RCCallInfo rcCallInfo : cls) {
                    if (rcCallInfo != null && rcCallInfo.isAlive() && !rcCallInfo.isSipNotInitiatedState()
                            && rcCallInfo.getMediaState() == RCSIP.PJSUA_CALL_MEDIA_ACTIVE 
                            && !rcCallInfo.isEnding()
                            && !rcCallInfo.unBoundPSIPCall()) {
                        return true;
                    }
                }
            }
        }
        
        Vector<RCCallInfo> callsForNotification = new Vector<RCCallInfo>();
        boolean notifySIPClient = false;
        int pjsipCount = ServicePJSIP.getInstance().getWrapper().PJSIP_call_get_count();
        synchronized (this) {
            if ((m_calls == null || m_calls.isEmpty()) && (pjsipCount == 0)) {
                return false;
            }
        }
        
        if (LogSettings.MARKET) {
            MktLog.d(TAG, "checkCallsConsistency...");
        }

        ServicePJSIP service = ServicePJSIP.getInstance();

        if (service.getState().state() != ServiceState.ON) {
            if (LogSettings.MARKET) {
                MktLog.w(TAG, "Calls exists, but SIP is not available. Clean up.");
            }
            notifySIPClient = true;
            // TODO: Define starting state :
            // cleanUpAllRCCalls(callsForNotification);
            // Cleanup not CREATED (outbound) calls
            // Cleanup expired
        } else {
            notifySIPClient = checkCallsConsistencyWhenSIPisActive(callsForNotification);
        }

        if (notifySIPClient) {
            ServicePJSIP.getInstance().getPJSIPCallbackListener().on_call_info_changed(PJSIP.sf_INVALID_RC_CALL_ID);
        }

        if (!callsForNotification.isEmpty()) {
            RCCallInfo[] ret = new RCCallInfo[callsForNotification.size()];
            ret = callsForNotification.toArray(ret);
            ServicePJSIP.getInstance().notifyCallsStateChanged_Sync(ret);
        }
        
        return false;
    }
    
    /**
     * Checks calls when SIP Stack is available.
     *  
     * @param callsForNotification calls to be notified about state changes 
     * 
     * @return if needed to notify previous SIPClient
     */
    private boolean checkCallsConsistencyWhenSIPisActive(Vector<RCCallInfo> callsForNotification) {
        boolean notifySIPClient = false;
        int[] calls = ServicePJSIP.getInstance().getWrapper().PJSIP_getActiveCallsList();
        Vector<PJSIPCallInfo> pjsipCalls = obtainPJSIPCallInfos(calls);

        if (calls.length != pjsipCalls.size()) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, "CALLS CONCISTENCY: PJSIP returned " + calls.length + " active calls when " + pjsipCalls.size()
                        + " infos were retrieved");
            }
        }

        dumpPJSIPCalls(pjsipCalls);
        dumpRCCalls();
        
        // TODO: Continue check consistency
        // 1. Find zombi PJSIP calls - delete
        // 2. Bind PJSIP<->RC
        // 3. Find zombi RC calls - delete
        // 3. Update calls status
        
        return notifySIPClient;
    }
    
    /**
     * Cleans up all calls.
     * 
     * @param callsForNotification calls for notification
     */
    private void cleanUpAllRCCalls(Vector<RCCallInfo> callsForNotification) {
        Collection<RCCallInfo> cls = m_calls.values();
        for (RCCallInfo rcCallInfo : cls) {
            if (rcCallInfo != null) {
                rcCallInfo.setEnding(RCCallInfo.CALL_ENDED_BY_SYSTEM);
                rcCallInfo.setMainError(RCSIP.SipCode.PJSIP_SC_CALLS_EXISTS_WHEN_SIP_FALSE_STATUS_CODE_EXTENDED);
                rcCallInfo.onCallStatusChange(false, RCSIP.PJSUA_CALL_STATE_DISCONNECTED, RCSIP.SipCode.PJSIP_SC_NOT_ACCEPTABLE_ANYWHERE.code(), null);
                deleteCall(rcCallInfo.getRcCallId());
                if (rcCallInfo.needClientNotification()) {
                    callsForNotification.add(rcCallInfo);
                }
            }
        }
    }
    
    /**
     * Dumps current PJSIP calls
     * 
     * @param pjsipCalls current PJSIP calls
     */
    private void dumpPJSIPCalls(Vector<PJSIPCallInfo> pjsipCalls) {
        if (LogSettings.MARKET) {
            if (!pjsipCalls.isEmpty()) {
                MktLog.d(TAG, "CALLS CONCISTENCY: PJSIP calls " + pjsipCalls.size());
                MktLog.d(TAG, ">====================================");

                for (PJSIPCallInfo info : pjsipCalls) {
                    MktLog.d(TAG, info.toString());
                }
                MktLog.d(TAG, "<====================================");
            } else {
                MktLog.d(TAG, "CALLS CONCISTENCY: No PJSIP Calls");
            }
        }
    }
    
    /**
     * Dumps current RC Calls.
     */
    private void dumpRCCalls() {
        if (LogSettings.MARKET) {
            synchronized (this) {
                if (!m_calls.isEmpty()) {
                    MktLog.d(TAG, "CALLS CONCISTENCY: RC Calls " + m_calls.size());
                    MktLog.d(TAG, ">====================================");
                    Collection<RCCallInfo> cls = m_calls.values();
                    for (RCCallInfo info : cls) {
                        MktLog.d(TAG, info.toString());
                    }
                    MktLog.d(TAG, "<====================================");
                } else {
                    MktLog.d(TAG, "CALLS CONCISTENCY: No RC Calls");
                }
            }
        }
    }
    
    /**
     * Obtains <code>PJSIPCallInfo</code> from PJSIP.
     * 
     * @param calls
     *            calls to obtain infos
     * 
     * @return the calls infos
     */
    private Vector<PJSIPCallInfo> obtainPJSIPCallInfos(final int[] calls) {
        Vector<PJSIPCallInfo> infos = new Vector<PJSIPCallInfo>();

        PJSIPCallInfo info = null;

        for (int i = 0; i < calls.length; i++) {
            info = new PJSIPCallInfo();
            int result = ServicePJSIP.getInstance().getWrapper().PJSIP_get_call_info(calls[i], info);
            if (result == PJSIP.sf_PJ_SUCCESS) {
                infos.add(info);
            }
        }

        return infos;
    }
}
