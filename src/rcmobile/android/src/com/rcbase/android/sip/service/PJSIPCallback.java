/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import android.telephony.TelephonyManager;

import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.client.RCSIP;
import com.rcbase.parsers.sipmessage.SipMessage;
import com.rcbase.parsers.sipmessage.SipMessageInput;
import com.rcbase.parsers.sipmessage.SipMessageOutputSimple;
import com.ringcentral.android.LogSettings;

/**
 * @author Denis Kudja
 *
 */
public class PJSIPCallback {
    private static final String TAG = "[RC]PJSIPCallback";
	
    
	/*
	 * SIP Stack callback functions
	 * parameters: 
	 * Account ID
	 */
	
	/**
	 * Registration status of an account had been changed.
	 */
	public static void on_reg_state(int accountId){
		ServicePJSIP.getInstance().getPJSIPCallbackListener().on_reg_state(accountId);
	}
	
	/**
	 * on_message
	 * parameters:
	 * @param pjsipCallId
	 * 
     * parameters[0] 	from    - URI of the sender.
     * parameters[1] 	to	    - URI of the destination message.
     * parameters[2] 	contact	- The Contact URI of the sender, if present.
     * parameters[3] 	mime_type - MIME type of the message.
     * parameters[4] 	body	- The message content.
	 */
	public static void on_message( int pjsipCallId, String[] parameters ){
		
	    if (LogSettings.MARKET) {
	        MktLog.i(TAG, "on_message PJSIP CallId " + pjsipCallId );
	    }
	    
		for( int i = 0; i < parameters.length; i++ ){
		    if (LogSettings.MARKET) {
		        MktLog.i(TAG, "on_message parameter [" + i + "] "+ parameters[i] );
		    }
		}

		SipMessageInput sipMsg = null;
		
		if( parameters[4] != null && parameters[4].length() > 0 ){
			final String msg = WrapPJSIP.cutMessage( parameters[4], WrapPJSIP.s_End_Msg );
			sipMsg = WrapPJSIP.parseRCMessage(msg);
		}
		
		long rcCallId = ServicePJSIP.getInstance().getCallMng().getRcCallIdByPjsipCallId( pjsipCallId );
		
		if( sipMsg != null ){
			
		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "on_message PJSIP callId " + pjsipCallId + 
		        		" Cmd:" + sipMsg.getCommand() + 
		        		" RC From:" + sipMsg.getHdr().getFrom() );
		    }

		    if( rcCallId == PJSIP.sf_INVALID_RC_CALL_ID ){
		    	rcCallId = ServicePJSIP.getInstance().getCallMng().getCallByRCFrom(sipMsg.getHdr().getFrom());
		    }
		    
			ServicePJSIP.getInstance().getPJSIPCallbackListener().on_message(rcCallId, sipMsg );
			
		    /*
		     * Possible need to add an additional checks there
		     */
		    if( rcCallId != PJSIP.sf_INVALID_RC_CALL_ID ){
		    	
				if( SipMessage.sf_CMD_AlreadyProcessed == sipMsg.getCommand() ||
						SipMessage.sf_CMD_SessionClose == sipMsg.getCommand() ){
					ServicePJSIP.getInstance().RingtoneStop();
				}
		    }
		}
	}
	
	
	/**
     * Notify application on incoming call.
     *
     * @param acc_id	The account which match the incoming call.
     * @param call_id	The call id that has just been created for the call.
	 * 
     * parameters[0] 	from    - URI of the sender.
     * parameters[1] 	to	    - URI of the destination message.
     * parameters[2] 	P-rc	- P-rc header, if present.
     * parameters[3]	Call-ID
	 */
	public static void on_incoming_call(int accountId, int pjsipCallId, String[] headers ) {
        final RCCallInfo rcCallInfo = ServicePJSIP.getInstance().getCallMng().createIncomingCall(pjsipCallId, headers[0]);
		
	    if (LogSettings.MARKET) {
	        MktLog.w(TAG, "on_incoming_call accountId " + accountId + 
	        		" RC CallId: " + rcCallInfo.getRcCallId() + 
	        		" PJSIP CallId: " + pjsipCallId  );
	        MktLog.d(TAG, rcCallInfo.toString());
	    }
    
		for( int i = 0; i < headers.length; i++ ){
		    if (LogSettings.MARKET) {
		        MktLog.v(TAG, "on_incoming_call header [" + i + "] "+ headers[i] );
		    }
		}
		
		SipMessageInput sipMsg = null;
		CallInfo callInfo = new CallInfo();
		
		callInfo.setRcCallId(rcCallInfo.getRcCallId());
		callInfo.setPjsipCallId(pjsipCallId);
		callInfo.setAccountId(accountId);
		callInfo.setNetworkType(rcCallInfo.getNetworkType());
		
		callInfo.setRemoteURI(headers[0]);
		callInfo.setLocalURI(headers[1]);
		callInfo.setRcHeader(WrapPJSIP.cutMessage( headers[2], WrapPJSIP.s_End_Msg ));
		callInfo.setDialogCallId( WrapPJSIP.cutMessage( headers[3], "\r\n"));
		
		callInfo.setCall_state(RCSIP.PJSUA_CALL_STATE_INCOMING);
		
		if( headers[2] != null && headers[2].length() > 0 ){
			final String msg = WrapPJSIP.cutMessage( headers[2], WrapPJSIP.s_End_Msg );
			sipMsg = WrapPJSIP.parseRCMessage(msg);
			callInfo.setSipMsg(sipMsg);
		}
		
		if( sipMsg != null ){
		    rcCallInfo.setRcFrom(sipMsg.getHdr().getFrom());
		}
		
		callInfo.setClientId(ServicePJSIP.getInstance().getUserInfo().getInstanceId());

	    /* 
	     * Reject incoming call if agent is Incoming calls set to Off
	     * 
	     * RFC 3261
	     * 486 Busy Here
   		 * The callee's end system was contacted successfully, but the callee is
   		 * currently not willing or able to take additional calls at this end
   		 * system.  The response MAY indicate a better time to call in the
   		 * Retry-After header field.  The user could also be available
   		 * elsewhere, such as through a voice mail service.  Status 600 (Busy
   		 * Everywhere) SHOULD be used if the client knows that no other end
   		 * system will be able to accept this call.
	     */

	    /*
	     * Reject call if
	     * 1. number of active calls or
	     * 2. Is incoming calls are allowed or
	     * 3. If we have an active mobile call
	     */
		if( ServicePJSIP.getInstance().isOutboundCallsOnly() || 
	    		( ServicePJSIP.getInstance().getActiveCallsCounter() >= PJSIP.sf_MAX_NUMBER_CALLS ) || 
	    			ServicePJSIP.getInstance().getPhoneState() == TelephonyManager.CALL_STATE_OFFHOOK){
	    	
		    if (LogSettings.ENGINEERING) {
		        EngLog.w(TAG, "on_incoming_call was ignored because Outbound Only:" + ServicePJSIP.getInstance().isOutboundCallsOnly() + 
		        		" or to many Active Calls:" + ServicePJSIP.getInstance().getActiveCallsCounter() + 
		        		" [" + PJSIP.sf_MAX_NUMBER_CALLS + "] or we have Active Mobile Call" );
		    }
		    
		    /*
		     * Mark call as finishing
		     */
		    rcCallInfo.setEnding(RCCallInfo.CALL_ENDED_BY_SYSTEM);
		    
		    boolean hangUp = false;
			if( callInfo.getSipMsg() != null && callInfo.getSipMsg().getHdr() != null ){
				final SipMessageOutputSimple msg_reject = 
					(SipMessageOutputSimple)SipMessage.prepareControlCallCommand( 
							SipMessage.sf_CMD_ClientReject,
							callInfo.getClientId(),
							callInfo.getSipMsg() );
				
				if( msg_reject != null ) {
				    if (LogSettings.MARKET) {
	                    MktLog.w(TAG, "on_incoming_call RC CallId " + rcCallInfo.getRcCallId() + " RC Header is not detected. Send REJECT.");
	                }
					final String To = SipMessage.prepareToAddress( callInfo.getSipMsg().getHdr().getFrom() );
					ServicePJSIP.getInstance().sendSipMessage( To, msg_reject );
				} else {
				    hangUp = true;
				}
			} else {
			    hangUp = true;
			}
			
            if (hangUp) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "on_incoming_call RC CallId " + rcCallInfo.getRcCallId() + " Hang up");
                }

                ServicePJSIP.getInstance().doCallHangupAsync(rcCallInfo.getRcCallId(), RCCallInfo.CALL_ENDED_BY_ERROR,
                        RCSIP.SipCode.PJSIP_SC_BUSY_HERE.code(), null);
            }
			
	    	return;
	    }
	    
		
		/*
		 * Send RC Call Control message ClientReceiveConfirm (17)
		 */
		if( callInfo.getSipMsg() != null && callInfo.getSipMsg().getHdr() != null ){
			final SipMessageOutputSimple msg_out = (SipMessageOutputSimple)SipMessage.prepareControlCallCommand( 
					SipMessage.sf_CMD_ClientReceiveConfirm, 
					callInfo.getClientId(),
					callInfo.getSipMsg() );

		    if (LogSettings.MARKET) {
		        MktLog.w(TAG, "on_incoming_call RC CallId " + rcCallInfo.getRcCallId() + " Send SIP Message ClientReceiveConfirm" );
		    }
			
			if( msg_out != null ){
				final String To = SipMessage.prepareToAddress( callInfo.getSipMsg().getHdr().getFrom() );
				ServicePJSIP.getInstance().sendSipMessage( To, msg_out );
			}
		}
		else{
		    if (LogSettings.ENGINEERING) {
		    	EngLog.w(TAG, "on_incoming_call RC callId " + rcCallInfo.getRcCallId() + " RC Header is not detected. Do not send ClientReceiveConfirm." );
		    }
		}		
		long notification = ServicePJSIP.getInstance().getIncomingNotificationId();
		
        if (PJSIP.sf_INVALID_RC_CALL_ID == notification) {

            if (LogSettings.MARKET) {
                MktLog.w(TAG, "on_incoming_call RC callId " + rcCallInfo.getRcCallId() + " Start Incoming Call notification");
            }
            ServicePJSIP.getInstance().setIncomingCallNotification(callInfo, 0);

            /*
             * Start Incoming Call Activity
             */
            ServicePJSIP.getInstance().startIncomingCallActivity(callInfo, 0);
        } else {
            RCCallInfo prevCall = ServicePJSIP.getInstance().getCallMng().getCallByRCCallId(notification);
            if (prevCall == null || !prevCall.isAlive() || prevCall.isEnding()) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, "on_incoming_call RC callId " + rcCallInfo.getRcCallId() + " Restart Incoming Call notification (prev:"
                            + notification + ")");
                }
                ServicePJSIP.getInstance().deleteIncomingCallNotification(notification);
                ServicePJSIP.getInstance().setIncomingCallNotification(callInfo, 0);
                ServicePJSIP.getInstance().startIncomingCallActivity(callInfo, 0);
            }
        }
		
		
		ServicePJSIP.getInstance().getPJSIPCallbackListener().on_incoming_call(callInfo );
		ServicePJSIP.getInstance().getCallMng().ensureClientNotified(rcCallInfo, true, true);
		
		/*
		 * Update active calls counter
		 */
    	ServicePJSIP.getInstance().updateActiveCallsCounter();
	}
	
	/**
	 * Notify application when media state in the call has changed.
	 */
	public static void on_call_media_state( int pjsipCallId, int media_status)
	{
	    if (LogSettings.MARKET) {
	        MktLog.i(TAG, "Media state changed PJSIP callId " + pjsipCallId + " status [" + media_status + "] " + RCSIP.getMediaStatus(media_status));
	    }
		
		RCCallInfo rcCallInfo = ServicePJSIP.getInstance().getCallMng().getRcCallByPjsipCallId( pjsipCallId );
		if(rcCallInfo == null ){
			return;
		}
	    
		rcCallInfo.setMediaState(media_status);
		ServicePJSIP.getInstance().getCallMng().ensureClientNotified(rcCallInfo, true, false);
		ServicePJSIP.getInstance().getPJSIPCallbackListener().on_call_media_state(rcCallInfo.getRcCallId(), media_status);
	}
	
	/**
	 * Notify application when media session has been unregistered from the
	 * conference bridge and about to be destroyed.
	 */
	public static void on_stream_destroyed( int pjsipCallId ){
	    if (LogSettings.MARKET) { 
	        MktLog.i(TAG, "Media stream was destroyed PJSIP CallId " + pjsipCallId );
	    }
	    
		final long rcCallId = ServicePJSIP.getInstance().getCallMng().getRcCallIdByPjsipCallId( pjsipCallId );
		if( rcCallId == PJSIP.sf_INVALID_RC_CALL_ID ){
			return;
		}
		
		ServicePJSIP.getInstance().getPJSIPCallbackListener().on_stop_call_media( rcCallId );
	}
	
    /**
     * Notify application when invite state has changed.
     */
    public static void on_call_state(int pjsipCallId, int state, int last_status, int has_dump, String[] dump) {
        if (LogSettings.MARKET) {
            MktLog.w(TAG, "on_call_state PJSIP ID:" + pjsipCallId + " state:" + RCSIP.getCallStateForLog(state) + "; LastStatus:"
                    + RCSIP.getSipResponseForLog(last_status));
        }
        String endCallDump = null;
        if (has_dump > 0 && dump != null && dump.length > 0) {
            endCallDump = dump[0];
        }
        ServicePJSIP.getInstance().getPJSIPCallbackListener().PJSIPCallbackOnCallState(pjsipCallId, state, last_status, endCallDump);
    }
	
    /**
     * Notify application when call transfer state changed 
     */
    public static void on_call_transfer_status( int pjsipCallId, int status, String[] status_progress_text ){
        if (LogSettings.MARKET) {
            MktLog.i(TAG, "on_call_transfer_status PJSIP CallId " + pjsipCallId + " status " + status );
        }
        
        final long rcCallId = ServicePJSIP.getInstance().getCallMng().getRcCallIdByPjsipCallId( pjsipCallId );
        if( rcCallId == PJSIP.sf_INVALID_RC_CALL_ID ){
            return;
        }
        ServicePJSIP.getInstance().getPJSIPCallbackListener().on_call_transfer_status( rcCallId, status, status_progress_text[0]);
    }
    
	/**
	 * Notify application when critical media error is occurred
	 * Called from RC Android Audio Device 
	 */
	public static void on_media_failed( int error ){
	    if (LogSettings.MARKET) {
	        MktLog.i(TAG, "on_media_failed " + error );
	    }
		ServicePJSIP.getInstance().getPJSIPCallbackListener().on_media_failed(error);
	}
	
}
