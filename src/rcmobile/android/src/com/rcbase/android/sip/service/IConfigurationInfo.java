/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

/**
 * Interface for receiving SIP configuration parameters
 * 
 * @author Denis Kudja
 */
public interface IConfigurationInfo {

}
