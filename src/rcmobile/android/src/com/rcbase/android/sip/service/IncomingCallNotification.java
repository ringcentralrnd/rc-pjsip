/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import java.util.Timer;
import java.util.TimerTask;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.SystemClock;
import android.os.Vibrator;

import com.rcbase.android.config.BUILD;
import com.rcbase.android.logging.MktLog;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConstants;
import com.ringcentral.android.RingCentralApp;


class IncomingCallNotification {
	private static final String TAG = "[RC]IncomingCallNotification";
	
	public static final int RINGER_MODE_SECOND_CALL = 100; // in case there will be maufacter specific constants
	
	private static final long RINGTONE_TIMEOUT = 60000l;
	private static final long RINTONE_SLEEP_DELAY = 1000l;
	private static final long RINTONE_DEATH_DELAY = 100l;
	
	private static final long VIBRATE_DURATION = 400l;
	private static final long VIBRATE_PAUSE_DURATION = 2500l;
	private static final long[] VIBRATE_PATTERN = { 0, VIBRATE_DURATION, VIBRATE_PAUSE_DURATION };
	
	private AudioManager mAudioManager;
	private Vibrator mVibratorService;
	private BeepCallNotification mSecondCallNotifier;
	private RingerModeReceiver mRingerModeReceiver;
		
	private final Object mLock = new Object();
	
	private RingThread mRingThread;
	private Timer mGuardTimer;
	private boolean mVibrating;
	private boolean mNotifying;		
	
	IncomingCallNotification() {
		mAudioManager = (AudioManager)RingCentralApp.getContextRC().getSystemService(Context.AUDIO_SERVICE);
		mVibratorService = (Vibrator)RingCentralApp.getContextRC().getSystemService(Context.VIBRATOR_SERVICE);
		mSecondCallNotifier = new BeepCallNotification(RingCentralApp.getContextRC());
		mRingerModeReceiver = new RingerModeReceiver();
		RingCentralApp.getContextRC().registerReceiver(mRingerModeReceiver, new IntentFilter(AudioManager.RINGER_MODE_CHANGED_ACTION));		
	}
	
	void destroy() {
		stopRingtone();
		mAudioManager = null;
		mVibratorService = null;
		mSecondCallNotifier = null;	
		
		if(mRingerModeReceiver != null) {
			RingCentralApp.getContextRC().unregisterReceiver(mRingerModeReceiver);
			mRingerModeReceiver = null;
		}
	}	
	
	void startRingtone(boolean multipleCalls, RCCallInfo rcCallInfo) {
		synchronized (mLock) {			
			if(!mNotifying) {
				if(LogSettings.MARKET) {
					MktLog.i(TAG, "startRingtone(), mNotifying : " + mNotifying + ", multipleCalls : " + multipleCalls );
				}		
				mNotifying = true;
				mGuardTimer = new Timer(TAG + ":mGuardTimer");
				mGuardTimer.schedule(new GuardTimerTask(), RINGTONE_TIMEOUT);			
			}
			if (multipleCalls) {
				if(LogSettings.MARKET) {
					MktLog.d(TAG, "startRingtone(), multiple calls, switching to in-call beeps ");
				}	
				startSeconCallBeep();				
				updateRingerMode(AudioManager.RINGER_MODE_SILENT);
				
				if(rcCallInfo != null) {
					rcCallInfo.setRingerMode(RINGER_MODE_SECOND_CALL);
				}
			} else {
				stopSeconCallBeep();
				final int ringerMode = mAudioManager.getRingerMode(); 
				updateRingerMode(ringerMode);
				
				if(rcCallInfo != null) {				
					rcCallInfo.setRingerMode(ringerMode);
					rcCallInfo.setRingerVolume(mAudioManager.getStreamVolume(AudioManager.STREAM_RING));
				}
			}			
		}				
	}
	
	
	void stopRingtone() {				
		synchronized (mLock) {
			if(mNotifying) {				
				if(LogSettings.MARKET) {
					MktLog.i(TAG, "stopRingtone(), mNotifying : " + mNotifying);
				}
				mNotifying = false;
				mGuardTimer.cancel();
				mGuardTimer = null;
			}
			stopSeconCallBeep();
			stopVibration();
			stopRing();			
		}		
	}
	
	private void updateRingerMode(int ringMode) {				
		if (ringMode == AudioManager.RINGER_MODE_SILENT) {
			if(LogSettings.MARKET) {
				MktLog.d(TAG, "updateRingerMode(), AudioManager.RINGER_MODE_SILENT");
			}									
			stopVibration();
			stopRing();					
		} else if (ringMode == AudioManager.RINGER_MODE_VIBRATE) {
			if(LogSettings.MARKET) {
				MktLog.d(TAG, "updateRingerMode(), AudioManager.RINGER_MODE_VIBRATE");
			}					
			startVibration();
			stopRing();
		} else { // RINGER_MODE_NORMAL or some undefined or carrier specific constant
			if(LogSettings.MARKET) {
				MktLog.d(TAG, "updateRingerMode(), AudioManager.RINGER_MODE_NORMAL or some undefined or carrier specific constant");
			}
			startVibration();
			startRing();					
		}		
	}
	
	
	/**
	 * Ringtone player
	 */	
	private void startRing() {
		synchronized (mLock) {
			final boolean isRinging = (mRingThread != null);			
			if(!isRinging) {
				if(LogSettings.MARKET) {
					MktLog.d(TAG, "startRing()");
				}
				mRingThread = new RingThread(getRingtone());
				mRingThread.start();
				
				if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
				    Intent ringStartedIntent = new Intent(RCMConstants.ACTION_INVOIP_RINGTONE_STARTED);
				    ringStartedIntent.putExtra(RCMConstants.EXTRA_VOIP_RING_STARTED, SystemClock.elapsedRealtime());
	                RingCentralApp.getContextRC().sendBroadcast(ringStartedIntent);
				}
			}
		}			
	}
	
	//should be called from a separate thread due to lock and join timeout
	private void stopRing() {
		synchronized (mLock) {
			final boolean isRinging = (mRingThread != null);			
			if(isRinging) {
				if(LogSettings.MARKET) {
					MktLog.d(TAG, "stopRing()");
				}
				try {
					if (mRingThread != null) {
						mRingThread.setStopped();
						mRingThread.interrupt(); 			
						mRingThread.join(RINTONE_DEATH_DELAY);		
						mRingThread = null;
					}
				} catch (Throwable t) {
					if (LogSettings.MARKET) {
						MktLog.e(TAG, "stopRing(), exception : " + t);
					}
				}
			}
		}//end synchronized		
	}
	
	private Ringtone getRingtone() {
		final Uri ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
		if (ringtoneUri != null) {
			return RingtoneManager.getRingtone(RingCentralApp.getContextRC(), ringtoneUri);			
		}
		return null;
	}
	
	
	/**
	 * Vibration
	 */		
	private void startVibration() {
		synchronized (mLock) {			
			if((mVibratorService != null) && !mVibrating) {
				if(LogSettings.MARKET) {
					MktLog.d(TAG, "startVibration()");
				}
				mVibratorService.vibrate(VIBRATE_PATTERN, 0);
				mVibrating = true;
			}			
		}		
	}
	
	private void stopVibration() {
		synchronized (mLock) {			
			if((mVibratorService != null) && mVibrating) {
				if(LogSettings.MARKET) {
					MktLog.d(TAG, "stopVibration()");
				}
				mVibratorService.cancel();
				mVibrating = false;
			}
		}
	}	
	
	
	/**
	 * Second call notification
	 *
	 */
	private void startSeconCallBeep() {
		synchronized (mLock) {						
			if(!mSecondCallNotifier.isBeeping()){
				if(LogSettings.MARKET) {
					MktLog.d(TAG, "startSeconCallBeep()");
				}
				mSecondCallNotifier.startBeep();
				
				if (BUILD.PERFORMANCE_MEASUREMENT_ENABLED){
                    Intent ringStartedIntent = new Intent(RCMConstants.ACTION_INVOIP_RINGTONE_STARTED);
                    ringStartedIntent.putExtra(RCMConstants.EXTRA_VOIP_RING_STARTED, SystemClock.elapsedRealtime());
                    RingCentralApp.getContextRC().sendBroadcast(ringStartedIntent);
                }
			}
			
		}
	}
	
	private void stopSeconCallBeep() {
		synchronized (mLock) {				
			if(mSecondCallNotifier.isBeeping()) {
				if(LogSettings.MARKET) {
					MktLog.d(TAG, "stopSeconCallBeep()");
				}
				mSecondCallNotifier.stopBeep();
			}
		}
	}	
		
	
	/*
	 * There is no default way in Android to make ringing cyclic, so will be rescheduling and replaying
	 */
	private class RingThread extends Thread {		
		private final Ringtone ringtone;
		private boolean stopped;		
		
		public RingThread(Ringtone ringtone) {
			super(TAG + ":RingThread");
			this.ringtone = ringtone;
		}
		
		public void setStopped() {
			if(LogSettings.MARKET) {
				MktLog.d(TAG, "RingThread.setStopped()");
			}
			this.stopped = true;
		}

		@Override
		public void run() {
			if(LogSettings.MARKET) {
				MktLog.d(TAG, "Starting ringer thread, ringtone : " + ((ringtone == null) ? "null" : ringtone.getTitle(RingCentralApp.getContextRC())));
			}
			try {
				while((ringtone != null) && !stopped) {
					ringtone.play();
					while(ringtone.isPlaying() && !stopped) {					
						Thread.sleep(RINTONE_SLEEP_DELAY);					
					}
				}
			} catch (InterruptedException e) {
				if(LogSettings.MARKET) {
					MktLog.d(TAG, "Thread interrupted, stop ringing");
				}
			} catch (Throwable t) {
				if(LogSettings.MARKET) {
					MktLog.e(TAG, "General error, aborting rings", t);
				}
			} finally {
				if(ringtone != null)
					ringtone.stop();
			}
			if(LogSettings.MARKET) {
				MktLog.d(TAG, "Ringer thread stopped.");
			}
		}		
	}
	
	public static final String getRingerModeAsString(int ringerMode) {
		if(ringerMode == AudioManager.RINGER_MODE_NORMAL) {
			return "RINGER_MODE_NORMAL";
		} else if(ringerMode == AudioManager.RINGER_MODE_SILENT) {
			return "RINGER_MODE_SILENT";
		} else if(ringerMode == AudioManager.RINGER_MODE_VIBRATE) {
			return "RINGER_MODE_VIBRATE";
		} else if(ringerMode == RINGER_MODE_SECOND_CALL) {
			return "RINGER_MODE_SECOND_CALL";
		} 
		return "UNKNOWN";		
	}
	
	private class GuardTimerTask extends TimerTask {
		
		@Override
		public void run() {
			if (LogSettings.MARKET) {
				MktLog.e(TAG, "GuardTimerTask.run(), notification timer exceeded, shutting down all notification");
			}
			stopRingtone();	
		}		
	}
	
	private class RingerModeReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (AudioManager.RINGER_MODE_CHANGED_ACTION.equals(intent.getAction())) {
				final int ringerMode = intent.getIntExtra(AudioManager.EXTRA_RINGER_MODE, AudioManager.RINGER_MODE_NORMAL);
				if (LogSettings.MARKET) {
					MktLog.d(TAG, "RingerModeReceiver.onReceive() : AudioManager.RINGER_MODE_CHANGED_ACTION, new mode : "+ ringerMode);
				}
				synchronized (mLock) {
					if(mNotifying && !mSecondCallNotifier.isBeeping()) {
						updateRingerMode(ringerMode);
					}
				}
			}
		}
	}
}
