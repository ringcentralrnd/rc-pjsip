/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

import com.ringcentral.android.RingCentralApp;
import com.ringcentral.android.provider.RCMProviderHelper;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Denis Kudja
 *
 */
public class AccountInfo implements Parcelable {

	private int m_accountId = PJSIP.sf_INVALID_ACCOUNT_ID;
	private boolean m_registered = false;
	
	private String m_extension;
	private String m_pin;
	private String m_loginId;
	private String m_psw;
	private String m_serviceProvider;
	private String m_sipOutboundProxy;
	
	public void copyFrom( AccountInfo account ){
		if( account == null )
			return;
		
		this.m_accountId = account.getAccountId();
		this.m_extension = account.getExtension();
		this.m_pin	= account.getPin();
		this.m_psw = account.getPsw();
		this.m_loginId = account.getLoginId();
		this.m_serviceProvider = account.getServiceProviderRaw();
		this.m_sipOutboundProxy = account.getSipOutboundProxRaw();
	}
	
//	public boolean isRegistered() {
//		return m_registered;
//	}
	public void setRegistered(boolean registered) {
		m_registered = registered;
	}
	public int getAccountId() {
		return m_accountId;
	}
	public void setAccountId(int accountId) {
		m_accountId = accountId;
		//m_accountId = 1011;
		//m_accountId = 7040;
		//m_accountId = 1019;
		//m_accountId = RCMProviderHelper.getTSCEnalbe(RingCentralApp.getContextRC()) ? 7040:1019;
	}
	public String getSipOutboundProxy() {
		return new String("sip:" + m_sipOutboundProxy );
	}
	public String getSipOutboundProxRaw() {
		return m_sipOutboundProxy ;
	}
	public void setSipOutboundProxy(String sipOutboundProxy) {
		m_sipOutboundProxy = sipOutboundProxy;
		//m_sipOutboundProxy="10.32.51.31";
		//m_sipOutboundProxy="155.212.214.183:80";
		//m_sipOutboundProxy="192.168.92.4";
		//m_sipOutboundProxy="10.32.51.25";
		//m_sipOutboundProxy=RCMProviderHelper.getTSCEnalbe(RingCentralApp.getContextRC()) ? "192.168.92.4":"10.32.51.25";
		
	}
	public String getExtension() {
		return new String(m_extension);
	}
	public void setExtension(String extension) {
		m_extension = extension;
		//m_extension="1011";
		//m_extension="7040";
		//m_extension="1019";
		//m_extension=RCMProviderHelper.getTSCEnalbe(RingCentralApp.getContextRC()) ? "7040":"1019";
	}
	public String getPin() {
		return new String(m_pin);
	}
	public void setPin(String pin) {
		//m_pin = pin;
		m_pin="";
	}
	/*
	 * Account is 
	 * sip:extension*pin@service_provider
	 * 
	 * for example: 
	 * sip:1222334444@ringcentral.com
	 * sip:1222334444*333@ringcentral.com
	 */
	public String getAccount() {
		final String login = new String("sip:" + m_extension +( m_pin == null || m_pin.length() == 0 || 0 == Integer.parseInt(m_pin) ? "" : "*" + m_pin ) + "@" + m_serviceProvider );
		//final String login = new String("sip:" + "1006@" + m_serviceProvider );
		return login;
	}
	public String getLoginId() {
		return new String(m_loginId);
	}
	public void setLoginId(String loginId) {
		m_loginId = loginId;
		//m_loginId="1011";
		//m_loginId="7040";
		//m_loginId="1019";
		//m_loginId=RCMProviderHelper.getTSCEnalbe(RingCentralApp.getContextRC()) ? "7040":"1019";
	}
	public String getPsw() {
		return new String(m_psw);
	}
	public void setPsw(String psw) {
		m_psw = psw;
		//m_psw="1234";
		//m_psw="7040";
		//m_psw="1234";
		//m_psw=RCMProviderHelper.getTSCEnalbe(RingCentralApp.getContextRC()) ? "7040":"1234";
	}
	public String getServiceProvider() {
		return new String("sip:" + m_serviceProvider);
	}
	public String getServiceProviderRaw() {
		return m_serviceProvider;
	}
	public void setServiceProvider(String serviceProvider) {
		m_serviceProvider = serviceProvider;
		//m_serviceProvider="10.32.51.31";
		//m_serviceProvider="192.168.92.4";
		//m_serviceProvider="10.32.51.25";
		//m_serviceProvider=RCMProviderHelper.getTSCEnalbe(RingCentralApp.getContextRC()) ? "192.168.92.4":"10.32.51.25";
		
	}
	/*
	 * 
	 */
	public static final Parcelable.Creator<AccountInfo> CREATOR = new Parcelable.Creator<AccountInfo>() {
		
		public AccountInfo createFromParcel( Parcel in ){
			return new AccountInfo(in);
		}
		
		public AccountInfo[] newArray( int size){
			return new AccountInfo[size];
		}
	};
	
	/*
	 * 
	 */
	public AccountInfo(){
		
	}

	public AccountInfo( Parcel in ){
		readFromParcel(in);
	}
	
	/* (non-Javadoc)
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(m_accountId);
		dest.writeString(m_extension);
		dest.writeString(m_pin);
		dest.writeString(m_loginId);
		dest.writeString(m_psw);
		dest.writeString(m_serviceProvider);
		dest.writeString(m_sipOutboundProxy);
	}
	/*
	 * 
	 */
	public void readFromParcel( Parcel in ){
		m_accountId = in.readInt();
		m_extension = in.readString();
		m_pin = in.readString();
		m_loginId = in.readString();
		m_psw  = in.readString();
		m_serviceProvider = in.readString();
		m_sipOutboundProxy = in.readString();
	}

}
