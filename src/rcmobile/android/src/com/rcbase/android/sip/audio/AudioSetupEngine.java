/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */


/**
 * Andrea's header (SBACP_API.h) quote:
 * 
 * ..
 * #define SBACP_TEST_OFF              0  < - no test mode || delay detected 
 * ..
 * #define SBACP_TEST_PING_DELAY       5  < - beep detection based Audio delay estimation
 * ..
 * 
 * On 8/20/11 6:39 AM, "Andrea Santilli" wrote:
 * 
 * >Hi Ben,
 * >
 * >just finished to write the beep based delay estimators and tested it offline using recordings
 * >
 * >At the end of the story is all about using the "test 5" command and
 * >read back the delay using "delay". Or in a equivaleny way use the
 * >following code
 * >
 * >SetTestMode (SBACP_TEST_PING_DELAY)
 * >
 * >...    wait until GetTestMode() goes back to 0
 * >
 * >GetAudioDelay();           // to read back the delay value
 * >
 * >I will some real time test Monday and send you the code in the afternoon.
 * >
 * >Bye
 * >   Andrea
 */
package com.rcbase.android.sip.audio;


import com.rcbase.android.logging.EngLog;
import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.service.ServicePJSIP;
import com.rcbase.android.sip.service.WrapPJSIP;
import com.rcbase.android.utils.media.RCMediaManager;
import com.ringcentral.android.LogSettings;
import com.ringcentral.android.RCMConstants;

public final class AudioSetupEngine {
    private static final String TAG = "[RC]AudioSetupEngine";

    private static final int TEST_INITIATED = 0;
    private static final int TEST_IN_PROGRESS = 1;
    private static final int TEST_COMPLETED = 2;
    
    private static final long POINTER_NATIVE_NULL = 0;
    private static final int AEC_DELAY_DETECTED = 0;
    
    private static AudioSetupEngine mInstance = null;    
    private ServicePJSIP mServicePJSIP;
    private WrapPJSIP mWrapPJSIP;
    private RCMediaManager mMediaManager;
    
    private final Object mLock = new Object();
    
    private volatile boolean mInProgress = false; 
    private int mTestAudioSource = RCMediaManager.MODE_DETACHED;
    private int mTestState = TEST_INITIATED;            
    private int mDelayValue =  RCMConstants.DEFAULT_AEC_DELAY_VALUE;             
    
    public AudioSetupEngine(ServicePJSIP servicePJSIP, WrapPJSIP wrapPJSIP, RCMediaManager mediaManager) {
    	this.mServicePJSIP = servicePJSIP;
    	this.mWrapPJSIP = wrapPJSIP;
    	this.mMediaManager = mediaManager;
    	mInProgress = false;
    	AudioSetupEngine.mInstance = this;    	
    }
    
    
    public void release() {       
    	stopTest();
    	AudioSetupEngine.mInstance = null;
    	mInProgress = false;
    	this.mServicePJSIP = null;
    	this.mWrapPJSIP = null;
    	this.mMediaManager = null;    	
    }

    
    public final void startTest(int testId) {
    	if(LogSettings.MARKET) {
            MktLog.i(TAG, "startTest(), testId : " + RCMediaManager.getModeTag(testId));
        }
    	
   		if ((mWrapPJSIP == null) || (mServicePJSIP == null)) {
            if(LogSettings.MARKET) {
                MktLog.w(TAG, "startTest(), Test started without proper initialization, aborting...");
            }
            return;
        }     

		synchronized (mLock) {
			if (mInProgress) {
				if (LogSettings.MARKET) {
					MktLog.w(TAG, "startTest(), already in progress, aboring...");
				}
				return;
			}

			mInProgress = true;
			mTestAudioSource = testId;

			try {
			    AudioDevice.setTestMode(true);
				mMediaManager.startAudio(mTestAudioSource);
				mWrapPJSIP.PJSIP_pjsip_start_test();
				mServicePJSIP.onTestStarted(mTestAudioSource);
				mTestState = TEST_INITIATED;
				if (LogSettings.ENGINEERING) {
					EngLog.d(TAG, "startTest(), AudioDevice.setTestMode(true)");
				}
			} catch (Throwable t) {
				if (LogSettings.MARKET) {
					MktLog.e(TAG, "startTest(), exception tryi", t);
				}				
				stopTest();
			}
		}
		
    }
    
    
    /** 
     * releases resources, setting back all flags & etc
     */
    public final void stopTest() {    	
    	synchronized (mLock) {
    		if(LogSettings.MARKET) {
	            MktLog.i(TAG, "stopTest(), mTestAudioSource : " + RCMediaManager.getModeTag(mTestAudioSource) + ", mInProgress : " + mInProgress + ", mTestState : " + mTestState);
	        }
    		AudioDevice.setTestMode(false);
    		if (mInProgress) {    			
	    		mWrapPJSIP.PJSIP_pjsip_stop_test();
	    		mMediaManager.stopAudio();
	    		mTestAudioSource = RCMediaManager.MODE_DETACHED;
	    		mTestState = TEST_COMPLETED;
	    		mInProgress = false;
    		}
		}
    }

    
    public int getDelay() {
        return mDelayValue;
    }
    
    
    /**
     * Wrapper method for AudioDevice
     * @param pAudioParam pointer to the structure of stream(located in native lib)
     */
    static void onPlayback(long pAudioParam) {
    	if(mInstance != null) {
    		mInstance.onPlaybackImpl(pAudioParam);
    	}
    }
   
    private void onPlaybackImpl(long pAudioParam) {
        if(!mInProgress) {
        	if(LogSettings.ENGINEERING) {
                EngLog.w(TAG, "onPlaybackImpl(), mInProgress = false, while notification continues.. aborting");
            }
            return; 
        }
        
        if(POINTER_NATIVE_NULL != pAudioParam) {    	
    		 switch(mTestState) {             
             case TEST_INITIATED:                     
                 if(LogSettings.ENGINEERING) {
                     EngLog.d(TAG, "onPlaybackImpl() TEST_INITIATED");                
                 }
                 
                 mTestState = TEST_IN_PROGRESS;
                 AudioDevicePJSIP.aecSetTestMode(pAudioParam);
                 break;                 
             case TEST_IN_PROGRESS:                
                 final int aecCurrentTestMode = AudioDevicePJSIP.aecGetCurrentTestMode(pAudioParam);                     
                 if(LogSettings.ENGINEERING) {
                     EngLog.v(TAG, "onPlaybackImpl() TEST_IN_PROGRESS, aecCurrentTestMode : " + aecCurrentTestMode);
                 }
                 
                 if(AEC_DELAY_DETECTED == aecCurrentTestMode ) {
                	 mDelayValue = AudioDevicePJSIP.aecGetAudioDelay(pAudioParam);                    	 
                     if(LogSettings.MARKET) {
                         MktLog.i(TAG, "onPlaybackImpl(), detected delay value : " + mDelayValue + "(sample), " + (mDelayValue/8) + "(milliseconds)");
                     }
                     mTestState = TEST_COMPLETED;
                     mServicePJSIP.onTestCompleted(mTestAudioSource, mDelayValue);
                 }
                 break;                    
             case TEST_COMPLETED:
            	 if(LogSettings.ENGINEERING) {
                     EngLog.d(TAG, "onPlaybackImpl() TEST_COMPLETED");
                 }
                 break;
             }							
        } else {
            if(LogSettings.MARKET) {
                MktLog.w(TAG, "pAudioParam = 0");
            }
        }
    }      

}
  