/**
 * Copyright (C) 2010-2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.service;

/**
 * @author Denis Kudja
 *
 */
public class PJSIP {
	public static final String sf_PJSIP_LIB_NAME = "librcpjsip.so";
	
	public static final int sf_INVALID_CALL_ID			= -1;
	public static final long sf_INVALID_RC_CALL_ID		= -1;
	public static final int sf_INVALID_ACCOUNT_ID		= -1;
	
	public static final int sf_MAX_NUMBER_CALLS			= 3;
	
	public static final int sf_PJ_SUCCESS				= 0;
	public static final int sf_PJ_ERRNO_START			= 20000;
	public static final int sf_PJ_ERRNO_SPACE_SIZE		= 50000;
	
	public static final int sf_PJ_ERRNO_START_STATUS 	= (sf_PJ_ERRNO_START + sf_PJ_ERRNO_SPACE_SIZE);
	
	public static final int sf_PJ_EUNKNOWN	    		= (sf_PJ_ERRNO_START_STATUS + 1);	/* 70001 Unknown error has been reported.*/
	public static final int sf_PJ_EINVAL	    		= (sf_PJ_ERRNO_START_STATUS + 4);	/* 70004 Invalid argument.*/
	public static final int sf_PJ_ENOTFOUND	    		= (sf_PJ_ERRNO_START_STATUS + 6);	/* 70006 Not found.*/
	public static final int sf_PJ_ENOMEM	    		= (sf_PJ_ERRNO_START_STATUS + 7);	/* 70007 Not enough memory.*/
	public static final int sf_PJ_EINVALIDOP    		= (sf_PJ_ERRNO_START_STATUS + 13);	/* 70013 Invalid operation.*/
	public static final int sf_PJ_ECANCELLED    		= (sf_PJ_ERRNO_START_STATUS + 14);	/* 70014 Operation is cancelled.*/
	public static final int sf_PJ_ETOOSMALL	    		= (sf_PJ_ERRNO_START_STATUS + 19);	/* 70019 Size is too small.*/
	public static final int sf_PJ_EIGNORED	    		= (sf_PJ_ERRNO_START_STATUS + 20);	/* 70020 Ignored.*/
	
	public static final String sf_SIP_header_P_Asserted_Identity	= "P-Asserted-Identity"; 
	
	public static final int RC_CALL_STATUS_HOLD 			= 0x00000001;
	public static final int RC_CALL_STATUS_FINISHED 		= 0x00000002;
	

	
	/*
	 * SIP Stack control functions
	 */
	
	public final static native int pjsip_create();
	public final static native int pjsip_destroy();
	public final static native int pjsip_init(String outputProxy, int SIPTransportLocalPort);
	public final static native int pjsip_start();
	
	/*
	 * SIP Stack Accounts configuration functions
	 * 
	 * Account parameters are
	 * [0] account
	 * [1] service provider
	 * [2] login Id
	 * [3] password
	 * [4] SIP outbound proxy
	 */
	public final static native int pjsip_acc_add(String[] accountParameters, int[] accountId, long forcedExpires);
	//by martin
	public final static native int pjsip_tsc_tunnel_set(int enable, String ip, int port , int red);
	public final static native int pjsip_acc_get_default();
	public final static native int pjsip_acc_modify( int accountId, String[] accountParameters, long forcedExpires );
	public final static native int pjsip_acc_del( int accountId );
	public final static native int pjsip_acc_get_status( int accountId, int[] status );
	public final static native int pjsip_acc_is_valid( int accountId  );
	public final static native int pjsip_acc_get_info( int accountId, String[] accountInfo );
	public final static native int pjsip_acc_set_registration( int accountId, boolean renew );
	
	/*
	 * SIP Stack Service functions
	 */
	public final static native void pjsip_ring_start( int callId );
	public final static native void pjsip_ringback_start(int call_id);
	public final static native void pjsip_ring_stop(int call_id);
	
	/*
	 * SIP Stack Call Control functions
	 */
	public final static native int pjsip_call_answer( int callId, int code, String[] headers );
	public final static native int pjsip_call_hangup( int callId, int code, String[] headers );
	public final static native int pjsip_call_hold( int callId );
	public final static native int pjsip_call_unhold( int callId );
	public final static native int pjsip_call_make( int accountId, String dstURI, String[] headers, int[] callId );
	public final static native int pjsip_call_send_request( int callId, String method, String clientId );
	public final static native int pjsip_call_transfer( int callId, String dstURI );
	public final static native int pjsip_call_dial_dtmf( int callId, String digits );
	public final static native int pjsip_call_finished( int callId );
	
	/**
	 * Disconnect call's media from speaker & recorder
	 */
	public final static native int pjsip_call_meia_disconnect( int callId );
	
	/**
	 * Reinvate all active calls
	 */
	public final static native int pjsip_call_reinvate_all();
	
	/**
	 * Hangup all active calls
	 */
	public final static native void pjsip_call_hangup_all();
	/**
	 * Attach sound device to this call. It is used when call is active again after unhold.
	 */
	public final static native int pjsip_call_attach_sound( int callId );
	
	/**
	 * Select or change sound device. Application may call this function at
	 * any time to replace current sound device.
	 *
	 * @param capture_dev   Device ID of the capture device.
	 * @param playback_dev	Device ID of the playback device.
	 *
	 * @return		PJ_SUCCESS on success, or the appropriate error code.
	 */
	public final static native int pjsip_set_snd_dev(int capture_dev, int playback_dev);
	
	/**
	 * Get currently active sound devices. If sound devices has not been created
	 * (for example when pjsua_start() is not called), it is possible that
	 * the function returns PJ_SUCCESS with -1 as device IDs.
	 *
	 * @param devices[0] On return it will be filled with device ID of the 
	 *			capture device.
	 * @param devices[1]	On return it will be filled with device ID of the 
	 *			device ID of the playback device.
	 *
	 * @return		PJ_SUCCESS on success, or the appropriate error code.
	 */
	public final static native int pjsip_get_snd_dev( int[] devices );
	
	/**
	 * Disconnect the main conference bridge from any sound devices, and let
	 * application connect the bridge to it's own sound device/master port.
	 */
	public final static native int pjsip_set_no_snd_dev();
	
	/*
	 * SIP Stack Call Info functions
	 */
	
	public final static native int pjsip_call_is_active( int callId );
	public final static native int pjsua_call_has_media( int callId );
	public final static native int pjsip_call_get_max_count();
	/** Get number of currently active calls. */	
	public final static native int pjsip_call_get_count();
	
	/**
	 * Return list of call IDs. If input array have not enough size then sf_PJ_ETOOSMALL
	 * is returned and 'count' contains number of elements.
	 * 
	 * Note: I recommend to use pjsip_call_get_max_count() for array size detection 
	 * before call pjsip_call_enum_calls().
	 * Because number of active calls can be changed during you call.
	 */
	public final static native int pjsip_call_enum_calls(int[] callId, int[] count );
	
	/**
	 * Call values are:
	 * 
	 * [0]	Local URI
	 * [1]	Local Contact
	 * [2]	Remote URI
	 * [3]	Remote contact
	 * [4]	Dialog Call-ID string
	 * 
 * Status are:
	 * [0]  state - Call state
	 * [1]  status code - Last status code heard, which can be used as cause code
	 * [2]  media status - Call media status
	 * [3]  account ID
	 * [4]  hold status ( 1 - HOLD, 0 - UNHOLD, -1 Unknown )
	 */
	public final static native int pjsip_call_get_info( int callId, 
			String[] values, 
			int[] status );
	
	/**
	 * Durations on seconds
	 *  
	 * [0]	Connect duration. Up-to-date call connected duration (zero when call is not established)
	 * [1]	Total duration. Total call duration, including set-up time
	 */
	public final static native int pjsip_call_get_duration( int callId, long[] duration );
	
	/**
	 * Send instant messaging outside dialog, using the specified account for 
	 * route set and authentication.
	 */
	public final static native int pjsip_message_send( int accountId, String dstURI, String content );
	
	/*
	 * Enum all supported codecs in the system.
	 * Return list of codec IDs
	 */
	public final static native int pjsip_enum_codecs( String[] codecId, int[] priority, int[] codecCount );
	
	/**
	 * @param codecId	Codec ID, which is a string that uniquely identify the codec.
	 * @param priority	Codec priority, 0-255, where zero means to disable the codec.
	 */
	public final static native int pjsip_codec_set_priority( String codecId, int priority );
	
	/*
	 * SIP Stack Conference service functions 
	 */
	public final static native float pjsip_conf_get_signal_rx_level( int slot );
	public final static native float pjsip_conf_get_signal_tx_level( int slot );
	
	/**
	 * Adjust the signal level to be transmitted from the bridge to the 
	 * specified port by making it louder or quieter.
	 *
	 * @param slot		The conference bridge slot number.
	 * @param level		Signal level adjustment. Value 1.0 means no level
	 *					adjustment, while value 0 means to mute the port.
	 */
	public final static native int pjsip_conf_adjust_tx_level( int slot, float level );
	public final static native float pjsip_conf_get_adjust_tx_level( int slot );
	
	/**
	 * Adjust the signal level to be received from the specified port (to
	 * the bridge) by making it louder or quieter.
	 *
	 * @param slot		The conference bridge slot number.
	 * @param level		Signal level adjustment. Value 1.0 means no level
	 *					adjustment, while value 0 means to mute the port.
	 */
	public final static native int pjsip_conf_adjust_rx_level( int slot, float level );
	public final static native float pjsip_conf_get_adjust_rx_level( int slot );
	
	/**
	 * Create UDP media transports for all the calls. This function creates
	 * one UDP media transport for each call.
	 *
	 * @param port	The "port" is used as the start port to bind the
	 *			sockets.
	 */	
	public final static native int pjsip_media_transports_create( int port );
	public final static native int pjsip_media_transports_update( int port );
    
	
	public final static native int pjsip_start_test();
	public final static native int pjsip_stop_test();
	
}
