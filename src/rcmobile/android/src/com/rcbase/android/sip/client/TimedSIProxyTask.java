/**
 * Copyright (C) 2012, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.client;

import android.os.RemoteException;
import android.os.SystemClock;

import com.rcbase.android.logging.MktLog;
import com.rcbase.android.sip.service.IServicePJSIP;
import com.ringcentral.android.LogSettings;

/**
 * <code>TimedSIProxyTask</code> allows to make asynchronous execution of SIP
 * service routines via client's <code>doServiceRequest</code> method,
 * indicating timeout when called thread will be waked-up if
 * <code>doServiceRequest</code> has not been completed in <code>timeout</code>.
 * See {@link #execute(long, boolean)}.
 */
public abstract class TimedSIProxyTask extends Thread {
    /**
     * Defines logging tag.
     */
    private static final String TAG = "[RC]TimedSIProxyTask";
    
    /**
     * Defines initial state of the execution.
     */
    public final static int PENDING    = 0;
    
    /**
     * Defines state when <code>doServiceRequest</code> was completed.See {@link #execute(long, boolean)}.
     */
    public final static int TIMEOUT    = 1;
    
    /**
     * Defines state when timeout occurred. See {@link #execute(long, boolean)} and
     * See {@link #wasCompletedWithError()}
     */
    public final static int COMPLETED  = 2;
    
    /**
     * Defines state when execution has not been completed due to service unavailability.
     */
    public final static int FAILED  = 3;
    
    /**
     * Keeps synchronization primitive.
     */
    private volatile Object mLock = new Object();
    
    /**
     * Keeps execution state.
     */
    private volatile int mState = PENDING;
    
    /**
     * Defines task(thread) name.
     */
    private volatile String mName = null;
    
    /**
     * Defines if <code>doServiceRequest</code> was completed with errors.
     */
    private volatile boolean mCompletedWithError = false;
    
    /**
     * Defines if <code>doServiceRequest</code> shall be completed even timeout happened.
     */
    private volatile boolean mdoServiceRequestEvenTimeoutOccured = false;

    /**
     * Default constructor.
     * 
     * @param name the task name for logging and analysis
     */
    public TimedSIProxyTask(String name) {
        super((name == null ? "NONAME" : name));
        mName = (name == null ? "NONAME" : name);
    }
    
    /**
     * Returns if <code>doServiceRequest</code> has been completed with errors (exception
     * when doServiceRequest was executed)
     * 
     * @return <code>true</code> if <code>doServiceRequest</code> has been completed with
     *         errors (exception when doServiceRequest was executed), otherwise
     *         <code>false</code>
     */
    public boolean wasCompletedWithError() {
        synchronized (mLock) {
            return mCompletedWithError;
        }
    }
    
    /**
     * Keeps service waiting lock.
     */
    private volatile Object mSIProxyLock = new Object();
    
    /**
     * Defines if it is waiting service. 
     */
    private volatile boolean mWaitingService = false;
    
    /**
     * Keeps SIProxy client.
     */
    private volatile SIProxyClientClass mSIProxyClient = new SIProxyClientClass();

    /**
     * SIProxy client.
     */
    private class SIProxyClientClass implements SIProxyClient {
        @Override
        public void onServiceConnectionStateChange() {
            synchronized (mSIProxyLock) {
                if (mWaitingService) {
                    int state = SIProxy.SHUTDOWN;
                    try {
                        state = SIProxy.getInstance().getServiceConnectionState();
                    } catch (java.lang.Throwable th) {
                    }
                    if (state == SIProxy.SHUTDOWN || state == SIProxy.CONNECTED) {
                        if (LogSettings.MARKET) {
                            MktLog.i(TAG, (mName + ": onServiceConnectionStateChange:notifyAll"));
                        }
                        mSIProxyLock.notifyAll();
                    } else {
                        if (LogSettings.MARKET) {
                            MktLog.d(TAG, (mName + ": onServiceConnectionStateChange:(re)-connecting"));
                        }
                        return;
                    }
                }
            }
            try {
                SIProxy.getInstance().removeClient(this);
            } catch (java.lang.Throwable th) {
            }
        }
    }
    
    private volatile long mStartExecutionTime = 0; 
    
    /**
     * Executes <code>doServiceRequest</code>.
     * 
     * @param timeout
     *            timeout when caller will be notified if <code>doServiceRequest</code>
     *            has not been finished in that time
     * @param doServiceRequestEvenTimeoutOccured
     *            defines if need complete <code>doServiceRequest</code> even timeout
     *            occured
     * 
     * @return status: {@link #TIMEOUT} if <code>doServiceRequest</code> has not been
     *         finished in <code>timeout</code> time; {@link #COMPLETED} if the
     *         <code>doServiceRequest</code> has been completed in <code>timeout</code>
     */
    public int execute(final long timeout, final boolean doServiceRequestEvenTimeoutOccured) {
        if (timeout < 0) {
            throw new java.lang.IllegalArgumentException(TAG + ':' + mName + ':' + "execute:negative timeout.");
        }
        
        synchronized (mLock) {
            mdoServiceRequestEvenTimeoutOccured = doServiceRequestEvenTimeoutOccured;
            if (mState != PENDING) {
                throw new java.lang.IllegalStateException(TAG + ':' + mName + ':' + "execute:invalid state.");
            }
            mStartExecutionTime = SystemClock.elapsedRealtime();
            start();
            try {
                mLock.wait(timeout);
            } catch (Throwable th) {
            }
            if (mState == PENDING) {
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, (mName + ":execute:timeout expired"));
                }
                mState = TIMEOUT;
            }
            return mState;
        }
    }
     
    /**
     * Sets new state and notifies waiter.
     * 
     * @param newState new state
     * @return new state
     */
    private int wakeUpAndReturnState(int newState) {
        synchronized (mLock) {
            mState = newState;
            mLock.notifyAll();
            return mState;
        }
    }
    
    /**
     * Define max time of service waiting.
     */
    private static final long SERVICE_WAITING_TIMEOUT = 5000;
    
    @Override
    public void run() {
        try {
            synchronized (mLock) {
                if (mState == TIMEOUT) {
                    if (!mdoServiceRequestEvenTimeoutOccured) {
                        if (LogSettings.MARKET) {
                            MktLog.i(TAG, (mName + ":run:ignore execution as expired"));
                        }
                        mLock.notifyAll();
                        return;
                    }
                } else if (mState != PENDING) {
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, (mName + ":run:ignore execution due to invalid state"));
                    }
                    mLock.notifyAll();
                    return;
                }
            }

            SIProxy proxy = SIProxy.getInstance();
            if (proxy == null) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, (mName + ":FAILED:SIProxy null"));
                }
                wakeUpAndReturnState(FAILED);
                return;
            }
            
            boolean serviceError = false;
            IServicePJSIP service = null;
            boolean removeProxy = false;
            int prevServicePid = 0;
            synchronized (mSIProxyLock) {
                SIProxy.ServiceRequest request = proxy.getService(mSIProxyClient);
                prevServicePid = request.servicePID;
                if (request.serviceState == SIProxy.SHUTDOWN) {
                    if (LogSettings.MARKET) {
                        MktLog.e(TAG, (mName + ":FAILED: proxy is shutdown"));
                    }
                    serviceError = true;
                } else if (request.serviceState != SIProxy.CONNECTED) {
                    mWaitingService = true;
                    if (LogSettings.MARKET) {
                        MktLog.i(TAG, (mName + ": Waiting service..."));
                    }
                    try {
                        mSIProxyLock.wait(SERVICE_WAITING_TIMEOUT);
                    } catch (Throwable th) {
                        if (LogSettings.MARKET) {
                            MktLog.w(TAG, (mName + ": TIMEOUT Waiting service."));
                        }
                        serviceError = true;
                    } finally {
                        mWaitingService = false;
                    }
                    removeProxy = true;
                    service = proxy.getService();
                } else {
                    removeProxy = true;
                    service = request.serviceHandle;
                }
            }
            
            if (removeProxy) {
                try {
                    proxy.removeClient(mSIProxyClient);
                } catch (java.lang.Throwable th) {
                }
            }
            mSIProxyClient = null;
            
            if (serviceError || service == null) {
                service = null;
                if (LogSettings.MARKET) {
                    MktLog.w(TAG, (mName + ": SERVICE ERROR"));
                }
                wakeUpAndReturnState(FAILED);
                return;
            }

            if (prevServicePid != proxy.getServicePID()) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, (mName + ":doServiceRequest:ignore execution: service PID changed"));
                }
                service = null;
                wakeUpAndReturnState(FAILED);
                return;
            }
            
            synchronized (mLock) {
                if (mState == TIMEOUT) {
                    if (!mdoServiceRequestEvenTimeoutOccured) {
                        if (LogSettings.MARKET) {
                            MktLog.i(TAG, (mName + ":doServiceRequest:ignore execution as expired"));
                        }
                        service = null;
                        mLock.notifyAll();
                        return;
                    }
                } else if (mState != PENDING) {
                    if (LogSettings.MARKET) {
                        MktLog.w(TAG, (mName + ":doServiceRequest:ignore execution due to invalid state"));
                    }
                    service = null;
                    mLock.notifyAll();
                    return;
                }
            }

            boolean error = false;
            try {
                doServiceRequest(service);
            } catch (Throwable th) {
                if (LogSettings.MARKET) {
                    MktLog.e(TAG, (mName + ":doServiceRequest:error:"), th);
                }
                error = true;
            }
            if (LogSettings.MARKET) {
                MktLog.d(TAG, (mName + ":completed in: " + (SystemClock.elapsedRealtime() - mStartExecutionTime) + " ms"));
            }
            service = null;
            synchronized (mLock) {
                mCompletedWithError = error;
                wakeUpAndReturnState(COMPLETED);
                return;
            }
        } catch (Throwable th) {
            if (LogSettings.MARKET) {
                MktLog.e(TAG, (mName + ":mainloop:error:"), th);
            }
        }
        mWaitingService = false;
        try {
            SIProxy.getInstance().removeClient(mSIProxyClient);
        } catch (java.lang.Throwable th) {
        }
        wakeUpAndReturnState(FAILED);
        return;
    }
    
    /**
     * The task will be executed by 
     */
    public abstract void doServiceRequest(IServicePJSIP service) throws RemoteException;
}
