/**
 * Copyright (C) 2012, RingCentral, Inc. 
 * 
 * All Rights Reserved.
 */
package com.rcbase.android.sip.client;

public interface SIProxyClient {
    /**
     * Notifies that SIP service connection state changed {@link SIProxy#getServiceConnectioState()}.
     */
    void onServiceConnectionStateChange();
}
