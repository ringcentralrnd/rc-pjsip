/* $Id: os_android.h $ */
/*
 * Created on base os_linux.h - part of PJSIP project
 * 
 */
#ifndef __PJ_COMPAT_OS_ANDROID_H__
#define __PJ_COMPAT_OS_ANDROID_H__

/**
 * @file os_android.h
 * @brief Describes Android operating system specifics.
 */

#define PJ_OS_NAME		    "android"

#define PJ_HAS_ARPA_INET_H	    1
#define PJ_HAS_ASSERT_H		    1
#define PJ_HAS_CTYPE_H		    1
#define PJ_HAS_ERRNO_H		    1
#define PJ_HAS_LINUX_SOCKET_H	    0
#define PJ_HAS_MALLOC_H		    1
#define PJ_HAS_NETDB_H		    1
#define PJ_HAS_NETINET_IN_H	    1
#define PJ_HAS_SETJMP_H		    1
#define PJ_HAS_STDARG_H		    1
#define PJ_HAS_STDDEF_H		    1
#define PJ_HAS_STDIO_H		    1
#define PJ_HAS_STDLIB_H		    1
#define PJ_HAS_STRING_H		    1
#define PJ_HAS_SYS_IOCTL_H	    1
#define PJ_HAS_SYS_SELECT_H	    1
#define PJ_HAS_SYS_SOCKET_H	    1
#define PJ_HAS_SYS_TIME_H	    1
#define PJ_HAS_SYS_TIMEB_H	    1
#define PJ_HAS_SYS_TYPES_H	    1
#define PJ_HAS_TIME_H		    1
#define PJ_HAS_UNISTD_H		    1
#define PJ_HAS_SEMAPHORE_H	    1

/*
 Without this define we have redifinition error on SRTP files
 srtp_config.h:114: error: redefinition of typedef 'uint64_t'
*/
#define PJ_HAS_STDINT_H		1

/*
 Without this define we have 
 sock_bsd.c:136: error: 'TCP_NODELAY' undeclared here (not in a function)
*/
#define PJ_HAS_NETINET_TCP_H	1

#define PJ_HAS_MSWSOCK_H	    0
#define PJ_HAS_WINSOCK_H	    0
#define PJ_HAS_WINSOCK2_H	    0

#define PJ_SOCK_HAS_INET_ATON	    1
/*
 We have implementation for inet_ntop, inet_pton too
 see sock_bsd.c
*/
#define PJ_SOCK_HAS_INET_NTOP	1
#define PJ_SOCK_HAS_INET_PTON	1

/* Set 1 if native sockaddr_in has sin_len member. 
 * Default: 0
 */
#define PJ_SOCKADDR_HAS_LEN	    0

/**
 * If this macro is set, it tells select I/O Queue that select() needs to
 * be given correct value of nfds (i.e. largest fd + 1). This requires
 * select ioqueue to re-scan the descriptors on each registration and
 * unregistration.
 * If this macro is not set, then ioqueue will always give FD_SETSIZE for
 * nfds argument when calling select().
 *
 * Default: 0
 */
#define PJ_SELECT_NEEDS_NFDS	    0

/* Is errno a good way to retrieve OS errors?
 */
#define PJ_HAS_ERRNO_VAR	    1

/* When this macro is set, getsockopt(SOL_SOCKET, SO_ERROR) will return
 * the status of non-blocking connect() operation.
 */
#define PJ_HAS_SO_ERROR             1

/* This value specifies the value set in errno by the OS when a non-blocking
 * socket recv() can not return immediate daata.
 */
#define PJ_BLOCKING_ERROR_VAL       EAGAIN

/* This value specifies the value set in errno by the OS when a non-blocking
 * socket connect() can not get connected immediately.
 */
#define PJ_BLOCKING_CONNECT_ERROR_VAL   EINPROGRESS

/* Default threading is enabled, unless it's overridden. */
#ifndef PJ_HAS_THREADS
#  define PJ_HAS_THREADS	    (1)
#endif

#define PJ_HAS_HIGH_RES_TIMER	    1
#define PJ_HAS_MALLOC               1
#ifndef PJ_OS_HAS_CHECK_STACK
#   define PJ_OS_HAS_CHECK_STACK    0
#endif
#define PJ_NATIVE_STRING_IS_UNICODE 0

#define PJ_ATOMIC_VALUE_TYPE	    long

/* If 1, pj_thread_create() should enforce the stack size when creating 
 * threads.
 * Default: 0 (let OS decide the thread's stack size).
 */
#define PJ_THREAD_SET_STACK_SIZE    	0

/* If 1, pj_thread_create() should allocate stack from the pool supplied.
 * Default: 0 (let OS allocate memory for thread's stack).
 */
#define PJ_THREAD_ALLOCATE_STACK    	0

/* Linux has socklen_t */
#define PJ_HAS_SOCKLEN_T		1

/*
 struct pj_sockaddr_in has additional field (see sock.h)
 char	   __pad[8];	/**< Padding.
*/
#define PJ_SOCKADDR_HAS_PAD	1

/* 
 If 1, use Read/Write mutex emulation for platforms that don't support it 
 Android does not have build in suport for POSIX RW mutex ( the same probem as for Win32 ).
 This functions are implemented on os_rwmutex.c & os_rwmutex.h files
*/
#define PJ_EMULATE_RWMUTEX	1


/*
 * PJMEDIA settings
 */

/* Disable non-Android audio devices */
#define PJMEDIA_AUDIO_DEV_HAS_PORTAUDIO     0
#define PJMEDIA_AUDIO_DEV_HAS_WMME          0
#define PJMEDIA_AUDIO_DEV_HAS_ANDROID       1

#define COMPATIBLE_ALSA 1

/*
We should not use threads.h because there is internal implementation of threads and
it can be changed on any time. However we need on possibility to increase thread priority.

Comments from threads.h see below

     * ***********************************************
     * ** Keep in sync with android.os.Process.java **
     * ***********************************************
     * 
     * This maps directly to the "nice" priorites we use in Android.
     * A thread priority should be chosen inverse-proportinally to
     * the amount of work the thread is expected to do. The more work
     * a thread will do, the less favorable priority it should get so that 
     * it doesn't starve the system. Threads not behaving properly might
     * be "punished" by the kernel.
     * Use the levels below when appropriate. Intermediate values are
     * acceptable, preferably use the {MORE|LESS}_FAVORABLE constants below.

*/

#ifndef _LIBS_UTILS_THREADS_H
enum {	
    ANDROID_PRIORITY_LOWEST         =  19,

    // use for background tasks 
    ANDROID_PRIORITY_BACKGROUND     =  10,
    
    // most threads run at normal priority 
    ANDROID_PRIORITY_NORMAL         =   0,
    
    // threads currently running a UI that the user is interacting with 
    ANDROID_PRIORITY_FOREGROUND     =  -2,

    // the main UI thread has a slightly more favorable priority 
    ANDROID_PRIORITY_DISPLAY        =  -4,
    
    // ui service treads might want to run at a urgent display (uncommon) 
    ANDROID_PRIORITY_URGENT_DISPLAY =  -8,
    
    // all normal audio threads 
    ANDROID_PRIORITY_AUDIO          = -16,
    
    // service audio threads (uncommon) 
    ANDROID_PRIORITY_URGENT_AUDIO   = -19,

    // should never be used in practice. regular process might not 
    // be allowed to use this level 
    ANDROID_PRIORITY_HIGHEST        = -20,

    ANDROID_PRIORITY_DEFAULT        = ANDROID_PRIORITY_NORMAL,
    ANDROID_PRIORITY_MORE_FAVORABLE = -1,
    ANDROID_PRIORITY_LESS_FAVORABLE = +1,
};
#endif

#  define PJ_IS_LITTLE_ENDIAN	1
#  define PJ_IS_BIG_ENDIAN	0
#endif	/* __PJ_COMPAT_OS_ANDROID_H__ */

