LOCAL_PATH := $(call my-dir)/../
include $(CLEAR_VARS)

LOCAL_MODULE    := pjmedia

LOCAL_C_INCLUDES := $(LOCAL_PATH)../pjlib/include/ \
	$(LOCAL_PATH)../pjlib-util/include/ \
	$(LOCAL_PATH)../pjnath/include/ \
	$(LOCAL_PATH)include/ \
	$(LOCAL_PATH)../ \
	$(LOCAL_PATH)../third_party/srtp/include \
	$(LOCAL_PATH)../third_party/srtp/include \
	$(LOCAL_PATH)../third_party/srtp/crypto/include \
	$(LOCAL_PATH)../third_party/build/srtp/ \
	$(LOCAL_PATH)../third_party/build/speex/  \
	$(LOCAL_PATH)../third_party/speex/include \
	$(LOCAL_PATH)../third_party/g729/include  \
	$(LOCAL_PATH)../third_party/rcutils/       \
	$(LOCAL_PATH)../third_party/silk/         \
 	$(LOCAL_PATH)../pjlib/include \
	$(LOCAL_PATH)../pjlib-util/include \
	$(LOCAL_PATH)../pjnath/include \
	$(LOCAL_PATH)../pjmedia/include \
	$(LOCAL_PATH)../third_party/tsc/include/ \
	$(LOCAL_PATH)../third_party\silk\SILK_SDK_SRC_FIX_v1.0.8\interface  
	

LOCAL_CFLAGS := $(RC_PJSIP_LIB_FLAGS)
PJSIP_LIB_SRC_DIR := src/pjmedia
PJMEDIADEV_SRC_DIR := src/pjmedia-audiodev
PJMEDIACODEC_SRC_DIR := src/pjmedia-codec

LOCAL_SRC_FILES := $(PJSIP_LIB_SRC_DIR)/alaw_ulaw.c \
	$(PJSIP_LIB_SRC_DIR)/alaw_ulaw_table.c \
	$(PJSIP_LIB_SRC_DIR)/bidirectional.c \
	$(PJSIP_LIB_SRC_DIR)/clock_thread.c \
	$(PJSIP_LIB_SRC_DIR)/codec.c \
	$(PJSIP_LIB_SRC_DIR)/conference.c \
	$(PJSIP_LIB_SRC_DIR)/conf_switch.c \
	$(PJSIP_LIB_SRC_DIR)/delaybuf.c \
	$(PJSIP_LIB_SRC_DIR)/echo_common.c \
	$(PJSIP_LIB_SRC_DIR)/echo_port.c \
	$(PJSIP_LIB_SRC_DIR)/echo_suppress.c \
	$(PJSIP_LIB_SRC_DIR)/endpoint.c \
	$(PJSIP_LIB_SRC_DIR)/errno.c \
	$(PJSIP_LIB_SRC_DIR)/g711.c \
	$(PJSIP_LIB_SRC_DIR)/jbuf.c \
	$(PJSIP_LIB_SRC_DIR)/master_port.c \
	$(PJSIP_LIB_SRC_DIR)/mem_capture.c \
	$(PJSIP_LIB_SRC_DIR)/mem_player.c \
	$(PJSIP_LIB_SRC_DIR)/null_port.c \
	$(PJSIP_LIB_SRC_DIR)/plc_common.c \
	$(PJSIP_LIB_SRC_DIR)/port.c \
	$(PJSIP_LIB_SRC_DIR)/splitcomb.c \
	$(PJSIP_LIB_SRC_DIR)/resample_resample.c \
	$(PJSIP_LIB_SRC_DIR)/resample_libsamplerate.c \
	$(PJSIP_LIB_SRC_DIR)/resample_port.c \
	$(PJSIP_LIB_SRC_DIR)/rtcp.c \
	$(PJSIP_LIB_SRC_DIR)/rtcp_xr.c \
	$(PJSIP_LIB_SRC_DIR)/rtp.c \
	$(PJSIP_LIB_SRC_DIR)/sdp.c \
	$(PJSIP_LIB_SRC_DIR)/sdp_cmp.c \
	$(PJSIP_LIB_SRC_DIR)/sdp_neg.c \
	$(PJSIP_LIB_SRC_DIR)/session.c \
	$(PJSIP_LIB_SRC_DIR)/silencedet.c \
	$(PJSIP_LIB_SRC_DIR)/sound_port.c \
	$(PJSIP_LIB_SRC_DIR)/stereo_port.c \
	$(PJSIP_LIB_SRC_DIR)/stream.c \
	$(PJSIP_LIB_SRC_DIR)/tonegen.c \
	$(PJSIP_LIB_SRC_DIR)/transport_adapter_sample.c \
	$(PJSIP_LIB_SRC_DIR)/transport_ice.c \
	$(PJSIP_LIB_SRC_DIR)/transport_loop.c \
	$(PJSIP_LIB_SRC_DIR)/transport_srtp.c \
	$(PJSIP_LIB_SRC_DIR)/transport_udp.c \
	$(PJSIP_LIB_SRC_DIR)/wav_player.c \
	$(PJSIP_LIB_SRC_DIR)/wav_playlist.c \
	$(PJSIP_LIB_SRC_DIR)/wav_writer.c \
	$(PJSIP_LIB_SRC_DIR)/wave.c \
	$(PJSIP_LIB_SRC_DIR)/wsola.c \
	$(PJMEDIADEV_SRC_DIR)/audiodev.c \
	$(PJMEDIADEV_SRC_DIR)/audiotest.c \
	$(PJMEDIADEV_SRC_DIR)/errno.c 

ifeq ($(RC_PJSIP_USE_SPEEX_EC),1)
	LOCAL_SRC_FILES += $(PJSIP_LIB_SRC_DIR)/echo_speex.c 
endif
ifeq ($(RC_PJSIP_USE_G729),1)
	LOCAL_SRC_FILES += $(PJMEDIACODEC_SRC_DIR)/g729.c
endif
ifeq ($(RC_PJSIP_USE_G722),1)
	LOCAL_SRC_FILES += $(PJMEDIACODEC_SRC_DIR)/g722.c \
				 $(PJMEDIACODEC_SRC_DIR)/g722/g722_enc.c \
				 $(PJMEDIACODEC_SRC_DIR)/g722/g722_dec.c
endif
ifeq ($(RC_PJSIP_USE_SPEEX),1)
	LOCAL_SRC_FILES += $(PJMEDIACODEC_SRC_DIR)/speex_codec.c 
endif
ifeq ($(RC_PJSIP_USE_ILBC),1)
	LOCAL_SRC_FILES += $(PJMEDIACODEC_SRC_DIR)/ilbc.c 
endif
ifeq ($(RC_PJSIP_USE_GSM),1)
	LOCAL_SRC_FILES += $(PJMEDIACODEC_SRC_DIR)/gsm.c 
endif
ifeq ($(RC_PJSIP_USE_SILK),1)
	#martin :moved to silk 
	LOCAL_SRC_FILES += $(PJMEDIACODEC_SRC_DIR)/silk.c 
endif


include $(BUILD_STATIC_LIBRARY)

