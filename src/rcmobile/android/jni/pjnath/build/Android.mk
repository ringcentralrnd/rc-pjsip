LOCAL_PATH := $(call my-dir)/../
include $(CLEAR_VARS)

LOCAL_MODULE    := pjnath

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../pjlib/include/ $(LOCAL_PATH)/../pjlib-util/include/ $(LOCAL_PATH)/include/
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../third_party/tsc/include/ 
LOCAL_CFLAGS := $(RC_PJSIP_LIB_FLAGS)
PJSIP_LIB_SRC_DIR := ./src/pjnath

LOCAL_SRC_FILES := $(PJSIP_LIB_SRC_DIR)/errno.c \
	$(PJSIP_LIB_SRC_DIR)/ice_session.c \
	$(PJSIP_LIB_SRC_DIR)/ice_strans.c \
	$(PJSIP_LIB_SRC_DIR)/nat_detect.c \
	$(PJSIP_LIB_SRC_DIR)/stun_auth.c \
	$(PJSIP_LIB_SRC_DIR)/stun_msg.c \
	$(PJSIP_LIB_SRC_DIR)/stun_msg_dump.c \
	$(PJSIP_LIB_SRC_DIR)/stun_session.c \
	$(PJSIP_LIB_SRC_DIR)/stun_sock.c \
	$(PJSIP_LIB_SRC_DIR)/stun_transaction.c \
	$(PJSIP_LIB_SRC_DIR)/turn_session.c \
	$(PJSIP_LIB_SRC_DIR)/turn_sock.c 


include $(BUILD_STATIC_LIBRARY)

