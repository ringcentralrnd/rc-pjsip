/*
 * rc_android_audio_dev.h
 *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 *  RingCentral Audio Device for Android
 */

#ifndef RC_ANDROID_AUDIO_DEV_H_
#define RC_ANDROID_AUDIO_DEV_H_

#include <pjmedia-audiodev/audiodev_imp.h>
#include <pj/assert.h>
#include <pj/log.h>
#include <pj/os.h>
#include <pj/string.h>
#include <sys/resource.h>
#include <pthread.h>
#include <jni.h>

PJ_BEGIN_DECL

//PJSIP declaration for Audio Device factory
PJ_DECL(pjmedia_aud_dev_factory*) pjmedia_rc_android_audio_dev_factory(pj_pool_factory *pf);

/*
 * Initialize the audio device factory.
 */
static pj_status_t rc_audio_factory_init(pjmedia_aud_dev_factory *f);

/*
 * Close this audio device factory and release all resources back to the operating system.
 */
static pj_status_t rc_audio_factory_destroy(pjmedia_aud_dev_factory *f);

/*
 * Get the number of audio devices installed in the system.
 */
static unsigned 	rc_audio_factory_get_dev_count(pjmedia_aud_dev_factory *f);

/*
 * Get the audio device information and capabilities.
 */
static pj_status_t	rc_audio_factory_get_dev_info(pjmedia_aud_dev_factory *f,
											unsigned index,
											pjmedia_aud_dev_info *info);

/*
 * Initialize the specified audio device parameter with the default
 * values for the specified device.
 */
static pj_status_t rc_audio_factory_default_param(pjmedia_aud_dev_factory *f,
											unsigned index,
											pjmedia_aud_param *param);
/*
 * Open the audio device and create audio stream.
 */
static pj_status_t rc_audio_factory_create_stream(pjmedia_aud_dev_factory *f,
											 const pjmedia_aud_param *param,
											 pjmedia_aud_rec_cb rec_cb,
											 pjmedia_aud_play_cb play_cb,
											 void *user_data,
											 pjmedia_aud_stream **p_aud_strm);


/**
 * See #pjmedia_aud_stream_get_param()
 */
static pj_status_t rc_audio_stream_get_param(pjmedia_aud_stream *strm, pjmedia_aud_param *param);

/**
 * See #pjmedia_aud_stream_get_cap()
 */
static pj_status_t rc_audio_stream_get_cap(pjmedia_aud_stream *strm, pjmedia_aud_dev_cap cap,  void *value);

/**
 * See #pjmedia_aud_stream_set_cap()
 */
static pj_status_t rc_audio_stream_set_cap(pjmedia_aud_stream *strm, pjmedia_aud_dev_cap cap, const void *value);

/**
 * See #pjmedia_aud_stream_start()
 */
static pj_status_t rc_audio_stream_start(pjmedia_aud_stream *strm);

/**
 * See #pjmedia_aud_stream_stop().
 */
static pj_status_t rc_audio_stream_stop(pjmedia_aud_stream *strm);

/**
 * See #pjmedia_aud_stream_destroy().
 */
static pj_status_t rc_audio_stream_destroy(pjmedia_aud_stream *strm);


PJ_END_DECL

#endif /* RC_ANDROID_AUDIO_DEV_H_ */
