/*
 * rc_android_audio_dev_java.h
 *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 *  RingCentral Audio Device for Android
 */

#ifndef RC_ANDROID_AUDIO_DEV_JAVA_H_
#define RC_ANDROID_AUDIO_DEV_JAVA_H_

#include <pjmedia-audiodev/audiodev_imp.h>
#include <jni.h>

#ifndef PJMEDIA_HAS_SBACP_EC
#define PJMEDIA_HAS_SBACP_EC 1
#endif

#include <SBACP_API.h>
#include <SBACP_Ver.h>

#ifndef ERROR_
#define ERROR_(expr) PJ_LOG(1, (THIS_FILE, expr ))
#endif

#define RC_TRACE 1

#if defined(RC_TRACE) && RC_TRACE!=0
	#define TRACE_(expr) PJ_LOG(4, (THIS_FILE, expr ))
	#define TRACE__(expr,prm) PJ_LOG(4, (THIS_FILE, expr, prm ))
#else
	#define TRACE_(expr)
	#define TRACE__(expr,prm)
#endif

/*
 * Structure is used for creating Audio Driver factory
 */
struct rc_android_audio_factory_java
{
	pjmedia_aud_dev_factory base;
	pj_pool_factory *pf;
	pj_pool_t *pool;
    pjmedia_aud_dev_info	 dev_info;
};

/* Sound stream. */
struct rc_android_audio_stream_java
{
    pjmedia_aud_stream	 base;		    /**< Base stream	   */
    pjmedia_aud_param	 param;		    /**< Settings	       */
    pj_pool_t           *pool;          /**< Memory pool.      */
    void                *user_data;     /**< Application data. */

    pjmedia_aud_rec_cb   rec_cb;        /**< Capture callback. */
    pjmedia_aud_play_cb  play_cb;       /**< Playback function.*/

	jclass 				audiodev_class;
    int 				bytes_per_frame;
	pj_int16_t*		  	frame_buf; //buffer has 1 frame size

	pj_int16_t*			aecFrameBuffSpk; // 1 frame buffer
	pj_int16_t* 		aecFrameBuffMic; // 1 frame buffer

#if defined(PJMEDIA_HAS_SBACP_EC) && PJMEDIA_HAS_SBACP_EC!=0
	ShortReal 		  	*speaker_buf_in;
	ShortReal 		  	*microphone_buf_in[1];
	ShortReal		  	*speaker_buf_out;
	ShortReal		  	*microphone_buf_out;
	I_SBACP      		*pSBACP;

    unsigned 			aec_samples_per_frame;
    unsigned 			aec_clock_rate;
    int 				aec_cycles;
#endif

};


PJ_BEGIN_DECL

//PJSIP declaration for Audio Device factory
PJ_DECL(pjmedia_aud_dev_factory*) pjmedia_rc_android_audio_dev_java_factory(pj_pool_factory *pf);

/*
 * Initialize the audio device factory.
 */
static pj_status_t rc_audio_factory_java_init(pjmedia_aud_dev_factory *f);

/*
 * Close this audio device factory and release all resources back to the operating system.
 */
static pj_status_t rc_audio_factory_java_destroy(pjmedia_aud_dev_factory *f);

/*
 * Get the number of audio devices installed in the system.
 */
static unsigned 	rc_audio_factory_java_get_dev_count(pjmedia_aud_dev_factory *f);

/*
 * Get the audio device information and capabilities.
 */
static pj_status_t	rc_audio_factory_java_get_dev_info(pjmedia_aud_dev_factory *f,
											unsigned index,
											pjmedia_aud_dev_info *info);

/*
 * Initialize the specified audio device parameter with the default
 * values for the specified device.
 */
static pj_status_t rc_audio_factory_java_default_param(pjmedia_aud_dev_factory *f,
											unsigned index,
											pjmedia_aud_param *param);
/*
 * Open the audio device and create audio stream.
 */
static pj_status_t rc_audio_factory_java_create_stream(pjmedia_aud_dev_factory *f,
											 const pjmedia_aud_param *param,
											 pjmedia_aud_rec_cb rec_cb,
											 pjmedia_aud_play_cb play_cb,
											 void *user_data,
											 pjmedia_aud_stream **p_aud_strm);


/**
 * See #pjmedia_aud_stream_get_param()
 */
static pj_status_t rc_audio_stream_java_get_param(pjmedia_aud_stream *strm, pjmedia_aud_param *param);

/**
 * See #pjmedia_aud_stream_get_cap()
 */
static pj_status_t rc_audio_stream_java_get_cap(pjmedia_aud_stream *strm, pjmedia_aud_dev_cap cap,  void *value);

/**
 * See #pjmedia_aud_stream_set_cap()
 */
static pj_status_t rc_audio_stream_java_set_cap(pjmedia_aud_stream *strm, pjmedia_aud_dev_cap cap, const void *value);

/**
 * See #pjmedia_aud_stream_start()
 */
static pj_status_t rc_audio_stream_java_start(pjmedia_aud_stream *strm);

/**
 * See #pjmedia_aud_stream_stop().
 */
static pj_status_t rc_audio_stream_java_stop(pjmedia_aud_stream *strm);

/**
 * See #pjmedia_aud_stream_destroy().
 */
static pj_status_t rc_audio_stream_java_destroy(pjmedia_aud_stream *strm);


PJ_END_DECL

#endif /* RC_ANDROID_AUDIO_DEV_H_ */
