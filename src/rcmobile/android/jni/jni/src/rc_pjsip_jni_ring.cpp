/*
 * rc_pjsip_jni_ring.cpp
 *
 *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 *  Implementation of PJSIP JNI interface
 *  SIP Stack Service functions functions.
 */

#include "rc_pjsip_jni.h"

#define THIS_FILE	"rc_pjsip_jni_ring.cpp"

#ifdef __cplusplus
extern "C" {
#endif

/* Ringtones		    US	       UK  */
#define RINGBACK_FREQ1	    440	    /* 400 */
#define RINGBACK_FREQ2	    480	    /* 450 */
#define RINGBACK_ON	    	2000    /* 400 */
#define RINGBACK_OFF	    4000    /* 200 */
#define RINGBACK_CNT	    1	    /* 2   */
#define RINGBACK_INTERVAL   4000    /* 2000 */

//#define RING_FREQ1	    	800
//#define RING_FREQ2	    	640
//#define RING_ON		    	200
//#define RING_OFF	    	100
//#define RING_CNT	    	3
//#define RING_INTERVAL		3000

#define RING_FREQ1	    	440
#define RING_FREQ2	    	480
#define RING_ON		    	2000
#define RING_OFF	    	4000
#define RING_CNT	    	1
#define RING_INTERVAL		4000

/*
 * Initialize Ringback port
 * ringback_init()
 */
pj_status_t ringback_init(const pjsua_media_config *media_cfg){
	pj_status_t status;
	unsigned samples_per_frame, i;
	pjmedia_tone_desc tone[RINGBACK_CNT];
	pj_str_t name;

	rc_cfg.ringback.cnt = 0;
	rc_cfg.ringback.slot = PJSUA_INVALID_ID;

	samples_per_frame = (media_cfg->audio_frame_ptime * media_cfg->clock_rate * media_cfg->channel_count) / 1000;
	 pj_strdup2_with_null( rc_cfg.pool, &name, "ringback");

	status = pjmedia_tonegen_create2(rc_cfg.pool,
					&name,
					media_cfg->clock_rate,
					media_cfg->channel_count,
					samples_per_frame,
					16,
					PJMEDIA_TONEGEN_LOOP,
					&rc_cfg.ringback.port);

	if (status != PJ_SUCCESS){
	    PJ_LOG(2, (THIS_FILE, "ring_init pjmedia_tonegen_create2 failed Result=%d", status ));
		return status;
	}

	pj_bzero(&tone, sizeof(tone));
	for (i = 0; i < RINGBACK_CNT; ++i) {
	    tone[i].freq1 		= RINGBACK_FREQ1;
	    tone[i].freq2 		= RINGBACK_FREQ2;
	    tone[i].on_msec 	= RINGBACK_ON;
	    tone[i].off_msec 	= RINGBACK_OFF;
	}

	tone[RINGBACK_CNT-1].off_msec = RINGBACK_INTERVAL;

	pjmedia_tonegen_play(rc_cfg.ringback.port, RINGBACK_CNT, tone, PJMEDIA_TONEGEN_LOOP );

	status = pjsua_conf_add_port(rc_cfg.pool,
			rc_cfg.ringback.port,
		    &rc_cfg.ringback.slot);

	if (status != PJ_SUCCESS){
	    PJ_LOG(2, (THIS_FILE, "ring_init pjsua_conf_add_port failed Result=%d", status ));
	}

	PJ_LOG(4, (THIS_FILE, "ringback_init finished" ));

	return status;
}

/*
 *	ring_init()
 */
pj_status_t ring_init(const pjsua_media_config *media_cfg){
	pj_status_t status;
	unsigned samples_per_frame, i;
	pjmedia_tone_desc tone[RING_CNT];
	pj_str_t name;

	rc_cfg.ring.cnt = 0;
	rc_cfg.ring.slot = PJSUA_INVALID_ID;

	samples_per_frame = (media_cfg->audio_frame_ptime * media_cfg->clock_rate * media_cfg->channel_count) / 1000;
	pj_strdup2_with_null( rc_cfg.pool, &name, "ring");

	PJ_LOG(4, (THIS_FILE, "ring_init ptime:%d clock rate:%d channels:%d samples:%d",
			media_cfg->audio_frame_ptime,
			media_cfg->clock_rate,
			media_cfg->channel_count,
			samples_per_frame));


	status = pjmedia_tonegen_create2(rc_cfg.pool,
					&name,
					media_cfg->clock_rate,
					media_cfg->channel_count,
					samples_per_frame,
					16,
					PJMEDIA_TONEGEN_LOOP,
					&rc_cfg.ring.port);

	if (status != PJ_SUCCESS){
	    PJ_LOG(2, (THIS_FILE, "ring_init pjmedia_tonegen_create2 failed Result=%d", status ));
		return status;
	}

	pj_bzero(&tone, sizeof(tone));
	for (i = 0; i < RING_CNT; ++i ) {
	    tone[i].freq1 		= RING_FREQ1;
	    tone[i].freq2 		= RING_FREQ2;
	    tone[i].on_msec 	= RING_ON;
	    tone[i].off_msec 	= RING_OFF;
	}

	tone[ RING_CNT - 1].off_msec = RING_INTERVAL;

	pjmedia_tonegen_play(rc_cfg.ring.port, RING_CNT, tone, PJMEDIA_TONEGEN_LOOP );

	status = pjsua_conf_add_port(rc_cfg.pool,
			rc_cfg.ring.port,
		    &rc_cfg.ring.slot);

	if (status != PJ_SUCCESS){
	    PJ_LOG(2, (THIS_FILE, "ring_init pjsua_conf_add_port failed Result=%d", status ));
	}

	PJ_LOG(4, (THIS_FILE, "ring_init finished" ));

	return status;
}

/*
 *	ringback_start()
 */
void ringback_start(pjsua_call_id call_id)
{
    PJ_LOG(4, (THIS_FILE, "ringback_start call ID %d ringback counter:%d", call_id, rc_cfg.ringback.cnt ));

    if (++rc_cfg.ringback.cnt == 1 && rc_cfg.ringback.slot != PJSUA_INVALID_ID ){
		pjsua_conf_connect( rc_cfg.ringback.slot, 0 );
	}
}

/*
 * ring_start()
 */
void ring_start(pjsua_call_id call_id)
{
    PJ_LOG(4, (THIS_FILE, "ring_start call ID %d ring counter:%d", call_id, rc_cfg.ring.cnt ));

    if (++rc_cfg.ring.cnt == 1 && rc_cfg.ring.slot != PJSUA_INVALID_ID) {
		pjsua_conf_connect( rc_cfg.ring.slot, 0 );
	}
}

/*
 * ring_stop()
 */
void ring_stop(pjsua_call_id call_id)
{
    PJ_LOG(4, (THIS_FILE, "ring_stop call ID %d Counters ring:%d ringback:%d", call_id, rc_cfg.ring.cnt, rc_cfg.ringback.cnt ));

	if ( rc_cfg.ringback.cnt > 0 ) {
//		if (--rc_cfg.ringback.cnt == 0 && rc_cfg.ringback.slot != PJSUA_INVALID_ID) {
		rc_cfg.ringback.cnt = 0;
		if ( rc_cfg.ringback.slot != PJSUA_INVALID_ID) {
			pjsua_conf_disconnect(rc_cfg.ringback.slot, 0);
			pjmedia_tonegen_rewind(rc_cfg.ringback.port);
		}
	}

	if ( rc_cfg.ring.cnt > 0 ) {
		//if (--rc_cfg.ring.cnt == 0 && rc_cfg.ring.slot != PJSUA_INVALID_ID) {
		rc_cfg.ring.cnt = 0;
		if (rc_cfg.ring.slot != PJSUA_INVALID_ID) {
			pjsua_conf_disconnect(rc_cfg.ring.slot, 0);
			pjmedia_tonegen_rewind(rc_cfg.ring.port);
		}
	}
}

/*
 *	ring_destroy()
 */
void ring_destroy()
{
    PJ_LOG(4, (THIS_FILE, "ring_destroy" ));

    if (rc_cfg.ring.port && rc_cfg.ring.slot != PJSUA_INVALID_ID) {
		pjsua_conf_remove_port(rc_cfg.ring.slot);
		rc_cfg.ring.slot = PJSUA_INVALID_ID;
		pjmedia_port_destroy(rc_cfg.ring.port);
		rc_cfg.ring.port = NULL;
	}
}
/*
 *	ringback_destroy()
 */
void ringback_destroy()
{
    PJ_LOG(4, (THIS_FILE,  "ringback_destroy" ));

    if (rc_cfg.ringback.port && rc_cfg.ringback.slot != PJSUA_INVALID_ID) {
		pjsua_conf_remove_port(rc_cfg.ringback.slot);
		rc_cfg.ringback.slot = PJSUA_INVALID_ID;
		pjmedia_port_destroy(rc_cfg.ringback.port);
		rc_cfg.ringback.port = NULL;
	}
}

/*
 * pjsip_ring_start()
 */
JNIEXPORT void JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1ring_1start(JNIEnv *, jclass, jint callId )
{
	ring_start( callId );
}

/*
 * 	pjsip_ringback_start()
 */
JNIEXPORT void JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1ringback_1start(JNIEnv *, jclass, jint callId )
{
	ringback_start( callId );
}

/*
 * 	pjsip_ring_stop()
 */
JNIEXPORT void JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1ring_1stop(JNIEnv *, jclass, jint callId )
{
	ring_stop( callId );
}

/*
 * pjsip_set_snd_dev
 *
 * Select or change sound device. Application may call this function at
 * any time to replace current sound device.
 *
 * @param capture_dev   Device ID of the capture device.
 * @param playback_dev	Device ID of the playback device.
 *
 * @return		PJ_SUCCESS on success, or the appropriate error code.
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1set_1snd_1dev(JNIEnv *,
		jclass,
		jint capture_dev,
		jint playback_dev )
{
	return (jint)pjsua_set_snd_dev( capture_dev, playback_dev );
}

/*
 * Method:    pjsip_get_snd_dev
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1get_1snd_1dev(JNIEnv *jenv,
		jclass, jintArray devices)
{
	pj_status_t status;
	int capture_dev, playback_dev;

	jint size = jenv->GetArrayLength( devices );
	if( size < 2 ){
		pjsua_perror(THIS_FILE, "pjsip_get_snd_dev failed ", PJ_ETOOSMALL );
		return PJ_ETOOSMALL;
	}

	status = pjsua_get_snd_dev( &capture_dev, &playback_dev );

	if (status != PJ_SUCCESS){
	    PJ_LOG(2, (THIS_FILE, "ring_init pjsua_conf_add_port failed Result=%d", status ));
	}
	else{
		  jenv->SetIntArrayRegion(devices, 0, 1, &capture_dev );
		  jenv->SetIntArrayRegion(devices, 1, 1, &playback_dev );
	}

	PJ_LOG(4, (THIS_FILE, "pjsip_get_snd_dev finished" ));
	return (jint)status;
}
/*
 * pjsip_set_no_snd_dev
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1set_1no_1snd_1dev(JNIEnv *, jclass)
{
	pjmedia_port* mport = pjsua_set_no_snd_dev();
	return ( mport == NULL ? PJ_EUNKNOWN : PJ_SUCCESS );
}

#ifdef __cplusplus
}
#endif
