/*
 * rc_pjsip_jni_message.cpp
 *
 *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 *  Implementation of PJSIP JNI interface
 *  SIP Stack Message send functions.
 */

#include <rc_pjsip_jni.h>

#define THIS_FILE	"rc_pjsip_jni_message.cpp"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Method:    pjsip_message_send
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1message_1send(JNIEnv *jenv,
		jclass,
		jint accountId,
		jstring dstURI,
		jstring content )
{
	pj_status_t status;
	pj_str_t pj_dst_uri;
	pj_str_t pj_content;
    pj_str_t pj_mime_text_plain;

	PJ_LOG(4, (THIS_FILE, "pjsip_message_send is starting... " ));

	const char* pdst_uri;
	const char* pcontent;

	pj_strdup2_with_null( rc_cfg.pool, &pj_mime_text_plain, "x-rc/agent" );

	pdst_uri = jenv->GetStringUTFChars( dstURI, JNI_FALSE );
	pj_strdup2_with_null( rc_cfg.pool, &pj_dst_uri, pdst_uri );
	jenv->ReleaseStringUTFChars( dstURI, pdst_uri );

	pcontent = jenv->GetStringUTFChars( content, JNI_FALSE );
	pj_strdup2_with_null( rc_cfg.pool, &pj_content, pcontent );
	jenv->ReleaseStringUTFChars( content, pcontent );

	status = pjsua_im_send( accountId, &pj_dst_uri, &pj_mime_text_plain, &pj_content, NULL, NULL );
	if( status != PJ_SUCCESS ){
		pjsua_perror(THIS_FILE, "pjsip_message_send failed ", status );
		return status;
	}

	PJ_LOG(4, (THIS_FILE, "pjsip_message_send finished %d", status ));
	return status;
}

#ifdef __cplusplus
}
#endif
