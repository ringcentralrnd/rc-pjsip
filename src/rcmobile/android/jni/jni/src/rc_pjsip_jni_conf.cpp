/*
 * rc_pjsip_jni_conf.cpp
*
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 *  Implementation of PJSIP JNI interface
 *  SIP Stack Codec functions.
 */

#include <rc_pjsip_jni.h>

#define THIS_FILE	"rc_pjsip_jni_codec.cpp"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef NORMAL_LEVEL
#define NORMAL_LEVEL	    128
#endif

/*
 * pjsip_conf_get_signal_rx_level
 */
JNIEXPORT jfloat JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1get_1signal_1rx_1level(JNIEnv *jni_env,
  jclass,
  jint slot)
{
	pj_status_t status;
	jfloat level = -1;
	unsigned rx_level = 0, tx_level = 0;

	status = pjsua_conf_get_signal_level( slot, &tx_level, &rx_level );
	if( status == PJ_SUCCESS ){
	    PJ_LOG(4, (THIS_FILE, "pjsip_conf_get_signal_rx_level TX:%f RX:%f", tx_level, rx_level ));
		level = rx_level;
	}
	else
	    PJ_LOG(2, (THIS_FILE, "pjsip_conf_get_signal_rx_level failed Result=%d", status ));

	return level;
}

/*
 * pjsip_conf_get_signal_tx_level
 */
JNIEXPORT jfloat JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1get_1signal_1tx_1level(JNIEnv *jni_env,
  jclass,
  jint slot)
{
	pj_status_t status;
	jfloat level = -1;
	unsigned rx_level = 0, tx_level = 0;

	status = pjsua_conf_get_signal_level( slot, &tx_level, &rx_level );
	if( status == PJ_SUCCESS ){
	    PJ_LOG(4, (THIS_FILE, "pjsip_conf_get_signal_tx_level TX:%f RX:%f", tx_level, rx_level ));
		level = tx_level;
	}
	else{
	    PJ_LOG(2, (THIS_FILE, "pjsip_conf_get_signal_tx_level failed Result=%d", status ));
	}

	return level;
}

/*
 * pjsip_conf_adjust_tx_level
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1adjust_1tx_1level(JNIEnv *jni_env,
	jclass,
	jint slot,
	jfloat level)
{
	float tx_level = level;
	pj_status_t status = pjsua_conf_adjust_tx_level( slot, tx_level );
	if( status != PJ_SUCCESS ){
	    PJ_LOG(2, (THIS_FILE, "pjsip_conf_adjust_tx_level failed Result=%d", status ));
	}

	return status;
}

/*
 * pjsip_conf_adjust_rx_level
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1adjust_1rx_1level(JNIEnv *jni_env,
	jclass,
	jint slot,
	jfloat level)
{
	float rx_level = level;
	pj_status_t status = pjsua_conf_adjust_rx_level( slot, rx_level );
	if( status != PJ_SUCCESS ){
	    PJ_LOG(2, (THIS_FILE, "pjsip_conf_adjust_rx_level failed Result=%d", status ));
	}

	return status;
}

/*
 * pjsip_conf_get_adjust_tx_level
 */
JNIEXPORT jfloat JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1get_1adjust_1tx_1level(JNIEnv *,
		jclass,
		jint slot)
{
	jfloat level = 0;
	pjsua_conf_port_info info;

	pj_status_t status =  pjsua_conf_get_port_info( slot, &info );
	if( status == PJ_SUCCESS ){
	    PJ_LOG(4, (THIS_FILE, "pjsip_conf_get_adjust_tx_level TX:%d RX:%d", info.tx_adj_level, info.rx_adj_level ));
		level = info.tx_adj_level +  + NORMAL_LEVEL;
	}

	return level;
}

/*
 * pjsip_conf_get_adjust_rx_level
 */
JNIEXPORT jfloat JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1get_1adjust_1rx_1level(JNIEnv *,
		jclass,
		jint slot )
{
	jfloat level = 0;
	pjsua_conf_port_info info;

	pj_status_t status =  pjsua_conf_get_port_info( slot, &info );
	if( status == PJ_SUCCESS ){
	    PJ_LOG(4, (THIS_FILE, "pjsip_conf_get_adjust_rx_level TX:%d RX:%d", info.tx_adj_level, info.rx_adj_level ));
		level = info.rx_adj_level + NORMAL_LEVEL;
	}

	return level;
}


#ifdef __cplusplus
}
#endif
