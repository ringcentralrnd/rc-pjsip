/*
 * rc_android_audio_dev_java.cpp
  *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 *  Implementation of RingCentral Audio Device for Android (JAVA)
 */

#include "rc_android_audio_dev_java.h"
#include <pjsua-lib/pjsua.h>
#include <pjsua-lib/pjsua_internal.h>
#include "rc_pjsip_jni.h"

#define THIS_FILE	"rc_pjsip_jni_audio_dev.cpp"

#define driver_name	"rc_android"

#define JNI_EXCEPTION_INIT() bool jni_exc_occurred = false;\
		jthrowable jni_exception = 0;

#define JNI_EXCEPTION_CHECK(update_flag)	jni_exception = jni_env->ExceptionOccurred();\
		if( jni_exception ){\
			PJ_LOG(2, (THIS_FILE, "JNI exception is detected"));\
			jni_env->ExceptionDescribe();\
			jni_env->ExceptionClear();\
			jni_exception = 0;\
			if(update_flag)\
				jni_exc_occurred = true;\
		}\

#define IF_JNI_EXCEPTION() jni_exc_occurred

#define JNI_EXCEPTION_CALLBACK(error_code) 	if( jni_env != 0 &&\
		rc_cfg.rc_callbacks.cb_class != 0 &&\
		rc_cfg.rc_callbacks.cb_methodId[on_media_failed_idx] != 0 ){\
		int error = error_code;\
		jni_env->CallStaticVoidMethod( rc_cfg.rc_callbacks.cb_class,\
				rc_cfg.rc_callbacks.cb_methodId[on_media_failed_idx],\
				error );\
	}\
	else\
		PJ_LOG(3, (THIS_FILE,  "Callback on_media_failed() failed. Wrong class or method ID" ));

// enable or disable using Andrea's AEC
#define PJMEDIA_USE_SBACP_EC

#define SBACP_HAS_HighPassMode	0

#ifndef BITS_PER_SAMPLE
#define BITS_PER_SAMPLE		16
#endif

#define AEC_DEFAULT_DELAY 320 //DK: AEC delay on milliseconds

/* Operations */
static pjmedia_aud_dev_factory_op rc_audio_factory_op =
{
    &rc_audio_factory_java_init,
    &rc_audio_factory_java_destroy,
    &rc_audio_factory_java_get_dev_count,
    &rc_audio_factory_java_get_dev_info,
    &rc_audio_factory_java_default_param,
    &rc_audio_factory_java_create_stream
};

/* Sound stream operations. */
static pjmedia_aud_stream_op rc_audio_stream_op =
{
	&rc_audio_stream_java_get_param,
	&rc_audio_stream_java_get_cap,
	&rc_audio_stream_java_set_cap,
	&rc_audio_stream_java_start,
	&rc_audio_stream_java_stop,
	&rc_audio_stream_java_destroy
};

/*
 *
 */
pjmedia_aud_dev_factory* pjmedia_rc_android_audio_dev_java_factory(pj_pool_factory *pf)
{
	pj_pool_t *pool;
	struct rc_android_audio_factory_java *f;

	/* create pool that will be used for audio driver memory allocations */
	pool = pj_pool_create(pf, "rc_android_audio", 64, 64, NULL);
    PJ_ASSERT_RETURN(pool, PJ_ENOMEM);

	f = PJ_POOL_ZALLOC_T(pool, struct rc_android_audio_factory_java);
    PJ_ASSERT_RETURN(f, PJ_ENOMEM);

	f->pf = pf;
	f->pool = pool;
	f->base.op = &rc_audio_factory_op;

    TRACE_("RC Audio factory has been created");

	return &f->base;
}

/*
 * Initialize the audio device factory.
 */
static pj_status_t rc_audio_factory_java_init(pjmedia_aud_dev_factory *f)
{
	struct rc_android_audio_factory_java* rc_af = (struct rc_android_audio_factory_java*)f;

    PJ_ASSERT_RETURN(f, PJ_EINVAL);

	pj_ansi_strcpy(rc_af->dev_info.name, driver_name );

	rc_af->dev_info.default_samples_per_sec = RC_PJSUA_DEFAULT_CLOCK_RATE;
	rc_af->dev_info.caps = 0;
	rc_af->dev_info.input_count = 1;
	rc_af->dev_info.output_count = 1;

    TRACE_("RC Audio factory is initialized");

	return PJ_SUCCESS;
}

/*
 * Close this audio device factory and release all resources back to the operating system.
 */
static pj_status_t rc_audio_factory_java_destroy(pjmedia_aud_dev_factory *f)
{
	struct rc_android_audio_factory_java* rc_af = (struct rc_android_audio_factory_java*)f;
    PJ_ASSERT_RETURN(f, PJ_EINVAL);

	TRACE_("RC Audio factory is destroyed");

	pj_pool_t *pool = rc_af->pool;
	rc_af->pool = NULL;
    pj_pool_release(pool);

    return PJ_SUCCESS;
}

/*
 * Get the number of audio devices installed in the system.
 */
static unsigned rc_audio_factory_java_get_dev_count(pjmedia_aud_dev_factory *f)
{
	PJ_UNUSED_ARG(f);
	return 1;
}

/*
 * Get the audio device information and capabilities.
 */
static pj_status_t	rc_audio_factory_java_get_dev_info(pjmedia_aud_dev_factory *f,
											unsigned index,
											pjmedia_aud_dev_info *info)
{
	struct rc_android_audio_factory_java* rc_af = (struct rc_android_audio_factory_java*)f;

    PJ_ASSERT_RETURN(f, PJ_EINVAL);
    PJ_ASSERT_RETURN(index == 0, PJMEDIA_EAUD_INVDEV);

	pj_bzero(info, sizeof(*info));
	pj_ansi_strcpy(info->name, "RcAudio");
    info->caps 						= rc_af->dev_info.caps;
    info->default_samples_per_sec 	= rc_af->dev_info.default_samples_per_sec;
    info->input_count 				= rc_af->dev_info.input_count;
    info->output_count 				= rc_af->dev_info.output_count;

	pj_ansi_strcpy(info->name, rc_af->dev_info.name );

	PJ_LOG(4,(THIS_FILE, "Get device info: caps:%d sample/sec: %d count In: %d Out:%d",
			info->caps,
			info->default_samples_per_sec,
			info->input_count,
			info->output_count ));


    return PJ_SUCCESS;
}

/*
 * Initialize the specified audio device parameter with the default
 * values for the specified device.
 */
static pj_status_t rc_audio_factory_java_default_param(pjmedia_aud_dev_factory *f,
											unsigned index,
											pjmedia_aud_param *param)
{
	struct rc_android_audio_factory_java* rc_af = (struct rc_android_audio_factory_java*)f;

    PJ_ASSERT_RETURN(f, PJ_EINVAL);
    PJ_ASSERT_RETURN(index == 0, PJMEDIA_EAUD_INVDEV);

    pj_bzero(param, sizeof(*param));

	if (rc_af->dev_info.input_count && rc_af->dev_info.output_count) {
		param->dir = PJMEDIA_DIR_CAPTURE_PLAYBACK;
		param->rec_id = index;
		param->play_id = index;
	} else if (rc_af->dev_info.input_count) {
		param->dir = PJMEDIA_DIR_CAPTURE;
		param->rec_id = index;
		param->play_id = PJMEDIA_AUD_INVALID_DEV;
	} else if (rc_af->dev_info.output_count) {
		param->dir = PJMEDIA_DIR_PLAYBACK;
		param->play_id = index;
		param->rec_id = PJMEDIA_AUD_INVALID_DEV;
	} else {
		return PJMEDIA_EAUD_INVDEV;
	}

	//DK: will use EC tail length as delay for AEC tests
	param->ec_tail_ms = pjsua_get_var()->media_cfg.ec_tail_len;
	param->sbacp_half_duplex = pjsua_get_var()->media_cfg.sbacp_half_duplex;

	param->rec_id 			 = index;
    param->play_id 			 = index;
    param->clock_rate 		 = rc_af->dev_info.default_samples_per_sec;

#if defined(PJMEDIA_HAS_SBACP_EC) && PJMEDIA_HAS_SBACP_EC!=0
    param->clock_rate 		 = RC_PJSUA_DEFAULT_CLOCK_RATE;//SBACP_GetSampleRate();
#else
    param->clock_rate 		 = RC_PJSUA_DEFAULT_CLOCK_RATE;
#endif

    param->channel_count 	 = 1;
    param->samples_per_frame = (param->clock_rate * pjsua_get_var()->media_cfg.audio_frame_ptime )/ 1000;  //DK: 160 samples per frame
    param->bits_per_sample 	 = BITS_PER_SAMPLE;
    param->flags 			 = rc_af->dev_info.caps;
	param->input_latency_ms  = PJMEDIA_SND_DEFAULT_REC_LATENCY;
	param->output_latency_ms = PJMEDIA_SND_DEFAULT_PLAY_LATENCY;

	PJ_LOG(4,(THIS_FILE, "Factory default params: sample/frame %d sample/sec %d ec_tail=%d",
			param->samples_per_frame,
			param->clock_rate,
			param->ec_tail_ms));

	PJ_LOG(4,(THIS_FILE, "Factory default params: rec id: %d play id: %d bits/sample %d latency in: %d ms out: %d ms",
			param->rec_id,
			param->play_id,
			param->bits_per_sample,
			param->input_latency_ms,
			param->output_latency_ms ));

	return PJ_SUCCESS;
}

/*
 * Open the audio device and create audio stream.
 */
static pj_status_t rc_audio_factory_java_create_stream(pjmedia_aud_dev_factory *f,
											 const pjmedia_aud_param *param,
											 pjmedia_aud_rec_cb rec_cb,
											 pjmedia_aud_play_cb play_cb,
											 void *user_data,
											 pjmedia_aud_stream **p_aud_strm)
{
	PJ_LOG(3, (THIS_FILE,"rc_audio_factory_java_create_stream started."));

	struct rc_android_audio_factory_java* rc_af = (struct rc_android_audio_factory_java*)f;
	pj_pool_t *pool;
    struct rc_android_audio_stream_java* stream;
    pj_status_t status;
    jmethodID midCreate = 0;
    jmethodID midTestMode = 0;
    jmethodID midDelayValue = 0;
    int internalSpeakerDelay = 0;
    jboolean jbResult = JNI_FALSE;
    jboolean jbTestMode = JNI_FALSE;

    PJ_ASSERT_RETURN(f, PJ_EINVAL);
	PJ_ASSERT_RETURN(play_cb && rec_cb && p_aud_strm && param, PJ_EINVAL);

	PJ_ASSERT_RETURN( param->bits_per_sample == 8 || param->bits_per_sample == 16, PJMEDIA_EAUD_SAMPFORMAT);

	pool = pj_pool_create(rc_af->pf, "rc_android_audio_dev", 1024, 1024, NULL);
    PJ_ASSERT_RETURN(pool, PJ_ENOMEM);

    stream = PJ_POOL_ZALLOC_T(pool, struct rc_android_audio_stream_java);
    PJ_ASSERT_RETURN(stream, PJ_ENOMEM);

#if defined(DRV_TRACE_INFO) && DRV_TRACE_INFO!=0
    status = createTrace( &stream->strace );
    if( status != PJ_SUCCESS )
    	return status;
#endif

    stream->pool 		= pool;
    stream->param 		= *param;
    stream->user_data 	= user_data;
    stream->rec_cb 		= rec_cb;
    stream->play_cb 	= play_cb;

    stream->frame_buf = (pj_int16_t*)pj_pool_zalloc(pool, sizeof(pj_int16_t) * param->samples_per_frame );
    PJ_ASSERT_RETURN(stream->frame_buf != NULL, PJ_ENOMEM);

    stream->aecFrameBuffSpk = (pj_int16_t*)pj_pool_zalloc(pool, sizeof(pj_int16_t) * param->samples_per_frame );
    PJ_ASSERT_RETURN(stream->aecFrameBuffSpk != NULL, PJ_ENOMEM);

    stream->aecFrameBuffMic = (pj_int16_t*)pj_pool_zalloc(pool, sizeof(pj_int16_t) * param->samples_per_frame );
    PJ_ASSERT_RETURN(stream->aecFrameBuffMic != NULL, PJ_ENOMEM);


	PJ_LOG(4, (THIS_FILE, "Create stream : %d samples/sec, %d samples/frame, %d bytes/sample [%d bits/sample] EC tail %d",
			param->clock_rate,
			param->samples_per_frame,
			param->bits_per_sample/8,
			param->bits_per_sample,
			param->ec_tail_ms ));

#if defined(PJMEDIA_HAS_SBACP_EC) && PJMEDIA_HAS_SBACP_EC!=0

	stream->speaker_buf_in = (ShortReal*)pj_pool_zalloc(pool, sizeof(ShortReal) * param->samples_per_frame );
    PJ_ASSERT_RETURN(stream->speaker_buf_in != NULL, PJ_ENOMEM);

    stream->microphone_buf_in[0] = (ShortReal*)pj_pool_zalloc(pool, sizeof(ShortReal) * param->samples_per_frame );
    PJ_ASSERT_RETURN(stream->microphone_buf_in[0] != NULL, PJ_ENOMEM);

    stream->speaker_buf_out = (ShortReal*)pj_pool_zalloc(pool, sizeof(ShortReal) * param->samples_per_frame );
    PJ_ASSERT_RETURN(stream->speaker_buf_out != NULL, PJ_ENOMEM);

    stream->microphone_buf_out = (ShortReal*)pj_pool_zalloc(pool, sizeof(ShortReal) * param->samples_per_frame );
    PJ_ASSERT_RETURN(stream->microphone_buf_out != NULL, PJ_ENOMEM);

    stream->pSBACP = new I_SBACP();
    if( stream->pSBACP == NULL ){
    	PJ_LOG(1,(THIS_FILE, "Create stream failed. SBACP_Create failed."));
    	return PJ_ENOMEM;
    }

    stream->aec_samples_per_frame	= stream->pSBACP->GetFrameSize();
    stream->aec_clock_rate			= stream->pSBACP->GetSampleRate();
    stream->aec_cycles = ( param->samples_per_frame > stream->aec_samples_per_frame ?
    		(param->samples_per_frame / stream->aec_samples_per_frame ) : 1 );

    stream->bytes_per_frame = param->samples_per_frame * ( param->bits_per_sample / 8 );

    stream->pSBACP->SetNumberOfMics( 1 );
    stream->pSBACP->SetBandLimitMode( SBACP_POTS_4KHZ );
    stream->pSBACP->SetTestMode(SBACP_TEST_OFF);

//    if (param->sbacp_half_duplex != 0) {
//        PJ_LOG(4,(THIS_FILE, "HALF_DUPLEX mode"));
//        stream->pSBACP->SetHalfDuplex(1);
//        stream->pSBACP->SetEnable(AEC_ENABLE, 0 );
//    } else {
        PJ_LOG(4,(THIS_FILE, "FULL_DUPLEX mode"));
//    }

    int delay = ( param->ec_tail_ms > 0 ? param->ec_tail_ms : AEC_DEFAULT_DELAY);
    PJ_LOG(4,(THIS_FILE, "AEC Delay %d [%d]", stream->pSBACP->GetAudioDelay( ), delay ));
    stream->pSBACP->SetAudioDelay( delay );


#ifdef ANDREA_OUTPUT_LOG

    PJ_LOG(4,(THIS_FILE, "SBACP_GetVer   : %s", stream->pSBACP->GetVer() ));
    PJ_LOG(4,(THIS_FILE, "SBACP_GetDate  : %s", stream->pSBACP->GetDate() ));
    PJ_LOG(4,(THIS_FILE, "SBACP_GetProdID: %s", stream->pSBACP->GetProdID() ));
    PJ_LOG(4,(THIS_FILE, "SBACP_GetFrameSize() : %d", stream->pSBACP->GetFrameSize() ));
    PJ_LOG(4,(THIS_FILE, "SBACP_GetSampleRate(): %d", stream->pSBACP->GetSampleRate() ));
    PJ_LOG(4,(THIS_FILE, "SBACP_GetAudioDelay(): %d", stream->pSBACP->GetAudioDelay() ));
//    PJ_LOG(4,(THIS_FILE, "SBACP_GetHalfDuplex(): %d", stream->pSBACP->GetHalfDuplex() ));
    PJ_LOG(4,(THIS_FILE, "SBACP_GetTestMode()  : %d", stream->pSBACP->GetTestMode() ));
    PJ_LOG(4,(THIS_FILE, "SBACP_GetBandLimitMode(): %d", stream->pSBACP->GetBandLimitMode() ));

#if defined(SBACP_HAS_HighPassMode) && SBACP_HAS_HighPassMode!=0
    PJ_LOG(4,(THIS_FILE, "SBACP_GetHighPassMode: %d", stream->pSBACP->GetHighPassMode() ));
#endif


#define AEC_PARAMETER( NPRM, PRM ) PJ_LOG(4,(THIS_FILE, "SBACP_GetEnable(%s): %d", PRM, stream->pSBACP->GetEnable( NPRM ) ));

    AEC_PARAMETER( AEC_ENABLE,          "AEC_ENABLE" )
    AEC_PARAMETER( RESCUE_ENABLE,       "RESCUE_ENABLE" )
    AEC_PARAMETER( POSTFILTER_ENABLE,   "POSTFILTER_ENABLE" )
    AEC_PARAMETER( NOISE_RED_ENABLE,    "NOISE_RED_ENABLE" )
    AEC_PARAMETER( EQUALIZER_ENABLE,    "EQUALIZER_ENABLE" )
    AEC_PARAMETER( TX_AGC_ENABLE,       "TX_AGC_ENABLE" )
    AEC_PARAMETER( RX_AGC_ENABLE,       "RX_AGC_ENABLE" )
    AEC_PARAMETER( MIC_STEER_ENABLE,    "MIC_STEER_ENABLE" )
    AEC_PARAMETER( SPEECH_DET_ENABLE,   "SPEECH_DET_ENABLE" )
    AEC_PARAMETER( TX_MUTE_ENABLE,      "TX_MUTE_ENABLE" )
    AEC_PARAMETER( RX_MUTE_ENABLE,      "RX_MUTE_ENABLE" )
    AEC_PARAMETER( USER_TEST_ENABLE,    "USER_TEST_ENABLE" )
    AEC_PARAMETER( THD_CHECK_ENABLE,    "THD_CHECK_ENABLE" )
    /*
     * We does not have re-sampling possibility
     * that is why we should have the same clock rate value
     */
    PJ_ASSERT_RETURN( echo->aec_clock_rate == param->clock_rate, PJ_EINVAL);

#endif //ANDREA_OUTPUT_LOG

#endif // defined(PJMEDIA_HAS_SBACP_EC) && PJMEDIA_HAS_SBACP_EC!=0

	ATTACH_CURRENT_THREAD(PJ_EUNKNOWN);

	JNI_EXCEPTION_INIT();

	/* Get class */
	//stream->audiodev_class = jni_env->FindClass("com/rcbase/android/sip/audio/AudioDevice");
	stream->audiodev_class = rc_cfg.AudioDriver_class;
	//JNI_EXCEPTION_CHECK(true);

	//if( IF_JNI_EXCEPTION() || stream->audiodev_class == 0 ){
	if( stream->audiodev_class == 0 ){
		ERROR_("Could not get class for com.rcbase.android.sip.audio.AudioDevice");
		goto leave;
	}

	midCreate = jni_env->GetStaticMethodID( stream->audiodev_class, "create", "(IIIIJ)Z" );
	JNI_EXCEPTION_CHECK(true);

	if( IF_JNI_EXCEPTION() || midCreate == 0 ){
		ERROR_("Could not get method ID of create() [com.rcbase.android.sip.audio.AudioDevice]");
		goto leave;
	}

	jbResult = jni_env->CallStaticBooleanMethod( stream->audiodev_class,
									midCreate,
									param->clock_rate,
									param->samples_per_frame,
									param->bits_per_sample,
									param->channel_count,
									(jlong)stream );

	JNI_EXCEPTION_CHECK(true);
	if( IF_JNI_EXCEPTION() || jbResult != JNI_TRUE ){
		ERROR_("create() failed. [com.rcbase.android.sip.audio.AudioDevice]");
		goto leave;
	}


	midTestMode = jni_env->GetStaticMethodID( stream->audiodev_class, "isTestMode", "()Z" );
	JNI_EXCEPTION_CHECK(true);

	if (IF_JNI_EXCEPTION() || midTestMode == 0) {
		ERROR_("Could not get method ID of create() [com.rcbase.android.sip.audio.AudioDevice]");
		goto leave;
	}

	jbTestMode = jni_env->CallStaticBooleanMethod(stream->audiodev_class, midTestMode);

	JNI_EXCEPTION_CHECK(true);
	if (IF_JNI_EXCEPTION()) {
		ERROR_("isTestMode() failed. [com.rcbase.android.sip.audio.AudioDevice]");
		goto leave;
	}

	if(jbTestMode) {
	    TRACE_(" jbTestMode is true! ");
	} else {
	    TRACE_(" jbTestMode is false :( ");
		stream->pSBACP->SetTestMode(SBACP_TEST_OFF);
	}

	// set Internal speaker delay
	midDelayValue = jni_env->GetStaticMethodID( stream->audiodev_class, "getDelayInternalSpeaker", "()I" );
	JNI_EXCEPTION_CHECK(true);

	if (IF_JNI_EXCEPTION() || midDelayValue == 0) {
		ERROR_("Could not get method ID of create() [com.rcbase.android.sip.audio.AudioDevice]");
		goto leave;
	}

	internalSpeakerDelay = jni_env->CallStaticIntMethod(stream->audiodev_class, midTestMode);

	JNI_EXCEPTION_CHECK(true);
	if (IF_JNI_EXCEPTION()) {
		ERROR_("getDelayInternalSpeaker() failed. [com.rcbase.android.sip.audio.AudioDevice]");
		goto leave;
	}

	if(!jbTestMode) {
		stream->pSBACP->SetAudioDelay(internalSpeakerDelay);
	}


	*p_aud_strm = &stream->base;
	(*p_aud_strm)->op = &rc_audio_stream_op;

	DETACH_CURRENT_THREAD();

	return PJ_SUCCESS;
leave:

	rc_audio_stream_java_destroy( (pjmedia_aud_stream*)stream );

	DETACH_CURRENT_THREAD();

 return PJ_EUNKNOWN;
}

/* API: Get the running parameters for the specified audio stream. */
static pj_status_t rc_audio_stream_java_get_param(pjmedia_aud_stream *stream,
						 pjmedia_aud_param *param)
{
	TRACE_("rc_audio_stream_java_get_param started.");

	struct rc_android_audio_stream_java *rc_strm = (struct rc_android_audio_stream_java*)stream;
    PJ_ASSERT_RETURN(stream && param, PJ_EINVAL);

    pj_memcpy(param, &rc_strm->param, sizeof(*param));

    return PJ_SUCCESS;
}

/* API: Get the value of a specific capability of the audio stream. */
static pj_status_t rc_audio_stream_java_get_cap(pjmedia_aud_stream *stream,
					       pjmedia_aud_dev_cap cap,
					       void *value)
{
	TRACE_("rc_audio_stream_java_get_cap started.");

	PJ_UNUSED_ARG(stream);
	PJ_UNUSED_ARG(cap);
	PJ_UNUSED_ARG(value);

	return PJ_ENOTSUP;
}


/* API: Set the value of a specific capability of the audio stream. */
static pj_status_t rc_audio_stream_java_set_cap(pjmedia_aud_stream *stream,
					       pjmedia_aud_dev_cap cap,
					       const void *value)
{
	TRACE_("rc_audio_stream_java_set_cap started.");

	PJ_UNUSED_ARG(stream);
	PJ_UNUSED_ARG(cap);
	PJ_UNUSED_ARG(value);

	return PJMEDIA_EAUD_INVCAP;
}

/* API: Start the stream. */
static pj_status_t rc_audio_stream_java_start(pjmedia_aud_stream *stream)
{
    PJ_ASSERT_RETURN( stream , PJ_EINVAL);
    struct rc_android_audio_stream_java *rc_strm = (struct rc_android_audio_stream_java*)stream;

	ATTACH_CURRENT_THREAD(PJ_EUNKNOWN);
	JNI_EXCEPTION_INIT();

    jmethodID midStart = 0;
    pj_status_t status;
    jboolean jbResult = JNI_FALSE;

    PJ_LOG(3, (THIS_FILE, "rc_audio_stream_java_start started"));

#if defined(PJMEDIA_HAS_SBACP_EC) && PJMEDIA_HAS_SBACP_EC!=0
    if (rc_strm->pSBACP != NULL) {
    	TRACE_("Restart AEC.");
    	rc_strm->pSBACP->Restart();
    }
#endif

	midStart = jni_env->GetStaticMethodID( rc_strm->audiodev_class, "start", "()Z" );
	JNI_EXCEPTION_CHECK(true);

	if( IF_JNI_EXCEPTION() || midStart == 0 ){
		ERROR_("Could not get method ID of start() [com.rcbase.android.sip.audio.AudioDevice]");
		goto leave;
	}

	jbResult = jni_env->CallStaticBooleanMethod( rc_strm->audiodev_class, midStart );

	JNI_EXCEPTION_CHECK(true);
	if( IF_JNI_EXCEPTION() || jbResult != JNI_TRUE ){
		ERROR_("start() failed. [com.rcbase.android.sip.audio.AudioDevice]");
		goto leave;
	}

	DETACH_CURRENT_THREAD();
    return PJ_SUCCESS;

leave:
	rc_audio_stream_java_stop(stream);

	DETACH_CURRENT_THREAD();
	return PJ_EUNKNOWN;
}

/* API: Stop the stream. */
static pj_status_t rc_audio_stream_java_stop(pjmedia_aud_stream *stream)
{
    PJ_ASSERT_RETURN( stream , PJ_EINVAL);
    struct rc_android_audio_stream_java *rc_strm = (struct rc_android_audio_stream_java*)stream;

	ATTACH_CURRENT_THREAD(PJ_EUNKNOWN);
	JNI_EXCEPTION_INIT();

    jmethodID midStop = 0;
    pj_status_t status;
    jboolean jbResult = JNI_FALSE;

    PJ_LOG(3, (THIS_FILE, "rc_audio_stream_java_stop started"));

	midStop = jni_env->GetStaticMethodID( rc_strm->audiodev_class, "stop", "()Z" );
	JNI_EXCEPTION_CHECK(true);

	if( IF_JNI_EXCEPTION() || midStop == 0 ){
		ERROR_("Could not get method ID of stop() [com.rcbase.android.sip.audio.AudioDevice]");
		goto leave;
	}

	jbResult = jni_env->CallStaticBooleanMethod( rc_strm->audiodev_class, midStop );

	JNI_EXCEPTION_CHECK(true);
	if( IF_JNI_EXCEPTION() || jbResult != JNI_TRUE ){
		ERROR_("stop() failed. [com.rcbase.android.sip.audio.AudioDevice]");
		goto leave;
	}

	DETACH_CURRENT_THREAD();
    return PJ_SUCCESS;

leave:

	DETACH_CURRENT_THREAD();
	return PJ_EUNKNOWN;
}

/* API: Destroy the stream. */
static pj_status_t rc_audio_stream_java_destroy(pjmedia_aud_stream *stream)
{
    struct rc_android_audio_stream_java *rc_strm = (struct rc_android_audio_stream_java*)stream;
    PJ_ASSERT_RETURN( stream , PJ_EINVAL);

    PJ_LOG(3, (THIS_FILE,"rc_audio_stream_java_destroy started"));

    jmethodID midDestroy = 0;
    jboolean jbResult = JNI_FALSE;
    pj_status_t status = PJ_SUCCESS;

    rc_audio_stream_java_stop(stream);

	ATTACH_CURRENT_THREAD(PJ_EUNKNOWN);
	JNI_EXCEPTION_INIT();

#if defined(PJMEDIA_HAS_SBACP_EC) && PJMEDIA_HAS_SBACP_EC!=0
    if (rc_strm->pSBACP) {
    	TRACE_("Destroy AEC.");
    	delete rc_strm->pSBACP;
    	rc_strm->pSBACP = NULL;
    }
#endif

    midDestroy = jni_env->GetStaticMethodID( rc_strm->audiodev_class, "destroy", "()Z" );
	JNI_EXCEPTION_CHECK(true);

	if( IF_JNI_EXCEPTION() || midDestroy == 0 ){
		ERROR_("Could not get method ID of destroy() [com.rcbase.android.sip.audio.AudioDevice]");
		status = PJ_EUNKNOWN;
	}

	jbResult = jni_env->CallStaticBooleanMethod( rc_strm->audiodev_class, midDestroy );

	JNI_EXCEPTION_CHECK(true);
	if( IF_JNI_EXCEPTION() || jbResult != JNI_TRUE ){
		ERROR_("destroy() failed. [com.rcbase.android.sip.audio.AudioDevice]");
		status = PJ_EUNKNOWN;
	}

    pj_pool_t *pool = rc_strm->pool;
    rc_strm->pool = NULL;
	pj_pool_release(pool);

	DETACH_CURRENT_THREAD();

	TRACE_("rc_audio_stream_java_destroy finished");
    return status;
}

/*
 * Method:    aecSetTestMode
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aecSetTestMode(JNIEnv *jenv,
		jclass,
		jlong params )
{
	PJ_ASSERT_RETURN(params, PJ_EINVAL);
	struct rc_android_audio_stream_java *rc_strm = (struct rc_android_audio_stream_java *)params;

	if(NULL != rc_strm->pSBACP)
	{
	    PJ_LOG(4, (THIS_FILE, " SET TEST MODE "));

		rc_strm->pSBACP->SetTestMode(SBACP_TEST_PING_DELAY);
	}
	else
	{
	    PJ_LOG(3, (THIS_FILE, " SET TEST MODE FAIL"));
	}

	return PJ_SUCCESS;
}

/*
 * Method:    aecGetCurrentTestMode
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aecGetCurrentTestMode(JNIEnv *jenv,
		jclass,
		jlong params )
{
	PJ_ASSERT_RETURN(params, PJ_EINVAL);
	struct rc_android_audio_stream_java *rc_strm = (struct rc_android_audio_stream_java *)params;

	if(NULL != rc_strm->pSBACP)
	{
		return rc_strm->pSBACP->GetTestMode();
	}
	else
	{
	    PJ_LOG(3, (THIS_FILE, "aecGetCurrentTestMode: error, rc_strm->pSBACP is null"));
		return -666; // magic digit to understand bad situation
	}
}

/*
 * Method:    aecGetMicDb
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aecGetMicDb(JNIEnv *jenv,
		jclass,
		jlong params )
{
	PJ_ASSERT_RETURN(params, PJ_EINVAL);
	struct rc_android_audio_stream_java *rc_strm = (struct rc_android_audio_stream_java *)params;

	if(NULL != rc_strm->pSBACP)
	{
		return rc_strm->pSBACP->GetMicGain_dB(0);
	}
	else
	{
		return -666; // magic digit to understand bad situation
	}
}

/*
 * Method:    aecGetMicDb
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aecGetAudioDelay(JNIEnv *jenv,
		jclass,
		jlong params )
{
	PJ_ASSERT_RETURN(params, PJ_EINVAL);
	struct rc_android_audio_stream_java *rc_strm = (struct rc_android_audio_stream_java *)params;

	if(NULL != rc_strm->pSBACP)
	{
	    TRACE__("current delay is %d ", rc_strm->pSBACP->GetAudioDelay());
		return rc_strm->pSBACP->GetAudioDelay();
	}
	else
	{
		return -666; // magic digit to understand bad situation
	}
}

