APP_PROJECT_PATH := $(call my-dir)/../RingCentral
APP_BUILD_SCRIPT= := $(call my-dir)/build/Android.mk
APP_OPTIM        := release
APP_ABI := armeabi armeabi-v7a
APP_PLATFORM := android-8
APP_STL := stlport_static
#APP_DEBUG := true


RC_PJSIP_USE_G729 := 0
RC_PJSIP_USE_ILBC := 1
RC_PJSIP_USE_G722 := 1
RC_PJSIP_USE_SPEEX := 0
RC_PJSIP_USE_SPEEX_EC := 0
RC_PJSIP_USE_GSM := 1
RC_PJSIP_USE_SILK := 1
RC_PJSIP_USE_TLS := 0
RC_PJSIP_USE_SBACP := 1
RC_PJSIP_USE_TSC :=1

#
# DK: Changed due to our servers do not understand sendonly/recvonly mode
# PJSUA_CALL_HOLD_TYPE_DEFAULT=PJSUA_CALL_HOLD_TYPE_RFC2543

ifeq ($(RC_PJSIP_USE_TSC),1)
RC_PROJECT_PJSIP_LIB_FLAGS := -DPJ_ANDROID=1 \
    -DPJSUA_CALL_HOLD_TYPE_DEFAULT=PJSUA_CALL_HOLD_TYPE_RFC2543 \
	-DPJMEDIA_HAS_G729_CODEC=$(RC_PJSIP_USE_G729) \
	-DPJMEDIA_HAS_ILBC_CODEC=$(RC_PJSIP_USE_ILBC) \
	-DPJMEDIA_HAS_G722_CODEC=$(RC_PJSIP_USE_G722) \
	-DPJMEDIA_HAS_SPEEX_CODEC=$(RC_PJSIP_USE_SPEEX) \
	-DPJMEDIA_HAS_GSM_CODEC=$(RC_PJSIP_USE_GSM) \
	-DPJMEDIA_HAS_SBACP_EC=$(RC_PJSIP_USE_SBACP) \
	-DPJMEDIA_HAS_SILK_CODEC=$(RC_PJSIP_USE_SILK) \
	-DPJMEDIA_HAS_SPEEX_AEC=$(RC_PJSIP_USE_SPEEX_EC) \
	-DPJ_HAS_SSL_SOCK=$(RC_PJSIP_USE_TLS) \
	-DTSC_SUPPORT=$(RC_PJSIP_USE_TSC) \
	-DTSC_LINUX=$(RC_PJSIP_USE_TSC) \
	-DTSC_ANDROID=$(RC_PJSIP_USE_TSC)
else
RC_PROJECT_PJSIP_LIB_FLAGS := -DPJ_ANDROID=1 \
    -DPJSUA_CALL_HOLD_TYPE_DEFAULT=PJSUA_CALL_HOLD_TYPE_RFC2543 \
	-DPJMEDIA_HAS_G729_CODEC=$(RC_PJSIP_USE_G729) \
	-DPJMEDIA_HAS_ILBC_CODEC=$(RC_PJSIP_USE_ILBC) \
	-DPJMEDIA_HAS_G722_CODEC=$(RC_PJSIP_USE_G722) \
	-DPJMEDIA_HAS_SPEEX_CODEC=$(RC_PJSIP_USE_SPEEX) \
	-DPJMEDIA_HAS_GSM_CODEC=$(RC_PJSIP_USE_GSM) \
	-DPJMEDIA_HAS_SBACP_EC=$(RC_PJSIP_USE_SBACP) \
	-DPJMEDIA_HAS_SILK_CODEC=$(RC_PJSIP_USE_SILK) \
	-DPJMEDIA_HAS_SPEEX_AEC=$(RC_PJSIP_USE_SPEEX_EC) \
	-DPJ_HAS_SSL_SOCK=$(RC_PJSIP_USE_TLS) 

endif
#by martin , do we really need so much TSC_XXXX?

