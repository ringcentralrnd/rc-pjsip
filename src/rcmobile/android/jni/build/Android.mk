LOCAL_PATH := $(call my-dir)
TOP_LOCAL_PATH := $(call my-dir)/../

ifeq ($(TARGET_ARCH_ABI),armeabi)
RC_PJSIP_LIB_FLAGS := $(RC_PROJECT_PJSIP_LIB_FLAGS) -DPJ_HAS_FLOATING_POINT=0
else
RC_PJSIP_LIB_FLAGS := $(RC_PROJECT_PJSIP_LIB_FLAGS) -DPJ_HAS_FLOATING_POINT=1
endif


include $(TOP_LOCAL_PATH)/pjlib/build/Android.mk
include $(TOP_LOCAL_PATH)/pjlib-util/build/Android.mk
include $(TOP_LOCAL_PATH)/pjnath/build/Android.mk
include $(TOP_LOCAL_PATH)/pjmedia/build/Android.mk
include $(TOP_LOCAL_PATH)/pjsip/build/Android.mk

include $(TOP_LOCAL_PATH)/third_party/build/resample/Android.mk

include $(TOP_LOCAL_PATH)/third_party/build/rcutils/Android.mk

ifeq ($(RC_PJSIP_USE_TLS),1)
include $(TOP_LOCAL_PATH)/third_party/openssl/Android.mk
endif

include $(TOP_LOCAL_PATH)/third_party/build/srtp/Android.mk

ifeq ($(RC_PJSIP_USE_ILBC),1)
	include $(TOP_LOCAL_PATH)/third_party/build/ilbc/Android.mk
endif
ifeq ($(RC_PJSIP_USE_GSM),1)
	include $(TOP_LOCAL_PATH)/third_party/build/gsm/Android.mk
endif
ifeq ($(RC_PJSIP_USE_SPEEX),1)
	include $(TOP_LOCAL_PATH)/third_party/build/speex/Android.mk
endif
ifeq ($(RC_PJSIP_USE_G729),1)
	include $(TOP_LOCAL_PATH)/third_party/build/g729/Android.mk
endif
ifeq ($(RC_PJSIP_USE_SILK),1)
	include $(TOP_LOCAL_PATH)/third_party/build/silk/Android.mk
endif

ifeq ($(RC_PJSIP_USE_TSC),1)
	#include $(TOP_LOCAL_PATH)/third_party/tsc/lib/android/jni/Android.mk
	include $(TOP_LOCAL_PATH)/third_party/build/tsc/Android.mk
endif


include $(TOP_LOCAL_PATH)/jni/build/Android.mk


