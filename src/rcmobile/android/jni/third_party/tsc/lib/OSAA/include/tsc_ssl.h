/*
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms,
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */
#ifndef __TSC_SSL_H__
#define __TSC_SSL_H__
#ifdef TSC_OPENSSL


#include <tsc_ssl_api.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/ssl.h>
#include <openssl/x509v3.h>

typedef enum
{
    tsc_ssl_init_normal = 0,
    tsc_ssl_init_error
} tsc_ssl_init_status;

int tsc_init_openssl ();

tsc_ssl_init_status tsc_init_ssl_context (tsc_security_config * cfg,
                                          tsc_transport transport_type);
int tsc_ssl_ctx_load_verify_locations_mem(SSL_CTX *ctx, const char *CAcert,
                                      int len, int type);
int tsc_ssl_ctx_use_certificate_mem(SSL_CTX *ctx, const char *buf,int len, int type);
int tsc_ssl_ctx_use_privatekey_mem(SSL_CTX *ctx, const char *buf,int len, int type);
int tsc_ssl_load_crls (SSL_CTX * ctx, char *crlfile);
int tsc_ssl_verify_callback (int ok, X509_STORE_CTX * store);
int tsc_passwd_call_back_routine (char *buf, int size, int flag,
                                  void *passwd);
int tsc_ssl_write (SSL * ssl, const void *buf, int num);
int tsc_ssl_read (SSL * ssl, void *buf, int num);
int tsc_ssl_shutdown (SSL * ssl);
void tsc_ssl_set_shutdown (SSL * ssl);
void tsc_ssl_free (SSL * ssl);
void tsc_ctx_free (tsc_transport transport_type);
int tsc_ssl_accept (SSL * ssl);
int tsc_ssl_connect (SSL * ssl);
SSL *tsc_ssl_new (tsc_transport transport_type);
int tsc_ssl_set_fd (SSL * ssl, int fd);
X509 *tsc_ssl_get_peer_certificate (const SSL * ssl);
const char *tsc_ssl_error_string (SSL * ssl, int size);
const char *tsc_ssl_error_string2 (int err);

#endif
#endif /* __TSC_TUNNEL_SOCKET_H__ */
