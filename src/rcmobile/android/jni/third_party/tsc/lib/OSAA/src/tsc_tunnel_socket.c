/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#define TSC_LOG_SUBSYSTEM TSC_LOG_SUBSYSTEM_OSAA

#include "tsc_tunnel_socket.h"
#include "tsc_log.h"
#include "tsc_data.h"

tsc_tunnel_socket *
tsc_tunnel_socket_create (tsc_tunnel_connection_params conn_param)
{
    tsc_tunnel_socket *handle =
        (tsc_tunnel_socket *) malloc (sizeof (tsc_tunnel_socket));
    tsc_transport transport = conn_param.transport;

    if (handle) {
        memset (handle, 0, sizeof (tsc_tunnel_socket));

        handle->ss_transport = transport;

        if (transport == tsc_transport_udp || transport == tsc_transport_dtls) {
            handle->socket = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

            if (handle->socket) {
                /* Under UDP we need to bind the socket to a local IP address 
                 */
                struct sockaddr_in addr;
                memset (&addr, 0, sizeof (struct sockaddr_in));
                addr.sin_family = AF_INET;

#if defined(TSC_LINUX) && (!defined(TSC_IOS) && !defined(TSC_WINDOWS))
		if(conn_param.choose_device == tsc_bool_true) {
		    
		    char devname[TSC_ADDR_STR_LEN];
		    strncpy(devname, conn_param.devname, strlen(conn_param.devname));
		    if(setsockopt(handle->socket, SOL_SOCKET, SO_BINDTODEVICE, &devname, strlen(devname)) == -1) {
			TSC_ERROR("tsc_tunnel_socket_create: Failed to setsockopt SO_BINDTODEVICE for devname %s. Errno is %d", devname, errno);
			
		    }
		    else {
			TSC_DEBUG("tsc_tunnel_socket_create: Socket bound to device name %s", devname);
		    }
		}
#endif
		addr.sin_addr.s_addr = INADDR_ANY;
		addr.sin_port = 0;
                if (bind
                    (handle->socket, (struct sockaddr *) &addr,
                     sizeof (struct sockaddr)) == -1) {
		    
#ifdef TSC_WINDOWS
                    closesocket (handle->socket);
#else
                    close (handle->socket);
#endif
                    free (handle);
                    handle = NULL;
                    TSC_ERROR
                        ("tsc_tunnel_socket_create: unable to bind UDP socket");
                }
                if (handle->ss_transport == tsc_transport_dtls) {
#ifdef TSC_OPENSSL
                    if ((handle->ss_bio =
                         BIO_new_dgram (handle->socket, BIO_NOCLOSE))
                        == NULL) {
                        TSC_ERROR
                            ("tsc_tunnel_socket_create: unable to create dgram BIO object");
                    }

                    if (!(handle->ss_ssl = tsc_ssl_new (tsc_transport_dtls))) {
                        TSC_ERROR
                            ("tsc_tunnel_socket_create: unable to create SSL object");
                    }

                    SSL_set_bio (handle->ss_ssl, handle->ss_bio,
                                 handle->ss_bio);
                    /* TODO: maybe this number should be not hardcodede
                       SSL_set_mtu(handle->ss_ssl, 1500); */
#endif
                }
            }
#ifdef TSC_FTCP
        }
        else if (handle->ss_transport == tsc_transport_ftcp) {
            /* nothing for now */
#endif
        }
        else if (handle->ss_transport == tsc_transport_tcp
                 || handle->ss_transport == tsc_transport_tls) {
            handle->socket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
	    if(conn_param.choose_device == tsc_bool_true) {
#if defined(TSC_LINUX) && (!defined(TSC_IOS) && !defined(TSC_WINDOWS))
		char devname[TSC_ADDR_STR_LEN];
		strncpy(devname, conn_param.devname, strlen(conn_param.devname));
		if(setsockopt(handle->socket, SOL_SOCKET, SO_BINDTODEVICE, &devname, strlen(devname)) == -1) {
		    TSC_ERROR("tsc_tunnel_socket_create: Failed to setsockopt SO_BINDTODEVICE for devname %s. Errno is %d", devname, errno);
		}
		else {
		    TSC_DEBUG("tsc_tunnel_socket_create: Socket bound to device name %s", devname);
		}
#else
		TSC_ERROR("tsc_tunnel_socket_create: Binding to a specific device is not support for this platform");
#endif
	    }
            if (handle->ss_transport == tsc_transport_tls) {
#ifdef TSC_OPENSSL
                if ((handle->ss_bio =
                     BIO_new_socket (handle->socket, BIO_NOCLOSE))
                    == NULL) {
                    TSC_ERROR
                        ("tsc_tunnel_socket_create: unable to create BIO object");
                }

                if (!(handle->ss_ssl = tsc_ssl_new (tsc_transport_tls))) {
                    TSC_ERROR
                        ("tsc_tunnel_socket_create: unable to create SSL object");
                }

                SSL_set_bio (handle->ss_ssl, handle->ss_bio, handle->ss_bio);
#endif
            }
        }
    }

    return handle;
}

#ifdef TSC_FTCP
tsc_bool
tsc_raw_filter (tsc_buffer * buffer)
{
    tsc_ip_address src_addr;
    tsc_ip_address dst_addr;
    uint8_t protocol;

    if (tsc_ip_parse
        (&src_addr, &dst_addr, &protocol,
         buffer->data + TSC_ETHER_HEADER_SIZE) == tsc_error_code_ok) {
        if (protocol == SOL_TCP) {
            uint32_t tcp_src_port;
            uint32_t tcp_dst_port;

            if (tsc_tcp_parse
                (&tcp_src_port, &tcp_dst_port,
                 buffer->data + TSC_ETHER_HEADER_SIZE + TSC_IP_HEADER_SIZE) ==
                tsc_error_code_ok) {
                uint32_t udp_src_port;
                uint32_t udp_dst_port;

                if (tsc_udp_parse
                    (&udp_src_port, &udp_dst_port,
                     buffer->data + TSC_ETHER_HEADER_SIZE +
                     TSC_IP_HEADER_SIZE + TSC_TCP_HEADER_SIZE)
                    == tsc_error_code_ok) {
                    return tsc_bool_true;
                    if (udp_src_port == tcp_src_port
                        && udp_dst_port == tcp_dst_port) {
                        return tsc_bool_true;
                    }
                }
            }
        }
    }

    return tsc_bool_false;
}
#endif

tsc_tunnel_socket_response
tsc_tunnel_socket_connect (tsc_tunnel_socket * handle,
                           tsc_ip_port_address * dest)
{
    if (handle) {
        memcpy (&(handle->dest), dest, sizeof (tsc_ip_port_address));
        if (handle->ss_transport == tsc_transport_udp) {
            handle->error = EINPROGRESS;
#ifdef TSC_FTCP
        }
        else if (handle->ss_transport == tsc_transport_ftcp) {
            /* let's find the device */
            char device[TSC_ADDR_STR_LEN];

            tsc_ip_address addr;
            addr.address = dest->address;

            if (tsc_find_route (&addr, device) == tsc_error_code_ok) {
                handle->raw_if = tsc_raw_open (device, tsc_raw_filter);

                if (handle->raw_if) {
                    handle->error = EWOULDBLOCK;

                    tsc_ip_address local;
                    if (tsc_ip_get_addr (device, &local) == tsc_error_code_ok) {
                        handle->src.address = local.address;
                        handle->src.port = tsc_time() & 0xffff;

                        return tsc_tunnel_socket_response_ok;
                    }
                }
            }

            return tsc_tunnel_socket_response_error;
#endif
        }
        else if (handle->ss_transport == tsc_transport_tcp
                 || handle->ss_transport == tsc_transport_tls
                 || handle->ss_transport == tsc_transport_dtls) {
            struct sockaddr_in addr;
            memset (&addr, 0, sizeof (struct sockaddr_in));
            addr.sin_family = AF_INET;
            addr.sin_port = htons (dest->port);
            addr.sin_addr.s_addr = htonl (dest->address);

            handle->result =
                connect (handle->socket, (struct sockaddr *) (&addr),
                         sizeof (struct sockaddr_in));
            if (handle->ss_transport != tsc_transport_dtls) {
#ifdef TSC_LINUX
                handle->error = errno;
#elif TSC_WINDOWS
                int error = WSAGetLastError ();

                handle->error = error;

                if (error == WSAEWOULDBLOCK) {
                    handle->error = EWOULDBLOCK;
                }
                else if (error == WSAEINPROGRESS) {
                    handle->error = EINPROGRESS;
                }
#endif
            }
            else {
                if (handle->result == -1) {
                    return tsc_tunnel_socket_response_error;
                }

#ifdef TSC_OPENSSL
                (void) BIO_ctrl_set_connected (handle->ss_bio, 1, &addr);
#endif

                handle->error = EINPROGRESS;
            }
        }

        return tsc_tunnel_socket_response_ok;
    }

    return tsc_tunnel_socket_response_error;
}

tsc_tunnel_socket_response
tsc_tunnel_socket_ioctl (tsc_tunnel_socket * handle, long cmd,
                         unsigned long *argp)
{
    if (handle) {
#ifdef TSC_FTCP
        if (handle->ss_transport == tsc_transport_ftcp) {
            return tsc_tunnel_socket_response_ok;
        }
#endif

#ifdef TSC_LINUX
        handle->result = ioctl (handle->socket, cmd, argp);
#elif TSC_WINDOWS
        handle->result = ioctlsocket (handle->socket, cmd, argp);
#endif

        handle->error = errno;

        return tsc_tunnel_socket_response_ok;
    }

    return tsc_tunnel_socket_response_error;
}

tsc_tunnel_socket_response
tsc_tunnel_socket_select (tsc_tunnel_socket * handle, fd_set * readfds,
                          fd_set * writefds, fd_set * exceptfds,
                          struct timeval * timeout)
{
    if (handle) {
        handle->result =
            select (handle->socket + 1, readfds, writefds, exceptfds,
                    timeout);
        handle->error = errno;
        return tsc_tunnel_socket_response_ok;
    }

    return tsc_tunnel_socket_response_error;
}

tsc_bool
tsc_tunnel_socket_ready_to_write (tsc_tunnel_socket * handle)
{
    if (handle) {
#ifdef TSC_FTCP
        if (handle->ss_transport == tsc_transport_ftcp) {
            return tsc_bool_true;
        }
#endif

        struct timeval timeout;
        fd_set write_flags;
        timeout.tv_sec = 0;
        timeout.tv_usec = 0;

        FD_ZERO (&write_flags);
        FD_SET (handle->socket, &write_flags);

        if (tsc_tunnel_socket_select
            (handle, NULL, &write_flags, NULL,
             &timeout) == tsc_tunnel_socket_response_ok) {
            if (handle->result > 0) {
                return tsc_bool_true;
            }
        }
    }

    return tsc_bool_false;
}

tsc_bool
tsc_tunnel_socket_ready_to_read (tsc_tunnel_socket * handle)
{
    if (handle) {
#ifdef TSC_FTCP
        if (handle->ss_transport == tsc_transport_ftcp) {
            if (tsc_raw_ready_to_read (handle->raw_if) == tsc_raw_response_ok) {
                return tsc_bool_true;
            }

            return tsc_bool_false;
        }
#endif

        struct timeval timeout;
        fd_set read_flags;

        timeout.tv_sec = 0;
        timeout.tv_usec = 1;

        FD_ZERO (&read_flags);
        FD_SET (handle->socket, &read_flags);

        if (tsc_tunnel_socket_select
            (handle, &read_flags, NULL, NULL,
             &timeout) == tsc_tunnel_socket_response_ok) {
            if (handle->result > 0) {
                return tsc_bool_true;
            }
        }
    }

    return tsc_bool_false;
}

tsc_tunnel_socket_response
tsc_tunnel_socket_set_non_blocking (tsc_tunnel_socket * handle)
{
    if (handle) {
#ifdef TSC_FTCP
        if (handle->ss_transport == tsc_transport_ftcp) {
            return tsc_tunnel_socket_response_ok;
        }
#endif

        unsigned long flags = 1;

        return tsc_tunnel_socket_ioctl (handle, FIONBIO, &flags);
    }

    return tsc_tunnel_socket_response_error;
}

tsc_bool
tsc_tunnel_socket_connected (tsc_tunnel_socket * handle)
{
    if (handle) {
        if (handle->ss_transport == tsc_transport_udp ||
#ifdef TSC_FTCP
            handle->ss_transport == tsc_transport_ftcp ||
#endif
            handle->ss_transport == tsc_transport_dtls) {
            /* if UDP we assume we are connected right away */
            return tsc_bool_true;
        }

        if (tsc_tunnel_socket_ready_to_write (handle) == tsc_bool_true) {
            socklen_t len = sizeof (int);

            int opt_value = 0;

            if (!getsockopt
                (handle->socket, SOL_SOCKET, SO_ERROR, (char *) &opt_value,
                 &len)) {
                if (!opt_value) {
                    /* we are really connected! */
                    return tsc_bool_true;
                }
            }
        }
    }

    return tsc_bool_false;
}

tsc_tunnel_socket_response
tsc_tunnel_socket_send (tsc_tunnel_socket * handle, void *buf, size_t len,
                        tsc_bool force_tcp, size_t min_size)
{
    if (handle) {
        if (handle->ss_transport == tsc_transport_udp) {
            struct sockaddr_in addr;
            memset (&addr, 0, sizeof (struct sockaddr_in));
            addr.sin_family = AF_INET;
            addr.sin_port = htons (handle->dest.port);
            addr.sin_addr.s_addr = htonl (handle->dest.address);
            handle->result =
                sendto (handle->socket, (char *) buf, len, 0,
                        (struct sockaddr *) &addr, sizeof (struct sockaddr));
#ifdef TSC_LINUX
	    handle->error = errno;
#elif TSC_WINDOWS
	    int error = WSAGetLastError ();
	    
	    handle->error = error;
	    
	    if (error == WSAEWOULDBLOCK) {
		handle->error = EWOULDBLOCK;
	    }
	    else if (error == WSAEINPROGRESS) {
		handle->error = EINPROGRESS;
	    }
	    else if (error == WSAEFAULT) {
		handle->error = EFAULT;
	    }
#endif
	}
        else if (handle->ss_transport == tsc_transport_tcp
                 || force_tcp == tsc_bool_true) {
            size_t sent = 0;

            if (handle->buffer_len + len < TSC_MAX_FRAME_SIZE) {
                memcpy(handle->buffer + handle->buffer_len, buf, len);
                handle->buffer_len += len;

                if (handle->buffer_len > min_size) {
                    void *buf = handle->buffer;
                    size_t len = handle->buffer_len;
                    uint32_t sleepcounter = 0;

                    while (sent < len) {
                        ssize_t tmp = send (handle->socket, (char *) buf + sent, len - sent, 0);

                        if (tmp > 0) {
                            sent += tmp;
                        } else if (sleepcounter++ < TSC_SEND_SLEEP_COUNT) {
                            tsc_sleep (1);
                        } else {
                            break;
                        }
                    }

                    handle->buffer_len -= sent;
                    memmove(handle->buffer, handle->buffer + sent, handle->buffer_len);

                    if (!sent) {
                        TSC_ERROR("tsc_tunnel_socket_send: cannot send data dumping packet [%p]\n", handle);

                        /*handle->buffer_len = 0;    */
                    }
                }
            }

            handle->result = sent;
#ifdef TSC_LINUX
	    handle->error = errno;
#elif TSC_WINDOWS
	    int error = WSAGetLastError ();
	    
	    handle->error = error;
	    
	    if (error == WSAEWOULDBLOCK) {
		handle->error = EWOULDBLOCK;
	    }
	    else if (error == WSAEINPROGRESS) {
		handle->error = EINPROGRESS;
	    }
	    else if (error == WSAEFAULT) {
		handle->error = EFAULT;
	    }
#endif 
	}
#ifdef TSC_FTCP
        else if (handle->ss_transport == tsc_transport_ftcp) {
            tsc_buffer msg;
            msg.len = len;
            memcpy (msg.data, buf, msg.len);

            if (tsc_raw_ftcp_send
                (handle->raw_if, &(handle->src), &(handle->dest),
                 &msg) == tsc_raw_response_ok) {
                handle->result = msg.len;

                return tsc_tunnel_socket_response_ok;
            }

            return tsc_tunnel_socket_response_error;
	}
#endif
       
        else if (handle->ss_transport == tsc_transport_tls
                 || handle->ss_transport == tsc_transport_dtls) {
#ifdef TSC_OPENSSL
            int err_ret = 0;
            if (handle->ss_ssl == NULL) {
                return tsc_tunnel_socket_response_error;
            }
            while (1) {
                handle->result = tsc_ssl_write (handle->ss_ssl, buf, len);
                if (handle->result <= 0) {
                    err_ret = SSL_get_error (handle->ss_ssl, handle->result);
                    TSC_DEBUG ("tsc_ssl_write: %s",
                               tsc_ssl_error_string2 (err_ret));
                    if (err_ret == SSL_ERROR_WANT_READ
                        || err_ret == SSL_ERROR_WANT_WRITE)
                        continue;
                    return tsc_tunnel_socket_response_error;
                }
                break;
            
#ifdef TSC_LINUX
		handle->error = errno;
#elif TSC_WINDOWS
		int error = WSAGetLastError ();
		
		handle->error = error;
		
		if (error == WSAEWOULDBLOCK) {
		    handle->error = EWOULDBLOCK;
		}
		else if (error == WSAEINPROGRESS) {
		    handle->error = EINPROGRESS;
		}
		else if (error == WSAEFAULT) {
		    handle->error = EFAULT;
		}
#endif
	    }
#endif
	}
	
	return tsc_tunnel_socket_response_ok;
	}
    return tsc_tunnel_socket_response_error;
}

ssize_t
tsc_tunnel_socket_recv (tsc_tunnel_socket * handle, void *buf, size_t len,
                        tsc_bool force_tcp)
{
    if (handle) {
        ssize_t size = 0;
        if (handle->ss_transport == tsc_transport_udp) {
            struct sockaddr_in addr;
            socklen_t from_len;
            memset (&addr, 0, sizeof (struct sockaddr_in));
            from_len = sizeof (struct sockaddr);
            size =
                recvfrom (handle->socket, (char *) buf, len, 0,
                          (struct sockaddr *) &addr, &from_len);
#ifdef TSC_FTCP
        }
        else if (handle->ss_transport == tsc_transport_ftcp) {
            tsc_buffer msg;

            tsc_ip_port_address src;
            tsc_ip_port_address dst;

            if (tsc_raw_ftcp_recv (handle->raw_if, &src, &dst, &msg) ==
                tsc_raw_response_ok) {
                memcpy (buf, msg.data, msg.len);

                return msg.len;
            }

            handle->error = EWOULDBLOCK;

            return -1;
#endif
        }
        else if (handle->ss_transport == tsc_transport_tcp
                 || force_tcp == tsc_bool_true) {
            size = recv (handle->socket, (char *) buf, len, 0);
        }
        else if (handle->ss_transport == tsc_transport_tls
                 || handle->ss_transport == tsc_transport_dtls) {
#ifdef TSC_OPENSSL
            int err_ret;
            ssize_t rlen = 0;
            if(handle->ss_ssl == NULL) {
                return 0;
            }
            do {
                size =
                    tsc_ssl_read (handle->ss_ssl, (uint8_t *) buf + rlen,
                                  len - rlen);
                err_ret = SSL_get_error (handle->ss_ssl, size);
                TSC_DEBUG ("tsc_ssl_read: %s",
                           tsc_ssl_error_string2 (err_ret));
                switch (err_ret) {
                case SSL_ERROR_NONE:
                    rlen += size;
                    break;
                }
                if (err_ret == SSL_ERROR_SSL || err_ret == SSL_ERROR_SYSCALL
                    || err_ret == SSL_ERROR_ZERO_RETURN) {
                    TSC_DEBUG ("ssl closed in error.");
                    rlen = -1;
                    break;
                }
            } while (SSL_pending (handle->ss_ssl));
            size = rlen;
#endif
        }
#ifdef TSC_LINUX
        handle->error = errno;
#elif TSC_WINDOWS
        int error = WSAGetLastError ();

        handle->error = error;

        if (error == WSAEWOULDBLOCK) {
            handle->error = EWOULDBLOCK;
        }
        else if (error == WSAEINPROGRESS) {
            handle->error = EINPROGRESS;
        }
        else if (error == WSAEFAULT) {
            handle->error = EFAULT;
        }
#endif
        return size;
    }

    return 0;
}

tsc_tunnel_socket_response
tsc_tunnel_socket_close (tsc_tunnel_socket * handle)
{
    if (handle) {
#ifdef TSC_FTCP
        if (handle->ss_transport == tsc_transport_ftcp) {
            if (handle->raw_if) {
                tsc_raw_close (handle->raw_if);

                handle->raw_if = NULL;
            }

            handle->result = 0;

            return tsc_tunnel_socket_response_ok;
        }
#endif

#ifdef TSC_OPENSSL
        SSL *ssl = handle->ss_ssl;
        if (ssl) {
            tsc_ssl_set_shutdown (ssl);
            tsc_ssl_free (ssl);
            ssl = NULL;
        }

#endif
#ifdef TSC_LINUX
        handle->result = shutdown (handle->socket, SHUT_RDWR);
#elif TSC_WINDOWS
        handle->result = shutdown (handle->socket, SD_BOTH);
#endif
        handle->error = errno;

        return tsc_tunnel_socket_response_ok;
    }

    return tsc_tunnel_socket_response_error;
}

tsc_tunnel_socket_response
tsc_tunnel_socket_delete (tsc_tunnel_socket * handle)
{
#ifdef TSC_FTCP
    if (handle->raw_if) {
        tsc_raw_close (handle->raw_if);

        handle->raw_if = NULL;
    }
#endif

    if (handle) {
        free (handle);
        handle = NULL;

        return tsc_tunnel_socket_response_ok;
    }

    return tsc_tunnel_socket_response_error;
}

tsc_tunnel_socket_response
tsc_tunnel_socket_set_no_delay (tsc_tunnel_socket * handle, tsc_bool no_delay)
{
    if (handle) {
        int flags = 0;

        if (handle->ss_transport == tsc_transport_udp ||
#ifdef TSC_FTCP
            handle->ss_transport == tsc_transport_ftcp ||
#endif
            handle->ss_transport == tsc_transport_dtls) {
            /* UDP is always no delay */
            return tsc_tunnel_socket_response_ok;
        }

        if (no_delay == tsc_bool_true) {
            flags = 1;
        }

        handle->result =
            setsockopt (handle->socket, IPPROTO_TCP, TCP_NODELAY,
                        (char *) &flags, sizeof (int));
        handle->error = errno;

        if (handle->result >= 0) {
            return tsc_tunnel_socket_response_ok;
        }
    }

    return tsc_tunnel_socket_response_error;
}

tsc_tunnel_socket_response
tsc_tunnel_socket_set_tos (tsc_tunnel_socket * handle, uint8_t tos)
{
    if (handle) {
        unsigned int value = tos;

        handle->result =
            setsockopt (handle->socket, IPPROTO_IP, IP_TOS, (char *) &value,
                        sizeof (unsigned int));
        handle->error = errno;

        if (handle->result >= 0) {
            return tsc_tunnel_socket_response_ok;
        }
    }

    return tsc_tunnel_socket_response_error;
}


tsc_error_code 
tsc_get_tunnel_socket_info (tsc_handle handle, tsc_tunnel_socket_info * tunnel_info)
{
    if (handle) {
	tsc_csm_info *info = (tsc_csm_info *) handle;

	struct sockaddr_in tunnel_name;
	socklen_t addrlen = sizeof(struct sockaddr_in);
	memset(&tunnel_name, 0, sizeof(struct sockaddr_in));
	if (getsockname(info->tunnel_socket->socket,(struct sockaddr *) &tunnel_name, &addrlen) > 0) {

	    return tsc_error_code_error;
	}
	tunnel_info->local_address.address = ntohl(tunnel_name.sin_addr.s_addr);
	tunnel_info->local_address.port = ntohs(tunnel_name.sin_port);

	memcpy(&(tunnel_info->remote_address), &(info->tunnel_params.connection_params[info->connection_index].server_address), sizeof(tsc_ip_port_address));

	memcpy(&(tunnel_info->nat_ipport), &(info->tunnel_params.connection_params[info->connection_index].nat_ipport), sizeof(tsc_ip_port_address));

	tunnel_info->transport =  info->tunnel_params.connection_params[info->connection_index].transport;
	tunnel_info->connection_index = info->connection_index;

	char local_addr[TSC_ADDR_STR_LEN];
	char remote_addr[TSC_ADDR_STR_LEN];
	char nat_ipport[TSC_ADDR_STR_LEN];
	if (!tsc_ip_port_address_to_str(&(tunnel_info->local_address), local_addr, TSC_ADDR_STR_LEN)) {
	    TSC_DEBUG ("%s: failed to convert tunnel local address\n", __FUNCTION__);
	}
	if (!tsc_ip_port_address_to_str(&(tunnel_info->remote_address), remote_addr, TSC_ADDR_STR_LEN)) {
	    TSC_DEBUG ("%s: failed to convert tunnel remote address\n", __FUNCTION__);
	}
	if (!tsc_ip_port_address_to_str (&(tunnel_info->nat_ipport), nat_ipport, TSC_ADDR_STR_LEN)) {
	    TSC_DEBUG ("%s: failed to convert tunnel NAT address\n", __FUNCTION__);
	}
	TSC_DEBUG("%s: local address %s, remote address %s, nat address %s\n",
		  __FUNCTION__, local_addr, remote_addr, nat_ipport);
	return tsc_error_code_ok;
	    
    }
    return tsc_error_code_error;
    
}
