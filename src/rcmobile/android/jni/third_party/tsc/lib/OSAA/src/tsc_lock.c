/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#define TSC_LOG_SUBSYSTEM TSC_LOG_SUBSYSTEM_OSAA

#include "tsc_lock.h"
#include "tsc_log.h"

tsc_lock *
tsc_lock_new ()
{
    tsc_lock *lock = (tsc_lock *) malloc (sizeof (tsc_lock));

    if (lock) {
        memset (lock, 0, sizeof (tsc_lock));
        lock->taken = tsc_bool_false;

#ifdef TSC_LINUX
        pthread_mutexattr_t mutexattr;

        pthread_mutexattr_init(&mutexattr);
        pthread_mutexattr_settype(&mutexattr, PTHREAD_MUTEX_RECURSIVE);

        pthread_mutex_init (&(lock->mutex), &mutexattr);
#elif TSC_WINDOWS
        char tmp[TSC_MAX_STR_LEN];
        sprintf (tmp, "tsc_lock_%d_%d", (int) tsc_time (), tsc_get_clock ());
        lock->mutex = CreateMutex (NULL, FALSE, tmp);
#endif
    }

    return lock;
}

tsc_lock_response
tsc_lock_get (tsc_lock * lock)
{
    if (lock) {
#ifdef TSC_LINUX
        int result = pthread_mutex_lock (&(lock->mutex));

        if (result == EOK || result == EDEADLK) {
            lock->taken = tsc_bool_true;
            lock->taker_thread = pthread_self ();

            return tsc_lock_response_ok;
        } else {
            TSC_ERROR("tsc_lock_get: failed to get lock [%d][%p]",
                result, lock);
        }
#elif TSC_WINDOWS
        DWORD result = WaitForSingleObject (lock->mutex, TSC_LOCK_TIMEOUT);

        if (result == WAIT_OBJECT_0 || result == WAIT_ABANDONED) {
            lock->taken = tsc_bool_true;
            lock->taker_thread = GetCurrentThreadId ();

            return tsc_lock_response_ok;
        } else {
            TSC_ERROR("tsc_lock_get: failed to get lock [%d][%d][%p]",
                result, GetLastError(), lock);
        }
#endif
    }

    return tsc_lock_response_error;
}

tsc_lock_response
tsc_lock_release (tsc_lock * lock)
{
    if (lock) {
        lock->taken = tsc_bool_false;

#ifdef TSC_LINUX
        pthread_mutex_unlock (&(lock->mutex));
#elif TSC_WINDOWS
        if (!ReleaseMutex (lock->mutex)) {
            TSC_ERROR("tsc_lock_release: failed to release lock [%d][%p]",
                GetLastError(), lock);
        }
#endif
    }

    return tsc_lock_response_error;
}

tsc_lock_response
tsc_lock_delete (tsc_lock * lock)
{
    if (lock) {
#ifdef TSC_LINUX
        pthread_mutex_destroy (&(lock->mutex));
#elif TSC_WINDOWS
        CloseHandle (lock->mutex);
#endif

        free ((void *) lock);

        return tsc_lock_response_ok;
    }

    return tsc_lock_response_error;
}

void *
tsc_lock_get_taker_thread (tsc_lock *lock)
{
    if (lock && (lock->taken == tsc_bool_true)) {
        return (void *)(lock->taker_thread);
    }

    return NULL;
}
