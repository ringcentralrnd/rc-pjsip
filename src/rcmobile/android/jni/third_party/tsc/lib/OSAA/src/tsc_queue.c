/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#include "tsc_queue.h"

tsc_queue *
tsc_queue_new (uint32_t total, size_t entry_size)
{
    tsc_queue *queue = (tsc_queue *) malloc (sizeof (tsc_queue));

    if (queue) {
        memset (queue, 0, sizeof (tsc_queue));

        queue->total = total;
        queue->entry_size = entry_size;
        queue->buffer = (void *) malloc (entry_size * total);
    }

    return queue;
}

tsc_queue_response
tsc_queue_delete (tsc_queue * queue)
{
    if (queue) {
        free ((void *) queue->buffer);
        free ((void *) queue);

        return tsc_queue_response_ok;
    }

    return tsc_queue_response_error;
}

tsc_queue_response
tsc_queue_write (tsc_queue * queue, void *data)
{
    if (queue) {
        if (queue->gap < queue->total) {
            memcpy ((uint8_t *) queue->buffer +
                    queue->write_ptr * queue->entry_size, data,
                    queue->entry_size);
            queue->write_ptr++;
            queue->gap++;

            if (queue->write_ptr == queue->total) {
                queue->write_ptr = 0;
            }

            return tsc_queue_response_ok;
        }
        else {
            return tsc_queue_response_overflow;
        }
    }

    return tsc_queue_response_error;
}

tsc_queue_response
tsc_queue_read (tsc_queue * queue, void *data)
{
    if (queue) {
        if (queue->gap > 0) {
            memcpy (data,
                    (uint8_t *) queue->buffer +
                    queue->read_ptr * queue->entry_size, queue->entry_size);
            queue->read_ptr++;
            queue->gap--;

            if (queue->read_ptr == queue->total) {
                queue->read_ptr = 0;
            }

            return tsc_queue_response_ok;
        }
        else {
            return tsc_queue_response_underrun;
        }
    }

    return tsc_queue_response_error;
}
