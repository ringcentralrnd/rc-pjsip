/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#ifndef __TSC_TUNNEL_SOCKET_H__
#define __TSC_TUNNEL_SOCKET_H__

#include "tsc_common.h"
#include "tsc_utils.h"
#ifdef TSC_OPENSSL
#include "tsc_ssl.h"
#endif

#define TSC_SEND_SLEEP_COUNT 20

#ifdef TSC_FTCP
#include "tsc_raw.h"
#endif

typedef enum
{
    tsc_tunnel_socket_response_ok = 0,
    tsc_tunnel_socket_response_error
} tsc_tunnel_socket_response;

typedef struct
{
    int socket;
    int result;
    int error;
    tsc_transport ss_transport;
    tsc_ip_port_address dest;
#ifdef TSC_OPENSSL
    SSL *ss_ssl;
    BIO *ss_bio;
#endif
#ifdef TSC_FTCP
    tsc_raw *raw_if;
    tsc_ip_port_address src;
#endif
    uint8_t buffer[TSC_MAX_FRAME_SIZE];
    size_t buffer_len;
} tsc_tunnel_socket;

typedef struct
{
    tsc_transport transport;
    tsc_bool choose_device;
    char devname[TSC_ADDR_STR_LEN];
} tsc_tunnel_connection_params;

    

tsc_tunnel_socket *tsc_tunnel_socket_create (tsc_tunnel_connection_params conn_param);

tsc_tunnel_socket_response tsc_tunnel_socket_connect (tsc_tunnel_socket *
                                                      handle,
                                                      tsc_ip_port_address *
                                                      addr);
tsc_tunnel_socket_response tsc_tunnel_socket_ioctl (tsc_tunnel_socket *
                                                    handle, long cmd,
                                                    unsigned long *argp);
tsc_tunnel_socket_response tsc_tunnel_socket_select (tsc_tunnel_socket *
                                                     handle, fd_set * readfds,
                                                     fd_set * writefds,
                                                     fd_set * exceptfds,
                                                     struct timeval *timeout);

tsc_tunnel_socket_response tsc_tunnel_socket_close (tsc_tunnel_socket *
                                                    handle);

tsc_tunnel_socket_response tsc_tunnel_socket_delete (tsc_tunnel_socket *
                                                     handle);
tsc_tunnel_socket_response
tsc_tunnel_socket_set_non_blocking (tsc_tunnel_socket * handle);
tsc_bool tsc_tunnel_socket_ready_to_write (tsc_tunnel_socket * handle);
tsc_bool tsc_tunnel_socket_ready_to_read (tsc_tunnel_socket * handle);
tsc_bool tsc_tunnel_socket_connected (tsc_tunnel_socket * handle);

tsc_tunnel_socket_response tsc_tunnel_socket_set_tos (tsc_tunnel_socket *
                                                      handle, uint8_t tos);

tsc_tunnel_socket_response tsc_tunnel_socket_set_no_delay (tsc_tunnel_socket *
                                                           handle,
                                                           tsc_bool no_delay);

tsc_tunnel_socket_response tsc_tunnel_socket_send (tsc_tunnel_socket * handle,
                                                   void *buf, size_t len,
                                                   tsc_bool force_tcp, size_t min_size);

ssize_t tsc_tunnel_socket_recv (tsc_tunnel_socket * handle, void *buf,
                                size_t len, tsc_bool force_tcp);

#endif /* __TSC_TUNNEL_SOCKET_H__ */
