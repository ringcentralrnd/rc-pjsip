/*
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms,
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#define TSC_LOG_SUBSYSTEM TSC_LOG_SUBSYSTEM_OSAA

#include "tsc_ssl.h"
#include "tsc_log.h"

#ifdef TSC_OPENSSL
#define TSC_CALL_BACK_USER_DATA "mwang"

static SSL_CTX *tsc_tls_ctx;
static SSL_CTX *tsc_dtls_ctx;

#ifdef TSC_ANDROID
char tsc_ssl_cert_name[TSC_CERT_FILE_LEN];
char tsc_ssl_key_name[TSC_KEY_FILE_LEN];
char tsc_ssl_ca_name[TSC_CA_FILE_LEN];
char tsc_ssl_crl_name[] = "";
#else
char tsc_ssl_cert_name[] = "tsccert.pem";
char tsc_ssl_key_name[] = "tsckey.pem";
char tsc_ssl_ca_name[] = "tscca.pem";
char tsc_ssl_crl_name[] = "";
#endif

const char cipherlist[] = 
    "DHE-RSA-AES256-SHA:"
    "DHE-RSA-AES128-SHA:"
    "AES256-SHA:"
    "AES128-SHA:"
    "EDH-RSA-DES-CBC3-SHA:"
    "DES-CBC3-SHA:"
    "EDH-RSA-DES-CBC-SHA:"
    "DES-CBC-SHA:"
    "EXP1024-DES-CBC-SHA:"
    "NULL-SHA:"
    "NULL-MD5";

static const char test_ca[] =
    "-----BEGIN CERTIFICATE-----\n"
    "MIIEdzCCA1+gAwIBAgIJAPdo10KJnV/bMA0GCSqGSIb3DQEBBQUAMIGDMQswCQYD\n"
    "VQQGEwJVUzELMAkGA1UECBMCTUExEzARBgNVBAcTCkJ1cmxpbmd0b24xFDASBgNV\n"
    "BAoTC0VuZ2luZWVyaW5nMRcwFQYDVQQDEw5BY21lIFBhY2tldCBNQTEjMCEGCSqG\n"
    "SIb3DQEJARYUbXdhbmdAYWNtZXBhY2tldC5jb20wHhcNMTEwMzAzMjE1MzI0WhcN\n"
    "MTYwMzAyMjE1MzI0WjCBgzELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAk1BMRMwEQYD\n"
    "VQQHEwpCdXJsaW5ndG9uMRQwEgYDVQQKEwtFbmdpbmVlcmluZzEXMBUGA1UEAxMO\n"
    "QWNtZSBQYWNrZXQgTUExIzAhBgkqhkiG9w0BCQEWFG13YW5nQGFjbWVwYWNrZXQu\n"
    "Y29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq9ocZ7o3qU6PHqaJ\n"
    "eL96hQBdgLcLxD+VHXB/IPB9rvHSuaWHAnZBMS1NjvFxHo/19ePxOVBQB8BCK+ZO\n"
    "e+dkkT8OC2J7V9NfMWgVSyo5VwkhDCkPcUVKxj3F/BJi6/J6kP6MYidV4fcmcQNe\n"
    "pPd7xdh/qwSnuryM1WJyJJukdlnLcgSJ0Xh8wUWCyDrmwLM1BSQmewEXTlsgcriV\n"
    "mfK3wnyndWgeRGE0sg2dCrtJmxc9lfgaSupaUPSLomqFkO9rc+58bFA1FfXQRVsN\n"
    "RBU+AjXRrcADWFijFrAPW1WlypU1z9FNrMBBUBPvBXDc3Uym310WgOKrQ4z4OOuB\n"
    "HSuBoQIDAQABo4HrMIHoMB0GA1UdDgQWBBTYLWf5T/jd08bQs03rHtfOv6NeADCB\n"
    "uAYDVR0jBIGwMIGtgBTYLWf5T/jd08bQs03rHtfOv6NeAKGBiaSBhjCBgzELMAkG\n"
    "A1UEBhMCVVMxCzAJBgNVBAgTAk1BMRMwEQYDVQQHEwpCdXJsaW5ndG9uMRQwEgYD\n"
    "VQQKEwtFbmdpbmVlcmluZzEXMBUGA1UEAxMOQWNtZSBQYWNrZXQgTUExIzAhBgkq\n"
    "hkiG9w0BCQEWFG13YW5nQGFjbWVwYWNrZXQuY29tggkA92jXQomdX9swDAYDVR0T\n"
    "BAUwAwEB/zANBgkqhkiG9w0BAQUFAAOCAQEAFgKkb6zzeeOT9ixBGqk2Bz20evFP\n"
    "u6ovHoauXxnXyk553OObonHxuJOgl52SpPdlfLAIkW8NyK5t0/THJchd1VjoMo31\n"
    "0dANV0iUGGeE29X+YJx5rrwm3RqOdAmJdhopstXRjRGwNqADueHBK9BD6jGNqYk2\n"
    "L3pDcz9wTBS1P6u8lKGnATNoMuDEwRI32TKE9prFkRQ0qYfiQd1TKKxvFbdu1Gj/\n"
    "T42CTNSG8Ol704ISGo4wDFisTNLYqWGfInP9MG+k2P4ef6Y9fnpbeR27Es6wfgks\n"
    "deqaA/K/H1v3ViMvaz/Q+D0YQ2IsS1knv3Q9WDiaaoFktDe8KbLAvm9E7w==\n"
    "-----END CERTIFICATE-----\n";

static const char test_cert[] =
    "-----BEGIN CERTIFICATE-----\n"
    "MIID0jCCArqgAwIBAgIBGTANBgkqhkiG9w0BAQUFADCBgzELMAkGA1UEBhMCVVMx\n"
    "CzAJBgNVBAgTAk1BMRMwEQYDVQQHEwpCdXJsaW5ndG9uMRQwEgYDVQQKEwtFbmdp\n"
    "bmVlcmluZzEXMBUGA1UEAxMOQWNtZSBQYWNrZXQgTUExIzAhBgkqhkiG9w0BCQEW\n"
    "FG13YW5nQGFjbWVwYWNrZXQuY29tMB4XDTExMDkyOTE1MTI1OVoXDTE2MDkyODE1\n"
    "MTI1OVowbjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAk1BMRQwEgYDVQQKEwtFbmdp\n"
    "bmVlcmluZzEXMBUGA1UEAxMOQWNtZSBQYWNrZXQgTUExIzAhBgkqhkiG9w0BCQEW\n"
    "FG13YW5nQGFjbWVwYWNrZXQuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKB\n"
    "gQD12Pql/jlWQ6ziREcVsxzfunTFgYoAB47IL3ABfMpGUtTjc/yNRwP1Q8XBQ5XG\n"
    "4KfAIxg6CJUBJOPKCSPFZAGg+wmZpXssnKHRZF42SzkzA58oqpH4fLriyPw3thOZ\n"
    "mOE/SUtOtbLaw2dI/PnyEzlpoe6LwPAgTaWdyfBDO4iLGwIDAQABo4HoMIHlMAkG\n"
    "A1UdEwQCMAAwHQYDVR0OBBYEFIe1U/J9ABj/eE8lR9g8H2cppKQIMIG4BgNVHSME\n"
    "gbAwga2AFNgtZ/lP+N3TxtCzTese186/o14AoYGJpIGGMIGDMQswCQYDVQQGEwJV\n"
    "UzELMAkGA1UECBMCTUExEzARBgNVBAcTCkJ1cmxpbmd0b24xFDASBgNVBAoTC0Vu\n"
    "Z2luZWVyaW5nMRcwFQYDVQQDEw5BY21lIFBhY2tldCBNQTEjMCEGCSqGSIb3DQEJ\n"
    "ARYUbXdhbmdAYWNtZXBhY2tldC5jb22CCQD3aNdCiZ1f2zANBgkqhkiG9w0BAQUF\n"
    "AAOCAQEATHjMTJOPfjqzRZb352zAes/g+qPH9VpEvhHYWViwxBHziw8fY0vCbtln\n"
    "ovlZSVA7UEijNxCJy5+0W8szZceXULrVPcDCdo6gnU5vpBSo25ZfTXakuKYJAulk\n"
    "Lzb037WJTSfruWsQTeu1kSE3YR5guL7et3G17RSmkVaPm4+Hb7hOCTmutK9Ag4gh\n"
    "C7R7DbjxcaRi9OI3+yea6BCWl8r5rKkQOCJy8cS6nPvL+kO3t7AfiIIcxCiIWJgs\n"
    "UWbd0EksNATtLBrhzbzpgGdKzOe9Xc5Wfx5Y8bbadq0yj9xivzYcueeuSQZ9ZM4B\n"
    "XgCKK+V1rwsoH73T0pBPMBRHP3uhDw==\n" "-----END CERTIFICATE-----\n";

static const char test_private_key[] =
    "-----BEGIN RSA PRIVATE KEY-----\n"
    "MIICXAIBAAKBgQD12Pql/jlWQ6ziREcVsxzfunTFgYoAB47IL3ABfMpGUtTjc/yN\n"
    "RwP1Q8XBQ5XG4KfAIxg6CJUBJOPKCSPFZAGg+wmZpXssnKHRZF42SzkzA58oqpH4\n"
    "fLriyPw3thOZmOE/SUtOtbLaw2dI/PnyEzlpoe6LwPAgTaWdyfBDO4iLGwIDAQAB\n"
    "AoGAN3SFPXI1/sZvNe7O1g88AR9u/ao1tcH+rBs/OaarfedrTA86taeCNg3eirSz\n"
    "EVRVopqH+Xc8HTD0DPu4zOq9DuDDoRgntnLm2hSATWYbcWUwW13Oyy23sPXCA/HE\n"
    "05e1nRcUANDX+H5L4rYSDA5PR6RzLTbCWtdkPJ5to8LryZkCQQD7mYQxo44TK1qf\n"
    "pJ+it+zMp+txsZQRdGTzQapVvjbNhRSBdtnlMy+DYs8ABRGKtQC6pUbpmf0G6fnh\n"
    "uhkw5vIdAkEA+iW1fACDFB3D/zutTVC+KPFyEkYUO0qrqms6NKEA+kTK2URGdYAl\n"
    "4sIDVNzfjq0kAMe9/kpRK9aInvkDkgHslwJBAPdB+U4nI/rmwlrdmBx7kHXYGGRV\n"
    "VeSJdKQzRkiZdJYpUOuid6Tt1qXSNwgX8xNTqnHC95ufP7DJxE73asZKUGkCQHdD\n"
    "L0nVvKkEoAO6Yh/wlRlUNDy+KLs0GLCBuxiIgrzPu57mzGkNFgMM2SXGwNd3eSje\n"
    "EpmAuT6bZDvXIXqlbDUCQG9LOXDLsPDcbSeslc7rr986qXUz4ewZ4zOgqWm4aRvu\n"
    "Q2l1YU9x4+4Dvt74OeHPJsSIreHlkd67Ha9Nna2w7+w=\n"
    "-----END RSA PRIVATE KEY-----\n";

int
tsc_init_openssl ()
{

    if (!SSL_library_init ()) {
        return (-1);
    }
    SSL_load_error_strings ();
    return 1;
}

int
tsc_ssl_load_crls (SSL_CTX * ctx, char *crlfile)
{
    X509_STORE *store;
    X509_LOOKUP *lookup;

    /* Get the X509_STORE from SSL context */
    if (!(store = SSL_CTX_get_cert_store (ctx))) {
        return (-1);
    }

    /* Add lookup file to X509_STORE */
    if (!(lookup = X509_STORE_add_lookup (store, X509_LOOKUP_file ()))) {
        return (-1);
    }

    /* Add the CRLS to the lookpup object */
    if (X509_load_crl_file (lookup, crlfile, X509_FILETYPE_PEM) != 1) {
        return (-1);
    }

    /* Set the flags of the store so that CRLS's are consulted */
    X509_STORE_set_flags (store,
                          X509_V_FLAG_CRL_CHECK | X509_V_FLAG_CRL_CHECK_ALL);

    return (1);
}

int
tsc_ssl_verify_callback (int ok, X509_STORE_CTX * store)
{
    char data[512];

    if (!ok) {
        X509 *cert = X509_STORE_CTX_get_current_cert (store);

        X509_NAME_oneline (X509_get_issuer_name (cert), data, 512);
        printf
            ("tsc_ssl_verify_callback: TLS verification error for issuer: '%s'",
             data);
        X509_NAME_oneline (X509_get_subject_name (cert), data, 512);
        printf
            ("tsc_ssl_verify_callback: TLS verification error for subject: '%s'",
             data);
    }
    return ok;
}

int
tsc_passwd_call_back_routine (char *buf, int size, int flag, void *passwd)
{
    strncpy (buf, (char *) (passwd), size);
    buf[size - 1] = '\0';
    return (strlen (buf));
}

int
tsc_ssl_ctx_load_verify_locations_mem (SSL_CTX * ctx, const char *CAcert,
                                       int len, int type)
{
    BIO *in;
    STACK_OF (X509_INFO) * inf;
    X509_INFO *itmp;
    int i, count = 0;

    if (type != SSL_FILETYPE_PEM)
        return 0;

    in = BIO_new_mem_buf ((void *) CAcert, len);

    if (in == NULL) {
        SSLerr (SSL_F_SSL_CTX_USE_CERTIFICATE, ERR_R_BUF_LIB);
        return 0;
    }
    inf = PEM_X509_INFO_read_bio (in, NULL, NULL, NULL);
    BIO_free (in);
    if (!inf) {
        X509err (X509_F_X509_LOAD_CERT_CRL_FILE, ERR_R_PEM_LIB);
        return 0;
    }
    for (i = 0; i < sk_X509_INFO_num (inf); i++) {
        itmp = sk_X509_INFO_value (inf, i);
        if (itmp->x509) {
            TSC_DEBUG
                ("SSL_CTX_load_verify_locations: loading x509 cert into store");
            X509_STORE_add_cert (ctx->cert_store, itmp->x509);
            count++;
        }
        if (itmp->crl) {
            TSC_DEBUG
                ("SSL_CTX_load_verify_locations: loading x509 crl into store");
            X509_STORE_add_crl (ctx->cert_store, itmp->crl);
            count++;
        }
    }
    sk_X509_INFO_pop_free (inf, X509_INFO_free);
    return count;
}

int
tsc_ssl_ctx_use_certificate_mem (SSL_CTX * ctx, const char *buf, int len,
                                 int type)
{
    int j;
    BIO *in;
    int ret = 0;
    X509 *x = NULL;

    in = BIO_new_mem_buf ((void *) buf, len);
    if (in == NULL) {
        SSLerr (SSL_F_SSL_CTX_USE_CERTIFICATE_FILE, ERR_R_BUF_LIB);
        goto end;
    }

    if (type == SSL_FILETYPE_ASN1) {
        j = ERR_R_ASN1_LIB;
        x = d2i_X509_bio (in, NULL);
    }
    else if (type == SSL_FILETYPE_PEM) {
        j = ERR_R_PEM_LIB;
        x = PEM_read_bio_X509 (in, NULL, ctx->default_passwd_callback,
                               ctx->default_passwd_callback_userdata);
    }
    else {
        SSLerr (SSL_F_SSL_CTX_USE_CERTIFICATE_FILE, SSL_R_BAD_SSL_FILETYPE);
        goto end;
    }

    if (x == NULL) {
        SSLerr (SSL_F_SSL_CTX_USE_CERTIFICATE_FILE, j);
        goto end;
    }

    ret = SSL_CTX_use_certificate (ctx, x);
  end:
    if (x != NULL)
        X509_free (x);
    if (in != NULL)
        BIO_free (in);
    return (ret);
}

int
tsc_ssl_ctx_use_privatekey_mem (SSL_CTX * ctx, const char *buf, int len,
                                int type)
{
    int j, ret = 0;
    BIO *in;
    EVP_PKEY *pkey = NULL;

    in = BIO_new_mem_buf ((void *) buf, len);
    if (in == NULL) {
        SSLerr (SSL_F_SSL_USE_PRIVATEKEY_FILE, ERR_R_BUF_LIB);
        goto end;
    }

    if (type == SSL_FILETYPE_PEM) {
        j = ERR_R_PEM_LIB;
        pkey =
            PEM_read_bio_PrivateKey (in, NULL, ctx->default_passwd_callback,
                                     ctx->default_passwd_callback_userdata);
    }
    else if (type == SSL_FILETYPE_ASN1) {
        j = ERR_R_ASN1_LIB;
        pkey = d2i_PrivateKey_bio (in, NULL);
    }
    else {
        SSLerr (SSL_F_SSL_CTX_USE_PRIVATEKEY_FILE, SSL_R_BAD_SSL_FILETYPE);
        goto end;
    }
    if (pkey == NULL) {
        SSLerr (SSL_F_SSL_CTX_USE_PRIVATEKEY_FILE, j);
        goto end;
    }
    ret = SSL_CTX_use_PrivateKey (ctx, pkey);
    EVP_PKEY_free (pkey);
  end:
    if (in != NULL)
        BIO_free (in);
    return (ret);
}


static tsc_ssl_init_status
tsc_init_ssl_context_common (SSL_CTX * tsc_ssl_ctx, tsc_security_config * cfg)
{
    if (tsc_ssl_ctx == NULL) {
        TSC_ERROR ("tsc_init_ssl_context: Could not create ssl_ctx");
        return tsc_ssl_init_error;
    }
    if(!SSL_CTX_set_cipher_list (tsc_ssl_ctx, cipherlist)){
        TSC_ERROR("tsc_init_ssl_context: Could not set cipherlist");
        return tsc_ssl_init_error;
    }
    if (cfg->read_from_file != tsc_bool_true) {
        TSC_DEBUG
            ("tsc_init_ssl_context_common: read certificate from memory buffer");
        TSC_DEBUG ("ca cert length is %d", cfg->config_ca.ca_len);
        if(!cfg->auth_disable){
            /* Testing code
                if(!tsc_ssl_ctx_load_verify_locations_mem(tsc_ssl_ctx, test_ca,
                strlen(test_ca), SSL_FILETYPE_PEM)){ */
            if (!tsc_ssl_ctx_load_verify_locations_mem
                (tsc_ssl_ctx, (const char *) cfg->config_ca.ca,
                cfg->config_ca.ca_len, SSL_FILETYPE_PEM)) {
                TSC_ERROR ("SSL_CTX_load_verify_locations: bad CA file");
                return tsc_ssl_init_error;
            }
            SSL_CTX_set_verify (tsc_ssl_ctx,
                                SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT,
                                tsc_ssl_verify_callback);
        }
        /* Selection Cipher suits - load the application specified ciphers */
        SSL_CTX_set_default_passwd_cb_userdata (tsc_ssl_ctx,
                                                (void *)
                                                TSC_CALL_BACK_USER_DATA);
        SSL_CTX_set_default_passwd_cb (tsc_ssl_ctx,
                                       tsc_passwd_call_back_routine);

        TSC_DEBUG ("client cert length is %d", cfg->config_cert.cert_len);
        /* Testing code if(!tsc_ssl_ctx_use_certificate_mem(tsc_ssl_ctx,
           test_cert, strlen(test_cert), SSL_FILETYPE_PEM)) { */
        if (!tsc_ssl_ctx_use_certificate_mem
            (tsc_ssl_ctx, (const char *) cfg->config_cert.cert,
             cfg->config_cert.cert_len, SSL_FILETYPE_PEM)) {
            TSC_ERROR
                ("tsc_init_ssl_context: SSL_CTX_use_certificate_file failed");
            return tsc_ssl_init_error;
        }
        TSC_DEBUG ("client key length is %d", cfg->private_key_len);
        /* Testing code if(!tsc_ssl_ctx_use_privatekey_mem(tsc_ssl_ctx,
           test_private_key, strlen(test_private_key), SSL_FILETYPE_PEM)) { */
        if (!tsc_ssl_ctx_use_privatekey_mem
            (tsc_ssl_ctx, (const char *) cfg->private_key,
             cfg->private_key_len, SSL_FILETYPE_PEM)) {
            TSC_ERROR
                ("tsc_init_ssl_context: SSL_CTX_use_PrivateKey_file failed");
            return tsc_ssl_init_error;
        }
        return tsc_ssl_init_normal;
    }
    else {
#ifdef TSC_ANDROID
        memset (tsc_ssl_ca_name, 0, sizeof (tsc_ssl_ca_name));
        memcpy (tsc_ssl_ca_name, cfg->ca_file, sizeof (cfg->ca_file));
        TSC_DEBUG ("tsc_init_ssl_context_common: ca_file is %s",
                   cfg->ca_file);
#endif
        /* Load the trusted CA's */
        if(!cfg->auth_disable){
            if (SSL_CTX_load_verify_locations (tsc_ssl_ctx, tsc_ssl_ca_name, NULL)
                != 1) {
                TSC_ERROR ("SSL_CTX_load_verify_locations: bad CA file");
                return tsc_ssl_init_error;
            }

            SSL_CTX_set_verify (tsc_ssl_ctx,
                                SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT,
                                tsc_ssl_verify_callback);
        }
        /* Selection Cipher suits - load the application specified ciphers */
        SSL_CTX_set_default_passwd_cb_userdata (tsc_ssl_ctx,
                                                (void *)
                                                TSC_CALL_BACK_USER_DATA);
        SSL_CTX_set_default_passwd_cb (tsc_ssl_ctx,
                                       tsc_passwd_call_back_routine);

#ifdef TSC_ANDROID
        memset (tsc_ssl_cert_name, 0, sizeof (tsc_ssl_cert_name));
        memcpy (tsc_ssl_cert_name, cfg->cert_file, sizeof (cfg->cert_file));
        TSC_DEBUG ("tsc_init_ssl_context_common: cert_file is %s",
                   cfg->cert_file);
        memset (tsc_ssl_key_name, 0, sizeof (tsc_ssl_key_name));
        memcpy (tsc_ssl_key_name, cfg->private_key_file,
                sizeof (cfg->private_key_file));
        TSC_DEBUG ("tsc_init_ssl_context_common: private_key_file is %s",
                   cfg->private_key_file);
#endif
        if (SSL_CTX_use_certificate_file
            (tsc_ssl_ctx, tsc_ssl_cert_name, SSL_FILETYPE_PEM) != 1) {
            TSC_ERROR
                ("tsc_init_ssl_context: SSL_CTX_use_certificate_file failed");
            return tsc_ssl_init_error;
        }

        if (SSL_CTX_use_PrivateKey_file
            (tsc_ssl_ctx, tsc_ssl_key_name, SSL_FILETYPE_PEM) != 1) {
            TSC_ERROR
                ("tsc_init_ssl_context: SSL_CTX_use_PrivateKey_file failed");
            return tsc_ssl_init_error;
        }
        return tsc_ssl_init_normal;
    }
}

tsc_ssl_init_status
tsc_init_ssl_context (tsc_security_config * cfg, tsc_transport transport_type)
{
    if (cfg == NULL) {
        TSC_ERROR ("tsc_init_ssl_context: Invalid configuration");
        return tsc_ssl_init_error;
    }

    if (tsc_transport_tls == transport_type) {
        tsc_tls_ctx = SSL_CTX_new (TLSv1_method ());
        return tsc_init_ssl_context_common (tsc_tls_ctx, cfg);
    }

    if (tsc_transport_dtls == transport_type) {
        tsc_dtls_ctx = SSL_CTX_new (DTLSv1_method ());
        if (tsc_dtls_ctx) {
            /* SSL_CTX_set_options(tsc_dtls_ctx, SSL_OP_NO_QUERY_MTU); */
            SSL_CTX_set_read_ahead (tsc_dtls_ctx, 1);
        }
        return tsc_init_ssl_context_common (tsc_dtls_ctx, cfg);
    }
    TSC_ERROR ("tsc_init_ssl_context: Invalid transport type");
    return tsc_ssl_init_error;
}

const char *
tsc_ssl_error_string (SSL * ssl, int size)
{
    int err;
    err = SSL_get_error (ssl, size);
    switch (err) {
    case SSL_ERROR_NONE:
        return "No error";
    case SSL_ERROR_WANT_WRITE:
        return "SSL_read returned SSL_ERROR_WANT_WRITE";
    case SSL_ERROR_WANT_READ:
        return "SSL_read returned SSL_ERROR_WANT_READ";
    case SSL_ERROR_WANT_X509_LOOKUP:
        return "SSL_read returned SSL_ERROR_WANT_X509_LOOKUP";
        break;
    case SSL_ERROR_SYSCALL:
        if (size < 0) {         /* not EOF */
            return strerror (errno);
        }
        else {                  /* EOF */
            return "SSL socket closed on SSL_read";
        }
    }
    return "Unknown SSL Error.";
}

const char *
tsc_ssl_error_string2 (int err)
{
    switch (err) {
    case SSL_ERROR_NONE:
        return "No error";
    case SSL_ERROR_WANT_WRITE:
        return "SSL_read returned SSL_ERROR_WANT_WRITE";
    case SSL_ERROR_WANT_READ:
        return "SSL_read returned SSL_ERROR_WANT_READ";
    case SSL_ERROR_WANT_X509_LOOKUP:
        return "SSL_read returned SSL_ERROR_WANT_X509_LOOKUP";
        break;
    case SSL_ERROR_SYSCALL:
        return "SSL socket closed on SSL_read";
    }
    return "Unknown SSL Error.";
}

int
tsc_ssl_write (SSL * ssl, const void *buf, int num)
{

    return SSL_write (ssl, buf, num);
}

int
tsc_ssl_read (SSL * ssl, void *buf, int num)
{

    return SSL_read (ssl, buf, num);
}

int
tsc_ssl_shutdown (SSL * ssl)
{

    return SSL_shutdown (ssl);
}

void
tsc_ssl_set_shutdown (SSL * ssl)
{

    SSL_set_shutdown (ssl, SSL_SENT_SHUTDOWN | SSL_RECEIVED_SHUTDOWN);
}

void
tsc_ssl_free (SSL * ssl)
{

    SSL_free (ssl);
}

void
tsc_ctx_free (tsc_transport transport_type) {
    if (tsc_transport_tls == transport_type) {
        SSL_CTX_free (tsc_tls_ctx);
    }
    if (tsc_transport_dtls == transport_type) {
        SSL_CTX_free (tsc_dtls_ctx);
    }
}

int
tsc_ssl_accept (SSL * ssl)
{

    return SSL_accept (ssl);
}

int
tsc_ssl_connect (SSL * ssl)
{

    return SSL_connect (ssl);
}

int
tsc_ssl_set_fd (SSL * ssl, int fd)
{

    return SSL_set_fd (ssl, fd);
}

X509 *
tsc_ssl_get_peer_certificate (const SSL * ssl)
{

    return SSL_get_peer_certificate (ssl);
}

SSL *
tsc_ssl_new (tsc_transport transport_type)
{
    if (tsc_transport_tls == transport_type) {
        return SSL_new (tsc_tls_ctx);
    }
    if (tsc_transport_dtls == transport_type) {
        return SSL_new (tsc_dtls_ctx);
    }
    return NULL;
}
#endif
