/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#ifndef __TSC_RAW_H__
#define __TSC_RAW_H__

#ifdef TSC_FTCP

#include "tsc_common.h"
#include "tsc_utils.h"
#include "tsc_thread.h"
#include "tsc_lock.h"
#include "tsc_queue.h"
#include "tsc_tcp.h"
#include "tsc_udp.h"
#include "tsc_ether.h"

#define TSC_RAW_QUEUE_SIZE 0x4000

typedef enum
{
    tsc_raw_response_ok = 0,
    tsc_raw_response_not_found,
    tsc_raw_response_no_data,
    tsc_raw_response_error
} tsc_raw_response;

typedef struct
{
    pcap_t *handler;
    tsc_thread *processing_thread;
    tsc_lock *in_lock;
    tsc_queue *in_queue;
      tsc_bool (*filter) (tsc_buffer *);
} tsc_raw;

size_t tsc_raw_if_size ();
tsc_raw_response tsc_raw_if_device (size_t idx, char *device);

tsc_raw *tsc_raw_open (char *device, tsc_bool (*filter) (tsc_buffer *));
tsc_raw_response tsc_raw_close (tsc_raw * raw_if);

tsc_raw_response tsc_raw_write (tsc_raw * raw_if, tsc_buffer * buffer);
tsc_raw_response tsc_raw_read (tsc_raw * raw_if, tsc_buffer * buffer);

tsc_raw_response tsc_raw_ready_to_read (tsc_raw * raw_if);

tsc_raw_response tsc_raw_ftcp_send (tsc_raw * raw_if,
                                    tsc_ip_port_address * src_addr,
                                    tsc_ip_port_address * dst_addr,
                                    tsc_buffer * buffer);

tsc_raw_response tsc_raw_ftcp_recv (tsc_raw * raw_if,
                                    tsc_ip_port_address * src_addr,
                                    tsc_ip_port_address * dst_addr,
                                    tsc_buffer * buffer);

#endif /* TSC_FTCP */

#endif /* __TSC_RAW_H__ */
