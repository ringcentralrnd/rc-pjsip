/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#include "tsc_log.h"
#include "tsc_lock.h"

#ifdef TSC_ANDROID
#define TSC_ANDROID_LOG(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, "tsc", __VA_ARGS__))
#endif

tsc_log_level global_log_level = tsc_log_level_disabled;

FILE *global_log_fd = NULL;
tsc_lock *global_log_lock = NULL;
tsc_log_func tsc_log_cb = NULL;

tsc_error_code
tsc_set_log_level (tsc_log_level log_level)
{
    global_log_level = log_level;

    return tsc_error_code_ok;
}

tsc_error_code
tsc_set_log_handler(tsc_log_func logfunction)
{
  tsc_log_cb = logfunction;

  return tsc_error_code_ok;
}

tsc_log_level
tsc_get_log_level ()
{
    return global_log_level;
}


tsc_error_code
tsc_log (uint32_t subsystem, tsc_log_level log_level, uint32_t line,
         const char *format, ...)
{
    if (log_level <= global_log_level) {
        if (!global_log_lock) {
	  global_log_lock = tsc_lock_new();
        }
	
	
        if (global_log_lock && (tsc_lock_get(global_log_lock) != tsc_lock_response_error)) {
            va_list list;

            va_start (list, format);

            char tmp[TSC_BUFFER_LEN];
            vsprintf (tmp, format, list);
#ifndef TSC_ANDROID
            FILE *output_fd = global_log_fd;

            if (!output_fd) {
                output_fd = stdout;
            }
#else
            FILE *output_fd = global_log_fd;
#endif

            time_t time = tsc_time ();

            struct tm *time_ptr = localtime (&time);

            if (time_ptr) {
                char aux[TSC_MAX_STR_LEN];
                strftime (aux, TSC_MAX_STR_LEN, "%a %x %X", time_ptr);

                int ticks = tsc_get_clock () - (tsc_get_clock () / 1000) * 1000;

                if (ticks < 0) {
                    ticks += 1000;
                }
		if(tsc_log_cb != NULL){
		    char tmpcb[TSC_BUFFER_LEN];
		    strcpy(tmpcb, format);
		    strcat(tmpcb, aux);
		    strcat(tmpcb,"\n");
		    tsc_log_cb(log_level, (const char*) tmpcb, list);
		}

#ifdef TSC_ANDROID
                if (output_fd) {
                    fprintf (output_fd, "%s.%03d ", aux, ticks);
                } else {
                    TSC_ANDROID_LOG ("%s.%03d ", aux, ticks);
                }
#else
                fprintf (output_fd, "%s.%03d ", aux, ticks);
#endif
            }

#ifdef TSC_ANDROID
            if (output_fd) {
                fprintf (output_fd, "%s <- l=%d\n", tmp, line);
            } else {
                TSC_ANDROID_LOG ("%s <- l=%d\n", tmp, line);
            }
#else
            fprintf (output_fd, "%s <- l=%d\n", tmp, line);

            fflush (output_fd);
#endif

            tsc_lock_release(global_log_lock);

            return tsc_error_code_ok;
        }
    }

    return tsc_error_code_not_logged;
}

tsc_error_code
tsc_set_log_output (FILE * log_fd)
{
    global_log_fd = log_fd;

    return tsc_error_code_ok;
}
