/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/
/*! \brief
statistics api
*/

#include "tsc_statistics.h"
#include "tsc_data.h"
#include "tsc_tunnel.h"

#define TSC_LOG_SUBSYSTEM TSC_LOG_SUBSYSTEM_GENERAL

tsc_error_code
tsc_process_stats (tsc_handle handle, tsc_stats_option option,
                   tsc_stats_action action, int value, tsc_statistics * stats)
{
    if (handle) {
        tsc_csm_info *tunnel_info = (tsc_csm_info *) handle;

        if (tunnel_info) {

            if (action == tsc_stats_action_inc) {

                if (option == tsc_sent_bytes) {
                    tunnel_info->statistics.sent_bytes += value;
                }

                else if (option == tsc_recv_bytes) {
                    tunnel_info->statistics.recv_bytes += value;
                }

                else if (option == tsc_socket_count) {
                    tunnel_info->statistics.socket_count += value;
                }

                else if (option == tsc_sockets_created) {
                    tunnel_info->statistics.sockets_created += value;
                }

                else if (option == tsc_max_queue_gap) {
                    tunnel_info->statistics.max_queue_gap += value;
                }

		else if (option == tsc_dropped_in_packets) {
		    tunnel_info->statistics.dropped_in_packets += value;
                }

		else if (option == tsc_dropped_out_packets) {
		    tunnel_info->statistics.dropped_out_packets += value;
                }

                else if (option == tsc_reconn_attempts) {
                    tunnel_info->statistics.reconn_attempts += value;
                }

                else if (option == tsc_keep_alive_count) {
                    tunnel_info->statistics.keep_alive_count += value;

                }

                else if (option == tsc_service_request_count) {
                    tunnel_info->statistics.service_request_count += value;

                }

                else if (option == tsc_min_in_processing) {
                     tunnel_info->statistics.min_in_processing += value;
                }

                else if (option == tsc_max_in_processing) {
                     tunnel_info->statistics.max_in_processing += value;
                }

                else if (option == tsc_avg_in_processing) {
                     tunnel_info->statistics.avg_in_processing += value;
                }

                else if (option == tsc_in_packet_count) {
                     tunnel_info->statistics.in_packet_count += value;
                }

                else if (option == tsc_min_out_processing) {
                     tunnel_info->statistics.min_out_processing += value;
                }

                else if (option == tsc_max_out_processing) {
                     tunnel_info->statistics.max_out_processing += value;
                }

                else if (option == tsc_avg_out_processing) {
                     tunnel_info->statistics.avg_out_processing += value;
                }

                else if (option == tsc_out_packet_count) {
                     tunnel_info->statistics.out_packet_count += value;
                }

                else {
                    TSC_ERROR ("tsc_process_stats: bad option %d [%p]",
                               option, handle);
                }


            }
            else if (action == tsc_stats_action_set) {

                if (option == tsc_sent_bytes) {
                    tunnel_info->statistics.sent_bytes = value;
                }

                else if (option == tsc_recv_bytes) {
                    tunnel_info->statistics.recv_bytes = value;
                }

                else if (option == tsc_socket_count) {
                    tunnel_info->statistics.socket_count = value;
                }

                else if (option == tsc_sockets_created) {
                    tunnel_info->statistics.sockets_created = value;
                }

                else if (option == tsc_max_queue_gap) {
                    tunnel_info->statistics.max_queue_gap = value;
                }
		
		else if (option == tsc_dropped_in_packets) {
		    tunnel_info->statistics.dropped_in_packets = value;
                }

		else if (option == tsc_dropped_out_packets) {
		    tunnel_info->statistics.dropped_out_packets = value;
                }

                else if (option == tsc_reconn_attempts) {
                    tunnel_info->statistics.reconn_attempts = value;
                }

                else if (option == tsc_keep_alive_count) {
                    tunnel_info->statistics.keep_alive_count = value;

                }
                else if (option == tsc_service_request_count) {
                    tunnel_info->statistics.service_request_count = value;

                }
                else if (option == tsc_min_in_processing) {
                     tunnel_info->statistics.min_in_processing = value;
                }

                else if (option == tsc_max_in_processing) {
                     tunnel_info->statistics.max_in_processing = value;
                }

                else if (option == tsc_avg_in_processing) {
                     tunnel_info->statistics.avg_in_processing = value;
                }

                else if (option == tsc_in_packet_count) {
                     tunnel_info->statistics.in_packet_count = value;
                }

                else if (option == tsc_min_out_processing) {
                     tunnel_info->statistics.min_out_processing = value;
                }

                else if (option == tsc_max_out_processing) {
                     tunnel_info->statistics.max_out_processing = value;
                }

                else if (option == tsc_avg_out_processing) {
                     tunnel_info->statistics.avg_out_processing = value;
                }

                else if (option == tsc_out_packet_count) {
                     tunnel_info->statistics.out_packet_count = value;
                }

                else {
                    TSC_ERROR ("tsc_process_stats: bad option %d [%p]",
                               option, handle);
                }


            }
            else if (action == tsc_stats_action_clr) {


                if (option == tsc_sent_bytes) {
                    tunnel_info->statistics.sent_bytes = 0;
                }

                else if (option == tsc_recv_bytes) {
                    tunnel_info->statistics.recv_bytes = 0;
                }

                else if (option == tsc_socket_count) {
                    tunnel_info->statistics.socket_count = 0;
                }

                else if (option == tsc_sockets_created) {
                    tunnel_info->statistics.sockets_created = 0;
                }

                else if (option == tsc_max_queue_gap) {
                    tunnel_info->statistics.max_queue_gap = 0;
                }

		else if (option == tsc_dropped_in_packets) {
		    tunnel_info->statistics.dropped_in_packets = 0;
		    
                }

		else if (option == tsc_dropped_out_packets) {
		    tunnel_info->statistics.dropped_out_packets = 0;
		    
                }

                else if (option == tsc_reconn_attempts) {
                    tunnel_info->statistics.reconn_attempts = 0;
                }

                else if (option == tsc_keep_alive_count) {
                    tunnel_info->statistics.keep_alive_count = 0;

                }

                else if (option == tsc_service_request_count) {
                    tunnel_info->statistics.service_request_count = 0;

                }

                else if (option == tsc_min_in_processing) {
                     tunnel_info->statistics.min_in_processing = 0;
                }

                else if (option == tsc_max_in_processing) {
                     tunnel_info->statistics.max_in_processing = 0;
                }

                else if (option == tsc_avg_in_processing) {
                     tunnel_info->statistics.avg_in_processing = 0;
                }

                else if (option == tsc_in_packet_count) {
                     tunnel_info->statistics.in_packet_count = 0;
                }

                else if (option == tsc_min_out_processing) {
                     tunnel_info->statistics.min_out_processing = 0;
                }

                else if (option == tsc_max_out_processing) {
                     tunnel_info->statistics.max_out_processing = 0;
                }

                else if (option == tsc_avg_out_processing) {
                     tunnel_info->statistics.avg_out_processing = 0;
                }

                else if (option == tsc_out_packet_count) {
                     tunnel_info->statistics.out_packet_count = 0;
                }
                else {
                    TSC_ERROR ("tsc_process_stats: bad option %d [%p]",
                               option, handle);
                }
            }
            else if (action == tsc_stats_action_get) {

                if (stats != NULL) {
                    memcpy (stats, &(tunnel_info->statistics),
                            sizeof (tsc_statistics));
                }
            }
            else {
                TSC_ERROR ("tsc_process_stats: bad action %d [%p]", action,
                           handle);
                return tsc_error_code_error;
            }
        }
        else {
            TSC_ERROR
                ("tsc_process_stats: failed to retrieve tunnel info [%p]",
                 handle);
            return tsc_error_code_error;
        }
    }
    return tsc_error_code_ok;
}


tsc_error_code
tsc_get_stats (tsc_handle handle, tsc_statistics * stats)
{

    tsc_csm_info *info = (tsc_csm_info *) handle;
    if (info) {
        if (tsc_lock_get (info->data_lock) != tsc_lock_response_error) {

            TSC_STATS_GET (handle, stats);

            tsc_lock_release (info->data_lock);

            TSC_DEBUG ("tsc_get_stats: stats retrieved ok [%p]", handle);

            return tsc_error_code_ok;
        }
    }
    TSC_ERROR ("tsc_get_stats: failed to retrieve stats [%p]", handle);
    return tsc_error_code_error;

}

tsc_error_code
tsc_dump_stats (tsc_handle handle)
{
    tsc_csm_info *info = (tsc_csm_info *) handle;
    TSC_DEBUG ("tsc_dump_stats: Tunnel Statistics [%p]", handle);
    TSC_DEBUG ("tsc_dump_stats: Bytes sent: %d", info->statistics.sent_bytes);
    TSC_DEBUG ("tsc_dump_stats: Bytes received: %d",
               info->statistics.recv_bytes);
    TSC_DEBUG ("tsc_dump_stats: Current socket count %d",
               info->statistics.socket_count);
    TSC_DEBUG ("tsc_dump_stats: Number of sockets created: %d",
               info->statistics.sockets_created);
    TSC_DEBUG ("tsc_dump_stats: Max detected packet queue delay: %d",
               info->statistics.max_queue_gap);
    TSC_DEBUG ("tsc_dump_stats: Number of outbound packets dropped: %d",
               info->statistics.dropped_in_packets);
    TSC_DEBUG ("tsc_dump_stats: Number of inbound packets dropped: %d",
               info->statistics.dropped_out_packets);
    TSC_DEBUG ("tsc_dump_stats: Reconnection Attempts: %d",
               info->statistics.reconn_attempts);
    TSC_DEBUG ("tsc_dump_stats: Keep alives sent: %d",
               info->statistics.keep_alive_count);
    TSC_DEBUG ("tsc_dump_stats: Packets received: %d",
               info->statistics.in_packet_count);
    TSC_DEBUG ("tsc_dump_stats: Min processing in queue delay: %d",
               info->statistics.min_in_processing);
    TSC_DEBUG ("tsc_dump_stats: Max processing in queue delay: %d",
               info->statistics.max_in_processing);
    TSC_DEBUG ("tsc_dump_stats: Avg processing in queue delay: %d",
               info->statistics.avg_in_processing);
    TSC_DEBUG ("tsc_dump_stats: Packets sent: %d",
               info->statistics.out_packet_count);
    TSC_DEBUG ("tsc_dump_stats: Min processing out queue delay: %d",
               info->statistics.min_out_processing);
    TSC_DEBUG ("tsc_dump_stats: Max processing out queue delay: %d",
               info->statistics.max_out_processing);
    TSC_DEBUG ("tsc_dump_stats: Avg processing out queue delay: %d",
               info->statistics.avg_out_processing);

    return tsc_error_code_ok;
}
