/*************************************************************************
* 
* Acme Packet CONFIDENTIAL
* __________________
* 
*  Copyright [2011] - [2012] Acme Packet Incorporated 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Acme Packet Incorporated and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Acme Packet Incorporated
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Acme Packet Incorporated.
*/

#include "tsc_csm.h"
#include "tsc_redundancy.h"

#define TSC_LOG_SUBSYSTEM TSC_LOG_SUBSYSTEM_STATE_MACHINE

tsc_bool tsc_csm_set_redundancy (tsc_csm_info * info, tsc_socket_info *socket, 
                                 tsc_ip_port_address * address,
                                 tsc_so_redundancy *redundancy, tsc_bool set)
{
    if (info) {
        if (info->state_info.state != tsc_state_established && info->state_info.state != tsc_state_established_slow_poll) {
            TSC_ERROR ("tsc_csm_set_redundancy : wrong state [state %d] [%p]", 
                       info->state_info.state, info);   

            return tsc_bool_false;
        }

        uint8_t redundancy_factor = redundancy->redundancy_factor;

        tsc_csm_msg data;
        memset(&data, 0, sizeof(tsc_csm_msg));
        data.msg_type = tsc_csm_msg_type_service_request;
        data.timestamp = tsc_get_clock();
        data.opaque = (void *)socket;

        if ((set == tsc_bool_true) && (redundancy_factor > 0)) {
            data.info.service_request.
            service_type = TSC_TLV_SERVICE_TYPE_ENABLE_REDUNDANCY;
        } else {
            data.info.service_request.
            service_type = TSC_TLV_SERVICE_TYPE_DISABLE_REDUNDANCY;
        }

        data.info.service_request.service_request.
        redundancy.redundancy_factor = redundancy_factor;

        tsc_transport transport = info->tunnel_params.connection_params[info->connection_index].
                                  transport;

        data.info.service_request.service_request.
        redundancy.connection_info_use = tsc_tlv_connection_info_use_ipv4;

        if (transport == tsc_transport_udp || transport == tsc_transport_dtls) {
            data.info.service_request.service_request.
                redundancy.redundancy_method = tsc_tlv_redundancy_method_udp_dtls;
        } else {
            if (redundancy->redundancy_method == tsc_so_redundancy_method_tcp_tls_fan_out) {
                data.info.service_request.service_request.
                    redundancy.redundancy_method = tsc_tlv_redundancy_method_tcp_tls_fan_out;
            } else {
                data.info.service_request.service_request.
                    redundancy.redundancy_method = tsc_tlv_redundancy_method_tcp_tls_load_balance;
            }
        }

        memcpy(&(data.info.service_request.service_request.
        redundancy.connection_info.connection_info_ipv4.
        src_address), address, sizeof(tsc_ip_port_address));

        if (tsc_csm_write_in_msg (info, &data) == tsc_error_code_ok) {
            TSC_DEBUG ("tsc_csm_set_redundancy: request queued ok [%p]",
                       info);

            return tsc_bool_true;
        }
    }

    TSC_ERROR ("tsc_csm_set_redundancy: failed to send request [%p]", info);

    return tsc_bool_false;
    
}

size_t tsc_redundancy_header_make(uint32_t sequence_number, size_t len, uint8_t *buf_ptr)
{
    /* 23|22|21|20|19|18|17|16||15|14|13|12|11|10|9|8||7|6|5|4|3|2|1|0
        R|          SEQUENCE NUMBER        |      PAYLOAD LENGTH       */

    uint32_t header = ((sequence_number & (TSC_REDUNDANCY_MAX_SEQ-1)) << 12) | len;

    buf_ptr[0] = (header & 0xff0000) >> 16;
    buf_ptr[1] = (header & 0xff00) >> 8;
    buf_ptr[2] = (header & 0xff);

    /* header size in bytes */
    return TSC_REDUNDANCY_HEADER_LENGTH;      
}

size_t tsc_redundancy_header_parse(uint8_t *buf_ptr, uint32_t *sequence_number, size_t *len)
{
    /* 23|22|21|20|19|18|17|16||15|14|13|12|11|10|9|8||7|6|5|4|3|2|1|0
        R|          SEQUENCE NUMBER        |      PAYLOAD LENGTH       */

    uint32_t header = (((uint32_t)buf_ptr[0]) << 16) | (((uint32_t)buf_ptr[1]) << 8) | ((uint32_t)buf_ptr[2]);

    if (sequence_number) {
        *sequence_number = header >> 12;
    }

    if (len) {
        *len = header & 0xfff;
    }

    /* header size in bytes */
    return TSC_REDUNDANCY_HEADER_LENGTH;      
}

size_t tsc_redundancy_make(tsc_socket_info *info, char *buf_ptr, size_t len, char *out_buf,
                           tsc_bool datagram, uint8_t index)
{
    uint32_t i;

    char *buf = buf_ptr;

    size_t header_length = tsc_redundancy_header_make(info->redundancy_info.tx_sequence_number - index,
                                                      len, (uint8_t *)out_buf);

    size_t offset = 0;

    if (buf_ptr) {
        info->redundancy_info.tx_sequence_number++;

        if (info->redundancy_info.tx_sequence_number == TSC_REDUNDANCY_MAX_SEQ) {
            info->redundancy_info.tx_sequence_number = 0;
        }

        memcpy(out_buf + header_length, buf_ptr, len);
        offset = len + header_length;
    }

    for (i = 0; i < info->redundancy_info.
         redundancy_factor; i++) {
        if (info->redundancy_info.redundancy_buffer[i].len > 0) {
            if (datagram == tsc_bool_true) {
                header_length = tsc_redundancy_header_make(info->redundancy_info.tx_sequence_number - index -i - 2,
                                                           info->redundancy_info.redundancy_buffer[i].len,
                                                           (uint8_t *)out_buf + offset);

                memcpy(out_buf + offset + header_length, 
                       info->redundancy_info.redundancy_buffer[i].data,
                       info->redundancy_info.redundancy_buffer[i].len);

                offset += info->redundancy_info.
                      redundancy_buffer[i].len + header_length;
            } else {
                if (index - 1 == i) {
                    header_length = tsc_redundancy_header_make(info->redundancy_info.tx_sequence_number - index,
                                                               info->redundancy_info.redundancy_buffer[i].len, 
                                                               (uint8_t *)out_buf);

                    memcpy(out_buf + header_length, 
                       info->redundancy_info.redundancy_buffer[i].data,
                       info->redundancy_info.redundancy_buffer[i].len);

                    offset = info->redundancy_info.redundancy_buffer[i].len + header_length;

                    break;
                }
            }
        }
    }

    if (buf_ptr) {
        for (i = TSC_REDUNDANCY_MAX_STREAMS - 1; i != 0; i--) {
            memcpy(&(info->redundancy_info.redundancy_buffer[i]),
                   &(info->redundancy_info.redundancy_buffer[i-1]),
                   sizeof(tsc_buffer));
        }

        memcpy(info->redundancy_info.redundancy_buffer[0].data, buf, len);
        info->redundancy_info.redundancy_buffer[0].len = len;
    }

    return offset;
}

tsc_bool tsc_redundancy_parse(tsc_csm_info * info, tsc_csm_msg * msg,
                              tsc_socket_info *found, tsc_queue *out_queue,
                              tsc_bool datagram)
{
    uint8_t *data = msg->info.buffer.data 
                  + TSC_IP_HEADER_SIZE
                  + TSC_UDP_HEADER_SIZE;

    size_t size = msg->info.buffer.len
                  - TSC_IP_HEADER_SIZE
                  - TSC_UDP_HEADER_SIZE;

    uint8_t *frame[TSC_REDUNDANCY_MAX_STREAMS + 1];
    uint32_t frame_len[TSC_REDUNDANCY_MAX_STREAMS + 1];

    int8_t count = 0;

    uint32_t sequence_number;
    size_t len;

    size_t ptr = 0;

    while (ptr < size) {
        uint32_t sequence;
        size_t header_length = tsc_redundancy_header_parse(data + ptr, &sequence, &len);

        if (!count) {
            sequence_number = sequence;
        }

        if (len > size) {
            TSC_DEBUG ("tsc_redundancy_parse: size %d illegal, not a redundancy frame [%p]", 
                      len,
                      info);

            return tsc_bool_false;
        }

        frame[count] = data + ptr + header_length;
        frame_len[count] = len;

        ptr += len + header_length;

        count++;
    }

    count--;

    while (count >= 0) {
        int32_t current_sequence_number = sequence_number - count;

        if (current_sequence_number < 0) {
            current_sequence_number += TSC_REDUNDANCY_MAX_SEQ;
        }

        uint32_t len = frame_len[count];

        tsc_csm_msg tmp;
        memcpy(&tmp, msg, sizeof(tsc_csm_msg));

        memcpy(tmp.info.buffer.data 
               + TSC_IP_HEADER_SIZE
               + TSC_UDP_HEADER_SIZE, frame[count], len);
        tmp.info.buffer.len = len + TSC_IP_HEADER_SIZE 
                                  + TSC_UDP_HEADER_SIZE;

        uint32_t j;

        tsc_bool received = tsc_bool_false;

        if (datagram == tsc_bool_true) {
            /* sliding window for udp/dtls */
            for (j = 0; j < TSC_REDUNDANCY_MAX_FRAMES; j++) {
                tsc_redundancy_frame *frame =
                                      &(found->redundancy_info.redundancy_frame[j]);

                if (frame->state == tsc_redundancy_state_set) {
                    if (frame->sequence_number == current_sequence_number) {
                        received = tsc_bool_true;
                    }

                    uint32_t distance = TSC_REDUNDANCY_SEQ_DISTANCE(current_sequence_number,
                                                                    frame->sequence_number);

                    if (distance > TSC_REDUNDANCY_MAX_STREAMS - 1) {
                        frame->state = tsc_redundancy_state_not_set;
                    }
                }
            }
        } else {
            /* keep track of last sequence number for tcp/tls */
            if (found->redundancy_info.load_balance == tsc_bool_false) {
                if (found->redundancy_info.rx_sequence_number_set == tsc_bool_false) {
                    found->redundancy_info.rx_sequence_number_set = tsc_bool_true;
                } else if ((found->redundancy_info.rx_sequence_number < TSC_REDUNDANCY_MAX_SEQ - 1) &&
                           (found->redundancy_info.rx_sequence_number + 1 != sequence_number)) {
                    received = tsc_bool_true;
                } else if ((found->redundancy_info.rx_sequence_number == TSC_REDUNDANCY_MAX_SEQ - 1) &&
                           (sequence_number > 0)) {
                    received = tsc_bool_true;
                }
            } else {
                /* sliding window for load balance */
                for (j = 0; j < TSC_REDUNDANCY_MAX_FRAMES; j++) {
                    tsc_redundancy_frame *frame =
                                          &(found->redundancy_info.redundancy_frame[j]);

                    if (frame->state == tsc_redundancy_state_set) {
                        if (frame->sequence_number == current_sequence_number) {
                            received = tsc_bool_true;
                        }
                    }
                }
            }
        }

        if (received == tsc_bool_false) {
            if (datagram == tsc_bool_true) {
                tsc_queue_write (out_queue, &tmp);

                for (j = 0; j < TSC_REDUNDANCY_MAX_FRAMES; j++) {
                    tsc_redundancy_frame *frame = &(found->redundancy_info.redundancy_frame[j]);

                    if (frame->state == tsc_redundancy_state_not_set) {
                        frame->state = tsc_redundancy_state_set;
                        frame->sequence_number = current_sequence_number;

                        break;
                    }
                }
            } else {
                if (found->redundancy_info.load_balance == tsc_bool_false) {
                    tsc_queue_write (out_queue, &tmp);

                    found->redundancy_info.rx_sequence_number = sequence_number;
                } else {
                    /* load balance */
                    tsc_bool process = tsc_bool_false;

                    if ((current_sequence_number > TSC_REDUNDANCY_MAX_SEQ - TSC_REDUNDANCY_MAX_FRAMES - 1) &&
                        (found->redundancy_info.rx_sequence_number < TSC_REDUNDANCY_MAX_SEQ 
                                                                   - TSC_REDUNDANCY_MAX_FRAMES - 1)) {
                        if (current_sequence_number >= (found->redundancy_info.rx_sequence_number + 
                                                        TSC_REDUNDANCY_MAX_SEQ) ) {
                            process = tsc_bool_true;
                        }                    
                    } else if (current_sequence_number >= found->redundancy_info.rx_sequence_number) {
                        process = tsc_bool_true;
                    }

                    if (process == tsc_bool_true) {
                        tsc_bool inserted = tsc_bool_false;

                        while (inserted == tsc_bool_false) {
                            for (j = 0; j < TSC_REDUNDANCY_MAX_FRAMES; j++) {
                                tsc_redundancy_frame *frame = &(found->redundancy_info.redundancy_frame[j]);
                                if (frame->state == tsc_redundancy_state_not_set) {
                                    frame->state = tsc_redundancy_state_set;
                                    frame->sequence_number = current_sequence_number;
                                    memcpy(&(frame->msg), &(tmp.info.buffer), sizeof(tsc_buffer));

                                    inserted = tsc_bool_true;

                                    break;
                                }
                            }

                            if (inserted == tsc_bool_false) {
                                uint32_t min_seq = 0xffffffff;

                                for (j = 0; j < TSC_REDUNDANCY_MAX_FRAMES; j++) {
                                    tsc_redundancy_frame *frame = &(found->redundancy_info.redundancy_frame[j]);

                                    if (min_seq > frame->sequence_number) {
                                        min_seq = frame->sequence_number;
                                    }
                                }

                                found->redundancy_info.rx_sequence_number = min_seq;
                            }

                            for (j = 0; j < TSC_REDUNDANCY_MAX_FRAMES; j++) {
                                tsc_redundancy_frame *frame = &(found->redundancy_info.redundancy_frame[j]);

                                if (frame->state == tsc_redundancy_state_set) {
                                    if (found->redundancy_info.rx_sequence_number == frame->sequence_number) {
                                        tsc_csm_msg tmp;
                                        memcpy(&tmp, msg, sizeof(tsc_csm_msg));
                                        memcpy(&(tmp.info.buffer), &(frame->msg), sizeof(tsc_buffer));

                                        tsc_queue_write (out_queue, &tmp);

                                        frame->state = tsc_redundancy_state_not_set;

                                        found->redundancy_info.rx_sequence_number++;

					if (found->redundancy_info.rx_sequence_number == 
                                            TSC_REDUNDANCY_MAX_SEQ) {
                                            found->redundancy_info.rx_sequence_number = 0;
                                        }

                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        count--;
    }

    return tsc_bool_true;
}
