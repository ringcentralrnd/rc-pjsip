#include "tsc_transaction.h"
#include "tsc_csm.h"

#define TSC_LOG_SUBSYSTEM TSC_LOG_SUBSYSTEM_STATE_MACHINE

tsc_error_code tsc_transaction_insert(tsc_handle handle, tsc_cm *msg, int8_t retries, 
                                    time_t timeout, void *opaque, tsc_lock *lock, 
                                    tsc_bool (*timeout_callback)(tsc_handle, tsc_transaction *),
                                    tsc_bool (*retry_callback)(tsc_handle, tsc_transaction *,
                                                               tsc_cm *request),
                                    tsc_bool (*response_callback)(tsc_handle, tsc_transaction *,
                                                                  tsc_cm *response))
{
    tsc_csm_info *info = (tsc_csm_info *) handle;

    if (info) {
        tsc_transaction *transaction = (tsc_transaction *)malloc(sizeof(tsc_transaction));
        memset((void *)transaction, 0, sizeof(tsc_transaction));

        memcpy(&(transaction->msg), msg, sizeof(tsc_cm));
        transaction->retries = retries;
        transaction->timeout = timeout;
        transaction->opaque = opaque;
        transaction->lock = lock;
        transaction->due = tsc_time() + timeout;
        transaction->timeout_callback = timeout_callback;
        transaction->retry_callback = retry_callback;
        transaction->response_callback = response_callback;

        if (!(info->transaction_list)) {
            info->transaction_list = transaction;
        } else {
            tsc_transaction *tmp = info->transaction_list;
            tsc_transaction *last = tmp;

            while (tmp) {
                last = tmp;
                
                tmp = tmp->next;
            }

            last->next = transaction;
        }

        if (lock) {
            if (tsc_lock_get (lock) != tsc_lock_response_error) {
                transaction->lock->opaque = NULL;
                tsc_lock_release (lock);
            }
        }

        TSC_DEBUG("tsc_transaction_insert: transaction %d inserted [%p]", 
                  transaction->msg.header.sequence, info);

        return tsc_error_code_ok;
    } else {
        TSC_ERROR("tsc_transaction_insert: invalid handle [%p]", info);

        return tsc_error_code_error;
    }
}

tsc_error_code tsc_transaction_remove(tsc_handle handle, tsc_transaction *transaction)
{
    tsc_csm_info *info = (tsc_csm_info *) handle;

    if (info && transaction) {
        tsc_transaction *tmp = info->transaction_list;
        tsc_transaction *last = NULL;

        while (tmp) {
            if (transaction && (tmp->msg.header.sequence == transaction->msg.header.sequence)) {
                break;
            }

            last = tmp;
            tmp = tmp->next;

            if (!transaction) {
                if (last->lock) {
                    if (tsc_lock_get (last->lock) != tsc_lock_response_error) {
                        last->lock->opaque = (void *)-1;

                        tsc_lock_release (last->lock);
                    }
                }

                free((void *)last);
            }
        }

        if (transaction) {
            if (tmp) {
                TSC_DEBUG("tsc_transaction_remove: transaction %d removed [%p]", 
                          transaction->msg.header.sequence, info);

                if (last) {
                    last->next = tmp->next;
                } else {
                    info->transaction_list = tmp->next;
                }

                if (tmp->lock) {
                    if (tsc_lock_get (tmp->lock) != tsc_lock_response_error) {
                        tmp->lock->opaque = (void *)-1;

                        tsc_lock_release (tmp->lock);
                    }
                }

                free((void *)tmp);

                return tsc_error_code_ok;
            } else {
                TSC_ERROR("tsc_transaction_remove: transaction %d not found [%p]", 
                          transaction->msg.header.sequence, info);

                return tsc_error_code_error;
            }
        } else {
            TSC_DEBUG("tsc_transaction_remove: all transactions removed [%p]", 
                      info);

            return tsc_error_code_ok;
        }
    } else {
        TSC_ERROR("tsc_transaction_remove: invalid handle [%p]", info);

        return tsc_error_code_error;
    }
}

tsc_error_code tsc_transaction_check_timeout(tsc_handle handle)
{
    tsc_csm_info *info = (tsc_csm_info *) handle;

    if (info) {
        tsc_transaction *tmp = info->transaction_list;

        while (tmp) {
            tsc_transaction *found = NULL;

            if (tmp->due <= tsc_time()) {
                if (!tmp->retries) {
                    if (tmp->timeout_callback) {
                        if (tmp->timeout_callback(handle, tmp) == tsc_bool_true) {
                            found = tmp;
                        }
                    } else {
                        found = tmp;
                    }
                } else {
                    if (tmp->retries != -1) {
                        tmp->retries--;
                    }

                    tmp->due = tsc_time() + tmp->timeout;

                    tsc_bool send = tsc_bool_true;

                    tsc_cm *msg = &(tmp->msg);

                    if (tmp->retry_callback) {
                        send = tmp->retry_callback(handle, tmp, msg);
                    }

                    if (send == tsc_bool_true) {
                        uint8_t output[TSC_CM_MAX_SIZE];
                        size_t len;

                        len = tsc_encode_cm (msg, output, TSC_CM_MAX_SIZE);

                        if (len) {
                            if (tsc_tunnel_socket_send
                                (info->tunnel_socket, output, len,
                                 tsc_bool_false, 0) == tsc_tunnel_socket_response_ok
                                && info->tunnel_socket->result > 0) {
                                TSC_DEBUG
                                    ("tsc_transaction_check: request sent (len %d) [%p]",
                                     len, info);

                                return tsc_error_code_ok;
                            }
                            else {
                                TSC_ERROR
                                    ("tsc_transaction_check: failed to send request [%p]",
                                     info);
                            }
                        }
                        else {
                            TSC_ERROR
                                ("tsc_transaction_check: failed to encode request [%p]",
                                 info);
                        }                        
                    }
                }
            }

            tmp = tmp->next;

            if (found) {
                if (tsc_transaction_remove(handle, found) == tsc_error_code_ok) {
                    TSC_DEBUG("tsc_transaction_check: transaction removed [%p]", info);
                } else {
                    TSC_ERROR("tsc_transaction_check: failed to remove transaction [%p]", info);
                }
            }
        }

        return tsc_error_code_ok;
    } else {
        TSC_ERROR("tsc_transaction_check: invalid handle [%p]", info);

        return tsc_error_code_error;
    }
}

tsc_error_code tsc_transaction_process_response(tsc_handle handle, void *buf,
                                                size_t len)
{
    tsc_csm_info *info = (tsc_csm_info *) handle;

    if (info) {
        tsc_cm tcm;
        memset (&tcm, 0, sizeof (tsc_cm));

        if (tsc_decode_cm (buf, len, &tcm) == tsc_bool_true) {
            tsc_transaction *tmp = info->transaction_list;

            while (tmp) {
                tsc_transaction *found = NULL;

                if (tmp->msg.header.sequence == tcm.header.sequence) {
                    if (tmp->response_callback) {
                        if (tmp->response_callback(handle, tmp, &tcm) == tsc_bool_true) {
                            found = tmp;
                        }
                    } else {
                        found = tmp;
                    }
                }

                tmp = tmp->next;

                if (found) {
                    if (tsc_transaction_remove(handle, found) == tsc_error_code_ok) {
                        TSC_DEBUG("tsc_transaction_process_response: transaction removed [%p]", info);
                    } else {
                        TSC_ERROR("tsc_transaction_process_response: failed to remove transaction [%p]", info);
                    }
                }
            }

            return tsc_error_code_ok;
        } else {
            TSC_ERROR
                ("tsc_transaction_process_response: failed to decode response [%p]",
                 info);

            return tsc_error_code_error;
        }
    } else {
        TSC_ERROR("tsc_transaction_check: invalid handle [%p]", info);

        return tsc_error_code_error;
    }
}
