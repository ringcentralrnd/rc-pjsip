/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#ifndef __TSC_CSM_H__
#define __TSC_CSM_H__

#include "tsc_data.h"
#include "tsc_ssl.h"
#include "tsc_version.h"
#include "tsc_common.h"
#include "tsc_network.h"

#define TSC_CONNECT_TIMEOUT 5
#define TSC_NEGOTIATION_TIMEOUT 5
#define TSC_NEGOTIATION_RETRY_TIMEOUT 1
#define TSC_DTLS_NEGOTIATION_RETRY_TIMEOUT 5
#define TSC_PROXY_TIMEOUT 5
#define TSC_KEEPALIVE_PENDING_TIMEOUT 5
#define TSC_SERVICE_RESPONSE_PENDING_TIMEOUT 5
#define TSC_RELEASE_PENDING_TIMEOUT 3
#define TSC_STATS_DUMP_INTERVAL 30
#define TSC_FATAL_ERROR_TIMEOUT 1
#define TSC_NETWORK_CONFIG_CHANGE_TIMEOUT 5

typedef enum
{
    tsc_csm_proxy_response_error,
    tsc_csm_proxy_response_pending,
    tsc_csm_proxy_response_ok,
    tsc_csm_proxy_response_authenticate
} tsc_csm_proxy_response;

tsc_error_code tsc_csm_write_in_msg (tsc_csm_info * info, tsc_csm_msg * msg);
tsc_error_code tsc_csm_write_out_msg (tsc_csm_info * info, tsc_csm_msg * msg);
tsc_bool tsc_csm_send_config_request (tsc_csm_info * info,
                                      tsc_bool reconnect);
tsc_bool tsc_csm_send_release_request (tsc_csm_info * info, tsc_lock *lock);
tsc_bool tsc_csm_send_keepalive (tsc_csm_info * info);
tsc_bool tsc_csm_send_client_service_request (tsc_csm_info * info, void *opaque, 
                                              tsc_cm_client_service_request *service_request);
tsc_bool tsc_csm_process_config_response (tsc_csm_info * info, void *buf,
                                          size_t len);
tsc_bool tsc_csm_process_release_response (tsc_handle handle, tsc_transaction *transaction,
                                           tsc_cm *response);
tsc_bool tsc_csm_process_keepalive_response (tsc_handle handle, tsc_transaction *transaction,
                                             tsc_cm *response);
tsc_bool tsc_csm_process_client_service_response (tsc_handle handle, tsc_transaction *transaction,
                                                  tsc_cm *response);
tsc_bool tsc_csm_process_data_in (tsc_csm_info * info);

tsc_bool tsc_csm_send_http_connect (tsc_csm_info * info,
                                    tsc_bool authentication);

tsc_bool tsc_csm_release_tunnel (tsc_csm_info * info, tsc_lock *lock);

tsc_bool tsc_csm_finish_tunnel (tsc_csm_info * info);

tsc_csm_proxy_response tsc_csm_process_proxy_response (tsc_csm_info * info);

#endif /* __TSC_CSM_H__ */
