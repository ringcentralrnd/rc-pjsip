/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#include "tsc_ip.h"

tsc_error_code
tsc_ip_make (tsc_ip_address * src_addr, tsc_ip_address * dst_addr,
             uint8_t protocol, uint8_t * ip_header, uint8_t * data,
             uint8_t tos, uint8_t ip_version, uint32_t len)
{
    struct iphdr *ip = (struct iphdr *) ip_header;
    uint32_t sum = 0;

    uint32_t i;
    uint32_t hi;
    uint32_t lo;

    uint32_t cksum;

    ip->ihl = 5;
    ip->version = ip_version;
    ip->tos = tos;
    ip->tot_len = htons (TSC_IP_HEADER_SIZE + len);
    ip->id = htons (0x0);
    ip->frag_off = htons (0x4000);
    ip->ttl = 64;
    ip->protocol = protocol;    /* udp */
    ip->saddr = htonl (src_addr->address);
    ip->daddr = htonl (dst_addr->address);
    ip->check = 0;

    for (i = 0; i < 10; i++) {
        uint32_t val = ntohs (*((unsigned short *) ip + i));
        sum += val;
    }

    hi = (sum & 0xffff0000) >> 16;
    lo = (sum & 0x0000ffff);

    cksum = lo + hi;

    ip->check = htons (~cksum & 0xffff);

    return tsc_error_code_ok;
}

tsc_error_code
tsc_ip_parse (tsc_ip_address * src_addr, tsc_ip_address * dst_addr,
              uint8_t * protocol, uint8_t * ip_version, uint8_t * ip_header)
{
    struct iphdr *ip = (struct iphdr *) ip_header;

    src_addr->address = ntohl (ip->saddr);
    dst_addr->address = ntohl (ip->daddr);
    *protocol = ip->protocol;
    *ip_version = ip->version;

    return tsc_error_code_ok;
}

#ifdef TSC_LINUX
tsc_error_code
tsc_ip_get_if (tsc_ip_address * src_addr, char *device)
{
    int s = socket (AF_INET, SOCK_DGRAM, 0);

    tsc_error_code result = tsc_error_code_error;

    if (s != -1) {
        struct ifreq *ifr, *ifend;
        struct ifconf ifc;
        struct ifreq ifs[0x100];

        ifc.ifc_len = sizeof (ifs);
        ifc.ifc_req = ifs;

        if (ioctl (s, SIOCGIFCONF, &ifc) != -1) {
            ifend = ifs + (ifc.ifc_len / sizeof (struct ifreq));

            for (ifr = ifc.ifc_req; ifr < ifend; ifr++) {
                if (ifr->ifr_addr.sa_family == AF_INET) {

                    if (htonl (src_addr->address) ==
                        *((uint32_t *) ((uint8_t *) ifr->ifr_addr.sa_data +
                                        2))) {
                        if (device) {
                            strcpy (device, ifr->ifr_name);

                            result = tsc_error_code_ok;
                        }
                    }
                }
            }
        }

        close (s);
    }

    return result;

}

tsc_error_code
tsc_ip_get_addr (char *device, tsc_ip_address * addr)
{
    int s = socket (AF_INET, SOCK_DGRAM, 0);

    tsc_error_code result = tsc_error_code_error;

    if (s != -1) {
        struct ifreq *ifr, *ifend;
        struct ifconf ifc;
        struct ifreq ifs[0x100];

        ifc.ifc_len = sizeof (ifs);
        ifc.ifc_req = ifs;

        if (ioctl (s, SIOCGIFCONF, &ifc) != -1) {
            ifend = ifs + (ifc.ifc_len / sizeof (struct ifreq));

            for (ifr = ifc.ifc_req; ifr < ifend; ifr++) {
                if (ifr->ifr_addr.sa_family == AF_INET) {

                    if (!strcmp (device, ifr->ifr_name)) {
                        if (addr) {
                            addr->address =
                                htonl (*
                                       ((uint32_t *) ((uint8_t *) ifr->
                                                      ifr_addr.sa_data + 2)));

                            result = tsc_error_code_ok;
                        }
                    }
                }
            }
        }

        close (s);
    }

    return result;

}
#endif

tsc_error_code tsc_ip_get_if_addr (uint8_t itf, char *buffer)
{
    strcpy(buffer, "");

#ifdef TSC_WINDOWS
    uint8_t count = 0;

    tsc_error_code result = tsc_error_code_error;

    IP_ADAPTER_INFO *adapter_info;
    IP_ADAPTER_INFO *adapter = NULL;
    ULONG size = sizeof(IP_ADAPTER_INFO);
    adapter_info = (IP_ADAPTER_INFO *)malloc(sizeof(IP_ADAPTER_INFO));

    DWORD value = GetAdaptersInfo(adapter_info, &size);

    if (value == ERROR_BUFFER_OVERFLOW) {
        free((void *)adapter_info);
        adapter_info = (IP_ADAPTER_INFO *)malloc(size);
        value = NO_ERROR;
    }

    if (value == NO_ERROR) {
        if (GetAdaptersInfo(adapter_info, &size) == NO_ERROR) {
            adapter = adapter_info;

            while (adapter) {
                char *ip_string = adapter->IpAddressList.IpAddress.String;

                if (ip_string && count == itf) {
                    strcpy(buffer, ip_string);

                    result = tsc_error_code_ok;

                    break;
                }

                count++;

                adapter = adapter->Next;
            }
        }

	free((void *)adapter_info);
    }

    return result;
#else
    int sock = socket(AF_INET, SOCK_DGRAM, 0);

    struct ifconf ifc;
    struct ifreq *ifr, *last;

    uint8_t count = 0;
		
    if (sock > 0) {
        ifc.ifc_len = 1024*sizeof(struct ifreq);
        ifc.ifc_buf = (char *)malloc(ifc.ifc_len);

        if (ioctl(sock, SIOCGIFCONF, &ifc) < 0 || ifc.ifc_len < (int)sizeof(struct ifreq)) {
            free((void *)ifc.ifc_buf);

            close(sock);

            return tsc_error_code_error;
        }

        ifr = (struct ifreq *)ifc.ifc_req;
        last = (struct ifreq *)((char *)ifr+ifc.ifc_len);

        while (ifr < last) {
            if (! (ifr->ifr_flags & (IFF_UP | IFF_LOOPBACK)) && (ifr->ifr_addr.sa_family == AF_INET) &&
                strncmp(ifr->ifr_name, "lo", 2) && strncmp(ifr->ifr_name, "dummy", 5) &&
                !strchr(ifr->ifr_name, ':')) {
                if (count == itf) {
                    strcpy(buffer, "");

                    uint8_t i = 0;

                    for (i = 0; i < 4; i++) {
                        char aux[TSC_MAX_STR_LEN];
                        sprintf(aux, "%d", ((uint8_t)ifr->ifr_addr.sa_data[i+2] & 0xff));

                        if (strlen(buffer)) {
                            strcat(buffer, ".");
                        }

                        strcat(buffer, aux);					
                    }

                    break;
                }

		count++;
            }

#ifdef HAVE_SA_LEN
            ifr = (struct ifreq *)((char *)ifr + ifr->ifr_addr.sa_len + IFNAMSIZ);
#else
            ifr = (struct ifreq *)((char *)ifr + sizeof(struct ifreq));
#endif
        }

        free((void *)ifc.ifc_buf);

        close(sock);

	return tsc_error_code_ok;
    }

    free((void *)ifc.ifc_buf);

    close(sock);

    return tsc_error_code_error;
#endif
}

tsc_error_code tsc_ip_get_if_count (uint8_t *count)
{
#ifdef TSC_WINDOWS
    DWORD ifs;

    if (GetNumberOfInterfaces(&ifs) == NO_ERROR) {
        *count = ifs;

        return tsc_error_code_ok;
    }

    return tsc_error_code_error;
#else
    int sock = socket(AF_INET, SOCK_DGRAM, 0);

    if (sock > 0) {
        struct ifconf ifc;
        struct ifreq *ifr, *last;

        *count = 0;

        ifc.ifc_len = 1024*sizeof(struct ifreq);
        ifc.ifc_buf = (char *)malloc(ifc.ifc_len);

        if (ioctl(sock, SIOCGIFCONF, &ifc) < 0)	{
            free((void *)ifc.ifc_buf);

            close(sock);
        } else {
            if (ifc.ifc_len < (int)sizeof(struct ifreq)) {
                free((void *)ifc.ifc_buf);

                close(sock);
            } else {
                ifr = (struct ifreq *)ifc.ifc_req;
                last = (struct ifreq *)((char *)ifr+ifc.ifc_len);

                while (ifr < last) {
                    if (! (ifr->ifr_flags & (IFF_UP | IFF_LOOPBACK)) && (ifr->ifr_addr.sa_family == AF_INET) &&
                        strncmp(ifr->ifr_name, "lo", 2) && strncmp(ifr->ifr_name, "dummy", 5) &&
                        !strchr(ifr->ifr_name, ':')) {
                        (*count)++;
                    }

#ifdef HAVE_SA_LEN
                    ifr = (struct ifreq *)((char *)ifr + ifr->ifr_addr.sa_len + IFNAMSIZ);
#else
                    ifr = (struct ifreq *)((char *)ifr + sizeof(struct ifreq));
#endif
                }

		free((void *)ifc.ifc_buf);

		close(sock);

		return tsc_error_code_ok;
            }
        }
    }

    return tsc_error_code_error;
#endif    
}


