/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#include "tsc_tcp.h"

uint16_t
tsc_tcp_checksum (uint32_t * src, uint32_t * dst, uint8_t * buffer,
                  uint32_t len, tsc_bool padding) 
{
    uint16_t src_addr[4];
    uint16_t dst_addr[4];
    src_addr[0] = ((*src & 0xff000000) >> 24) & 0xff;
    src_addr[1] = ((*src & 0xff0000) >> 16) & 0xff;
    src_addr[2] = ((*src & 0xff00) >> 8) & 0xff;
    src_addr[3] = *src & 0xff;
    dst_addr[0] = ((*dst & 0xff000000) >> 24) & 0xff;
    dst_addr[1] = ((*dst & 0xff0000) >> 16) & 0xff;
    dst_addr[2] = ((*dst & 0xff00) >> 8) & 0xff;
    dst_addr[3] = *dst & 0xff;

    uint16_t padd = 0;
    if (padding == tsc_bool_true) {
        padd = 1;
        buffer[len] = 0;
    }
    uint32_t sum = 0;

    uint16_t tmp;

    int i = 0;
    
        /* make 16 bit words out of every two adjacent 8 bit words and 
           calculate the sum of all 16 vit words */ 
        for (i = 0; i < len + padd; i = i + 2) {
        tmp = ((buffer[i] << 8) & 0xff00) + (buffer[i + 1] & 0xff);
        sum = sum + tmp;
    }
    
        /* add the TCP pseudo header which contains: the IP source and
           destination addresses, */ 
        for (i = 0; i < 4; i = i + 2) {
        tmp = ((src_addr[i] << 8) & 0xff00) + (src_addr[i + 1] & 0xff);
        sum = sum + tmp;
    }
    for (i = 0; i < 4; i = i + 2) {
        tmp = ((dst_addr[i] << 8) & 0xff00) + (dst_addr[i + 1] & 0xff);
        sum = sum + tmp;
    }
    
        /* the protocol number and the length of the TCP packet */ 
        sum = sum + SOL_TCP + len;
    
        /* keep only the last 16 bits of the 32 bit calculated sum and add
           the carries */ 
        while (sum >> 16) {
        sum = (sum & 0xffff) + (sum >> 16);
    }
    
        /* Take the one's complement of sum */ 
        sum = htons (~sum);
    return ((uint16_t) sum);
}

tsc_error_code tsc_tcp_make (tsc_ip_port_address * src_addr,
                               tsc_ip_port_address * dst_addr,
                               uint8_t * tcp_header, uint8_t * data,
                               uint32_t len)
{
    struct tcphdr *tcp = (struct tcphdr *) tcp_header;

    tcp->source = htons (src_addr->port);
    tcp->dest = htons (dst_addr->port);
    tcp->check = htons (0);
    tcp->doff = 5;
    tcp->window = 0xffff;

    tsc_bool padding = tsc_bool_false;

    if ((len - (tcp->doff * 4)) % 2) {
        padding = tsc_bool_true;
    }

    tcp->check =
        tsc_tcp_checksum (&src_addr->address, &dst_addr->address, tcp_header,
                          TSC_TCP_HEADER_SIZE + len, padding);

    return tsc_error_code_ok;
}

tsc_error_code
tsc_tcp_parse (uint32_t * src_port, uint32_t * dst_port, uint8_t * tcp_header)
{
    struct tcphdr *tcp = (struct tcphdr *) tcp_header;

    *src_port = ntohs (tcp->source);
    *dst_port = ntohs (tcp->dest);

    return tsc_error_code_ok;
}
