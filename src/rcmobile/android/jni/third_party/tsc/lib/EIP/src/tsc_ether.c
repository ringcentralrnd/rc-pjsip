/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#include "tsc_ether.h"
#include "tsc_ip.h"

tsc_error_code
tsc_ether_make (tsc_ip_address * src_addr, tsc_ip_address * dst_addr,
                uint16_t ether_type, uint8_t * ether_hdr)
{
    struct ether_header *ether = (struct ether_header *) ether_hdr;

    ether->ether_type = htons (ether_type);

    char device[TSC_ADDR_STR_LEN];

    if (tsc_ip_get_if (src_addr, device) == tsc_error_code_ok) {
        if (tsc_resolve_arp (device, dst_addr, ether->ether_dhost) ==
            tsc_error_code_ok) {
            if (tsc_get_mac (device, ether->ether_shost) == tsc_error_code_ok) {
                return tsc_error_code_ok;
            }
        }
    }

    return tsc_error_code_error;
}

tsc_error_code
tsc_resolve_arp (char *device, tsc_ip_address * src_addr,
                 uint8_t * ether_addr)
{
    int s = socket (AF_INET, SOCK_DGRAM, 0);

    tsc_error_code result = tsc_error_code_error;

    if (s != -1) {
        struct arpreq req_arp;
        memset (&req_arp, 0, sizeof (struct arpreq));
        struct sockaddr_in *addr = (struct sockaddr_in *) &(req_arp.arp_pa);
        addr->sin_family = AF_INET;
        addr->sin_addr.s_addr = htonl (src_addr->address);

        strcpy (req_arp.arp_dev, device);

        tsc_bool sent = tsc_bool_false;

        for (;;) {
            if (ioctl (s, SIOCGARP, &req_arp) != -1) {
                uint32_t i;

                for (i = 0; i < ETHER_ADDR_LEN; i++) {
                    ether_addr[i] = ((uint8_t *) req_arp.arp_ha.sa_data)[i];
                }

                result = tsc_error_code_ok;

                break;
            }
            else if (sent == tsc_bool_true) {
                break;
            }
            else {
                /* send dummy echo request to induce ARP request */
                addr->sin_port = htons (7);

                sendto (s, "dummy", 4, 0, &(req_arp.arp_pa),
                        sizeof (struct sockaddr_in));

                /* wait for response */
                tsc_sleep (1000);

                sent = tsc_bool_true;
            }
        }

        close (s);
    }

    return result;
}

tsc_error_code
tsc_get_mac (char *device, uint8_t * ether_addr)
{
    int s = socket (AF_INET, SOCK_DGRAM, 0);

    tsc_error_code result = tsc_error_code_error;

    if (s != -1) {
        struct ifreq req;
        memset (&req, 0, sizeof (struct ifreq));
        strcpy (req.ifr_name, device);

        if (ioctl (s, SIOCGIFHWADDR, &req) != -1) {
            uint32_t i;

            for (i = 0; i < ETHER_ADDR_LEN; i++) {
                ether_addr[i] = ((uint8_t *) req.ifr_hwaddr.sa_data)[i];
            }

            result = tsc_error_code_ok;
        }

        close (s);
    }

    return result;
}

tsc_error_code
tsc_find_route (tsc_ip_address * address, char *device)
{
    int s = socket (AF_INET, SOCK_DGRAM, 0);

    tsc_error_code result = tsc_error_code_error;

    if (s != -1) {
        /* let's force ARP entry */
        struct sockaddr_in addr;
        memset (&addr, 0, sizeof (struct sockaddr_in));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = htonl (address->address);
        addr.sin_port = htons (7);
        sendto (s, "dummy", 4, 0, (struct sockaddr *) &addr,
                sizeof (struct sockaddr_in));
        /* wait for response */
        tsc_sleep (1000);

        struct ifreq *ifr, *ifend;
        struct ifconf ifc;
        struct ifreq ifs[0x100];

        ifc.ifc_len = sizeof (ifs);
        ifc.ifc_req = ifs;

        if (ioctl (s, SIOCGIFCONF, &ifc) != -1) {
            ifend = ifs + (ifc.ifc_len / sizeof (struct ifreq));

            for (ifr = ifc.ifc_req; ifr < ifend; ifr++) {
                if (ifr->ifr_addr.sa_family == AF_INET) {
                    strcpy (device, ifr->ifr_name);

                    struct arpreq req_arp;
                    memset (&req_arp, 0, sizeof (struct arpreq));
                    struct sockaddr_in *addr =
                        (struct sockaddr_in *) &(req_arp.arp_pa);
                    addr->sin_family = AF_INET;
                    addr->sin_addr.s_addr = htonl (address->address);

                    strcpy (req_arp.arp_dev, device);

                    if (ioctl (s, SIOCGARP, &req_arp) != -1) {
                        result = tsc_error_code_ok;

                        break;
                    }
                }
            }
        }

        close (s);
    }

    return result;
}
