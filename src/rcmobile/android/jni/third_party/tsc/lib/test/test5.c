/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 * it is used in SDK testing
 *
 */

#include "tsc_control_api.h"
#include "tsc_socket_api.h"

int
main (int argc, char **argv)
{
	tsc_tunnel_params tunnel_params;

	uint32_t address;
	tsc_handle tunnel;
	
    tsc_config config;
	char str[TSC_ADDR_STR_LEN];
	int socket;
	unsigned long flags = 1;
	struct sockaddr_in addr;
	struct sockaddr accept_addr;
    int addrlen;
    tsc_ctrl_init ();

    tsc_set_log_level (tsc_log_level_trace);

    
    tunnel_params.connection_params[0].server_address.port = 7000;
    
    tsc_inet_pton (AF_INET, "182.168.31.11", &address);
    tunnel_params.connection_params[0].server_address.address = ntohl (address);

    tunnel_params.max_connections = 1;

    tunnel = tsc_ctrl_new_tunnel (&tunnel_params, NULL);

    if (!tunnel) {
        printf ("failed to access tunnel\n");

        exit (0);
    }

    tsc_get_config (tunnel, &config);

    
    tsc_ip_address_to_str (&(config.internal_address), str, TSC_ADDR_STR_LEN);
    printf ("\ninternal ip address => %s\n", str);
    tsc_ip_port_address_to_str (&(config.sip_server), str, TSC_ADDR_STR_LEN);
    printf ("\nsip_server => %s\n", str);

    socket = tsc_socket (tunnel, AF_INET, SOCK_STREAM, 0);

    printf ("created socket %d\n", socket);
	   
    tsc_ioctl (socket, FIONBIO, &flags);
	    
    memset (&addr, 0, sizeof (struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = htons (5060);
    addr.sin_addr.s_addr = htonl (config.internal_address.address);

    if (tsc_bind
        (socket, (struct sockaddr *) (&addr), sizeof (struct sockaddr_in))) {
        printf ("cannot bind socket");
        exit (0);
    }

    if (tsc_listen (socket, 1)) {
        printf ("cannot listen");
        exit (0);
    }

   

    for (;;) {
        socket = tsc_accept (socket, &accept_addr, &addrlen);

        if (socket == -1) {
            if (errno != EAGAIN) {
                printf ("failed to accept");
                exit (0);
            }
        }
        else {
            break;
        }
    }

    printf ("accepted %X:%d\n",
            ntohl (((struct sockaddr_in *) &accept_addr)->sin_addr.s_addr),
            ntohs (((struct sockaddr_in *) &accept_addr)->sin_port));


    for (;;) {
        struct tsc_timeval timeout;
		tsc_fd_set read_flags;
		int res;
        timeout.tv_sec = 0;
        timeout.tv_usec = 0;
		       
        TSC_FD_ZERO (&read_flags);
        TSC_FD_SET (socket, &read_flags);

        res = tsc_select (socket + 1, &read_flags, NULL, NULL, &timeout);

        if (res > 0) {
            char data[TSC_MAX_FRAME_SIZE];
            uint32_t size = TSC_MAX_FRAME_SIZE;

            int res = tsc_recv (socket, data, size, 0);

            printf ("data ready! %d\n", res);

            data[res] = 0;

            if (res == -1) {
                if (tsc_get_errno () == EWOULDBLOCK) {
                    printf ("EWOULDBLOCK\n");
                }
            }
            else {
                printf ("\nreceived data\n\n%s", data);

                break;
            }
        }
    }

    tsc_close (socket);

    getchar ();

    tsc_delete_tunnel (tunnel);

    return 0;
}
