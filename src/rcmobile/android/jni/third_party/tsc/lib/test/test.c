/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 * it is used in SDK testing
 *
 */

#include "tsc_tunnel.h"
#include "tsc_socket_api.h"

int
main (int argc, char **argv)
{
	tsc_tunnel_params tunnel_params;
	tsc_requested_config req_config;
	uint32_t address;
	tsc_handle tunnel;
	char *invite;
	tsc_config config;
	char str[TSC_ADDR_STR_LEN];
	int socket;
	uint8_t data[TSC_MAX_FRAME_SIZE];
    uint32_t size = TSC_MAX_FRAME_SIZE;
	tsc_ip_port_address src;

    tsc_ip_port_address src_addr;
    tsc_ip_port_address dst_addr;

    tsc_init ();

    tsc_set_log_level (tsc_log_level_disabled);

    
    /* Initialize tunnel parameters structure*/
    memset (&tunnel_params, 0, sizeof (tsc_tunnel_params));
    

    tunnel_params.connection_params[0].server_address.port = 7000;
    
    tsc_inet_pton (AF_INET, "182.168.31.11", &address);
    tunnel_params.connection_params[0].server_address.address = ntohl (address);
    tunnel_params.connection_params[0].transport = tsc_transport_tcp;

    tunnel_params.max_connections = 1;

    tunnel = tsc_new_tunnel (&tunnel_params, &req_config);

    for (;;) {
        tsc_state_info state_info;
        tsc_get_state (tunnel, &state_info);

        if (state_info.state == tsc_state_established) {
            break;
        }
        else if (state_info.state == tsc_state_fatal_error) {
            printf ("failed to establish tunnel\n");

            tsc_delete_tunnel (tunnel);

            exit (0);
        }
    }

    invite =
        (char *)
        "INVITE sip:222222@acme.com:5060 SIP/2.0\nVia: SIP/2.0/UDP 182.168.31.40:5060;branch=1\nFrom: 111111 <sip:111111@acme.com>;tag=_ph1_tag\nTo: 222222 <sip:222222@acme.com>\nCall-ID: _1-2_call_id-12986@acme.com-1-\nCSeq: 1 INVITE\nContact: sip:111111@182.168.31.40:5060\nMax-Forwards: 70\nSubject: TBD\nContent-Type: application/sdp\nContent-Length: 131\n\nv=0\no=user1 53655765 2353687637 IN IP4 182.168.31.40\ns=-\nc=IN IP4 182.168.31.40\nt=0 0\nm=audio 10000 RTP/AVP 0\na=rtpmap:0 PCMU/8000\n";

    
    tsc_get_config (tunnel, &config);

    
    tsc_ip_address_to_str (&(config.internal_address), str, TSC_ADDR_STR_LEN);
    printf ("\ninternal ip address => %s\n", str);
    tsc_ip_port_address_to_str (&(config.sip_server), str, TSC_ADDR_STR_LEN);
    printf ("\nsip_server => %s\n", str);

    
    src.address = config.internal_address.address;
    src.port = 5060;

    socket = tsc_socket (tunnel, AF_INET, SOCK_DGRAM, 0);

    printf ("created socket %d\n", socket);

    tsc_output_option option;
    option.realtime = tsc_bool_true;

    tsc_send_udp_data (tunnel, &src, &(config.sip_server), invite,
                       strlen (invite), &option);

    while (tsc_recv_udp_data (tunnel, &src_addr, &dst_addr, data, &size) ==
           tsc_error_code_no_data);

    tsc_ip_port_address_to_str (&src_addr, str, TSC_ADDR_STR_LEN);

    printf ("\nreceived data from %s\n\n%s", str, data);

    getchar ();

    tsc_delete_tunnel (tunnel);

    return 0;
}
