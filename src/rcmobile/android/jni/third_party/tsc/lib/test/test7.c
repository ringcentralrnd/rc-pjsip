/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 * it is used in SDK testing
 *
 */

#include "tsc_control_api.h"
#include "tsc_socket_api.h"

#include "../OSAA/include/tsc_raw.h"

int
main (int argc, char **argv)
{
#ifdef TSC_FTCP
    char src_address_str[0x100];
    char dst_address_str[0x100];

    strcpy(src_address_str, "");
    strcpy(dst_address_str, "");

    int src_port = 0;
    int dst_port = 0;

    uint32_t i;

    for (i = 1; i < argc; i++) {
        if (!strcmp (argv[i], "-si")) {
            strcpy (src_address_str, argv[++i]);
        }
        else if (!strcmp (argv[i], "-di")) {
            strcpy (dst_address_str, argv[++i]);
        }
        else if (!strcmp (argv[i], "-sp")) {
            src_port = atoi (argv[++i]);
        }
        else if (!strcmp (argv[i], "-dp")) {
            dst_port = atoi (argv[++i]);
        }
    }

    if (!src_port || !dst_port || !strlen(src_address_str) || !strlen(dst_address_str)) {
        printf("usage: test7 -si source_ip -sp source_port -di dest_ip -dp dest_port\n");

        exit(0);
    }

    printf("frame from %s:%d -> %s:%d\n", src_address_str, src_port, dst_address_str, dst_port);

    uint32_t src_address;
    tsc_inet_pton(AF_INET, src_address_str, &src_address);

    uint32_t dst_address;
    tsc_inet_pton(AF_INET, dst_address_str, &dst_address);

    tsc_ip_address src;
    src.address = ntohl(src_address);

    char device[0x100];
    if (tsc_ip_get_if(&src, device) == tsc_error_code_error) {
        printf("failed to find device\n");

        exit(0);
    } else {
        printf("%s is in device %s\n", src_address_str, device);    }


    tsc_raw *raw_if = tsc_raw_open(device, NULL);

    if (raw_if) {
        char *invite = (char *)
        "INVITE sip:222222@acme.com:5060 SIP/2.0\nVia: SIP/2.0/UDP 182.168.31.40:5060;branch=1\nFrom: 111111 <sip:111111@acme.com>;tag=_ph1_tag\nTo: 222222 <sip:222222@acme.com>\nCall-ID: _1-2_call_id-12986@acme.com-1-\nCSeq: 1 INVITE\nContact: sip:111111@182.168.31.40:5060\nMax-Forwards: 70\nSubject: TBD\nContent-Type: application/sdp\nContent-Length: 131\n\nv=0\no=user1 53655765 2353687637 IN IP4 182.168.31.40\ns=-\nc=IN IP4 182.168.31.40\nt=0 0\nm=audio 10000 RTP/AVP 0\na=rtpmap:0 PCMU/8000\n";

        tsc_buffer buffer;
        buffer.len = strlen(invite);
        memcpy(buffer.data, invite, buffer.len);

        tsc_ip_port_address src_addr;
        tsc_ip_port_address dst_addr;

        src_addr.address = ntohl(src_address);
        src_addr.port = src_port;
        dst_addr.address = ntohl(dst_address);
        dst_addr.port = dst_port;

        tsc_raw_ftcp_send(raw_if, &src_addr, &dst_addr, &buffer);
    } else {
        printf("failed to get raw device\n");

        exit(0);
    }
#endif /* TSC_FTCP */

    return 0;
}
