/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/
/*! \brief
low level tunnel API
*/

/** @file
 * This file is part of TSC Client API
 * and defines low level tunnel API
 *
 */

#ifndef __TSC_VQ_H__
#define __TSC_VQ_H__

#include "tsc_qos.h"

#include "tsc_lock.h"
#include "tsc_thread.h"

#include "tsc_socket_api.h"

#ifdef TSC_WINDOWS
#define TSC_RANDOM(x) (int32_t)((long)rand()*x/RAND_MAX)
#else
#define TSC_RANDOM(x) (int32_t)((long long)rand()*x/RAND_MAX)
#endif

#define TSC_QOS_MAX_PLAYOUT_BUFFERS 10

#define	TSC_TRANSCODING_SIGN_BIT 0x80
#define	TSC_TRANSCODING_QUANT_MASK 0xf
#define	TSC_TRANSCODING_NSEGS 8
#define	TSC_TRANSCODING_SEG_SHIFT 4
#define	TSC_TRANSCODING_SEG_MASK 0x70
#define	TSC_TRANSCODING_BIAS 0x84

#define TSC_QOS_LAST_PACKET_TIMEOUT 5

typedef struct
{
    int32_t socket;
    uint8_t pt;
    uint32_t pcm_ptr;
    uint16_t seq;
    uint32_t ssrc;
    uint32_t ts;
} tsc_qos_rtp_tx;

typedef struct
{
    int32_t socket;
    uint16_t seq;
    tsc_bool active;
} tsc_qos_rtp_rx;

typedef struct
{
    tsc_thread *read;
    tsc_thread *write;
    tsc_bool end;
    tsc_lock *data_lock;
    tsc_qos_state state;
    tsc_qos_input input;
    tsc_qos_output output;
    tsc_handle tunnel_handle;
    tsc_qos_rtp_rx rtp_rx;
    tsc_qos_rtp_tx rtp_tx;
} tsc_qos_info;

uint8_t pcm_to_ulaw (int16_t pcm);
uint16_t ulaw_to_pcm (int8_t ulaw);

tsc_bool tsc_qos_finish_threads (tsc_qos_info * info);

#endif /* __TSC_VQ_H__ */
