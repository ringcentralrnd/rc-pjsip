/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#define TSC_LOG_SUBSYSTEM TSC_LOG_SUBSYSTEM_QOS

#include "tsc_vq.h"

double calculate_pesq (char *ref_filename, char *filename, int rate);

void *
qos_read (void *arg)
{
    tsc_qos_info *info = (tsc_qos_info *) arg;

    TSC_DEBUG ("qos_read: thread started [%p]", info->tunnel_handle);

    uint8_t *playout_buffer[TSC_QOS_MAX_PLAYOUT_BUFFERS];
    uint32_t playout_seq[TSC_QOS_MAX_PLAYOUT_BUFFERS];
    uint32_t playout_level = 0;

    uint32_t i;

    for (i = 0; i < TSC_QOS_MAX_PLAYOUT_BUFFERS; i++) {
        playout_buffer[i] =
            (uint8_t *) malloc (sizeof (uint8_t) * TSC_MAX_FRAME_SIZE);
        memset (playout_buffer[i], 0, sizeof (uint8_t) * TSC_MAX_FRAME_SIZE);
        playout_seq[i] = 0;
    }

    time_t last_packet = tsc_time ();

    uint32_t played_packets = 0;

    for (;;) {
        uint32_t delay = 20;

        if (tsc_lock_get (info->data_lock) == tsc_lock_response_ok) {
            if (info->end == tsc_bool_true) {
                tsc_lock_release (info->data_lock);

                break;
            }

            if (info->state == tsc_qos_state_stopped) {
                TSC_DEBUG ("qos_read: qos is now running [%p]",
                           info->tunnel_handle);

                info->state = tsc_qos_state_running;

                info->rtp_rx.socket = info->rtp_tx.socket =
                    tsc_socket (info->tunnel_handle, AF_INET, SOCK_DGRAM, 0);

                srand (tsc_time ());

                if (info->rtp_tx.socket != -1) {
                    TSC_DEBUG ("qos_read: rtp socket %d created [%p]",
                               info->rtp_tx.socket, info->tunnel_handle);

                    tsc_config config;

                    if (tsc_get_config (info->tunnel_handle, &config) ==
                        tsc_error_code_error) {
                        tsc_close (info->rtp_tx.socket);

                        TSC_ERROR
                            ("qos_read: failed to retrieve config %d [%p]",
                             info->rtp_tx.socket, info->tunnel_handle);

                        info->state = tsc_qos_state_failure;
                    }

                    struct sockaddr_in rtp_addr;
                    memset (&rtp_addr, 0, sizeof (struct sockaddr_in));
                    rtp_addr.sin_family = AF_INET;
                    rtp_addr.sin_port = htons (info->input.local_rtp_port);
                    rtp_addr.sin_addr.s_addr =
                        htonl (config.internal_address.address);

                    if (!tsc_bind
                        (info->rtp_tx.socket, (struct sockaddr *) (&rtp_addr),
                         sizeof (struct sockaddr_in))) {
                        /* mulaw pt = 0 */
                        info->rtp_tx.pt = 0;
                        info->rtp_tx.ssrc = TSC_RANDOM (0xffffffff);
                        info->rtp_tx.seq = 0;
                        info->rtp_tx.ts = 0;
                        info->rtp_tx.pcm_ptr = 0;

                        info->rtp_rx.active = tsc_bool_false;
                        info->rtp_rx.seq = 0;

                        info->output.pcm_len = 0;
                    }
                    else {
                        tsc_close (info->rtp_tx.socket);

                        TSC_ERROR
                            ("qos_read: failed to bind RTP socket %d [%p]",
                             info->rtp_tx.socket, info->tunnel_handle);

                        info->state = tsc_qos_state_failure;
                    }

                }
                else {
                    TSC_ERROR ("qos_read: failed to create RTP socket [%p]",
                               info->tunnel_handle);

                    info->state = tsc_qos_state_failure;
                }
            }
            else if (info->state == tsc_qos_state_running) {
                delay = info->input.pcm_frame_size * 1000 / 8000;       /* ms 
                                                                         */

                for (;;) {
                    struct tsc_timeval timeout;
                    timeout.tv_sec = 0;
                    timeout.tv_usec = 0;

                    tsc_fd_set read_flags;
                    TSC_FD_ZERO (&read_flags);
                    TSC_FD_SET (info->rtp_rx.socket, &read_flags);

                    int result =
                        tsc_select (info->rtp_rx.socket + 1, &read_flags,
                                    NULL, NULL, &timeout);

                    if (result > 0) {
                        uint8_t data[TSC_MAX_FRAME_SIZE];
                        uint32_t size = TSC_MAX_FRAME_SIZE;

                        struct sockaddr from;
                        socklen_t fromlen;

                        uint16_t seq = 0;

                        int len =
                            tsc_recvfrom (info->rtp_rx.socket, (char *) data,
                                          size, 0, &from, &fromlen);

                        if (len > 0) {
                            last_packet = tsc_time ();

                            char addr[INET_ADDRSTRLEN];
                            tsc_inet_ntop (AF_INET,
                                           &(((struct sockaddr_in *) &from)->
                                             sin_addr), addr,
                                           INET_ADDRSTRLEN);

                            seq = data[2] << 8 | data[3];

                            TSC_DEBUG
                                ("qos_read: received %d bytes from %s:%d (rtp seq %d)\n",
                                 len, addr,
                                 ntohs (((struct sockaddr_in *) &from)->
                                        sin_port), seq);

                            info->output.rtp_buf_ts[seq] = tsc_get_clock ();

                            int32_t j = 0;

                            for (j = 0; j < playout_level; j++) {
                                if (playout_seq[j] < seq) {
                                    break;
                                }
                            }

                            if (j < playout_level) {
                                int32_t i = 0;

                                for (i = playout_level - 1; i >= j; i--) {
                                    memcpy (playout_buffer[i + 1],
                                            playout_buffer[i],
                                            sizeof (uint8_t) *
                                            TSC_MAX_FRAME_SIZE);
                                    playout_seq[i + 1] = playout_seq[i];
                                }
                            }

                            memcpy (playout_buffer[j], data + 16, len - 16);
                            playout_seq[j] = seq;

                            if (playout_level <
                                TSC_QOS_MAX_PLAYOUT_BUFFERS - 1) {
                                playout_level++;
                            }
                        }
                    }
                    else {
                        break;
                    }
                }

                if (tsc_time () > last_packet + TSC_QOS_LAST_PACKET_TIMEOUT) {
                    char ref_filename[TSC_ADDR_STR_LEN];
                    sprintf (ref_filename, "src%d.pcm", tsc_get_clock ());
                    FILE *file_write = fopen (ref_filename, "wb");
                    if (file_write) {
                        fwrite (info->input.pcm_buffer, sizeof (int16_t),
                                info->input.pcm_len, file_write);
                        fclose (file_write);
                    }

                    char filename[TSC_ADDR_STR_LEN];
                    sprintf (filename, "dst%d.pcm", tsc_get_clock ());
                    file_write = fopen (filename, "wb");
                    if (file_write) {
                        fwrite (info->output.pcm_buffer, sizeof (int16_t),
                                info->output.pcm_len, file_write);
                        fclose (file_write);
                    }

                    info->output.pesq =
                        calculate_pesq (ref_filename, filename, 8000);

                    remove (ref_filename);
                    remove (filename);

                    info->state = tsc_qos_state_done;

                    uint32_t playout_latency = 0;
                    uint32_t network_latency = 0;

                    uint32_t i;

                    uint32_t playout_total = 0;
                    uint32_t network_total = 0;

                    uint32_t lost_packets = 0;

                    for (i = 0;
                         i < info->input.pcm_len / info->input.pcm_frame_size;
                         i++) {
                        if (info->output.rtp_buf_ts[i]) {
                            network_latency +=
                                info->output.rtp_buf_ts[i] -
                                info->output.rtp_out_ts[i];
                            network_total++;
                        }
                        else {
                            if (info->output.rtp_out_ts[i]) {
                                lost_packets++;
                            }
                        }

                        if (info->output.rtp_in_ts[i]) {
                            playout_latency +=
                                info->output.rtp_in_ts[i] -
                                info->output.rtp_out_ts[i];
                            playout_total++;
                        }
                    }

                    if (network_total > 0) {
                        network_latency /= network_total;
                    }

                    if (playout_total > 0) {
                        playout_latency /= playout_total;
                    }

                    info->output.network_latency = network_latency;
                    info->output.playout_latency = playout_latency;

                    info->output.network_loss =
                        100.0 * (double) lost_packets /
                        (double) (info->input.pcm_len /
                                  info->input.pcm_frame_size);

                    info->output.playout_loss =
                        100.0 * (1 +
                                 info->input.pcm_len /
                                 info->input.pcm_frame_size -
                                 played_packets) /
                        (double) (info->input.pcm_len /
                                  info->input.pcm_frame_size);

                    if (info->output.network_loss > 100) {
                        info->output.network_loss = 100;
                    }

                    if (info->output.playout_loss > 100) {
                        info->output.playout_loss = 100;
                    }

                    TSC_DEBUG
                        ("qos_read: PESQ: %f, network_latency: %dms, playout_latency: %dms, network_loss: %f%%, playout_loss: %f%%\n",
                         info->output.pesq, info->output.network_latency,
                         info->output.playout_latency,
                         info->output.network_loss,
                         info->output.playout_loss);
                }

                if (info->rtp_rx.active == tsc_bool_false) {
                    if (playout_level > info->input.playout_buffer_size) {
                        info->rtp_rx.active = tsc_bool_true;
                        info->rtp_rx.seq = playout_seq[playout_level - 1];
                    }
                }

                tsc_bool found = tsc_bool_false;

                if (info->rtp_rx.active == tsc_bool_true) {
                    while (playout_level > 0) {
                        if (playout_seq[playout_level - 1] ==
                            info->rtp_rx.seq) {
                            found = tsc_bool_true;

                            break;
                        }
                        else if (playout_seq[playout_level - 1] <
                                 info->rtp_rx.seq) {
                            playout_level--;

                            if (!playout_level) {
                                info->rtp_rx.active = tsc_bool_false;
                            }
                            /* playout_seq[playout_level-1] >
                               info->rtp_rx.seq */
                        }
                        else {
                            break;
                        }
                    }
                }

                uint8_t speech[TSC_MAX_FRAME_SIZE];

                uint32_t pcm_frame_size = info->input.pcm_frame_size;

                if (found == tsc_bool_true) {
                    memcpy (speech, playout_buffer[playout_level - 1],
                            pcm_frame_size * sizeof (uint8_t));

                    info->output.rtp_in_ts[info->rtp_rx.seq] =
                        tsc_get_clock ();

                    if (info->output.rtp_buf_ts[info->rtp_rx.seq] -
                        info->output.rtp_out_ts[info->rtp_rx.seq] >
                        info->input.playout_buffer_size *
                        info->input.pcm_frame_size * 1000 / 8000) {
                        found = tsc_bool_false;
                    }
                    else {
                        played_packets++;
                    }
                }

                if (info->output.pcm_len < info->input.pcm_len * 2) {
                    uint32_t i = 0;

                    for (i = 0; i < pcm_frame_size; i++) {
                        if (found == tsc_bool_true) {
                            info->output.pcm_buffer[info->output.pcm_len++] =
                                ulaw_to_pcm (speech[i]);
                        }
                        else {
                            info->output.pcm_buffer[info->output.pcm_len++] =
                                0;
                        }
                    }

                    info->rtp_rx.seq++;
                }
            }

            tsc_lock_release (info->data_lock);
        }

        tsc_sleep (delay);
    }


    TSC_DEBUG ("qos_read: thread terminated [%p]", info->tunnel_handle);

    return 0L;
}

void *
qos_write (void *arg)
{
    tsc_qos_info *info = (tsc_qos_info *) arg;

    TSC_DEBUG ("qos_write: thread started [%p]", info->tunnel_handle);

    for (;;) {
        uint32_t delay = 20;

        if (tsc_lock_get (info->data_lock) == tsc_lock_response_ok) {
            if (info->end == tsc_bool_true) {
                tsc_lock_release (info->data_lock);

                break;
            }

            if (info->state == tsc_qos_state_running) {
                uint8_t frame[TSC_MAX_FRAME_SIZE];
                memset (frame, 0, TSC_MAX_FRAME_SIZE * sizeof (uint8_t));

                frame[0] = 0x80;

                if (!info->rtp_tx.seq) {
                    frame[1] |= 0x80;
                }

                frame[1] |= info->rtp_tx.pt;
                frame[2] = (info->rtp_tx.seq & 0xff00) >> 8;
                frame[3] = (info->rtp_tx.seq & 0xff);

                frame[4] = (info->rtp_tx.ts & 0xff000000) >> 24;
                frame[5] = (info->rtp_tx.ts & 0xff0000) >> 16;
                frame[6] = (info->rtp_tx.ts & 0xff00) >> 8;
                frame[7] = (info->rtp_tx.ts & 0xff);

                frame[8] = (info->rtp_tx.ssrc & 0xff000000) >> 24;
                frame[9] = (info->rtp_tx.ssrc & 0xff0000) >> 16;
                frame[10] = (info->rtp_tx.ssrc & 0xff00) >> 8;
                frame[11] = (info->rtp_tx.ssrc & 0xff);

                frame[12] = (info->rtp_tx.ssrc & 0xff000000) >> 24;
                frame[13] = (info->rtp_tx.ssrc & 0xff0000) >> 16;
                frame[14] = (info->rtp_tx.ssrc & 0xff00) >> 8;
                frame[15] = (info->rtp_tx.ssrc & 0xff);

                int16_t *pcm_buffer = info->input.pcm_buffer;
                uint32_t pcm_len = info->input.pcm_len;
                uint32_t pcm_frame_size = info->input.pcm_frame_size;

                uint32_t pcm_ptr = info->rtp_tx.pcm_ptr;

                delay = pcm_frame_size * 1000 / 8000;   /* ms */

                if (pcm_ptr < pcm_len) {
                    uint32_t i;

                    for (i = 0; i < pcm_frame_size; i++) {
                        frame[16 + i] = pcm_to_ulaw (pcm_buffer[pcm_ptr++]);
                    }

                    tsc_config config;

                    if (tsc_get_config (info->tunnel_handle, &config) ==
                        tsc_error_code_error) {
                        tsc_close (info->rtp_tx.socket);

                        TSC_ERROR
                            ("qos_write: failed to retrieve config %d [%p]",
                             info->rtp_tx.socket, info->tunnel_handle);

                        info->state = tsc_qos_state_failure;
                    }
                    else {
                        struct sockaddr_in rtp_addr;
                        memset (&rtp_addr, 0, sizeof (struct sockaddr_in));
                        rtp_addr.sin_family = AF_INET;
                        rtp_addr.sin_port =
                            htons (info->input.local_rtp_port);
                        rtp_addr.sin_addr.s_addr =
                            htonl (config.internal_address.address);

                        info->output.rtp_out_ts[info->rtp_tx.seq] =
                            tsc_get_clock ();

                        /* sending UDP RTP */
                        if (tsc_sendto
                            (info->rtp_tx.socket, (char *) frame,
                             pcm_frame_size + 16, 0,
                             (struct sockaddr *) (&rtp_addr),
                             sizeof (struct sockaddr_in)) > 0) {
                            info->rtp_tx.pcm_ptr = pcm_ptr;
                            info->rtp_tx.seq++;
                            info->rtp_tx.ts += pcm_frame_size;
                        }
                    }
                }
            }

            tsc_lock_release (info->data_lock);
        }

        tsc_sleep (delay);
    }

    TSC_DEBUG ("qos_write: thread terminated [%p]", info->tunnel_handle);

    return 0L;
}

tsc_qos_handle
tsc_new_qos (tsc_handle handle, tsc_qos_input * input)
{
    if (!input) {
        TSC_ERROR ("tsc_new_qos: input informantion not set [%p]", handle);

        return NULL;
    }

    if (input->codec != tsc_qos_codec_ulaw) {
        TSC_ERROR ("tsc_new_qos: only G.711 ulaw allowed [%p]", handle);

        return NULL;
    }

    if (input->playout_buffer_size >= TSC_QOS_MAX_PLAYOUT_BUFFERS) {
        TSC_ERROR ("tsc_new_qos: playout buffer size is invalid [%p]",
                   handle);

        return NULL;
    }

    tsc_qos_info *qos_handle =
        (tsc_qos_info *) malloc (sizeof (tsc_qos_info));

    if (!qos_handle) {
        TSC_ERROR ("tsc_new_qos: failed to allocate qos [%p]", handle);

        return NULL;
    }

    memset (qos_handle, 0, sizeof (tsc_qos_info));

    memcpy (&(qos_handle->input), input, sizeof (tsc_qos_input));

    qos_handle->state = tsc_qos_state_stopped;

    qos_handle->end = tsc_bool_false;

    qos_handle->tunnel_handle = handle;

    qos_handle->data_lock = tsc_lock_new ();

    if (!qos_handle->data_lock) {
        TSC_ERROR ("tsc_new_qos: failed to allocate end lock [%p]", handle);
        free ((void *) qos_handle);

        return NULL;
    }

    qos_handle->read = tsc_thread_new ((void *) qos_read, qos_handle);

    if (!qos_handle->read) {
        TSC_ERROR ("tsc_new_qos: failed to allocate read thread [%p]",
                   handle);
        tsc_lock_delete (qos_handle->data_lock);
        free ((void *) qos_handle);

        return NULL;
    }

    qos_handle->write = tsc_thread_new ((void *) qos_write, qos_handle);

    if (!qos_handle->write) {
        TSC_ERROR ("tsc_new_qos: failed to allocate write thread [%p]",
                   handle);
        tsc_thread_finish (qos_handle->read);
        tsc_thread_delete (qos_handle->read);
        tsc_lock_delete (qos_handle->data_lock);
        free ((void *) qos_handle);

        return NULL;
    }

    qos_handle->output.pcm_len = qos_handle->input.pcm_len * 2;
    qos_handle->output.pcm_buffer =
        (int16_t *) malloc (sizeof (int16_t) * qos_handle->output.pcm_len);

    qos_handle->output.rtp_in_ts =
        (uint32_t *) malloc (sizeof (uint32_t) * qos_handle->output.pcm_len /
                             qos_handle->input.pcm_frame_size);
    memset (qos_handle->output.rtp_in_ts, 0,
            sizeof (uint32_t) * qos_handle->output.pcm_len /
            qos_handle->input.pcm_frame_size);

    qos_handle->output.rtp_buf_ts =
        (uint32_t *) malloc (sizeof (uint32_t) * qos_handle->output.pcm_len /
                             qos_handle->input.pcm_frame_size);
    memset (qos_handle->output.rtp_buf_ts, 0,
            sizeof (uint32_t) * qos_handle->output.pcm_len /
            qos_handle->input.pcm_frame_size);

    qos_handle->output.rtp_out_ts =
        (uint32_t *) malloc (sizeof (uint32_t) * qos_handle->output.pcm_len /
                             qos_handle->input.pcm_frame_size);
    memset (qos_handle->output.rtp_out_ts, 0,
            sizeof (uint32_t) * qos_handle->output.pcm_len /
            qos_handle->input.pcm_frame_size);

    TSC_DEBUG ("tsc_new_qos: qos state machine created [%p]", handle);

    return qos_handle;
}

tsc_bool
tsc_qos_finish_threads (tsc_qos_info * info)
{
    if (tsc_lock_get (info->data_lock) == tsc_lock_response_ok) {
        info->end = tsc_bool_true;

        tsc_lock_release (info->data_lock);

        return tsc_bool_true;
    }
    else {
        TSC_ERROR ("tsc_qos_finish_threads: failed to get lock [%p]",
                   info->tunnel_handle);
    }

    return tsc_bool_false;
}

tsc_error_code
tsc_delete_qos (tsc_qos_handle handle)
{
    if (!handle) {
        TSC_ERROR ("tsc_delete_qos: wrong qos handle [%p]", handle);

        return tsc_error_code_error;
    }

    tsc_qos_info *info = (tsc_qos_info *) handle;

    if (tsc_qos_finish_threads (info) == tsc_bool_false) {
        TSC_ERROR ("tsc_delete_qos: failed to finish threads [%p]",
                   info->tunnel_handle);

        return tsc_error_code_error;
    }

    free ((void *) info->output.rtp_out_ts);
    free ((void *) info->output.rtp_in_ts);
    free ((void *) info->output.rtp_buf_ts);

    free ((void *) info->output.pcm_buffer);
    tsc_thread_finish (info->write);
    tsc_thread_delete (info->write);
    tsc_thread_finish (info->read);
    tsc_thread_delete (info->read);
    tsc_lock_delete (info->data_lock);
    free ((void *) info);

    return tsc_error_code_ok;
}

int32_t
law_search (int32_t val, int16_t * table, uint32_t size)
{
    uint32_t i;

    for (i = 0; i < size; i++) {
        if (val <= *table++) {
            return i;
        }
    }

    return size;
}

uint8_t
pcm_to_ulaw (int16_t pcm)
{
    int32_t mask;
    int32_t seg;
    uint8_t uval;
    int16_t seg_end[0x8] =
        { 0xFF, 0x1FF, 0x3FF, 0x7FF, 0xFFF, 0x1FFF, 0x3FFF, 0x7FFF };

    if (pcm < 0) {
        pcm = TSC_TRANSCODING_BIAS - pcm;
        mask = 0x7F;
    }
    else {
        pcm += TSC_TRANSCODING_BIAS;
        mask = 0xFF;
    }



    seg = law_search (pcm, seg_end, 8);

    if (seg >= 8) {
        return (0x7F ^ mask);
    }
    else {
        uval = (seg << 4) | ((pcm >> (seg + 3)) & 0xF);

        return (uval ^ mask);
    }
}

uint16_t
ulaw_to_pcm (int8_t ulaw)
{
    int32_t t;
    ulaw = ~ulaw;

    t = ((ulaw & TSC_TRANSCODING_QUANT_MASK) << 3) + TSC_TRANSCODING_BIAS;

    t <<= ((unsigned) ulaw & TSC_TRANSCODING_SEG_MASK) >>
        TSC_TRANSCODING_SEG_SHIFT;

    return ((ulaw & TSC_TRANSCODING_SIGN_BIT) ? (TSC_TRANSCODING_BIAS - t)
            : (t - TSC_TRANSCODING_BIAS));
}

tsc_error_code
tsc_get_qos_state (tsc_qos_handle handle, tsc_qos_state * state)
{
    tsc_qos_info *info = (tsc_qos_info *) handle;

    if (tsc_lock_get (info->data_lock) == tsc_lock_response_ok) {
        memcpy (state, &(info->state), sizeof (tsc_qos_state));

        tsc_lock_release (info->data_lock);

        return tsc_error_code_ok;
    }
    else {
        TSC_ERROR ("tsc_get_qos_state: failed to get lock [%p]",
                   info->tunnel_handle);
    }

    return tsc_error_code_error;
}

tsc_error_code
tsc_get_qos_output (tsc_qos_handle handle, tsc_qos_output * output)
{
    tsc_qos_info *info = (tsc_qos_info *) handle;

    if (tsc_lock_get (info->data_lock) == tsc_lock_response_ok) {
        memcpy (output, &(info->output), sizeof (tsc_qos_output));

        tsc_lock_release (info->data_lock);

        return tsc_error_code_ok;
    }
    else {
        TSC_ERROR ("tsc_get_qos_output: failed to get lock [%p]",
                   info->tunnel_handle);
    }

    return tsc_error_code_error;
}
