/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#include "tsc_common.h"

#ifndef __TSC_ENCODER_H__
#define __TSC_ENCODER_H__



#define TSC_CM_MAX_SIZE 0x400

#define TSC_VERSION_ID_IP_4 0x4
#define TSC_VERSION_ID_IP_6 0x6

#ifdef TSC_REDUNDANCY
#define TSC_VERSION_ID_IP_4_RED 0x5
#define TSC_VERSION_ID_IP_6_RED 0x7
#endif


#define TSC_TLV_RESPONSE_CODE                0x03
#define TSC_TLV_INTERNAL_IPV4_ADDRESS        0x04
#define TSC_TLV_INTERNAL_IPV4_NETMASK        0x05
#define TSC_TLV_KEEP_ALIVE_INTERNAL          0x06
#define TSC_TLV_SUPPORTED_VERSION_ID         0x07
#define TSC_TLV_PADDING                      0x08
#define TSC_TLV_REDIRECTED_IPV4_ADDRESS      0x09
#define TSC_TLV_SERVER_SIP_IPV4_ADDRESS_PORT 0x0A
#define TSC_TLV_CLIENT_INFO                  0x17
#define TSC_TLV_CONNECTION_INFO_IPV4         0x18
#define TSC_TLV_CONNECTION_INFO_IPV6         0x19
#define TSC_TLV_CONNECTION_ID                0x1A
#define TSC_TLV_SERVICE_TYPE                 0x1B
#define TSC_TLV_REDUNDANCY_FACTOR            0x1C
#define TSC_TLV_TUNNEL_ID                    0x1D
#define TSC_TLV_PEER_IPV4_ADDRESS_PORT       0x1E
#define TSC_TLV_REDUNDANCY_METHOD            0x1F

#define TSC_TLV_LENGTH_OF_RESPONSE_CODE                 0x02
#define TSC_TLV_LENGTH_OF_INTERNAL_IPV4_ADDRESS         0x04
#define TSC_TLV_LENGTH_OF_INTERNAL_IPV4_NETMASK         0x04
#define TSC_TLV_LENGTH_OF_KEEPALIVE_INTERVAL            0x02
#define TSC_TLV_LENGTH_OF_SUPPORTED_VERSION_ID          0x01
#define TSC_TLV_LENGTH_OF_REDIRECT_IPV4_ADDRESS         0x04
#define TSC_TLV_LENGTH_OF_SERVER_SIP_IPV4_ADDRESS_PORT  0x07
#define TSC_TLV_LENGTH_OF_CLIENT_INFO                   0x100
#define TSC_TLV_LENGTH_OF_CONNECTION_INFO_IPV4          0x0F
#define TSC_TLV_LENGTH_OF_CONNECTION_INFO_IPV6          0x27
#define TSC_TLV_LENGTH_OF_CONNECTION_ID                 0x04
#define TSC_TLV_LENGTH_OF_SERVICE_TYPE                  0x01
#define TSC_TLV_LENGTH_OF_REDUNDANCY_FACTOR             0x01
#define TSC_TLV_LENGTH_OF_TUNNEL_ID                     0x08
#define TSC_TLV_LENGTH_OF_REDUNDANCY_METHOD             0x01
#define TSC_TLV_LENGTH_OF_PEER_IPV4_ADDRESS_PORT        0x06

#define TSC_TLV_SERVICE_TYPE_ENABLE_HEADER_COMPRESSION  0x01
#define TSC_TLV_SERVICE_TYPE_DISABLE_HEADER_COMPRESSION 0x02
#define TSC_TLV_SERVICE_TYPE_ENABLE_REDUNDANCY          0x03
#define TSC_TLV_SERVICE_TYPE_DISABLE_REDUNDANCY         0x04

#define TSC_TLV_REDUNDANCY_MAX_TUNNEL_ID 20

typedef enum
{
    tsc_cm_type_reserve = 0,
    tsc_cm_type_config_request = 1,
    tsc_cm_type_config_response = 2,
    tsc_cm_type_config_resume_request = 3,
    tsc_cm_type_config_resume_response = 4,
    tsc_cm_type_config_release_request = 5,
    tsc_cm_type_config_release_response = 6,
    tsc_cm_type_keepalive = 7,
    tsc_cm_type_keepalive_response = 8,
    tsc_cm_type_authentication_request = 9,
    tsc_cm_type_authentication_response = 10,
    tsc_cm_type_client_service_request = 11,
    tsc_cm_type_client_service_response = 12
} tsc_cm_type;

typedef enum
{
    tsc_response_code_success = 0,
    tsc_response_code_invalid_session_id = 1,
    tsc_response_code_ip_address_black_list = 2,
    tsc_response_code_out_of_resources = 3,
    tsc_response_code_service_unavailable = 4,
    tsc_response_code_version_not_supported = 5,
    tsc_response_code_redirection = 6
} tsc_response_code;

typedef struct
{
    uint32_t version_id;
    tsc_cm_type msg_type;
    tsc_tunnel_id tunnel_id;
    uint32_t sequence;
} tsc_cm_header;

typedef struct
{
    tsc_bool valid_internal_ip_address;
    tsc_bool valid_internal_ip_mask;
    tsc_bool valid_sip_server;
    tsc_bool valid_supported_version_id;
    tsc_bool valid_keepalive_interval;
    tsc_bool valid_client_info;

    tsc_ip_address internal_ip_address;
    tsc_ip_mask internal_ip_mask;
    tsc_ip_port_address_prot sip_server;
    uint32_t supported_version_id;
    uint32_t keepalive_interval;
    uint8_t client_info[TSC_TLV_LENGTH_OF_CLIENT_INFO];
    uint32_t client_info_len;
} tsc_cm_config_request;

typedef struct
{
    tsc_bool valid_internal_ip_address;
    tsc_bool valid_internal_ip_mask;
    tsc_bool valid_sip_server;
    tsc_bool valid_redirected_ip_address;
    tsc_bool valid_peer_ip_port;
    tsc_bool valid_supported_version_id;
    tsc_bool valid_keepalive_interval;

    tsc_response_code response_code;
    tsc_ip_address internal_ip_address;
    tsc_ip_mask internal_ip_mask;
    tsc_ip_port_address_prot sip_server;
    tsc_ip_address redirected_ip_address;
    tsc_ip_port_address peer_ip_port;
    uint32_t supported_version_id;
    uint32_t keepalive_interval;
} tsc_cm_config_response;

typedef struct
{
    /* For now resume request doesn't have TLVs */
    void *dummy;                /* to prevent warnings under ANSI-C */
} tsc_cm_config_resume_request;

typedef struct
{
    tsc_bool valid_internal_ip_address;
    tsc_bool valid_internal_ip_mask;
    tsc_bool valid_sip_server;
    tsc_bool valid_redirected_ip_address;
    tsc_bool valid_peer_ip_port;
    tsc_bool valid_keepalive_interval;

    tsc_response_code response_code;
    tsc_ip_address internal_ip_address;
    tsc_ip_mask internal_ip_mask;
    tsc_ip_port_address_prot sip_server;
    tsc_ip_address redirected_ip_address;
    tsc_ip_port_address peer_ip_port;
    uint32_t keepalive_interval;
} tsc_cm_config_resume_response;

typedef struct
{
    /* For now resume request doesn't have TLVs */
    void *dummy;                /* to prevent warnings under ANSI-C */
} tsc_cm_config_release_request;

typedef struct
{
    tsc_response_code response_code;
} tsc_cm_config_release_response;

typedef struct
{
    /* For now keep alive request doesn't have TLVs */
    void *dummy;                /* to prevent warnings under ANSI-C */
} tsc_cm_keepalive_request;

typedef struct
{
    /* For now keep alive doesn't have TLVs */
    void *dummy;                /* to prevent warnings under ANSI-C */
} tsc_cm_keepalive_response;

typedef struct
{
    uint8_t tos;
    uint8_t protocol;
    uint8_t ttl;
    tsc_ip_port_address src_address;
    tsc_ip_port_address dst_address;
} tsc_tlv_connection_info_ipv4;

typedef enum
{
    tsc_tlv_connection_info_use_ipv4 = 0,
    tsc_tlv_connection_info_use_ipv6,
    tsc_tlv_connection_info_use_id
} tsc_tlv_connection_info_use;

typedef struct
{
    tsc_tlv_connection_info_use connection_info_use;

    union {
        tsc_tlv_connection_info_ipv4 connection_info_ipv4;
    } connection_info;
} tsc_cm_client_service_request_header_compression;

typedef enum
{
    tsc_tlv_redundancy_method_udp_dtls = 0,
    tsc_tlv_redundancy_method_tcp_tls_fan_out,
    tsc_tlv_redundancy_method_tcp_tls_load_balance
} tsc_tlv_redundancy_method;

typedef struct
{
    uint8_t redundancy_factor;

    tsc_tlv_connection_info_use connection_info_use;
    tsc_tlv_redundancy_method redundancy_method;

    union {
        tsc_tlv_connection_info_ipv4 connection_info_ipv4;
        uint32_t connection_id;
    } connection_info;
} tsc_cm_client_service_request_redundancy;

typedef struct
{
    uint8_t max_tunnels;

    tsc_tunnel_id tunnel_id[TSC_TLV_REDUNDANCY_MAX_TUNNEL_ID];
} tsc_cm_client_service_response_redundancy;

typedef struct
{
    uint8_t service_type;

    union {
        tsc_cm_client_service_request_header_compression header_compression;
        tsc_cm_client_service_request_redundancy redundancy;
    } service_request;
} tsc_cm_client_service_request;

typedef struct
{
    tsc_response_code response_code;

    union {
        tsc_cm_client_service_response_redundancy redundancy;
    } service_response;
} tsc_cm_client_service_response;

typedef union
{
    tsc_cm_config_request config_request;
    tsc_cm_config_response config_response;
    tsc_cm_config_resume_request config_resume_request;
    tsc_cm_config_resume_response config_resume_response;
    tsc_cm_config_release_request config_release_request;
    tsc_cm_config_release_response config_release_response;
    tsc_cm_keepalive_request keepalive_request;
    tsc_cm_keepalive_response keepalive_response;
    tsc_cm_client_service_request client_service_request;
    tsc_cm_client_service_response client_service_response;
} tsc_cm_msg;

typedef struct
{
    tsc_cm_header header;
    tsc_cm_msg msg;
} tsc_cm;

/*APIs to encode/decode CM */

/* encode control message
 input: msg and outputLen (buffer size)
 output: output (encoded data) and size of the encoded data, 0 if it is not possible
 to encode (i.e. outputLen not big enough) 
*/
size_t tsc_encode_cm (tsc_cm * msg, void *output, size_t outputLen);

/* decode control message
 input: input and inputLen buffer
 output: outputMsg structure and response code = TRUE/FALSE to indicate
 whether decoding was successfull or not
*/
tsc_bool tsc_decode_cm (void *input, size_t inputLen, tsc_cm * outputMsg);

/* Note that whatever message is encoded with tsc_encode_cm should be decoded
 with tsc_decode_cm. This function is for diagnostics only and should
 verify that all possible messages are encoded/decoded correctly. It should
 return TRUE or FALSE depending whether the test fails or not
*/
tsc_bool tsc_encode_decode_diags ();

#endif /* __TSC_ENCODER_H__ */
