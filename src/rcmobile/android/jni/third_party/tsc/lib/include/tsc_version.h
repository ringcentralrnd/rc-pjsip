
/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#include "tsc_log.h"

#define TSC_PREFIX "nnTSC"
#define TSC_MAJOR "1"
#define TSC_MINOR1 "0"
#define TSC_MINOR2 "0"
#define TSC_PATCH "p1"
#define TSC_BUILD "30"

#define TSC_MAX_VERSION_SIZE 0x100

#ifdef TSC_WINDOWS
#define TSC_VERSION_STR(version)                                                 \
    _snprintf((char *)version, TSC_MAX_VERSION_SIZE, "%s%s%s%s%s (Build %s)",    \
             TSC_PREFIX,                                                         \
             TSC_MAJOR,                                                          \
             TSC_MINOR1,                                                         \
             TSC_MINOR2,                                                         \
             TSC_PATCH,                                                          \
             TSC_BUILD);
#else
#define TSC_VERSION_STR(version)                                                \
    snprintf((char *)version, TSC_MAX_VERSION_SIZE, "%s%s%s%s%s (Build %s)",	\
	     TSC_PREFIX,					      	        \
	     TSC_MAJOR,							        \
	     TSC_MINOR1,						        \
	     TSC_MINOR2,						        \
	     TSC_PATCH,							        \
	     TSC_BUILD);
#endif

#define TSC_LOG_VERSION()		   \
    char version[TSC_MAX_VERSION_SIZE];    \
    TSC_VERSION_STR(version);		   \
    TSC_ERROR("tsc version: %s", version);


#define TSC_PRINT_VERSION()             \
    char version[TSC_MAX_VERSION_SIZE]; \
    TSC_VERSION_STR(version);           \
    printf("%s",version );
