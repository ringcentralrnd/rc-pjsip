/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/
/*! \brief
low level tunnel API
*/

/** @file
 * This file is part of TSC Client API
 * and defines low level tunnel API
 *
 */

#ifndef __TSC_QOS_H__
#define __TSC_QOS_H__

#ifdef __cplusplus
extern "C" {
#endif
#include "tsc_tunnel.h"
typedef void *tsc_qos_handle;

typedef enum
{
    tsc_qos_codec_ulaw = 0
} tsc_qos_codec;

typedef enum
{
    tsc_qos_state_stopped = 0,
    tsc_qos_state_running,
    tsc_qos_state_failure,
    tsc_qos_state_done
} tsc_qos_state;

typedef struct
{
    /* pcm data allocated by application */
    int16_t *pcm_buffer;
    uint32_t pcm_len;
    uint16_t pcm_frame_size;
    tsc_qos_codec codec;
    uint16_t local_rtp_port;
    uint16_t playout_buffer_size;
} tsc_qos_input;

typedef struct
{
    int16_t *pcm_buffer;
    uint32_t pcm_len;
    double pesq;
    uint32_t network_latency;
    uint32_t playout_latency;
    double network_loss;
    double playout_loss;
    uint32_t *rtp_out_ts;
    uint32_t *rtp_buf_ts;
    uint32_t *rtp_in_ts;
} tsc_qos_output;

tsc_qos_handle tsc_new_qos (tsc_handle handle, tsc_qos_input * input);

tsc_error_code tsc_get_qos_state (tsc_qos_handle handle,
                                  tsc_qos_state * state);

tsc_error_code tsc_get_qos_output (tsc_qos_handle handle,
                                   tsc_qos_output * output);

tsc_error_code tsc_delete_qos (tsc_qos_handle handle);

#ifdef __cplusplus
}
#endif
#endif /* __TSC_QOS_H__ */
