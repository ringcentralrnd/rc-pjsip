/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/*! \brief
SSL related structures
*/


/** @file
 * This file is part of TSC Client API
 * and defines SSL configuration interface
 *
 */
#include "tsc_common.h"

#ifndef __TSC_SSL_API_H__
#define __TSC_SSL_API_H__

#ifdef __cplusplus
extern "C" {
#endif
#define TSC_MAX_CA_LEN 16000
#define TSC_MAX_CERT_LEN 16000
#define TSC_MAX_PRIV_KEY_LEN 3000
#define TSC_CA_FILE_LEN 1024
#define TSC_CERT_FILE_LEN 1024
#define TSC_KEY_FILE_LEN 1024
/*! \struct tsc_security_config_ca
* certificate authority
*/
/*! \brief
* SSL CA
*/
typedef struct
{
    uint8_t ca[TSC_MAX_CA_LEN];
    uint32_t ca_len;
} tsc_security_config_ca;


/*! \struct tsc_security_config_cert
* certificate
*/
/*! \brief
* SSL cert
*/
typedef struct
{
    uint8_t cert[TSC_MAX_CERT_LEN];
    uint32_t cert_len;
} tsc_security_config_cert;


/*! \struct tsc_security_config
* SSL configuration including file to be read from, #CAs,
* certificates and certifying authorities
*/
/*! \brief
* SSL config
*/
typedef struct
{
    tsc_bool read_from_file;
    tsc_bool auth_disable;
    uint8_t ca_file[TSC_CA_FILE_LEN];
    uint8_t cert_file[TSC_CERT_FILE_LEN];
    uint8_t private_key_file[TSC_KEY_FILE_LEN];
    uint32_t ca_count;
    tsc_security_config_ca config_ca;
    uint32_t cert_count;
    tsc_security_config_cert config_cert;
    uint8_t private_key[TSC_MAX_PRIV_KEY_LEN];
    uint32_t private_key_len;
} tsc_security_config;


#ifdef __cplusplus
}
#endif
#endif /* __TSC_SSL_API_H__ */
