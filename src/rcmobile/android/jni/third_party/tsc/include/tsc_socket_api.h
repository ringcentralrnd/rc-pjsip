/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/*! \brief
* Socket API
*/

/** @file
 * This file is part of TSC Client API
 * and defines socket API
 * Most of the structures are tsc versions of standard Socket APIs
 */

#include "tsc_tunnel.h"

#ifndef __TSC_SOCKET_H__
#define __TSC_SOCKET_H__

#ifdef __cplusplus
extern "C" {
#endif
#define SO_TSM_REALTIME       100       /* TSM socket option to enable
                                           real-time */

#ifdef TSC_REDUNDANCY
#define SO_TSC_REDUNDANCY 101           /* TSM socket option to enable
                                           redundancy */
#endif

#define SO_TSC_QUEUE_SIZE     200
#define SO_TSC_SLOW_IDLE_POLL 300
/*! \typedef tsc_nfds_t
*same as nfds of generic socket API
*/
typedef unsigned int tsc_nfds_t;

/*! \struct tsc_pollfd
*file descriptor polling information
*tsc version of standard pollfd struct
*/
/*! \brief
* fd poll info
*/
struct tsc_pollfd
{
    int fd;
    short events;
    short revents;
};

/*! \struct tsc_timeval
* generic time description structure
* whole time passed in sec plus time in microseconds
* tsc version of standard timeval struct
*/
/*! \brief
* time description
*/
struct tsc_timeval
{
    long tv_sec;
    long tv_usec;
};

#define TSC_FD_SETSIZE 64

#define TSC_POLLIN          0x001       /* There is data to read.  */
#define TSC_POLLPRI         0x002       /* There is urgent data to read.  */
#define TSC_POLLOUT         0x004       /* Writing now will not block.  */

/*! \struct tsc_fd_set
* file descriptor set information
* number and size of fds in the set
* tsc version of standard fd_set struct
*/
/*! \brief
* file descriptor set
*/
typedef struct
{
    uint32_t fd_count;
    int fd_array[TSC_FD_SETSIZE];
} tsc_fd_set;

#define TSC_FD_CLR(fd, set) do { \
    uint32_t __i; \
    for (__i = 0; __i < ((tsc_fd_set *)(set))->fd_count ; __i++) { \
        if (((tsc_fd_set *)(set))->fd_array[__i] == fd) { \
            while (__i < ((tsc_fd_set *)(set))->fd_count-1) { \
                ((tsc_fd_set *)(set))->fd_array[__i] = \
                    ((tsc_fd_set *)(set))->fd_array[__i+1]; \
                __i++; \
            } \
            ((tsc_fd_set *)(set))->fd_count--; \
            break; \
        } \
    } \
} while(0)

#define TSC_FD_SET(fd, set) do { \
    if (((tsc_fd_set *)(set))->fd_count < TSC_FD_SETSIZE) \
        ((tsc_fd_set *)(set))->fd_array[((tsc_fd_set *)(set))->fd_count++]=(fd);\
} while(0)

#define TSC_FD_ZERO(set) (((tsc_fd_set *)(set))->fd_count=0)

/** @addtogroup TSCSocketAPI */
/*@{*/

/*!
* Purpose: check if fd is a member of the set \n
* tsc version of standard FD_ISSET function
@return Type: int.\n Returns non zero value if fd is a member of the set, else returns 0 
@param fd \n Type: int.\n file descriptor
@param set \n Type: tsc_fd_set variable.\n set of FDs to look in
*/
int TSC_FD_ISSET (int fd, tsc_fd_set * set);

/*!
* Purpose: returns tunnel handle for a given tsc socket \n
@param s \n Type: int.\n Socket handle returned by socket().
@return Type: tsc_handle.\n Returns NULL if no tunnel is found
 */
tsc_handle tsc_get_tunnel (int s);

/*!
* Purpose: creates a new socket amd initializes queues for the socket \n
* tsc version of standard Socket(af, type, protocol) function
@return Type: int.\n Returns non zero value if socket created
@param handle \n Type: tsc_handle .\n handle for the created socket, value changes on return. data structure to keep all info related to socket(configuration. connection times, connection state etc.) 
@param af \n Type: int .\n address family for the socket
@param type \n Type: int .\n type of socket (Currently support SOCK_STREAM and SOCK_DGRAM)
@param protocol \n Type: int .\n protocol (tcp/udp etc.)
*/
int tsc_socket (tsc_handle handle, int af, int type, int protocol);

/*!
* Purpose: bind a socket handle s to a port \n
* tsc version of standard bind(socket, sockaddr, namelen) function
@param s \n Type: int.\n Socket handle returned by socket().
@param addr \n Type: sockaddr variable.\n socket address structure of local address to assign to the bound socket
@param namelen \n Type: int.\n sizeof(addr)
@return Type: int.\n Returns -1 if port already occupied
 */
int tsc_bind (int s, struct sockaddr *addr, int namelen);

/*!
* Purpose: release port associated with socket s \n
* tsc-version of standard Linux close or Windows closesocket function
@param s \n Type: int. \n Socket handle to be closed
@return Type: int.\n 
*/
int tsc_close (int s);

/*!
* Purpose: UDP sendto \n
* This would take data form a buffer and send it onward to the socket specified by the 'to' parameter
* can be used in non-connection mode
* tsc version of standard sendto function
@param s \n Type: int. \n socket handle
@param buf \n Type: string.\n buffer with data to be sent
@param len \n Type: int. \n length of data
@param flags \n Type: int. \n flags to control how call is made
@param to \n Type: sockaddr variable. \n destination sockaddr
@param tolen \n Type: int. \n byte size of address pointed by 'to' structure
@return Type: int.\n Returns total no. of bytes sent, if error will return -1 
*/
int tsc_sendto (int s, char *buf, int len, int flags, struct sockaddr *to,
                int tolen);

/*!
* Purpose: UDP recvfrom \n
* enables receiving data into a buffer pointed to by 'buf' from a socket 'from'
* can be used in non-connection mode
* tsc version of standard recvfrom function
@param s \n Type: int. \n socket handle
@param buf \n Type: string.\n buffer for incoming data
@param len \n Type: int. \n len(buffer)
@param flags \n Type: int. \n flags to control how call is made
@param from \n Type: sockaddr variable. \n source sockaddr
@param fromlen \n Type: socklen_t variable. \n byte size of buffer pointed to by from
@return Type: int.\n Returns total no. of bytes sent, if error will return -1 
*/
int tsc_recvfrom (int s, char *buf, int len, int flags, struct sockaddr *from,
                  socklen_t * fromlen);

/*!
* Purpose: control open socket with cmd \n
* tsc version of standard ioctl function to work with tsc_socket
@param s \n Type: int. \n socket handle
@param cmd \n Type: long.\n command to execute (FIONBIO, F_GETFL, F_SETFL)
@param argp \n Type: unsigned long. \n pointer to argument for cmd
@return Type:  int.\n Returns socket_error_code if command fails
*/
int tsc_ioctl (int s, long cmd, unsigned long *argp);
/*!
* Purpose: get local socket name \n
* tsc version of standard getsockname function \n
@param s \n Type: int. \n socket handle
@param name \n Type: sockaddr variable.\n buffer to store socket name, value changes on return
@param namelen \n Type: int. \n Length of string in name.sa_data. value changes on return 
@return Type:  int.\n Returns 0 if no error occurs
*/
int tsc_getsockname (int s, struct sockaddr *name, socklen_t *namelen);

/*!
* Purpose: set socket option at level \n
* tsc version of standard setsockopt function \n
* supported values: \n
*  level = SOL_SOCKET
*  optname = SO_TSM_REALTIME -> enables/disables socket as real-time
*  optname = IP_TOS -> set TOS value in IP header
@param s \n Type: int. \n socket handle
@param level \n Type: int.\n level at which optname is defined
@param optname \n Type: int. \n option to set
@param optval \n Type: char pointer.\n pointer to buffer in which value for option specified
@param optlen \n Type: int. \n size of buffer
@return Type:  int.\n Returns 0 if no error
*/
int tsc_setsockopt (int s, int level, int optname, char *optval, int optlen);

/*!
* Purpose: get socket option at level \n 
* tsc version of standard getsockopt function
@param s \n Type: int. \n socket handle
@param level \n Type: int.\n level at which optname is defined
@param optname \n Type: int. \n option to get
@param optval \n Type: char pointer.\n pointer to buffer in which value for option specified
@param optlen \n Type: int pointer. \n pointer to size of buffer
@return Type:  int.\n Returns 0 if no error
*/
int tsc_getsockopt (int s, int level, int optname, char *optval, int *optlen);

/*!
* Purpose: disable send/receive on socket, shutdown the connection \n
* tsc version of standard shutdown function
@param s \n Type: int. \n socket handle
@param how \n Type: int. \n flags to specify which operations would not allowed
@return \n Type: int.\n Returns 0 if no error
*/
int tsc_shutdown (int s, int how);

/*!
* Purpose: control open fd with cmd \n
* tsc version of standard fcntl function
@param s \n Type: int. \n socket handle
@param cmd \n Type: int.\n command to execute, either F_GETFL or F_SETFL
@param arg \n Type: long. \n argument to 'cmd' 
@return Type:  int.\n Returns 0 if no error
*/
int tsc_fcntl (int s, int cmd, long arg);

/*! 
* Purpose: determine status of socket to perform synchronous I/O \n
*this would let us know if there is data on the socket for pending read/write operations \n
* tsc version of standard select function
@param nfds \n Type: int.\n 1+socket handle with highest value
@param readfds \n Type: tsc_fd_set variable. \n set of file/socket descriptors to be polled for non blockinfg read/wriet
@param writefds \n Type: tsc_fd_set variable.\n file/socket handles to poll for non blocking write
@param exceptfds \n Type: tsc_fd_set variable. \n file socket handles to poll for error detection
@param timeout \n Type: tsc_timeval variable.\n time for which descriptors polled for available I/O operations
@return Type:  int.\n no of handles in fd ready for I/O
*/
int tsc_select (int nfds, tsc_fd_set * readfds, tsc_fd_set * writefds,
                tsc_fd_set * exceptfds, struct tsc_timeval *timeout);

/*!
* Purpose: determine status of one/more sockets \n
* tsc version of standard poll function
@param fds \n Type: tsc_pollfd array.\n set of sockets for which status requested
@param nfds \n Type: \ref tsc_nfds_t . \n no of tsc_pollfd structures in fds
@param timeout \n Type: int.\n Wait behavior
@return Type:  int.\ no of structures in fds for which condition is met
*/
int tsc_poll (struct tsc_pollfd *fds, tsc_nfds_t nfds, int timeout);

/*!
* Purpose: is fd related to handle
@param handle \n Type: \ref tsc_handle.\n socket handle
@param fd \n Type: int.\n file descriptor
@return Type:  bool.\n  
*/
tsc_bool tsc_is_valid_fd (tsc_handle handle, int fd);

/*!
* Purpose: connect to socket \n
* this initiates a connection request to the server \n 
* tsc version of standrd connect function
@param s \n Type: int. \n socket handle
@param name \n Type: sockaddr variable.\n socket name
@param namelen \n Type: int. \n sizeof(name)
@return Type:  int.\n Returns 0 if no error
*/
int tsc_connect (int s, struct sockaddr *name, int namelen);

/*!
* Purpose: allow incoming connection on socket \n
* tsc version of standard accept function
@param s \n Type: int. \n socket handle
@param addr \n Type: sockaddr variable.\n pointer to buffer
@param addrlen \n Type: int. \n sizeof(addr)
@return Type:  int.\n descriptor for new socket
*/
int tsc_accept (int s, struct sockaddr *addr, int *addrlen);

/*!
* Purpose: change socket state to listening \n
* tsc version of standard listen function
@param s \n Type: int. \n socket handle
@param backlog \n Type: int.\n maximum pending connections
@return Type:  int.\n Returns 0 if no error
*/
int tsc_listen (int s, int backlog);

/*!
* Purpose: receive data from socket 's' and put the data into the buffer 'buf' \n
* The receive call is made when data on buffer is expected, ideally after a tsc_select would tell us of pending reads \n
* tsc version of standard recv function
@param s \n Type: int. \n socket handle
@param buf \n Type: char pointer.\n pointer to buffer
@param flags \n Type: int. \n flags (currently no flags are supported)
@param len \n Type: int. \n length in bytes of buf
@return Type:  int.\n Returns no of bytes received and buf has data received
*/
int tsc_recv (int s, char *buf, int len, int flags);

/*!
* Purpose: send data in buffer 'buf' on a socket 's' \n
* tsc version of standard send function
@param s \n Type: int. \n socket handle
@param buf \n Type: char pointer.\n pointer to buffer with data to be sent
@param flags \n Type: int. \n flags (currently no flags are supported)
@param len \n Type: int. \n length in bytes of buf
@return Type:  int.\n Returns no of bytes sent
*/
int tsc_send (int s, char *buf, int len, int flags);

/*@}*/

#ifdef __cplusplus
}
#endif
#endif /* __TSC_SOCKET_H__ */
