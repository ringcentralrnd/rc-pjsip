/*
 * rtphdr.h
 *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 */

#ifndef RTPHDR_H_
#define RTPHDR_H_

#include <pj/os.h>
#include <pj/types.h>
#include <pj/string.h>

struct rtp_info {
    pj_uint16_t rtp_order;
    pj_uint32_t rtp_timestamp;

    pj_timestamp timestamp;
};

int save_rtp_info( const struct rtp_info* irtp,
        int block_count,
        FILE* f_out );

#endif /* RTPHDR_H_ */
