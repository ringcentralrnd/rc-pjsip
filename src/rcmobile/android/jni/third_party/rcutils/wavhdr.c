/*
 * wavhdr.c
 *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 */

#include <wavhdr.h>

pj_uint32_t ck_riff_id = 0x46464952; //
pj_uint32_t ck_wave_id = 0x45564157; //
pj_uint32_t ck_fmt_id  = 0x20746D66; //
pj_uint32_t ck_data_id = 0x61746164; //

struct s_chunk {
  pj_uint32_t ck_id;
  unsigned int ck_size;
};

struct s_wave_fmt {
  unsigned short tag;
  unsigned short channels;
  unsigned int samples_per_sec;
  unsigned int avg_bytes_per_sec;
  unsigned short  block_align;
  unsigned short  bits_per_sample;
};

struct wave_hdr {
    struct s_chunk m_chank;
    pj_uint32_t fmt;
    struct s_chunk s_chank;
    struct s_wave_fmt w_fmt;
    struct s_chunk d_chank;
};

int prepare_wav_header( int clock_rate,
                         int channels,
                         int bits_per_sample,
                         int data_size,
                         FILE* f_record )
{

    struct wave_hdr w_hdr;
    pj_ssize_t wsize;

    if( !f_record )
        return 0;

    w_hdr.m_chank.ck_id     = ck_riff_id;
    w_hdr.m_chank.ck_size   = 4 + sizeof(struct s_wave_fmt) + sizeof(struct s_chunk) + data_size * (bits_per_sample/8);

    w_hdr.fmt               = ck_wave_id;

    w_hdr.s_chank.ck_id     = ck_fmt_id;
    w_hdr.s_chank.ck_size   = sizeof(struct s_wave_fmt);

    w_hdr.w_fmt.tag              = 1;
    w_hdr.w_fmt.channels         = channels;
    w_hdr.w_fmt.samples_per_sec  = clock_rate;
    w_hdr.w_fmt.bits_per_sample  = bits_per_sample;
    w_hdr.w_fmt.avg_bytes_per_sec= (w_hdr.w_fmt.bits_per_sample / 8) * w_hdr.w_fmt.channels * w_hdr.w_fmt.samples_per_sec;
    w_hdr.w_fmt.block_align      = (w_hdr.w_fmt.bits_per_sample / 8) * w_hdr.w_fmt.channels;

    w_hdr.d_chank.ck_id          = ck_data_id;
    w_hdr.d_chank.ck_size        = data_size * (w_hdr.w_fmt.bits_per_sample / 8);

    wsize = sizeof(w_hdr);

    fwrite((void*)&w_hdr, wsize, 1, f_record );

    return 1;
}


