#hack by marting.hong
#copy from csipsimple and ref pjproject::ilbc
ifeq ($(RC_PJSIP_USE_SILK),1)
LOCAL_PATH := $(call my-dir)/../../silk/





### Glue for pjsip codec ###
include $(CLEAR_VARS)
LOCAL_MODULE := silk

SILK_PATH := $(LOCAL_PATH)/SILK_SDK_SRC_FIX_v1.0.8/
PJ_SILK_PATH := $(LOCAL_PATH)/pj_sources/

#ok, no issue above ,martin

# pj
PJ_DIR = $(LOCAL_PATH)/../../../
LOCAL_C_INCLUDES += $(PJ_DIR)/pjlib/include \
	$(PJ_DIR)/pjlib-util/include \
	$(PJ_DIR)/pjnath/include/ \
	$(PJ_DIR)/pjmedia/include/
# silk
LOCAL_C_INCLUDES += $(SILK_PATH)/interface
SILK_FILES := $(wildcard $(SILK_PATH)/src/*.c)
LOCAL_SRC_FILES += $(SILK_FILES:$(LOCAL_PATH)/%=%)
# self
LOCAL_C_INCLUDES += $(PJ_SILK_PATH)
#LOCAL_SRC_FILES += $(PJ_SILK_PATH)/silk.c

#LOCAL_SHARED_LIBRARIES += libpjsipjni

LOCAL_CFLAGS := $(RC_PJSIP_LIB_FLAGS)

include $(BUILD_STATIC_LIBRARY)
include $(CLEAR_VARS)

endif