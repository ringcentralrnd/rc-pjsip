LOCAL_PATH := $(call my-dir)/../../srtp/

include $(CLEAR_VARS)
LOCAL_MODULE    := srtp

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../pjlib/include \
			$(LOCAL_PATH)crypto/include/ \
			$(LOCAL_PATH)include/ \
			$(LOCAL_PATH)/../build/srtp/

			

LOCAL_CFLAGS := $(RC_PJSIP_LIB_FLAGS)
PJSIP_LIB_SRC_DIR := 

LOCAL_SRC_FILES := $(PJSIP_LIB_SRC_DIR)/crypto/cipher/cipher.c $(PJSIP_LIB_SRC_DIR)/crypto/cipher/null_cipher.c      \
		$(PJSIP_LIB_SRC_DIR)/crypto/cipher/aes.c $(PJSIP_LIB_SRC_DIR)/crypto/cipher/aes_icm.c             \
		$(PJSIP_LIB_SRC_DIR)/crypto/cipher/aes_cbc.c \
		$(PJSIP_LIB_SRC_DIR)/crypto/hash/null_auth.c $(PJSIP_LIB_SRC_DIR)/crypto/hash/sha1.c \
        $(PJSIP_LIB_SRC_DIR)/crypto/hash/hmac.c $(PJSIP_LIB_SRC_DIR)/crypto/hash/auth.c \
		$(PJSIP_LIB_SRC_DIR)/crypto/replay/rdb.c $(PJSIP_LIB_SRC_DIR)/crypto/replay/rdbx.c               \
		$(PJSIP_LIB_SRC_DIR)/crypto/replay/ut_sim.c \
		$(PJSIP_LIB_SRC_DIR)/crypto/math/datatypes.c $(PJSIP_LIB_SRC_DIR)/crypto/math/stat.c \
		$(PJSIP_LIB_SRC_DIR)/crypto/rng/rand_source.c $(PJSIP_LIB_SRC_DIR)/crypto/rng/prng.c \
		$(PJSIP_LIB_SRC_DIR)/crypto/rng/ctr_prng.c \
		$(PJSIP_LIB_SRC_DIR)/pjlib/srtp_err.c \
		$(PJSIP_LIB_SRC_DIR)/crypto/kernel/crypto_kernel.c  $(PJSIP_LIB_SRC_DIR)/crypto/kernel/alloc.c   \
		$(PJSIP_LIB_SRC_DIR)/crypto/kernel/key.c \
		$(PJSIP_LIB_SRC_DIR)/srtp/srtp.c 


include $(BUILD_STATIC_LIBRARY)
