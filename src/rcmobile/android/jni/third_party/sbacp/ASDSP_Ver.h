/******************************************************************************
*
*   Filename:    ASDSP_Ver.h
*
*   Description:    ASDSP TOOLS VERSION DEFINITION
*
*   Copyright (C) 2003-2011 -- ASDSP S.r.l. -- www.asdsp.com
*
*******************************************************************************/

#ifndef __ASDSP_VER_H
#define __ASDSP_VER_H

#define ASDSP_VER      "01.14Z"
#define ASDSP_DATE     "08/22/11"

#endif // __ASDSP_VER_H

/******************************************************************************
*
*   Copyright (C) 2003-2011 -- ASDSP S.r.l. -- www.asdsp.com
*
*******************************************************************************/
