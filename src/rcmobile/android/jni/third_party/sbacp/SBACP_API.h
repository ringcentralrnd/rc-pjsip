/******************************************************************************
*
*   Filename:    SBACP_API.hpp
*
*   Description: SBACP - Subband Audio Conference Processor
*
*   Copyright (C) 2003-2011 -- ASDSP S.r.l. -- www.asdsp.com
*
*******************************************************************************/
/**@mainpage

SBACP is an all-in-one software solution for high-quality, multi-microphone hands-free speech communication,
built as a tight integration of the following ASDSP software modules:
- Multi-channel Subband Acoustic Echo Canceller (AEC)
- Subband Noise Reduction (NR)
- Subband Residual Echo Reduction (ER)
- Microphone Steering (only for multi microphone systems)
- Microphone Equalizer (EQ)
- Automatic Gain Control (AGC) for both the TX and RX side

The SBACP algorithm is shown in the block diagram (see Fig. 1).

The core block is the subband AEC. Background noise spectrum and residual echo spectrum estimators are jointly
used to control the step size and as side inputs of the Postfilter that act as combined noise and residual echo reduction.
Finally a voice driven AGC is used to keep the transmit signal loudness into a comfort range.
Even though a single microphone is depicted in the diagram the algorithm is designed to work as well in multiple microphone
applications and in this case microphone steering is used to select to the best microphone.
The number of microphones and the net bandwidth can be set run time (up the their maximum value predefined in a header file).

\image html SBACP.gif
\image latex SBACP.png "SBACP Block Diagram" width=15cm

*/

#ifndef __SBACP_API_H
#define __SBACP_API_H

//=============================================================================
//  Basic Type definitions
//=============================================================================

#ifndef __ASDSP_H

#define ShortReal   float

#endif  // __ASDSP_H


//=============================================================================
//  Enable Flag (Switch ON/OFF internal functionalities)
//=============================================================================

enum E_EnableIndex
{
    AEC_ENABLE          = 0,            //!< EchoCanceller
    RESCUE_ENABLE       = 1,            //!< RescueDetector
    POSTFILTER_ENABLE   = 2,            //!< Postfilter
    NOISE_RED_ENABLE    = 3,            //!< Noise Reduction
    EQUALIZER_ENABLE    = 4,            //!< TX Equalizer
    TX_AGC_ENABLE       = 5,            //!< TxAGC
    RX_LIMITER_ENABLE   = 6,            //!< RxLimiter
    RX_LOSS_ENABLE      = 7,            //!< RxLoss
    MIC_STEER_ENABLE    = 8,            //!< MicSteering
    SPEECH_DET_ENABLE   = 9,            //!< Enable pitch based Speech Detector
    TX_MUTE_ENABLE      = 10,           //!< TX MUTE
    RX_MUTE_ENABLE      = 11,           //!< RX MUTE
    USER_TEST_ENABLE    = 12,           //!< User defined test
    SBACP_NUM_ENABLE                    // This way SBACP_NUM_ENABLE is always properly defined
};


//=============================================================================
//  I_SBACP Interface Class Definition
//=============================================================================

class I_SBACP                                               //! SBACP Interface Class
{
public:
    I_SBACP();                                              //!< Constructor

    ~I_SBACP();                                             //!< Destructor

    void Restart();                                         //!< Restart (to be used at the beginning of a call)

    ///@name  Package ID strings
    //@{
    const char* GetVer()      const;                        //!< Version ID string
    const char* GetDate()     const;                        //!< Version date string
    const char* GetProdID()   const;                        //!< Product ID string
    const char* GetPlatform() const;                        //!< Platform ID string
    //@}

    //-----------------------------------------------------
    /** @name   Audio I/O Configuration parameters       */
    //-----------------------------------------------------
    //@{

    //-----------------------------------------------------
    //  Sample Rate and Frame Size (set at build time)
    //-----------------------------------------------------

    int GetSampleRate () const;                             //!< Get the sample rate [Hz]
    int GetFrameSize ()  const;                             //!< Get the frame size [samples]


    //-----------------------------------------------------
    //  Get/Set the Audio Delay for the speaker reference
    //-----------------------------------------------------

    int  GetAudioDelay() const;                             //!< Get the Audio Delay for the speaker reference [samples]
    void SetAudioDelay (int AudioDelay);                    //!< Set the Audio Delay for the speaker reference [samples]


    //-----------------------------------------------------
    //  Get/Set the number of enabled mic inputs
    //-----------------------------------------------------

    int  GetNumberOfMics () const;                          //!< Get the number of mic inputs
    void SetNumberOfMics (int NumberOfMics);                //!< Set the number of mic inputs


    //-----------------------------------------------------
    //  Get/Set the Gain [dB] for a given mic input
    //-----------------------------------------------------

    float GetMicGain_dB (int mic_index) const;              //!< Get the Gain [dB] for a given mic input
    void  SetMicGain_dB (int mic_index, float Gain_dB);     //!< Set the Gain [dB] for a given mic input

#if _MIC_GAIN_CONTROL_
    //-----------------------------------------------------
    //  Microphone Gain Closed Loop Control
    //-----------------------------------------------------

    float GetMicLevel_dB () const;                          //!< Get the Mic Level [dB] (to be used for level meter)
    float GetSpkLevel_dB () const;                          //!< Get the Speaker Level [dB] (for level meter)
    
    int GetMicGainDelta () const;                           //!< Get the delta step for Automatic Mic Gain Closed Loop Control (-1 = "high", +1 = "low", 0 = "OK")
#endif  // _MIC_GAIN_CONTROL_

    //-----------------------------------------------------
    //  RxLimiter (Speaker Volume and Peak Limiter)
    //-----------------------------------------------------

    float GetVolume_dB() const;                             //!< Get the speaker output Volume [dB]
    void  SetVolume_dB (float Volume_dB);                   //!< Set the speaker output Volume [dB]
    

    //-----------------------------------------------------
    //  Enable Flag (enable/disable internal modules)
    //-----------------------------------------------------

    bool GetEnable (E_EnableIndex EnableIndex) const;       //!< Get each Enable Flag individually
    void SetEnable (E_EnableIndex EnableIndex, bool Flag);  //!< Set each Enable Flag individually

    void PrintEnable();                                     //!< Print all the Enable Flags


    //-----------------------------------------------------
    //  Band Limit Mode
    //-----------------------------------------------------

    #define SBACP_FULL_BAND       0                         //!< Freq Max = 1/2 Sample Rate
    #define SBACP_POTS_BAND       1                         //!< Freq Max = 3500 Hz
    #define SBACP_WIDE_7KHZ       2                         //!< Freq Max = 7000 Hz
    #define SBACP_WIDE_14KHZ      3                         //!< Freq Max = 14000 Hz
    #define SBACP_POTS_4KHZ       4                         //!< Freq Max = 4000 Hz
    #define SBACP_WIDE_8KHZ       5                         //!< Freq Max = 8000 Hz
    #define SBACP_WIDE_16KHZ      6                         //!< Freq Max = 16000 Hz

    int  GetBandLimitMode () const;                         //!< Get the Band Limit Mode
    void SetBandLimitMode (int BandLimitMode);              //!< Set the Band Limit Mode
    //@}

    //-----------------------------------------------------
    /** @name  Test Mode                                 */
    //-----------------------------------------------------
    //@{
    #define SBACP_TEST_OFF              0
    #define SBACP_TEST_MIC_BYPASS       1                   //!< mic[SteeredMic]  --> TxOut
    #define SBACP_TEST_SPK_BYPASS       2                   //!< speaker          --> SpkOut
    #define SBACP_TEST_BYPASS           3                   //!< mic[SteeredMic]  --> TxOut           speaker --> SpkOut
    #define SBACP_TEST_CHECK_DELAY      4                   //!< mic[SteeredMic] + SpeakerRef --> TxOut
    #define SBACP_TEST_PING_DELAY       5                   //!< Beep detection based Audio delay estimation
    #define SBACP_TEST_RESERVED         6                   //!< Reserved test mode (internal use only)

    void SetTestMode (int TestMode);
    int  GetTestMode() const;
    //@}

    bool GetSignalDetect() const;                           //!< Get the Signal Activity Detection flag


    //-----------------------------------------------------
    /** @name       Frame Processing                     */
    //-----------------------------------------------------
    //@{
    void ProcessSpkFrame                                    //! Process one speaker frame and feed the reference delay line
    (
        ShortReal*  speaker,                                //!< speaker input
        ShortReal*  SpkOut                                  //!< speaker output
    );

    void ProcessSpkFrame                                    //! Just feed the reference delay line with one speaker frame
    (
        ShortReal*  speaker                                 //!< speaker input
    );

    void ProcessMicFrame                                    //! Process one mic frame using the speaker reference in the delay line
    (
        ShortReal* mic[],                                   //!< mic inputs
        ShortReal* TxOut                                    //!< TX output
    );

    //-----------------------------------------------------
    //  Get the SpkReference delay line depth counter: 
    //  it is incremented by 1 on ProcessMicFrame()
    //  and decremented by 1 on ProcessSpkFrame();
    //  so a negative count shows delay line underflow
    //-----------------------------------------------------

    int GetSpkDelayLineDepth () const;                      // [frames]

    //@}

    //-----------------------------------------------------
    //  Text Output (wrapper for printf)
    //-----------------------------------------------------

    void OverrideTextOut (int (*pTextOut)(const char* msg)); //!< Set the function pointer TextOut to pTextOut


    //-----------------------------------------------------
    /** @name   Probe outputs
                to be used for testing and benchmarking   */
    //-----------------------------------------------------
    //@{
    void Probe();                                           //!< To be called at the end of each frame iteration

    void SetProbeMode (int ProbeMode);                      //!< Set ProbeMode (0 ==> Probe Output OFF)
    int  GetProbeMode () const;                             //!< Get ProbeMode (0 ==> Probe Output OFF)

    void SetProbeTime (int Time_ms);                        //!< Set ProbeTime [ms]. (a Report is printed once every ProbeTime ms)
    //@}

    //-----------------------------------------------------
    /// @name   String Commands Parser
    //-----------------------------------------------------
    //@{
    void SetUpCommandTable(void);                           //!< Setup test and diagnostic user command table

    bool CurrGroup (void);                                  //!< @return "true" if the current command group is "MCHP"

    int ParseCommand                                        //! Parse a command string. @return true if a valid user command is found; \n false otherwise.
    (
        char* CommandString                                 //!< command string: it is cleared (set to a null string) during parsing
    );

    void NewCommand                                         //! Create a new user command
    (
        const char*     Name,                               //!< Command name (case insensitive)
        int             NumArg,                             //!< Number of argument
        const char*     Description,                        //!< Description string to be printed on help
        void            (*pCommandExec)                     //! Command executing function pointer
        (
            int     Argc,                                       //!< Number of arguments (command name excluded)
            char**  Argv,                                       //!< Argument array Argv[i], i=0, Argc-1
            void*   Handle                                      //!< module handle
        )
    );

    //@}
    //-----------------------------------------------------
    
private:
    void* hSBACP;                                           // Context Handle
};


#endif // __SBACP_API_H

/******************************************************************************
*
*   Copyright (C) 2003-2011 -- ASDSP S.r.l. -- www.asdsp.com
*
*******************************************************************************/
