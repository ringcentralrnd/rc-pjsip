/******************************************************************************
*
*   Filename:   PFA_FFT_Ver.h
*
*   Description: PRIME FACTOR FFT ALGORITHM
*
*   Copyright (C) 2003-2010 -- ASDSP S.r.l. -- www.asdsp.com
*
*******************************************************************************/

#ifndef __PFA_FFT_VER_H
#define __PFA_FFT_VER_H

#define PFA_FFT_VER        "01.03F"
#define PFA_FFT_DATE       "11/27/10"

#endif // __PFA_FFT_VER_H

/******************************************************************************
*
*   Copyright (C) 2003-2010 -- ASDSP S.r.l. -- www.asdsp.com
*
*******************************************************************************/
