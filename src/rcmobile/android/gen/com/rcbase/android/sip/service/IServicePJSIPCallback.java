/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: F:\\share\\android\\android-ndk-r8\\apps\\RingCentral\\src\\com\\rcbase\\android\\sip\\service\\IServicePJSIPCallback.aidl
 */
package com.rcbase.android.sip.service;
/**
 * @author Denis Kudja
 *
 */
public interface IServicePJSIPCallback extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.rcbase.android.sip.service.IServicePJSIPCallback
{
private static final java.lang.String DESCRIPTOR = "com.rcbase.android.sip.service.IServicePJSIPCallback";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.rcbase.android.sip.service.IServicePJSIPCallback interface,
 * generating a proxy if needed.
 */
public static com.rcbase.android.sip.service.IServicePJSIPCallback asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.rcbase.android.sip.service.IServicePJSIPCallback))) {
return ((com.rcbase.android.sip.service.IServicePJSIPCallback)iin);
}
return new com.rcbase.android.sip.service.IServicePJSIPCallback.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_account_changed_register_state:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
boolean _arg1;
_arg1 = (0!=data.readInt());
int _arg2;
_arg2 = data.readInt();
this.account_changed_register_state(_arg0, _arg1, _arg2);
reply.writeNoException();
return true;
}
case TRANSACTION_on_incoming_call:
{
data.enforceInterface(DESCRIPTOR);
com.rcbase.android.sip.service.CallInfo _arg0;
if ((0!=data.readInt())) {
_arg0 = com.rcbase.android.sip.service.CallInfo.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.on_incoming_call(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_on_message:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
com.rcbase.parsers.sipmessage.SipMessageInput _arg1;
if ((0!=data.readInt())) {
_arg1 = com.rcbase.parsers.sipmessage.SipMessageInput.CREATOR.createFromParcel(data);
}
else {
_arg1 = null;
}
this.on_message(_arg0, _arg1);
reply.writeNoException();
return true;
}
case TRANSACTION_call_media_state:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
int _arg1;
_arg1 = data.readInt();
this.call_media_state(_arg0, _arg1);
reply.writeNoException();
return true;
}
case TRANSACTION_call_media_stop:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
this.call_media_stop(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_call_state:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
int _arg1;
_arg1 = data.readInt();
int _arg2;
_arg2 = data.readInt();
int _arg3;
_arg3 = data.readInt();
int _arg4;
_arg4 = data.readInt();
this.call_state(_arg0, _arg1, _arg2, _arg3, _arg4);
reply.writeNoException();
return true;
}
case TRANSACTION_on_call_transfer_status:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
int _arg1;
_arg1 = data.readInt();
java.lang.String _arg2;
_arg2 = data.readString();
this.on_call_transfer_status(_arg0, _arg1, _arg2);
reply.writeNoException();
return true;
}
case TRANSACTION_on_audio_route_changed:
{
data.enforceInterface(DESCRIPTOR);
com.rcbase.android.utils.media.AudioState _arg0;
if ((0!=data.readInt())) {
_arg0 = com.rcbase.android.utils.media.AudioState.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.on_audio_route_changed(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_http_registration_state:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
this.http_registration_state(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_on_sip_stack_started:
{
data.enforceInterface(DESCRIPTOR);
boolean _arg0;
_arg0 = (0!=data.readInt());
this.on_sip_stack_started(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_on_call_info_changed:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
this.on_call_info_changed(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_on_test_started:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
this.on_test_started(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_on_test_completed:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
int _arg1;
_arg1 = data.readInt();
this.on_test_completed(_arg0, _arg1);
reply.writeNoException();
return true;
}
case TRANSACTION_notifyCallsStateChanged:
{
data.enforceInterface(DESCRIPTOR);
com.rcbase.android.sip.service.RCCallInfo[] _arg0;
_arg0 = data.createTypedArray(com.rcbase.android.sip.service.RCCallInfo.CREATOR);
this.notifyCallsStateChanged(_arg0);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.rcbase.android.sip.service.IServicePJSIPCallback
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
public void account_changed_register_state(int accountId, boolean registered, int code) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(accountId);
_data.writeInt(((registered)?(1):(0)));
_data.writeInt(code);
mRemote.transact(Stub.TRANSACTION_account_changed_register_state, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void on_incoming_call(com.rcbase.android.sip.service.CallInfo callInfo) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((callInfo!=null)) {
_data.writeInt(1);
callInfo.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_on_incoming_call, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void on_message(long rcCallId, com.rcbase.parsers.sipmessage.SipMessageInput sipMsg) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(rcCallId);
if ((sipMsg!=null)) {
_data.writeInt(1);
sipMsg.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_on_message, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void call_media_state(long rcCallId, int state) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(rcCallId);
_data.writeInt(state);
mRemote.transact(Stub.TRANSACTION_call_media_state, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void call_media_stop(long rcCallId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(rcCallId);
mRemote.transact(Stub.TRANSACTION_call_media_stop, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void call_state(long rcCallId, int state, int last_status, int ending_reason, int media_state) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(rcCallId);
_data.writeInt(state);
_data.writeInt(last_status);
_data.writeInt(ending_reason);
_data.writeInt(media_state);
mRemote.transact(Stub.TRANSACTION_call_state, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void on_call_transfer_status(long rcCallId, int status, java.lang.String statusProgressText) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(rcCallId);
_data.writeInt(status);
_data.writeString(statusProgressText);
mRemote.transact(Stub.TRANSACTION_on_call_transfer_status, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void on_audio_route_changed(com.rcbase.android.utils.media.AudioState audioState) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((audioState!=null)) {
_data.writeInt(1);
audioState.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_on_audio_route_changed, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void http_registration_state(int code) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(code);
mRemote.transact(Stub.TRANSACTION_http_registration_state, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void on_sip_stack_started(boolean result) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(((result)?(1):(0)));
mRemote.transact(Stub.TRANSACTION_on_sip_stack_started, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void on_call_info_changed(long rcCallId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(rcCallId);
mRemote.transact(Stub.TRANSACTION_on_call_info_changed, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void on_test_started(int id) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(id);
mRemote.transact(Stub.TRANSACTION_on_test_started, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void on_test_completed(int id, int value) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(id);
_data.writeInt(value);
mRemote.transact(Stub.TRANSACTION_on_test_completed, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * NEW "CENTRIC" SCHEME ================================================================================================================
     *//**
     * SIProxy notifications
     */
public void notifyCallsStateChanged(com.rcbase.android.sip.service.RCCallInfo[] calls) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeTypedArray(calls, 0);
mRemote.transact(Stub.TRANSACTION_notifyCallsStateChanged, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_account_changed_register_state = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_on_incoming_call = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_on_message = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_call_media_state = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_call_media_stop = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
static final int TRANSACTION_call_state = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
static final int TRANSACTION_on_call_transfer_status = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
static final int TRANSACTION_on_audio_route_changed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
static final int TRANSACTION_http_registration_state = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
static final int TRANSACTION_on_sip_stack_started = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
static final int TRANSACTION_on_call_info_changed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
static final int TRANSACTION_on_test_started = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
static final int TRANSACTION_on_test_completed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
static final int TRANSACTION_notifyCallsStateChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
}
public void account_changed_register_state(int accountId, boolean registered, int code) throws android.os.RemoteException;
public void on_incoming_call(com.rcbase.android.sip.service.CallInfo callInfo) throws android.os.RemoteException;
public void on_message(long rcCallId, com.rcbase.parsers.sipmessage.SipMessageInput sipMsg) throws android.os.RemoteException;
public void call_media_state(long rcCallId, int state) throws android.os.RemoteException;
public void call_media_stop(long rcCallId) throws android.os.RemoteException;
public void call_state(long rcCallId, int state, int last_status, int ending_reason, int media_state) throws android.os.RemoteException;
public void on_call_transfer_status(long rcCallId, int status, java.lang.String statusProgressText) throws android.os.RemoteException;
public void on_audio_route_changed(com.rcbase.android.utils.media.AudioState audioState) throws android.os.RemoteException;
public void http_registration_state(int code) throws android.os.RemoteException;
public void on_sip_stack_started(boolean result) throws android.os.RemoteException;
public void on_call_info_changed(long rcCallId) throws android.os.RemoteException;
public void on_test_started(int id) throws android.os.RemoteException;
public void on_test_completed(int id, int value) throws android.os.RemoteException;
/**
     * NEW "CENTRIC" SCHEME ================================================================================================================
     *//**
     * SIProxy notifications
     */
public void notifyCallsStateChanged(com.rcbase.android.sip.service.RCCallInfo[] calls) throws android.os.RemoteException;
}
