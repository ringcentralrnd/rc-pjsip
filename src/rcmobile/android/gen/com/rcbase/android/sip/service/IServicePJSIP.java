/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: F:\\share\\android\\android-ndk-r8\\apps\\RingCentral\\src\\com\\rcbase\\android\\sip\\service\\IServicePJSIP.aidl
 */
package com.rcbase.android.sip.service;
/**
 * @author Denis Kudja
 *
 */
public interface IServicePJSIP extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.rcbase.android.sip.service.IServicePJSIP
{
private static final java.lang.String DESCRIPTOR = "com.rcbase.android.sip.service.IServicePJSIP";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.rcbase.android.sip.service.IServicePJSIP interface,
 * generating a proxy if needed.
 */
public static com.rcbase.android.sip.service.IServicePJSIP asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.rcbase.android.sip.service.IServicePJSIP))) {
return ((com.rcbase.android.sip.service.IServicePJSIP)iin);
}
return new com.rcbase.android.sip.service.IServicePJSIP.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_startSipStack:
{
data.enforceInterface(DESCRIPTOR);
boolean _arg0;
_arg0 = (0!=data.readInt());
int _result = this.startSipStack(_arg0);
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_stopSipStack:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.stopSipStack();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_getSipServicePID:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.getSipServicePID();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_testMode_emualateCrash:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
this.testMode_emualateCrash(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_isStartedSipStack:
{
data.enforceInterface(DESCRIPTOR);
boolean _result = this.isStartedSipStack();
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_registerCallback:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
com.rcbase.android.sip.service.IServicePJSIPCallback _arg1;
_arg1 = com.rcbase.android.sip.service.IServicePJSIPCallback.Stub.asInterface(data.readStrongBinder());
this.registerCallback(_arg0, _arg1);
reply.writeNoException();
return true;
}
case TRANSACTION_unregisterCallback:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
this.unregisterCallback(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_call_answer:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
long _arg1;
_arg1 = data.readLong();
this.call_answer(_arg0, _arg1);
reply.writeNoException();
return true;
}
case TRANSACTION_call_answer_and_hold:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
long _arg1;
_arg1 = data.readLong();
this.call_answer_and_hold(_arg0, _arg1);
reply.writeNoException();
return true;
}
case TRANSACTION_call_answer_and_hangup:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
long _arg1;
_arg1 = data.readLong();
this.call_answer_and_hangup(_arg0, _arg1);
reply.writeNoException();
return true;
}
case TRANSACTION_call_hangup:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
int _arg1;
_arg1 = data.readInt();
boolean _arg2;
_arg2 = (0!=data.readInt());
int _result = this.call_hangup(_arg0, _arg1, _arg2);
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_call_hold:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
int _result = this.call_hold(_arg0);
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_call_unhold:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
int _result = this.call_unhold(_arg0);
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_call_make_call_init:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
java.lang.String _arg1;
_arg1 = data.readString();
java.lang.String _arg2;
_arg2 = data.readString();
int _result = this.call_make_call_init(_arg0, _arg1, _arg2);
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_call_make_call_complete:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
this.call_make_call_complete(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_call_dial_dtmf:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
java.lang.String _arg1;
_arg1 = data.readString();
int _result = this.call_dial_dtmf(_arg0, _arg1);
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_call_mark_as_ending:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
int _result = this.call_mark_as_ending(_arg0);
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_call_get_info:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
com.rcbase.android.sip.service.CallInfo _arg1;
if ((0!=data.readInt())) {
_arg1 = com.rcbase.android.sip.service.CallInfo.CREATOR.createFromParcel(data);
}
else {
_arg1 = null;
}
int _result = this.call_get_info(_arg0, _arg1);
reply.writeNoException();
reply.writeInt(_result);
if ((_arg1!=null)) {
reply.writeInt(1);
_arg1.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_call_is_active:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
boolean _result = this.call_is_active(_arg0);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_call_has_media:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
boolean _result = this.call_has_media(_arg0);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_call_get_count:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.call_get_count();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_call_get_pjsip_count:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.call_get_pjsip_count();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_call_get_active_calls:
{
data.enforceInterface(DESCRIPTOR);
long[] _result = this.call_get_active_calls();
reply.writeNoException();
reply.writeLongArray(_result);
return true;
}
case TRANSACTION_acc_get:
{
data.enforceInterface(DESCRIPTOR);
com.rcbase.android.sip.service.AccountInfo _arg0;
if ((0!=data.readInt())) {
_arg0 = com.rcbase.android.sip.service.AccountInfo.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
int _result = this.acc_get(_arg0);
reply.writeNoException();
reply.writeInt(_result);
if ((_arg0!=null)) {
reply.writeInt(1);
_arg0.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_acc_is_registered:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
boolean _result = this.acc_is_registered(_arg0);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_message_send:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
java.lang.String _arg1;
_arg1 = data.readString();
java.lang.String _arg2;
_arg2 = data.readString();
int _result = this.message_send(_arg0, _arg1, _arg2);
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_get_snd_dev:
{
data.enforceInterface(DESCRIPTOR);
int[] _result = this.get_snd_dev();
reply.writeNoException();
reply.writeIntArray(_result);
return true;
}
case TRANSACTION_ring_stop:
{
data.enforceInterface(DESCRIPTOR);
this.ring_stop();
reply.writeNoException();
return true;
}
case TRANSACTION_ringtone_stop:
{
data.enforceInterface(DESCRIPTOR);
this.ringtone_stop();
reply.writeNoException();
return true;
}
case TRANSACTION_conf_adjust_tx_level:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
float _arg1;
_arg1 = data.readFloat();
int _result = this.conf_adjust_tx_level(_arg0, _arg1);
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_conf_adjust_rx_level:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
float _arg1;
_arg1 = data.readFloat();
int _result = this.conf_adjust_rx_level(_arg0, _arg1);
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_conf_get_adjust_rx_level:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
float _result = this.conf_get_adjust_rx_level(_arg0);
reply.writeNoException();
reply.writeFloat(_result);
return true;
}
case TRANSACTION_conf_get_adjust_tx_level:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
float _result = this.conf_get_adjust_tx_level(_arg0);
reply.writeNoException();
reply.writeFloat(_result);
return true;
}
case TRANSACTION_conf_get_signal_rx_level:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
float _result = this.conf_get_signal_rx_level(_arg0);
reply.writeNoException();
reply.writeFloat(_result);
return true;
}
case TRANSACTION_conf_get_signal_tx_level:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
float _result = this.conf_get_signal_tx_level(_arg0);
reply.writeNoException();
reply.writeFloat(_result);
return true;
}
case TRANSACTION_getAudioState:
{
data.enforceInterface(DESCRIPTOR);
com.rcbase.android.utils.media.AudioState _result = this.getAudioState();
reply.writeNoException();
if ((_result!=null)) {
reply.writeInt(1);
_result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_toggleBluetoothState:
{
data.enforceInterface(DESCRIPTOR);
this.toggleBluetoothState();
reply.writeNoException();
return true;
}
case TRANSACTION_toggleSpeakerState:
{
data.enforceInterface(DESCRIPTOR);
this.toggleSpeakerState();
reply.writeNoException();
return true;
}
case TRANSACTION_storeCurrentLog:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
java.lang.String _result = this.storeCurrentLog(_arg0);
reply.writeNoException();
reply.writeString(_result);
return true;
}
case TRANSACTION_ase_initialize:
{
data.enforceInterface(DESCRIPTOR);
this.ase_initialize();
reply.writeNoException();
return true;
}
case TRANSACTION_asw_start:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
this.asw_start(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_asw_stop:
{
data.enforceInterface(DESCRIPTOR);
this.asw_stop();
reply.writeNoException();
return true;
}
case TRANSACTION_asw_release:
{
data.enforceInterface(DESCRIPTOR);
this.asw_release();
reply.writeNoException();
return true;
}
case TRANSACTION_set_snd_dev_test_mode:
{
data.enforceInterface(DESCRIPTOR);
this.set_snd_dev_test_mode();
reply.writeNoException();
return true;
}
case TRANSACTION_getLatestFinishedCalls:
{
data.enforceInterface(DESCRIPTOR);
com.rcbase.android.sip.service.RCCallInfo[] _result = this.getLatestFinishedCalls();
reply.writeNoException();
reply.writeTypedArray(_result, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
return true;
}
case TRANSACTION_getCurrentCalls:
{
data.enforceInterface(DESCRIPTOR);
com.rcbase.android.sip.service.RCCallInfo[] _result = this.getCurrentCalls();
reply.writeNoException();
reply.writeTypedArray(_result, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
return true;
}
case TRANSACTION_getServiceState:
{
data.enforceInterface(DESCRIPTOR);
com.rcbase.android.sip.service.ServiceState _result = this.getServiceState();
reply.writeNoException();
if ((_result!=null)) {
reply.writeInt(1);
_result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_cleanupLogAndLatestFinishedCalls:
{
data.enforceInterface(DESCRIPTOR);
this.cleanupLogAndLatestFinishedCalls();
reply.writeNoException();
return true;
}
case TRANSACTION_cleanupLog:
{
data.enforceInterface(DESCRIPTOR);
this.cleanupLog();
reply.writeNoException();
return true;
}
case TRANSACTION_getCurrentLog:
{
data.enforceInterface(DESCRIPTOR);
boolean _arg0;
_arg0 = (0!=data.readInt());
com.rcbase.android.logging.LogItem[] _result = this.getCurrentLog(_arg0);
reply.writeNoException();
reply.writeTypedArray(_result, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
return true;
}
case TRANSACTION_canMakeVoIPOutboundCall:
{
data.enforceInterface(DESCRIPTOR);
boolean _result = this.canMakeVoIPOutboundCall();
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_isPlayBackDevice:
{
data.enforceInterface(DESCRIPTOR);
boolean _result = this.isPlayBackDevice();
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_testEnqueedCommandsMaxNumberExceeded:
{
data.enforceInterface(DESCRIPTOR);
this.testEnqueedCommandsMaxNumberExceeded();
reply.writeNoException();
return true;
}
case TRANSACTION_testCommandExecutionTimeExceeded:
{
data.enforceInterface(DESCRIPTOR);
this.testCommandExecutionTimeExceeded();
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.rcbase.android.sip.service.IServicePJSIP
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
public int startSipStack(boolean testmode) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(((testmode)?(1):(0)));
mRemote.transact(Stub.TRANSACTION_startSipStack, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public int stopSipStack() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_stopSipStack, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public int getSipServicePID() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getSipServicePID, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public void testMode_emualateCrash(long delay) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(delay);
mRemote.transact(Stub.TRANSACTION_testMode_emualateCrash, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
//    int restartSipStack();

public boolean isStartedSipStack() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_isStartedSipStack, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public void registerCallback(int hash, com.rcbase.android.sip.service.IServicePJSIPCallback callback) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(hash);
_data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_registerCallback, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void unregisterCallback(int hash) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(hash);
mRemote.transact(Stub.TRANSACTION_unregisterCallback, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
//    boolean isHttpRegistrationDone();
//    boolean isOutboundCallsOnly();
//    void outboundCallsOnlyFinish();

public void call_answer(long callId, long delayMillis) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(callId);
_data.writeLong(delayMillis);
mRemote.transact(Stub.TRANSACTION_call_answer, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void call_answer_and_hold(long callId, long delayMillis) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(callId);
_data.writeLong(delayMillis);
mRemote.transact(Stub.TRANSACTION_call_answer_and_hold, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void call_answer_and_hangup(long callId, long delayMillis) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(callId);
_data.writeLong(delayMillis);
mRemote.transact(Stub.TRANSACTION_call_answer_and_hangup, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public int call_hangup(long callId, int code, boolean detachSound) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(callId);
_data.writeInt(code);
_data.writeInt(((detachSound)?(1):(0)));
mRemote.transact(Stub.TRANSACTION_call_hangup, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public int call_hold(long callId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(callId);
mRemote.transact(Stub.TRANSACTION_call_hold, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
//    int call_hold_all();

public int call_unhold(long callId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(callId);
mRemote.transact(Stub.TRANSACTION_call_unhold, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
//    int call_make_call( int accountId, String remoteURI, String remoteName ); 

public int call_make_call_init(int accountId, java.lang.String remoteURI, java.lang.String remoteName) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(accountId);
_data.writeString(remoteURI);
_data.writeString(remoteName);
mRemote.transact(Stub.TRANSACTION_call_make_call_init, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public void call_make_call_complete(long callId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(callId);
mRemote.transact(Stub.TRANSACTION_call_make_call_complete, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
//    int call_attach_sound( long callId );
//    int call_send_request( long callId, String method, String clientId ); 
//    int call_transfer( long callId, String remoteURI );

public int call_dial_dtmf(long callId, java.lang.String digits) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(callId);
_data.writeString(digits);
mRemote.transact(Stub.TRANSACTION_call_dial_dtmf, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
//    int call_detach_sound( long callId );

public int call_mark_as_ending(long callId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(callId);
mRemote.transact(Stub.TRANSACTION_call_mark_as_ending, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public int call_get_info(long callId, com.rcbase.android.sip.service.CallInfo callInfo) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(callId);
if ((callInfo!=null)) {
_data.writeInt(1);
callInfo.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_call_get_info, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
if ((0!=_reply.readInt())) {
callInfo.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public boolean call_is_active(long callId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(callId);
mRemote.transact(Stub.TRANSACTION_call_is_active, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public boolean call_has_media(long callId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(callId);
mRemote.transact(Stub.TRANSACTION_call_has_media, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public int call_get_count() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_call_get_count, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public int call_get_pjsip_count() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_call_get_pjsip_count, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public long[] call_get_active_calls() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
long[] _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_call_get_active_calls, _data, _reply, 0);
_reply.readException();
_result = _reply.createLongArray();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
//    long call_get_duration( long callId );
//    int acc_get_default();

public int acc_get(com.rcbase.android.sip.service.AccountInfo accountInfo) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((accountInfo!=null)) {
_data.writeInt(1);
accountInfo.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_acc_get, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
if ((0!=_reply.readInt())) {
accountInfo.readFromParcel(_reply);
}
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
//    int acc_del( int accountId );
//    int acc_set_registration( int accountId, boolean renew );

public boolean acc_is_registered(int accountId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(accountId);
mRemote.transact(Stub.TRANSACTION_acc_is_registered, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public int message_send(int accountId, java.lang.String dstURI, java.lang.String content) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(accountId);
_data.writeString(dstURI);
_data.writeString(content);
mRemote.transact(Stub.TRANSACTION_message_send, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
//    CodecInfo[] enum_codecs();
//    int codec_set_priority( String codecId, int priority );

public int[] get_snd_dev() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int[] _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_get_snd_dev, _data, _reply, 0);
_reply.readException();
_result = _reply.createIntArray();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
//    void set_snd_dev();
//    void ring_start();

public void ring_stop() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_ring_stop, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void ringtone_stop() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_ringtone_stop, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public int conf_adjust_tx_level(int slot, float level) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(slot);
_data.writeFloat(level);
mRemote.transact(Stub.TRANSACTION_conf_adjust_tx_level, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public int conf_adjust_rx_level(int slot, float level) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(slot);
_data.writeFloat(level);
mRemote.transact(Stub.TRANSACTION_conf_adjust_rx_level, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public float conf_get_adjust_rx_level(int slot) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
float _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(slot);
mRemote.transact(Stub.TRANSACTION_conf_get_adjust_rx_level, _data, _reply, 0);
_reply.readException();
_result = _reply.readFloat();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public float conf_get_adjust_tx_level(int slot) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
float _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(slot);
mRemote.transact(Stub.TRANSACTION_conf_get_adjust_tx_level, _data, _reply, 0);
_reply.readException();
_result = _reply.readFloat();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public float conf_get_signal_rx_level(int slot) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
float _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(slot);
mRemote.transact(Stub.TRANSACTION_conf_get_signal_rx_level, _data, _reply, 0);
_reply.readException();
_result = _reply.readFloat();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public float conf_get_signal_tx_level(int slot) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
float _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(slot);
mRemote.transact(Stub.TRANSACTION_conf_get_signal_tx_level, _data, _reply, 0);
_reply.readException();
_result = _reply.readFloat();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public com.rcbase.android.utils.media.AudioState getAudioState() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
com.rcbase.android.utils.media.AudioState _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getAudioState, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
_result = com.rcbase.android.utils.media.AudioState.CREATOR.createFromParcel(_reply);
}
else {
_result = null;
}
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public void toggleBluetoothState() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_toggleBluetoothState, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void toggleSpeakerState() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_toggleSpeakerState, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public java.lang.String storeCurrentLog(int applicationPID) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
java.lang.String _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(applicationPID);
mRemote.transact(Stub.TRANSACTION_storeCurrentLog, _data, _reply, 0);
_reply.readException();
_result = _reply.readString();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public void ase_initialize() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_ase_initialize, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void asw_start(int testId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(testId);
mRemote.transact(Stub.TRANSACTION_asw_start, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void asw_stop() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_asw_stop, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void asw_release() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_asw_release, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void set_snd_dev_test_mode() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_set_snd_dev_test_mode, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * Returns latest finished RC VoIP calls for dumping.
     */
public com.rcbase.android.sip.service.RCCallInfo[] getLatestFinishedCalls() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
com.rcbase.android.sip.service.RCCallInfo[] _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getLatestFinishedCalls, _data, _reply, 0);
_reply.readException();
_result = _reply.createTypedArray(com.rcbase.android.sip.service.RCCallInfo.CREATOR);
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * Returns current RC VoIP calls for dumping.
     */
public com.rcbase.android.sip.service.RCCallInfo[] getCurrentCalls() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
com.rcbase.android.sip.service.RCCallInfo[] _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getCurrentCalls, _data, _reply, 0);
_reply.readException();
_result = _reply.createTypedArray(com.rcbase.android.sip.service.RCCallInfo.CREATOR);
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * Returns current service state. 
     */
public com.rcbase.android.sip.service.ServiceState getServiceState() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
com.rcbase.android.sip.service.ServiceState _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getServiceState, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
_result = com.rcbase.android.sip.service.ServiceState.CREATOR.createFromParcel(_reply);
}
else {
_result = null;
}
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * Cleans-up Log on SIP service side and latest finished calls.
     */
public void cleanupLogAndLatestFinishedCalls() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_cleanupLogAndLatestFinishedCalls, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * Cleans-up Log.
     */
public void cleanupLog() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_cleanupLog, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * Returns current log.
     * 
     */
public com.rcbase.android.logging.LogItem[] getCurrentLog(boolean cleanup_after_getting) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
com.rcbase.android.logging.LogItem[] _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(((cleanup_after_getting)?(1):(0)));
mRemote.transact(Stub.TRANSACTION_getCurrentLog, _data, _reply, 0);
_reply.readException();
_result = _reply.createTypedArray(com.rcbase.android.logging.LogItem.CREATOR);
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * Returns if outbound call can be made.
     */
public boolean canMakeVoIPOutboundCall() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_canMakeVoIPOutboundCall, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * Returns if device is in playback mode.
     */
public boolean isPlayBackDevice() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_isPlayBackDevice, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * Commands consistency check.
     */
public void testEnqueedCommandsMaxNumberExceeded() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_testEnqueedCommandsMaxNumberExceeded, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * Commands consistency check.
     */
public void testCommandExecutionTimeExceeded() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_testCommandExecutionTimeExceeded, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_startSipStack = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_stopSipStack = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_getSipServicePID = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_testMode_emualateCrash = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_isStartedSipStack = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
static final int TRANSACTION_registerCallback = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
static final int TRANSACTION_unregisterCallback = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
static final int TRANSACTION_call_answer = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
static final int TRANSACTION_call_answer_and_hold = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
static final int TRANSACTION_call_answer_and_hangup = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
static final int TRANSACTION_call_hangup = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
static final int TRANSACTION_call_hold = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
static final int TRANSACTION_call_unhold = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
static final int TRANSACTION_call_make_call_init = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
static final int TRANSACTION_call_make_call_complete = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
static final int TRANSACTION_call_dial_dtmf = (android.os.IBinder.FIRST_CALL_TRANSACTION + 15);
static final int TRANSACTION_call_mark_as_ending = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16);
static final int TRANSACTION_call_get_info = (android.os.IBinder.FIRST_CALL_TRANSACTION + 17);
static final int TRANSACTION_call_is_active = (android.os.IBinder.FIRST_CALL_TRANSACTION + 18);
static final int TRANSACTION_call_has_media = (android.os.IBinder.FIRST_CALL_TRANSACTION + 19);
static final int TRANSACTION_call_get_count = (android.os.IBinder.FIRST_CALL_TRANSACTION + 20);
static final int TRANSACTION_call_get_pjsip_count = (android.os.IBinder.FIRST_CALL_TRANSACTION + 21);
static final int TRANSACTION_call_get_active_calls = (android.os.IBinder.FIRST_CALL_TRANSACTION + 22);
static final int TRANSACTION_acc_get = (android.os.IBinder.FIRST_CALL_TRANSACTION + 23);
static final int TRANSACTION_acc_is_registered = (android.os.IBinder.FIRST_CALL_TRANSACTION + 24);
static final int TRANSACTION_message_send = (android.os.IBinder.FIRST_CALL_TRANSACTION + 25);
static final int TRANSACTION_get_snd_dev = (android.os.IBinder.FIRST_CALL_TRANSACTION + 26);
static final int TRANSACTION_ring_stop = (android.os.IBinder.FIRST_CALL_TRANSACTION + 27);
static final int TRANSACTION_ringtone_stop = (android.os.IBinder.FIRST_CALL_TRANSACTION + 28);
static final int TRANSACTION_conf_adjust_tx_level = (android.os.IBinder.FIRST_CALL_TRANSACTION + 29);
static final int TRANSACTION_conf_adjust_rx_level = (android.os.IBinder.FIRST_CALL_TRANSACTION + 30);
static final int TRANSACTION_conf_get_adjust_rx_level = (android.os.IBinder.FIRST_CALL_TRANSACTION + 31);
static final int TRANSACTION_conf_get_adjust_tx_level = (android.os.IBinder.FIRST_CALL_TRANSACTION + 32);
static final int TRANSACTION_conf_get_signal_rx_level = (android.os.IBinder.FIRST_CALL_TRANSACTION + 33);
static final int TRANSACTION_conf_get_signal_tx_level = (android.os.IBinder.FIRST_CALL_TRANSACTION + 34);
static final int TRANSACTION_getAudioState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 35);
static final int TRANSACTION_toggleBluetoothState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 36);
static final int TRANSACTION_toggleSpeakerState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 37);
static final int TRANSACTION_storeCurrentLog = (android.os.IBinder.FIRST_CALL_TRANSACTION + 38);
static final int TRANSACTION_ase_initialize = (android.os.IBinder.FIRST_CALL_TRANSACTION + 39);
static final int TRANSACTION_asw_start = (android.os.IBinder.FIRST_CALL_TRANSACTION + 40);
static final int TRANSACTION_asw_stop = (android.os.IBinder.FIRST_CALL_TRANSACTION + 41);
static final int TRANSACTION_asw_release = (android.os.IBinder.FIRST_CALL_TRANSACTION + 42);
static final int TRANSACTION_set_snd_dev_test_mode = (android.os.IBinder.FIRST_CALL_TRANSACTION + 43);
static final int TRANSACTION_getLatestFinishedCalls = (android.os.IBinder.FIRST_CALL_TRANSACTION + 44);
static final int TRANSACTION_getCurrentCalls = (android.os.IBinder.FIRST_CALL_TRANSACTION + 45);
static final int TRANSACTION_getServiceState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 46);
static final int TRANSACTION_cleanupLogAndLatestFinishedCalls = (android.os.IBinder.FIRST_CALL_TRANSACTION + 47);
static final int TRANSACTION_cleanupLog = (android.os.IBinder.FIRST_CALL_TRANSACTION + 48);
static final int TRANSACTION_getCurrentLog = (android.os.IBinder.FIRST_CALL_TRANSACTION + 49);
static final int TRANSACTION_canMakeVoIPOutboundCall = (android.os.IBinder.FIRST_CALL_TRANSACTION + 50);
static final int TRANSACTION_isPlayBackDevice = (android.os.IBinder.FIRST_CALL_TRANSACTION + 51);
static final int TRANSACTION_testEnqueedCommandsMaxNumberExceeded = (android.os.IBinder.FIRST_CALL_TRANSACTION + 52);
static final int TRANSACTION_testCommandExecutionTimeExceeded = (android.os.IBinder.FIRST_CALL_TRANSACTION + 53);
}
public int startSipStack(boolean testmode) throws android.os.RemoteException;
public int stopSipStack() throws android.os.RemoteException;
public int getSipServicePID() throws android.os.RemoteException;
public void testMode_emualateCrash(long delay) throws android.os.RemoteException;
//    int restartSipStack();

public boolean isStartedSipStack() throws android.os.RemoteException;
public void registerCallback(int hash, com.rcbase.android.sip.service.IServicePJSIPCallback callback) throws android.os.RemoteException;
public void unregisterCallback(int hash) throws android.os.RemoteException;
//    boolean isHttpRegistrationDone();
//    boolean isOutboundCallsOnly();
//    void outboundCallsOnlyFinish();

public void call_answer(long callId, long delayMillis) throws android.os.RemoteException;
public void call_answer_and_hold(long callId, long delayMillis) throws android.os.RemoteException;
public void call_answer_and_hangup(long callId, long delayMillis) throws android.os.RemoteException;
public int call_hangup(long callId, int code, boolean detachSound) throws android.os.RemoteException;
public int call_hold(long callId) throws android.os.RemoteException;
//    int call_hold_all();

public int call_unhold(long callId) throws android.os.RemoteException;
//    int call_make_call( int accountId, String remoteURI, String remoteName ); 

public int call_make_call_init(int accountId, java.lang.String remoteURI, java.lang.String remoteName) throws android.os.RemoteException;
public void call_make_call_complete(long callId) throws android.os.RemoteException;
//    int call_attach_sound( long callId );
//    int call_send_request( long callId, String method, String clientId ); 
//    int call_transfer( long callId, String remoteURI );

public int call_dial_dtmf(long callId, java.lang.String digits) throws android.os.RemoteException;
//    int call_detach_sound( long callId );

public int call_mark_as_ending(long callId) throws android.os.RemoteException;
public int call_get_info(long callId, com.rcbase.android.sip.service.CallInfo callInfo) throws android.os.RemoteException;
public boolean call_is_active(long callId) throws android.os.RemoteException;
public boolean call_has_media(long callId) throws android.os.RemoteException;
public int call_get_count() throws android.os.RemoteException;
public int call_get_pjsip_count() throws android.os.RemoteException;
public long[] call_get_active_calls() throws android.os.RemoteException;
//    long call_get_duration( long callId );
//    int acc_get_default();

public int acc_get(com.rcbase.android.sip.service.AccountInfo accountInfo) throws android.os.RemoteException;
//    int acc_del( int accountId );
//    int acc_set_registration( int accountId, boolean renew );

public boolean acc_is_registered(int accountId) throws android.os.RemoteException;
public int message_send(int accountId, java.lang.String dstURI, java.lang.String content) throws android.os.RemoteException;
//    CodecInfo[] enum_codecs();
//    int codec_set_priority( String codecId, int priority );

public int[] get_snd_dev() throws android.os.RemoteException;
//    void set_snd_dev();
//    void ring_start();

public void ring_stop() throws android.os.RemoteException;
public void ringtone_stop() throws android.os.RemoteException;
public int conf_adjust_tx_level(int slot, float level) throws android.os.RemoteException;
public int conf_adjust_rx_level(int slot, float level) throws android.os.RemoteException;
public float conf_get_adjust_rx_level(int slot) throws android.os.RemoteException;
public float conf_get_adjust_tx_level(int slot) throws android.os.RemoteException;
public float conf_get_signal_rx_level(int slot) throws android.os.RemoteException;
public float conf_get_signal_tx_level(int slot) throws android.os.RemoteException;
public com.rcbase.android.utils.media.AudioState getAudioState() throws android.os.RemoteException;
public void toggleBluetoothState() throws android.os.RemoteException;
public void toggleSpeakerState() throws android.os.RemoteException;
public java.lang.String storeCurrentLog(int applicationPID) throws android.os.RemoteException;
public void ase_initialize() throws android.os.RemoteException;
public void asw_start(int testId) throws android.os.RemoteException;
public void asw_stop() throws android.os.RemoteException;
public void asw_release() throws android.os.RemoteException;
public void set_snd_dev_test_mode() throws android.os.RemoteException;
/**
     * Returns latest finished RC VoIP calls for dumping.
     */
public com.rcbase.android.sip.service.RCCallInfo[] getLatestFinishedCalls() throws android.os.RemoteException;
/**
     * Returns current RC VoIP calls for dumping.
     */
public com.rcbase.android.sip.service.RCCallInfo[] getCurrentCalls() throws android.os.RemoteException;
/**
     * Returns current service state. 
     */
public com.rcbase.android.sip.service.ServiceState getServiceState() throws android.os.RemoteException;
/**
     * Cleans-up Log on SIP service side and latest finished calls.
     */
public void cleanupLogAndLatestFinishedCalls() throws android.os.RemoteException;
/**
     * Cleans-up Log.
     */
public void cleanupLog() throws android.os.RemoteException;
/**
     * Returns current log.
     * 
     */
public com.rcbase.android.logging.LogItem[] getCurrentLog(boolean cleanup_after_getting) throws android.os.RemoteException;
/**
     * Returns if outbound call can be made.
     */
public boolean canMakeVoIPOutboundCall() throws android.os.RemoteException;
/**
     * Returns if device is in playback mode.
     */
public boolean isPlayBackDevice() throws android.os.RemoteException;
/**
     * Commands consistency check.
     */
public void testEnqueedCommandsMaxNumberExceeded() throws android.os.RemoteException;
/**
     * Commands consistency check.
     */
public void testCommandExecutionTimeExceeded() throws android.os.RemoteException;
}
