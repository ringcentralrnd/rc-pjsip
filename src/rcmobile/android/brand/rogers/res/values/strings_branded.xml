<?xml version="1.0" encoding="utf-8"?>
  <!--
    Copyright (C) 2010-2012, RingCentral, Inc. All Rights Reserved.
  -->

  <!-- ===================================================================
    Brand-dependent string resources for the Rogers brand.
  =====================================================================-->
  
  <!-- ===================================================================
    IMPORTANT!!!
    
    This file MUST contain ALL brand-dependent string definitions. 
	All other project resources (XML files) MUST NOT contain any brand-dependent strings
	but only references to this file.

	Don't reference strings from this file directly in the Java code.
    Use references from 'strings.xml'.
  =====================================================================-->

<resources xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">

    <!-- =====================================================================-->
    <!-- ===================== Brand definition ==============================-->
    <!-- =====================================================================-->

	<!-- This is the EXTERNAL application name -->
	<!-- as seen in the App Store, on the Android desktop etc. -->
    <string name="brand_AppLabel">IP Voice</string>
    
	<!-- This is the INTERNAL application name used inside the application UI -->
    <string name="brand_AppName">Rogers Hosted IP Voice</string>
    

    <string name="brand_name">Rogers</string>
	<string name="brand_product_name">Rogers Hosted IP Voice</string>
	<string name="brand_product_program_name">Rogers Hosted IP Voice Mobile</string>
	<string name="brand_product_program_short_name">Rogers IP Voice</string>
	

    <!-- =====================================================================-->
    <!-- ========================= Login Screen ==============================-->
    <!-- =====================================================================-->
    <string name="branded_loginscreen_setup_dialog_msg">Please finish express setup to fully take advantage of your Rogers Hosted IP Voice phone system and send activation emails to your extension users.</string> 

    <string name="branded_loginscreen_trial_dlg_msg_admin_expired">Your free trial has expired. To upgrade to a paid account and continue enjoying the Rogers Hosted IP Voice features you love, click Upgrade Now below.</string>
    <string name="branded_loginscreen_trial_dlg_msg_admin_expireinxdays">Your free trial expires in %d days. To upgrade to a paid account and continue enjoying the Rogers Hosted IP Voice features you love, click Upgrade Now below.</string>
    <string name="branded_loginscreen_trial_dlg_msg_admin_expirein1day">Your free trial expires in 1 day. To upgrade to a paid account and continue enjoying the Rogers Hosted IP Voice features you love, click Upgrade Now below.</string>
    <string name="branded_loginscreen_trial_dlg_msg_admin_expiretoday">Your free trial expires today. To upgrade to a paid account and continue enjoying the Rogers Hosted IP Voice features you love, click Upgrade Now below.</string>
    <string name="branded_loginscreen_trial_dlg_msg_user_expired">Your free trial has expired. To upgrade to a paid account and continue enjoying the Rogers Hosted IP Voice features you love, please ask an administrator to login and upgrade your account.</string>
        

    <!-- =====================================================================-->
    <!-- ================= RING OUT ==========================================-->
    <!-- =====================================================================-->
	
    <!-- RingOut Status screen -->
    <string name="branded_ringout_status_mid_label_text_1line">The other party will see your Rogers Hosted IP Voice number as caller ID:</string>
    <string name="branded_ringout_status_mid_label_text_2line">The other party will see your\nRogers Hosted IP Voice number as caller ID:</string>
    

    <!-- =====================================================================-->
    <!-- ================= MESSAGES ========================================= -->
    <!-- =====================================================================-->
    
    <string name="branded_messages_notification_ticker">New Rogers Hosted IP Voice Message.</string>
 	
 	<string name="branded_PDF_viewer_could_not_be_detected">Your Android device does not have a PDF viewer installed. To view Rogers Hosted IP Voice faxes, Rogers recommends you download the free Adobe Reader from the Android Market.</string>
        
        
    <!-- =====================================================================-->
    <!-- ================= SETTINGS ============================== -->
    <!-- =====================================================================-->

    <string name="branded_settings_tell_a_friend_body_value">\n<xliff:g id="user_name">%1$s</xliff:g> thought you should know about business phone service from Rogers Hosted IP Voice.
\n\nYou get an instantly activated toll free or local number with advanced call forwarding, a professional auto-receptionist that answers and directs calls to virtual extensions, Internet fax and a whole lot more. Turn your Android phone into feature-rich mobile office with this free app from Rogers Hosted IP Voice.
\n\nSo go ahead. Port your existing numbers to Rogers Hosted IP Voice, or get an entirely new number for your business. Either way, you’ll soon be converting callers into customers.
\n\nTo learn more about Rogers Hosted IP Voice service, please visit: www.rogers.com</string>
        
    <string name="branded_settings_feedback_subject_value">Rogers Hosted IP Voice <xliff:g id="app_version">%1$s</xliff:g> Android Feedback</string>
    <string name="branded_settings_about_title">About Rogers Hosted IP Voice</string>
    <string name="branded_settings_about_feedback_email">rogersmobilefeedback&#64;ringcentral.com</string>
        
    <string name="branded_settings_noname_user_name">Rogers Hosted IP Voice User</string>
        
    <string name="branded_ringcentral_calls_place">Choose where you would like to take your Rogers Hosted IP Voice calls:</string>
        

    <!-- =====================================================================-->
    <!-- ================= OUTGOING CALL ACTIVITY ==================-->
    <!-- =====================================================================-->

    <string name="branded_voip_call_incoming">Incoming Rogers Hosted IP Voice Call</string>
        

    <!-- =====================================================================-->
    <!-- =================      AUDIO SETUP WIZARD     =======================-->
    <!-- =====================================================================-->
        
    <string name="branded_asw_calibration_description">To ensure the best sound quality during your VoIP calls please allow the Rogers Hosted IP Voice application to perform a 30 second microphone calibration of your Android device.</string>
    <string name="branded_asw_calibration_completed_ok">Your Android device microphone was successfully calibrated with the Rogers Hosted IP Voice application. If you want to run the microphone calibration again you can find it under the settings menu.</string>
        
    
    <!-- =====================================================================-->
    <!-- =================      INTERNAL PERMISSIONS   =======================-->
    <!-- =====================================================================-->
    <string name="branded_internal_permission_label">Internal Rogers Hosted IP Voice service actions</string>
    <string name="branded_internal_permission_description">Allows the application to use internal Rogers Hosted IP Voice service actions</string>
    
    
    <!-- ============================== -->
	<!-- =============911 ToS========== -->
    <!-- ============================== -->
	    
	<string name="branded_tos911_text">[TBD]</string>

	<string name="branded_tos911_dlg_voip_calling_message">VoIP over Wi-Fi and 3G calling capabilities will be disabled until you acknowledge that 911 calling may not be available when using Rogers Hosted IP Voice VoIP.</string>


    <!-- ============================== -->
    <!--  Application Description  -->
    <!-- ============================== -->

    <string name="branded_descriptionTextOldUser">"The Rogers Hosted IP Voice app is a perfect complement to your Rogers Hosted IP Voice service. It lets you take complete control of your business phone system directly from your Android, so you can manage your calls, voice messages and faxes, everywhere.
        \n<b>With Rogers Hosted IP Voice on your Android you can:</b>\n
        \n<li>Make calls using your Rogers Hosted IP Voice business number</li>
        \n<li>Show your Rogers Hosted IP Voice business toll free number as your caller ID</li>
        \n<li>Keep all your business voicemails and faxes separate from your personal messages</li>
        \n<li>Listen to (and forward) your business voicemail messages in any order with visual voicemail</li>
        \n<li>View, send and forward faxes from your Android</li>
        \n<li>View call time, date and duration and return calls directly from your Rogers Hosted IP Voice call logs</li>
        \n<li>Automatically access all your company extensions as a contact group</li>
        \n<li>Toggle Do Not Disturb option to forward all calls directly to voicemail</li>
        \n\n<b>Rogers Hosted IP Voice delivers a mobile business phone system that includes:</b>
        \n<li>Local or toll-free numbers (including 800 number, 855, 866, 877 and 888  numbers)</li>
        \n<li>Auto-receptionist</li>
        \n<li>Mobile virtual pbx</li>
        \n<li>Multiple Extensions</li>
        \n<li>Advanced call management and answering rules</li>
        \n<li>Multiple Voicemail Boxes</li>
        \n<li>Visual voicemail</li>
        \n<li>Internet Fax</li>
        \n<li>Music on hold</li>
        \n<li>Custom greetings</li>
        \n<li>Call screening</li>
        \n<li>Call queues</li>
        \n<li>Dial-by-name directory</li>
        \n\n<b>IMPORTANT:</b> Rogers Hosted IP Voice for Android <xliff:g id="app_version">%1$s</xliff:g> requires an existing Rogers Hosted IP Voice account. Visit <a href="http://www.rogers.com/hostedipvoice"><font fgcolor="#ff0000ff">www.rogers.com/hostedipvoice</font></a> to create an account and instantly activate your mobile business phone system."
    </string>
    
    <string name="branded_descriptionTextAmarosaUser">"The Rogers Hosted IP Voice app is a perfect complement to your Rogers Hosted IP Voice service. It lets you take complete control of your business phone system directly from your Android, so you can manage your calls, voice messages and faxes, everywhere.
        \n<b>With Rogers Hosted IP Voice on your Android you can:</b>\n
        \n<li>Make calls using your Rogers Hosted IP Voice business number</li>
        \n<li>Show your Rogers Hosted IP Voice business toll free number as your caller ID</li>
        \n<li>Keep all your business voicemails and faxes separate from your personal messages</li>
        \n<li>Listen to (and forward) your business voicemail messages in any order with visual voicemail</li>
        \n<li>View, send and forward faxes from your Android</li>
        \n<li>View call time, date and duration and return calls directly from your Rogers Hosted IP Voice call logs</li>
        \n<li>Automatically access all your company extensions as a contact group</li>
        \n<li>Toggle Do Not Disturb option to forward all calls directly to voicemail</li>
        \n\n<b>Rogers Hosted IP Voice delivers a mobile business phone system that includes:</b>
        \n<li>Local or toll-free numbers (including 800 number, 855, 866, 877 and 888  numbers)</li>
        \n<li>Auto-receptionist</li>
        \n<li>Mobile virtual pbx</li>
        \n<li>Multiple Extensions</li>
        \n<li>Advanced call management and answering rules</li>
        \n<li>Multiple Voicemail Boxes</li>
        \n<li>Visual voicemail</li>
        \n<li>Internet Fax</li>
        \n<li>Music on hold</li>
        \n<li>Custom greetings</li>
        \n<li>Call screening</li>
        \n<li>Call queues</li>
        \n<li>Dial-by-name directory</li>
        \n\n<b>IMPORTANT:</b> Rogers Hosted IP Voice for Android <xliff:g id="app_version">%1$s</xliff:g> requires an existing Rogers Hosted IP Voice account. Visit <a href="http://www.rogers.com/hostedipvoice"><font fgcolor="#ff0000ff">www.rogers.com/hostedipvoice</font></a> to create an account and instantly activate your mobile business phone system."
    </string>
	    

    <!-- =====================================================================-->
    <!-- ======================= Terms of Services ===========================-->
    <!-- =====================================================================-->
        
<string name="branded_tos_text">
    This End User License Agreement (\"EULA\") constitutes a legal agreement between you and Rogers Communications Partnership, 
    an Ontario partnership, and its subsidiaries, affiliates, agents, and/or licensors (\"Rogers\", \"we\" or \"us\")
    for Rogers’ mobile service (\"Rogers Hosted IP Voice Mobile VOIP\") and software application (the \"Licensed Application\")
    made available by or through other companies (\"Third Parties\") for use on other mobile devices (e.g., Research in Motion&#174;
    for use on Blackberry&#174; or Apple Inc. (\"Apple\") Application Store for use on the iPhone).\n
    You acknowledge that the EULA is concluded between you and Rogers only, and not the Third-Party, and that Rogers,
    not any Third-Party, is solely responsible for the Licensed Application and the content thereof. You agree that you
    may not use the Licensed Application in any way that conflicts with or violates the terms of service or other
    agreements between you and any third-party. You also acknowledge and agree that such Third-Parties, and their
    subsidiaries, are third party beneficiaries of this EULA, and that, upon your acceptance of the terms and conditions
    of the EULA, such Third-Parties will have the right (and will be deemed to have accepted the right) to enforce the
    EULA against you as a third party beneficiary thereof.\n\n
    NO 911 CALLING\n\n
    DO NOT USE THE ROGERS HOSTED IP VOICE MOBILE VOIP APPLICATION TO CALL 911. IN AN EMERGENCY, EXIT THE ROGERS HOSTED
    IP VOICE MOBILE VOIP SERVICE AND DIAL 911 FROM YOUR IPHONE.\n
    YOU ACKNOWLEDGE AND AGREE THAT THE ROGERS HOSTED IP VOICE MOBILE VOIP APPLICATION IS NOT A SUBSTITUTE FOR A TRADITIONAL
    TELEPHONE AND THAT YOU WILL NOT USE THE ROGERS HOSTED IP VOICE MOBILE VOIP SERVICE TO CALL 911.\n\n
    NO iPOD OR iTOUCH CALLING\n\n
    THE ROGERS HOSTED IP VOICE MOBILE VOIP SERVICE CANNOT BE USED TO MAKE OR RECEIVE CALLS, INCLUDING 911 CALLS, FROM ANY
    iPOD OR iTOUCH DEVICE EVEN THOUGH THE ROGERS HOSTED IP VOICE MOBILE VOIP APPLICATION MAY BE DOWNLOADEDABLE TO AND/OR
    DISPLAY A RINGOUT DIAL-PAD ON SUCH DEVICES. IN AN EMERGENCY, EXIT THE ROGERS HOSTED IP VOICE MOBILE VOIP SERVICE AND
    DIAL 911 FROM A TRADITIONAL PHONE DEVICE (LANDLINE OR CELL PHONE) WITH 911 DIALING CAPABILITIES.\n\n
    ADDITIONAL AGREEMENTS\n\n
    Your use of the Rogers Hosted IP Voice Mobile VOIP Service and Licensed Application may be governed by one or more
    other agreements with Rogers in addition to the terms set forth herein. If you use the Rogers Hosted IP Voice Mobile
    VOIP Service, you are required to agree to the Terms of Service for the Rogers Hosted IP Voice Mobile VOIP Service.
    The most recent version of the Rogers Hosted IP Voice Mobile VOIP Service terms and conditions is available on the
    Rogers website.\n
    You may choose to purchase or use other Rogers services, including, telephone services and related services
    (\"Other Service\" or \"Other Services\" and, collectively with the Rogers Hosted IP Voice Mobile VOIP Service,
    the \"Service\" or \"Services\"). Your use of the Other Services requires you to agree to the Rogers Small Business
    Terms of Service. If you access the Services through Rogers’s website (the \"Website\"), you are required to agree
    to be bound by the Website on Rogers. If you access Services through Rogers’s Call Controller&#8482; software, you
    are required to agree to the End User License Agreement for that software.\n\n
    LICENSE AND RESTRICTIONS\n\n
    Subject to the terms of this EULA, Rogers hereby grants you a limited, personal, revocable, non-exclusive,
    non-sublicensable, non-assignable, non-transferable, non-resellable license and right to use the Licensed Application
    for the sole purpose of accessing the Rogers Hosted IP Voice Mobile VOIP Services on any mobile device you own or
    control and as permitted by any Third-Party agreement(s) related to said mobile devices or the use thereof.\n
    Intellectual Property Rights. You acknowledge and agree that any and all intellectual property rights (the \"IP Rights\")
    in the Rogers Hosted IP Voice Mobile VOIP Service and Licensed Application are and shall remain the exclusive property
    of Rogers and/or its licensors. Nothing in this EULA intends to or shall transfer any IP Rights to, or to vest any
    IP Rights in you. You are only entitled to the limited use of the rights granted to you in this EULA. You will not
    take any action to jeopardize, limit or interfere with the IP Rights. You acknowledge and agree that any unauthorized
    use of the IP Rights is a violation of this EULA, as well as a violation of applicable intellectual property laws.
    You acknowledge and understand that all title and rights in and to any third party content that is not contained in
    the Rogers Hosted IP Voice Mobile VOIP Service and Licensed Application, but may be accessed through the Services,
    is the property of the respective content owners and may be protected by applicable patent, copyright, or other
    intellectual property laws and treaties.\n
    No Grant of Rights to Third Parties and No Resale. You agree not to sell, assign, rent, lease, distribute, export,
    import, act as an intermediary or provider, or otherwise grant rights to third parties with regard to the Licensed
    Application or Services or any part thereof without our prior written consent.\n
    No Modifications. You agree not undertake, cause, permit or authorize the modification, creation of derivative works,
    translation, reverse engineering, decompiling, disassembling or hacking of the Licensed Application, the Services,
    or any part thereof. You agree not intercept, capture, emulate, or redirect the communications protocols used by Rogers
    for any purpose, including without limitation causing the Services or Licensed Application to connect to any computer
    server or other device not authorized by Rogers or in a manner not authorized by Rogers.
    New Versions of the Licensed Application. Rogers, in its sole discretion, reserves the right to add additional features
    or functions, or to provide programming fixes, updates and upgrades, to the Services or Licensed Application.
    You acknowledge and agree that Rogers has no obligation to make available to you any subsequent versions of the
    Licensed Application. You also agree that you may have to enter into a renewed version of this EULA if you want to
    download, install or use a new version of the Services or Licensed Application.\n
    In addition, you and Rogers acknowledge that no Third-Party has no obligation whatsoever to furnish any maintenance
    and support services with respect to the Services or Licensed Application and that Rogers is solely responsible for
    the provision of maintenance and support to the extent such maintenance and support is required under applicable law.\n\n
    TERMINATION\n\n
    Rogers reserves the right to modify immediately your use of the Services and Licensed Application, including by blocking
    access to the Services, if Rogers determines that your use of the Licensed Application violates or has at any time
    violated this EULA or any other applicable agreement between you and Rogers.\n
    Upon termination of this EULA by you or by Rogers, you (a) acknowledge and agree that all licenses and rights to use
    the Services and Licensed Application shall terminate, (b) will cease any and all use of the Licensed Application,
    and (c) will remove the Licensed Application from all mobile devices, hard drives, networks, and other storage
    media in your possession or under your control.\n\n
    LEGAL COMPLIANCE AND EXPORT RESTRICTIONS\n\n
    You represent and warrant that (i) you are not located in a country that is subject to a U.S. Government embargo, or
    that has been designated by the U.S. Government as a \"terrorist supporting\" country; and (ii) you are not listed on
    any U.S. Government list of prohibited or restricted parties. You also acknowledge that the Services and Licensed
    Application may be subject to other U.S. and foreign laws and regulations governing the export of software by physical
    or electronic means. You agree to comply with all applicable U.S and foreign laws that apply to Rogers as well as end-user,
    end-use, and destination restrictions imposed by U.S. and foreign governments.\n\n
    WARRANTY DISCLAIMER\n\n
    THE SERVICES AND LICENSED APPLICATION PROVIDED HEREUNDER IS PROVIDED \"AS IS,\" AND ROGERS MAKES NO WARRANTIES,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE AND ANY SIMILAR WARRANTY WHETHER SAID WARRANTY ARISES UNDER PROVISIONS OF ANY LAW OF THE UNITED STATES OR ANY
    STATE THEREOF. ROGERS MAKES NO REPRESENTATIONS OR WARRANTIES THAT THE SOTWARE IS FREE OF RIGHTFUL CLAIMS OF ANY THIRD
    PARTY FOR INFRINGEMENT OF PROPRIETARY RIGHTS. THE ENTIRE RISK ASSOCIATED WITH THE USE OF THE SERVICES AND LICENSED
    APPLICATION SHALL BE BORNE SOLELY BY YOU.\n
    ROGERS MAKES NO WARRANTY THAT THE SERVICES AND LICENSED APPLICATION WILL MEET YOUR REQUIREMENTS, THAT ACCESS TO THE
    SERVICES WILL BE UNINTERRUPTED, TIMELY, SECURE, ERROR FREE, OR THAT ANY DEFECTS IN THE SERVICES AND LICENSED
    APPLICATION WILL BE CORRECTED. YOU ACKNOWLEDGE THAT ANY DATA OR INFORMATION DOWNLOADED OR OTHERWISE OBTAINED OR
    ACQUIRED THROUGH THE USE OF THE SERVICES AND LICENSED APPLICATION ARE AT YOUR SOLE RISK AND DISCRETION AND ROGERS
    WILL NOT BE LIABLE OR RESPONSIBLE FOR ANY DAMAGE TO YOU OR YOUR PROPERTY. YOU ACKNOWLEDGE THAT IT IS YOUR
    RESPONSIBILITY TO FOLLOW PROPER BACKUP PROCEDURES TO PROTECT AGAINST LOSS OR ERROR RESULTING FROM USE OF THE
    SERVICES AND LICENSED APPLICATION.\n
    NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM ROGERS, ITS EMPLOYEES, OR THROUGH OR FROM
    THE SERVICES SHALL CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THIS EULA.\n
    SOME JURISDICTIONS DO NOT PERMIT THE DISCLAIMER OF CERTAIN IMPLIED WARRANTIES, SO CERTAIN OF THE FOREGOING
    DISCLAIMERS MAY NOT APPLY TO YOU.\n
    IN THE EVENT OF ANY FAILURE OF THE SERVICES AND LICENSED APPLICATION TO CONFORM TO ANY APPLICABLE WARRANTY, YOU MAY
    NOTIFY ROGERS, AND ROGERS WILL REFUND THE PURCHASE PRICE, IF ANY, FOR THE LICENSED APPLICATION ONLY TO YOU. TO THE
    MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, ROGERS WILL HAVE NO OTHER WARRANTY OBLIGATION WHATSOEVER WITH RESPECT
    TO THE SERVICE AND LICENSED APPLICATION AND ANY OTHER CLAIMS, LOSSES, LIABILITIES, DAMAGES, COSTS OR EXPENSES
    ATTRIBUTABLE TO ANY FAILURE TO CONFORM TO ANY APPLICABLE WARRANTY WILL BE ROGERS’S SOLE RESPONSIBILITY. ROGERS DOES
    NOT HEREBY WAIVE OR LIMIT THE WARRANTY DISCLAIMERS SET FORTH ABOVE.\n\n
    LIMITATION OF LIABILITY\n\n
    IN NO EVENT SHALL ROGERS BE LIABLE TO YOU OR ANY THIRD PARTY FOR SPECIAL, INDIRECT, INCIDENTAL, ECONOMIC (INCLUDING,
    BUT NOT LIMITED TO LOST REVENUES OR LOST PROFITS) OR CONSEQUENTIAL DAMAGES WHETHER ARISING UNDER CONTRACT, WARRANTY,
    OR TORT (INCLUDING NEGLIGENCE OR STRICT LIABILITY) OR ANY OTHER THEORY OF LIABILITY. ROGERS’ TOTAL LIABILITY FOR ANY
    AND ALL DAMAGES, REGARDLESS OF THE FORM OF THE ACTION, SHALL BE LIMITED AND CAPPED IN THEIR ENTIRETY TO THE GREATER
    OF THE TOTAL AMOUNT PAID, IF ANY, BY YOU FOR THE LICENSED APPLICATION AND ANY MONTHLY FEES ROGERS CHARGED YOU FOR THE
    ROGERS HOSTED IP VOICE SERVICES DURING THE ONE (1) MONTH IMMMEDIATELY PRIOR TO THE DATE THAT THE EVENTS GIVING RISE
    TO THE ACTION OR CLAIM FIRST OCCURRED. THE LIMITATION OF LIABILITY REFLECTS THE ALLOCATION OF RISK BETWEEN THE PARTIES.
    THE LIMITATIONS SPECIFIED IN THIS SECTION WILL SURVIVE AND APPLY IN ANY AND ALL CIRCUMSTANCES. SOME JURISDICTIONS DO
    NOT ALLOW CERTAIN LIMITATIONS OF LIABILITY, SO CERTAIN OF THE FOREGOING LIMITATIONS MAY NOT APPLY TO YOU.\n\n
    INDEMNIFICATION\n\n
    You agree to indemnify and hold harmless Rogers, its directors, officers, employees, shareholders, and agents from
    and against all liabilities, losses, costs, expenses (including reasonable attorneys’ fees), and damages resulting
    from (1) any negligent acts, omissions or willful misconduct by you, (2) your use of the Services and Licensed
    Application, (3) any breach of this EULA by you, and/or (4) your violation of any law or of any rights of any third party.
    The provisions of this section are for the benefit of Rogers and its officers, directors, employees, agents, licensors,
    suppliers, and any third-party information providers. Each of these individuals or entities expressly retains the
    right to assert and enforce those provisions directly against you on its own behalf.\n
    You and Rogers acknowledge that, in the event of any third party claim that your possession and use of the Services
    or Licensed Application infringes that third party’s intellectual property rights, Rogers, not any Third-Party, will
    be solely responsible for the investigation, defense, settlement and discharge of any such intellectual property
    infringement claim.\n\n
    PRIVACY\n\n
    Rogers’s privacy policy is available on the website. By using the Service and Licensed Application, you consent to
    the collection, use and disclosure of personal information as set out in the privacy policy.\n\n
    NOTICES/CONTACT INFORMATION\n\n
    All communication with Rogers should specify your name and account information. Rogers’ contact information is as follows:\n
    Rogers Communications, 9th Floor, 333 Bloor Street East, Toronto, ON M4W 1G9.\n
    All legal notices from you to Rogers must be made in writing. Legal notice to us shall be effective when directed to our Legal Department and received at our address.\n\n
    GENERAL INFORMATION\n\n
    The laws of the province of Ontario, and the federal laws of Canada applicable therein, govern this EULA.\n
    You acknowledge that Rogers, not any Third Party, is responsible for addressing any claim by you or any third-party
    relating to the Licensed Application, or to your possession and/or use of the Licensed Application, including but
    not limited to: (i) product liability claims; (ii) any claim that the Licensed Application fails to conform to any
    applicable legal or regulatory requirement; and (iii) claims arising under consumer protection or similar legislation.\n
    If any part of this EULA is held invalid or unenforceable, that portion shall be construed to reflect the parties’
    original intent, and the remaining portions shall remain in full force and effect.\n
    The failure of Rogers to exercise or enforce any right or provision of this EULA shall not constitute a waiver of
    such right or provision.\n
    You agree not to transfer or assign this EULA or any of your rights under this EULA. Any purported transfer or
    assignment by you in violation of this section is void. Subject to the foregoing, this EULA shall be binding on and
    inure to the benefit of the parties, their successors, permitted assigns and legal representatives.\n
    The provisions of this EULA relating to intellectual property ownership, restrictions on use, disclaimers of
    warranties, limitations of liability and indemnification shall survive termination or expiration of this EULA for any reason.\n
    The section titles in this EULA are for convenience only and have no legal or contractual effect.\n\n
    DISPUTE RESOLUTION\n\n
    You agree that any disputes between you and Rogers shall be adjudicated in the provincial courts of Ontario, Canada.
    Venue for all claims and disputes between you and Rogers shall be the City of Toronto, Ontario. You hereby consent
    to venue and personal jurisdiction in such courts with respect to such claims or disputes and irrevocably waive any
    right that such party may have to assert that such forum is not convenient or that any such court lacks jurisdiction.
    Some jurisdictions do not allow governing law provisions, so certain of the foregoing governing law provisions may
    not apply to you.\n
    Notwithstanding the adjudication requirement above, for any dispute involving $25,000 or less, the party requesting
    relief may choose to resolve the dispute through binding, non-appearance-based arbitration (i.e., arbitration
    conducted online, through written filings, and/or via teleconference). Such arbitration shall be conducted through
    an established alternative dispute resolution service mutually agreed on by the parties, and any judgment rendered
    by the arbitrator may be entered in any court having jurisdiction. Some jurisdictions do not allow arbitration
    provisions, so certain of the foregoing arbitration provisions may not apply to you.\n\n
    OTHER LICENSES\n\n
    Portions of the Software may also be governed by the additional terms of certain software licenses, which are available
    here:\n
    &#8226; a. JSON Framework License\n
    &#8226; b. KissXML License\n
    &#8226; c. Global IP Solutions iLBC Public License\n
    &#8226; d. libSRTP License\n\n
    Copyright 2011 Rogers Communications Partnership. All rights reserved.
 </string>


</resources>
