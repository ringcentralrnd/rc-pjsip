/**
 * Copyright (C) 2010-2011, RingCentral, Inc. 
 * All Rights Reserved.
 */
package com.rcbase.parsers.sipmessage;
import com.rcbase.parsers.sipmessage.SipMessageHeaderInput;
import com.rcbase.parsers.sipmessage.SipMessageBodyInput;

parcelable SipMessageInput;