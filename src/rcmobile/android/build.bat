@echo off

REM ===============================================================
REM Copyright (C) 2011-2012, RingCentral, Inc. All Rights Reserved.
REM ===============================================================

REM ---------------------------------------------------------------
REM Usage:
REM
REM build					- build all brands
REM build -all  	    	- build all berands
REM build <brand_name>		- build selected brand only
REM ---------------------------------------------------------------

if not "%2"=="" goto INCORRECT_SYNTAX

setlocal
set ANT_HOME=./env/ant
set ANT_BIN=./env/ant/bin/ant
set ANT_OPTS= -Dlog4j.configuration=file:./env/log4j.properties
set LOG4J_LISTENER=-listener 

if "%1"=="" goto BUILD_ALL
if %1==-all goto BUILD_ALL

call %ANT_BIN% -Dbrand_code_name=%1 -f brands_build.xml
goto END

:BUILD_ALL
call %ANT_BIN% -Dbrand_code_name=ringcentral -f brands_build.xml
if ERRORLEVEL 1 goto END

call %ANT_BIN% -Dbrand_code_name=att -f brands_build.xml
if ERRORLEVEL 1 goto END

call %ANT_BIN% -Dbrand_code_name=rogers -f brands_build.xml
goto END

:INCORRECT_SYNTAX
echo build.bat: Incorrect syntax: Too many parameters

:END
pause
