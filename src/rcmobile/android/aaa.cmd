rem 将NDK目录中，build\awk\extract-pid.awk 复制到 RingCentral 源代码目录下
rem  进入 RingCentral 源码目录，执行这个命令

setlocal
rem 如果cygwin不在C:\cygwin，修改下面这句
set path=%path%;C:\cygwin\bin

echo Checking adb & device ...
adb devices

echo Killing gdbserver if exist ...
adb shell ps | gawk -f extract-pid.awk -v PACKAGE="gdbserver">tmp.pid
set /P PID= < tmp.pid
if %PID%e NEQ "e" (adb shell kill -9 %PID%) else (echo No gdbserver running)
adb shell ps | gawk -f extract-pid.awk -v PACKAGE="gdbserver">tmp.pid
set /P PID= < tmp.pid
if %PID%e NEQ "e" (adb shell kill -9 %PID%) else (echo No gdbserver running)

echo Searching service PID ...
adb shell ps | gawk -f extract-pid.awk -v PACKAGE="com.ringcentral.android:comm">tmp.pid
set /P PID= < tmp.pid
if %PID%e EQ "e" (echo Application NOT running!!!)

echo Launch gdbserver in a new terminal ...
echo When end debug, Ctrl-C if it do not quit by itself
start adb shell run-as com.ringcentral.android lib/gdbserver +debug-socket —attach %PID%

echo Launch bridge in a new terminal ...
echo When end debug, Ctrl-C if it do not quit by itself
start adb forward tcp:5555 localfilesystem:/data/data/com.ringcentral.android/debug-socket

echo Pulling needed files ...
adb pull /system/bin/app_process obj\local\armeabi-v7a
adb pull /system/bin/linker obj\local\armeabi-v7a
adb pull /system/lib/libc.so obj\local\armeabi-v7a

echo Creating gdbsetup file ...

echo set solib-search-path ./obj/local/armeabi-v7a > gdbsetup
echo dir jni/pjsip/src/pjsip >> gdbsetup
echo file obj/local/armeabi-v7a/app_process >> gdbsetup
echo target remote :5555 >> gdbsetup

echo Running GDB ...
F:\share\android\android-ndk-r8\toolchains\arm-linux-androideabi-4.4.3\prebuilt\windows\bin\arm-linux-androideabi-gdb.exe -x gdbsetup

endlocal
