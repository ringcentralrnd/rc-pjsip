LOCAL_PATH := $(call my-dir)/../../ilbc/

include $(CLEAR_VARS)
LOCAL_MODULE    := ilbc

LOCAL_C_INCLUDES := $(LOCAL_PATH)../../pjlib/include/ $(LOCAL_PATH)

LOCAL_CFLAGS := $(RC_PJSIP_LIB_FLAGS)
PJSIP_LIB_SRC_DIR := 

LOCAL_SRC_FILES := $(PJSIP_LIB_SRC_DIR)/FrameClassify.c $(PJSIP_LIB_SRC_DIR)/LPCdecode.c $(PJSIP_LIB_SRC_DIR)/LPCencode.c \
		   $(PJSIP_LIB_SRC_DIR)/StateConstructW.c $(PJSIP_LIB_SRC_DIR)/StateSearchW.c $(PJSIP_LIB_SRC_DIR)/anaFilter.c \
		   $(PJSIP_LIB_SRC_DIR)/constants.c $(PJSIP_LIB_SRC_DIR)/createCB.c $(PJSIP_LIB_SRC_DIR)/doCPLC.c \
		   $(PJSIP_LIB_SRC_DIR)/enhancer.c $(PJSIP_LIB_SRC_DIR)/filter.c $(PJSIP_LIB_SRC_DIR)/gainquant.c \
		   $(PJSIP_LIB_SRC_DIR)/getCBvec.c $(PJSIP_LIB_SRC_DIR)/helpfun.c $(PJSIP_LIB_SRC_DIR)/hpInput.c \
		   $(PJSIP_LIB_SRC_DIR)/hpOutput.c $(PJSIP_LIB_SRC_DIR)/iCBConstruct.c $(PJSIP_LIB_SRC_DIR)/iCBSearch.c \
		   $(PJSIP_LIB_SRC_DIR)/iLBC_decode.c $(PJSIP_LIB_SRC_DIR)/iLBC_encode.c $(PJSIP_LIB_SRC_DIR)/lsf.c \
		   $(PJSIP_LIB_SRC_DIR)/packing.c $(PJSIP_LIB_SRC_DIR)/syntFilter.c
		   
include $(BUILD_STATIC_LIBRARY)