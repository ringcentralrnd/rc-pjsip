LOCAL_PATH := $(call my-dir)/../../rcutils/

include $(CLEAR_VARS)
LOCAL_MODULE    := rcutils

LOCAL_C_INCLUDES := $(LOCAL_PATH)../../pjlib/include/ $(LOCAL_PATH)

LOCAL_CFLAGS := $(RC_PJSIP_LIB_FLAGS)
PJSIP_LIB_SRC_DIR := 

LOCAL_SRC_FILES := $(PJSIP_LIB_SRC_DIR)/wavhdr.c \
				$(PJSIP_LIB_SRC_DIR)/rtphdr.c 
		   
include $(BUILD_STATIC_LIBRARY)