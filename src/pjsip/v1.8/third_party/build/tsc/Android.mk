#hack by martin
ifeq ($(RC_PJSIP_USE_TSC),1)
LOCAL_PATH := $(call my-dir)/../../tsc

include $(CLEAR_VARS)
LOCAL_MODULE    := tsm_s
#LOCAL_ARM_MODE := arm

TSC_ROOT := lib
TSC_TPL := $(TSC_ROOT)/TPL
#TSC_TPL := $(LOCAL_PATH)/lib/TPL
TSC_TAPI := $(TSC_ROOT)/TAPI
TSC_OSAA := $(TSC_ROOT)/OSAA
TSC_CSM := $(TSC_ROOT)/CSM
TSC_EIP := $(TSC_ROOT)/EIP
TSC_QOS := $(TSC_ROOT)/QOS

TSC_PESQ := $(TSC_ROOT)/QOS/pesq

TSC_UIP := $(TSC_EIP)/uip-1.0



LOCAL_CFLAGS := -D TSC_ANDROID -D TSC_LINUX -D TSC_UIP  -D TSC_REDUNDANCY -D TSC_MONITOR

TSC_LIB_INCLUDES := $(LOCAL_PATH)/$(TSC_ROOT)/include $(LOCAL_PATH)/$(TSC_CSM)/include $(LOCAL_PATH)/$(TSC_EIP)/include \
                    $(LOCAL_PATH)/$(TSC_OSAA)/include $(LOCAL_PATH)/$(TSC_TAPI)/include $(LOCAL_PATH)/$(TSC_TPL)/include \
                    $(LOCAL_PATH)/$(TSC_UIP) $(LOCAL_PATH)/$(TSC_UIP)/uip $(LOCAL_PATH)/$(TSC_UIP)/tsc $(LOCAL_PATH)/$(TSC_ROOT)/../extlib/openssl-1.0.0e/include \
		    $(LOCAL_PATH)/$(TSC_QOS)/include $(LOCAL_PATH)/include

LOCAL_C_INCLUDES += $(TSC_LIB_INCLUDES)

TSC_LIB_SRC_FILES :=$(TSC_TPL)/src/tsc_encoder.c $(TSC_TAPI)/src/tsc_log.c \
                   $(TSC_TAPI)/src/tsc_utils.c $(TSC_OSAA)/src/tsc_lock.c \
                   $(TSC_OSAA)/src/tsc_ssl.c $(TSC_OSAA)/src/tsc_queue.c $(TSC_OSAA)/src/tsc_thread.c \
                   $(TSC_OSAA)/src/tsc_tunnel_socket.c $(TSC_EIP)/src/tsc_ip.c \
                   $(TSC_EIP)/src/tsc_udp.c $(TSC_CSM)/src/tsc_control.c $(TSC_CSM)/src/tsc_redundancy.c \
                   $(TSC_QOS)/src/tsc_network.c $(TSC_CSM)/src/tsc_data.c $(TSC_CSM)/src/tsc_statistics.c  $(TSC_CSM)/src/tsc_csm.c \
                   $(TSC_CSM)/src/tsc_transaction.c $(TSC_UIP)/uip/uip.c $(TSC_UIP)/uip/uip_arp.c \
                   $(TSC_UIP)/uip/uiplib.c $(TSC_UIP)/uip/psock.c \
                   $(TSC_UIP)/uip/timer.c $(TSC_UIP)/uip/uip-neighbor.c \
		   $(TSC_QOS)/src/tsc_vq.c $(TSC_PESQ)/pesqdsp.c $(TSC_PESQ)/pesqio.c \
		   $(TSC_PESQ)/pesqmod.c $(TSC_PESQ)/pesqmain.c $(TSC_PESQ)/dsp.c

LOCAL_SRC_FILES += $(TSC_LIB_SRC_FILES)

LOCAL_LDLIBS := -llog $(LOCAL_PATH)/../libssltsc.so $(LOCAL_PATH)/../libcryptotsc.so

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)

TSC_ROOT := lib

TSC_TPL := $(TSC_ROOT)/TPL
TSC_TAPI := $(TSC_ROOT)/TAPI
TSC_OSAA := $(TSC_ROOT)/OSAA
TSC_CSM := $(TSC_ROOT)/CSM
TSC_EIP := $(TSC_ROOT)/EIP
TSC_QOS := $(TSC_ROOT)/QOS

TSC_PESQ := $(TSC_ROOT)/QOS/pesq

TSC_UIP := $(TSC_EIP)/uip-1.0

LOCAL_MODULE    := tsm
#LOCAL_ARM_MODE := arm

LOCAL_CFLAGS := -D TSC_ANDROID -D TSC_LINUX -D TSC_UIP  -D TSC_REDUNDANCY -D TSC_MONITOR

LOCAL_STATIC_LIBRARIES := libtsm_s

TSC_LIB_INCLUDES :=$(LOCAL_PATH)/$(TSC_ROOT)/include $(LOCAL_PATH)/$(TSC_CSM)/include $(LOCAL_PATH)/$(TSC_EIP)/include \
                    $(LOCAL_PATH)/$(TSC_OSAA)/include $(LOCAL_PATH)/$(TSC_TAPI)/include $(LOCAL_PATH)/$(TSC_TPL)/include \
                    $(LOCAL_PATH)/$(TSC_UIP) $(LOCAL_PATH)/$(TSC_UIP)/uip $(LOCAL_PATH)/$(TSC_UIP)/tsc $(LOCAL_PATH)/$(TSC_ROOT)/../extlib/openssl-1.0.0e/include \
		    $(LOCAL_PATH)/$(TSC_QOS)/include $(LOCAL_PATH)/include

LOCAL_C_INCLUDES +=$(TSC_LIB_INCLUDES)


TSC_LIB_SRC_FILES :=$(TSC_TPL)/src/tsc_encoder.c $(TSC_TAPI)/src/tsc_log.c \
                   $(TSC_TAPI)/src/tsc_utils.c $(TSC_OSAA)/src/tsc_lock.c \
                   $(TSC_OSAA)/src/tsc_ssl.c $(TSC_OSAA)/src/tsc_queue.c $(TSC_OSAA)/src/tsc_thread.c \
                   $(TSC_OSAA)/src/tsc_tunnel_socket.c $(TSC_EIP)/src/tsc_ip.c \
                   $(TSC_EIP)/src/tsc_udp.c $(TSC_CSM)/src/tsc_control.c $(TSC_CSM)/src/tsc_redundancy.c \
                   $(TSC_CSM)/src/tsc_data.c  $(TSC_QOS)/src/tsc_network.c $(TSC_CSM)/src/tsc_statistics.c $(TSC_CSM)/src/tsc_csm.c \
                   $(TSC_CSM)/src/tsc_transaction.c $(TSC_UIP)/uip/uip.c $(TSC_UIP)/uip/uip_arp.c \
                   $(TSC_UIP)/uip/uiplib.c $(TSC_UIP)/uip/psock.c \
                   $(TSC_UIP)/uip/timer.c $(TSC_UIP)/uip/uip-neighbor.c \
		   $(TSC_QOS)/src/tsc_vq.c $(TSC_PESQ)/pesqdsp.c $(TSC_PESQ)/pesqio.c \
		   $(TSC_PESQ)/pesqmod.c $(TSC_PESQ)/pesqmain.c $(TSC_PESQ)/dsp.c
LOCAL_SRC_FILES +=$(TSC_LIB_SRC_FILES) 

LOCAL_LDLIBS := -llog $(LOCAL_PATH)/lib/android/libssltsc.so $(LOCAL_PATH)/lib/android/libcryptotsc.so

include $(BUILD_SHARED_LIBRARY)

endif