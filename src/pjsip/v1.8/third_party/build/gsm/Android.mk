LOCAL_PATH := $(call my-dir)/../../gsm
include $(CLEAR_VARS)
LOCAL_MODULE    := gsm

LOCAL_C_INCLUDES := $(LOCAL_PATH)../../pjlib/include/ $(LOCAL_PATH) $(LOCAL_PATH)/inc 

LOCAL_CFLAGS := -DSASR -DWAV49 -DNeedFunctionPrototypes=1 $(RC_PJSIP_LIB_FLAGS) 
PJSIP_LIB_SRC_DIR := src

LOCAL_SRC_FILES := $(PJSIP_LIB_SRC_DIR)/add.c $(PJSIP_LIB_SRC_DIR)/code.c $(PJSIP_LIB_SRC_DIR)/decode.c \
		   $(PJSIP_LIB_SRC_DIR)/gsm_create.c $(PJSIP_LIB_SRC_DIR)/gsm_decode.c $(PJSIP_LIB_SRC_DIR)/gsm_destroy.c \
		   $(PJSIP_LIB_SRC_DIR)/gsm_encode.c $(PJSIP_LIB_SRC_DIR)/gsm_explode.c $(PJSIP_LIB_SRC_DIR)/gsm_implode.c \
		   $(PJSIP_LIB_SRC_DIR)/gsm_option.c $(PJSIP_LIB_SRC_DIR)/long_term.c $(PJSIP_LIB_SRC_DIR)/lpc.c \
		   $(PJSIP_LIB_SRC_DIR)/preprocess.c $(PJSIP_LIB_SRC_DIR)/rpe.c $(PJSIP_LIB_SRC_DIR)/short_term.c \
		   $(PJSIP_LIB_SRC_DIR)/table.c
		   
include $(BUILD_STATIC_LIBRARY)