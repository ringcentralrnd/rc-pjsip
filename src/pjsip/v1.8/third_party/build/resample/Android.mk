LOCAL_PATH := $(call my-dir)/../../resample/

include $(CLEAR_VARS)
LOCAL_MODULE    := resample

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../pjlib/include/ $(LOCAL_PATH)/include/ \
		   $(LOCAL_PATH)/../build/resample/

LOCAL_CFLAGS := $(RC_PJSIP_LIB_FLAGS)
PJSIP_LIB_SRC_DIR := src

LOCAL_SRC_FILES := $(PJSIP_LIB_SRC_DIR)/resamplesubs.c


include $(BUILD_STATIC_LIBRARY)
