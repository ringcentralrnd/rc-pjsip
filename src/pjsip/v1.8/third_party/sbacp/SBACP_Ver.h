/******************************************************************************
*
*   Filename:       SBACP_Ver.h
*
*   Description:    SUBBAND AUDIO CONFERENCE PROCESSING - VERSION DEFINITION
*
*   Copyright (C) 2003-2011 -- ASDSP S.r.l. -- www.asdsp.com
*
*******************************************************************************/

#ifndef __SBACP_VER_H
#define __SBACP_VER_H

#define SBACP_VER      "01.22V"
#define SBACP_DATE     "08/22/11"

#endif // __SBACP_VER_H

/******************************************************************************
*
*   Copyright (C) 2003-2011 -- ASDSP S.r.l. -- www.asdsp.com
*
*******************************************************************************/
