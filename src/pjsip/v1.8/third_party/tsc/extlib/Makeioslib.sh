### Create directories
cd openssl-1.0.0e/

rm -r ../openssl_*
mkdir ../openssl_i386/
mkdir ../openssl_i386/lib

mkdir ../openssl_armv6/
mkdir ../openssl_armv6/lib

mkdir ../openssl_armv7/
mkdir ../openssl_armv7/lib

### Build i386 version for simulator
make clean
./Configure BSD-generic32 

sed -e 's/CC= gcc/CC= \/Applications\/Xcode.app\/Contents\/Developer\/Platforms\/iPhoneSimulator.platform\/Developer\/usr\/bin\/gcc -arch i386/g' -i '' Makefile

sed -e 's/CFLAG= -DOPENSSL_THREADS/CFLAG= -isysroot \/Applications\/Xcode.app\/Contents/\/Developer\/Platforms\/iPhoneSimulator.platform\/Developer\/SDKs\/iPhoneSimulator5.1.sdk -DOPENSSL_THREADS/g' -i '' Makefile

make
cp libcrypto.a libssl.a ../openssl_i386/lib
make clean



### Build armv6 version for older iPhones


./Configure BSD-generic32 

sed -e 's/CC= gcc/CC= \/Applications\/Xcode.app\/Contents\/Developer\/Platforms\/iPhoneOS.platform\/Developer\/usr\/bin\/gcc -arch armv6/g' -i '' Makefile

sed -e 's/CFLAG= -DOPENSSL_THREADS/CFLAG= -isysroot  \/Applications\/Xcode.app\/Contents\/Developer\/Platforms\/iPhoneOS.platform\/Developer\/SDKs\/iPhoneOS5.1.sdk -DOPENSSL_THREADS/g' -i '' Makefile

make

cp libcrypto.a libssl.a ../openssl_armv6/lib
make clean



### Build armv7 for iPhone/iPad

./Configure BSD-generic32 

sed -e 's/CC= gcc/CC= \/Applications\/Xcode.app\/Contents\/Developer\/Platforms\/iPhoneOS.platform\/Developer\/usr\/bin\/gcc -arch armv7/g' -i '' Makefile

sed -e 's/CFLAG= -DOPENSSL_THREADS/CFLAG= -isysroot \/Applications\/Xcode.app\/Contents\/Developer\/Platforms\/iPhoneOS.platform\/Developer\/SDKs\/iPhoneOS5.1.sdk -DOPENSSL_THREADS/g' -i '' Makefile

make

cp libcrypto.a libssl.a ../openssl_armv7/lib
rm *.a
cd ..

### Create universial library
lipo -create openssl_i386/lib/libcrypto.a openssl_armv6/lib/libcrypto.a openssl_*armv7/lib/libcrypto.a -output libcrypto.a
lipo -create openssl_i386/lib/libssl.a openssl_armv6/lib/libssl.a openssl_armv7/lib/libssl.a -output libssl.a
cp *.a openssl-1.0.0e
rm -rf openssl_*
rm *.a

