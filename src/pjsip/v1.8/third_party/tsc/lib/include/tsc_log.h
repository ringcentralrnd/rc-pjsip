/*
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/
/*! \brief
* Logging facilities
*/

/** @file
 * This file is part of TSC Client API
 * and defines logging facility
 *
 */

#include "tsc_common.h"
#include "tsc_utils.h"

#ifndef __TSC_LOG_H__
#define __TSC_LOG_H__

#ifdef __cplusplus
extern "C" {
#endif /* */
#define TSC_ERROR(...) tsc_log(TSC_LOG_SUBSYSTEM, tsc_log_level_error, __LINE__, __VA_ARGS__)
#define TSC_NOTICE(...) tsc_log(TSC_LOG_SUBSYSTEM, tsc_log_level_notice, __LINE__, __VA_ARGS__)
#define TSC_DEBUG(...) tsc_log(TSC_LOG_SUBSYSTEM, tsc_log_level_debug, __LINE__, __VA_ARGS__)
#define TSC_TRACE(...) tsc_log(TSC_LOG_SUBSYSTEM, tsc_log_level_trace, __LINE__, __VA_ARGS__)

/*! \typedef tsc_log_level
* log importance levels
these log levels can be used in applications to decide sensitivity of the messsage being logged
one can set up a global_log_level and calls to tsc_log(...) only with a log-level greater than global_log_level will be logged
logging being an expensive operation, less important log calls can be made using a lower tsc_log_level and thus avoide by setting up a higher global_log_level
the sdk uses only 4 of these log levels currently - error/notice/debug/trace.tsc_log_level_trace being the highest 
*/
typedef enum
{ tsc_log_level_disabled = 0,   /* no logs are saved */
    tsc_log_level_critical = 2,
    tsc_log_level_error = 3,    /* only shows errors */
    tsc_log_level_warning = 4,
    tsc_log_level_notice = 5,
    tsc_log_level_debug = 7,    /* shows everything but packet information */
    tsc_log_level_info = 8,
    tsc_log_level_trace = 9     /* contains all logging information,
                                   including packet dumps */
} tsc_log_level;

/*! /def
The subsystems enable you to understand the origin of issues from the log file 
ENCODER - Encoding related issues- either message wasn't constructed properly or response was not parsed
STATE MACHINE - Tunneling Client State Machine Issue - Tunnel Connection Issues/ Control API issues
OSAA - OS Application Adaptation API issue - socket read/ write queues, locks, creation/ connection/ polling issues with tunnel sockets etc. 
QOS - Speech Quality related issues
*/
#define TSC_LOG_SUBSYSTEM_GENERAL 1
#define TSC_LOG_SUBSYSTEM_ENCODER 2
#define TSC_LOG_SUBSYSTEM_STATE_MACHINE 4
#define TSC_LOG_SUBSYSTEM_OSAA 8
#define TSC_LOG_SUBSYSTEM_QOS 16

/*! \typedef tsc_log_func
* Log callback prototype
This function provides a prototype for implementing your own logging callback function. This can be used for outputting log messages 
on screen in OOP programs that don't use file streams to output text. Specifically this can be used to display log messages onscreen on a mobile
device.
*/
typedef void (*tsc_log_func)(tsc_log_level log_level, const char *format, va_list args);

/** @addtogroup TSCServiceAPI*/
/** @{*/

/* Logging */
/*!
*Purpose: Writes to the log file details passed in format and arguments specifying the log subsystem and log importance level \n
Every time a change happens to the state machine (exchange of config messages, data messages, rtp requests, or even fetching info from handle) info is entered to the log file 
for debug purposes along with the line number, function name and current time.For all errors, notices, debug and info look into the log file set using tsc_set_log_level. \n
Calls to this function are made through macros TSC_DEBUG, TSC_ERROR, TSC_TRACE, TSC_NOTICE
@param subsystem \n Type: uint32_t.\n Use: subsystem for which to log
@param log_level \n Type:\ref tsc_log_level.\n Use: log importance levels
@param line \n Type: uint32_t.\n Use: line no
@param format \n Type: string.\n Use: logging format. contains the function name that initiated the logging call.
@return Type: \ref tsc_error_code.\n Returns tsc_error_code_ok if logging successful
*/
tsc_error_code tsc_log (uint32_t subsystem, tsc_log_level log_level,
                        uint32_t line, const char *format, ...);

/*!
*Purpose: This function could be used to enable logging at certain levels enumerated in tsc_log_level. \n
The higher the log level, the more information is logged. The tsc_log_level names are descriptive of their usage.
@param log_level \n Type:\ref tsc_log_level.\n Use: log level to be enabled
@return Type: \ref tsc_error_code.\n Returns 0 if task successful 
*/
tsc_error_code tsc_set_log_level (tsc_log_level log_level);

/*!
*Purpose: This function returns the current log level.\n
The higher the log level, the more information is logged. The tsc_log_level names are descriptive of their usage.
@return Type: \ref tsc_log_level.\n Returns current log level
*/
tsc_log_level tsc_get_log_level ();

/*!
* Purpose: Sets the output file stream for logging. \n 
Calls to tsc_log would add log-comments into this file. Can also set to stdout/stderr for console level debug.
@param log_fd \n Type: FILE pointer.\n Use: file stream to log into.
@return Type: \ref tsc_error_code.\n Returns tsc_error_code_ok if filestream setting successful
*/
tsc_error_code tsc_set_log_output (FILE * log_fd);

/*!
* Purpose: Sets the callback function pointer \n 
Calls to tsc_log would also call the function pointer passed as the argument. The function pointer must fit the model specified above
@param logfunction \n Type:Function pointer cast as a tsc_log_func.\n Use: function to use as a callback for log messages
@return Type: \ref tsc_error_code.\n Returns tsc_error_code_ok if callback setting is successful
*/
tsc_error_code tsc_set_log_handler(tsc_log_func logfunction);
/** @}*/
#ifdef __cplusplus
}
#endif /* */
#endif /* __TSC_LOG_H__ */
