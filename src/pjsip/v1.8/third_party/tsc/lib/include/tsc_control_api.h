/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/
/*! \brief
* Tunnel management API
*/

/** @file
 * This file is part of TSC Client API
 * and defines Client control API
 *
 */

#ifndef __TSC_CONTROL_H__
#define __TSC_CONTROL_H__

#ifdef __cplusplus
extern "C" {
#endif
#include "tsc_tunnel.h"

/** @addtogroup TSCTunnelManagementAPI */
/** @{ */

/* Initialize API */
/*!
Initialize API
@return Type: \ref tsc_error_code.\n Returns 0 if Initialization successful.
*/
tsc_error_code tsc_ctrl_init ();

/* Creates a new handle to client tunnel state-machine*/
/*!
Creates a new handle to client tunnel state-machine \n
Creates a new connection(state: connection),sends config messages and parses received config message(state: negotiaition)
to enable setting up of a tunnel(state:established)
@param tunnel_params \n Type: tsc_tunnel_params variable.\n Use: Set tscf server parameters for connection setup
@param requested_config \n Type: tsc_requested_config variable.\n Use: Requested config will contain a tunnel id. Can be set with a previous tunnel ID to attempt to bring an old tunnel back up.
@return Type: \ref tsc_handle.\n Handle to newly created tunnel. contains information including connection time, state info, internal ip addr, socket info etc.
*/
tsc_handle tsc_ctrl_new_tunnel (tsc_tunnel_params * tunnel_params,
                                tsc_requested_config * requested_config);


/* Return state of client tunnel state-machine*/
/*!
this function could be used to read the current state info from the handle \n
The state info is comprised of TunnelState- Connected, Established, Negotiating, Fatal Error etc. and Error Code Info(ok or error) 
@param handle \n Type: \ref tsc_handle.\n Use: socket handle
@param state_info \n Type: tsc_state_info variable.\n Use: Get state info in this variable.Value changes on return.
@return Type: \ref tsc_error_code.\n Returns tsc_error_code_ok if state fetched, else return tsc_error_code_error
*/
tsc_error_code tsc_ctrl_get_state (tsc_handle handle,
                                   tsc_state_info * state_info);

/*!
Once we have an established handle, this function could be used to read the current config info from the handle
@param handle \n Type: \ref tsc_handle.\n Use: socket handle
@param config \n Type: tsc_config variable.\n Use: Get config info in this variable. Value changes on return. Config info will contain tunnel IP address as well as other information.
@return Type: \ref tsc_error_code.\n Returns tsc-error_code_ok if configuration fetched else return tsc_error_code_error 
*/
/* Get client tunnel configuration information (after tunnel is established)*/
tsc_error_code tsc_ctrl_get_config (tsc_handle handle, tsc_config * config);

/*!
Terminates Client tunnel releasing all resources. \n
The Connection with the server is closed and all information stored in the handle is reset.
@param handle \n Type: \ref tsc_handle.\n Use: handle  to tunnel being closed. Value changes on return.
@return Type: \ref tsc_error_code.\n Returns tsc_error_code_ok if deletion of tunnel successful, else return tsc_error_code_error
*/
/* Terminates client tunnel, releases all resources*/
tsc_error_code tsc_ctrl_delete_tunnel (tsc_handle handle);

/** }@*/
#ifdef __cplusplus
}
#endif
#endif /* __TSC_CONTROL_H__ */
