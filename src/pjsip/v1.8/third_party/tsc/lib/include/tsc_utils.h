/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/*! \brief
*basic OS independent utility functions
*/

/** @file
 * This file is part of TSC Client API
 * and defines various utilities
 *
 */
#include "tsc_common.h"

#ifndef __TSC_UTILS_H__
#define __TSC_UTILS_H__

#define TSC_ADDR_STR_LEN 0x100
#define TSC_BUFFER_LEN 0x4000

#define DIV_AND_CEIL(x,y) ((x/y)*y != x) ? x/y+1 : x/y

#define TSC_HEXDUMP(data, dataSize, str, head, tail)			\
    if(tsc_get_log_level() == tsc_log_level_trace){			\
	tsc_hexdump(data, dataSize, str, head, tail);			\
    }									\
    

#ifdef __cplusplus
extern "C" {
#endif

/** @addtogroup TSCServiceAPI**/
/** @{*/

/*!
* Purpose: converts tsc_ip_port_address to string
* can be used to convert ipaddress to the format xxx.xxx.xxx.xxx:xxx 
@param addr \n Type: tsc_ip_port_address variable.\n tsc ip port address
@param str \n Type: string.\n string to be filled
@param max_len \n Type: size_t. \n 
@return Type: \ref tsc_bool.\n 
*/
tsc_bool tsc_ip_port_address_to_str (tsc_ip_port_address * addr, char *str,
                                     size_t max_len);

/*!
* Purpose: converts string to tsc_ip_port_address structure
* can be used to convert 192.168.1.1:5060 to properly filled tsc_ip_port_address structure
@param str \n Type: String formatted as IP Address: Port
@param addr \n Type: string.\n tsc_ip_port_address struct to be filled
@param max_len \n Type: size of str buffer
@return Type: \ref tsc_bool.\n 
*/
tsc_bool
tsc_str_to_ip_port_address (char *str, tsc_ip_port_address * addr, size_t max_len);
    
/*!
* Purpose: coverts tsc_ip_address to string
@param addr\n Type: tsc_ip_address variable. \n tsc ip address
@param str \n Type: string.\n string to be filled
@param max_len \n Type: size_t. \n 
@return Type: tsc_bool.\n 
*/
tsc_bool tsc_ip_address_to_str (tsc_ip_address * addr, char *str,
                                size_t max_len);

/*!
* Purpose: converts tsc_ip_mask to string
@param addr\n Type: tsc_ip_mask variable. \n ip mask
@param str \n Type: string.\n string to be filled
@param max_len \n Type: size_t. \n  
@return Type: tsc_bool.\n 
*/
tsc_bool tsc_ip_mask_to_str (tsc_ip_mask * addr, char *str, size_t max_len);


/*!
* Purpose: do a hexdump of a buffer, used for logging purposes
@param data \n Type: void pointer \n buffer to do hexdump from
@param datasize\n Type: int. \n size(data)
@param str \n Type: string.\n pointer to a buffer to fill the data 
@param head \n Type: int. \n buffer head
@param tail \n Type: int. \n buffer tail
@return Type:  char pointer.\n 
*/
char *tsc_hexdump (void *data, int dataSize, char *str, int head, int tail);

/*!
* Purpose: OS independent implementation of UNIX time() function
@return Type: time_t.\n  
*/
time_t tsc_time ();

/*!
* Purpose: milliseconds since the computer started
@return Type: uint32_t.\n 
*/
uint32_t tsc_get_clock ();

/*!
* Purpose: sleep for ms milliseconds
@param ms \n Type: uint32_t. \n milliseconds to sleep for
@return Type: tsc_bool.\n
*/
tsc_bool tsc_sleep (uint32_t ms);

/*!
* Purpose: set socket errno value
@param tmp \n Type: int. \n errno value
@return Type: tsc_bool.\n 
*/
tsc_bool tsc_set_errno (int tmp);

/*!
* Purpose: retrieve socket errno value
@return Type: int.\n socket errno
*/
int tsc_get_errno ();

/*!
* Purpose: OS independent implementation of UNIX inet_pton
@param af \n Type: int. \n address family
@param src \n Type: char pointer. \n presentation format address to convert 
@param dst \n Type: void pointer. \n store converetd address here
@return Type: int.\n return 1 on success
*/
int tsc_inet_pton (int af, const char *src, void *dst);

/*!
* Purpose: OS independent implementation of UNIX inet_ntop
@param af \n Type: int. \n address family
@param src \n Type: const void pointer. \n network format address to convert
@param dst \n Type: char pointer. \n store converted address here
@param cnt \n Type: socklen_t. \n 
@return Type: const char pointer.\n pointer to buffer containing text string, if successful 
*/
const char *tsc_inet_ntop (int af, const void *src, char *dst, socklen_t cnt);

/*!
* Purpose: base 64 conversion for len
@param len \n Type: uint32_t. \n to encode
@return Type: uint32_t.\n base 64 encoded value for len
*/
uint32_t tsc_base64_encode_len (uint32_t len);

/*!
* Purpose: encode using base 64
* to make binary data survive transport through transport layers that are not 8- bit clean
@param buffer \n Type: uint8_t pointer. \n pointer to buffer conatining text to encode
@param encoded \n Type: char pointer. \n pointer to buffer conatining base 64 encoded ascii chars
@param len \n Type: uint32_t. \n length of buffer 
@return Type: uint32_t. \n length of base 64 encoded data
*/
uint32_t tsc_base64_encode (char *encoded, const uint8_t * buffer,
                            uint32_t len);

/*!
* Purpose: run pesq score between reference pcm filename and filename
*/
tsc_bool tsc_run_pesq (char *ref_filename, char *filename, 
                       void (*callback)(void *, double pesq), void *opaque);

/** @}*/
#ifdef __cplusplus
}
#endif
#endif /* __TSC_UTILS_H__ */
