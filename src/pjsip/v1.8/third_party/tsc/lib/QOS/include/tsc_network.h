/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/
/*! \brief
low level tunnel API
*/

/** @file
 * This file is part of TSC Client API
 * and defines low level tunnel API
 *
 */

#ifndef __TSC_NETWORK_H__
#define __TSC_NETWORK_H__

#include "tsc_lock.h"
#include "tsc_thread.h"

#include "tsc_socket_api.h"

#define TSC_NETWORK_DURATION 10
#define TSC_NETWORK_PERIOD 500
#define TSC_NETWORK_FRAME_SIZE 172 /* 12 for RTP + 160 for G711 */
#define TSC_NETWORK_AVG_LEN 3

#define TSC_NETWORK_QUALITY_RANGE 3

#ifdef TSC_WINDOWS
#define TSC_RANDOM(x) (int32_t)((long)rand()*x/RAND_MAX)
#else
#define TSC_RANDOM(x) (int32_t)((long long)rand()*x/RAND_MAX)
#endif

#define TSC_NETWORK_BURST_1_SIZE 200
#define TSC_NETWORK_BURST_2_SIZE 500

typedef struct
{
    tsc_handle tunnel_handle;
    uint32_t packet_count;
    uint32_t *latency;
    uint32_t *jitter;
    uint32_t burst_1_count;
    uint32_t avg_burst_1_size;
    float burst_1_ratio;
    uint32_t burst_2_count;
    uint32_t avg_burst_2_size;
    float burst_2_ratio;
    uint32_t loss_count;
    float loss_ratio;
    void *opaque;
} tsc_network_data;

typedef struct
{
    tsc_thread *read;
    tsc_thread *write;
    tsc_lock *data_lock;
    tsc_handle tunnel_handle;
    size_t frame_size;
    void (*monitor_callback)(tsc_network_data *);
    int socket;
    tsc_bool done;
    uint32_t packet_count;
    uint32_t *tx_timestamp;
    uint32_t *rx_timestamp;
    uint32_t local_port;
    void *opaque;
} tsc_network_info;

tsc_error_code tsc_network_monitor(tsc_handle tunnel_handle,
                                   size_t frame_size, time_t duration, void *opaque,
                                   void (*monitor_callback)(tsc_network_data *));

#endif /* __TSC_NETWORK_H__ */
