/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#define TSC_LOG_SUBSYSTEM TSC_LOG_SUBSYSTEM_QOS

#include "tsc_network.h"

void *
network_read (void *arg)
{
    tsc_network_info *network_info = (tsc_network_info *)arg;

    TSC_DEBUG ("network_read: thread started [%p]", network_info->tunnel_handle);

    for (;;) {
        if (tsc_lock_get (network_info->data_lock) == tsc_lock_response_ok) {
            if (network_info->done == tsc_bool_true) {
                tsc_lock_release (network_info->data_lock);

                break;
            }

            struct tsc_timeval timeout;
            timeout.tv_sec = 0;
            timeout.tv_usec = 0;

            tsc_fd_set read_flags;
            TSC_FD_ZERO (&read_flags);
            TSC_FD_SET (network_info->socket, &read_flags);

            int result =
                 tsc_select (network_info->socket + 1, &read_flags,
                             NULL, NULL, &timeout);

            if (result > 0) {
                uint8_t data[TSC_MAX_FRAME_SIZE];
                uint32_t size = TSC_MAX_FRAME_SIZE;

                struct sockaddr from;
                socklen_t fromlen;

                uint32_t sequence = 0;

                int len =
                    tsc_recvfrom (network_info->socket, (char *) data,
                                  size, 0, &from, &fromlen);

                if (len > 0) {
                    sequence = data[0] | (data[1] << 8);

                    if (!network_info->rx_timestamp[sequence]) {
                        network_info->rx_timestamp[sequence] = tsc_get_clock();
                    }

                    if (sequence == network_info->packet_count - 1) {
                        network_info->done = tsc_bool_true;
                    }
                }
            }

            tsc_lock_release (network_info->data_lock);
        }

        tsc_sleep(1);
    }

    if (network_info->monitor_callback) {
        tsc_network_data data;
        memset(&data, 0, sizeof(tsc_network_data));
        data.packet_count = network_info->packet_count - 1;
        data.tunnel_handle = network_info->tunnel_handle;
        data.latency = (uint32_t *)malloc(sizeof(uint32_t)*data.packet_count);
        data.jitter = (uint32_t *)malloc(sizeof(uint32_t)*data.packet_count);
        data.opaque = network_info->opaque;
        memset(data.latency, 0, sizeof(uint32_t)*data.packet_count);
        memset(data.jitter, 0, sizeof(uint32_t)*data.packet_count);

        uint32_t i = 0;

        for (i = 0; i < data.packet_count; i++) {
            if (network_info->tx_timestamp[i] && network_info->rx_timestamp[i]) {
                data.latency[i] = network_info->rx_timestamp[i] - 
                                  network_info->tx_timestamp[i];

                if (i > 0 && network_info->rx_timestamp[i-1]) {
                    data.jitter[i] = network_info->rx_timestamp[i] - 
                                     network_info->rx_timestamp[i - 1];
                }

                if (data.latency[i] > TSC_NETWORK_BURST_1_SIZE) {
                    data.burst_1_count++;
                    data.avg_burst_1_size += data.latency[i];
                }

                if (data.latency[i] > TSC_NETWORK_BURST_2_SIZE) {
                    data.burst_2_count++;
                    data.avg_burst_2_size += data.latency[i];
                }
            } else {
                data.loss_count++;
            }
        }

        if (data.burst_1_count > 0) {
            data.avg_burst_1_size /= data.burst_1_count;

            data.burst_1_ratio = 100.0*(float)data.burst_1_count / (float)data.packet_count;
        }

        if (data.burst_2_count > 0) {
            data.avg_burst_2_size /= data.burst_2_count;

            data.burst_2_ratio = 100.0*(float)data.burst_2_count / (float)data.packet_count;
        }

        if (data.loss_count > 0) {
            data.loss_ratio = 100.0*(float)data.loss_count / (float)data.packet_count;
        }

        network_info->monitor_callback(&data);

        free(data.latency);
        free(data.jitter);
    }

    TSC_DEBUG ("network_read: thread terminated [%p]", network_info->tunnel_handle);

    tsc_close(network_info->socket);
    tsc_lock_delete(network_info->data_lock);

    return 0L;
}

void *
network_write (void *arg)
{
    tsc_network_info *network_info = (tsc_network_info *)arg;

    TSC_DEBUG ("network_write: thread started [%p]", network_info->tunnel_handle);

    srand (tsc_time ());

    network_info->socket = tsc_socket (network_info->tunnel_handle, AF_INET, SOCK_DGRAM, 0);

    tsc_config config;

    if (tsc_get_config (network_info->tunnel_handle, &config) ==
                        tsc_error_code_error) {
        tsc_close (network_info->socket);

        TSC_ERROR("network_write: failed to retrieve config %d [%p]",
                  network_info->socket, network_info->tunnel_handle);

        return 0L;
    }

    struct sockaddr_in addr;
    memset (&addr, 0, sizeof (struct sockaddr_in));
    addr.sin_family = AF_INET;
    network_info->local_port = TSC_RANDOM (0x7fff) + 1024;
    addr.sin_port = htons (network_info->local_port);
    addr.sin_addr.s_addr =
                  htonl (config.internal_address.address);

    if (!tsc_bind(network_info->socket, (struct sockaddr *) (&addr),
                  sizeof (struct sockaddr_in))) {
        
    } else {
        tsc_close (network_info->socket);

        TSC_ERROR("network_write: failed to bind socket %d [%p]",
                  network_info->socket, network_info->tunnel_handle);

        return 0L;
    }

    network_info->done = tsc_bool_false;

    network_info->read = tsc_thread_new ((void *) network_read, network_info);

    if (!network_info->read) {
        TSC_ERROR ("network_write: failed to allocate read thread [%p]",
                   network_info->tunnel_handle);
        tsc_lock_delete (network_info->data_lock);
        free ((void *) network_info);

        return 0L;
    }

    uint32_t sequence = 0;

    for (;;) {
        if (tsc_lock_get (network_info->data_lock) == tsc_lock_response_ok) {
            if (network_info->done == tsc_bool_true) {
                tsc_lock_release (network_info->data_lock);

                break;
            }

            uint8_t frame[TSC_MAX_FRAME_SIZE];
            memset (frame, 0, TSC_MAX_FRAME_SIZE * sizeof (uint8_t));

            uint32_t i;

            for (i = 0; i < network_info->frame_size; i++) {
                frame[i] = i & 0xff;
            }

            frame[0] = sequence & 0xff;
            frame[1] = (sequence & 0xff00) >> 8;

            tsc_config config;

            if (tsc_get_config (network_info->tunnel_handle, &config) ==
                                tsc_error_code_ok) {
                struct sockaddr_in addr;
                memset (&addr, 0, sizeof (struct sockaddr_in));
                addr.sin_family = AF_INET;
                addr.sin_port = htons (network_info->local_port);
                addr.sin_addr.s_addr = htonl (config.internal_address.address);

                network_info->tx_timestamp[sequence] =
                    tsc_get_clock ();

                uint32_t loop = 1;

                if (sequence == network_info->packet_count-1) {
                    loop = 10;
                }

                while (loop-- > 0) {
                    if (tsc_sendto(network_info->socket, (char *)frame,
                                   network_info->frame_size, 0,
                                   (struct sockaddr *)(&addr),
                                   sizeof (struct sockaddr_in)) > 0) {
                    }
                }

                sequence++;

                if (sequence >= network_info->packet_count) {
                    network_info->done = tsc_bool_true;
                }
            }

            tsc_lock_release (network_info->data_lock);
        }

        tsc_sleep(TSC_NETWORK_PERIOD);
    }

    TSC_DEBUG ("network_write: thread terminated [%p]", network_info->tunnel_handle);

    return 0L;
}

tsc_error_code 
tsc_network_monitor(tsc_handle tunnel_handle,
                    size_t frame_size, time_t duration, void *opaque, 
                    void (*monitor_callback)(tsc_network_data *data))
{
    if (!tunnel_handle || !duration || !frame_size) {
        TSC_ERROR ("tsc_network_monitor: invalid parameters [%p]", tunnel_handle);

        return tsc_error_code_error;
    }

    tsc_network_info *network_info = 
        (tsc_network_info *) malloc (sizeof (tsc_network_info));

    if (!network_info) {
        TSC_ERROR ("tsc_network_monitor: failed to allocate network info [%p]", tunnel_handle);

        return tsc_error_code_error;
    }

    memset (network_info, 0, sizeof (tsc_network_info));

    network_info->tunnel_handle = tunnel_handle;
    network_info->frame_size = frame_size;
    network_info->monitor_callback = monitor_callback;
    network_info->opaque = opaque;

    network_info->data_lock = tsc_lock_new ();

    if (!network_info->data_lock) {
        TSC_ERROR ("tsc_network_monitor: failed to allocate end lock [%p]", tunnel_handle);
        free ((void *) network_info);

        return tsc_error_code_error;
    }

    network_info->write = tsc_thread_new ((void *) network_write, network_info);

    if (!network_info->write) {
        TSC_ERROR ("tsc_network_monitor: failed to allocate write thread [%p]",
                   tunnel_handle);
        tsc_thread_finish (network_info->read);
        tsc_thread_delete (network_info->read);
        tsc_lock_delete (network_info->data_lock);
        free ((void *) network_info);

        return tsc_error_code_error;
    }

    network_info->packet_count = duration*1000 / TSC_NETWORK_PERIOD;

    network_info->tx_timestamp = (uint32_t *)malloc(sizeof(uint32_t)*network_info->packet_count);
    memset(network_info->tx_timestamp, 0, sizeof(uint32_t)*network_info->packet_count);
    network_info->rx_timestamp = (uint32_t *)malloc(sizeof(uint32_t)*network_info->packet_count);
    memset(network_info->rx_timestamp, 0, sizeof(uint32_t)*network_info->packet_count);

    TSC_DEBUG ("tsc_network_monitor: network monitoring started [%p]", tunnel_handle);

    return tsc_error_code_ok;
}
