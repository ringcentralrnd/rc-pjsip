echo $1

if [ "$1" = "" ];
then 
    ARG="all"
    echo Making combination library...
else
    ARG=$1
    echo Making $ARG library
fi


make clean
cp Makefile.ios Makefile.temp
rm -r ./armv6/
rm -r ./armv7/
rm -r ./i386/
if [ "$ARG" = "all" ];
then
    mkdir ./armv6/
    mkdir ./armv7/
    mkdir ./i386/
fi

if [ "$ARG" = "all" ] || [ "$ARG" = "i386" ];
then 
    sed -e 's/PATH =/PATH =\/Applications\/Xcode.app\/Contents\/Developer\/Platforms\/iPhoneSimulator.platform\/Developer\/usr/g' -i '' Makefile.ios
    sed -e 's/IFLAG =/IFLAG = -isysroot \/Applications\/Xcode.app\/Contents\/Developer\/Platforms\/iPhoneSimulator.platform\/Developer\/SDKs\/iPhoneSimulator5.1.sdk/g' -i '' Makefile.ios
    sed -e 's/GCCFLAGS =/GCCFLAGS = -arch i386/g' -i '' Makefile.ios

    make -f Makefile.ios
fi

if [ "$ARG" = "all" ];
then
    cp ./bin/libtsc.a  ./i386/libtsc
    cp ./bin/libtsc.dylib  ./i386/libtsc.dy
    make clean

fi 
cp Makefile.temp Makefile.ios 
if [ "$ARG" = "i386" ];
then
    rm Makefile.temp
fi


if [ "$ARG" = "all" ] || [ "$ARG" = "armv6" ];
then 
    sed -e 's/PATH =/PATH = \/Applications\/Xcode.app\/Contents\/Developer\/Platforms\/iPhoneOS.platform\/Developer\/usr/g' -i '' Makefile.ios
    sed -e 's/IFLAG =/IFLAG = -isysroot \/Applications\/Xcode.app\/Contents\/Developer\/Platforms\/iPhoneOS.platform\/Developer\/SDKs\/iPhoneOS5.1.sdk/g' -i '' Makefile.ios
    sed -e 's/GCCFLAGS =/GCCFLAGS = -arch armv6/g' -i '' Makefile.ios
    make -f Makefile.ios
fi

if [ "$ARG" = "all" ]
then
cp ./bin/libtsc.a  ./armv6/libtsc
cp ./bin/libtsc.dylib  ./armv6/libtsc.dy
make clean
fi

cp Makefile.temp Makefile.ios

if [ "$ARG" = "armv6" ]
then
    rm Makefile.temp
fi


if [ "$ARG" = "all" ] || [ "$ARG" = "armv7" ]
then 
    sed -e 's/PATH =/PATH = \/Applications\/Xcode.app\/Contents\/Developer\/Platforms\/iPhoneOS.platform\/Developer\/usr/g' -i '' Makefile.ios
    sed -e 's/IFLAG =/IFLAG = -isysroot \/Applications\/Xcode.app\/Contents\/Developer\/Platforms\/iPhoneOS.platform\/Developer\/SDKs\/iPhoneOS5.1.sdk/g' -i '' Makefile.ios
    sed -e 's/GCCFLAGS =/GCCFLAGS = -arch armv7/g' -i '' Makefile.ios
    make -f Makefile.ios
fi

if [ "$ARG" = "all" ]
then
cp ./bin/libtsc.a  ./armv7/libtsc
cp ./bin/libtsc.dylib  ./armv7/libtsc.dy
make clean
fi

cp Makefile.temp Makefile.ios

if [ "$ARG" = "armv7" ];
then
    rm Makefile.temp
fi

if [ "$ARG" = "all" ];
then
    rm Makefile.temp
fi

if [ "$ARG" = "all" ];
then
    rm Makefile.temp
    lipo -create ./i386/libtsc ./armv6/libtsc ./armv7/libtsc -output ./bin/libtsc.a
    lipo -create ./i386/libtsc.dy ./armv6/libtsc.dy ./armv7/libtsc.dy -output ./bin/libtsc.dylib
    rm -r ./i386/
    rm -r ./armv6/
    rm -r ./armv7/
fi



