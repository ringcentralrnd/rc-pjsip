/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#include "tsc_utils.h"
#include "tsc_thread.h"

double calculate_pesq (char *ref_filename, char *filename, int rate);

tsc_bool
tsc_ip_port_address_to_str (tsc_ip_port_address * addr, char *str,
                            size_t max_len)
{
    if (max_len >= TSC_ADDR_STR_LEN) {
        sprintf (str, "%d.%d.%d.%d:%d",
                 ((addr->address & 0xFF000000) >> 24) & 0xff,
                 ((addr->address & 0xFF0000) >> 16) & 0xff,
                 ((addr->address & 0xFF00) >> 8) & 0xff, addr->address & 0xff,
                 addr->port);

        return tsc_bool_true;
    }

    return tsc_bool_false;
}

tsc_bool
tsc_str_to_ip_port_address (char *str, tsc_ip_port_address * addr, size_t max_len)
{
    
    if(max_len > 0){

	char tmp[TSC_ADDR_STR_LEN];
	char *pch = strchr(str,':');
	
	if(pch) {

	    uint32_t index = pch - str;
	    if(index > 0)
	    {
		
		strncpy(tmp, str, index);
		tmp[index] = '\0';
		if(tsc_inet_pton(AF_INET, tmp, &(addr->address)) > 0)
		{
		    addr->address = htonl(addr->address);
		    strncpy(tmp, str + index +1 , strlen(str) - (index+1));
		    tmp[strlen(str) - (index+1)] ='\0';
		    addr->port = atoi(tmp);
		    return tsc_bool_true;
		}
	    }
	}
    }
    return tsc_bool_false;
    
}


tsc_bool
tsc_ip_address_to_str (tsc_ip_address * addr, char *str, size_t max_len)
{
    if (max_len >= TSC_ADDR_STR_LEN) {
        sprintf (str, "%d.%d.%d.%d",
                 ((addr->address & 0xFF000000) >> 24) & 0xff,
                 ((addr->address & 0xFF0000) >> 16) & 0xff,
                 ((addr->address & 0xFF00) >> 8) & 0xff,
                 addr->address & 0xff);

        return tsc_bool_true;
    }

    return tsc_bool_false;
}

tsc_bool
tsc_ip_mask_to_str (tsc_ip_mask * addr, char *str, size_t max_len)
{
    if (max_len >= TSC_ADDR_STR_LEN) {
        sprintf (str, "%d.%d.%d.%d", ((addr->mask & 0xFF000000) >> 24) & 0xff,
                 ((addr->mask & 0xFF0000) >> 16) & 0xff,
                 ((addr->mask & 0xFF00) >> 8) & 0xff, addr->mask & 0xff);

        return tsc_bool_true;
    }

    return tsc_bool_false;
}

tsc_bool
tsc_set_errno (int tmp)
{
    errno = tmp;

    return tsc_bool_true;
}

int
tsc_get_errno ()
{
    return errno;
}

char *
tsc_hexdump (void *data, int data_size, char *str, int head, int tail)
{
    strcpy (str, "");

    int k;

    int l = 0;
    int max_lines = DIV_AND_CEIL (data_size, 0x10);

    tsc_bool separator = tsc_bool_false;

    for (k = 0; k < data_size; k += 0x10, l++) {
        int c = 0x10;

        if (data_size - k < c) {
            c = data_size - k;
        }

        int i;

        char tmp[TSC_ADDR_STR_LEN];
        char aux[TSC_ADDR_STR_LEN];

        sprintf (aux, "%04X] ", k);
        strcpy (tmp, aux);

        for (i = 0; i < 0x10; i++) {
            if (i <= c) {
                sprintf (aux, "%02X ", ((uint8_t *) data)[k + i]);
            }
            else {
                sprintf (aux, "%02X ", 0);
            }

            strcat (tmp, aux);
        }

        strcat (tmp, "[");

        for (i = 0; i < 0x10; i++) {
            if (i <= c) {
                char txt = ((char *) data)[k + i];

                if (txt > 31 && txt < 127) {
                    sprintf (aux, "%c", txt);
                    strcat (tmp, aux);
                }
                else {
                    strcat (tmp, ".");
                }
            }
            else {
                strcat (tmp, ".");
            }
        }

        strcat (tmp, "]\n");

        if (!head || (l < head)) {
            strcat (str, tmp);
        }
        else if (l >= (max_lines - tail)) {
            strcat (str, tmp);
        }
        else if (separator == tsc_bool_false) {
            separator = tsc_bool_true;
            sprintf (aux, "%04X] ", k);
            strcat (str, aux);
            strcat (str,
                    "..................................................................\n");
        }
    }

    return str;
}

time_t
tsc_time ()
{
    return time (NULL);
}

uint32_t
tsc_get_clock ()
{
#ifdef TSC_IOS
    struct timeval time_val;
    gettimeofday(&time_val, NULL);
    return time_val.tv_sec * 1000 + time_val.tv_usec / 1000;
#elif TSC_LINUX
    struct timespec time_spec;
    clock_gettime (CLOCK_REALTIME, &time_spec);

    return time_spec.tv_sec * 1000 + time_spec.tv_nsec / 1000000;
#elif TSC_WINDOWS
    return timeGetTime ();
#endif

    return 0;
}

tsc_bool
tsc_sleep (uint32_t ms)
{
#ifdef TSC_LINUX
    usleep (ms * 1000);
#elif TSC_WINDOWS
    Sleep (ms);
#endif

    return tsc_bool_true;
}

int
tsc_inet_pton (int af, const char *src, void *dst)
{
#ifdef TSC_LINUX
    return inet_pton (af, src, dst);
#elif TSC_WINDOWS
    int size = sizeof (SOCKADDR_IN);

    SOCKADDR_IN addr;

    int result =
        WSAStringToAddress ((char *) src, AF_INET, NULL, (LPSOCKADDR) & addr,
                            &size);

    if (result != -1) {
        *((uint32_t *) dst) = addr.sin_addr.s_addr;
    }

    return result;
#endif
}

const char *
tsc_inet_ntop (int af, const void *src, char *dst, socklen_t cnt)
{
#ifdef TSC_LINUX
    return inet_ntop (af, src, dst, cnt);
#elif TSC_WINDOWS
    uint32_t address = ntohl (*((uint32_t *) src));

    sprintf (dst, "%d.%d.%d.%d", (address & 0xff000000) >> 24,
             (address & 0x00ff0000) >> 16, (address & 0x0000ff00) >> 8,
             (address & 0x000000ff));

    return dst;
#endif
}

static const char *basis64 =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

uint32_t
tsc_base64_encode_len (uint32_t len)
{
    return ((len + 2) / 3 * 4) + 1;
}

uint32_t
tsc_base64_encode (char *encoded, const uint8_t * buffer, uint32_t len)
{
    char *ptr = encoded;

    uint32_t i;

    for (i = 0; i < len - 2; i += 3) {
        *ptr++ = basis64[(buffer[i] >> 2) & 0x3F];
        *ptr++ =
            basis64[((buffer[i] & 0x3) << 4) |
                    ((int) (buffer[i + 1] & 0xF0) >> 4)];
        *ptr++ =
            basis64[((buffer[i + 1] & 0xF) << 2) |
                    ((int) (buffer[i + 2] & 0xC0) >> 6)];
        *ptr++ = basis64[buffer[i + 2] & 0x3F];
    }

    if (i < len) {
        *ptr++ = basis64[(buffer[i] >> 2) & 0x3F];

        if (i == (len - 1)) {
            *ptr++ = basis64[((buffer[i] & 0x3) << 4)];
            *ptr++ = '=';
        }
        else {
            *ptr++ =
                basis64[((buffer[i] & 0x3) << 4) |
                        ((int) (buffer[i + 1] & 0xF0) >> 4)];
            *ptr++ = basis64[((buffer[i + 1] & 0xF) << 2)];
        }

        *ptr++ = '=';
    }

    *ptr++ = '\0';

    return ptr - encoded;
}

typedef struct
{
    char ref_filename[TSC_MAX_STR_LEN];
    char filename[TSC_MAX_STR_LEN];
    int rate;
    void (*callback)(void *, double pesq);
    void *opaque;
} tsc_run_pesq_info;

void *
tsc_run_pesq_thread (void *arg)
{
    tsc_run_pesq_info *info = (tsc_run_pesq_info *)arg;

    double pesq = calculate_pesq(info->ref_filename, info->filename, info->rate);

    if (info->callback) {
        info->callback(info->opaque, pesq);
    }

    free(info);

    return 0L;
}

tsc_bool
tsc_run_pesq (char *ref_filename, char *filename, void (*callback)(void *, double pesq), void *opaque)
{
    tsc_run_pesq_info *info = (tsc_run_pesq_info *)malloc(sizeof(tsc_run_pesq_info));
    memset(info, 0, sizeof(tsc_run_pesq_info));

    strcpy(info->ref_filename, ref_filename);
    strcpy(info->filename, filename);
    info->rate = 8000;
    info->callback = callback;
    info->opaque = opaque;

    tsc_thread_new( (void *)tsc_run_pesq_thread, (void *)info );

    return tsc_bool_true;
}
