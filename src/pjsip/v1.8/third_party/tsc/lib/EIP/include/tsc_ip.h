/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#ifndef __TSC_IP_H__
#define __TSC_IP_H__

#include "tsc_common.h"

#define TSC_IP_HEADER_SIZE sizeof(struct iphdr)

tsc_error_code tsc_ip_make (tsc_ip_address * src_addr,
                            tsc_ip_address * dst_addr, uint8_t protocol,
                            uint8_t * ip_header, uint8_t * data, uint8_t tos,
                            uint8_t ip_version, uint32_t len);

tsc_error_code tsc_ip_parse (tsc_ip_address * src_addr,
                             tsc_ip_address * dst_addr, uint8_t * protocol,
                             uint8_t * ip_version, uint8_t * ip_header);

#ifdef TSC_LINUX
tsc_error_code tsc_ip_get_if (tsc_ip_address * src_addr, char *device);
tsc_error_code tsc_ip_get_addr (char *device, tsc_ip_address * addr);
#endif

tsc_error_code tsc_ip_get_if_count (uint8_t *count);
tsc_error_code tsc_ip_get_if_addr (uint8_t itf, char *buffer);

#endif  /*__TSC_IP_H__ */
