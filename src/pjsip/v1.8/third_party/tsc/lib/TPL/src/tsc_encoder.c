/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#define TSC_LOG_SUBSYSTEM TSC_LOG_SUBSYSTEM_ENCODER

#include "tsc_encoder.h"
#include "tsc_log.h"


/* encode control message
input: msg and outputLen (buffer size)
output: output (encoded data) and size of the encoded data, 0 if it is not possible
to encode (i.e. outputLen not big enough)
*/
size_t
tsc_encode_cm (tsc_cm * msg, void *output, size_t outputLen)
{
    uint16_t tlv_count = 0;
    unsigned char output_msg[TSC_CM_MAX_SIZE];
    uint16_t counter = 0;
    output_msg[0] = (unsigned char) (msg->header.version_id << 4);      /* version 
                                                                           of 
                                                                           header, 
                                                                           CM_Indic, 
                                                                           R,E 
                                                                         */
    output_msg[1] = (unsigned char) (msg->header.msg_type);     /* CM_type */
    if (msg->header.msg_type == tsc_cm_type_config_request) {
        if (msg->msg.config_request.valid_internal_ip_address ==
            tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_request.valid_internal_ip_mask == tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_request.valid_sip_server == tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_request.valid_supported_version_id ==
            tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_request.valid_keepalive_interval == tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_request.valid_keepalive_refresher == tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_request.valid_client_info == tsc_bool_true) {
            tlv_count++;
        }
    }
    else if (msg->header.msg_type == tsc_cm_type_config_response) {
        tlv_count++;            /* response_code */
        if (msg->msg.config_response.valid_internal_ip_address ==
            tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_response.valid_internal_ip_mask == tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_response.valid_sip_server == tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_response.valid_redirected_ip_address ==
            tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_response.valid_supported_version_id ==
            tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_response.valid_keepalive_interval ==
            tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_response.valid_keepalive_refresher ==
            tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_response.valid_peer_ip_port == tsc_bool_true) {
            tlv_count++;
        }
    }
    else if (msg->header.msg_type == tsc_cm_type_config_resume_response) {
        tlv_count++;            /* response_code */
        if (msg->msg.config_resume_response.valid_internal_ip_address ==
            tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_resume_response.valid_internal_ip_mask ==
            tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_resume_response.valid_sip_server == tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_resume_response.valid_redirected_ip_address ==
            tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_resume_response.valid_keepalive_interval ==
            tsc_bool_true) {
            tlv_count++;
        }
        if (msg->msg.config_resume_response.valid_peer_ip_port == tsc_bool_true) {
            tlv_count++;
        }
    }
    else if (msg->header.msg_type == tsc_cm_type_config_release_response) {
        tlv_count++;            /* response_code */
    }
    else if (msg->header.msg_type == tsc_cm_type_client_service_request) {
        tlv_count++;            /* service_type */

        if (msg->msg.client_service_request.service_type == TSC_TLV_SERVICE_TYPE_ENABLE_HEADER_COMPRESSION ||
            msg->msg.client_service_request.service_type == TSC_TLV_SERVICE_TYPE_DISABLE_HEADER_COMPRESSION) {
            tlv_count++; /* connection info */
        } else if (msg->msg.client_service_request.service_type == TSC_TLV_SERVICE_TYPE_ENABLE_REDUNDANCY ||
            msg->msg.client_service_request.service_type == TSC_TLV_SERVICE_TYPE_DISABLE_REDUNDANCY) {
            tlv_count++; /* redundancy_method */
            tlv_count++; /* connection info */
            tlv_count++; /* redundancy_factor */
        }
	else if (msg->msg.client_service_request.service_type == TSC_TLV_SERVICE_TYPE_ENABLE_ODD ||
            msg->msg.client_service_request.service_type == TSC_TLV_SERVICE_TYPE_DISABLE_ODD) {
            tlv_count++; /* connection info*/
        }

    }
    else if (msg->header.msg_type == tsc_cm_type_client_service_response) {
        tlv_count++;            /* response_code */

        tlv_count += msg->msg.client_service_response.service_response
                             .redundancy.max_tunnels;
    }

    output_msg[2] = (unsigned char) (tlv_count >> 8 & 0xFF);    /* tlv_count */
    output_msg[3] = (unsigned char) (tlv_count & 0xFF); /* tlv_count */

    /* Tunnel Session ID */
    output_msg[4] = (unsigned char) (msg->header.tunnel_id.hi >> 24 & 0xFF);
    output_msg[5] = (unsigned char) (msg->header.tunnel_id.hi >> 16 & 0xFF);
    output_msg[6] = (unsigned char) (msg->header.tunnel_id.hi >> 8 & 0xFF);
    output_msg[7] = (unsigned char) (msg->header.tunnel_id.hi & 0xFF);

    output_msg[8] = (unsigned char) (msg->header.tunnel_id.lo >> 24 & 0xFF);
    output_msg[9] = (unsigned char) (msg->header.tunnel_id.lo >> 16 & 0xFF);
    output_msg[10] = (unsigned char) (msg->header.tunnel_id.lo >> 8 & 0xFF);
    output_msg[11] = (unsigned char) (msg->header.tunnel_id.lo & 0xFF);

    /* Sequence number */
    output_msg[12] = (unsigned char) (msg->header.sequence >> 24 & 0xFF);
    output_msg[13] = (unsigned char) (msg->header.sequence >> 16 & 0xFF);
    output_msg[14] = (unsigned char) (msg->header.sequence >> 8 & 0xFF);
    output_msg[15] = (unsigned char) (msg->header.sequence & 0xFF);

    counter = 16;
    if (tlv_count > 0) {
        if (msg->header.msg_type == tsc_cm_type_config_request) {
            if (msg->msg.config_request.valid_internal_ip_address ==
                tsc_bool_true) {
                /* tlv type: Internal_IPv4_Address */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_INTERNAL_IPV4_ADDRESS);
                counter++;
                /* tlv length: 4 bytes */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_INTERNAL_IPV4_ADDRESS);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.
                                       internal_ip_address.
                                       address >> 24 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.
                                       internal_ip_address.
                                       address >> 16 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.
                                       internal_ip_address.
                                       address >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.
                                       internal_ip_address.address & 0xFF);
                counter++;
            }
            if (msg->msg.config_request.valid_internal_ip_mask ==
                tsc_bool_true) {
                /* tlv type: Internal_IPv4_Netmask */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_INTERNAL_IPV4_NETMASK);
                counter++;
                /* tlv length: 4 bytes */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_INTERNAL_IPV4_NETMASK);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.
                                       internal_ip_mask.mask >> 24 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.
                                       internal_ip_mask.mask >> 16 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.
                                       internal_ip_mask.mask >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.
                                       internal_ip_mask.mask & 0xFF);
                counter++;
            }
            if (msg->msg.config_request.valid_sip_server == tsc_bool_true) {
                /* tlv type: Server_SIP_IPv4_Address_Port */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_SERVER_SIP_IPV4_ADDRESS_PORT);
                counter++;
                /* tlv length: 6 bytes */
                output_msg[counter] =
                    (unsigned
                     char) (TSC_TLV_LENGTH_OF_SERVER_SIP_IPV4_ADDRESS_PORT);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.sip_server.
                                       address >> 24 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.sip_server.
                                       address >> 16 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.sip_server.
                                       address >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.sip_server.
                                       address & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.sip_server.
                                       port >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.sip_server.
                                       port & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.sip_server.
                                       protocol & 0xFF);
                counter++;
            }
            if (msg->msg.config_request.valid_supported_version_id ==
                tsc_bool_true) {
                /* tlv type: Supported_Version_ID */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_SUPPORTED_VERSION_ID);
                counter++;
                /* tlv length: 1 byte */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_SUPPORTED_VERSION_ID);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.
                                       supported_version_id & 0xFF);
                counter++;
            }
            if (msg->msg.config_request.valid_keepalive_interval ==
                tsc_bool_true) {
                /* tlv type: keepalive_interval */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_KEEP_ALIVE_INTERNAL);
                counter++;
                /* tlv length: 2 bytes */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_KEEPALIVE_INTERVAL);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.
                                       keepalive_interval >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.
                                       keepalive_interval & 0xFF);
                counter++;
            }
            if (msg->msg.config_request.valid_keepalive_refresher ==
                tsc_bool_true) {
                /* tlv type: keepalive_refresher */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_KEEP_ALIVE_REFRESHER);
                counter++;
                /* tlv length: 1 byte */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_KEEPALIVE_REFRESHER);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_request.
                                       keepalive_refresher & 0xFF);
                counter++;
            }
            if (msg->msg.config_request.valid_client_info ==
                tsc_bool_true) {
                /* tlv type: client info */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_CLIENT_INFO);
                counter++;
                /* tlv length: 2 bytes */
                output_msg[counter] =
                    (unsigned char) (msg->msg.config_request.client_info_len);
                counter++;

                memcpy(output_msg + counter, msg->msg.config_request.client_info,
                       msg->msg.config_request.client_info_len);

                counter += msg->msg.config_request.client_info_len;
            }
        }
        else if (msg->header.msg_type == tsc_cm_type_config_response) {
            /* tlv type: Response_Code */
            output_msg[counter] = (unsigned char) (TSC_TLV_RESPONSE_CODE);
            counter++;
            /* tlv length: 2 bytes */
            output_msg[counter] =
                (unsigned char) (TSC_TLV_LENGTH_OF_RESPONSE_CODE);
            counter++;
            /* tlv value */
            output_msg[counter]
                = (unsigned char) (msg->msg.config_response.
                                   response_code >> 8 & 0xFF);
            counter++;
            output_msg[counter]
                = (unsigned char) (msg->msg.config_response.
                                   response_code & 0xFF);
            counter++;
            if (msg->msg.config_response.valid_internal_ip_address ==
                tsc_bool_true) {
                /* tlv type: Internal_IPv4_Address */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_INTERNAL_IPV4_ADDRESS);
                counter++;
                /* tlv length: 4 bytes */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_INTERNAL_IPV4_ADDRESS);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       internal_ip_address.
                                       address >> 24 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       internal_ip_address.
                                       address >> 16 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       internal_ip_address.
                                       address >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       internal_ip_address.address & 0xFF);
                counter++;
            }
            if (msg->msg.config_response.valid_internal_ip_mask ==
                tsc_bool_true) {
                /* tlv type: Internal_IPv4_Netmask */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_INTERNAL_IPV4_NETMASK);
                counter++;
                /* tlv length: 4 bytes */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_INTERNAL_IPV4_NETMASK);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       internal_ip_mask.mask >> 24 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       internal_ip_mask.mask >> 16 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       internal_ip_mask.mask >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       internal_ip_mask.mask & 0xFF);
                counter++;
            }
            if (msg->msg.config_response.valid_sip_server == tsc_bool_true) {
                /* tlv type: Server_SIP_IPv4_Address_Port */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_SERVER_SIP_IPV4_ADDRESS_PORT);
                counter++;
                /* tlv length: 6 bytes */
                output_msg[counter] =
                    (unsigned
                     char) (TSC_TLV_LENGTH_OF_SERVER_SIP_IPV4_ADDRESS_PORT);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.sip_server.
                                       address >> 24 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.sip_server.
                                       address >> 16 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.sip_server.
                                       address >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.sip_server.
                                       address & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.sip_server.
                                       port >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.sip_server.
                                       port & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.sip_server.
                                       protocol & 0xFF);
                counter++;
            }
            if (msg->msg.config_response.valid_redirected_ip_address ==
                tsc_bool_true) {
                /* tlv type: Redirected_IPv4_Address */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_REDIRECTED_IPV4_ADDRESS);
                counter++;
                /* tlv length: 4 bytes */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_REDIRECT_IPV4_ADDRESS);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       redirected_ip_address.
                                       address >> 24 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       redirected_ip_address.
                                       address >> 16 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       redirected_ip_address.
                                       address >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       redirected_ip_address.address & 0xFF);
                counter++;
            }
            if (msg->msg.config_response.valid_supported_version_id ==
                tsc_bool_true) {
                /* tlv type: Supported_Version_ID */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_SUPPORTED_VERSION_ID);
                counter++;
                /* tlv length: 1 byte */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_SUPPORTED_VERSION_ID);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       supported_version_id & 0xFF);
                counter++;
            }
            if (msg->msg.config_response.valid_keepalive_interval ==
                tsc_bool_true) {
                /* tlv type: keepalive_interval */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_KEEP_ALIVE_INTERNAL);
                counter++;
                /* tlv length: 2 bytes */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_KEEPALIVE_INTERVAL);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       keepalive_interval >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       keepalive_interval & 0xFF);
                counter++;
            }
            if (msg->msg.config_response.valid_keepalive_refresher ==
                tsc_bool_true) {
                /* tlv type: keepalive_refresher */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_KEEP_ALIVE_REFRESHER);
                counter++;
                /* tlv length: 1 byte */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_KEEPALIVE_REFRESHER);
                counter++;
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_response.
                                       keepalive_refresher & 0xFF);
                counter++;
            }
            if (msg->msg.config_response.valid_peer_ip_port
		== tsc_bool_true) {
                /* tlv type: peer_ipv4_address_port */
                output_msg[counter] =
		    (unsigned char) (TSC_TLV_PEER_IPV4_ADDRESS_PORT);
                counter++;
                /* tlv length: 6 bytes */
                output_msg[counter] =
		    (unsigned char) (TSC_TLV_LENGTH_OF_PEER_IPV4_ADDRESS_PORT);
                counter++;
                /* tlv value */
                output_msg[counter]
                        = (unsigned char) (msg->msg.config_response.peer_ip_port.address
                                >> 24 & 0xFF);
                counter++;
                output_msg[counter]
                        = (unsigned char) (msg->msg.config_response.peer_ip_port.address
                                >> 16 & 0xFF);
                counter++;
                output_msg[counter]
                        = (unsigned char) (msg->msg.config_response.peer_ip_port.address
                                >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                        = (unsigned char) (msg->msg.config_response.peer_ip_port.address
                                & 0xFF);
                counter++;
                output_msg[counter]
                        = (unsigned char) (msg->msg.config_response.peer_ip_port.port
                                >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                        = (unsigned char) (msg->msg.config_response.peer_ip_port.port
                                & 0xFF);
                counter++;
            }
        }
        else if (msg->header.msg_type == tsc_cm_type_config_resume_response) {
            /* tlv type: Response_Code */
            output_msg[counter] = (unsigned char) (TSC_TLV_RESPONSE_CODE);
            counter++;
            /* tlv length: 2 bytes */
            output_msg[counter] =
                (unsigned char) (TSC_TLV_LENGTH_OF_RESPONSE_CODE);
            counter++;
            /* tlv value */
            output_msg[counter]
                = (unsigned char) (msg->msg.config_resume_response.
                                   response_code >> 8 & 0xFF);
            counter++;
            output_msg[counter]
                = (unsigned char) (msg->msg.config_resume_response.
                                   response_code & 0xFF);
            counter++;
            if (msg->msg.config_resume_response.valid_internal_ip_address ==
                tsc_bool_true) {
                /* tlv type: Internal_IPv4_Address */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_INTERNAL_IPV4_ADDRESS);
                counter++;
                /* tlv length: 4 bytes */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_INTERNAL_IPV4_ADDRESS);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       internal_ip_address.
                                       address >> 24 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       internal_ip_address.
                                       address >> 16 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       internal_ip_address.
                                       address >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       internal_ip_address.address & 0xFF);
                counter++;
            }
            if (msg->msg.config_resume_response.valid_internal_ip_mask ==
                tsc_bool_true) {
                /* tlv type: Internal_IPv4_Netmask */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_INTERNAL_IPV4_NETMASK);
                counter++;
                /* tlv length: 4 bytes */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_INTERNAL_IPV4_NETMASK);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       internal_ip_mask.mask >> 24 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       internal_ip_mask.mask >> 16 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       internal_ip_mask.mask >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       internal_ip_mask.mask & 0xFF);
                counter++;
            }
            if (msg->msg.config_resume_response.valid_sip_server ==
                tsc_bool_true) {
                /* tlv type: Server_SIP_IPv4_Address_Port */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_SERVER_SIP_IPV4_ADDRESS_PORT);
                counter++;
                /* tlv length: 6 bytes */
                output_msg[counter] =
                    (unsigned
                     char) (TSC_TLV_LENGTH_OF_SERVER_SIP_IPV4_ADDRESS_PORT);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       sip_server.address >> 24 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       sip_server.address >> 16 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       sip_server.address >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       sip_server.address & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       sip_server.port >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       sip_server.port & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       sip_server.protocol & 0xFF);
                counter++;
            }
            if (msg->msg.config_resume_response.valid_redirected_ip_address ==
                tsc_bool_true) {
                /* tlv type: Redirected_IPv4_Address */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_REDIRECTED_IPV4_ADDRESS);
                counter++;
                /* tlv length: 4 bytes */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_REDIRECT_IPV4_ADDRESS);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       redirected_ip_address.
                                       address >> 24 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       redirected_ip_address.
                                       address >> 16 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       redirected_ip_address.
                                       address >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       redirected_ip_address.address & 0xFF);
                counter++;
            }
            if (msg->msg.config_resume_response.valid_keepalive_interval ==
                tsc_bool_true) {
                /* tlv type: keepalive_interval */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_KEEP_ALIVE_INTERNAL);
                counter++;
                /* tlv length: 2 bytes */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_KEEPALIVE_INTERVAL);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       keepalive_interval >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.config_resume_response.
                                       keepalive_interval & 0xFF);
                counter++;
            }
            if (msg->msg.config_resume_response.valid_peer_ip_port
		== tsc_bool_true) {
                /* tlv type: peer_ipv4_address_port */
                output_msg[counter] =
		    (unsigned char) (TSC_TLV_PEER_IPV4_ADDRESS_PORT);
                counter++;
                /* tlv length: 6 bytes */
                output_msg[counter] =
		    (unsigned char) (TSC_TLV_LENGTH_OF_PEER_IPV4_ADDRESS_PORT);
                counter++;
                /* tlv value */
                output_msg[counter]
                        = (unsigned char) (msg->msg.config_resume_response.peer_ip_port.address
                                >> 24 & 0xFF);
                counter++;
                output_msg[counter]
                        = (unsigned char) (msg->msg.config_resume_response.peer_ip_port.address
                                >> 16 & 0xFF);
                counter++;
                output_msg[counter]
                        = (unsigned char) (msg->msg.config_resume_response.peer_ip_port.address
                                >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                        = (unsigned char) (msg->msg.config_resume_response.peer_ip_port.address
                                & 0xFF);
                counter++;
                output_msg[counter]
                        = (unsigned char) (msg->msg.config_resume_response.peer_ip_port.port
                                >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                        = (unsigned char) (msg->msg.config_resume_response.peer_ip_port.port
                                & 0xFF);
                counter++;
            }
        }
        else if (msg->header.msg_type == tsc_cm_type_config_release_response) {
            /* tlv type: Response_Code */
            output_msg[counter] = (unsigned char) (TSC_TLV_RESPONSE_CODE);
            counter++;
            /* tlv length: 2 bytes */
            output_msg[counter] =
                (unsigned char) (TSC_TLV_LENGTH_OF_RESPONSE_CODE);
            counter++;
            /* tlv value */
            output_msg[counter]
                = (unsigned char) (msg->msg.config_release_response.
                                   response_code >> 8 & 0xFF);
            counter++;
            output_msg[counter]
                = (unsigned char) (msg->msg.config_release_response.
                                   response_code & 0xFF);
            counter++;
        }
        else if (msg->header.msg_type == tsc_cm_type_client_service_request) {
            /* tlv type: Response_Code */
            output_msg[counter] = (unsigned char) (TSC_TLV_SERVICE_TYPE);
            counter++;
            /* tlv length */
            output_msg[counter] =
                (unsigned char) (TSC_TLV_LENGTH_OF_SERVICE_TYPE);
            counter++;
            /* tlv value */
            output_msg[counter]
                = (unsigned char) (msg->msg.client_service_request.
                                   service_type & 0xFF);
            counter++;

            if (msg->msg.client_service_request.service_type == TSC_TLV_SERVICE_TYPE_ENABLE_REDUNDANCY ||
                msg->msg.client_service_request.service_type == TSC_TLV_SERVICE_TYPE_DISABLE_REDUNDANCY) {
                /* tlv type: Response_Code */
                output_msg[counter] = (unsigned char) (TSC_TLV_REDUNDANCY_FACTOR);
                counter++;
                /* tlv length */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_REDUNDANCY_FACTOR);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.redundancy_factor & 0xFF);
                counter++;

                output_msg[counter] = (unsigned char) (TSC_TLV_REDUNDANCY_METHOD);
                counter++;
                /* tlv length */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_REDUNDANCY_METHOD);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.redundancy_method & 0xFF);
                counter++;

                if (msg->msg.client_service_request.service_request.
                    redundancy.connection_info_use ==
                    tsc_tlv_connection_info_use_ipv4) {
                    /* tlv type: Connection Info IPv4 */
                    output_msg[counter] = (unsigned char) (TSC_TLV_CONNECTION_INFO_IPV4);
                    counter++;
                    /* tlv length */
                    output_msg[counter] =
                        (unsigned char) (TSC_TLV_LENGTH_OF_CONNECTION_INFO_IPV4);
                    counter++;
                    /* tlv value */
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.connection_info.
                                           connection_info_ipv4.tos & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.connection_info.
                                           connection_info_ipv4.protocol & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.connection_info.
                                           connection_info_ipv4.ttl & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.connection_info.
                                           connection_info_ipv4.src_address.
                                           address >> 24 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.connection_info.
                                           connection_info_ipv4.src_address.
                                           address >> 16 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.connection_info.
                                           connection_info_ipv4.src_address.
                                           address >> 8 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.connection_info.
                                           connection_info_ipv4.src_address.
                                           address & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.connection_info.
                                           connection_info_ipv4.dst_address.
                                           address >> 24 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.connection_info.
                                           connection_info_ipv4.dst_address.
                                           address >> 16 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.connection_info.
                                           connection_info_ipv4.dst_address.
                                           address >> 8 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.connection_info.
                                           connection_info_ipv4.dst_address.
                                           address & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.connection_info.
                                           connection_info_ipv4.src_address.
                                           port >> 8 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.connection_info.
                                           connection_info_ipv4.src_address.
                                           port & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.connection_info.
                                           connection_info_ipv4.dst_address.
                                           port >> 8 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           redundancy.connection_info.
                                           connection_info_ipv4.dst_address.
                                           port & 0xFF);
                    counter++;
                }
            }
	    
            else if (msg->msg.client_service_request.service_type == TSC_TLV_SERVICE_TYPE_ENABLE_ODD ||
		     msg->msg.client_service_request.service_type == TSC_TLV_SERVICE_TYPE_DISABLE_ODD) {
	    
		if (msg->msg.client_service_request.service_request.
                    odd.connection_info_use ==
                    tsc_tlv_connection_info_use_ipv4) {
                    /* tlv type: Connection Info IPv4 */
                    output_msg[counter] = (unsigned char) (TSC_TLV_CONNECTION_INFO_IPV4);
                    counter++;
                    /* tlv length */
                    output_msg[counter] =
                        (unsigned char) (TSC_TLV_LENGTH_OF_CONNECTION_INFO_IPV4);
                    counter++;
                    /* tlv value */
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           odd.connection_info.
                                           connection_info_ipv4.tos & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
					   odd.connection_info.
                                           connection_info_ipv4.protocol & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           odd.connection_info.
                                           connection_info_ipv4.ttl & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
					   odd.connection_info.
                                           connection_info_ipv4.src_address.
                                           address >> 24 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
					   odd.connection_info.
                                           connection_info_ipv4.src_address.
                                           address >> 16 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
					   odd.connection_info.
                                           connection_info_ipv4.src_address.
                                           address >> 8 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
					   odd.connection_info.
                                           connection_info_ipv4.src_address.
                                           address & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
					   odd.connection_info.
                                           connection_info_ipv4.dst_address.
                                           address >> 24 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
					   odd.connection_info.
                                           connection_info_ipv4.dst_address.
                                           address >> 16 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
                                           odd.connection_info.
                                           connection_info_ipv4.dst_address.
                                           address >> 8 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
					   odd.connection_info.
                                           connection_info_ipv4.dst_address.
                                           address & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
					   odd.connection_info.
                                           connection_info_ipv4.src_address.
                                           port >> 8 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
					   odd.connection_info.
                                           connection_info_ipv4.src_address.
                                           port & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
					   odd.connection_info.
                                           connection_info_ipv4.dst_address.
                                           port >> 8 & 0xFF);
                    counter++;
                    output_msg[counter]
                        = (unsigned char) (msg->msg.client_service_request.service_request.
					   odd.connection_info.
                                           connection_info_ipv4.dst_address.
                                           port & 0xFF);
                    counter++;
                }
            }
        }
        else if (msg->header.msg_type == tsc_cm_type_client_service_response) {
            /* tlv type: Response_Code */
            output_msg[counter] = (unsigned char) (TSC_TLV_RESPONSE_CODE);
            counter++;
            /* tlv length: 2 bytes */
            output_msg[counter] =
                (unsigned char) (TSC_TLV_LENGTH_OF_RESPONSE_CODE);
            counter++;
            /* tlv value */
            output_msg[counter]
                = (unsigned char) (msg->msg.client_service_response.
                                   response_code >> 8 & 0xFF);
            counter++;
            output_msg[counter]
                = (unsigned char) (msg->msg.client_service_response.
                                   response_code & 0xFF);
            counter++;

            uint8_t i;

            for (i = 0; i < msg->msg.client_service_response.service_response
                                    .redundancy.max_tunnels; i++) {
                /* tlv type: Response_Code */
                output_msg[counter] = (unsigned char) (TSC_TLV_TUNNEL_ID);
                counter++;
                /* tlv length: 2 bytes */
                output_msg[counter] =
                    (unsigned char) (TSC_TLV_LENGTH_OF_TUNNEL_ID);
                counter++;
                /* tlv value */
                output_msg[counter]
                    = (unsigned char) (msg->msg.client_service_response
                                       .service_response.redundancy.tunnel_id[i].hi
                                       >> 24 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.client_service_response
                                       .service_response.redundancy.tunnel_id[i].hi
                                       >> 16 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.client_service_response
                                       .service_response.redundancy.tunnel_id[i].hi
                                       >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.client_service_response
                                       .service_response.redundancy.tunnel_id[i].hi
                                       & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.client_service_response
                                       .service_response.redundancy.tunnel_id[i].lo
                                       >> 24 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.client_service_response
                                       .service_response.redundancy.tunnel_id[i].lo
                                       >> 16 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.client_service_response
                                       .service_response.redundancy.tunnel_id[i].lo
                                       >> 8 & 0xFF);
                counter++;
                output_msg[counter]
                    = (unsigned char) (msg->msg.client_service_response
                                       .service_response.redundancy.tunnel_id[i].lo
                                       & 0xFF);
                counter++;
            }

        }
    }
    if (counter >= outputLen) {
        return 0;
    }
    else {
        memcpy (output, output_msg, counter);
    }
    return counter;
}

/* decode control message
	input: input and input_len buffer
	output: output_msg structure and response code = TRUE/FALSE to indicate
	whether decoding was successfull or not
*/
tsc_bool
tsc_decode_cm (void *input, size_t input_len, tsc_cm * output_msg)
{
    tsc_bool ret_val = tsc_bool_false;
    unsigned char *input_msg;
    uint16_t tlv_count;
    uint32_t temp_var_hi;
    uint32_t temp_var_lo;
    uint32_t temp_var2;
    uint16_t counter;
    memset (output_msg, 0, sizeof (tsc_cm));
    if (input_len < 16 || input_len > TSC_CM_MAX_SIZE)
        return ret_val;
    input_msg = (unsigned char *) input;
    output_msg->header.version_id = (uint32_t) ((0xFF & input_msg[0]) >> 4);
    output_msg->header.msg_type = (tsc_cm_type) (0xFF & input_msg[1]);
    tlv_count = 0xFFFF;
    tlv_count = (uint16_t) ((0xFF & input_msg[2]) << 8);
    tlv_count = (tlv_count | (0xFF & input_msg[3]));

    temp_var_hi = 0xFFFFFFFF;
    temp_var_lo = 0xFFFFFFFF;

    temp_var_hi = ((uint32_t) (0xFF & input_msg[4]) << 24);
    temp_var_hi = (temp_var_hi | ((uint32_t) (0xFF & input_msg[5]) << 16));
    temp_var_hi = (temp_var_hi | ((uint32_t) (0xFF & input_msg[6]) << 8));
    temp_var_hi = (temp_var_hi | ((uint32_t) (0xFF & input_msg[7])));

    temp_var_lo = ((uint32_t) (0xFF & input_msg[8]) << 24);
    temp_var_lo = (temp_var_lo | ((uint32_t) (0xFF & input_msg[9]) << 16));
    temp_var_lo = (temp_var_lo | ((uint32_t) (0xFF & input_msg[10]) << 8));
    temp_var_lo = (temp_var_lo | (uint32_t) (0xFF & input_msg[11]));

    output_msg->header.tunnel_id.hi = temp_var_hi;
    output_msg->header.tunnel_id.lo = temp_var_lo;

    temp_var2 = 0xFFFFFFFF;
    temp_var2 = ((0xFF & input_msg[12]) << 24);
    temp_var2 = (temp_var2 | ((0xFF & input_msg[13]) << 16));
    temp_var2 = (temp_var2 | ((0xFF & input_msg[14]) << 8));
    temp_var2 = (temp_var2 | (0xFF & input_msg[15]));
    output_msg->header.sequence = temp_var2;
    counter = 16;
    if (tlv_count > 0) {
        if (output_msg->header.msg_type == tsc_cm_type_config_request) {
            int i, j;
            unsigned char length = 0x00;
            for (i = tlv_count; i > 0; i--) {
                switch ((0xFF & input_msg[counter])) {
                case TSC_TLV_INTERNAL_IPV4_ADDRESS:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_INTERNAL_IPV4_ADDRESS)
                        return ret_val;
                    temp_var2 =
                        ((0xFF & input_msg[counter]) << 8 * (length - 1));
                    counter++;
                    for (j = length - 2; j >= 0; j--) {
                        temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                                  << j * 8));
                        counter++;
                    }
                    output_msg->msg.config_request.internal_ip_address.
                        address = temp_var2;

                    output_msg->msg.config_request.valid_internal_ip_address =
                        tsc_bool_true;

                    break;
                case TSC_TLV_INTERNAL_IPV4_NETMASK:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_INTERNAL_IPV4_NETMASK)
                        return ret_val;
                    temp_var2 =
                        ((0xFF & input_msg[counter]) << 8 * (length - 1));
                    counter++;
                    for (j = length - 2; j >= 0; j--) {
                        temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                                  << j * 8));
                        counter++;
                    }
                    output_msg->msg.config_request.internal_ip_mask.mask =
                        temp_var2;

                    output_msg->msg.config_request.valid_internal_ip_mask =
                        tsc_bool_true;

                    break;
                case TSC_TLV_SERVER_SIP_IPV4_ADDRESS_PORT:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length !=
                        TSC_TLV_LENGTH_OF_SERVER_SIP_IPV4_ADDRESS_PORT)
                        return ret_val;
                    temp_var2 = ((0xFF & input_msg[counter]) << 24);
                    counter++;
                    temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                              << 16));
                    counter++;
                    temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                              << 8));
                    counter++;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.config_request.sip_server.address =
                        temp_var2;
                    temp_var2 =
                        ((0x0000 << 16) | ((0xFF & input_msg[counter]) << 8));
                    counter++;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.config_request.sip_server.port =
                        temp_var2;
                    temp_var2 = (0xFF & input_msg[counter]);
                    counter++;
                    output_msg->msg.config_request.sip_server.protocol =
			(tsc_transport) temp_var2;

                    output_msg->msg.config_request.valid_sip_server =
                        tsc_bool_true;

                    break;
                case TSC_TLV_SUPPORTED_VERSION_ID:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_SUPPORTED_VERSION_ID)
                        return ret_val;
                    temp_var2 = (0xFF & input_msg[counter]);
                    counter++;
                    output_msg->msg.config_request.supported_version_id =
                        temp_var2;

                    output_msg->msg.config_request.
                        valid_supported_version_id = tsc_bool_true;

                    break;
                case TSC_TLV_KEEP_ALIVE_INTERNAL:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_KEEPALIVE_INTERVAL)
                        return ret_val;
                    temp_var2 = ((0xFF & input_msg[counter]) << 8);
                    counter++;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.config_request.keepalive_interval =
                        temp_var2;

                    output_msg->msg.config_request.valid_keepalive_interval =
                        tsc_bool_true;

                    break;
                case TSC_TLV_KEEP_ALIVE_REFRESHER:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_KEEPALIVE_REFRESHER)
                        return ret_val;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.config_request.keepalive_refresher = (tsc_keepalive_refresher)
                        temp_var2;

                    output_msg->msg.config_request.valid_keepalive_refresher =
                        tsc_bool_true;

                    break;
                case TSC_TLV_CLIENT_INFO:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;

                    memset(output_msg->msg.config_request.client_info, 0,
                           TSC_TLV_LENGTH_OF_CLIENT_INFO);

                    memcpy(output_msg->msg.config_request.client_info, 
                           input_msg + counter, length);

                    output_msg->msg.config_request.client_info_len = length;

                    output_msg->msg.config_request.valid_client_info =
                        tsc_bool_true;

                    break;
                default:
                    return ret_val;
                }
            }
        }
        else if (output_msg->header.msg_type == tsc_cm_type_config_response) {
            int i, j;
            unsigned char length = 0x00;
            for (i = tlv_count; i > 0; i--) {
                switch ((0xFF & input_msg[counter])) {
                case TSC_TLV_RESPONSE_CODE:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_RESPONSE_CODE)
                        return ret_val;
                    temp_var2 = ((0xFF & input_msg[counter]) << 8);
                    counter++;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.config_response.response_code =
                        (tsc_response_code) temp_var2;
                    break;
                case TSC_TLV_INTERNAL_IPV4_ADDRESS:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_INTERNAL_IPV4_ADDRESS) {
                        return ret_val;
                    }
                    temp_var2 =
                        ((0xFF & input_msg[counter]) << 8 * (length - 1));
                    counter++;
                    for (j = length - 2; j >= 0; j--) {
                        temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                                  << j * 8));
                        counter++;
                    }
                    output_msg->msg.config_response.internal_ip_address.
                        address = temp_var2;

                    output_msg->msg.config_response.
                        valid_internal_ip_address = tsc_bool_true;

                    break;
                case TSC_TLV_INTERNAL_IPV4_NETMASK:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_INTERNAL_IPV4_NETMASK)
                        return ret_val;
                    temp_var2 =
                        ((0xFF & input_msg[counter]) << 8 * (length - 1));
                    counter++;
                    for (j = length - 2; j >= 0; j--) {
                        temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                                  << j * 8));
                        counter++;
                    }
                    output_msg->msg.config_response.internal_ip_mask.mask =
                        temp_var2;

                    output_msg->msg.config_response.valid_internal_ip_mask =
                        tsc_bool_true;

                    break;
                case TSC_TLV_SERVER_SIP_IPV4_ADDRESS_PORT:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length !=
                        TSC_TLV_LENGTH_OF_SERVER_SIP_IPV4_ADDRESS_PORT)
                        return ret_val;
                    temp_var2 = ((0xFF & input_msg[counter]) << 24);
                    counter++;
                    temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                              << 16));
                    counter++;
                    temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                              << 8));
                    counter++;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.config_response.sip_server.address =
                        temp_var2;
                    temp_var2 = ((0xFF & input_msg[counter]) << 8);
                    counter++;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.config_response.sip_server.port =
                        temp_var2;
                    temp_var2 = (0xFF & input_msg[counter]);
                    counter++;
                    output_msg->msg.config_response.sip_server.protocol =
                        (tsc_transport) temp_var2;

                    output_msg->msg.config_response.valid_sip_server =
                        tsc_bool_true;

                    break;
                case TSC_TLV_REDIRECTED_IPV4_ADDRESS:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_REDIRECT_IPV4_ADDRESS)
                        return ret_val;
                    temp_var2 =
                        ((0xFF & input_msg[counter]) << 8 * (length - 1));
                    counter++;
                    for (j = length - 2; j >= 0; j--) {
                        temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                                  << j * 8));
                        counter++;
                    }
                    output_msg->msg.config_response.redirected_ip_address.
                        address = temp_var2;

                    output_msg->msg.config_response.
                        valid_redirected_ip_address = tsc_bool_true;

                    break;
		case TSC_TLV_PEER_IPV4_ADDRESS_PORT:
		    counter++;
		    length = (0xFF & input_msg[counter]);
		    counter++;
		    if (length != TSC_TLV_LENGTH_OF_PEER_IPV4_ADDRESS_PORT)
			return ret_val;
		    temp_var2 = ((0xFF & input_msg[counter]) << 24);
		    counter++;
		    temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                << 16));
		    counter++;
		    temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
					    << 8));
		    counter++;
		    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
		    counter++;
		    output_msg->msg.config_response.peer_ip_port.address
			= temp_var2;
		    temp_var2 = ((0xFF & input_msg[counter]) << 8);
		    counter++;
		    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
		    counter++;
		    output_msg->msg.config_response.peer_ip_port.port
			= temp_var2;

		    output_msg->msg.config_response.valid_peer_ip_port = tsc_bool_true;

		    break;
                case TSC_TLV_SUPPORTED_VERSION_ID:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_SUPPORTED_VERSION_ID)
                        return ret_val;
                    temp_var2 = (0xFF & input_msg[counter]);
                    counter++;
                    output_msg->msg.config_response.supported_version_id =
                        temp_var2;

                    output_msg->msg.config_response.
                        valid_supported_version_id = tsc_bool_true;

                    break;
                case TSC_TLV_KEEP_ALIVE_INTERNAL:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_KEEPALIVE_INTERVAL)
                        return ret_val;
                    temp_var2 = ((0xFF & input_msg[counter]) << 8);
                    counter++;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.config_response.keepalive_interval =
                        temp_var2;

                    output_msg->msg.config_response.valid_keepalive_interval =
                        tsc_bool_true;

                    break;
                case TSC_TLV_KEEP_ALIVE_REFRESHER:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_KEEPALIVE_REFRESHER)
                        return ret_val;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.config_response.keepalive_refresher = (tsc_keepalive_refresher)
                        temp_var2;

                    output_msg->msg.config_response.valid_keepalive_refresher =
                        tsc_bool_true;

                    break;
                default:
                    return ret_val;
                }
            }
        }
        else if (output_msg->header.msg_type ==
                 tsc_cm_type_config_resume_response) {
            int i, j;
            unsigned char length = 0x00;
            for (i = tlv_count; i > 0; i--) {
                switch ((0xFF & input_msg[counter])) {
                case TSC_TLV_RESPONSE_CODE:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_RESPONSE_CODE)
                        return ret_val;
                    temp_var2 = ((0xFF & input_msg[counter]) << 8);
                    counter++;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.config_resume_response.response_code =
                        (tsc_response_code) temp_var2;
                    break;
                case TSC_TLV_INTERNAL_IPV4_ADDRESS:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_INTERNAL_IPV4_ADDRESS) {
                        return ret_val;
                    }
                    temp_var2 =
                        ((0xFF & input_msg[counter]) << 8 * (length - 1));
                    counter++;
                    for (j = length - 2; j >= 0; j--) {
                        temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                                  << j * 8));
                        counter++;
                    }
                    output_msg->msg.config_resume_response.
                        internal_ip_address.address = temp_var2;

                    output_msg->msg.config_resume_response.
                        valid_internal_ip_address = tsc_bool_true;

                    break;
                case TSC_TLV_INTERNAL_IPV4_NETMASK:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_INTERNAL_IPV4_NETMASK)
                        return ret_val;
                    temp_var2 =
                        ((0xFF & input_msg[counter]) << 8 * (length - 1));
                    counter++;
                    for (j = length - 2; j >= 0; j--) {
                        temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                                  << j * 8));
                        counter++;
                    }
                    output_msg->msg.config_resume_response.internal_ip_mask.
                        mask = temp_var2;

                    output_msg->msg.config_resume_response.
                        valid_internal_ip_mask = tsc_bool_true;

                    break;
                case TSC_TLV_SERVER_SIP_IPV4_ADDRESS_PORT:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length !=
                        TSC_TLV_LENGTH_OF_SERVER_SIP_IPV4_ADDRESS_PORT)
                        return ret_val;
                    temp_var2 = ((0xFF & input_msg[counter]) << 24);
                    counter++;
                    temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                              << 16));
                    counter++;
                    temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                              << 8));
                    counter++;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.config_resume_response.sip_server.
                        address = temp_var2;
                    temp_var2 = ((0xFF & input_msg[counter]) << 8);
                    counter++;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.config_resume_response.sip_server.port =
                        temp_var2;
                    temp_var2 = (0xFF & input_msg[counter]);
                    counter++;
                    output_msg->msg.config_resume_response.sip_server.protocol =
			(tsc_transport) temp_var2;

                    output_msg->msg.config_resume_response.valid_sip_server =
                        tsc_bool_true;

                    break;
                case TSC_TLV_REDIRECTED_IPV4_ADDRESS:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_REDIRECT_IPV4_ADDRESS)
                        return ret_val;
                    temp_var2 =
                        ((0xFF & input_msg[counter]) << 8 * (length - 1));
                    counter++;
                    for (j = length - 2; j >= 0; j--) {
                        temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                                  << j * 8));
                        counter++;
                    }
                    output_msg->msg.config_resume_response.
                        redirected_ip_address.address = temp_var2;

                    output_msg->msg.config_resume_response.
                        valid_redirected_ip_address = tsc_bool_true;

                    break;
		case TSC_TLV_PEER_IPV4_ADDRESS_PORT:
		    counter++;
		    length = (0xFF & input_msg[counter]);
		    counter++;
		    if (length != TSC_TLV_LENGTH_OF_PEER_IPV4_ADDRESS_PORT)
			return ret_val;
		    temp_var2 = ((0xFF & input_msg[counter]) << 24);
		    counter++;
		    temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
					    << 16));
		    counter++;
		    temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
					    << 8));
		    counter++;
		    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
		    counter++;
		    output_msg->msg.config_resume_response.peer_ip_port.address
			= temp_var2;
		    temp_var2 = ((0xFF & input_msg[counter]) << 8);
		    counter++;
		    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
		    counter++;
		    output_msg->msg.config_resume_response.peer_ip_port.port
			= temp_var2;

		    output_msg->msg.config_resume_response.valid_peer_ip_port = tsc_bool_true;

		    break;
                case TSC_TLV_KEEP_ALIVE_INTERNAL:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_KEEPALIVE_INTERVAL)
                        return ret_val;
                    temp_var2 = ((0xFF & input_msg[counter]) << 8);
                    counter++;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.config_resume_response.
                        keepalive_interval = temp_var2;

                    output_msg->msg.config_resume_response.
                        valid_keepalive_interval = tsc_bool_true;

                    break;
                default:
                    return ret_val;
                }
            }
        }
        else if (output_msg->header.msg_type ==
                 tsc_cm_type_config_release_response) {
            int i;
            unsigned char length = 0x00;
            for (i = tlv_count; i > 0; i--) {
                switch ((0xFF & input_msg[counter])) {
                case TSC_TLV_RESPONSE_CODE:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_RESPONSE_CODE)
                        return ret_val;
                    temp_var2 = ((0xFF & input_msg[counter]) << 8);
                    counter++;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.config_resume_response.response_code =
                        (tsc_response_code) temp_var2;
                    break;
                default:
                    return ret_val;
                }
            }
        }
        else if (output_msg->header.msg_type == tsc_cm_type_client_service_response) {
            int i, j;
            unsigned char length = 0x00;
            uint8_t max_tunnels = 0;
	    uint8_t type = (0xFF & input_msg[counter]);
           /* The service type is not included in the TLV for older versions of the TSCF server
	    * This code checks to see if a service type is present, otherwise the TLV is assumed to 
	    * be a redundancy response, older versions of the encoder did
	    */
	    if (type ==  TSC_TLV_SERVICE_TYPE) {
	        counter++;
		length = (0xFF & input_msg[counter]);
		counter++;
		if (length != TSC_TLV_LENGTH_OF_SERVICE_TYPE)
		    return ret_val;
		temp_var2 =
		    (0xFF & input_msg[counter]);
		counter++;
		output_msg->msg.client_service_response.service_type = temp_var2;
		tlv_count--;
	    }
	    if (output_msg->msg.client_service_response.service_type == 0) {
		output_msg->msg.client_service_response.service_type
		    = TSC_TLV_SERVICE_TYPE_ENABLE_REDUNDANCY;	
	    }
	    for (i = tlv_count; i > 0; i--) {
                switch ((0xFF & input_msg[counter])) {
		  case TSC_TLV_SERVICE_TYPE:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_SERVICE_TYPE)
                        return ret_val;
                    temp_var2 =
                        (0xFF & input_msg[counter]);
                    counter++;
                    output_msg->msg.client_service_response.service_type = temp_var2;
                    break; 
                case TSC_TLV_RESPONSE_CODE:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_RESPONSE_CODE)
                        return ret_val;
                    temp_var2 = ((0xFF & input_msg[counter]) << 8);
                    counter++;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.client_service_response.response_code =
                        (tsc_response_code) temp_var2;
                    break;
                case TSC_TLV_TUNNEL_ID:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_TUNNEL_ID) {
                        return ret_val;
                    }
                    temp_var2 =
                        ((0xFF & input_msg[counter]) << 8 * (4 - 1));
                    counter++;
                    for (j = 2; j >= 0; j--) {
                        temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                                  << j * 8));
                        counter++;
                    }
		    if((output_msg->msg.client_service_response.service_type == 
			TSC_TLV_SERVICE_TYPE_ENABLE_REDUNDANCY) ||
		       (output_msg->msg.client_service_response.service_type == 
		       TSC_TLV_SERVICE_TYPE_DISABLE_REDUNDANCY)) {
		
			output_msg->msg.client_service_response.service_response
			    .redundancy.tunnel_id[max_tunnels].hi = temp_var2;
			
			temp_var2 =
			    ((0xFF & input_msg[counter]) << 8 * (4 - 1));
			counter++;
			for (j = 2; j >= 0; j--) {
			    temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
						      << j * 8));
			    counter++;
			}
			output_msg->msg.client_service_response.service_response
			    .redundancy.tunnel_id[max_tunnels].lo = temp_var2;
			
			max_tunnels++;
			
			output_msg->msg.client_service_response.service_response
			    .redundancy.max_tunnels = max_tunnels;
		    }
		    else if((output_msg->msg.client_service_response.service_type == 
			      TSC_TLV_SERVICE_TYPE_ENABLE_ODD) ||
			     (output_msg->msg.client_service_response.service_type == 
			      TSC_TLV_SERVICE_TYPE_DISABLE_ODD)) {
			
			output_msg->msg.client_service_response.service_response
			    .odd.tunnel_id.hi = temp_var2;
			
			temp_var2 =
			    ((0xFF & input_msg[counter]) << 8 * (4 - 1));
			counter++;
			for (j = 2; j >= 0; j--) {
			    temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
						      << j * 8));
			    counter++;
			}
			output_msg->msg.client_service_response.service_response
			    .odd.tunnel_id.lo = temp_var2;	
		    }
			    
		    
                    break;
                default:
                    return ret_val;
                }
            }
        }
        else if (output_msg->header.msg_type == tsc_cm_type_client_service_request) {
            int i, j;
            unsigned char length = 0x00;
            for (i = tlv_count; i > 0; i--) {
                switch ((0xFF & input_msg[counter])) {
                case TSC_TLV_SERVICE_TYPE:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_SERVICE_TYPE)
                        return ret_val;
                    temp_var2 =
                        (0xFF & input_msg[counter]);
                    counter++;
                    output_msg->msg.client_service_request.service_type = temp_var2;

                    break;
                case TSC_TLV_REDUNDANCY_FACTOR:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_REDUNDANCY_FACTOR)
                        return ret_val;
                    temp_var2 =
                        (0xFF & input_msg[counter]);
                    counter++;
                    output_msg->msg.client_service_request.service_request.
                                    redundancy.redundancy_factor = temp_var2;

                    break;
                case TSC_TLV_REDUNDANCY_METHOD:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_REDUNDANCY_METHOD)
                        return ret_val;
                    temp_var2 =
                        (0xFF & input_msg[counter]);
                    counter++;
                    output_msg->msg.client_service_request.service_request.
                                    redundancy.redundancy_method = (tsc_tlv_redundancy_method)temp_var2;

                    break;
                case TSC_TLV_CONNECTION_INFO_IPV4:
                    counter++;
                    length = (0xFF & input_msg[counter]);
                    counter++;
                    if (length != TSC_TLV_LENGTH_OF_CONNECTION_INFO_IPV4)
                        return ret_val;
                    temp_var2 =
                        (0xFF & input_msg[counter]);
                    counter++;
                    output_msg->msg.client_service_request.service_request.
                                    redundancy.connection_info.
                                    connection_info_ipv4.tos = temp_var2;

                    temp_var2 =
                        (0xFF & input_msg[counter]);
                    counter++;
                    output_msg->msg.client_service_request.service_request.
                                    redundancy.connection_info.
                                    connection_info_ipv4.protocol = temp_var2;
                    temp_var2 =
                        (0xFF & input_msg[counter]);
                    counter++;
                    output_msg->msg.client_service_request.service_request.
                                    redundancy.connection_info.
                                    connection_info_ipv4.ttl = temp_var2;

                    temp_var2 =
                        ((0xFF & input_msg[counter]) << 8 * (length - 1));
                    counter++;
                    for (j = length - 2; j >= 0; j--) {
                        temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                                  << j * 8));
                        counter++;
                    }
                    output_msg->msg.client_service_request.service_request.
                                    redundancy.connection_info.
                                    connection_info_ipv4.src_address.
                                    address = temp_var2;

                    temp_var2 =
                        ((0xFF & input_msg[counter]) << 8 * (length - 1));
                    counter++;
                    for (j = length - 2; j >= 0; j--) {
                        temp_var2 = (temp_var2 | ((0xFF & input_msg[counter])
                                                  << j * 8));
                        counter++;
                    }
                    output_msg->msg.client_service_request.service_request.
                                    redundancy.connection_info.
                                    connection_info_ipv4.dst_address.
                                    address = temp_var2;

                    temp_var2 = ((0xFF & input_msg[counter]) << 8);
                    counter++;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.client_service_request.service_request.
                                    redundancy.connection_info.
                                    connection_info_ipv4.src_address.
                                    port = temp_var2;

                    temp_var2 = ((0xFF & input_msg[counter]) << 8);
                    counter++;
                    temp_var2 = (temp_var2 | (0xFF & input_msg[counter]));
                    counter++;
                    output_msg->msg.client_service_request.service_request.
                                    redundancy.connection_info.
                                    connection_info_ipv4.dst_address.
                                    port = temp_var2;

                    break;

                default:
                    return ret_val;
                }
            }
        }
    }
    if (counter > input_len) {
        return ret_val;
    }
    ret_val = tsc_bool_true;
    return ret_val;
}

tsc_bool
tsc_encode_decode_diags ()
{
    tsc_bool ret_val = tsc_bool_false;

    /* test case 1 for config_request */
    tsc_cm tcm;

    uint32_t internal_ip;
    uint32_t internal_ip_mask;
    uint32_t sip_ip;
    uint32_t sip_port = 5060;
    uint32_t keepalive_interval = 10;
    uint32_t support_version_id = 1;
    char input_internal_ip[] = "192.168.1.1";
    char input_internal_ip_mask[] = "255.255.255.1";
    char input_sip_ip[] = "192.168.1.2";

    unsigned char output[TSC_CM_MAX_SIZE];

    tsc_cm tcm_output;
    tsc_bool decode_result;
    tsc_cm tcm_output2;
    tsc_bool decode_result2;
    tsc_cm tcm_output3;

    char decode_internal_ip[INET_ADDRSTRLEN];
    char decode_sip_ip[INET_ADDRSTRLEN];
    char decode_ip_mask[INET_ADDRSTRLEN];

    tsc_cm tcm2;
    tsc_cm tcm3;

    /* for test 2 */
    uint32_t internal_ip2;
    uint32_t sip_ip2;
    uint32_t sip_port2 = 5062;
    uint32_t internal_ip_mask2;
    uint32_t redirectIp2;
    uint32_t keepalive_interval2 = 20;
    uint32_t support_version_id2 = 2;
    uint8_t response_code2 = 2;

    char input_internal_ip2[] = "192.168.2.1";
    char input_internal_ip_mask2[] = "255.255.255.2";
    char input_sip_ip2[] = "192.168.2.2";
    char input_redirect_ip2[] = "192.168.2.3";

    unsigned char output2[TSC_CM_MAX_SIZE];

    char decode_internal_ip2[INET_ADDRSTRLEN];
    char decode_sip_ip2[INET_ADDRSTRLEN];
    char decode_ip_mask2[INET_ADDRSTRLEN];
    char decode_redirect_ip2[INET_ADDRSTRLEN];

    /* for test 3 */
    uint32_t internal_ip3;
    uint32_t sip_ip3;
    uint32_t sip_port3 = 5063;
    uint32_t internal_ip_mask3;
    uint32_t redirectIp3;
    uint32_t keepalive_interval3 = 30;
    uint8_t response_code3 = 3;

    char input_internal_ip3[] = "192.168.3.1";
    char input_internal_ip_mask3[] = "255.255.255.3";
    char input_sip_ip3[] = "192.168.3.2";
    char input_redirect_ip3[] = "192.168.3.3";

    unsigned char output3[TSC_CM_MAX_SIZE];

    char decode_internal_ip3[INET_ADDRSTRLEN];
    char decode_sip_ip3[INET_ADDRSTRLEN];
    char decode_ip_mask3[INET_ADDRSTRLEN];
    char decode_redirect_ip3[INET_ADDRSTRLEN];

    tcm.header.version_id = 0x01;
    tcm.header.msg_type = tsc_cm_type_config_request;
    tcm.header.tunnel_id.lo = 0x01;
    tcm.header.tunnel_id.hi = 0x00;
    tcm.header.sequence = 0x01;

    tcm.msg.config_request.valid_internal_ip_address = tsc_bool_true;
    tcm.msg.config_request.valid_sip_server = tsc_bool_true;
    tcm.msg.config_request.valid_internal_ip_mask = tsc_bool_true;
    tcm.msg.config_request.valid_keepalive_interval = tsc_bool_true;
    tcm.msg.config_request.valid_supported_version_id = tsc_bool_true;


    if (1 != tsc_inet_pton (AF_INET, input_internal_ip, &internal_ip)) {
        TSC_ERROR ("Error parsing ip: %s", input_internal_ip);
        return ret_val;
    }
    if (1 !=
        tsc_inet_pton (AF_INET, input_internal_ip_mask, &internal_ip_mask)) {
        TSC_ERROR ("Error parsing ip: %s", input_internal_ip_mask);
        return ret_val;
    }
    if (1 != tsc_inet_pton (AF_INET, input_sip_ip, &sip_ip)) {
        TSC_ERROR ("Error parsing ip: %s", input_sip_ip);
        return ret_val;
    }
    tcm.msg.config_request.internal_ip_address.address = internal_ip;
    tcm.msg.config_request.internal_ip_mask.mask = internal_ip_mask;
    tcm.msg.config_request.sip_server.address = sip_ip;
    tcm.msg.config_request.sip_server.port = sip_port;
    tcm.msg.config_request.keepalive_interval = keepalive_interval;
    tcm.msg.config_request.supported_version_id = support_version_id;

    tsc_encode_cm (&tcm, output, TSC_CM_MAX_SIZE);

    decode_result = tsc_decode_cm (output, TSC_CM_MAX_SIZE, &tcm_output);
    if (!decode_result) {
        TSC_ERROR ("Error decoding");
        return ret_val;
    }

    tsc_inet_ntop (AF_INET,
                   (char *) &tcm_output.msg.config_request.
                   internal_ip_address.address, decode_internal_ip,
                   INET_ADDRSTRLEN);
    tsc_inet_ntop (AF_INET,
                   (char *) &tcm_output.msg.config_request.internal_ip_mask.
                   mask, decode_ip_mask, INET_ADDRSTRLEN);
    tsc_inet_ntop (AF_INET,
                   (char *) &tcm_output.msg.config_request.sip_server.address,
                   decode_sip_ip, INET_ADDRSTRLEN);

    if (strcmp (decode_internal_ip, input_internal_ip)) {
        TSC_ERROR ("Error decoding input_internal_ip");
        return ret_val;
    }
    if (strcmp (decode_sip_ip, input_sip_ip)) {
        TSC_ERROR ("Error decoding input_sip_ip");
        return ret_val;
    }
    if (strcmp (decode_ip_mask, input_internal_ip_mask)) {
        TSC_ERROR ("Error decoding input_internal_ip_mask");
        return ret_val;
    }
    if (tcm_output.msg.config_request.sip_server.port != sip_port) {
        TSC_ERROR ("Error decoding sip_port");
        return ret_val;
    }
    if (tcm_output.msg.config_request.keepalive_interval !=
        keepalive_interval) {
        TSC_ERROR ("Error decoding keepalive_interval");
        return ret_val;
    }
    if (tcm_output.msg.config_request.supported_version_id !=
        support_version_id) {
        TSC_ERROR ("Error decoding supported_version_id");
        return ret_val;
    }

    TSC_NOTICE ("test case 1 passed!");

    /* test case 2 for tsc_cm_type_config_response */

    tcm2.header.version_id = 0x01;
    tcm2.header.msg_type = tsc_cm_type_config_response;
    tcm2.header.tunnel_id.lo = 0x01;
    tcm2.header.tunnel_id.hi = 0x00;
    tcm2.header.sequence = 0x01;

    tcm2.msg.config_response.valid_internal_ip_address = tsc_bool_true;
    tcm2.msg.config_response.valid_internal_ip_mask = tsc_bool_true;
    tcm2.msg.config_response.valid_keepalive_interval = tsc_bool_true;
    tcm2.msg.config_response.valid_redirected_ip_address = tsc_bool_true;
    tcm2.msg.config_response.valid_sip_server = tsc_bool_true;
    tcm2.msg.config_response.valid_supported_version_id = tsc_bool_true;


    if (1 != tsc_inet_pton (AF_INET, input_internal_ip2, &internal_ip2)) {
        TSC_ERROR ("Error parsing ip: %s\n", input_internal_ip2);
        return ret_val;
    }
    if (1 != tsc_inet_pton (AF_INET, input_sip_ip2, &sip_ip2)) {
        TSC_ERROR ("Error parsing ip: %s\n", input_sip_ip2);
        return ret_val;
    }
    if (1 !=
        tsc_inet_pton (AF_INET, input_internal_ip_mask2,
                       &internal_ip_mask2)) {
        TSC_ERROR ("Error parsing ip: %s\n", input_internal_ip_mask2);
        return ret_val;
    }
    if (1 != tsc_inet_pton (AF_INET, input_redirect_ip2, &redirectIp2)) {
        TSC_ERROR ("Error parsing ip: %s\n", input_redirect_ip2);
        return ret_val;
    }

    tcm2.msg.config_response.internal_ip_address.address = internal_ip2;
    tcm2.msg.config_response.internal_ip_mask.mask = internal_ip_mask2;
    tcm2.msg.config_response.keepalive_interval = keepalive_interval2;
    tcm2.msg.config_response.redirected_ip_address.address = redirectIp2;
    tcm2.msg.config_response.response_code =
        (tsc_response_code) response_code2;
    tcm2.msg.config_response.sip_server.address = sip_ip2;
    tcm2.msg.config_response.sip_server.port = sip_port2;
    tcm2.msg.config_response.supported_version_id = support_version_id2;


    tsc_encode_cm (&tcm2, output2, TSC_CM_MAX_SIZE);
    decode_result2 = tsc_decode_cm (output2, TSC_CM_MAX_SIZE, &tcm_output2);
    if (!decode_result2) {
        return ret_val;
    }

    tsc_inet_ntop (AF_INET,
                   &tcm_output2.msg.config_response.internal_ip_address.
                   address, decode_internal_ip2, INET_ADDRSTRLEN);
    tsc_inet_ntop (AF_INET,
                   &tcm_output2.msg.config_response.internal_ip_mask.mask,
                   decode_ip_mask2, INET_ADDRSTRLEN);
    tsc_inet_ntop (AF_INET,
                   &tcm_output2.msg.config_response.sip_server.address,
                   decode_sip_ip2, INET_ADDRSTRLEN);
    tsc_inet_ntop (AF_INET,
                   &tcm_output2.msg.config_response.redirected_ip_address.
                   address, decode_redirect_ip2, INET_ADDRSTRLEN);
    if (strcmp (decode_internal_ip2, input_internal_ip2)) {
        TSC_ERROR ("Error decoding input_internal_ip2");
        return ret_val;
    }
    if (strcmp (decode_sip_ip2, input_sip_ip2)) {
        TSC_ERROR ("Error decoding input_sip_ip2");
        return ret_val;
    }
    if (strcmp (decode_ip_mask2, input_internal_ip_mask2)) {
        TSC_ERROR ("Error decoding input_internal_ip_mask2");
        return ret_val;
    }
    if (strcmp (decode_redirect_ip2, input_redirect_ip2)) {
        TSC_ERROR ("Error decoding input_redirect_ip2");
        return ret_val;
    }
    if (tcm_output2.msg.config_response.sip_server.port != sip_port2) {
        TSC_ERROR ("Error decoding sip_port2");
        return ret_val;
    }
    if (tcm_output2.msg.config_response.keepalive_interval !=
        keepalive_interval2) {
        TSC_ERROR ("Error decoding keepalive_interval");
        return ret_val;
    }
    if (tcm_output2.msg.config_response.supported_version_id !=
        support_version_id2) {
        TSC_ERROR ("Error decoding supported_version_id");
        return ret_val;
    }
    TSC_NOTICE ("test case 2 passed!");

    /* test case 3 for tsc_cm_type_config_resume_response */

    tcm3.header.version_id = 0x01;
    tcm3.header.msg_type = tsc_cm_type_config_resume_response;
    tcm3.header.tunnel_id.lo = 0x01;
    tcm3.header.tunnel_id.hi = 0x00;
    tcm3.header.sequence = 0x01;

    tcm3.msg.config_resume_response.valid_internal_ip_address = tsc_bool_true;
    tcm3.msg.config_resume_response.valid_internal_ip_mask = tsc_bool_true;
    tcm3.msg.config_resume_response.valid_keepalive_interval = tsc_bool_true;
    tcm3.msg.config_resume_response.valid_redirected_ip_address =
        tsc_bool_true;
    tcm3.msg.config_resume_response.valid_sip_server = tsc_bool_true;


    if (1 != tsc_inet_pton (AF_INET, input_internal_ip3, &internal_ip3)) {
        TSC_ERROR ("Error parsing ip: %s", input_internal_ip3);
        return ret_val;
    }
    if (1 != tsc_inet_pton (AF_INET, input_sip_ip3, &sip_ip3)) {
        TSC_ERROR ("Error parsing ip: %s", input_sip_ip3);
        return ret_val;
    }
    if (1 !=
        tsc_inet_pton (AF_INET, input_internal_ip_mask3,
                       &internal_ip_mask3)) {
        TSC_ERROR ("Error parsing ip: %s", input_internal_ip_mask3);
        return ret_val;
    }
    if (1 != tsc_inet_pton (AF_INET, input_redirect_ip3, &redirectIp3)) {
        TSC_ERROR ("Error parsing ip: %s", input_redirect_ip3);
        return ret_val;
    }
    tcm3.msg.config_resume_response.internal_ip_address.address =
        internal_ip3;
    tcm3.msg.config_resume_response.internal_ip_mask.mask = internal_ip_mask3;
    tcm3.msg.config_resume_response.keepalive_interval = keepalive_interval3;
    tcm3.msg.config_resume_response.redirected_ip_address.address =
        redirectIp3;
    tcm3.msg.config_resume_response.response_code =
        (tsc_response_code) response_code3;
    tcm3.msg.config_resume_response.sip_server.address = sip_ip3;
    tcm3.msg.config_resume_response.sip_server.port = sip_port3;


    tsc_encode_cm (&tcm3, output3, TSC_CM_MAX_SIZE);

    tsc_decode_cm (output3, TSC_CM_MAX_SIZE, &tcm_output3);

    tsc_inet_ntop (AF_INET,
                   &tcm_output3.msg.config_resume_response.
                   internal_ip_address.address, decode_internal_ip3,
                   INET_ADDRSTRLEN);
    tsc_inet_ntop (AF_INET,
                   &tcm_output3.msg.config_resume_response.internal_ip_mask.
                   mask, decode_ip_mask3, INET_ADDRSTRLEN);
    tsc_inet_ntop (AF_INET,
                   &tcm_output3.msg.config_resume_response.sip_server.address,
                   decode_sip_ip3, INET_ADDRSTRLEN);
    tsc_inet_ntop (AF_INET,
                   &tcm_output3.msg.config_resume_response.
                   redirected_ip_address.address, decode_redirect_ip3,
                   INET_ADDRSTRLEN);

    if (strcmp (decode_internal_ip3, input_internal_ip3)) {
        TSC_ERROR ("Error decoding input_internal_ip3");
        return ret_val;
    }
    if (strcmp (decode_sip_ip3, input_sip_ip3)) {
        TSC_ERROR ("Error decoding input_sip_ip3");
        return ret_val;
    }
    if (strcmp (decode_ip_mask3, input_internal_ip_mask3)) {
        TSC_ERROR ("Error decoding input_internal_ip_mask");
        return ret_val;
    }
    if (strcmp (decode_redirect_ip3, input_redirect_ip3)) {
        TSC_ERROR ("Error decoding input_redirect_ip3");
        return ret_val;
    }
    if (tcm_output3.msg.config_resume_response.sip_server.port != sip_port3) {
        TSC_ERROR ("Error decoding sip_port3");
        return ret_val;
    }
    if (tcm_output3.msg.config_resume_response.keepalive_interval !=
        keepalive_interval3) {
        TSC_ERROR ("Error decoding keepalive_interval");
        return ret_val;
    }
    TSC_NOTICE ("test case 3 passed!");
    ret_val = tsc_bool_true;
    return ret_val;
}
