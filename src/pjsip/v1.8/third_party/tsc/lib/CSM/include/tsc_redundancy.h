#ifndef __TSC_REDUNDANCY_H__
#define __TSC_REDUNDANCY_H__

#define TSC_REDUNDANCY_MAX_STREAMS 2
#define TSC_REDUNDANCY_MAX_FRAMES 10

#define TSC_REDUNDANCY_MAX_SEQ 0x1000

#define TSC_REDUNDANCY_SEQ_DISTANCE(s1,s2) TSC_MIN((s1 > s2) ? (s1 - s2) : (s2 - s1), \
                                           TSC_REDUNDANCY_MAX_SEQ - 1 - ((s1 > s2) ? (s1 - s2) : (s2 - s1)))

#define TSC_REDUNDANCY_HEADER_LENGTH 3

typedef enum
{
    tsc_redundancy_state_not_set,
    tsc_redundancy_state_set,
} tsc_redundancy_frame_state;

typedef struct
{
    uint32_t sequence_number;
    tsc_redundancy_frame_state state;
    tsc_buffer msg;
} tsc_redundancy_frame;

#define TSC_SENDTO_REDUNDANCY_FLAG 0x8000

typedef struct
{
    uint8_t redundancy_factor;
    tsc_buffer redundancy_buffer[TSC_REDUNDANCY_MAX_STREAMS];
    tsc_redundancy_frame redundancy_frame[TSC_REDUNDANCY_MAX_FRAMES];
    uint32_t rx_sequence_number;
    tsc_bool rx_sequence_number_set;
    uint32_t tx_sequence_number;
    tsc_bool load_balance;
    uint8_t load_balance_index;
    uint8_t load_balance_size;
} tsc_redundancy_info;

size_t tsc_redundancy_header_make(uint32_t sequence_number, size_t len, uint8_t *buf_ptr);
size_t tsc_redundancy_header_parse(uint8_t *buf_ptr, uint32_t *sequence_number, size_t *len);

#endif /* __TSC_REDUNDANCY_H__ */
