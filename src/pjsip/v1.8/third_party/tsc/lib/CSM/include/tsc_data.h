/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#ifndef __TSC_DATA_H__
#define __TSC_DATA_H__

#include "tsc_socket_api.h"
#include "tsc_lock.h"
#include "tsc_queue.h"
#include "tsc_tunnel.h"
#include "tsc_lock.h"
#include "tsc_thread.h"
#include "tsc_queue.h"
#include "tsc_tunnel_socket.h"
#include "tsc_transaction.h"

#include "tsc_udp.h"
#include "tsc_statistics.h"

#ifdef TSC_UIP
#include "uip.h"
#include "clock.h"
#include "timer.h"

#define TSC_UIP_FRAME_SIZE 1400
#endif

#ifdef TSC_REDUNDANCY
#include "tsc_redundancy.h"
#endif

#define TSC_CSM_QUEUE_SIZE  100

#define TSC_SOCKET_ATTRIB_NON_BLOCKING 1

#define TSC_TIMEVAL_DELTA 100

#define TSC_SOCKET_MAX_SOCKETS 0x10000  /* 65536 sockets per application */
#define TSC_SOCKET_MAX_PORT 0x10000     /* Port cannot be greater then 65535 */
#define TSC_SOCKET_FIRST_SOCKET 80000
#define TSC_SLEEP_COUNT 100
#define TSC_ANDROID_SLEEP_COUNT 1

#define TSC_NEW_TUNNEL_CONNECT_TIMEOUT 10

typedef enum
{
    tsc_csm_msg_type_finish,
    tsc_csm_msg_type_service_request,
    tsc_csm_msg_type_data
} tsc_csm_msg_type;

typedef struct
{
    tsc_csm_msg_type msg_type;
    union {
        tsc_buffer buffer;
        tsc_cm_client_service_request service_request;
    } info;
    uint32_t timestamp;
    void *opaque;
} tsc_csm_msg;

#ifdef TSC_UIP
#define TSC_MAX_PORTS_PER_TUNNEL 0x10000

#define TSC_TIMEOUT_GUARD_TIME 2

typedef enum
{
    tsc_uip_state_idle = 0,

    tsc_uip_state_connect,
    tsc_uip_state_connecting,
    tsc_uip_state_connected,

    tsc_uip_state_close,
    tsc_uip_state_closing,
    tsc_uip_state_closed,

    tsc_uip_state_listen,
    tsc_uip_state_listening,

    tsc_uip_state_failure
} tsc_uip_state;

#define TSC_UIP_MAX_BACKLOG 40
#endif

#ifdef TSC_UIP
#define TSC_MAX_TCP_BUFFER_SIZE 0x10000

typedef struct
{
    uint8_t data[TSC_MAX_TCP_BUFFER_SIZE];
    uint32_t ptr;
} tsc_tcp_buffer;

tsc_bool tsc_write_tcp_buffer (tsc_tcp_buffer * buffer, uint8_t * data,
                               uint32_t len);
uint32_t tsc_read_tcp_buffer (tsc_tcp_buffer * buffer, uint8_t * data,
                              uint32_t len);
#endif

typedef struct
{
    tsc_bool enabled;
    tsc_notification_data notification_data;
    void (*notification)(tsc_notification_data *);
    void *internal_data;
} tsc_notification_info;

typedef struct tsc_socket_info_s
{
    tsc_handle handle;
    tsc_queue *out_queue;
    tsc_lock *out_queue_lock;
    tsc_queue *in_queue;
    tsc_lock *in_queue_lock;
    tsc_ip_port_address src_address;
    struct tsc_socket_info_s *next;
    uint32_t socket_attrib;
    int fd;
    tsc_output_option socket_options;
    tsc_notification_info *socket_received_info;
#ifdef TSC_REDUNDANCY
    tsc_redundancy_info redundancy_info;
    tsc_handle redundancy_tunnel_list[TSC_TLV_REDUNDANCY_MAX_TUNNEL_ID];
#endif
#ifdef TSC_ODD
    tsc_handle odd_tunnel;
#endif
#ifdef TSC_UIP
    tsc_ip_port_address dst_address;
    int type;
    tsc_uip_state state;
    tsc_tcp_buffer send_buffer;
    tsc_tcp_buffer retx_buffer;
    tsc_tcp_buffer recv_buffer;
    struct uip_conn *connection;
    time_t timeout_timer;
    uint32_t backlog;
    uint32_t pending_count;
    struct tsc_socket_info_s *pending_list[TSC_UIP_MAX_BACKLOG];
    struct tsc_socket_info_s *parent_socket;
#endif
} tsc_socket_info;

typedef struct
{
    tsc_thread *csm;
    tsc_queue *in_queue;
    tsc_queue *cm_queue;
    tsc_lock *in_queue_lock;
    tsc_queue *out_queue;
    tsc_lock *out_queue_lock;
    tsc_tunnel_params tunnel_params;
    tsc_requested_config requested_config;
    tsc_state_info state_info;
    tsc_tunnel_socket *tunnel_socket;
    time_t connect_time;
    time_t negotiation_time;
    time_t negotiation_retry_time;
    time_t keepalive_time;
    time_t poll_time;
    time_t proxy_time;
    time_t stats_time;
    time_t fatal_error_sleep_time;
    tsc_config config;
    tsc_lock *data_lock;
    tsc_buffer input_frame;
    tsc_socket_info *socket_info;
    tsc_statistics statistics;
    http_proxy_buffer proxy_buffer;
    uint32_t proxy_tries;
    uint32_t reconnect_tries;
    uint32_t connection_index;
#ifdef TSC_PCAP_CAPTURE
    FILE *pcap_fd;
#endif
#ifdef TSC_REDUNDANCY
    tsc_socket_info *redundancy_socket;
    tsc_notification_info *redundancy_notification_info;
#endif
#ifdef TSC_ODD
    tsc_socket_info *odd_socket;
    tsc_notification_info *odd_notification_info;
#endif
    tsc_transaction *transaction_list;
    tsc_notification_info *tunnel_socket_notification_info;
    tsc_notification_info *tunnel_termination_notification_info;
    time_t network_config_check;
    uint8_t network_config_count;
    char network_config_str[TSC_MAX_STR_LEN];
} tsc_csm_info;

#ifdef TSC_UIP
tsc_bool tsc_check_tcp (tsc_csm_info * tunnel_info,
                        tsc_socket_info * socket_info);
tsc_bool tsc_uip_data (tsc_csm_info * tunnel_info);
tsc_bool tsc_handle_incoming_tcp (tsc_csm_info * tunnel_info,
                                  tsc_csm_msg * msg);

tsc_bool tsc_set_socket_state (tsc_socket_info * socket_info,
                               tsc_uip_state state);
#endif

#ifdef TSC_REDUNDANCY
tsc_bool tsc_csm_set_redundancy (tsc_csm_info * info, tsc_socket_info *socket,
                                 tsc_ip_port_address * address,
                                 tsc_so_redundancy *redundancy, tsc_bool set);

size_t tsc_redundancy_make(tsc_socket_info *info, char *buf_ptr, size_t len, char *out_buf,
                           tsc_bool datagram, uint8_t index);

tsc_bool tsc_redundancy_parse(tsc_csm_info * info, tsc_csm_msg * msg,
                              tsc_socket_info *found, tsc_queue *out_queue,
                              tsc_bool datagram);
#endif

#ifdef TSC_ODD
tsc_bool tsc_csm_set_odd(tsc_csm_info * info, tsc_socket_info * socket, 
			 tsc_ip_port_address * address,
                         tsc_bool set);
#endif

tsc_error_code tsc_socket_init ();
tsc_socket_info *tsc_new_socket ();
tsc_bool tsc_delete_socket (int fd);
tsc_bool tsc_valid_socket (int fd);
tsc_socket_info *tsc_get_socket_info (int fd);
tsc_bool tsc_clear_sockets (tsc_handle handle);

tsc_bool tsc_close_aux (tsc_socket_info * info, tsc_csm_info * tunnel_info);

tsc_bool tsc_tunnel_insert_socket (tsc_handle handle, tsc_socket_info * info);
tsc_bool tsc_tunnel_delete_socket (tsc_handle handle, tsc_socket_info * info);
tsc_bool tsc_tunnel_find_socket (tsc_handle handle, tsc_socket_info * info);
tsc_socket_info *tsc_tunnel_find_socket_addr (tsc_handle handle,
                                              tsc_ip_port_address * addr,
#ifdef TSC_UIP
					      tsc_bool source_address, int type);
#else
                                              tsc_bool source_address);
#endif


#ifdef TSC_UIP
tsc_socket_info *tsc_tunnel_find_socket_uip_conn (tsc_handle handle,
                                                  struct uip_conn *connection,
                                                  tsc_bool lock);
#endif


tsc_error_code tsc_recv_queue_data (tsc_handle handle,
                                    tsc_lock * out_queue_lock,
                                    tsc_queue * out_queue, void *buffer,
                                    uint32_t * size);

tsc_error_code tsc_recv_udp_queue_data (tsc_handle handle,
                                        tsc_lock * out_queue_lock,
                                        tsc_queue * out_queue,
                                        tsc_ip_port_address * src,
                                        tsc_ip_port_address * dst,
                                        void *buffer, uint32_t * size);

#endif /* __TSC_DATA_H__ */
