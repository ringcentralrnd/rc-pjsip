/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#ifndef __TSC_TRANSACTION_H__
#define __TSC_TRANSACTION_H__

#include "tsc_tunnel.h"
#include "tsc_encoder.h"
#include "tsc_lock.h"

typedef struct tsc_transaction_s
{
    tsc_cm msg;
    /* -1 retry forever */
    int8_t retries;
    time_t timeout;
    time_t due;
    void *opaque;

    tsc_lock *lock;

    tsc_bool (*timeout_callback)(tsc_handle handle, struct tsc_transaction_s *transaction);
    tsc_bool (*retry_callback)(tsc_handle handle, struct tsc_transaction_s *transaction,
                               tsc_cm *request);
    tsc_bool (*response_callback)(tsc_handle handle, struct tsc_transaction_s *transaction,
                                  tsc_cm *response);

    struct tsc_transaction_s *next;
} tsc_transaction;

tsc_error_code tsc_transaction_insert(tsc_handle handle, tsc_cm *msg, int8_t retries, 
                                    time_t timeout, void *opaque, tsc_lock *lock, 
                                    tsc_bool (*timeout_callback)(tsc_handle, tsc_transaction *),
                                    tsc_bool (*retry_callback)(tsc_handle, tsc_transaction *,
                                                               tsc_cm *request),
                                    tsc_bool (*response_callback)(tsc_handle, tsc_transaction *,
                                                                  tsc_cm *response));

tsc_error_code tsc_transaction_remove(tsc_handle handle, tsc_transaction *transaction);

tsc_error_code tsc_transaction_check_timeout(tsc_handle handle);

tsc_error_code tsc_transaction_process_response(tsc_handle handle, void *buf,
                                                size_t len);

#endif /* __TSC_TRANSACTION_H__ */
