/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/
/*! \brief
statistics api
*/
#ifndef __TSC_STATS_H__
#define __TSC_STATS_H__

#include "tsc_tunnel.h"


typedef enum
{
    tsc_sent_bytes = 0,
    tsc_recv_bytes,
    tsc_socket_count,
    tsc_sockets_created,
    tsc_max_queue_gap,
    tsc_dropped_in_packets,
    tsc_dropped_out_packets,
    tsc_reconn_attempts,
    tsc_keep_alive_count,
    tsc_service_request_count,
    tsc_in_packet_count,
    tsc_min_in_processing,
    tsc_avg_in_processing,
    tsc_max_in_processing,
    tsc_out_packet_count,
    tsc_min_out_processing,
    tsc_avg_out_processing,
    tsc_max_out_processing,
    tsc_null

} tsc_stats_option;

typedef enum
{
    tsc_stats_action_inc = 0,
    tsc_stats_action_set,
    tsc_stats_action_clr,
    tsc_stats_action_get,

} tsc_stats_action;

#define TSC_STATS_INC(handle, option, value) tsc_process_stats(handle, option, tsc_stats_action_inc, value, NULL)
#define TSC_STATS_SET(handle, option, value) tsc_process_stats(handle, option, tsc_stats_action_set, value, NULL)
#define TSC_STATS_CLR(handle, option) tsc_process_stats(handle, option, tsc_stats_action_clr, 0, NULL)
#define TSC_STATS_GET(handle, stats) tsc_process_stats(handle, tsc_null, tsc_stats_action_get, 0, stats)

tsc_error_code tsc_process_stats (tsc_handle handle, tsc_stats_option option,
                                  tsc_stats_action action, int value,
                                  tsc_statistics * stats);
#endif
