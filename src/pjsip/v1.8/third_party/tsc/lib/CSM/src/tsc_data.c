/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#define TSC_LOG_SUBSYSTEM TSC_LOG_SUBSYSTEM_STATE_MACHINE

#include "tsc_data.h"

tsc_socket_info *socket_table[TSC_SOCKET_MAX_SOCKETS];

#ifdef TSC_UIP
/*  unfortunatelly uip doesn't have a context to access opaque data
 so let's make one and set it before uip processing*/
tsc_csm_info *uip_current_tunnel;

struct timer periodic_timer;
#endif

tsc_error_code
tsc_socket_init ()
{
    memset (socket_table, 0,
            sizeof (tsc_socket_info *) * TSC_SOCKET_MAX_SOCKETS);

#ifdef TSC_UIP
    uip_init ();

    timer_set (&periodic_timer, CLOCK_SECOND / 2);

    uip_current_tunnel = NULL;
#endif

    return tsc_error_code_ok;
}

tsc_socket_info *
tsc_new_socket ()
{
    int i;

    for (i = 0; i < TSC_SOCKET_MAX_SOCKETS; i++) {
        if (!socket_table[i]) {
            tsc_socket_info *info =
                (tsc_socket_info *) malloc (sizeof (tsc_socket_info));

            if (info) {
                memset (info, 0, sizeof (tsc_socket_info));

                info->fd = i + TSC_SOCKET_FIRST_SOCKET;

                socket_table[i] = info;

                TSC_DEBUG ("tsc_new_socket: new socket %d allocated",
                           info->fd);

                return info;
            }
        }
    }

    TSC_ERROR ("tsc_new_socket: failed to allocate a new socket");

    return NULL;
}

tsc_bool
tsc_delete_socket (int fd)
{
    if (tsc_valid_socket (fd) == tsc_bool_true) {
        free ((void *) socket_table[fd - TSC_SOCKET_FIRST_SOCKET]);

        socket_table[fd - TSC_SOCKET_FIRST_SOCKET] = NULL;

        TSC_DEBUG ("tsc_delete_socket: socket %d has been removed", fd);

        return tsc_bool_true;
    }

    TSC_ERROR ("tsc_delete_socket: failed to remove socket %d", fd);

    return tsc_bool_false;
}

tsc_bool
tsc_valid_socket (int fd)
{
    if (fd >= TSC_SOCKET_FIRST_SOCKET
        && fd < TSC_SOCKET_FIRST_SOCKET + TSC_SOCKET_MAX_SOCKETS) {
        tsc_socket_info *info = socket_table[fd - TSC_SOCKET_FIRST_SOCKET];

        if (info) {
            return tsc_bool_true;
        }
    }

    return tsc_bool_false;
}

int
tsc_socket (tsc_handle handle, int af, int type, int protocol)
{
    tsc_socket_info *info;
    TSC_DEBUG ("tsc_socket: creating new socket (%d,%d,%d) [%p]", af, type,
               protocol, handle);

    if (af != AF_INET) {
        TSC_ERROR ("tsc_new_tunnel: only AF_INET supported [%p]", handle);

        return -1;
    }

#ifndef TSC_UIP
    if (type != SOCK_DGRAM) {
        TSC_ERROR ("tsc_new_tunnel: only SOCK_DGRAM supported [%p]", handle);

        return -1;
    }
#endif

    info = tsc_new_socket ();

    if (info) {
        info->handle = handle;

#ifdef TSC_UIP
        info->type = type;
#endif

        info->out_queue =
            tsc_queue_new (TSC_CSM_QUEUE_SIZE, sizeof (tsc_csm_msg));
	info->in_queue =
	    tsc_queue_new (TSC_CSM_QUEUE_SIZE, sizeof (tsc_csm_msg)); /*temporary size*/

        if (info->out_queue && info->in_queue) {
            info->out_queue_lock = tsc_lock_new ();
	    info->in_queue_lock = tsc_lock_new ();

            if (info->out_queue_lock && info->in_queue_lock) {
                if (tsc_tunnel_insert_socket (handle, info) == tsc_bool_true) {
                    TSC_DEBUG
                        ("tsc_socket: socket initialized fd=%d [%p][%p]",
                         info->fd, info, handle);
                    TSC_STATS_INC (handle, tsc_socket_count, 1);
                    TSC_STATS_INC (handle, tsc_sockets_created, 1);
                    return info->fd;
                }
                else {
                    TSC_ERROR
                        ("tsc_socket: failed to insert socket into tunnel [%p]",
                         handle);

                    tsc_lock_delete (info->out_queue_lock);
		    tsc_lock_delete (info->in_queue_lock);

                    tsc_queue_delete (info->out_queue);
		    tsc_queue_delete (info->in_queue);

                    tsc_delete_socket (info->fd);
                }
            }
            else {
                TSC_ERROR
                    ("tsc_socket: failed to allocate socket queue locks [%p]",
                     handle);

                tsc_queue_delete (info->out_queue);
		tsc_queue_delete (info->in_queue);
                tsc_delete_socket (info->fd);
            }
        }
        else {
            TSC_ERROR ("tsc_socket: failed to allocate socket queues [%p]",
                       handle);

            tsc_delete_socket (info->fd);
        }
    }
    else {
        TSC_ERROR ("tsc_socket: failed to allocate socket [%p]", handle);
    }

    return -1;
}

tsc_bool
tsc_tunnel_insert_socket (tsc_handle handle, tsc_socket_info * info)
{
    if (handle && info) {
        tsc_csm_info *tunnel_info = (tsc_csm_info *) handle;

        if (tsc_lock_get (tunnel_info->data_lock) != tsc_lock_response_error) {
            tsc_socket_info *socket_info = tunnel_info->socket_info;

            if (!socket_info) {
                tunnel_info->socket_info = info;
            }
            else {
                tsc_socket_info *socket_info_prev = NULL;

                while (socket_info) {
                    socket_info_prev = socket_info;
                    socket_info = socket_info->next;
                }

                socket_info_prev->next = info;
            }

            tsc_lock_release (tunnel_info->data_lock);

            TSC_DEBUG ("tsc_tunnel_insert_socket: socket inserted [%p][%p]",
                       info, handle);

            return tsc_bool_true;
        }
    }

    TSC_ERROR ("tsc_tunnel_insert_socket: failed to insert socket [%p][%p]",
               info, handle);

    return tsc_bool_false;
}

tsc_bool
tsc_tunnel_delete_socket (tsc_handle handle, tsc_socket_info * info)
{
    tsc_bool result = tsc_bool_false;

    if (handle && info) {
        tsc_csm_info *tunnel_info = (tsc_csm_info *) handle;

        if (tsc_lock_get (tunnel_info->data_lock) != tsc_lock_response_error) {
            tsc_socket_info *socket_info = tunnel_info->socket_info;

            if (socket_info) {
                if (socket_info == info) {
                    tunnel_info->socket_info = info->next;

                    result = tsc_bool_true;
                }
                else {
                    tsc_socket_info *socket_info_prev = NULL;

                    while (socket_info) {
                        if (socket_info == info) {
                            socket_info_prev->next = info->next;

                            result = tsc_bool_true;

                            break;
                        }

                        socket_info_prev = socket_info;
                        socket_info = socket_info->next;
                    }
                }
            }

            tsc_lock_release (tunnel_info->data_lock);

            if (result == tsc_bool_true) {
                TSC_DEBUG
                    ("tsc_tunnel_delete_socket: socket deleted [%p][%p]",
                     info, handle);
            }
            else {
                TSC_ERROR
                    ("tsc_tunnel_delete_socket: socket not found [%p][%p]",
                     info, handle);
            }

            return result;
        }
        else {
            TSC_ERROR ("tsc_tunnel_delete_socket: cannot get lock [%p][%p]",
                       info, handle);
        }
    }
    else {
        TSC_ERROR
            ("tsc_tunnel_delete_socket: failed to delete socket [%p][%p]",
             info, handle);
    }

    return result;
}

tsc_socket_info *
tsc_tunnel_find_socket_addr (tsc_handle handle, tsc_ip_port_address * addr,
#ifdef TSC_UIP
                             tsc_bool source_address, int type)
#else
                             tsc_bool source_address)
#endif
{
    tsc_socket_info *found = NULL;

    if (handle) {
        tsc_csm_info *tunnel_info = (tsc_csm_info *) handle;

        if (tsc_lock_get (tunnel_info->data_lock) != tsc_lock_response_error) {
            tsc_socket_info *socket_info = tunnel_info->socket_info;

            if (socket_info) {
                tsc_ip_port_address *ptr = &(socket_info->src_address);

#ifdef TSC_UIP
                if (source_address == tsc_bool_false) {
                    ptr = &(socket_info->dst_address);
                }
#endif

#ifdef TSC_UIP
                if (ptr->address == addr->address && ptr->port == addr->port && 
                    socket_info->type == type) {
#else
                if (ptr->address == addr->address && ptr->port == addr->port) {
#endif
                    found = socket_info;
                }
                else {
                    tsc_socket_info *socket_info_prev = NULL;

                    while (socket_info) {
                        tsc_ip_port_address *ptr =
                            &(socket_info->src_address);

#ifdef TSC_UIP
                        if (source_address == tsc_bool_false) {
                            ptr = &(socket_info->dst_address);
                        }
#endif

#ifdef TSC_UIP
                        if (ptr->address == addr->address
                            && ptr->port == addr->port && socket_info->type == type) {
#else
                        if (ptr->address == addr->address
                            && ptr->port == addr->port) {
#endif
                            found = socket_info;

                            break;
                        }

                        socket_info_prev = socket_info;
                        socket_info = socket_info->next;
                    }
                }
            }

            tsc_lock_release (tunnel_info->data_lock);

            return found;
        }
        else {
            TSC_ERROR ("tsc_tunnel_find_socket_addr: cannot get lock [%p]",
                       handle);
        }
    }
    else {
        TSC_ERROR ("tsc_tunnel_find_socket_addr: failed to find socket [%p]",
                   handle);
    }

    return found;
}

#ifdef TSC_UIP
tsc_socket_info *
tsc_tunnel_find_socket_uip_conn (tsc_handle handle,
                                 struct uip_conn * connection, tsc_bool lock)
{
    tsc_socket_info *found = NULL;

    if (handle) {
        tsc_csm_info *tunnel_info = (tsc_csm_info *) handle;

        if (lock == tsc_bool_false
            || (tsc_lock_get (tunnel_info->data_lock) !=
                tsc_lock_response_error)) {
            tsc_socket_info *socket_info = tunnel_info->socket_info;

            if (socket_info) {
                if (socket_info->connection == connection) {
                    found = socket_info;
                }
                else {
                    tsc_socket_info *socket_info_prev = NULL;

                    while (socket_info) {
                        if (socket_info->connection == connection) {
                            found = socket_info;

                            break;
                        }

                        socket_info_prev = socket_info;
                        socket_info = socket_info->next;
                    }
                }
            }

            if (lock == tsc_bool_true) {
                tsc_lock_release (tunnel_info->data_lock);
            }

            return found;
        }
        else {
            TSC_ERROR ("tsc_tunnel_find_socket_addr: cannot get lock [%p]",
                       handle);
        }
    }
    else {
        TSC_ERROR ("tsc_tunnel_find_socket_addr: failed to find socket [%p]",
                   handle);
    }

    return NULL;
}
#endif

tsc_bool
tsc_tunnel_find_socket (tsc_handle handle, tsc_socket_info * info)
{
    tsc_bool found = tsc_bool_false;

    if (handle && info) {
        tsc_csm_info *tunnel_info = (tsc_csm_info *) handle;

        if (tsc_lock_get (tunnel_info->data_lock) != tsc_lock_response_error) {
            tsc_socket_info *socket_info = tunnel_info->socket_info;

            if (socket_info) {
                if (socket_info == info) {
                    found = tsc_bool_true;
                }
                else {
                    tsc_socket_info *socket_info_prev = NULL;

                    while (socket_info) {
                        if (socket_info == info) {
                            found = tsc_bool_true;

                            break;
                        }

                        socket_info_prev = socket_info;
                        socket_info = socket_info->next;
                    }
                }
            }

            tsc_lock_release (tunnel_info->data_lock);

            return found;
        }
        else {
            TSC_ERROR ("tsc_tunnel_find_socket: cannot get lock [%p][%p][%p]",
                       tsc_lock_get_taker_thread(tunnel_info->data_lock),
                       info, handle);
        }
    }
    else {
        TSC_ERROR ("tsc_tunnel_find_socket: failed to find socket [%p][%p]",
                   info, handle);
    }

    return found;
}

int
tsc_bind (int s, struct sockaddr *addr, int namelen)
{
    tsc_socket_info *info = tsc_get_socket_info (s);

    if (info) {
        tsc_csm_info *tunnel_info = (tsc_csm_info *) (info->handle);

        if (tunnel_info) {
            if (tsc_lock_get (tunnel_info->data_lock) != tsc_lock_response_error) {
                struct sockaddr_in *in_addr = (struct sockaddr_in *) addr;

                if (in_addr->sin_family == AF_INET) {
                    /* let's check for other sockets using the same addresses 
                     */
                    tsc_ip_port_address addr;
                    char addr_str[TSC_ADDR_STR_LEN];
                    addr.address = ntohl (in_addr->sin_addr.s_addr);
                    addr.port = ntohs (in_addr->sin_port);

                    tsc_ip_port_address_to_str (&addr, addr_str,
                                                TSC_ADDR_STR_LEN);

                    if (addr.address == INADDR_LOOPBACK) {
                        TSC_DEBUG
                            ("tsc_bind: localhost %s address detected [%p][%p]",
                             addr_str, info, tunnel_info);

                        addr.address =
                            tunnel_info->config.internal_address.address;

                        tsc_ip_port_address_to_str (&addr, addr_str,
                                                    TSC_ADDR_STR_LEN);
                    }

                    if (addr.address !=
                        tunnel_info->config.internal_address.address) {
                        TSC_ERROR
                            ("tsc_bind: address %s is different than internal one [%p][%p]",
                             addr_str, info, tunnel_info);

                        tsc_lock_release (tunnel_info->data_lock);

                        return -1;
                    }
                    if (addr.port == 0) {
                        uint32_t i;
                        tsc_bool found = tsc_bool_false;

                        for (i = 2048; i < TSC_SOCKET_MAX_PORT; i++) {
                            addr.port = i;
                            if (!tsc_tunnel_find_socket_addr
#ifdef TSC_UIP
                                (tunnel_info, &addr, tsc_bool_true, info->type)) {
#else
                                (tunnel_info, &addr, tsc_bool_true)) {
#endif
                                found = tsc_bool_true;
                                tsc_ip_port_address_to_str (&addr, addr_str,
                                                            TSC_ADDR_STR_LEN);
                                break;
                            }
                        }
                        if (!found) {
                            TSC_DEBUG
                                ("tsc_bind: Port allocation failed, no free ports [%p][%p]",
                                 info, tunnel_info);
                            return -1;
                        }


                    }


                    if (!tsc_tunnel_find_socket_addr
#ifdef TSC_UIP
                        (tunnel_info, &addr, tsc_bool_true, info->type)) {
#else
                        (tunnel_info, &addr, tsc_bool_true)) {
#endif
                        memcpy (&(info->src_address), &addr,
                                sizeof (tsc_ip_port_address));

                        TSC_DEBUG ("tsc_bind: socket bound as %s [%p][%p]",
                                   addr_str, info, tunnel_info);

#ifdef TSC_REDUNDANCY
                        if (info->socket_options.redundancy.redundancy_factor > 0) {
                            if (tsc_csm_set_redundancy(tunnel_info, info, &(info->src_address),
                                &(info->socket_options.redundancy), 
                                tsc_bool_true) == tsc_bool_true) {
                                TSC_DEBUG
                                    ("tsc_bind: redundancy was requested [%p][%p]",
                                     info, tunnel_info);
                            } else {
                                TSC_ERROR
                                    ("tsc_bind: failed to set redundancy [%p][%p]",
                                     info, tunnel_info);
                            }
                        }
#endif

                        tsc_lock_release (tunnel_info->data_lock);

                        return 0;
                    }
                    else {
                        TSC_DEBUG
                            ("tsc_bind: address %s is already bound [%p][%p]",
                             addr_str, info, tunnel_info);

                        tsc_lock_release (tunnel_info->data_lock);

                        return -1;
                    }
                }
                else {
                    TSC_ERROR ("tsc_bind: failed to bind socket [%p][%p]",
                               info, tunnel_info);
                }

                tsc_lock_release (tunnel_info->data_lock);
            }
            else {
                TSC_ERROR ("tsc_bind: failed to get lock [%p][%p][%p]", 
                           tsc_lock_get_taker_thread(tunnel_info->data_lock),
                           info, tunnel_info);
            }
        }
        else {
            TSC_ERROR ("tsc_bind: cannot find tunnel info [%p]", info);
        }
    }
    else {
        TSC_ERROR ("tsc_bind: failed to bind socket [%p]", info);
    }

    return -1;
}

tsc_bool
tsc_clear_sockets (tsc_handle handle)
{
    tsc_csm_info *tunnel_info = (tsc_csm_info *) handle;

    if (tunnel_info) {
        tsc_socket_info *socket_info = tunnel_info->socket_info;

        tsc_socket_info *socket_info_next = NULL;

        while (socket_info) {
            socket_info_next = socket_info->next;

            TSC_DEBUG ("tsc_clear_sockets: removing socket [%p][%p]",
                       socket_info, tunnel_info);

            tsc_close (socket_info->fd);

            socket_info = socket_info_next;
        }

        return tsc_bool_true;
    }

    TSC_ERROR ("tsc_clear_sockets: failed to clear sockets [%p]", handle);

    return tsc_bool_false;
}

tsc_bool
tsc_close_aux (tsc_socket_info * info, tsc_csm_info * tunnel_info)
{
    if (tsc_tunnel_delete_socket (tunnel_info, info) == tsc_bool_true) {

	char address[TSC_ADDR_STR_LEN];
	tsc_ip_port_address_to_str(&(info->src_address), address, TSC_ADDR_STR_LEN);
        tsc_lock_delete (info->out_queue_lock);
        tsc_queue_delete (info->out_queue);
	tsc_lock_delete (info->in_queue_lock);
        tsc_queue_delete (info->in_queue);
        tsc_delete_socket (info->fd);


        TSC_DEBUG ("tsc_close_aux: socket terminated %s [%p][%p]", address,
                   info, tunnel_info);

        return tsc_bool_true;
    }

    TSC_ERROR ("tsc_close_aux: cannot remove entry from tunnel [%p][%p]",
               info, tunnel_info);

    return tsc_bool_false;
}

int
tsc_close (int s)
{
    tsc_socket_info *info = tsc_get_socket_info (s);

    if (info) {
        tsc_csm_info *tunnel_info = (tsc_csm_info *) (info->handle);

        if (tunnel_info) {
#ifdef TSC_UIP
            if (info->type == SOCK_STREAM) {
                if (info->state == tsc_uip_state_connected) {
                    tsc_set_socket_state (info, tsc_uip_state_close);
                }

                TSC_ERROR("tsc_close: closing TCP socket [%p][%p]",
                          info, tunnel_info);

                TSC_STATS_INC (info->handle, tsc_socket_count, -1);
                return 0;
            }
#endif
#ifdef TSC_REDUNDANCY
            if (info->redundancy_info.redundancy_factor > 0) {
                tsc_so_redundancy redundancy;
                memset(&redundancy, 0, sizeof(tsc_so_redundancy));

                if (tsc_setsockopt(s, SOL_SOCKET, SO_TSC_REDUNDANCY,
                                   (char *)&redundancy, sizeof(tsc_so_redundancy)) == -1) {
                    TSC_ERROR
                        ("tsc_close: failed to remove redundancy [%p][%p]",
                         info, tunnel_info);
                } else {
                    TSC_DEBUG
                        ("tsc_close: redundancy removal was requested [%p][%p]",
                         info, tunnel_info);
                }

                tsc_sleep(100); /* let's give the request time to be sent, needs to be improved
                                   probabily wait for the notification before proceeding */
            }

            uint8_t i;

            for (i = 0; i < TSC_REDUNDANCY_MAX_STREAMS; i++) {
                if (info->redundancy_tunnel_list[i]) {
                    TSC_DEBUG("tsc_close: removing secundary tunnel %p [%p][%p]",
                        info->redundancy_tunnel_list[i], info, tunnel_info);

                    tsc_delete_tunnel(info->redundancy_tunnel_list[i]);
                }
            }
#endif

            if (tsc_lock_get (tunnel_info->data_lock) != tsc_lock_response_error) {
                tsc_close_aux (info, tunnel_info);

                TSC_STATS_INC (tunnel_info, tsc_socket_count, -1);
                tsc_lock_release (tunnel_info->data_lock);
                return 0;
            }
            else {
                TSC_ERROR ("tsc_close: failed to get lock [%p][%p][%p]",
                           tsc_lock_get_taker_thread(tunnel_info->data_lock), 
                           info, tunnel_info);
            }
        }
        else {
            TSC_ERROR ("tsc_close: cannot find tunnel info [%p]", info);
        }
    }
    else {
        TSC_ERROR ("tsc_close: failed to close [%p]", info);
    }

    return -1;
}

int
tsc_sendto (int s, char *buf_ptr, int len, int flags, struct sockaddr *to,
            int tolen)
{
    tsc_socket_info *info = tsc_get_socket_info (s);
    tsc_set_errno (0);
    int ret_len = 0;
    int send_size = 0;

    if (info) {
        tsc_csm_info *tunnel_info = (tsc_csm_info *) (info->handle);

#ifdef TSC_REDUNDANCY
        if (flags & TSC_SENDTO_REDUNDANCY_FLAG) {
            uint8_t index = flags ^ TSC_SENDTO_REDUNDANCY_FLAG;

            tunnel_info = (tsc_csm_info *)(info->redundancy_tunnel_list[index]);
        }
#endif

        if (tunnel_info) {
            if (tsc_lock_get (tunnel_info->data_lock) != tsc_lock_response_error) {
                struct sockaddr_in *in_addr = (struct sockaddr_in *) to;

                if (in_addr->sin_family == AF_INET) {
                    tsc_ip_port_address dst_addr;
                    dst_addr.address = ntohl (in_addr->sin_addr.s_addr);
                    dst_addr.port = ntohs (in_addr->sin_port);

                    tsc_lock_release (tunnel_info->data_lock);

                    char *buf = buf_ptr;

#ifdef TSC_REDUNDANCY
                    tsc_transport transport =
                        tunnel_info->tunnel_params.connection_params
                                     [tunnel_info->connection_index].transport;

                    /* if transport is UDP/DTLS and redundancy is set we include 
                       our last UDP frame in the current UDP packet */

                    char tmp_buf[TSC_MAX_FRAME_SIZE*(TSC_REDUNDANCY_MAX_STREAMS + 1)];
                    memset(tmp_buf, 0, TSC_MAX_FRAME_SIZE*(TSC_REDUNDANCY_MAX_STREAMS + 1));

                    if (transport == tsc_transport_udp || 
                        transport == tsc_transport_dtls) {
                        if (info->redundancy_info.redundancy_factor > 0) {
                            len = tsc_redundancy_make(info, buf_ptr, len, tmp_buf,
                                                      tsc_bool_true, 0);
                            buf = tmp_buf;
                        }
                    } else {
                        /* this only runs on the primary tunnel */
                        if (info->redundancy_info.redundancy_factor > 0 &&
                            tunnel_info == (tsc_csm_info *) (info->handle)) {
                            if (info->redundancy_info.load_balance == tsc_bool_true) {
                                /* load balance */
                                len = tsc_redundancy_make(info, buf_ptr, len, tmp_buf,
                                                  tsc_bool_true, 0);
                                buf = tmp_buf;

                                uint8_t index = info->redundancy_info.load_balance_index;

                                info->redundancy_info.load_balance_index++;

                                if (info->redundancy_info.load_balance_index >=
                                    info->redundancy_info.load_balance_size) {
                                    info->redundancy_info.load_balance_index = 0;
                                }

                                if (index > 0) {
                                    if (len > 0) {
                                        return tsc_sendto(s, tmp_buf, len, 
                                                   TSC_SENDTO_REDUNDANCY_FLAG + index - 1,
                                                   to, tolen);
                                    }
                                }
                            } else {
                                /* no load balance */
                                uint8_t count;

                                for (count = 0; count < info->redundancy_info.redundancy_factor;
                                     count++) {
                                    char tmp_buf[TSC_MAX_FRAME_SIZE*(TSC_REDUNDANCY_MAX_STREAMS + 1)];
                                    memset(tmp_buf, 0, TSC_MAX_FRAME_SIZE*(TSC_REDUNDANCY_MAX_STREAMS + 1));

                                    int len = tsc_redundancy_make(info, NULL, 0, tmp_buf,
                                                                  tsc_bool_false, count + 1);

                                    if (len > 0) {
                                        tsc_sendto(s, tmp_buf, len,  
                                                   TSC_SENDTO_REDUNDANCY_FLAG + count,
                                                   to, tolen);
                                    }
                                }

                                len = tsc_redundancy_make(info, buf_ptr, len, tmp_buf,
                                                               tsc_bool_false, 0);

                                buf = tmp_buf;
                            }
                        }
                    }
#endif
#ifdef TSC_ODD
		    if (info->socket_options.odd == tsc_bool_true) {

			tsc_state_info state_info;
			tsc_get_state (info->odd_tunnel, &state_info);
			
			if (state_info.state != tsc_state_established && state_info.state != tsc_state_established_slow_poll) {
			    TSC_ERROR ("tsc_sendto: odd tunnel is in wrong state [state %d][%p]", state_info.state, info->odd_tunnel);
			    tunnel_info = (tsc_csm_info *) info->odd_tunnel;
			}
			else {
			    tunnel_info = (tsc_csm_info *) info->odd_tunnel;
			}  
		    }
#endif
                    while (len) {
                        send_size = len;
			
                        /* No chunking is required since the server can handle
                           super frames in inner layer, OS takes care of outer
                           layer fragmentation */
			
                        info->socket_options.private_data = (void *)info;

                        /* if we requested redundancy and it is still pending
                           we just drop the packet */
			
		    
			if (tsc_send_udp_data
			    (tunnel_info, &(info->src_address), &dst_addr,
			     buf + ret_len, send_size, &(info->socket_options))
                            == tsc_error_code_ok) {
			    char dst_addr_str[TSC_ADDR_STR_LEN];
			    tsc_ip_port_address_to_str (&dst_addr,
							dst_addr_str,
							TSC_ADDR_STR_LEN);
			    char src_addr_str[TSC_ADDR_STR_LEN];
			    tsc_ip_port_address_to_str (&(info->src_address),
							src_addr_str,
							TSC_ADDR_STR_LEN);
			    
                            TSC_DEBUG
				("tsc_sendto: %d-byte udp data sent [%s=>%s] [%p][%p]",
                                 send_size, src_addr_str, dst_addr_str, info,
                                 tunnel_info);
                            len -= send_size;
                            ret_len += send_size;
                        }
                        else {
                            TSC_ERROR
                                ("tsc_sendto: tsc_send_udp_data failed [%p][%p]",
                                 info, tunnel_info);
			    
                            ret_len = -1;
			    
                            break;
                        }
		    }
                    return ret_len;
                }
                else {
                    TSC_ERROR
                        ("tsc_sendto: only AF_INET is supported [%p][%p]",
                         info, tunnel_info);
                }
		
                tsc_lock_release (tunnel_info->data_lock);
            }
            else {
                TSC_ERROR ("tsc_sendto: failed to get lock [%p][%p][%p]", 
                           tsc_lock_get_taker_thread(tunnel_info->data_lock),
                           info, tunnel_info);
            }
        }
        else {
            TSC_ERROR ("tsc_sendto: cannot find tunnel info [%p]", info);
        }
    }
    else {
        TSC_ERROR ("tsc_sendto: failed to send data [%p]", info);
    }

    return -1;
}

int
tsc_recvfrom (int s, char *buf, int len, int flags, struct sockaddr *from,
              socklen_t * fromlen)
{
    tsc_socket_info *info = tsc_get_socket_info (s);

    tsc_set_errno (0);

    if (info) {
        tsc_csm_info *tunnel_info = (tsc_csm_info *) (info->handle);

        if (tunnel_info) {
            for (;;) {
                tsc_ip_port_address src_addr;
                tsc_ip_port_address dst_addr;

                uint32_t size = len;

                tsc_error_code result = tsc_recv_udp_queue_data (tunnel_info,
                                                                 info->
                                                                 out_queue_lock,
                                                                 info->
                                                                 out_queue,
                                                                 &src_addr,
                                                                 &dst_addr,
                                                                 buf, &size);

                if (result == tsc_error_code_ok) {
                    struct sockaddr_in *in_addr = (struct sockaddr_in *) from;
                    memset (in_addr, 0, sizeof (struct sockaddr_in));
                    in_addr->sin_family = AF_INET;
                    in_addr->sin_port = htons (src_addr.port);
                    in_addr->sin_addr.s_addr = htonl (src_addr.address);

                    *fromlen = sizeof (struct sockaddr_in);

                    return size;
                }
                else if (result == tsc_error_code_no_data) {
                    if (info->socket_attrib & TSC_SOCKET_ATTRIB_NON_BLOCKING) {
                        tsc_set_errno (EWOULDBLOCK);

                        break;
                    }
                }
                else {
                    TSC_ERROR ("tsc_recvfrom: data failure [%p]", s);
                    tsc_set_errno (EFAULT);

                    break;
                }

                tsc_sleep(1); /* so we don't spin */
            }
        }
    }
    else {
        TSC_ERROR ("tsc_recvfrom: failed to recv data [%p]", s);
    }

    return -1;
}

int
tsc_ioctl (int s, long cmd, unsigned long *argp)
{
    tsc_socket_info *info = tsc_get_socket_info (s);

    if (info) {
        tsc_csm_info *tunnel_info = (tsc_csm_info *) (info->handle);

        if (tunnel_info) {
            if (tsc_lock_get (tunnel_info->data_lock) != tsc_lock_response_error) {
                if (cmd == (long) FIONBIO) {
                    if (!*argp) {
                        if (info->
                            socket_attrib & TSC_SOCKET_ATTRIB_NON_BLOCKING) {
                            info->socket_attrib ^=
                                TSC_SOCKET_ATTRIB_NON_BLOCKING;
                        }

                        TSC_DEBUG ("tsc_ioctl: set blocking [%p][%p]", info,
                                   tunnel_info);
                    }
                    else {
                        info->socket_attrib |= TSC_SOCKET_ATTRIB_NON_BLOCKING;

                        TSC_DEBUG ("tsc_ioctl: set unblocking [%p][%p]", info,
                                   tunnel_info);
                    }

                    tsc_lock_release (tunnel_info->data_lock);

                    return 0;
                }
                else {
                    TSC_ERROR ("tsc_ioctl: unknown cmd %d [%p][%p]", cmd,
                               info, tunnel_info);
                }

                tsc_lock_release (tunnel_info->data_lock);
            }
            else {
                TSC_ERROR ("tsc_ioctl: failed to get lock [%p][%p][%p]",
                           tsc_lock_get_taker_thread(tunnel_info->data_lock),
                           info, tunnel_info);
            }
        }
        else {
            TSC_ERROR ("tsc_ioctl: cannot find tunnel info [%p]", info);
        }
    }
    else {
        TSC_ERROR ("tsc_ioctl: failed to set ioctl [%p]", info);
    }

    return -1;
}

int
tsc_getsockname (int s, struct sockaddr *name, socklen_t *namelen)
{

    struct sockaddr_in *sa;
    struct sockaddr_in t;

    tsc_socket_info *info = tsc_get_socket_info (s);
    
    if (info) {

        sa = sizeof(*sa) > *namelen ? &t : (struct sockaddr_in *)name;
	
        sa->sin_family = AF_INET;
        sa->sin_port = htons(info->src_address.port);
        sa->sin_addr.s_addr = htonl(info->src_address.address);

        if (sa == &t) {
            memcpy(name, sa, *namelen);
        }

        *namelen = sizeof(*sa);
    }
    else{
        TSC_ERROR("tsc_getsockname: failed to return socket info [%d]", s);

        return -1;	
    }

    return 0;
    
}

int
tsc_setsockopt (int s, int level, int optname, char *optval, int optlen)
{
    tsc_socket_info *info = tsc_get_socket_info (s);
    if (info) {
        tsc_csm_info *tunnel_info = (tsc_csm_info *) (info->handle);

        if (tunnel_info) {
            if (level == SOL_SOCKET) {
                if (optname == SO_TSM_REALTIME) {
#if 0
                    if (tsc_lock_get (tunnel_info->data_lock) ==
                        tsc_lock_response_ok) {
                        int value = *((int *) optval);

                        if (value) {
                            info->socket_options.realtime = tsc_bool_true;
                        }
                        else {
                            info->socket_options.realtime = tsc_bool_false;
                        }

                        TSC_DEBUG
                            ("tsc_setsockopt: setting SO_TSM_REALTIME %d [%p][%p]",
                             value, info, tunnel_info);

                        tsc_lock_release (tunnel_info->data_lock);

                        return 0;
                    }
                    else {
                        TSC_ERROR ("tsc_setsockopt: cannot get lock [%p][%p]",
                                   info, tunnel_info);
                    }
#endif
                }
#ifdef TSC_REDUNDANCY
                else if (optname == SO_TSC_REDUNDANCY) {
		    if(info->type == SOCK_DGRAM) {
                        if (tsc_lock_get (tunnel_info->data_lock) !=
                            tsc_lock_response_error) {
                            tsc_so_redundancy *redundancy = (tsc_so_redundancy *)optval;
                            uint8_t redundancy_factor = redundancy->redundancy_factor;

                            if (redundancy_factor > TSC_REDUNDANCY_MAX_STREAMS) {
                                TSC_ERROR
                                    ("tsc_setsockopt: max redundancy reached [%p][%p]",
                                    info, tunnel_info);

                                tsc_lock_release (tunnel_info->data_lock);

                                return -1;
                            }

                            memcpy(&(info->socket_options.redundancy), redundancy, sizeof(tsc_so_redundancy));

                            TSC_DEBUG
                                ("tsc_setsockopt: setting redundancy (%d) [%p][%p]",
                                 redundancy_factor, info, tunnel_info);

                            if (!info->src_address.address && !info->src_address.port) {
                                TSC_DEBUG
                                    ("tsc_setsockopt: socket is not bound yet [%p][%p]",
                                     info, tunnel_info);                            
                            } else {
                                if (tsc_csm_set_redundancy(tunnel_info, info, &(info->src_address),
                                    &(info->socket_options.redundancy), tsc_bool_true) == tsc_bool_true) {
                                    TSC_DEBUG
                                        ("tsc_setsockopt: redundancy was requested [%p][%p]",
                                         info, tunnel_info);
                                } else {
                                    TSC_ERROR
                                        ("tsc_setsockopt: failed to set redundancy [%p][%p]",
                                         info, tunnel_info);
                                }
                            }

                            tsc_lock_release (tunnel_info->data_lock);

                            return 0;
                        }
                        else {
                            TSC_ERROR ("tsc_setsockopt: cannot get lock [%p][%p]",
                                       info, tunnel_info);
                        }
                    }
		    else {
			TSC_ERROR
			    ("tsc_setsockopt: redundancy not allowed on TCP sockets [%p][%p]",
			     info, tunnel_info);
		    }
                }
#endif
#ifdef TSC_ODD
		else if (optname == SO_TSC_ODD) {
		    
		    if(info->type == SOCK_DGRAM) {
			if (tsc_lock_get (tunnel_info->data_lock) !=
			    tsc_lock_response_error) {
			    /*optval;*/
			    uint32_t* port = (uint32_t *) optval;
			    
			    TSC_DEBUG
				("tsc_setsockopt: setting on demand dtls (%d) [%p][%p]",
				 s, info, tunnel_info);
			    
			    if (!info->src_address.address && !info->src_address.port) {
				TSC_ERROR
				    ("tsc_setsockopt: socket is not bound yet (odd option)[%p][%p]",
				     info, tunnel_info);                            
			    } else {
				if (tsc_csm_set_odd(tunnel_info, info, &(info->src_address),
						    tsc_bool_true) == tsc_bool_true) {
				    TSC_DEBUG
					("tsc_setsockopt: on demand dtls tunnel was requested [%p][%p]",
					 info, tunnel_info);
				    tsc_lock_release (tunnel_info->data_lock);	
				    return 0;
				    
				} else {
				    TSC_ERROR
					("tsc_setsockopt: failed to set on demand dtls[%p][%p]",
                                     info, tunnel_info);
				}
			    }
			}
			tsc_lock_release (tunnel_info->data_lock);
			    
		    }
		    else {
			TSC_ERROR
			    ("tsc_setsockopt: failed to set on demand dtls. Socket option only available on socket type SOCK_DGRAM [%p][%p]",
			     info, tunnel_info);
		    }		    
		}
#endif
		else if (optname == IP_TOS) {
                    if (tsc_lock_get (tunnel_info->data_lock) !=
                        tsc_lock_response_error) {
                        uint8_t tos = *((unsigned int *) optval);

                        info->socket_options.tos = tos;

                        TSC_DEBUG
                            ("tsc_setsockopt: setting IP_TOS %X [%p][%p]",
                             tos, info, tunnel_info);

                        tsc_lock_release (tunnel_info->data_lock);

                        return 0;
                    }
                    else {
                        TSC_ERROR ("tsc_setsockopt: cannot get lock [%p][%p]",
                                   info, tunnel_info);
                    }
                }
		else if (optname == SO_TSC_QUEUE_SIZE)
		{
		    if (tsc_lock_get (tunnel_info->data_lock) !=
                        tsc_lock_response_error) {
			
			if (tsc_lock_get (info->in_queue_lock) !=
			    tsc_lock_response_error) {
			
			    if (info->in_queue->gap == 0){

				uint32_t total = *((uint32_t*) optval);
				size_t entry_size = info->in_queue->entry_size;
				tsc_queue_delete(info->in_queue);
				info->in_queue = tsc_queue_new(total, entry_size);

				tsc_lock_release (tunnel_info->data_lock);
				tsc_lock_release (info->in_queue_lock);
				TSC_DEBUG("tsc_setsockopt: SO_TSC_QUEUESIZE socket in_queue set to hold %d messages [%p][%p]",
					  total, info, tunnel_info);
				
				return 0;
				
			    }
			    else{
				
				tsc_lock_release (tunnel_info->data_lock);
				tsc_lock_release (info->in_queue_lock);
				TSC_ERROR("tsc_setsockopt: SO_TSC_QUEUESIZE failed to set new queue size [%p][%p]",
					  info, tunnel_info);
				
			    }	
			}
			else {
			    tsc_lock_release (tunnel_info->data_lock);
			    TSC_ERROR ("tsc_setsockopt: cannot get lock [%p][%p]",
				       info, tunnel_info);
			}

		    }
		    else {
			TSC_ERROR ("tsc_setsockopt: cannot get lock [%p][%p]",
				   info, tunnel_info);
		    }
		}
		else if (optname == SO_TSC_SLOW_IDLE_POLL) {
		    if (tsc_lock_get (tunnel_info->data_lock) !=
		        tsc_lock_response_error) {
		        uint8_t slow_idle_poll = *((unsigned int *) optval);
		        if (slow_idle_poll) {
		            info->socket_options.slow_idle_poll = tsc_bool_true;
		        } else {
		            info->socket_options.slow_idle_poll = tsc_bool_false;
		        }
		        TSC_DEBUG ("tsc_setsockopt: setting SO_TSC_SLOW_IDLE_POLL %d [%p][%p]",
		                   slow_idle_poll, info, tunnel_info);

		        tsc_lock_release (tunnel_info->data_lock);

		        return 0;
		    }
		    else {
		        TSC_ERROR ("tsc_setsockopt: cannot get lock [%p][%p]",
		                   info, tunnel_info);
		    }

		}
		else {
		    TSC_ERROR ("tsc_setsockopt: unknown optname %d [%p][%p]",
                               optname, info, tunnel_info);
		}
	    }
	    else {
		TSC_ERROR ("tsc_setsockopt: unknown level %d [%p][%p]", level,
			   info, tunnel_info);
	    }
	}
	else {
	    TSC_ERROR ("tsc_setsockopt: failed to get tunnel [%p]", level,
		       info);
	}
    }
    else {
        TSC_ERROR ("tsc_setsockopt: failed to setsockopt [%p]", info);
    }
    
    return -1;
}

int
tsc_getsockopt (int s, int level, int optname, char *optval, int *optlen)
{

    tsc_socket_info *info = tsc_get_socket_info (s);

    if (info) {
        tsc_csm_info *tunnel_info = (tsc_csm_info *) (info->handle);

        if (tunnel_info) {
            if (level == SOL_SOCKET) {
                if (optname == SO_ERROR) {
#ifdef TSC_UIP
                    if (info->type == SOCK_STREAM) {
                        if (tsc_lock_get (tunnel_info->data_lock) !=
                            tsc_lock_response_error) {
                            if (info->state == tsc_uip_state_connected) {
                                *((int *) optval) = 0;
                            }
                            else if (info->state == tsc_uip_state_closed
                                     && (info->timeout_timer
                                         && tsc_time () >=
                                         info->timeout_timer)) {
                                *((int *) optval) = ETIMEDOUT;
                            }
                            else if (info->state == tsc_uip_state_failure
                                     && (info->timeout_timer
                                         && tsc_time () >=
                                         info->timeout_timer)) {
                                *((int *) optval) = ECONNRESET;
                            }
                            else {
                                *((int *) optval) = EINPROGRESS;
                            }

                            tsc_lock_release (tunnel_info->data_lock);

                            return 0;
                        }
			else {
                            TSC_ERROR
                                ("tsc_getsockopt: cannot get lock [%p][%p]",
                                 info, tunnel_info);
                        }
			tsc_lock_release (tunnel_info->data_lock);
		    }
		    
		    else if (info->type == SOCK_DGRAM) {
#endif
			if (tsc_lock_get (tunnel_info->data_lock) ==
			    tsc_lock_response_ok) {
			    
			    *((int *) optval) = tunnel_info->tunnel_socket->error;
			    tunnel_info->tunnel_socket->error = 0;
			    tsc_lock_release (tunnel_info->data_lock);
			    return 0;
			}
			else {
                            TSC_ERROR
                                ("tsc_getsockopt: cannot get lock [%p][%p]",
                                 info, tunnel_info);
                        }
			tsc_lock_release (tunnel_info->data_lock);
#ifdef TSC_UIP
		    }

                    else {
                        TSC_ERROR
                            ("tsc_getsockopt: type not found[%p][%p]",
                             info, tunnel_info);
                    }
#endif
                }
#ifdef TSC_UIP
		    
		else if (optname == SO_TYPE){

		    *((int *) optval) = info->type;
		    return 0;
		    
		}
#endif
#ifdef TSC_REDUNDANCY
		else if (optname == SO_TSC_REDUNDANCY) {

		    if (*optlen != sizeof(tsc_so_redundancy)){
			TSC_ERROR ("tsc_getsockopt: Provided optlen is not valid for optname SO_TSC_REDUNDANCY %d [%p][%p]",
				   optname, info, tunnel_info);
			return -1;
		    }

		    memcpy(optval, &(info->socket_options.redundancy), sizeof(tsc_so_redundancy));
		    ((tsc_so_redundancy *)(optval))->redundancy_factor = info->redundancy_info.redundancy_factor;

		    return 0;
			
		}
#endif		
		
                else {
                    TSC_ERROR ("tsc_getsockopt: unknown optname %d [%p][%p]",
                               optname, info, tunnel_info);
                }
            }
            else {
                TSC_ERROR ("tsc_getsockopt: unknown level %d [%p][%p]", level,
                           info, tunnel_info);
            }
        }
        else {
            TSC_ERROR ("tsc_getsockopt: failed to get tunnel [%p]", level,
                       info);
        }
    }
    else {
        TSC_ERROR ("tsc_getsockopt: failed to getsockopt [%p]", info);
    }
    return -1;
}

int
tsc_shutdown (int s, int how)
{
    return tsc_close (s);
}

tsc_error_code
tsc_recv_queue_data (tsc_handle handle, tsc_lock * out_queue_lock,
                     tsc_queue * out_queue, void *buffer, uint32_t * size)
{
    tsc_state_info state_info;
    tsc_csm_info *info;
    tsc_get_state (handle, &state_info);

    if (state_info.state != tsc_state_established && state_info.state != tsc_state_established_slow_poll) {
        TSC_ERROR ("tsc_recv_queue_data: wrong state [state %d][%p]", state_info.state, handle);

        return tsc_error_code_cannot_recv_data;
    }

    info = (tsc_csm_info *) handle;

    if (info) {
        if (tsc_lock_get (out_queue_lock) != tsc_lock_response_error) {
            tsc_csm_msg data;

            if (tsc_queue_read (out_queue, &data) == tsc_queue_response_ok) {
                tsc_lock_release (out_queue_lock);

                if (*size > data.info.buffer.len) {
                    *size = data.info.buffer.len;
                    memcpy (buffer, data.info.buffer.data, *size);

                    uint32_t read_time = tsc_get_clock() - data.timestamp;

                    uint32_t total = info->statistics.in_packet_count * 
                                     info->statistics.avg_in_processing + read_time;

                    TSC_STATS_INC (handle, tsc_in_packet_count, 1);

                    TSC_STATS_SET (handle, tsc_avg_in_processing, total /
                                   info->statistics.in_packet_count);

                    if (read_time > info->statistics.max_in_processing) {
                        TSC_STATS_SET (handle, tsc_max_in_processing, read_time);
                    }

                    if (read_time < info->statistics.min_in_processing) {
                        TSC_STATS_SET (handle, tsc_min_in_processing, read_time);
                    }

                    ((uint8_t *) buffer)[*size] = 0;
                    TSC_STATS_INC (handle, tsc_recv_bytes, *size);

                    return tsc_error_code_ok;
                }
                else {
                    TSC_ERROR
                        ("tsc_recv_queue_data: not enough buffer space (have %d, need %d) [%p]",
                         *size, data.info.buffer.len, handle);

                    return tsc_error_code_error;
                }
            }

            tsc_lock_release (out_queue_lock);

            return tsc_error_code_no_data;
        }
    }

    TSC_ERROR ("tsc_recv_queue_data: failed to recv data [%p]", handle);

    return tsc_error_code_error;

}

tsc_error_code
tsc_recv_udp_queue_data (tsc_handle handle, tsc_lock * out_queue_lock,
                         tsc_queue * out_queue, tsc_ip_port_address * src,
                         tsc_ip_port_address * dst, void *buffer,
                         uint32_t * size)
{
    tsc_error_code result =
        tsc_recv_queue_data (handle, out_queue_lock, out_queue, buffer, size);

    if (result == tsc_error_code_ok) {
        tsc_ip_address src_addr;
        tsc_ip_address dst_addr;

        uint8_t protocol;
        uint8_t version;

        if (tsc_ip_parse (&src_addr, &dst_addr, &protocol, &version, (uint8_t *) buffer)
            == tsc_error_code_ok) {
            if (protocol == SOL_UDP) {
                uint32_t src_port;
                uint32_t dst_port;

                if (tsc_udp_parse
                    (&src_port, &dst_port,
                     (uint8_t *) buffer + TSC_IP_HEADER_SIZE) ==
                    tsc_error_code_ok) {
                    *size -= (TSC_IP_HEADER_SIZE + TSC_UDP_HEADER_SIZE);
                    memmove (buffer,
                            (uint8_t *) buffer + TSC_IP_HEADER_SIZE +
                            TSC_UDP_HEADER_SIZE, *size);
                    src->address = src_addr.address;
                    dst->address = dst_addr.address;
                    src->port = src_port;
                    dst->port = dst_port;

                    ((uint8_t *) buffer)[*size] = 0;
                }
                else {
                    TSC_ERROR
                        ("tsc_recv_udp_queue_data: failed to parse udp header [%p]",
                         handle);

                    result = tsc_error_code_error;
                }
            }
            else {
                TSC_ERROR
                    ("tsc_recv_udp_queue_data: data is not udp (%d) [%p]",
                     protocol, handle);

                result = tsc_error_code_error;
            }
        }
        else {
            TSC_ERROR
                ("tsc_recv_udp_queue_data: failed to parse ip header [%p]",
                 handle);

            result = tsc_error_code_error;
        }
    }

    return result;
}

int
tsc_fcntl (int s, int cmd, long arg)
{
    tsc_socket_info *info = tsc_get_socket_info (s);

    if (info) {
        tsc_csm_info *tunnel_info = (tsc_csm_info *) (info->handle);

        if (tunnel_info) {
            if (tsc_lock_get (tunnel_info->data_lock) != tsc_lock_response_error) {
                if (cmd == F_GETFL) {
                    tsc_lock_release (tunnel_info->data_lock);

                    if (info->socket_attrib & TSC_SOCKET_ATTRIB_NON_BLOCKING) {
                        return O_NONBLOCK;
                    }
                    else {
                        return 0;
                    }
                }
                else if (cmd == F_SETFL) {
                    if (arg & O_NONBLOCK) {
                        if (info->
                            socket_attrib & TSC_SOCKET_ATTRIB_NON_BLOCKING) {
                            info->socket_attrib ^=
                                TSC_SOCKET_ATTRIB_NON_BLOCKING;
                        }

                        TSC_DEBUG ("tsc_fcntl: set unblocking [%p][%p]", info,
                                   tunnel_info);
                    }
                    else {
                        info->socket_attrib |= TSC_SOCKET_ATTRIB_NON_BLOCKING;

                        TSC_DEBUG ("tsc_fcntl: set blocking [%p][%p]", info,
                                   tunnel_info);
                    }

                    tsc_lock_release (tunnel_info->data_lock);

                    return 0;
                }
                else {
                    TSC_ERROR ("tsc_fcntl: unknown cmd %d [%p][%p]", cmd,
                               info, tunnel_info);
                }

                tsc_lock_release (tunnel_info->data_lock);
            }
            else {
                TSC_ERROR ("tsc_fcntl: failed to get lock [%p][%p][%p]", 
                           tsc_lock_get_taker_thread(tunnel_info->data_lock),
                           info, tunnel_info);
            }
        }
        else {
            TSC_ERROR ("tsc_fcntl: cannot find tunnel info [%p]", info);
        }
    }
    else {
        TSC_ERROR ("tsc_fcntl: failed to set ioctl [%p]", info);
    }

    return -1;
}

int
TSC_FD_ISSET (int fd, tsc_fd_set * set)
{
    uint32_t i;

    for (i = 0; i < ((tsc_fd_set *) (set))->fd_count; i++) {
        if (((tsc_fd_set *) (set))->fd_array[i] == fd) {
            return 1;
        }
    }

    return 0;
}

int
tsc_select (int nfds, tsc_fd_set * readfds, tsc_fd_set * writefds,
            tsc_fd_set * exceptfds, struct tsc_timeval *timeout)
{
    int total = 0;
    tsc_fd_set readfds_o;
    tsc_fd_set writefds_o;
    tsc_fd_set exceptfds_o;

    /* in ms */
    int32_t total_time = -1;

    if (timeout) {
        total_time = timeout->tv_sec * 1000 + timeout->tv_usec / 1000;
    }

    TSC_FD_ZERO (&readfds_o);

    TSC_FD_ZERO (&writefds_o);
    
    TSC_FD_ZERO (&exceptfds_o);

    if(exceptfds) {
        *exceptfds = exceptfds_o;
    }

    while ((total_time >= 0) || (total_time == -1)) {
        uint32_t i;

        for (i = 0; readfds && (i < ((tsc_fd_set *) (readfds))->fd_count);
             i++) {
            int s = ((tsc_fd_set *) (readfds))->fd_array[i];

            tsc_socket_info *info = tsc_get_socket_info (s);

            if (info) {
                tsc_handle handle = info->handle;

                if (s < nfds
                    && (tsc_tunnel_find_socket (handle, info) ==
                        tsc_bool_true)) {
                    tsc_csm_info *tunnel_info =
                        (tsc_csm_info *) (info->handle);

                    if (tunnel_info == (tsc_csm_info *) handle) {
                        if (tsc_lock_get (tunnel_info->data_lock) !=
                            tsc_lock_response_error) {
                            tsc_bool ready_to_read;
                            if (tunnel_info->state_info.state ==
                                tsc_state_fatal_error) {
                                TSC_ERROR ("tsc_select: wrong state [state %d][%p][%p]",
					   tunnel_info->state_info.state, info, tunnel_info);

                                tsc_lock_release (tunnel_info->data_lock);

                                tsc_set_errno (EFAULT);

                                return -1;
                            }

                            ready_to_read = tsc_bool_false;

#ifdef TSC_UIP
                            if (info->type == SOCK_STREAM
                                && (info->recv_buffer.ptr > 0)) {
                                ready_to_read = tsc_bool_true;
                            }
#endif

#ifdef TSC_UIP
                            if (info->type == SOCK_DGRAM
                                && (info->out_queue->gap > 0)) {
#else
                            if (info->out_queue->gap > 0) {
#endif
                                ready_to_read = tsc_bool_true;
                            }

                            if (ready_to_read == tsc_bool_true) {
                                total++;

                                TSC_FD_SET (s, &readfds_o);
                            }

                            tsc_lock_release (tunnel_info->data_lock);
                        }
                        else {
                            TSC_ERROR
                                ("tsc_select: (read) failed get lock [%p][%p]",
                                 info, tunnel_info);

                            tsc_set_errno (EFAULT);

                            return -1;
                        }
                    }
                    else {
                        TSC_ERROR
                            ("tsc_select: (read) failed get tunnel handle [%p]",
                             info);

                        tsc_set_errno (EFAULT);

                        return -1;
                    }
                }
            }
        }

        for (i = 0; writefds && (i < ((tsc_fd_set *) (writefds))->fd_count);
             i++) {
            int s = ((tsc_fd_set *) (writefds))->fd_array[i];

            tsc_socket_info *info = tsc_get_socket_info (s);

            if (info) {
                tsc_handle handle = info->handle;

                if (s < nfds
                    && (tsc_tunnel_find_socket (handle, info) ==
                        tsc_bool_true)) {
                    tsc_csm_info *tunnel_info =
                        (tsc_csm_info *) (info->handle);

                    if (tunnel_info == (tsc_csm_info *) handle) {
                        if (tsc_lock_get (tunnel_info->data_lock) !=
                            tsc_lock_response_error) {
#ifdef TSC_UIP
                            if(info->type == SOCK_STREAM) {
                                if(info->state == tsc_uip_state_connected) {
                                   total++;

                                   TSC_FD_SET (s, &writefds_o);
                                }
                            }else if(info->type == SOCK_DGRAM){
                                total++;

                                TSC_FD_SET (s, &writefds_o);
                            }else{
                                /*This should not happen*/
                                TSC_ERROR("tsc_select: socket info type is not correct!");
                            }
#else
                            total++;

                            TSC_FD_SET (s, &writefds_o);
#endif

                            tsc_lock_release (tunnel_info->data_lock);
                        }
                        else {
                            TSC_ERROR
                                ("tsc_select: (write) failed get lock [%p][%p]",
                                 info, tunnel_info);

                            tsc_set_errno (EFAULT);

                            return -1;
                        }
                    }
                    else {
                        TSC_ERROR
                            ("tsc_select: (write) failed get tunnel handle [%p]",
                             info);

                        tsc_set_errno (EFAULT);

                        return -1;
                    }
                }
            }
        }

        if (total) {
            if (readfds) {
                *readfds = readfds_o;
            }

            if (writefds) {
                *writefds = writefds_o;
            }

            break;
        }

        /* wait forever */
        if (total_time == -1) {
            tsc_sleep (TSC_TIMEVAL_DELTA);
        }
        else if (total_time > TSC_TIMEVAL_DELTA) {
            total_time -= TSC_TIMEVAL_DELTA;
            tsc_sleep (TSC_TIMEVAL_DELTA);
        }
        else if (total_time > 0) {
            tsc_sleep (total_time);
            if (readfds) {
                *readfds = readfds_o;
            }

            if (writefds) {
                *writefds = writefds_o;
            }
            break;
        }
        else {
            if (readfds) {
                *readfds = readfds_o;
            }

            if (writefds) {
                *writefds = writefds_o;
            }
            break;
        }
    }

    return total;
}

int
tsc_poll (struct tsc_pollfd *fds, tsc_nfds_t nfds, int timeout)
{
    struct tsc_timeval poll_timeout;

    struct tsc_timeval *poll_timeout_ptr = NULL;

    tsc_nfds_t i;

    int max_fd = 0;

    uint32_t read_count = 0;
    tsc_fd_set read_flags;

    uint32_t write_count = 0;
    tsc_fd_set write_flags;
    tsc_fd_set *write_flags_ptr;
    tsc_fd_set *read_flags_ptr;
    int res;

    if (timeout >= 0) {
        poll_timeout.tv_sec = timeout / 1000;
        poll_timeout.tv_usec = (timeout - poll_timeout.tv_sec * 1000) * 1000;

        poll_timeout_ptr = &poll_timeout;
    }

    TSC_FD_ZERO (&read_flags);

    TSC_FD_ZERO (&write_flags);

    for (i = 0; i < nfds; i++) {
        if (fds[i].events & TSC_POLLIN) {
            TSC_FD_SET (fds[i].fd, &read_flags);
            read_count++;
        }

        if (fds[i].events & TSC_POLLOUT) {
            TSC_FD_SET (fds[i].fd, &write_flags);
            write_count++;
        }

        if (fds[i].fd > max_fd) {
            max_fd = fds[i].fd;
        }

        fds[i].revents = 0;
    }

    write_flags_ptr = &write_flags;
    read_flags_ptr = &read_flags;

    if (!read_count) {
        read_flags_ptr = NULL;
    }

    if (!write_count) {
        write_flags_ptr = NULL;
    }

    res =
        tsc_select (max_fd + 1, read_flags_ptr, write_flags_ptr, NULL,
                    poll_timeout_ptr);

    if (res > 0) {
        for (i = 0; i < nfds; i++) {
            if (read_flags_ptr) {
                if (TSC_FD_ISSET (fds[i].fd, read_flags_ptr)) {
                    fds[i].revents |= TSC_POLLIN;
                }
            }

            if (write_flags_ptr) {
                if (TSC_FD_ISSET (fds[i].fd, write_flags_ptr)) {
                    fds[i].revents |= TSC_POLLOUT;
                }
            }
        }
    }

    return res;
}

tsc_bool
tsc_is_valid_fd (tsc_handle handle, int fd)
{
    return tsc_tunnel_find_socket (handle, tsc_get_socket_info (fd));
}

tsc_socket_info *
tsc_get_socket_info (int fd)
{
    if (tsc_valid_socket (fd) == tsc_bool_true) {
        return socket_table[fd - TSC_SOCKET_FIRST_SOCKET];
    }

    return NULL;
}

int
tsc_connect (int s, struct sockaddr *name, int namelen)
{
#ifdef TSC_UIP
    tsc_socket_info *info = tsc_get_socket_info (s);

    tsc_set_errno (0);

    if (info) {
        tsc_csm_info *tunnel_info = (tsc_csm_info *) (info->handle);

        if (tunnel_info) {
            if(tsc_lock_get (tunnel_info->data_lock) !=
               tsc_lock_response_error) {
                /*This is mainly for non-blocking case, if inner tcp 
                  already connected. Then return 0 immediately.*/
                if(info->state == tsc_uip_state_connected) {
                    tsc_lock_release (tunnel_info->data_lock);
                    return 0;
                }
                tsc_lock_release (tunnel_info->data_lock);
            } else {
                    TSC_ERROR ("tsc_connect: failed to get lock [%p][%p][%p]",
                               tsc_lock_get_taker_thread(tunnel_info->data_lock),
                               info, tunnel_info);
                    return -1;
            }
            
            struct sockaddr_in local_addr;
            tsc_config config;
            uint16_t local_port = 0;

            tsc_get_config (tunnel_info, &config);

            memset (&local_addr, 0, sizeof (struct sockaddr_in));
            local_addr.sin_family = AF_INET;

            local_addr.sin_port = htons (local_port);
            local_addr.sin_addr.s_addr =
                htonl (config.internal_address.address);

            if (!tsc_bind
                (s, (struct sockaddr *) (&local_addr),
                 sizeof (struct sockaddr_in))) {
                if (tsc_lock_get (tunnel_info->data_lock) !=
                    tsc_lock_response_error) {
                    struct sockaddr_in *in_addr = (struct sockaddr_in *) name;

                    if (in_addr->sin_family == AF_INET) {
                        tsc_ip_port_address addr;
                        char addr_str[TSC_ADDR_STR_LEN];

                        addr.address = ntohl (in_addr->sin_addr.s_addr);
                        addr.port = ntohs (in_addr->sin_port);

                        memcpy (&(info->dst_address), &addr,
                                sizeof (tsc_ip_port_address));

                        tsc_ip_port_address_to_str (&addr, addr_str,
                                                    TSC_ADDR_STR_LEN);

                        tsc_set_socket_state (info, tsc_uip_state_connect);

                        tsc_lock_release (tunnel_info->data_lock);

                        TSC_DEBUG
                            ("tsc_connect: attempting to connect to %s [%p][%p]",
                             addr_str, info, tunnel_info);

                        info->timeout_timer = 0;

                        for (;;) {
                            if (tsc_lock_get (tunnel_info->data_lock) !=
                                tsc_lock_response_error) {
                                if (info->state == tsc_uip_state_connected) {
                                    tsc_lock_release (tunnel_info->data_lock);

                                    return 0;
                                } else if (info->state == tsc_uip_state_failure) {
                                    tsc_lock_release (tunnel_info->data_lock);

                                    return -1;
                                }

                                if (info->
                                    socket_attrib &
                                    TSC_SOCKET_ATTRIB_NON_BLOCKING) {
                                    tsc_set_errno (EWOULDBLOCK);

                                    TSC_DEBUG
                                        ("tsc_connect: would block [%p][%p]",
                                         info, tunnel_info);

                                    tsc_lock_release (tunnel_info->data_lock);

                                    break;
                                }

                                tsc_lock_release (tunnel_info->data_lock);

                                if (info->timeout_timer
                                    && tsc_time () >= info->timeout_timer) {
                                    return -1;
                                }
                            }
                        }

                        return -1;
                    }
                    else {
                        TSC_ERROR ("tsc_connect: unknown family [%p][%p]",
                                   info, tunnel_info);

                        tsc_lock_release (tunnel_info->data_lock);

                        return -1;
                    }
                }
                else {
                    TSC_ERROR ("tsc_connect: failed to get lock [%p][%p][%p]",
                               tsc_lock_get_taker_thread(tunnel_info->data_lock),
                               info, tunnel_info);

                    return -1;
                }
            }
            else {
                tsc_ip_port_address addr;
                char addr_str[TSC_ADDR_STR_LEN];
                addr.address = ntohl (local_addr.sin_addr.s_addr);
                addr.port = ntohs (local_addr.sin_port);


                tsc_ip_port_address_to_str (&addr, addr_str,
                                            TSC_ADDR_STR_LEN);

                TSC_ERROR
                    ("tsc_connect: cannot bind local address %s [%p][%p]",
                     addr_str, info, tunnel_info);

                return -1;
            }
        }
        else {
            TSC_ERROR ("tsc_connect: cannot find tunnel info [%p]", info);
        }
    }
    else {
        TSC_ERROR ("tsc_connect: failed to connect socket [%p]", info);
    }

    return -1;
#else
    TSC_ERROR ("tsc_connect: not supported");

    return -1;
#endif
}

int
tsc_accept (int s, struct sockaddr *addr, int *addrlen)
{
#ifdef TSC_UIP
    tsc_socket_info *info;
    tsc_set_errno (0);

    info = tsc_get_socket_info (s);

    if (info) {
        if (info->type == SOCK_STREAM) {
            if (info->state == tsc_uip_state_listening
                || info->state == tsc_uip_state_listen) {
                tsc_csm_info *tunnel_info = (tsc_csm_info *) (info->handle);

                if (tunnel_info) {
                    for (;;) {
                        if (tsc_lock_get (tunnel_info->data_lock) !=
                            tsc_lock_response_error) {
                            if (info->pending_count > 0) {
                                struct sockaddr_in *in_addr;
                                tsc_socket_info *socket_info =
                                    info->pending_list[0];

                                info->pending_count--;

                                memcpy (info->pending_list,
                                        info->pending_list + 1,
                                        info->pending_count *
                                        sizeof (struct tsc_socket_info_s *));

                                in_addr = (struct sockaddr_in *) addr;
                                in_addr->sin_family = AF_INET;
                                in_addr->sin_addr.s_addr =
                                    htonl (socket_info->dst_address.address);
                                in_addr->sin_port =
                                    htons (socket_info->dst_address.port);

                                tsc_lock_release (tunnel_info->data_lock);

                                TSC_DEBUG
                                    ("tsc_accept: socket %d accepted [%p][%p]",
                                     socket_info->fd, info, tunnel_info);

                                return socket_info->fd;
                            }

                            tsc_lock_release (tunnel_info->data_lock);
                        }
                        else {
                            if (info->
                                socket_attrib &
                                TSC_SOCKET_ATTRIB_NON_BLOCKING) {
                                tsc_set_errno (EAGAIN);

                                break;
                            }
                        }
                    }
                }
                else {
                    TSC_ERROR ("tsc_accept: cannot find tunnel info [%p]",
                               info);
                }
            }
            else {
                TSC_ERROR ("tsc_accept: socket is not listening [%p]", info);
            }
        }
        else {
            TSC_ERROR ("tsc_accept: socket is not TCP [%p]", info);
        }
    }
    else {
        TSC_ERROR ("tsc_accept: failed to accept [%p]", info);
    }

    return -1;
#else
    TSC_ERROR ("tsc_accept: not supported");

    return -1;
#endif
}

int
tsc_listen (int s, int backlog)
{
#ifdef TSC_UIP
    tsc_socket_info *info = tsc_get_socket_info (s);
    tsc_set_errno (0);

    if (info) {
        if (info->type == SOCK_STREAM) {
            tsc_csm_info *tunnel_info = (tsc_csm_info *) (info->handle);

            if (tunnel_info) {
                if (tsc_lock_get (tunnel_info->data_lock) !=
                    tsc_lock_response_error) {
                    if (info->src_address.address || info->src_address.port) {
                        if (info->state == tsc_uip_state_idle) {
                            tsc_set_socket_state (info, tsc_uip_state_listen);

                            if (backlog > TSC_UIP_MAX_BACKLOG) {
                                backlog = TSC_UIP_MAX_BACKLOG;

                                TSC_DEBUG
                                    ("tsc_listen: set backlog limit to %d [%p][%p]",
                                     backlog, info, tunnel_info);
                            }

                            info->backlog = backlog;
                            info->pending_count = 0;

                            tsc_lock_release (tunnel_info->data_lock);

                            TSC_DEBUG
                                ("tsc_listen: socket ready to listen [%p][%p]",
                                 info, tunnel_info);

                            return 0;
                        }
                        else {
                            TSC_ERROR
                                ("tsc_listen: socket is not idle [%p][%p]",
                                 info, tunnel_info);

                            tsc_set_errno (EADDRINUSE);
                        }
                    }
                    else {
                        TSC_ERROR ("tsc_listen: socked is not bound [%p][%p]",
                                   info, tunnel_info);

                        tsc_set_errno (EBADF);
                    }
                }
                else {
                    TSC_ERROR ("tsc_listen: failed to get lock [%p][%p][%p]",
                               tsc_lock_get_taker_thread(tunnel_info->data_lock),
                               info, tunnel_info);
                }
            }
            else {
                TSC_ERROR ("tsc_listen: cannot find tunnel info [%p]", info);
            }
        }
        else {
            TSC_ERROR ("tsc_listen: socket is not TCP [%p]", info);
        }
    }
    else {
        TSC_ERROR ("tsc_listen: failed to listen [%p]", info);
    }

    return -1;
#else
    TSC_ERROR ("tsc_listen: not supported");

    return -1;
#endif
}

tsc_handle
tsc_get_tunnel (int s)
{
    tsc_socket_info *info = tsc_get_socket_info (s);

    if (info) {
        if (info->handle) {
            return info->handle;
        }
    }

    TSC_ERROR ("tsc_get_tunnel: tunnel handle not found");

    return NULL;
}

int
tsc_recv (int s, char *buf, int len, int flags)
{
#ifdef TSC_UIP
    tsc_socket_info *info = tsc_get_socket_info (s);

    tsc_set_errno (0);

    if (info) {
        if (info->type == SOCK_STREAM) {
            if (info->state == tsc_uip_state_connected) {
                tsc_csm_info *tunnel_info = (tsc_csm_info *) (info->handle);

                if (tunnel_info) {
                    for (;;) {
                        if (tsc_lock_get (tunnel_info->data_lock) !=
                            tsc_lock_response_error) {
                            int result =
                                tsc_read_tcp_buffer (&(info->recv_buffer),
                                                     (uint8_t *) buf, len);

                            tsc_lock_release (tunnel_info->data_lock);

                            if (result > 0) {
                                /* hack for SIP... SD sends IP/UDP stuff
                                   inside data */
                                if (buf[0] == 0x0) {
                                    result -= 44;
                                    memmove (buf, buf + 44, result);
                                }
                                /* end of hack. needs to be removed long term 
                                   (until SD is fixed) */

                                return result;
                            }
                            else {
                                if (info->
                                    socket_attrib &
                                    TSC_SOCKET_ATTRIB_NON_BLOCKING) {
                                    tsc_set_errno (EWOULDBLOCK);

                                    break;
                                }
                            }
                        }
                    }
                }
                else {
                    TSC_ERROR ("tsc_recv: cannot find tunnel info [%p]",
                               info);
                }
            }
            else {
                TSC_ERROR ("tsc_recv: socket is not connected [%p]", info);

                /* to comply with BSD specs for sockets, when far end has
                   disconnected we must return 0 */

                return 0;
            }
        }
        else {
            TSC_ERROR ("tsc_recv: socket is not TCP [%p]", info);
        }
    }
    else {
        TSC_ERROR ("tsc_recv: failed to send data [%p]", info);
    }

    return -1;
#else
    TSC_ERROR ("tsc_recv: not supported");

    return -1;
#endif
}

int
tsc_send (int s, char *buf, int len, int flags)
{
#ifdef TSC_UIP
    tsc_socket_info *info;
    tsc_set_errno (0);

    info = tsc_get_socket_info (s);

    if (info) {
        if (info->type == SOCK_STREAM) {
            tsc_csm_info *tunnel_info = (tsc_csm_info *) (info->handle);

            if (tunnel_info) {
                if (tsc_lock_get (tunnel_info->data_lock) !=
                    tsc_lock_response_error) {
                    if (info->state == tsc_uip_state_connected) {
                        int result = -1;

                        if (tsc_write_tcp_buffer
                            (&(info->send_buffer), (uint8_t *) buf,
                             len) == tsc_bool_true) {
                            TSC_DEBUG
                                ("tsc_send: out %d-byte tcp data buffered [%p][%p]",
                                 len, info, tunnel_info);

                            result = len;
                        }
                        else {
                            TSC_ERROR ("tsc_send: failed to buffer [%p][%p]",
                                       info, tunnel_info);
                        }

                        tsc_lock_release (tunnel_info->data_lock);

                        return result;
                    }
                    else {
                        TSC_ERROR
                            ("tsc_send: socket is not connected [%p][%p]",
                             info, tunnel_info);

                    }
                }
                else {
                    TSC_ERROR ("tsc_send: failed to get lock [%p][%p][%p]", 
                               tsc_lock_get_taker_thread(tunnel_info->data_lock),
                               info, tunnel_info);
                }
            }
            else {
                TSC_ERROR ("tsc_send: cannot find tunnel info [%p]", info);
            }
        }
        else {
            TSC_ERROR ("tsc_send: socket is not TCP [%p]", info);
        }
    }
    else {
        TSC_ERROR ("tsc_send: failed to send data [%p]", info);
    }

    return -1;
#else
    TSC_ERROR ("tsc_send: not supported");

    return -1;
#endif
}

#ifdef TSC_UIP
void
uip_log (char *msg)
{
    TSC_DEBUG ("uip_log: %s", msg);
}
#endif

#ifdef TSC_UIP
void
tsc_uip_process ()
{
    tsc_socket_info *socket_info =
        tsc_tunnel_find_socket_uip_conn (uip_current_tunnel, uip_conn,
                                         tsc_bool_true);

    if (socket_info) {
        if (uip_poll() || uip_acked()) {
            if (socket_info->send_buffer.ptr) {
                int len = socket_info->send_buffer.ptr;

                if (len > TSC_UIP_FRAME_SIZE) {
                    TSC_DEBUG ("tsc_uip_process: truncating TCP frame (len %d) [%p][%p]", 
                               len, socket_info, socket_info->handle);

                    len = TSC_UIP_FRAME_SIZE;
                }

                TSC_DEBUG ("tsc_uip_process: buffering TCP frame (len %d) [%p][%p]",
                           len, socket_info, socket_info->handle);

                uip_send (socket_info->send_buffer.data, len);

                socket_info->retx_buffer.ptr = 0;
                tsc_write_tcp_buffer(&(socket_info->retx_buffer), socket_info->send_buffer.data, len);

                socket_info->send_buffer.ptr -= len;
                memmove (socket_info->send_buffer.data, socket_info->send_buffer.data + len, 
                         socket_info->send_buffer.ptr);
            }
        } else if (uip_rexmit()) {
            if (socket_info->retx_buffer.ptr) {
                TSC_DEBUG ("tsc_uip_process: rexmitting TCP frame (len %d) [%p][%p]",
                           socket_info->retx_buffer.ptr, socket_info, socket_info->handle);

                uip_send (socket_info->retx_buffer.data, socket_info->retx_buffer.ptr);
            } else {
                TSC_ERROR ("tsc_uip_process: nothing to rexmit [%p][%p]",
                           socket_info, socket_info->handle);
            }
        }

        if (uip_datalen() && uip_newdata()) {
            if (tsc_write_tcp_buffer
                (&(socket_info->recv_buffer), (uint8_t *) uip_appdata,
                 uip_datalen ()) == tsc_bool_true) {
                TSC_DEBUG
                    ("tsc_uip_process: in %d-byte tcp data buffered [%p][%p]",
                     uip_datalen (), socket_info, socket_info->handle);
            }
            else {
                TSC_ERROR ("tsc_uip_process: failed to buffer [%p][%p]",
                           socket_info, socket_info->handle);
            }
        }

        if (socket_info->state == tsc_uip_state_connecting) {
            if (uip_connected ()) {
                tsc_set_socket_state (socket_info, tsc_uip_state_connected);
            }
            if (uip_timedout ()) {
                tsc_set_socket_state (socket_info, tsc_uip_state_failure);
            }
        }

        if (uip_closed ()) {
            if (socket_info->state == tsc_uip_state_connected
                || socket_info->state == tsc_uip_state_closing) {
                tsc_set_socket_state (socket_info, tsc_uip_state_closed);
            }
        }

        if (socket_info->state == tsc_uip_state_close) {
            tsc_set_socket_state (socket_info, tsc_uip_state_closing);

            TSC_ERROR ("tsc_uip_process: closing connection [%p][%p]",
                       socket_info, socket_info->handle);

            uip_close ();
        }

        if (uip_conn->tcpstateflags == UIP_CLOSED) {
            tsc_set_socket_state (socket_info, tsc_uip_state_failure);
        }
    }
    else {                      /* no socket_info */
        if (uip_connected ()) {
            tsc_config config;
            tsc_ip_port_address local_addr;
            tsc_socket_info *parent_socket;
            tsc_get_config (uip_current_tunnel, &config);


            local_addr.address = config.internal_address.address;
            local_addr.port = htons (uip_conn->lport);

            parent_socket =
                tsc_tunnel_find_socket_addr (uip_current_tunnel, &local_addr,
#ifdef TSC_UIP
                                             tsc_bool_true, socket_info->type);
#else
                                             tsc_bool_true);
#endif

            if (parent_socket) {
                if (parent_socket->pending_count < parent_socket->backlog) {
                    int new_socket =
                        tsc_socket (uip_current_tunnel, AF_INET, SOCK_STREAM,
                                    0);

                    tsc_socket_info *socket_info =
                        tsc_get_socket_info (new_socket);

                    if (socket_info) {
                        char addr_str[TSC_ADDR_STR_LEN];
                        uint32_t *addr_ptr;

                        TSC_DEBUG
                            ("tsc_uip_process: new socket created [%p][%p]",
                             socket_info, socket_info->handle);


                        socket_info->dst_address.port =
                            ntohs (uip_conn->rport);
                        addr_ptr = (uint32_t *) (uip_conn->ripaddr);
                        socket_info->dst_address.address = ntohl (*addr_ptr);

                        tsc_ip_port_address_to_str (&
                                                    (socket_info->
                                                     dst_address), addr_str,
                                                    TSC_ADDR_STR_LEN);

                        TSC_DEBUG
                            ("tsc_uip_process: remote address %s [%p][%p]",
                             addr_str, socket_info, socket_info->handle);

                        uip_conn->appstate.socket_info = (void *) socket_info;

                        socket_info->connection = uip_conn;

                        tsc_set_socket_state (socket_info,
                                              tsc_uip_state_connected);

                        tsc_ip_port_address_to_str (&local_addr, addr_str,
                                                    TSC_ADDR_STR_LEN);

                        TSC_DEBUG
                            ("tsc_uip_process: found parent socket %s [%p][%p]",
                             addr_str, socket_info, socket_info->handle);

                        socket_info->parent_socket = parent_socket;

                        parent_socket->pending_list[parent_socket->
                                                    pending_count] =
                            socket_info;

                        parent_socket->pending_count++;
                    }
                    else {
                        TSC_ERROR
                            ("tsc_uip_process: failed to create new socket [%p][%p]",
                             socket_info, uip_current_tunnel);

                        uip_close ();
                    }
                }
                else {
                    TSC_ERROR
                        ("tsc_uip_process: reaching backlog limit [%p][%p]",
                         socket_info, socket_info->handle);

                    uip_close ();
                }
            }
            else {
                TSC_ERROR
                    ("tsc_uip_process: failed to find parent socket [%p][%p]",
                     socket_info, socket_info->handle);

                uip_close ();
            }
        }
    }
}
#endif

#ifdef TSC_UIP
tsc_bool
tsc_check_tcp (tsc_csm_info * tunnel_info, tsc_socket_info * socket_info)
{
    uip_current_tunnel = tunnel_info;

    if (socket_info->type == SOCK_STREAM) {
        if (socket_info->state == tsc_uip_state_connect) {
            uip_ipaddr_t ip_addr;
            uint32_t address;
            struct uip_conn *connection;
            tsc_set_socket_state (socket_info, tsc_uip_state_connecting);


            address = socket_info->src_address.address;

#ifdef TSC_WINDOWS
            uip_ipaddr (ip_addr, (address & 0xff000000) >> 24,
                        (address & 0x00ff0000) >> 16,
                        (address & 0x0000ff00) >> 8, (address & 0x000000ff));
#else
            uip_ipaddr (ip_addr, (address & 0x00ff0000) >> 16,
                        (address & 0xff000000) >> 24, (address & 0x000000ff),
                        (address & 0x0000ff00) >> 8);
#endif

            uip_sethostaddr (ip_addr);

            address = socket_info->dst_address.address;

#ifdef TSC_WINDOWS
            uip_ipaddr (ip_addr, (address & 0xff000000) >> 24,
                        (address & 0x00ff0000) >> 16,
                        (address & 0x0000ff00) >> 8, (address & 0x000000ff));
#else
            uip_ipaddr (ip_addr, (address & 0x00ff0000) >> 16,
                        (address & 0xff000000) >> 24, (address & 0x000000ff),
                        (address & 0x0000ff00) >> 8);
#endif

            connection =
                uip_connect (&ip_addr, htons (socket_info->dst_address.port));

            connection->appstate.socket_info = (void *) socket_info;

            socket_info->connection = connection;
        }
        else if (socket_info->state == tsc_uip_state_listen) {
            char addr_str[TSC_ADDR_STR_LEN];
            uip_ipaddr_t ip_addr;
            uint32_t address;
            tsc_set_socket_state (socket_info, tsc_uip_state_listening);

            tsc_ip_port_address_to_str (&(socket_info->src_address), addr_str,
                                        TSC_ADDR_STR_LEN);

            TSC_DEBUG ("tsc_check_tcp: socket listening as %s [%p][%p]",
                       addr_str, socket_info, socket_info->handle);


            address = socket_info->src_address.address;

#ifdef TSC_WINDOWS
            uip_ipaddr (ip_addr, (address & 0xff000000) >> 24,
                        (address & 0x00ff0000) >> 16,
                        (address & 0x0000ff00) >> 8, (address & 0x000000ff));
#else
            uip_ipaddr (ip_addr, (address & 0x00ff0000) >> 16,
                        (address & 0xff000000) >> 24, (address & 0x000000ff),
                        (address & 0x0000ff00) >> 8);
#endif

            uip_sethostaddr (ip_addr);

            address = socket_info->dst_address.address;

#ifdef TSC_WINDOWS
            uip_ipaddr (ip_addr, (address & 0xff000000) >> 24,
                        (address & 0x00ff0000) >> 16,
                        (address & 0x0000ff00) >> 8, (address & 0x000000ff));
#else
            uip_ipaddr (ip_addr, (address & 0x00ff0000) >> 16,
                        (address & 0xff000000) >> 24, (address & 0x000000ff),
                        (address & 0x0000ff00) >> 8);
#endif

            uip_listen (htons (socket_info->src_address.port));
        }

        return tsc_bool_true;
    }

    return tsc_bool_false;
}
#endif

#ifdef TSC_UIP
tsc_bool
tsc_uip_data (tsc_csm_info * tunnel_info)
{
    uip_current_tunnel = tunnel_info;

    if (timer_expired (&periodic_timer)) {
        int i;
        timer_reset (&periodic_timer);

        for (i = 0; i < UIP_CONNS; i++) {
            tsc_socket_info *socket_info;
            uip_periodic (i);

            socket_info =
                tsc_tunnel_find_socket_uip_conn (tunnel_info, uip_conn,
                                                 tsc_bool_false);

            if (socket_info) {
                if (uip_len > 0) {
                    if (!(socket_info->src_address.port)) {
                        /* local port not assigned yet! */
                        socket_info->src_address.port =
                            ntohs (uip_conn->lport);

                        TSC_DEBUG
                            ("tsc_uip_data: assigning local port %d to socket [%p][%p]",
                             socket_info->src_address.port, socket_info,
                             socket_info->handle);
                    }

                    if (tsc_send_data
                        (socket_info->handle, uip_buf + TSC_ETHER_HDR_LEN,
                         uip_len,
                         &(socket_info->socket_options)) !=
                        tsc_error_code_ok) {
                        TSC_ERROR
                            ("tsc_uip_data: failed to send data [%p][%p]",
                             socket_info, socket_info->handle);
                    }
                }
            }
        }

        return tsc_bool_true;
    }

    return tsc_bool_false;
}
#endif

#ifdef TSC_UIP
clock_time_t
clock_time ()
{
    return tsc_get_clock ();
}
#endif

#ifdef TSC_UIP
tsc_bool
tsc_handle_incoming_tcp (tsc_csm_info * tunnel_info, tsc_csm_msg * msg)
{
    tsc_socket_info *socket_info;
    uip_current_tunnel = tunnel_info;

    uip_len = msg->info.buffer.len;
    memcpy (uip_buf + TSC_ETHER_HDR_LEN, msg->info.buffer.data, uip_len);
    uip_len += TSC_ETHER_HDR_LEN;
    uip_input ();

    if (uip_len > 0) {
        socket_info =
            tsc_tunnel_find_socket_uip_conn (tunnel_info, uip_conn,
                                             tsc_bool_true);

        if (socket_info) {
            if (tsc_send_data
                (tunnel_info, uip_buf + TSC_ETHER_HDR_LEN, uip_len,
                 &(socket_info->socket_options))
                != tsc_error_code_ok) {
                TSC_ERROR
                    ("tsc_handle_incoming_tcp: failed to send data [%p]",
                     tunnel_info);

                return tsc_bool_false;
            }

            if (uip_conn->tcpstateflags == UIP_CLOSED) {
                tsc_set_socket_state (socket_info, tsc_uip_state_closed);
            }
        }
    }

    return tsc_bool_true;
}
#endif

#ifdef TSC_UIP
tsc_bool
tsc_write_tcp_buffer (tsc_tcp_buffer * buffer, uint8_t * data, uint32_t len)
{
    if (buffer->ptr + len >= TSC_MAX_TCP_BUFFER_SIZE) {
        TSC_DEBUG ("tsc_write_tcp_buffer: failed to buffer");

        return tsc_bool_false;
    }

    memcpy (buffer->data + buffer->ptr, data, len);
    buffer->ptr += len;

    return tsc_bool_true;
}
#endif

#ifdef TSC_UIP
uint32_t
tsc_read_tcp_buffer (tsc_tcp_buffer * buffer, uint8_t * data, uint32_t len)
{
    if (!len) {
        /* read all */
        len = buffer->ptr;
    }

    if (buffer->ptr < len) {
        len = buffer->ptr;
    }

    memcpy (data, buffer->data, len);
    buffer->ptr -= len;
    memmove (buffer->data, buffer->data + len, buffer->ptr);

    return len;
}
#endif

#ifdef TSC_UIP
tsc_bool
tsc_set_socket_state (tsc_socket_info * socket_info, tsc_uip_state state)
{
    if (socket_info->state != state) {
        char *state_str;
        socket_info->state = state;

        state_str = (char *) "idle";

        if (state == tsc_uip_state_connect) {
            state_str = (char *) "connect";
        }
        else if (state == tsc_uip_state_connecting) {
            state_str = (char *) "connecting";
        }
        else if (state == tsc_uip_state_connected) {
            state_str = (char *) "connected";
        }
        else if (state == tsc_uip_state_close) {
            state_str = (char *) "close";
        }
        else if (state == tsc_uip_state_closing) {
            state_str = (char *) "closing";
        }
        else if (state == tsc_uip_state_closed) {
            state_str = (char *) "closed";
        }
        else if (state == tsc_uip_state_failure) {
            state_str = (char *) "failure";
        }
        else if (state == tsc_uip_state_listen) {
            state_str = (char *) "listen";
        }
        else if (state == tsc_uip_state_listening) {
            state_str = (char *) "listening";
        }

        if (!socket_info->timeout_timer
            && (socket_info->state == tsc_uip_state_failure
                || socket_info->state == tsc_uip_state_closed)) {
            /* Let's assume TSC_TIMEOUT_GUARD_TIME secs is long enough to
               send out pending RST if connection is bad */
            socket_info->timeout_timer = tsc_time () + TSC_TIMEOUT_GUARD_TIME;
        }

        TSC_DEBUG ("tsc_set_socket_state: socket set to %s state [%p][%p]",
                   state_str, socket_info, socket_info->handle);

        return tsc_bool_true;
    }

    return tsc_bool_false;
}
#endif

int
tsc_getpeername (int s, struct sockaddr *name, socklen_t *namelen)
{
#ifdef TSC_UIP
    tsc_socket_info *info = tsc_get_socket_info (s);

    tsc_set_errno (0);

    if (info) {
        if (info->state == tsc_uip_state_connected) {
            if (info->type == SOCK_STREAM) {
                if (sizeof(struct sockaddr_in) <= *namelen) {
                    ((struct sockaddr_in *)name)->sin_family = AF_INET;
                    ((struct sockaddr_in *)name)->sin_port = htons(info->dst_address.port);
                    ((struct sockaddr_in *)name)->sin_addr.s_addr = htonl(info->dst_address.address);

                    *namelen = sizeof(struct sockaddr_in);

                    return 0;
                } else {
                    TSC_ERROR ("tsc_getpeername: buffer too small [%p]", info);
                }
            }
            else {
                TSC_ERROR ("tsc_getpeername: socket is not TCP [%p]", info);
            }
        }
        else {
            TSC_ERROR ("tsc_getpeername: socket is not connected [%p]", info);
        }
    }
    else {
        TSC_ERROR ("tsc_getpeername: failed to get peer name [%p]", info);
    }

    return -1;
#else
    TSC_ERROR ("tsc_getpeername: not supported");

    return -1;
#endif
}

