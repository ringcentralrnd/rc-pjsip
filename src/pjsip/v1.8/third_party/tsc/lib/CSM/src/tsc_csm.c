/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#define TSC_LOG_SUBSYSTEM TSC_LOG_SUBSYSTEM_STATE_MACHINE

#include "tsc_csm.h"


tsc_error_code
tsc_init ()
{
    TSC_DEBUG ("tsc_init: initializing API");

#ifdef TSC_WINDOWS
    WSADATA wsaData;

    if (WSAStartup (MAKEWORD (2, 2), &wsaData) != 0) {
        TSC_DEBUG ("tsc_init: failed to init winsock");
    }
#endif

    TSC_DEBUG ("tsc_init: done");

    return tsc_error_code_ok;
}

#ifdef TSC_ANDROID
static JavaVM *jvm = 0;
JavaVM *java_vm;


/*
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved) {
    jvm = vm;
    return JNI_VERSION_1_4;
}
*/
#endif

void
csm_post_connect (tsc_csm_info * info, tsc_bool initial)
{
    tsc_bool reconfig;
    tsc_transport transport =
        info->tunnel_params.connection_params[info->connection_index].
        transport;

    if (transport == tsc_transport_tcp ||
#ifdef TSC_FTCP
        transport == tsc_transport_ftcp ||
#endif
        transport == tsc_transport_udp) {
        if (transport == tsc_transport_tcp) {
            TSC_DEBUG ("csm_post_connect: tcp connected [%p]", info);
        }

        info->state_info.state = tsc_state_negotiating;
        TSC_DEBUG ("csm_post_connect: negotiating tunnel [%p]", info);

        if (initial) {
            info->config.sequence = 1;

            info->negotiation_time = tsc_time();
        }

        info->negotiation_retry_time = tsc_time();

        reconfig = tsc_bool_false;

        if (info->reconnect_tries > 0) {
            reconfig = tsc_bool_true;
        }

        if (info->requested_config.tunnel_id.hi
            && info->requested_config.tunnel_id.lo) {

            info->config.tunnel_id.lo = info->requested_config.tunnel_id.lo;
            info->config.tunnel_id.hi = info->requested_config.tunnel_id.hi;

            reconfig = tsc_bool_true;

            TSC_DEBUG ("csm_thread: tunnel %08X%08X is being requested [%p]",
                       info->requested_config.tunnel_id.hi,
                       info->requested_config.tunnel_id.lo, info);
        }

        if (tsc_csm_send_config_request (info, reconfig) == tsc_bool_false) {
            TSC_ERROR ("csm_post_connect: failed to send config request [%p]",
                       info);

            info->state_info.state = tsc_state_fatal_error;
            info->state_info.error = tsc_error_code_cannot_configure;
            info->fatal_error_sleep_time = tsc_time();
        }
    }
    else if (transport == tsc_transport_tls
             || transport == tsc_transport_dtls) {
        TSC_DEBUG ("csm_post_connect: start ssl negotiation [%p]", info);
#ifdef TSC_OPENSSL
        info->state_info.state = tsc_state_ssl_connecting;
#endif
    }
}

void
tsc_csm_notify_tunnel_termination_info(tsc_csm_info* info)
{
    if (info->tunnel_termination_notification_info &&
	(info->tunnel_termination_notification_info->enabled == tsc_bool_true)) {
        tsc_notification_info *notification_info = info->tunnel_termination_notification_info;
        tsc_notification_termination_info_data data;
        memcpy(&(data.config), &(info->config), sizeof(tsc_config));

        if (notification_info->notification) {
            notification_info->notification_data.data = &data;
            notification_info->notification(&(notification_info->notification_data));
        }
    }
}

void *
csm_thread (void *arg)
{
#ifdef TSC_ANDROID
    int status;
    JNIEnv *env;
    tsc_bool is_attached = tsc_bool_false;

	//by martin
	
	jvm=java_vm;
	
    status = (*jvm)->GetEnv(jvm, (void **) &env, JNI_VERSION_1_4);
    TSC_DEBUG("csm_thread: jvm->GetEnv return Status is %d", status);
    if(status < 0) {
        TSC_ERROR("csm_thread: failed to get JNI environment");
        status = (*jvm)->AttachCurrentThread(jvm, &env, NULL);
        if(status < 0) {
            /*It shouldn't happen, if it happens, there probably
 *  *               will be trouble of native thread detach*/
            TSC_ERROR("csm_thread: failed to attach");
        } else {
            TSC_DEBUG("csm_thread: attached successfully");
            is_attached = tsc_bool_true;
        }
    } else {
        status = (*jvm)->AttachCurrentThread(jvm, &env, NULL);
        if(status < 0) {
            TSC_ERROR("csm_thread: failed to attach");
        } else {
            TSC_DEBUG("csm_thread: attached successfully");
            is_attached = tsc_bool_true;
        }
    }
#endif
    tsc_csm_info *info = (tsc_csm_info *) arg;
    uint32_t sleepcounter = 0;
    tsc_bool slow_poll_sockopt = tsc_bool_false;
    tsc_transport transport;
    tsc_security_config *sec_config;
    TSC_DEBUG ("csm_thread: thread started [%p]", info);

#ifdef TSC_PCAP_CAPTURE
    if (info->tunnel_params.pcap_capture.enabled == tsc_bool_true) {
        info->pcap_fd = fopen(info->tunnel_params.pcap_capture.filename, "wb");

        if (info->pcap_fd) {
	    uint32_t magic = 0xa1b2c3d4;
	    uint16_t version_major = 2;
	    uint16_t version_minor = 4;
	    uint32_t snap_len = 0xffff;
	    uint32_t zero = 0;
	    uint32_t one = 1;

	    fwrite(&magic, sizeof(uint8_t), sizeof(uint32_t), info->pcap_fd); /* magic */
	    fwrite(&version_major, sizeof(uint8_t), sizeof(uint16_t), info->pcap_fd); /* major */
	    fwrite(&version_minor, sizeof(uint8_t), sizeof(uint16_t), info->pcap_fd); /* minor */
	    fwrite(&zero, sizeof(uint8_t), sizeof(uint32_t), info->pcap_fd); /* thiszone */
	    fwrite(&zero, sizeof(uint8_t), sizeof(uint32_t), info->pcap_fd); /* sigfigs */
	    fwrite(&snap_len, sizeof(uint8_t), sizeof(uint32_t), info->pcap_fd); /* snaplen */
	    fwrite(&one, sizeof(uint8_t), sizeof(uint32_t), info->pcap_fd); /* linktype */

	    TSC_DEBUG ("csm_thread: pcap capture started %s [%p]", 
		       info->tunnel_params.pcap_capture.filename, info);

        } else {
	    TSC_ERROR ("csm_thread: pcap capture cannot open %s [%p]", 
		       info->tunnel_params.pcap_capture.filename, info);
        }
    } else {
        TSC_DEBUG ("csm_thread: pcap capture disabled [%p]", info);
    }
#endif

    transport =
        info->tunnel_params.connection_params[info->connection_index].
        transport;
    sec_config =
        &(info->tunnel_params.connection_params[info->connection_index].
          sec_config);

    if (transport == tsc_transport_tls || transport == tsc_transport_dtls) {
#ifdef TSC_OPENSSL
        if (tsc_init_openssl () != 1) {
            TSC_ERROR ("csm_thread: OpenSSL initialization problem! [%p]",
                       info);
            return 0L;
        }
        else {
            TSC_DEBUG ("csm_thread: OpenSSL initialization passed! [%p]",
                       info);
        }
#else
        TSC_DEBUG
            ("csm_thread: to use TLS transport you must compile TSCF with OpenSSL [%p]",
             info);
#endif
    }

    time_t tick = tsc_time();

    for (;;) {
        /* every second let's print a tick message to make sure the SM is still alive */
        if (tick != tsc_time()) {
            tick = tsc_time();

            TSC_DEBUG("csm_thread: state-machine tick [%p]", info);
        }

        if (tsc_lock_get (info->in_queue_lock) != tsc_lock_response_error) {
            tsc_csm_msg data;
            if (tsc_queue_read (info->in_queue, &data) ==
                tsc_queue_response_ok) {
                tsc_lock_release (info->in_queue_lock);

                TSC_DEBUG ("csm_thread: csm msg_type %d incoming [%p]",
                           data.msg_type, info);
                if (data.msg_type == tsc_csm_msg_type_finish) {
                    break;
                }
                else if (data.msg_type == tsc_csm_msg_type_service_request) {
                    TSC_DEBUG ("csm_thread: sending client service request [%p]",
                               info);

                    if (tsc_csm_send_client_service_request (info, data.opaque, 
                                                             &(data.info.service_request))
			== tsc_bool_false) {
                        TSC_ERROR
                              ("csm_thread: failed to send client service request [%p]",
                             info);

                        info->state_info.state = tsc_state_fatal_error;
                        info->state_info.error =
                            tsc_error_code_service_failure;
                        info->fatal_error_sleep_time = tsc_time();
                    }
                }
                else if (data.msg_type == tsc_csm_msg_type_data &&
                         (info->state_info.state == tsc_state_established ||
                          info->state_info.state == tsc_state_established_slow_poll)) {
                    char str[TSC_BUFFER_LEN];
                    TSC_HEXDUMP (data.info.buffer.data, data.info.buffer.len, str, 0,
                                 0);
                    TSC_TRACE
                        ("csm_thread [%p]: data out start\n%scsm_thread [%p]: data out end",
                         info, str, info);

                    if (info->in_queue->gap > 1) {
                        /* normally there should be no backup of data here so 
                           we print this out as warning. Never more than 1
                           packet in the buffer, if not, there is a
                           performance issue */
                        if (info->statistics.max_queue_gap <
                            info->in_queue->gap) {
                            TSC_STATS_SET (info, tsc_max_queue_gap,
                                           info->in_queue->gap);
                        }

                        TSC_DEBUG ("csm_thread: output buffer > 1 [%p]",
                                   info);
                    }

                    if (tsc_tunnel_socket_send
                        (info->tunnel_socket, data.info.buffer.data,
                         data.info.buffer.len,
                         tsc_bool_false, 0) == tsc_tunnel_socket_response_ok
                        && info->tunnel_socket->result > 0) {
                        TSC_DEBUG ("csm_thread: data sent (len %d) [%p]",
                                   data.info.buffer.len, info);
                        TSC_STATS_INC (info, tsc_sent_bytes, data.info.buffer.len);

                        uint32_t write_time = tsc_get_clock() - data.timestamp;

                        uint32_t total = info->statistics.out_packet_count * 
			    info->statistics.avg_out_processing + write_time;

                        TSC_STATS_INC (info, tsc_out_packet_count, 1);

                        TSC_STATS_SET (info, tsc_avg_out_processing, total /
                                       info->statistics.out_packet_count);

                        if (write_time > info->statistics.max_out_processing) {
                            TSC_STATS_SET (info, tsc_max_out_processing, write_time);
                        }

                        if (write_time < info->statistics.min_out_processing) {
                            TSC_STATS_SET (info, tsc_min_out_processing, write_time);
                        }


                        info->keepalive_time = tsc_time ();
                        info->poll_time = tsc_time();
                        info->state_info.state = tsc_state_established;
                    }
                    else if
			(info->tunnel_socket->error &&
			 info->tunnel_socket->error != EWOULDBLOCK) {
			TSC_ERROR
			    ("csm_thread: failed to send data (errno %d) [%p]",
			     info->tunnel_socket->error, info);
			
			info->state_info.state = tsc_state_fatal_error;
                        info->state_info.error = tsc_error_code_cannot_send_data;
                        info->fatal_error_sleep_time = tsc_time();
		    }
		}
	    }	
	    else {
		tsc_lock_release (info->in_queue_lock);
	    }
        }

        if (tsc_lock_get (info->data_lock) != tsc_lock_response_error) {
            tsc_http_proxy_config *proxy_config =
                &(info->tunnel_params.
                  connection_params[info->connection_index].proxy_config);
            tsc_ip_port_address *server_address =
                &(info->tunnel_params.
                  connection_params[info->connection_index].server_address);
            tsc_transport transport =
                info->tunnel_params.connection_params[info->connection_index].
                transport;
            tsc_transport_buffering buffering =
                info->tunnel_params.connection_params[info->connection_index].
                buffering;
            uint8_t tos =
                info->tunnel_params.connection_params[info->connection_index].
                tos;

#ifdef TSC_UIP
            /* uip processing */
            tsc_socket_info *socket_info = info->socket_info;
            tsc_bool verify_inner_tcp = tsc_bool_false;

            if (socket_info) {
                tsc_socket_info *socket_info_prev = NULL;

                while (socket_info) {
                    if(socket_info->type == SOCK_STREAM) {
                        verify_inner_tcp = tsc_bool_true;
                        tsc_check_tcp (info, socket_info);
                    }
                    socket_info_prev = socket_info;
                    socket_info = socket_info->next;
                }
            }

            if(verify_inner_tcp) {
                tsc_uip_data (info);
            }
#endif

            /* only "read" data if we are
               connected/established */
            if (info->state_info.state != tsc_state_disconnected
                && info->state_info.state != tsc_state_connecting
                && info->state_info.state != tsc_state_proxy_connecting
#ifdef TSC_OPENSSL
                && info->state_info.state != tsc_state_ssl_connecting
#endif
                && info->state_info.state != tsc_state_fatal_error) {
                if (tsc_tunnel_socket_ready_to_read (info->tunnel_socket) ==
                    tsc_bool_true) {
                    uint8_t input_frame[TSC_MAX_FRAME_SIZE];
                    ssize_t len = 0;
                    if (info->tunnel_socket->ss_transport == tsc_transport_tcp ||
                        info->tunnel_socket->ss_transport == tsc_transport_tls) {
                        len = tsc_tunnel_socket_recv (info->tunnel_socket,
						      input_frame,
						      TSC_MAX_FRAME_SIZE/2,
						      tsc_bool_false);
                    } else {
                        len = tsc_tunnel_socket_recv (info->tunnel_socket,
						      input_frame,
						      TSC_MAX_FRAME_SIZE,
						      tsc_bool_false);
                    }
                    if (len > 0) {
                        TSC_DEBUG ("csm_thread: just read %d bytes [%p]", len,
                                   info);
		
                        if (len + info->input_frame.len < TSC_MAX_FRAME_SIZE) {
                            memcpy (info->input_frame.data +
                                    info->input_frame.len, input_frame, len);
                            info->input_frame.len += len;

                            if (tsc_csm_process_data_in (info) ==
                                tsc_bool_false) {
                                TSC_ERROR
                                    ("csm_thread: failed to process buffer [%p]",
                                     info);

                                info->state_info.state =
                                    tsc_state_fatal_error;
                                info->state_info.error =
                                    tsc_error_code_cannot_recv_data;
                                info->fatal_error_sleep_time = 
                                    tsc_time();
                            }
                        }
                        else {
			    char str[TSC_BUFFER_LEN];
                            TSC_ERROR ("csm_thread: buffer overload! [%p]",
                                       info);
			    
			    tsc_hexdump (input_frame, len, str, 0,
					 0);
			    TSC_ERROR("csm_thread [%p]: input_frame buffer\n%s", info, str);
			    tsc_hexdump (info->input_frame.data, info->input_frame.len, str, 0,
					 0);
			    TSC_ERROR("csm_thread [%p]: info->input_frame buffer\n%s", info, str);
			    
			    memset(&(info->input_frame.data), 0, TSC_MAX_FRAME_SIZE);
			    info->input_frame.len = 0;
                            info->state_info.state = tsc_state_fatal_error;
                            info->state_info.error =
                                tsc_error_code_cannot_recv_data;
                            info->fatal_error_sleep_time = tsc_time();
                        }
                    }
                    else if (len < 0) {
                        if (info->tunnel_socket->ss_transport == tsc_transport_tls ||
			    info->tunnel_socket->ss_transport == tsc_transport_dtls) {
                            TSC_ERROR
                                ("csm_thread: ssl error, failed to read data (errno %d) [%p]",
                                 info->tunnel_socket->error, info);

                            info->state_info.state = tsc_state_fatal_error;
                            info->state_info.error =
                                tsc_error_code_cannot_recv_data;
                            info->fatal_error_sleep_time = tsc_time();
                        }
                        if (info->tunnel_socket->error &&
                            info->tunnel_socket->error != EWOULDBLOCK) {
                            TSC_ERROR
                                ("csm_thread: failed to read data (errno %d) [%p]",
                                 info->tunnel_socket->error, info);

                            info->state_info.state = tsc_state_fatal_error;
                            info->state_info.error =
                                tsc_error_code_cannot_recv_data;
                            info->fatal_error_sleep_time = tsc_time();
                        }
                    }
                }
            }
            if (info->state_info.state == tsc_state_fatal_error &&
                (tsc_time() >= info->fatal_error_sleep_time + TSC_FATAL_ERROR_TIMEOUT)) {
                TSC_ERROR
                    ("csm_thread: error %d detected, attempting to reconnect [%p]",
                     info->state_info.error, info);
		tsc_socket_info *socket_info;
#ifdef TSC_ODD
		socket_info= info->socket_info;

                TSC_ERROR
                    ("csm_thread: has socket info %p [%p]",
                     socket_info, info);

                while (socket_info) {
		    
		    if (socket_info->odd_tunnel) {
			    TSC_DEBUG("csm_thread: removing On Demand DTLS tunnel %p [%p][%p]",
				      socket_info->odd_tunnel, socket_info, info);
			    
			tsc_delete_tunnel(socket_info->odd_tunnel);
			
			socket_info->odd_tunnel = NULL;
		    }
		    
		}
#endif
#ifdef TSC_REDUNDANCY
		socket_info = info->socket_info;

                TSC_ERROR
                    ("csm_thread: has socket info %p [%p]",
                     socket_info, info);

                while (socket_info) {
                    uint8_t i;

                    for (i = 0; i < TSC_REDUNDANCY_MAX_STREAMS; i++) {
                        if (socket_info->redundancy_tunnel_list[i]) {
                            TSC_DEBUG("csm_thread: removing secondary tunnel %p [%p][%p]",
				      socket_info->redundancy_tunnel_list[i], socket_info, info);

                            tsc_delete_tunnel(socket_info->redundancy_tunnel_list[i]);

                            socket_info->redundancy_tunnel_list[i] = NULL;
                        }
                    }

                    memset(&(socket_info->redundancy_info), 0, sizeof(tsc_redundancy_info));
                    
                    socket_info = socket_info->next;
                }
#endif
	
		
                info->state_info.state = tsc_state_disconnected;

                if (tsc_transaction_remove(info, NULL) == tsc_error_code_error) {
                    TSC_ERROR ("csm_thread: failed to remove all pending transactions [%p]", info);
                } else {
                    TSC_DEBUG ("csm_thread: all pending transactions removed [%p]", info);
                }

                if (!info->reconnect_tries) {
                    tsc_csm_notify_tunnel_termination_info(info);
                }

                TSC_STATS_SET (info, tsc_reconn_attempts, info->reconnect_tries++);

                if (info->tunnel_socket) {
                    if (tsc_tunnel_socket_close (info->tunnel_socket) ==
                        tsc_tunnel_socket_response_error) {
                        TSC_ERROR ("csm_thread: failed to close socket [%p]",
                                   info);
                    }
                    else {
                        TSC_DEBUG ("csm_thread: socket closed [%p]", info);

                        if (tsc_tunnel_socket_delete (info->tunnel_socket) ==
                            tsc_tunnel_socket_response_error) {
                            TSC_ERROR
                                ("csm_thread: failed to delete socket [%p]",
                                 info);
                        }
                        else {
                            TSC_DEBUG ("csm_thread: socket deleted [%p]",
                                       info);
                        }
                    }

                    info->tunnel_socket = NULL;
                }

                if (info->config.tunnel_id.lo || info->config.tunnel_id.hi) {
                    info->requested_config.tunnel_id.hi =
                        info->config.tunnel_id.hi;
                    info->requested_config.tunnel_id.lo =
                        info->config.tunnel_id.lo;

                    /* acording to specs, when tunnel goes out of service
                       after the tunnel has been established we try to
                       connect starting by the very first server in the list */
                    info->connection_index = 0;

                    TSC_DEBUG
                        ("csm_thread: tunnel %08X%08X will be requested [%p]",
                         info->requested_config.tunnel_id.hi,
                         info->requested_config.tunnel_id.lo, info);
                }
                else {
                    /* if we are here it means we fail to connect/negotiate
                       the tunnel now we try the next server in the list */

                    info->connection_index++;

                    if (info->connection_index >=
                        info->tunnel_params.max_connections) {
                        info->connection_index = 0;
                    }

                    TSC_DEBUG
                        ("csm_thread: trying next server in list (index %d) [%p]",
                         info->connection_index, info);
                }
            }
            else if (info->state_info.state == tsc_state_disconnected) {
                TSC_DEBUG ("csm_thread: tunnel is disconnected [%p]", info);
#ifdef TSC_OPENSSL
                if(transport == tsc_transport_tls || transport == tsc_transport_dtls) {
                    if (tsc_init_ssl_context (sec_config, transport) !=
                        tsc_ssl_init_normal) {
                        TSC_ERROR ("csm_thread: tsc_init_ssl_context() failed! [%p]",
				   info);
                        return 0L;
                    }
                }
#endif
		tsc_tunnel_connection_params conn_params;
		conn_params.transport = transport;
		conn_params.choose_device = info->tunnel_params.connection_params[info->connection_index].choose_device;
#ifdef TSC_IOS
		conn_params.background_mode = info->tunnel_params.connection_params[info->connection_index].background_mode;	
#endif
		strcpy(conn_params.devname,  info->tunnel_params.connection_params[info->connection_index].devname);
                info->tunnel_socket = tsc_tunnel_socket_create (conn_params);

                if (info->tunnel_socket) {
                    info->state_info.state = tsc_state_connecting;

                    if (tos > 0) {
                        if (tsc_tunnel_socket_set_tos
                            (info->tunnel_socket,
                             tos) == tsc_tunnel_socket_response_ok) {
                            TSC_DEBUG
                                ("csm_thread: tunnel socket set as tos %X [%p]",
                                 tos, info);
                        }
                        else {
                            TSC_ERROR
                                ("csm_thread: failed to set tunnel socket as tos %X [%p]",
                                 tos, info);
                            info->state_info.state = tsc_state_fatal_error;
                            info->state_info.error =
                                tsc_error_code_cannot_connect;
                            info->fatal_error_sleep_time = tsc_time();
                        }
                    }
                    else {
                        TSC_DEBUG ("csm_thread: tos was not requested [%p]",
                                   info);
                    }

                    if (buffering == tsc_transport_buffering_disabled) {
                        if (tsc_tunnel_socket_set_no_delay
                            (info->tunnel_socket,
                             tsc_bool_true) ==
                            tsc_tunnel_socket_response_ok) {
                            TSC_DEBUG
                                ("csm_thread: tunnel socket set as no delay [%p]",
                                 info);
                        }
                        else {
                            TSC_ERROR
                                ("csm_thread: failed to set tunnel socket as no delay [%p]",
                                 info);
                            info->state_info.state = tsc_state_fatal_error;
                            info->state_info.error =
                                tsc_error_code_cannot_connect;
                            info->fatal_error_sleep_time = tsc_time();
                        }
                    }
                    else {
                        TSC_DEBUG
                            ("csm_thread: no delay was not requested [%p]",
                             info);
                    }

                    if (tsc_tunnel_socket_set_non_blocking
                        (info->tunnel_socket) ==
                        tsc_tunnel_socket_response_ok) {
                        tsc_ip_port_address remote_address;
                        char addr_str[TSC_ADDR_STR_LEN];
                        if (proxy_config->proxy_enabled == tsc_bool_true) {
                            TSC_DEBUG
                                ("csm_thread: connection through http proxy [%p]",
                                 info);
                            memcpy (&remote_address,
                                    &(proxy_config->proxy_address),
                                    sizeof (tsc_ip_port_address));
                        }
                        else {
                            TSC_DEBUG
                                ("csm_thread: no http proxy has been configured [%p]",
                                 info);
                            memcpy (&remote_address, server_address,
                                    sizeof (tsc_ip_port_address));
                        }

                        tsc_ip_port_address_to_str (&remote_address, addr_str,
                                                    TSC_ADDR_STR_LEN);
                        TSC_DEBUG
                            ("csm_thread: attempting to connect %s [%p]",
                             addr_str, info);

                        if (tsc_tunnel_socket_connect
                            (info->tunnel_socket,
                             &remote_address) ==
                            tsc_tunnel_socket_response_ok) {
                            if (info->tunnel_socket->error == EINPROGRESS
                                || info->tunnel_socket->error ==
                                EWOULDBLOCK) {
                                info->connect_time = tsc_time ();
                                info->poll_time = info->connect_time;
                            }
                            else {
                                TSC_ERROR
                                    ("csm_thread: bad errno %d for connect [%p]",
                                     info->tunnel_socket->error, info);

                                info->state_info.state =
                                    tsc_state_fatal_error;
                                info->state_info.error =
                                    tsc_error_code_cannot_connect;
                                info->fatal_error_sleep_time = tsc_time();
                            }
                        }
                        else {
                            TSC_ERROR
                                ("csm_thread: failed to issue socket connect [%p]",
                                 info);

                            info->state_info.state = tsc_state_fatal_error;
                            info->state_info.error =
                                tsc_error_code_cannot_connect;
                            info->fatal_error_sleep_time = tsc_time();
                        }
                    }
                    else {
                        TSC_ERROR
                            ("csm_thread: failed to set socket non-blocking [%p]",
                             info);

                        info->state_info.state = tsc_state_fatal_error;
                        info->state_info.error =
                            tsc_error_code_cannot_connect;
                        info->fatal_error_sleep_time = tsc_time();
                    }
                }
                else {
                    TSC_ERROR ("csm_thread: failed to create socket [%p]",
                               info);

                    info->state_info.state = tsc_state_fatal_error;
                    info->state_info.error = tsc_error_code_cannot_connect;
                    info->fatal_error_sleep_time = tsc_time();
                }
            }
            else if (info->state_info.state == tsc_state_connecting) {
                if (tsc_tunnel_socket_connected (info->tunnel_socket)
                    == tsc_bool_true) {
                    if (proxy_config->proxy_enabled == tsc_bool_true) {
                        tsc_bool authentication;
                        TSC_DEBUG ("csm_thread: proxy connected [%p]", info);

                        info->proxy_time = tsc_time ();
                        info->poll_time = info->proxy_time;
                        info->state_info.state = tsc_state_proxy_connecting;
                        memset (&(info->proxy_buffer), 0,
                                sizeof (http_proxy_buffer));

                        authentication = tsc_bool_false;

                        if (info->proxy_tries > 0) {
                            authentication = tsc_bool_true;
                        }

                        if (tsc_csm_send_http_connect (info, authentication)
                            == tsc_bool_true) {
                            TSC_DEBUG ("csm_thread: http connect sent [%p]",
                                       info);
                        }
                        else {
                            TSC_ERROR
                                ("csm_thread: failed to send http connect request [%p]",
                                 info);

                            info->state_info.state = tsc_state_fatal_error;
                            info->state_info.error =
                                tsc_error_code_cannot_connect;
                            info->fatal_error_sleep_time = tsc_time();
                        }
                    }
                    else {
                        csm_post_connect (info, tsc_bool_true);
                    }
                }
                else {
                    /* not connected yet */
                    if (tsc_time () >
                        (info->connect_time + TSC_CONNECT_TIMEOUT)) {
                        TSC_ERROR
                            ("csm_thread: failed to connect, timeout [%p]",
                             info);

                        info->state_info.state = tsc_state_fatal_error;
                        info->state_info.error =
                            tsc_error_code_cannot_connect;
                        info->fatal_error_sleep_time = tsc_time();
                    }
                }
            }
            else if (info->state_info.state == tsc_state_proxy_connecting) {
                if (tsc_tunnel_socket_ready_to_read (info->tunnel_socket) ==
                    tsc_bool_true) {
                    uint8_t input_frame[TSC_MAX_FRAME_SIZE];
                    ssize_t len = tsc_tunnel_socket_recv (info->tunnel_socket,
                                                          input_frame,
                                                          TSC_MAX_FRAME_SIZE /
                                                          2, tsc_bool_true);

                    if (len > 0) {
                        TSC_DEBUG ("csm_thread: just read %d bytes [%p]", len,
                                   info);

                        if (info->proxy_buffer.len + len <
                            HTTP_PROXY_MAX_MSG_SIZE) {
                            tsc_csm_proxy_response response;
                            memcpy (info->proxy_buffer.data +
                                    info->proxy_buffer.len, input_frame, len);
                            info->proxy_buffer.len += len;

                            response = tsc_csm_process_proxy_response (info);

                            if (response == tsc_csm_proxy_response_ok) {
                                csm_post_connect (info, tsc_bool_true);
                            }
                            else if (response ==
                                     tsc_csm_proxy_response_authenticate) {
                                if (!info->proxy_tries) {
                                    if (info->tunnel_socket) {
                                        if (tsc_tunnel_socket_close
                                            (info->tunnel_socket)
                                            ==
                                            tsc_tunnel_socket_response_error)
                                        {
                                            TSC_ERROR
                                                ("csm_thread: failed to close socket [%p]",
                                                 info);
                                        }
                                        else {
                                            TSC_DEBUG
                                                ("csm_thread: socket closed [%p]",
                                                 info);

                                            if (tsc_tunnel_socket_delete
                                                (info->tunnel_socket)
                                                ==
                                                tsc_tunnel_socket_response_error)
                                            {
                                                TSC_ERROR
                                                    ("csm_thread: failed to delete socket [%p]",
                                                     info);
                                            }
                                            else {
                                                TSC_DEBUG
                                                    ("csm_thread: socket deleted [%p]",
                                                     info);
                                            }
                                        }
                                        info->tunnel_socket = NULL;
                                    }
                                    info->state_info.state =
                                        tsc_state_disconnected;

                                    info->proxy_tries++;
                                }
                                else {
                                    TSC_ERROR
                                        ("csm_thread: failed to authenticate [%p]",
                                         info);

                                    info->state_info.state =
                                        tsc_state_fatal_error;
                                    info->state_info.error =
                                        tsc_error_code_cannot_authenticate;
                                    info->fatal_error_sleep_time = tsc_time();
                                }
                            }
                            else if (response == tsc_csm_proxy_response_error) {
                                TSC_ERROR
                                    ("csm_thread: failed to buffer proxy response [%p]",
                                     info);

                                info->state_info.state =
                                    tsc_state_fatal_error;
                                info->state_info.error =
                                    tsc_error_code_cannot_authenticate;
                                info->fatal_error_sleep_time = tsc_time();
                            }
                        }
                        else {
                            TSC_ERROR
                                ("csm_thread: failed to buffer proxy response [%p]",
                                 info);

                            info->state_info.state = tsc_state_fatal_error;
                            info->state_info.error =
                                tsc_error_code_cannot_connect;
                            info->fatal_error_sleep_time = tsc_time();
                        }
                    }
                }
                else {
                    /* not connected to proxy yet */
                    if (tsc_time () > (info->proxy_time + TSC_PROXY_TIMEOUT)) {
                        TSC_ERROR
                            ("csm_thread: failed to access proxy, timeout [%p]",
                             info);

                        info->state_info.state = tsc_state_fatal_error;
                        info->state_info.error =
                            tsc_error_code_cannot_connect;
                        info->fatal_error_sleep_time = tsc_time();
                    }
                }
#ifdef TSC_OPENSSL
            }
            else if (info->state_info.state == tsc_state_ssl_connecting) {
                int err;
                int err_ret;

                err = SSL_connect (info->tunnel_socket->ss_ssl);
                if (err == 1) {
                    tsc_bool reconfig;
                    TSC_DEBUG ("csm_thread: ssl connected [%p]", info);
                    info->state_info.state = tsc_state_negotiating;
                    TSC_DEBUG ("csm_thread: negotiating tunnel [%p]", info);
                    info->negotiation_time = tsc_time ();
                    info->negotiation_retry_time = tsc_time ();
                    info->poll_time = info->negotiation_retry_time;
                    info->config.sequence = 1;

                    reconfig = tsc_bool_false;

                    if (info->reconnect_tries > 0) {
                        reconfig = tsc_bool_true;
                    }

                    if (info->requested_config.tunnel_id.hi
                        && info->requested_config.tunnel_id.lo) {

                        info->config.tunnel_id.lo =
                            info->requested_config.tunnel_id.lo;
                        info->config.tunnel_id.hi =
                            info->requested_config.tunnel_id.hi;

                        reconfig = tsc_bool_true;

                        TSC_DEBUG
                            ("csm_thread: tunnel %08X%08X is being requested [%p]",
                             info->requested_config.tunnel_id.hi,
                             info->requested_config.tunnel_id.lo, info);
                    }

                    if (tsc_csm_send_config_request (info, reconfig) ==
                        tsc_bool_false) {
                        TSC_ERROR
                            ("csm_thread: failed to send config request [%p]",
                             info);

                        info->state_info.state = tsc_state_fatal_error;
                        info->state_info.error =
                            tsc_error_code_cannot_configure;
                        info->fatal_error_sleep_time = tsc_time();
                    }
                }
                else if (err < 0) {
                    err_ret =
                        SSL_get_error (info->tunnel_socket->ss_ssl, err);
                    switch (err_ret) {
		      case SSL_ERROR_WANT_WRITE:
		      case SSL_ERROR_WANT_READ:
                        break;
		      default:
                        TSC_ERROR ("csm_thread: Error SSL connect: %s [%p]\n",
                                   tsc_ssl_error_string2 (err_ret), info);
                        info->state_info.state = tsc_state_fatal_error;
                        info->state_info.error =
                            tsc_error_code_cannot_connect;
                        info->fatal_error_sleep_time = tsc_time();
                        break;
                    }
                }
                else if (err == 0) {
                    if (transport != tsc_transport_dtls) {
                        info->state_info.state = tsc_state_fatal_error;
                        info->state_info.error =
                            tsc_error_code_cannot_connect;
                        info->fatal_error_sleep_time = tsc_time();
                    }
                }
#endif
            }
            else if (info->state_info.state == tsc_state_negotiating) {
                tsc_csm_msg msg;

                if (tsc_queue_read (info->cm_queue, &msg) ==
                    tsc_queue_response_ok) {
                    if (tsc_csm_process_config_response
                        (info, msg.info.buffer.data,
                         msg.info.buffer.len) == tsc_bool_true) {
                        char tmp[TSC_ADDR_STR_LEN];
                        info->state_info.state = tsc_state_established;
                        info->reconnect_tries = 0;
                        info->keepalive_time = tsc_time ();
                        info->poll_time = info->keepalive_time;
                        TSC_DEBUG
                            ("csm_thread: tunnel %08X%08X established [%p]",
                             info->config.tunnel_id.hi,
                             info->config.tunnel_id.lo, info);


                        if (tsc_ip_address_to_str
                            (&(info->config.internal_address), tmp,
                             TSC_ADDR_STR_LEN) == tsc_bool_true) {
                            TSC_DEBUG
                                ("csm_thread: tunnel internal address %s [%p]",
                                 tmp, info);
                        }

                        if (tsc_ip_mask_to_str
                            (&(info->config.mask), tmp,
                             TSC_ADDR_STR_LEN) == tsc_bool_true) {
                            TSC_DEBUG
                                ("csm_thread: tunnel internal mask %s [%p]",
                                 tmp, info);
                        }

                        if (tsc_ip_port_address_to_str
                            (&(info->config.sip_server), tmp,
                             TSC_ADDR_STR_LEN) == tsc_bool_true) {
                            TSC_DEBUG
                                ("csm_thread: tunnel sip server %s [%p]", tmp,
                                 info);
                        }

                        TSC_DEBUG ("csm_thread: keepalive interval %d [%p]",
                                   info->config.keepalive_interval, info);

                    }
                    else {
                        TSC_ERROR
                            ("csm_thread: failed to process response [%p]",
                             info);

                        info->state_info.state = tsc_state_fatal_error;
                        info->state_info.error =
                            tsc_error_code_cannot_configure;
                        info->fatal_error_sleep_time = tsc_time();
                    }
                }
                else {
                    /* not negotiated yet */
                    if (tsc_time () >
                        (info->negotiation_time + TSC_NEGOTIATION_TIMEOUT)) {
                        TSC_ERROR
                            ("csm_thread: failed to negotiate, timeout [%p]",
                             info);

                        info->state_info.state = tsc_state_fatal_error;
                        info->state_info.error =
                            tsc_error_code_cannot_configure;
                        info->fatal_error_sleep_time = tsc_time();
                    }

                    if (transport == tsc_transport_udp) {
                        if (tsc_time () >
                            (info->negotiation_retry_time +
                             TSC_NEGOTIATION_RETRY_TIMEOUT)) {
                            csm_post_connect (info, tsc_bool_false);

                            TSC_DEBUG
                                ("csm_thread: retrying negotiation [%p]",
                                 info);
                        }
                    }

                    if (transport == tsc_transport_dtls) {
                        if (tsc_time () >
                            (info->negotiation_retry_time +
                             TSC_DTLS_NEGOTIATION_RETRY_TIMEOUT)) {
                            csm_post_connect (info, tsc_bool_false);

                            TSC_DEBUG
                                ("csm_thread: dtls retrying negotiation [%p]",
                                 info);
                        }
                    }
                }
            }
            else if (info->state_info.state == tsc_state_established
		     || info->state_info.state == tsc_state_established_slow_poll) {
                /* let's check transactions for timeouts */
                tsc_transaction_check_timeout(info);

                /* let's check for network config changes every second 
                   the idea is to check whether the IPs of the interfaces
                   have changed and then reestablish the tunnel accordingly */
                if (info->network_config_check < tsc_time()) {
                    info->network_config_check = tsc_time() + TSC_NETWORK_CONFIG_CHANGE_TIMEOUT;

                    char str[TSC_MAX_STR_LEN];
                    strcpy(str, "");

                    uint8_t intfs;

                    if (tsc_ip_get_if_count(&intfs) == tsc_error_code_ok) {
                        uint8_t i = 0;

                        for (i = 0; i < intfs; i++) {
                            char tmp[TSC_MAX_STR_LEN];

                            if (tsc_ip_get_if_addr(i, tmp) == tsc_error_code_ok) {
                                strcat(str, tmp);
                                strcat(str, " ");
                            }
                        }

                        if (strlen(info->network_config_str) && (info->network_config_count == intfs)) {
                            if (strcmp(info->network_config_str, str)) {
                                TSC_DEBUG
                                    ("csm_thread: network configuration changes detected [%p]",
                                     info);

                                tsc_transport transport =
                                    info->tunnel_params.connection_params[info->connection_index].
                                    transport;

				strcpy(info->network_config_str, "");

                                if (transport == tsc_transport_udp) {
                                    TSC_ERROR
                                        ("csm_thread: tunnel needs to be reestablished [%p]",
                                         info);                             

                                    info->state_info.state = tsc_state_fatal_error;
                                    info->state_info.error = tsc_error_code_error;
                                    info->fatal_error_sleep_time = tsc_time();
                                }
                            }
                        }

                        strcpy(info->network_config_str, str);
                        info->network_config_count = intfs;
                    }
                }

                tsc_socket_info *socket_info = info->socket_info;

                slow_poll_sockopt = tsc_bool_true;
                while (socket_info) {
                    if (socket_info->socket_options.slow_idle_poll == tsc_bool_false){
                        slow_poll_sockopt = tsc_bool_false;
                        break;
                    }

                    socket_info = socket_info->next;
                }

                tsc_csm_msg msg;

                if (tsc_queue_read (info->cm_queue, &msg) ==
                    tsc_queue_response_ok) {
                    if (tsc_transaction_process_response
                        (info, msg.info.buffer.data,
                         msg.info.buffer.len) == tsc_error_code_ok) {
                        TSC_DEBUG ("csm_thread: response processed [%p]",
                                   info);
                    }
                    else {
                        TSC_ERROR
                            ("csm_thread: failed to process response [%p]",
                             info);

                        info->state_info.state = tsc_state_fatal_error;
                        info->state_info.error = tsc_error_code_error;
                        info->fatal_error_sleep_time = tsc_time();
                    }
                }

                /* let's check keepalive */
                if (info->config.keepalive_interval
                    && (tsc_time () >
                        (info->keepalive_time +
                         info->config.keepalive_interval))) {
                    TSC_DEBUG ("csm_thread: renewing tunnel lease [%p]",
                               info);

                    info->keepalive_time = tsc_time();

                    if (tsc_csm_send_keepalive (info) == tsc_bool_false) {
                        TSC_ERROR
                            ("csm_thread: failed to send keepalive request [%p]",
                             info);

                        info->state_info.state = tsc_state_fatal_error;
                        info->state_info.error =
                            tsc_error_code_keepalive_failure;
                        info->fatal_error_sleep_time = tsc_time();
                    }
                }

		if(tsc_time () > 
		   (info->stats_time + TSC_STATS_DUMP_INTERVAL)) {

		    tsc_dump_stats(info);
		    info->stats_time = tsc_time ();
		}		    
            }

            tsc_lock_release (info->data_lock);
        }
        if(info->state_info.state == tsc_state_established && (tsc_time() > info->poll_time + 3)
           && slow_poll_sockopt) {
            info->state_info.state = tsc_state_established_slow_poll;
        }
        if(info->state_info.state == tsc_state_established_slow_poll) {
#if defined(TSC_ANDROID) || defined(TSC_IOS)

            if (sleepcounter++ > TSC_ANDROID_SLEEP_COUNT) {
                sleepcounter = 0;
                tsc_sleep (1);
            }
#else
            if (sleepcounter++ > TSC_SLEEP_COUNT) {
                sleepcounter = 0;
                tsc_sleep (1000);
            }
#endif
        } else {
#if defined(TSC_ANDROID) || defined(TSC_IOS)

            if (sleepcounter++ > TSC_ANDROID_SLEEP_COUNT) {
                sleepcounter = 0;
                tsc_sleep (1);
            }
#else
            if (sleepcounter++ > TSC_SLEEP_COUNT) {
                sleepcounter = 0;
                tsc_sleep (1);
            }
#endif
        }
    }

    if (tsc_tunnel_socket_close (info->tunnel_socket) ==
        tsc_tunnel_socket_response_error) {
        TSC_ERROR ("csm_thread: failed to close socket [%p]", info);
    }

    if (tsc_tunnel_socket_delete(info->tunnel_socket) ==
        tsc_tunnel_socket_response_error) {
	TSC_ERROR ("csm_thread: failed to delete socket [%p]", info); 
    }
#ifdef TSC_OPENSSL
    tsc_ctx_free (transport);
#endif

#ifdef TSC_PCAP_CAPTURE
    if (info->pcap_fd) {
        TSC_DEBUG ("csm_thread: pcap capture finished %s [%p]", 
                   info->tunnel_params.pcap_capture.filename, info);

        fclose(info->pcap_fd);
    }
#endif

    TSC_DEBUG ("csm_thread: thread terminated [%p]", info);

#ifdef TSC_ANDROID
    if(is_attached) {
        (*jvm)->DetachCurrentThread(jvm);
        TSC_DEBUG("csm_thread: thread detached");
    }
#endif
    return 0L;
}

tsc_handle
tsc_new_tunnel (tsc_tunnel_params * tunnel_params,
                tsc_requested_config * requested_config)
{
    tsc_csm_info *handle;
    TSC_LOG_VERSION();
    TSC_DEBUG ("tsc_new_tunnel: starting csm");
    if (!tunnel_params) {
        TSC_ERROR ("tsc_new_tunnel: tunnel params not set");

        return NULL;
    }

    if (tunnel_params->max_connections > TSC_MAX_CONNECTION_PARAMS) {
        TSC_ERROR ("tsc_new_tunnel: max connections out of bounds");

        return NULL;
    }

    handle = (tsc_csm_info *) malloc (sizeof (tsc_csm_info));

    if (handle) {
        memset (handle, 0, sizeof (tsc_csm_info));

        memcpy (&(handle->tunnel_params), tunnel_params,
                sizeof (tsc_tunnel_params));

        if (requested_config) {
            memcpy (&(handle->requested_config), requested_config,
                    sizeof (tsc_requested_config));
        }

        handle->state_info.state = tsc_state_disconnected;

        handle->connection_index = 0;

        handle->cm_queue =
            tsc_queue_new (TSC_CSM_QUEUE_SIZE, sizeof (tsc_csm_msg));

        if (handle->cm_queue) {
            handle->in_queue =
                tsc_queue_new (TSC_CSM_QUEUE_SIZE, sizeof (tsc_csm_msg));

            if (handle->in_queue) {
                handle->out_queue =
                    tsc_queue_new (TSC_CSM_QUEUE_SIZE, sizeof (tsc_csm_msg));

                if (handle->out_queue) {
                    handle->in_queue_lock = tsc_lock_new ();

                    if (handle->in_queue_lock) {
                        handle->out_queue_lock = tsc_lock_new ();

                        if (handle->out_queue_lock) {
                            handle->data_lock = tsc_lock_new ();

                            if (handle->data_lock) {
                                handle->csm =
                                    tsc_thread_new ((void *) csm_thread,
                                                    handle);

                                if (handle->csm) {
                                    TSC_DEBUG
                                        ("tsc_new_tunnel: csm initialized [%p]",
                                         handle);

                                    return (tsc_handle) handle;
                                }
                                else {
                                    TSC_ERROR
                                        ("tsc_new_tunnel: failed to allocate csm thread");
                                    tsc_lock_delete (handle->data_lock);
                                    tsc_lock_delete (handle->in_queue_lock);
                                    tsc_queue_delete (handle->in_queue);
                                    tsc_queue_delete (handle->cm_queue);
                                    tsc_lock_delete (handle->out_queue_lock);
                                    tsc_queue_delete (handle->out_queue);
                                    free ((void *) handle);
                                }
                            }
                            else {
                                TSC_ERROR
                                    ("tsc_new_tunnel: failed to allocate csm data_lock");
                                tsc_lock_delete (handle->in_queue_lock);
                                tsc_queue_delete (handle->in_queue);
                                tsc_queue_delete (handle->cm_queue);
                                tsc_lock_delete (handle->out_queue_lock);
                                tsc_queue_delete (handle->out_queue);
                                free ((void *) handle);
                            }
                        }
                        else {
                            TSC_ERROR
                                ("tsc_new_tunnel: failed to allocate csm out_queue_lock");
                            tsc_queue_delete (handle->out_queue);
                            tsc_queue_delete (handle->in_queue);
                            tsc_queue_delete (handle->cm_queue);
                            tsc_lock_delete (handle->in_queue_lock);
                            free ((void *) handle);
                        }
                    }
                    else {
                        TSC_ERROR
                            ("tsc_new_tunnel: failed to allocate csm in_queue_lock");
                        tsc_queue_delete (handle->out_queue);
                        tsc_queue_delete (handle->in_queue);
                        tsc_queue_delete (handle->cm_queue);
                        free ((void *) handle);
                    }
                }
                else {
                    TSC_ERROR
                        ("tsc_new_tunnel: failed to allocate csm out_queue");
                    tsc_queue_delete (handle->in_queue);
                    tsc_queue_delete (handle->cm_queue);
                    free ((void *) handle);
                }
            }
            else {
                TSC_ERROR ("tsc_new_tunnel: failed to allocate csm in_queue");
                tsc_queue_delete (handle->cm_queue);
                free ((void *) handle);
            }
        }
        else {
            TSC_ERROR ("tsc_new_tunnel: failed to allocate csm cm_queue");
            free ((void *) handle);
        }
    }
    else {
        TSC_ERROR ("tsc_new_tunnel: failed to allocate csm");
    }

    return NULL;
}

tsc_bool
tsc_csm_finish_tunnel (tsc_csm_info *info)
{
    if (info) {
        tsc_csm_msg data;

        /* removing all sockets */
        tsc_socket_info *socket_info = info->socket_info;

        /*while (socket_info) {
            tsc_close(socket_info->fd);
            if(socket_info->type == SOCK_STREAM) {
                socket_info = socket_info->next;
            } else{
                socket_info = info->socket_info;
            }
        }*/

        data.msg_type = tsc_csm_msg_type_finish;
        data.timestamp = tsc_get_clock();

        if (tsc_csm_write_in_msg (info, &data) == tsc_error_code_ok) {
            tsc_csm_notify_tunnel_termination_info(info);

            tsc_clear_sockets (info);

            tsc_thread_finish (info->csm);
            tsc_thread_delete (info->csm);

            tsc_lock_delete (info->data_lock);
            tsc_lock_delete (info->in_queue_lock);
            tsc_queue_delete (info->in_queue);
            tsc_lock_delete (info->out_queue_lock);
            tsc_queue_delete (info->out_queue);
            tsc_queue_delete (info->cm_queue);

            free ((void *) info);

            TSC_DEBUG ("tsc_csm_finish_tunnel: csm finished [%p]", info);

            return tsc_bool_true;
        }
    }

    TSC_ERROR ("tsc_csm_finish_tunnel: failed to finish csm [%p]", info);

    return tsc_bool_false;
}

tsc_error_code
tsc_delete_tunnel (tsc_handle handle)
{
    tsc_csm_info *info = (tsc_csm_info *) handle;
    
    if (info) {
        tsc_dump_stats (handle);
        TSC_DEBUG ("tsc_delete_tunnel: debug info, tunnel state is %d[%p]\n",
                    info->state_info.state, handle);    	
	if(info->state_info.state == tsc_state_established 
	   || info->state_info.state == tsc_state_established_slow_poll) {
	    TSC_DEBUG ("tsc_delete_tunnel: get in, debug info, tunnel state is %d[%p]\n",
                    info->state_info.state, handle);
	    tsc_lock *lock = tsc_lock_new();
	    
	    if (tsc_csm_release_tunnel (info, lock) == tsc_bool_true) {
	        TSC_DEBUG ("tsc_delete_tunnel: after tsc_csm_release_tunnel[%p]\n", handle);	
		for (;;) {
		    if (tsc_lock_get(lock) == tsc_lock_response_ok) {
			if (lock->opaque) {
			    tsc_lock_release(lock);
			    
			    break;
			}
			
			tsc_lock_release(lock);
		    }
		    
		    tsc_sleep(100);
		}
	    }   
	    
	    tsc_lock_delete(lock);
	}
        TSC_DEBUG("tsc_delete_tunnel: before tsc_csm_finish_tunnel [%p]\n", handle);	
	tsc_csm_finish_tunnel(info);
	
	return tsc_error_code_ok;
    }


    TSC_ERROR ("tsc_delete_tunnel: failed to delete csm [%p]", handle);
    
    return tsc_error_code_error;
}
    
tsc_error_code
tsc_csm_write_in_msg (tsc_csm_info * info, tsc_csm_msg * msg)
{
    if (info) {
        tsc_lock *in_queue_lock = info->in_queue_lock;
        tsc_queue *in_queue = info->in_queue;

        if (tsc_lock_get (in_queue_lock) != tsc_lock_response_error) {

            if(tsc_queue_write (in_queue, msg) == tsc_queue_response_overflow) {

		tsc_lock_release (in_queue_lock);		
		TSC_ERROR("tsc_csm_write_in_msg: failed to write data to queue. Dropping data [%p]",info);
		if(tsc_get_log_level() == tsc_log_level_debug) {
		    
		    char str[TSC_BUFFER_LEN];
		    tsc_hexdump(msg->info.buffer.data, msg->info.buffer.len, str, 0, 0);
		    TSC_DEBUG("tsc_csm_write_in_msg: Data contents\n%s", str);
		    
		}
		
		TSC_STATS_INC(info, tsc_dropped_in_packets, 1);
		return tsc_error_code_queue_overflow;
		
	    }

	    tsc_lock_release (in_queue_lock);
	    return tsc_error_code_ok;
  	    
        }
        else {
            TSC_ERROR ("tsc_csm_write_in_msg: failed to get lock [%p][%p]", 
                       tsc_lock_get_taker_thread(info->data_lock),
                       info);
        }
    }
    return tsc_error_code_error;
    
}


tsc_error_code
tsc_csm_write_out_msg (tsc_csm_info * info, tsc_csm_msg * msg)
{
#ifdef TSC_REDUNDANCY
    /* the packet came in a secundary tunnel associated to a socket in the primary
       tunnel, processing has to be done in the primary tunnel */
    if (info->redundancy_socket) {
        TSC_DEBUG
            ("tsc_csm_write_out_msg: redundant needs to be processed \
in tunnel %p, socket %p [%p]", info->redundancy_socket->handle, info->redundancy_socket,
             info);

        tsc_lock *out_queue_lock = info->redundancy_socket->out_queue_lock;
        tsc_queue *out_queue = info->redundancy_socket->out_queue;

        if (tsc_lock_get (out_queue_lock) != tsc_lock_response_error) {
            /* this is always TCP tunnel */
            if (info->redundancy_socket->redundancy_info.load_balance == tsc_bool_true) {
                tsc_redundancy_parse((tsc_csm_info *)(info->redundancy_socket->handle), msg, 
                                     info->redundancy_socket, out_queue, tsc_bool_false);
            } else {
                tsc_redundancy_parse((tsc_csm_info *)(info->redundancy_socket->handle), msg, 
                                     info->redundancy_socket, out_queue, tsc_bool_false);
            }

            tsc_lock_release (out_queue_lock);

            return tsc_error_code_ok;
        } else {
            TSC_ERROR
                ("tsc_csm_write_out_msg: failed to get lock [%p][%p]",
                 tsc_lock_get_taker_thread(out_queue_lock),
                 info);

            return tsc_error_code_error;
        }
    }
#endif

    if (info) {
        /* since there is socket info, we fill the socket queue instead */
        tsc_lock *out_queue_lock;
        tsc_queue *out_queue;

        if (info->socket_info) {
            /* let's get IP layer */
            tsc_ip_address src_addr;
            tsc_ip_address dst_addr;
            uint8_t protocol;
            uint8_t version;

            if (tsc_ip_parse
                (&src_addr, &dst_addr, &protocol, &version, 
                 msg->info.buffer.data) == tsc_error_code_ok) {
                if (protocol == SOL_UDP) {
                    uint32_t src_port;
                    uint32_t dst_port;

                    if (tsc_udp_parse
                        (&src_port, &dst_port,
                         msg->info.buffer.data + TSC_IP_HEADER_SIZE) ==
                        tsc_error_code_ok) {
                        tsc_socket_info *socket_info;
                        tsc_socket_info *found;
                        tsc_ip_port_address src;
                        tsc_ip_port_address dst;

                        src.address = src_addr.address;
                        dst.address = dst_addr.address;
                        src.port = src_port;
                        dst.port = dst_port;

                          socket_info = info->socket_info;

                        found = NULL;

                        while (socket_info) {
                            if ((socket_info->src_address.address ==
                                 dst.address)
                                && (socket_info->src_address.port ==
                                    dst.port)) {
                                found = socket_info;

                                break;
                            }

                            socket_info = socket_info->next;
                        }

                        if (found) {
                            tsc_lock *out_queue_lock = found->out_queue_lock;
                            tsc_queue *out_queue = found->out_queue;

                            if (tsc_lock_get (out_queue_lock) !=
                                tsc_lock_response_error) {
                                char dst_addr_str[TSC_ADDR_STR_LEN];
                                char src_addr_str[TSC_ADDR_STR_LEN];

#ifdef TSC_REDUNDANCY
                                tsc_transport transport =
                                    info->tunnel_params.
                                    connection_params[info->connection_index].
                                    transport;

                                if (version == TSC_VERSION_ID_IP_4_RED || 
                                    version == TSC_VERSION_ID_IP_6_RED) {
                                    if (transport == tsc_transport_udp || 
                                        transport == tsc_transport_dtls) {
                                        tsc_redundancy_parse(info, msg, found, 
                                                             out_queue, tsc_bool_true);
                                    } else {
                                        if (found->redundancy_info.load_balance ==
                                            tsc_bool_false) {
                                            tsc_redundancy_parse(info, msg, found, 
                                                                 out_queue, tsc_bool_false);
                                        } else {
                                            tsc_redundancy_parse(info, msg, found, 
                                                                 out_queue, tsc_bool_false);
                                        }
                                    }
                                } else {
#endif
                                    if(tsc_queue_write (out_queue, msg) == tsc_queue_response_overflow) {
                                        tsc_lock_release (out_queue_lock);

                                        TSC_ERROR("tsc_csm_write_out_msg: failed to write data to queue. Dropping data [%p]",info);
                                        if(tsc_get_log_level() == tsc_log_level_debug) {
                                            char str[TSC_BUFFER_LEN];

                                            tsc_hexdump(msg->info.buffer.data, msg->info.buffer.len, str, 0, 0);

                                            TSC_DEBUG("tsc_csm_write_out_msg: Data contents\n%s", str);
                                        }
                                        
                                        TSC_STATS_INC(info, tsc_dropped_out_packets, 1);

                                        return tsc_error_code_queue_overflow;
                                    }
#ifdef TSC_REDUNDANCY
                                }
#endif

                                tsc_lock_release (out_queue_lock);

                                tsc_ip_port_address_to_str (&dst,
                                                            dst_addr_str,
                                                            TSC_ADDR_STR_LEN);

                                tsc_ip_port_address_to_str (&src,
                                                            src_addr_str,
                                                            TSC_ADDR_STR_LEN);

                                TSC_DEBUG
                                    ("tsc_csm_write_out_msg: %d-byte udp data \
recv (socket %p) [%s=>%s] [%p]",
                                     msg->info.buffer.len, found, src_addr_str,
                                     dst_addr_str, info);
                                if(found->socket_received_info) {

                                    tsc_notification_info *notification_info = found->socket_received_info;
                                    tsc_notification_socket_received_data data;
                                    memset(&data, 0, sizeof(tsc_notification_socket_received_data));
				    
                                    if(notification_info->notification){

                                        data.size = msg->info.buffer.len;
                                        data.socket = found->fd;
                                        notification_info->notification_data.data = &data;
                                        notification_info->notification(&(notification_info->notification_data));
                                    }
                                
                                }
                                return tsc_error_code_ok;

                            }
                            else {
                                TSC_ERROR
                                    ("tsc_csm_write_out_msg: failed to get lock [%p][%p]",
                                     tsc_lock_get_taker_thread(out_queue_lock),
                                     info);

                                return tsc_error_code_error;

                            }
                        }
                        else {
                            char dst_addr_str[TSC_ADDR_STR_LEN];
                            tsc_ip_port_address_to_str (&dst, dst_addr_str,
                                                        TSC_ADDR_STR_LEN);

                            TSC_DEBUG
                                ("tsc_csm_write_out_msg: failed to find a socket for %s [%p]",
                                 dst_addr_str, info);
                        }
                    }
                    else {
                        TSC_ERROR
                            ("tsc_csm_write_out_msg: failed to parse udp header [%p]",
                             info);

			return tsc_error_code_error;

                    }
                }
#ifdef TSC_UIP
                else if (protocol == SOL_TCP) {
                    if (tsc_handle_incoming_tcp (info, msg) == tsc_bool_false) {
                        TSC_ERROR
                            ("tsc_csm_write_out_msg: failed to handle incoming TCP [%p]",
                             info);

			return tsc_error_code_error;
                    }

                    return tsc_error_code_ok;
		    
                }
#endif
		else if (protocol == SOL_ICMP) {
		    
		    TSC_ERROR
			("tsc_csm_write_out_msg: data is ICMP. Discarding data (%d) [%p]",
                         protocol, info); 
		     
		    return tsc_error_code_ok;
		    
		}	
                else {
#ifdef TSC_UIP
                    TSC_ERROR
                        ("tsc_csm_write_out_msg: data is not udp/tcp (%d) [%p]",
                         protocol, info);
#else
                    TSC_ERROR
                        ("tsc_csm_write_out_msg: data is not udp (%d) [%p]",
                         protocol, info);
#endif

                    return tsc_error_code_error;
                }
            }
            else {
                TSC_ERROR
                    ("tsc_csm_write_out_msg: failed to parse ip header [%p]",
                     info);

		return tsc_error_code_error;
              
            }
        }
	out_queue_lock = info->out_queue_lock;
        out_queue = info->out_queue;

        if (tsc_lock_get (out_queue_lock) != tsc_lock_response_error) {

	    if(tsc_queue_write (out_queue, msg) == tsc_queue_response_overflow) {

		tsc_lock_release (out_queue_lock);		
		TSC_ERROR("tsc_csm_write_out_msg: failed to write data to queue. Dropping data [%p]",info);
		if(tsc_get_log_level() == tsc_log_level_debug) {
		    
		    char str[TSC_BUFFER_LEN];
		    tsc_hexdump(msg->info.buffer.data, msg->info.buffer.len, str, 0, 0);
		    TSC_DEBUG("tsc_csm_write_out_msg: Data contents\n%s", str);
		    
		}
		
		TSC_STATS_INC(info, tsc_dropped_out_packets, 1);
		return tsc_error_code_queue_overflow;
		
	    }
	    tsc_lock_release (out_queue_lock);
            return tsc_error_code_ok;

        }
       
    }

    return tsc_error_code_error;
}

tsc_bool
tsc_csm_process_client_service_response (tsc_handle handle, tsc_transaction *transaction,
                                         tsc_cm *response)
{
    tsc_csm_info *info = (tsc_csm_info *)handle;
    tsc_bool socket_info_valid = tsc_bool_false;
    if (info) {
        if (response->header.msg_type == tsc_cm_type_client_service_response) {
            if (!response->msg.client_service_response.response_code) {
                TSC_DEBUG
                    ("tsc_csm_process_client_service_response: client_service response recv'd [%p]",
		     info);
#ifdef TSC_REDUNDANCY
		if (transaction->msg.msg.client_service_request.service_type ==
                    TSC_TLV_SERVICE_TYPE_ENABLE_REDUNDANCY ||
                    transaction->msg.msg.client_service_request.service_type ==
                    TSC_TLV_SERVICE_TYPE_DISABLE_REDUNDANCY) {
		    tsc_transport transport =
			info->tunnel_params.connection_params[info->connection_index].
			transport;
		    printf("Hit red csr\n");
		    
		    tsc_socket_info *socket_info = (tsc_socket_info *)transaction->opaque;
		    tsc_socket_info *socket_info_temp = info->socket_info;
		    while (socket_info_temp) {
			if(socket_info == socket_info_temp) {
			    socket_info_valid = tsc_bool_true;
			    break;
			}
			socket_info_temp = socket_info_temp->next;
		    }
		    if(!socket_info_valid) {
			TSC_ERROR("tsc_csm_process_client_service_response: socket has been closed!");
			return tsc_bool_false;
		    }
		
		    if (socket_info) {
			if (socket_info->socket_options.redundancy.redundancy_factor > 0) {
			    if (!socket_info->redundancy_info.redundancy_factor) {
				socket_info->redundancy_info.redundancy_factor = 
				    socket_info->socket_options.redundancy.redundancy_factor;

				if (transport == tsc_transport_tcp || 
				    transport == tsc_transport_tls) {
				    if (socket_info->socket_options.redundancy.redundancy_method == 
					tsc_so_redundancy_method_tcp_tls_load_balance) {
					socket_info->redundancy_info.load_balance = 
					    tsc_bool_true;
				    } else {
					socket_info->redundancy_info.load_balance = 
					    tsc_bool_false;
				    }

				    TSC_DEBUG ("tsc_csm_process_client_service_response: load balance %d [%p][%p]",
					       socket_info->redundancy_info.load_balance,
					       socket_info->redundancy_info.redundancy_factor, 
					       socket_info, info);

				    socket_info->redundancy_info.load_balance_index = 
					0;

				    uint8_t max_tunnels = response->msg.
					client_service_response.
					service_response.
					redundancy.max_tunnels;

				    socket_info->redundancy_info.load_balance_size = max_tunnels;

				    uint8_t i;

				    for (i = 0; i < max_tunnels; i++) {
					tsc_requested_config requested_config;
					memcpy(&requested_config,
					       &(response->msg.client_service_response.
						 service_response.redundancy.tunnel_id[i]),
					       sizeof(tsc_requested_config));

					socket_info->redundancy_tunnel_list[i] =
					    tsc_new_tunnel(&(info->tunnel_params),
							   &requested_config); 

					((tsc_csm_info *)socket_info->
					 redundancy_tunnel_list[i])->
					    redundancy_socket = socket_info;

					TSC_DEBUG ("tsc_csm_process_client_service_response: redundancy tunnel %08X%08X [%p][%p]",
						   response->msg.client_service_response.service_response
						   .redundancy.tunnel_id[i].hi,
						   response->msg.client_service_response.service_response
						   .redundancy.tunnel_id[i].lo,
						   socket_info->redundancy_info.redundancy_factor, 
						   socket_info, info);
				    }
				}

				/* notification has been enabled */
				if (info->redundancy_notification_info) {
				    tsc_notification_info *notification_info = info->redundancy_notification_info;

				    tsc_notification_redundancy_info_data data;
				    memset(&data, 0, sizeof(tsc_notification_redundancy_info_data));

				    data.available = tsc_bool_true;
				    data.enabled = tsc_bool_true;
				    data.socket = socket_info->fd;
				    memcpy(&(data.options), &(socket_info->socket_options.redundancy),
					   sizeof(tsc_so_redundancy));

				    if (notification_info->notification) {
					notification_info->notification_data.data = &data;
					notification_info->notification(&(notification_info->notification_data));
				    }
				}

				TSC_DEBUG ("tsc_csm_process_client_service_response: socket redundancy set (%d) [%p][%p]",
					   socket_info->redundancy_info.redundancy_factor, 
					   socket_info, info);
			    }
			} else {
			    /* notification has been enabled */
			    if (info->redundancy_notification_info) {
				tsc_notification_info *notification_info = info->redundancy_notification_info;

				tsc_notification_redundancy_info_data data;
				memset(&data, 0, sizeof(tsc_notification_redundancy_info_data));

				data.available = tsc_bool_true;
				data.enabled = tsc_bool_false;
				data.socket = socket_info->fd;
				memcpy(&(data.options), &(socket_info->socket_options.redundancy),
				       sizeof(tsc_so_redundancy));

				if (notification_info->notification) {
				    notification_info->notification_data.data = &data;
				    notification_info->notification(&(notification_info->notification_data));
				}
			    }

			    socket_info->redundancy_info.redundancy_factor = 0;
			}
		    }
		}
#endif
#ifdef TSC_ODD
		else if (transaction->msg.msg.client_service_request.service_type ==
			 TSC_TLV_SERVICE_TYPE_ENABLE_ODD) {

		    tsc_socket_info *socket_info = (tsc_socket_info *)transaction->opaque;
		    tsc_socket_info *socket_info_temp = info->socket_info;
		    while (socket_info_temp) {
			if(socket_info == socket_info_temp) {
			    socket_info_valid = tsc_bool_true;
			    break;
			}
			socket_info_temp = socket_info_temp->next;
		    }
		    if(!socket_info_valid) {
			TSC_ERROR("tsc_csm_process_client_service_response: socket has been closed!");
			return tsc_bool_false;
		    }
			
		    if (socket_info) {
			TSC_DEBUG ("tsc_csm_process_client_service_response: on demand dtls tunnel [%p][%p]",
				   socket_info, info);
			tsc_requested_config requested_config;
			memcpy(&requested_config,
			       &(response->msg.client_service_response.
				 service_response.odd.tunnel_id),
			       sizeof(tsc_requested_config));
			tsc_tunnel_params tunnel_params;
			memcpy(&(tunnel_params.connection_params[0]), &(info->tunnel_params.connection_params[info->connection_index]),
			       sizeof(tsc_connection_params));
			tunnel_params.max_connections = 1;
			if(tunnel_params.connection_params[0].transport == tsc_transport_tcp) {
			    tunnel_params.connection_params[0].transport = tsc_transport_udp;
				
			}
			else if (tunnel_params.connection_params[0].transport == tsc_transport_tls)  {
			    tunnel_params.connection_params[0].transport = tsc_transport_dtls;	
			}
			socket_info->odd_tunnel =
			    tsc_new_tunnel(&tunnel_params,
					   &requested_config);
			((tsc_csm_info *)socket_info->
			 odd_tunnel)->odd_socket = socket_info;
			    
			TSC_DEBUG ("tsc_csm_process_client_service_response: odd tunnel %08X%08X [%p][%p]",
				   response->msg.client_service_response.service_response
				   .odd.tunnel_id.hi,
				   response->msg.client_service_response.service_response
				   .odd.tunnel_id.lo,
				   socket_info, info);
				       
			/* notification has been enabled */
			if (info->odd_notification_info) {
			    tsc_notification_info *notification_info = info->odd_notification_info;
				
			    tsc_notification_odd_info_data data;
			    memset(&data, 0, sizeof(tsc_notification_odd_info_data));
				
			    data.available = tsc_bool_true;
			    data.enabled = tsc_bool_true;
			    data.socket = socket_info->fd;
			    if (notification_info->notification) {
				notification_info->notification_data.data = &data;
				notification_info->notification(&(notification_info->notification_data));
			    }
			}
		    }
		} 
		else if(transaction->msg.msg.client_service_request.service_type ==
			TSC_TLV_SERVICE_TYPE_DISABLE_ODD) {
		    tsc_socket_info *socket_info = (tsc_socket_info *)transaction->opaque;
		    
		    /* notification has been enabled */
		    if (info->odd_notification_info) {
			tsc_notification_info *notification_info = info->odd_notification_info;
			    
			tsc_notification_redundancy_info_data data;
			memset(&data, 0, sizeof(tsc_notification_odd_info_data));
			    
			data.available = tsc_bool_true;
			data.enabled = tsc_bool_false;
			data.socket = socket_info->fd;
			if (notification_info->notification) {
			    notification_info->notification_data.data = &data;
			    notification_info->notification(&(notification_info->notification_data));
			}
		    }
			
		    socket_info->redundancy_info.redundancy_factor = 0;
		}
#endif
		return tsc_bool_true;
	    }
    
	}
	else {
	    TSC_ERROR
		("tsc_csm_process_client_service_response: bad response code %d [%p]",
		 response->msg.config_response.response_code, info);
		    
#ifdef TSC_REDUNDANCY
	    tsc_socket_info *socket_info = (tsc_socket_info *)transaction->opaque;
		    
	    if (socket_info) {
		if (socket_info->socket_options.redundancy.redundancy_factor > 0) {
		    /* notification has been enabled */
		    if (info->redundancy_notification_info) {
			tsc_notification_info *notification_info = info->redundancy_notification_info;

			tsc_notification_redundancy_info_data data;
			memset(&data, 0, sizeof(tsc_notification_redundancy_info_data));

			data.available = tsc_bool_false;
			data.socket = socket_info->fd;
			memcpy(&(data.options), &(socket_info->socket_options.redundancy),
			       sizeof(tsc_so_redundancy));

			if (notification_info->notification) {
			    notification_info->notification_data.data = &data;
			    notification_info->notification(&(notification_info->notification_data));
			}
		    }
		}
	    }

	    socket_info->redundancy_info.redundancy_factor = 0;

	    return tsc_bool_true;
#endif
	}
    }
    else {
	TSC_ERROR
	    ("tsc_csm_process_client_service_response: bad msg type %d [%p]",
	     response->header.msg_type, info);
    }
    return tsc_bool_false;
}

tsc_bool
tsc_csm_send_client_service_request (tsc_csm_info * info, void *opaque, 
                                     tsc_cm_client_service_request *service_request)
{
    if (info) {
        tsc_cm tcm;
        uint8_t output[TSC_CM_MAX_SIZE];
        size_t len;

        memset (&tcm, 0, sizeof (tsc_cm));
        tcm.header.version_id = TSC_VERSION;
        tcm.header.msg_type = tsc_cm_type_client_service_request;
        tcm.header.tunnel_id.hi = info->config.tunnel_id.hi;
        tcm.header.tunnel_id.lo = info->config.tunnel_id.lo;
        tcm.header.sequence = (info->config.sequence)++;

        memcpy(&(tcm.msg.client_service_request), 
               service_request, sizeof(tsc_cm_client_service_request));

        len = tsc_encode_cm (&tcm, output, TSC_CM_MAX_SIZE);

        if (len) {
            if (tsc_tunnel_socket_send
                (info->tunnel_socket, output, len,
                 tsc_bool_false, 0) == tsc_tunnel_socket_response_ok
                && info->tunnel_socket->result > 0) {
                TSC_DEBUG
                    ("tsc_csm_send_client_service_request: client service request sent (len %d) [%p]",
                     len, info);

                tsc_transaction_insert(info, &tcm, -1, 
				       TSC_SERVICE_RESPONSE_PENDING_TIMEOUT, opaque, NULL, 
				       NULL, NULL, 
				       tsc_csm_process_client_service_response);


                TSC_STATS_INC (info, tsc_service_request_count, 1);
                return tsc_bool_true;
            }
            else {
                TSC_ERROR
                    ("tsc_csm_send_client_service_request: failed to send client service request [%p]",
                     info);
            }
        }
        else {
            TSC_ERROR
                ("tsc_csm_send_client_service_request: failed to encode client service request [%p]",
                 info);
        }
    }

    return tsc_bool_false;
}


tsc_bool
tsc_csm_send_config_request (tsc_csm_info * info, tsc_bool reconnect)
{
    if (info) {
        tsc_cm tcm;
        uint8_t output[TSC_CM_MAX_SIZE];
        size_t len;
        memset (&tcm, 0, sizeof (tsc_cm));
        tcm.header.version_id = TSC_VERSION;
        tcm.header.msg_type = tsc_cm_type_config_request;

        if (reconnect == tsc_bool_true) {
            tcm.header.tunnel_id.hi = info->config.tunnel_id.hi;
            tcm.header.tunnel_id.lo = info->config.tunnel_id.lo;
        }
        else {
            tcm.header.tunnel_id.hi = 0xFFFFFFFF;
            tcm.header.tunnel_id.lo = 0xFFFFFFFF;
        }

        tcm.header.sequence = (info->config.sequence)++;

        uint8_t buffer[TSC_MAX_VERSION_SIZE];
        TSC_VERSION_STR(buffer);

        time_t time = tsc_time ();

        struct tm *time_ptr = localtime (&time);

        if (time_ptr) {
            char aux[TSC_MAX_STR_LEN];
            strftime (aux, TSC_MAX_STR_LEN, " %x %X", time_ptr);

            strcat((char *)buffer, aux);
        }

        uint32_t size = strlen((char *)buffer);

        tcm.msg.config_request.valid_client_info = tsc_bool_true;
        memcpy(tcm.msg.config_request.client_info, buffer, size);
        tcm.msg.config_request.client_info_len = size;

        tcm.msg.config_request.supported_version_id = TSC_VERSION;
        tcm.msg.config_request.valid_supported_version_id = tsc_bool_true;

        /* tcm.msg.config_request.valid_internal_ip_address = tsc_bool_true;
           tcm.msg.config_request.valid_sip_server = tsc_bool_true;
           tcm.msg.config_request.valid_internal_ip_mask = tsc_bool_true;
           tcm.msg.config_request.valid_keepalive_interval = tsc_bool_true; */

        len = tsc_encode_cm (&tcm, output, TSC_CM_MAX_SIZE);

        if (len) {
            if (tsc_tunnel_socket_send
                (info->tunnel_socket, output, len,
                 tsc_bool_false, 0) == tsc_tunnel_socket_response_ok
                && info->tunnel_socket->result > 0) {
                TSC_DEBUG
                    ("tsc_csm_send_config_request: config request sent (len %d) [%p]",
                     len, info);

                return tsc_bool_true;
            }
            else {
                TSC_ERROR
                    ("tsc_csm_send_config_request: failed to send config request [%p]",
                     info);
            }
        }
        else {
            TSC_ERROR
                ("tsc_csm_send_config_request: failed to encode config request [%p]",
                 info);
        }
    }

    return tsc_bool_false;
}

tsc_bool
tsc_csm_process_release_response (tsc_handle handle, tsc_transaction *transaction,
                                  tsc_cm *response)
{
    tsc_csm_info *info = (tsc_csm_info *)handle;

    if (info) {
        if (response->header.msg_type == tsc_cm_type_config_release_response) {
            if (!response->msg.config_response.response_code) {
                TSC_DEBUG
                    ("tsc_csm_process_release_response: config release response recv'd [%p]",
                     info);

                return tsc_bool_true;
            }
            else {
                TSC_ERROR
                    ("tsc_csm_process_release_response: bad response code %d [%p]",
                     response->msg.config_response.response_code, info);
            }
        }
        else {
            TSC_ERROR
                ("tsc_csm_process_release_response: bad msg type %d [%p]",
                 response->header.msg_type, info);
        }
    }

    return tsc_bool_false;
}

tsc_bool
tsc_csm_send_release_request (tsc_csm_info * info, tsc_lock *lock)
{
    if (info) {
        tsc_cm tcm;
        uint8_t output[TSC_CM_MAX_SIZE];
        size_t len;

        memset (&tcm, 0, sizeof (tsc_cm));
        tcm.header.version_id = TSC_VERSION;
        tcm.header.msg_type = tsc_cm_type_config_release_request;
        tcm.header.tunnel_id.hi = info->config.tunnel_id.hi;
        tcm.header.tunnel_id.lo = info->config.tunnel_id.lo;
        tcm.header.sequence = (info->config.sequence)++;

        len = tsc_encode_cm (&tcm, output, TSC_CM_MAX_SIZE);

        if (len) {
            if (tsc_tunnel_socket_send
                (info->tunnel_socket, output, len,
                 tsc_bool_false, 0) == tsc_tunnel_socket_response_ok
                && info->tunnel_socket->result > 0) {
                TSC_DEBUG
                    ("tsc_csm_send_release_request: config release sent (len %d) [%p]",
                     len, info);

                tsc_transaction_insert(info, &tcm, 0, 
				       TSC_RELEASE_PENDING_TIMEOUT, NULL, lock,
				       NULL, NULL, 
				       tsc_csm_process_release_response);

                return tsc_bool_true;
            }
            else {
                TSC_ERROR
                    ("tsc_csm_send_release_request: failed to send config release [%p]",
                     info);
            }
        }
        else {
            TSC_ERROR
                ("tsc_csm_send_release_request: failed to encode config release [%p]",
                 info);
        }
    }

    return tsc_bool_false;
}

void
tsc_csm_notify_tunnel_socket_info(tsc_csm_info* info)
{
    tsc_notification_info *notification_info = info->tunnel_socket_notification_info;
    tsc_tunnel_socket_info data;
    if (notification_info->notification) {
	if (info->tunnel_socket && tsc_get_tunnel_socket_info(info, &data) != tsc_error_code_error) {
	    notification_info->notification_data.data = &data;
	    notification_info->notification(&(notification_info->notification_data));
	} else {
	    TSC_ERROR ("%s: failed to get tunnel socket info\n", __FUNCTION__);
	}
    }
}

tsc_bool
tsc_csm_process_config_response (tsc_csm_info * info, void *buf, size_t len)
{
    if (info) {
        tsc_cm tcm;
        memset (&tcm, 0, sizeof (tsc_cm));

        if (tsc_decode_cm (buf, len, &tcm) == tsc_bool_true) {
            if (tcm.header.msg_type == tsc_cm_type_config_response) {
                if (!tcm.msg.config_response.response_code) {
		    tsc_bool do_socket_info_notify = tsc_bool_false;

                    if (info->tunnel_socket_notification_info &&
			(info->tunnel_socket_notification_info->enabled == tsc_bool_true)) {
                        do_socket_info_notify = tsc_bool_true;
                    }

                    TSC_DEBUG
                        ("tsc_csm_process_config_response: config response recv'd [%p]",
                         info);

                    if (tcm.msg.config_response.valid_internal_ip_address ==
                        tsc_bool_true) {
                        memcpy (&(info->config.internal_address),
                                &(tcm.msg.config_response.
                                  internal_ip_address),
                                sizeof (tsc_ip_address));
                    }

                    if (tcm.msg.config_response.valid_internal_ip_mask ==
                        tsc_bool_true) {
                        memcpy (&(info->config.mask),
                                &(tcm.msg.config_response.internal_ip_mask),
                                sizeof (tsc_ip_mask));
                    }

                    if (tcm.msg.config_response.valid_sip_server ==
                        tsc_bool_true) {
                        memcpy (&(info->config.sip_server),
                                &(tcm.msg.config_response.sip_server),
                                sizeof (tsc_ip_port_address));
                    }

                    if (tcm.msg.config_response.valid_peer_ip_port ==
                        tsc_bool_true) {
			/* Check if notification is enabled and notify if tunnel socket info changed */
			if (do_socket_info_notify == tsc_bool_true &&
			    (info->tunnel_params.connection_params[info->connection_index].nat_ipport.address ==
			     tcm.msg.config_response.peer_ip_port.address) &&
			    (info->tunnel_params.connection_params[info->connection_index].nat_ipport.port ==
			     tcm.msg.config_response.peer_ip_port.port)) {
			    do_socket_info_notify = tsc_bool_false;
			}
                        memcpy (&(info->tunnel_params.connection_params[info->connection_index].nat_ipport),
                                &(tcm.msg.config_response.peer_ip_port),
                                sizeof (tsc_ip_port_address));
                    }

                    if (tcm.msg.config_response.valid_keepalive_interval ==
                        tsc_bool_true) {
#ifdef TSC_ANDROID
                        if (tcm.msg.config_response.keepalive_interval > TSC_KEEPALIVE_GUARD) {
                            /* lease needs to be renewed TSC_KEEPALIVE_GUARD seconds
                               before expiration time (for android only as timer gets
                               distorted when devices goes to sleep) */
                            info->config.keepalive_interval =
                                tcm.msg.config_response.keepalive_interval -TSC_KEEPALIVE_GUARD;
                        } else {
                            /* lease needs to be renewed at 66% of the way */
                            info->config.keepalive_interval =
                                tcm.msg.config_response.keepalive_interval * 2 /
                                3;
                        }
#else
                        /* lease needs to be renewed at 66% of the way */
                        info->config.keepalive_interval =
                            tcm.msg.config_response.keepalive_interval * 2 /
                            3;
#endif
                    }

                    if (tcm.msg.config_response.valid_supported_version_id ==
                        tsc_bool_true) {
                        info->config.supported_version_id =
                            tcm.msg.config_response.supported_version_id;
                    }

                    info->config.tunnel_id.hi = tcm.header.tunnel_id.hi;
                    info->config.tunnel_id.lo = tcm.header.tunnel_id.lo;

		    if (do_socket_info_notify == tsc_bool_true) {
			tsc_csm_notify_tunnel_socket_info(info);
		    }

                    return tsc_bool_true;
                }
                else {
                    TSC_ERROR
                        ("tsc_csm_process_config_response: bad response code %d [%p]",
                         tcm.msg.config_response.response_code, info);

                    info->config.tunnel_id.hi = 0;
                    info->config.tunnel_id.lo = 0;
                }
            }
            else {
                TSC_ERROR
                    ("tsc_csm_process_config_response: bad msg type %d [%p]",
                     tcm.header.msg_type, info);
            }
        }
        else {
            TSC_ERROR
                ("tsc_csm_process_config_response: failed to process config response [%p]",
                 info);
        }
    }

    return tsc_bool_false;
}

tsc_bool
tsc_csm_process_keepalive_response (tsc_handle handle, tsc_transaction *transaction,
                                    tsc_cm *response)
{
    tsc_csm_info *info = (tsc_csm_info *)handle;

    if (info) {
        if (response->header.msg_type == tsc_cm_type_keepalive_response) {
            if (!response->msg.config_response.response_code) {
                TSC_DEBUG
                    ("tsc_csm_process_keepalive_response: keepalive response recv'd [%p]",
                     info);
                return tsc_bool_true;
            }
            else {
                TSC_ERROR
                    ("tsc_csm_process_keepalive_response: bad response code %d [%p]",
                     response->msg.config_response.response_code, info);
            }
        }
        else {
            TSC_ERROR
                ("tsc_csm_process_keepalive_response: bad msg type %d [%p]",
                 response->header.msg_type, info);
        }
    }

    return tsc_bool_false;
}

tsc_bool
tsc_csm_keepalive_timeout (tsc_handle handle, tsc_transaction *transaction)
{
    tsc_csm_info *info = (tsc_csm_info *)handle;

    TSC_ERROR("csm_thread: never received keepalive response [%p]",
              info);

    info->state_info.state = tsc_state_fatal_error;
    info->state_info.error = tsc_error_code_keepalive_failure;
    info->fatal_error_sleep_time = tsc_time();
    
    return tsc_bool_true;
}

tsc_bool
tsc_csm_send_keepalive (tsc_csm_info * info)
{
    if (info) {
        tsc_cm tcm;
        uint8_t output[TSC_CM_MAX_SIZE];
        size_t len;

        memset (&tcm, 0, sizeof (tsc_cm));
        tcm.header.version_id = TSC_VERSION;
        tcm.header.msg_type = tsc_cm_type_keepalive;
        tcm.header.tunnel_id.hi = info->config.tunnel_id.hi;
        tcm.header.tunnel_id.lo = info->config.tunnel_id.lo;
        tcm.header.sequence = (info->config.sequence)++;

        len = tsc_encode_cm (&tcm, output, TSC_CM_MAX_SIZE);

        if (len) {
            if (tsc_tunnel_socket_send
                (info->tunnel_socket, output, len,
                 tsc_bool_false, 0) == tsc_tunnel_socket_response_ok
                && info->tunnel_socket->result > 0) {
                TSC_DEBUG
                    ("tsc_csm_send_keepalive: keepalive sent (len %d) [%p]",
                     len, info);

                tsc_transaction_insert(info, &tcm, 0, 
				       TSC_KEEPALIVE_PENDING_TIMEOUT, NULL, NULL, 
				       tsc_csm_keepalive_timeout, NULL, 
				       tsc_csm_process_keepalive_response);

                TSC_STATS_INC (info, tsc_keep_alive_count, 1);

                return tsc_bool_true;
            }
            else {
                TSC_ERROR
                    ("tsc_csm_send_keepalive: failed to send keepalive [%p]",
                     info);
            }
        }
        else {
            TSC_ERROR
                ("tsc_csm_send_keepalive: failed to encode keepalive [%p]",
                 info);
        }
    }

    return tsc_bool_false;
}

tsc_error_code
tsc_get_state (tsc_handle handle, tsc_state_info * state_info)
{
    tsc_csm_info *info = (tsc_csm_info *) handle;

    if (info) {
        if (tsc_lock_get (info->data_lock) != tsc_lock_response_error) {
            memcpy (state_info, &(info->state_info), sizeof (tsc_state_info));

            tsc_lock_release (info->data_lock);

            return tsc_error_code_ok;
        }
    }

    TSC_ERROR ("tsc_get_state: failed to retrieve state [%p]", handle);

    return tsc_error_code_error;
}

tsc_error_code
tsc_get_config (tsc_handle handle, tsc_config * config)
{
    tsc_csm_info *info = (tsc_csm_info *) handle;

    if (info) {
        if (tsc_lock_get (info->data_lock) != tsc_lock_response_error) {

            memcpy (config, &(info->config), sizeof (tsc_config));

            tsc_lock_release (info->data_lock);

            TSC_DEBUG ("tsc_get_config: config retrieved ok [%p]", handle);

            return tsc_error_code_ok;
        }
    }

    TSC_ERROR ("tsc_get_config: failed to retrieve config [%p]", handle);

    return tsc_error_code_error;
}

tsc_error_code
tsc_get_wakeup_interval (tsc_handle handle, uint32_t *wakeup_interval)
{
    tsc_csm_info *info = (tsc_csm_info *) handle;

    if (info) {
        if (tsc_lock_get (info->data_lock) != tsc_lock_response_error) {
            if (wakeup_interval) {
                if (info->config.keepalive_interval
                    && (tsc_time () >
                        (info->keepalive_time +
                         info->config.keepalive_interval))) {
                    /* let's wake up now! */
                    *wakeup_interval = 0;
                } else {
                    *wakeup_interval = info->keepalive_time + info->config.keepalive_interval - tsc_time();
                }                
            }            

            tsc_lock_release (info->data_lock);

            TSC_DEBUG ("tsc_get_wakeup_interval: wakeup interval retrieved ok [%p]", handle);

            return tsc_error_code_ok;
        }
    }

    TSC_ERROR ("tsc_get_wakeup_interval: failed to retrieve wakeup interval [%p]", handle);

    return tsc_error_code_error;
}

tsc_error_code
tsc_send_data (tsc_handle handle, void *buffer, uint32_t size,
               tsc_output_option * option)
{
    tsc_state_info state_info;
    tsc_csm_info *info;
    tsc_get_state (handle, &state_info);

    if (state_info.state != tsc_state_established && state_info.state != tsc_state_established_slow_poll) {
        TSC_ERROR ("tsc_send_data: wrong state [state %d] [%p]", state_info.state, handle);

        return tsc_error_code_cannot_send_data;
    }

    info = (tsc_csm_info *) handle;

    if (info) {
#ifdef TSC_PCAP_CAPTURE
        if (info->pcap_fd) {
            uint32_t clock = tsc_get_clock();

            uint32_t sec = clock/1000;
            uint32_t usec = (clock - sec*1000)*1000;
    
            uint32_t len = size + TSC_ETHER_HDR_LEN;

            fwrite(&sec, sizeof(uint8_t), sizeof(uint32_t), info->pcap_fd);
            fwrite(&usec, sizeof(uint8_t), sizeof(uint32_t), info->pcap_fd);
            fwrite(&len, sizeof(uint8_t), sizeof(uint32_t), info->pcap_fd);
            fwrite(&len, sizeof(uint8_t), sizeof(uint32_t), info->pcap_fd);

            struct ether_header ether;
            memset(&ether, sizeof(struct ether_header), 0);

            ether.ether_type = htons(ETHERTYPE_IP);

            fwrite(&ether, sizeof(uint8_t), sizeof(struct ether_header), info->pcap_fd);

            fwrite(buffer, sizeof(uint8_t), size, info->pcap_fd);

            fflush(info->pcap_fd);

            TSC_DEBUG ("tsc_send_data: pcap dumping (len %d) [%p]", len,
                       handle);
        }
#endif

        if (option && option->realtime == tsc_bool_true) {
            if (tsc_tunnel_socket_send
                (info->tunnel_socket, buffer, size,
                 tsc_bool_true, 0) == tsc_tunnel_socket_response_ok
                && info->tunnel_socket->result > 0) {
                TSC_DEBUG ("tsc_send_data: data sent ok (len %d) [%p]", size,
                           handle);
                TSC_STATS_INC (handle, tsc_sent_bytes, size);
                return tsc_error_code_ok;
            }
        }
        else {
            if (tsc_lock_get (info->data_lock) != tsc_lock_response_error) {
                tsc_csm_msg data;
                data.msg_type = tsc_csm_msg_type_data;
                memcpy (data.info.buffer.data, buffer, size);
                data.info.buffer.len = size;
                data.timestamp = tsc_get_clock();

                tsc_error_code error = tsc_csm_write_in_msg (info, &data);

                tsc_lock_release (info->data_lock);
	    
                if (error == tsc_error_code_ok) {
                    TSC_DEBUG ("tsc_send_data: data queued ok (len %d) [%p]",
                               size, handle);
                }
                else {
                    TSC_ERROR ("tsc_send_data: failed to queue data [%p]", handle);
                }

                return error;
            } else {
                TSC_ERROR ("tsc_send_data: failed to get lock [%p][%p]", 
			   tsc_lock_get_taker_thread(info->data_lock),
			   handle);
            }
        }
    }

    TSC_ERROR ("tsc_send_data: failed to send data [%p]", handle);

    return tsc_error_code_error;
}

tsc_error_code
tsc_send_udp_data (tsc_handle handle, tsc_ip_port_address * src,
                   tsc_ip_port_address * dst, void *buffer, uint32_t size,
                   tsc_output_option * option)
{
    tsc_csm_info *info = (tsc_csm_info *) handle;

    if (info) {
        uint8_t data[TSC_MAX_FRAME_SIZE];
        memcpy (data + TSC_IP_HEADER_SIZE + TSC_UDP_HEADER_SIZE, buffer,
                size);

        if (tsc_udp_make
            (src->port, dst->port, data + TSC_IP_HEADER_SIZE,
             data + TSC_IP_HEADER_SIZE + TSC_UDP_HEADER_SIZE,
             size) == tsc_error_code_ok) {
            tsc_bool loopback;
            tsc_ip_address ip_src;
            tsc_ip_address ip_dst;
            ip_src.address = src->address;

            loopback = tsc_bool_false;

            if (dst->address == INADDR_LOOPBACK) {
                TSC_DEBUG ("tsc_send_data: detected loopback [%p]", handle);

                loopback = tsc_bool_true;

                ip_dst.address = info->config.internal_address.address;
            }
            else {
                ip_dst.address = dst->address;
            }

            uint8_t ip_version = TSC_VERSION_ID_IP_4;

#ifdef TSC_REDUNDANCY
            /* let's detect whether redundancy is enabled, if so, our IP header will be 5 instead
               of 4 to mark redundancy */
            tsc_socket_info *socket_info = (tsc_socket_info *)option->private_data;

            if (socket_info && socket_info->redundancy_info.redundancy_factor > 0) {
                ip_version = TSC_VERSION_ID_IP_4_RED;
            }
#endif

            if (tsc_ip_make
                (&ip_src, &ip_dst, SOL_UDP, data, data + TSC_IP_HEADER_SIZE,
                 option->tos, ip_version, 
                 size + TSC_UDP_HEADER_SIZE) == tsc_error_code_ok) {

                /* if packet is going to loopback interface then put it
                   directly in the in queue (don't send it to the tunnel) */
                if (loopback == tsc_bool_true) {
                    tsc_csm_msg msg;
                    msg.msg_type = tsc_csm_msg_type_data;
                    msg.info.buffer.len =
                        size + TSC_UDP_HEADER_SIZE + TSC_IP_HEADER_SIZE;
                    memcpy (msg.info.buffer.data, data, msg.info.buffer.len);
                    msg.timestamp = tsc_get_clock();

                    if (tsc_csm_write_out_msg (info, &msg) == tsc_error_code_ok) {
                        return tsc_error_code_ok;
                    }
                }
                else {
                    return tsc_send_data (handle, data,
                                          size + TSC_UDP_HEADER_SIZE +
                                          TSC_IP_HEADER_SIZE, option);
                }
            }
        }
    }

    TSC_ERROR ("tsc_send_data: failed to send data [%p]", handle);

    return tsc_error_code_error;
}

tsc_bool
tsc_csm_process_data_in (tsc_csm_info * info)
{
    if (info) {
        tsc_bool result = tsc_bool_true;

        uint8_t *data = info->input_frame.data;
        uint32_t size = info->input_frame.len;

        char str[TSC_BUFFER_LEN];
        TSC_HEXDUMP (data, size, str, 0, 0);
        TSC_TRACE
            ("tsc_csm_process_data_in [%p]: data in start\n%stsc_csm_process_data_in [%p]: data in end",
             info, str, info);

        while (size > 0) {
            uint8_t version_id = (((uint8_t *) data)[0x0] & 0xf0) >> 4;

            if (version_id == TSC_VERSION_ID_IP_4
#ifdef TSC_REDUNDANCY
                || version_id == TSC_VERSION_ID_IP_4_RED || version_id == TSC_VERSION_ID_IP_6_RED
#endif
                || version_id == TSC_VERSION_ID_IP_6) {
                uint32_t length = ntohs (((uint16_t *) data)[0x1]);
                info->poll_time = tsc_time();
                if(info->state_info.state == tsc_state_established_slow_poll){
                    info->state_info.state = tsc_state_established;
                }
                if (length <= size) {
                    tsc_csm_msg msg;
                    TSC_DEBUG
                        ("tsc_csm_process_data_in: whole ip found (len %d) [%p]",
                         length, info);


                    msg.msg_type = tsc_csm_msg_type_data;
                    memcpy (msg.info.buffer.data, data, length);
                    msg.info.buffer.len = length;
                    msg.timestamp = tsc_get_clock();

#ifdef TSC_PCAP_CAPTURE
                    if (info->pcap_fd) {
                        uint32_t clock = tsc_get_clock();

                        uint32_t sec = clock/1000;
                        uint32_t usec = (clock - sec*1000)*1000;

                        uint32_t len = length + TSC_ETHER_HDR_LEN;

                        fwrite(&sec, sizeof(uint8_t), sizeof(uint32_t), info->pcap_fd);
                        fwrite(&usec, sizeof(uint8_t), sizeof(uint32_t), info->pcap_fd);
                        fwrite(&len, sizeof(uint8_t), sizeof(uint32_t), info->pcap_fd);
                        fwrite(&len, sizeof(uint8_t), sizeof(uint32_t), info->pcap_fd);

                        struct ether_header ether;
                        memset(&ether, sizeof(struct ether_header), 0);

                        ether.ether_type = htons(ETHERTYPE_IP);

                        fwrite(&ether, sizeof(uint8_t), sizeof(struct ether_header), info->pcap_fd);

                        fwrite(data, sizeof(uint8_t), length, info->pcap_fd);

                        fflush(info->pcap_fd);

                        TSC_DEBUG ("tsc_csm_process_data_in: pcap dumping (len %d) [%p]", len,
                                   info);
                    }
#endif

                    if (tsc_csm_write_out_msg (info, &msg) == tsc_error_code_error) {
                        TSC_DEBUG
                            ("tsc_csm_process_data_in: failed to push msg [%p]",
                             length, info);
                        result = tsc_bool_false;

                        break;
                    }

                    size -= length;
                    memmove (data, data + length, size);
                }
                else if (length > TSC_MAX_FRAME_SIZE) {
                    TSC_DEBUG("tsc_csm_process_data_in: data (len %d) is bigger than "
                              "TSC_MAX_FRAME_SIZE (len %d)", length,TSC_MAX_FRAME_SIZE);
                    result = tsc_bool_false;
                    break;
                }
                else {
                    TSC_DEBUG
                        ("tsc_csm_process_data_in: not enough data for IP (len %d, req len %d) [%p]",
                         size, length, info);

                    break;
                }
            }
            else {
                if (size >= 16) {
                    uint16_t tlv_count = ntohs (((uint16_t *) data)[1]);
                    uint32_t byte_count;
                    uint8_t *byte_ptr;
                    tsc_bool found;
                    TSC_DEBUG
                        ("tsc_csm_process_data_in: tlv count is %d [%p]",
                         tlv_count, info);

                    byte_count = size - 16;

                    byte_ptr = (uint8_t *) data + 16;

                    found = tsc_bool_false;

                    if (!tlv_count) {
                        found = tsc_bool_true;
                    }

                    while ((tlv_count > 0) && (byte_count > 0)) {
                        uint8_t tlv_type = *byte_ptr++;
                        byte_count--;

                        if (byte_count > 0) {
                            uint8_t tlv_len = *byte_ptr++;
                            byte_count--;

                            TSC_DEBUG
                                ("tsc_csm_process_data_in: found tlv %d len %d [%p]",
                                 tlv_type, tlv_len, info);

                            if (byte_count >= tlv_len) {
                                byte_ptr += tlv_len;
                                byte_count -= tlv_len;

                                tlv_count--;

                                if (!tlv_count) {
                                    found = tsc_bool_true;

                                    break;
                                }
                            }
                            else {
                                TSC_DEBUG
                                    ("tsc_csm_process_data_in: not enough data to process tlv len %d [%p]",
                                     size, info);

                                break;
                            }
                        }
                        else {
                            TSC_DEBUG
                                ("tsc_csm_process_data_in: not enough data to process CM len %d [%p]",
                                 size, info);

                            break;
                        }
                    }

                    if (found == tsc_bool_true) {
                        uint32_t len = byte_ptr - data;
                        tsc_csm_msg msg;

                        TSC_DEBUG
                            ("tsc_csm_process_data_in: found CM len %d [%p]",
                             len, info);


                        msg.msg_type = tsc_csm_msg_type_data;
                        memcpy (msg.info.buffer.data, data, len);
                        msg.info.buffer.len = len;

                        if (tsc_queue_write (info->cm_queue, &msg) ==
                            tsc_queue_response_error) {
                            TSC_DEBUG
                                ("tsc_csm_process_data_in: failed to push cm [%p]",
                                 len, info);
                            result = tsc_bool_false;

                            break;
                        }

                        size -= len;
                        memmove (data, data + len, size);
                    }
                    else {
                        TSC_DEBUG
                            ("tsc_csm_process_data_in: incomplete CM [%p]\n",
                             info);

                        break;
                    }
                }
                else {
                    TSC_DEBUG
                        ("tsc_csm_process_data_in: CM buffer is not big enough for processing len %d [%p]",
                         size, info);

                    break;
                }

            }
        }

        TSC_DEBUG ("tsc_csm_process_data_in: updating len %d [%p]", size,
                   info);

        info->input_frame.len = size;

        return result;
    }

    TSC_ERROR ("tsc_csm_process_data_in: failed to process data [%p]", info);

    return tsc_bool_false;
}

tsc_error_code
tsc_recv_data (tsc_handle handle, void *buffer, uint32_t * size)
{
    tsc_csm_info *info = (tsc_csm_info *) handle;

    if (info) {
        return tsc_recv_queue_data (handle, info->out_queue_lock,
                                    info->out_queue, buffer, size);
    }

    TSC_ERROR ("tsc_recv_data: failed to recv data [%p]", handle);

    return tsc_error_code_error;
}

tsc_error_code
tsc_recv_udp_data (tsc_handle handle, tsc_ip_port_address * src,
                   tsc_ip_port_address * dst, void *buffer, uint32_t * size)
{
    tsc_csm_info *info = (tsc_csm_info *) handle;

    if (info) {
        return tsc_recv_udp_queue_data (handle, info->out_queue_lock,
                                        info->out_queue, src, dst, buffer,
                                        size);
    }

    TSC_ERROR ("tsc_recv_udp_data: failed to recv udp data [%p]", handle);

    return tsc_error_code_error;
}

tsc_bool
tsc_csm_send_http_connect (tsc_csm_info * info, tsc_bool authentication)
{
    char tunnel_addr_str[TSC_ADDR_STR_LEN];
    char proxy_addr_str[TSC_ADDR_STR_LEN];
    char buffer[TSC_BUFFER_LEN];
    char str[TSC_BUFFER_LEN];

    tsc_http_proxy_config *proxy_config =
        &(info->tunnel_params.connection_params[info->connection_index].
          proxy_config);
    tsc_ip_port_address *server_address =
        &(info->tunnel_params.connection_params[info->connection_index].
          server_address);

    tsc_ip_port_address_to_str (server_address, tunnel_addr_str,
                                TSC_ADDR_STR_LEN);

    tsc_ip_port_address_to_str (&(proxy_config->proxy_address),
                                proxy_addr_str, TSC_ADDR_STR_LEN);

    sprintf (buffer, "CONNECT %s HTTP/1.0\nHost: %s\n", tunnel_addr_str,
             proxy_addr_str);

    if (authentication == tsc_bool_true && strlen (proxy_config->username)) {
        char tmp[TSC_ADDR_STR_LEN];
        char aux[TSC_ADDR_STR_LEN];
        sprintf (aux, "%s:%s", proxy_config->username,
                 proxy_config->password);
        tsc_base64_encode (tmp, (uint8_t *) aux, strlen (aux));
        sprintf (aux, "Proxy-Authorization: basic %s\n\n", tmp);
        strcat (buffer, aux);
    }
    else {
        strcat (buffer, "\n");
    }


    TSC_HEXDUMP (buffer, strlen (buffer), str, 0, 0);
    TSC_TRACE
        ("tsc_csm_send_http_connect [%p]:\n%stsc_csm_send_http_connect [%p]: end",
         info, str, info);

    if (tsc_tunnel_socket_send
        (info->tunnel_socket, buffer, strlen (buffer),
         tsc_bool_true, 0) == tsc_tunnel_socket_response_ok
        && info->tunnel_socket->result > 0) {
        TSC_DEBUG ("tsc_csm_send_http_connect: http request sent [%p]", info);

        return tsc_bool_true;
    }

    TSC_ERROR ("tsc_csm_send_http_connect: failed to send http request [%p]",
               info);

    return tsc_bool_false;
}

tsc_csm_proxy_response
tsc_csm_process_proxy_response (tsc_csm_info * info)
{
    /* response looks like HTTP/x.y nnn text nnn is in between two spaces */

    http_proxy_buffer *buffer = &(info->proxy_buffer);

    char data[TSC_BUFFER_LEN];
    char str[TSC_BUFFER_LEN];
    char *init;
    uint32_t content_length = 0;
    uint32_t response_code = 0;
    size_t len = buffer->len;
    memset (data, 0, TSC_BUFFER_LEN);
    memcpy (data, buffer->data, len);


    TSC_HEXDUMP (data, len, str, 0, 0);
    TSC_TRACE
        ("tsc_csm_process_proxy_response [%p]:\n%stsc_csm_send_http_connect [%p]: end",
         info, str, info);

    init = data;

    for (;;) {
        char line[TSC_BUFFER_LEN];

        uint32_t i = 0;
        while (*init) {
            line[i] = *init;

            if ((*init == '\n' && *(init + 1) == '\r')
                || (*init == '\r' && *(init + 1) == '\n')) {
                line[i] = 0;
                init += 2;
                break;
            }
            else if (*init == '\n' || *init == '\r') {
                line[i] = 0;
                init++;
                break;
            }

            i++;
            init++;
        }

        if (!strlen (line)) {
            size_t total;
            TSC_DEBUG
                ("tsc_csm_process_proxy_response: checking content_length %d [%p]",
                 content_length, info);
            total = len - (init - data);

            if (total < content_length) {
                TSC_DEBUG
                    ("tsc_csm_process_proxy_response: not enough data %d [%p]",
                     total, info);

                return tsc_csm_proxy_response_pending;
            }
            else {
                TSC_DEBUG
                    ("tsc_csm_process_proxy_response: have enough data %d [%p]",
                     total, info);

                break;
            }
        }
        else if (strstr (line, "Content-Length: ")) {
            strcpy (line, strstr (line, ": ") + 2);
            content_length = atoi (line);
        }
        else if (!strncmp (line, "HTTP", 4)) {
            if (strstr (line, " ")) {
                strcpy (line, strstr (line, " ") + 1);

                if (strstr (line, " ")) {
                    *strstr (line, " ") = 0;

                    response_code = atoi (line);
                }
            }
        }
    }

    TSC_DEBUG
        ("tsc_csm_process_proxy_response: http response_code is %d [%p]",
         response_code, info);

    if (response_code == 200) {
        return tsc_csm_proxy_response_ok;
    }
    else if (response_code == 407) {
        return tsc_csm_proxy_response_authenticate;
    }

    return tsc_csm_proxy_response_error;
}

tsc_bool
tsc_csm_release_tunnel (tsc_csm_info * info, tsc_lock *lock)
{
    if (info) {
        if (tsc_lock_get (info->data_lock) != tsc_lock_response_error) {
            if (info->state_info.state == tsc_state_established
                || info->state_info.state == tsc_state_established_slow_poll) {
                if (tsc_csm_send_release_request (info, lock) == tsc_bool_true) {
                    tsc_lock_release (info->data_lock);

                    TSC_DEBUG
                        ("tsc_csm_release_tunnel: tunnel is waiting for response [%p]",
                         info);

                    return tsc_bool_true;
                }
                else {
                    TSC_ERROR ("tsc_csm_release_tunnel: failed to release tunnel [%p]",
                               info);
                }
            }
            else {
                TSC_ERROR ("tsc_csm_release_tunnel: wrong state [state %d] [%p]", info->state_info.state, info);
		
            }

            tsc_lock_release (info->data_lock);
        }
        else {
            TSC_ERROR ("tsc_csm_release_tunnel: failed to get lock [%p][%p]",
                       tsc_lock_get_taker_thread(info->data_lock),
                       info);
        }
    }
    else {
        TSC_ERROR ("tsc_csm_release_tunnel: failed to release tunnel [%p]",
                   info);
    }

    return tsc_bool_false;
}

#ifdef TSC_MONITOR
void monitor_callback(tsc_network_data *network_data)
{
    tsc_notification_info *notification_info = (tsc_notification_info *)network_data->opaque;

    if (notification_info->enabled == tsc_bool_false) {
        free((void *)notification_info->internal_data);
        free((void *)notification_info);
    } else {
        tsc_csm_info *info = (tsc_csm_info *)network_data->tunnel_handle;

        tsc_notification_network_info_data data;
        memset(&data, 0, sizeof(tsc_notification_network_info_data));

        tsc_transport transport =
            info->tunnel_params.connection_params[info->connection_index].transport;

        uint8_t quality = 0;

        if (transport == tsc_transport_tcp || transport == tsc_transport_tls) {
            uint8_t value = (uint8_t)(TSC_NETWORK_QUALITY_RANGE * network_data->burst_1_ratio / 20.0);

            if (value > TSC_NETWORK_QUALITY_RANGE) {
                value = TSC_NETWORK_QUALITY_RANGE;
            }

            quality = TSC_NETWORK_QUALITY_RANGE - value;
        } else {
            uint8_t value = (uint8_t)(TSC_NETWORK_QUALITY_RANGE * network_data->loss_ratio / 20.0);

            if (value > TSC_NETWORK_QUALITY_RANGE) {
                value = TSC_NETWORK_QUALITY_RANGE;
            }

            quality = TSC_NETWORK_QUALITY_RANGE - value;
        }

        uint8_t i;

        tsc_notification_network_info_data *past_data = (tsc_notification_network_info_data *)
	    notification_info->internal_data;

        for (i = TSC_NETWORK_AVG_LEN - 1; i > 0; i--) {
            memcpy(past_data + i, past_data + i - 1, sizeof(tsc_notification_network_info_data));
        }

        past_data[0].quality = quality;

        uint8_t avg = 0;
        uint8_t count = 0;

        for (i = 0; i < TSC_NETWORK_AVG_LEN; i++) {
            avg += (TSC_NETWORK_AVG_LEN - i)*past_data[i].quality;
            count += (TSC_NETWORK_AVG_LEN - i);
        }

        data.quality = avg / count;

        if (notification_info->notification) {
            notification_info->notification_data.data = &data;
            notification_info->notification(&(notification_info->notification_data));
        }

        tsc_network_monitor (network_data->tunnel_handle, TSC_NETWORK_FRAME_SIZE, 
                             TSC_NETWORK_DURATION, (void *)notification_info,
                             monitor_callback);
    }    
}
#endif

tsc_notification_handle tsc_notification_enable (tsc_handle handle, tsc_notification type,
                                                 void (*notification)(tsc_notification_data *),
                                                 void *opaque)
{
    TSC_DEBUG("tsc_notification_enable: handle %p type %d cb %p opaque %p", handle, type, notification, opaque);

    tsc_csm_info *info = (tsc_csm_info *) handle;
    
    if (info) {
	tsc_notification_info *notification_info = (tsc_notification_info *)
	    malloc(sizeof(tsc_notification_info));

	memset(notification_info, 0, sizeof(tsc_notification_info));

	notification_info->notification_data.type = type;
	notification_info->notification_data.opaque = opaque;
	notification_info->notification_data.handle = handle;
	notification_info->notification = notification;
	notification_info->enabled = tsc_bool_true;

        switch (type) {
	  case tsc_notification_network_info:
#if 0
	      {
		  notification_info->internal_data = (void *)
		      malloc(sizeof(tsc_notification_network_info_data)*TSC_NETWORK_AVG_LEN);

		  memset(notification_info->internal_data, 0,
                         sizeof(tsc_notification_network_info_data)*TSC_NETWORK_AVG_LEN);

		  uint8_t i;
		  for (i = 0; i < TSC_NETWORK_AVG_LEN; i++) {
		      tsc_notification_network_info_data 
			  *data = (tsc_notification_network_info_data *)
			  notification_info->internal_data;

		      data[i].quality = TSC_NETWORK_QUALITY_RANGE;
		  }

		  tsc_network_monitor (handle, TSC_NETWORK_FRAME_SIZE, 
				       TSC_NETWORK_DURATION, (void *)notification_info,
				       monitor_callback);
	      }
#endif
	      break;
	  case tsc_notification_tunnel_socket_info:
	      {
		  if (!info->tunnel_socket_notification_info) {
		      info->tunnel_socket_notification_info = notification_info;
		      /* This is the first time. Invoke the callback to notify the current socket info */
		      tsc_csm_notify_tunnel_socket_info(info);
		  } else {
		      free(notification_info);
		      TSC_ERROR ("tsc_notification_enable: tunnel_socket notification already set [%p]", handle);

		      return NULL;
		  }

		  TSC_DEBUG ("tsc_notification_enable: notification type %d enabled [%p]", type, handle);

		  return notification_info;
	      }
	      break;
	  case tsc_notification_tunnel_termination_info:
	      {
		  if (!info->tunnel_termination_notification_info) {
		      info->tunnel_termination_notification_info = notification_info;
		  } else {
		      free(notification_info);
		      TSC_ERROR ("tsc_notification_enable: tunnel termination notification already set [%p]", handle);

		      return NULL;
		  }

		  TSC_DEBUG ("tsc_notification_enable: notification type %d enabled [%p]", type, handle);

		  return notification_info;
	      }
	      break;
	  case tsc_notification_socket_received:
	      {
		  tsc_socket_info *socket_info = info->socket_info;
		  tsc_socket_info *found = NULL;
		  int* fd = (int*)notification_info->notification_data.opaque;
		  while(socket_info) {
		      if(socket_info->fd == *fd)
			  found = socket_info;
		      break;
		      socket_info = socket_info->next;
		  }
	
	      
		  if(found) {
		      if (!(found->socket_received_info)) {
			  found->socket_received_info = notification_info;
		      } else {
			  free(notification_info);
			  TSC_ERROR ("tsc_notification_enable: data received notification already set [%p]", handle);                    
			  return NULL;
		      }
		  }
		  else {
		      free(notification_info);
		      TSC_ERROR ("tsc_notification_enable: Unable to find socket for fd [%d] [%p]", *fd, handle);
		      return NULL;
		  }
		  TSC_DEBUG ("tsc_notification_enable: notification type %d enabled [%p]", type, handle);
		  
		  return notification_info;            
	      }
	      break;
#ifdef TSC_ODD
	  case tsc_notification_odd:
	      {
		  if (!info->odd_notification_info) {
		      info->odd_notification_info = notification_info;
		  } else {
		      free(notification_info);
		      TSC_ERROR ("tsc_notification_enable: On Demand DTLS notification already set [%p]", handle);
		  
		      return NULL;
		  }
		  
		  TSC_DEBUG ("tsc_notification_enable: notification type %d enabled [%p]", type, handle);
		  
		  return notification_info;            
	      }
	      break;
#endif
#ifdef TSC_REDUNDANCY
	  case tsc_notification_redundancy:
	      {
		  if (!info->redundancy_notification_info) {
		      info->redundancy_notification_info = notification_info;
		  } else {
		      free(notification_info);
		      TSC_ERROR ("tsc_notification_enable: redundancy notification already set [%p]", handle);
		  
		      return NULL;
		  }
		  
		  TSC_DEBUG ("tsc_notification_enable: notification type %d enabled [%p]", type, handle);
		  
		  return notification_info;            
	      }
	      break;
#endif
	  default:
	    free(notification_info);
            TSC_ERROR ("tsc_notification_enable: notification type not supported [%p]", handle);
        }
    } else {
        TSC_ERROR ("tsc_notification_enable: failed to enable notifications [%p]", handle);
    }
    return NULL;
}

tsc_error_code tsc_notification_disable (tsc_notification_handle notification_handle)
{
    tsc_notification_info *info = (tsc_notification_info *) notification_handle;
    
    if (info) {
        info->enabled = tsc_bool_false;

#ifdef TS_REDUNDANCY
        tsc_notification_data *data = &(info->notification_data);

        if (data && data->type == tsc_notification_redundancy) {
            tsc_csm_info *tunnel_info = (tsc_csm_info *) data->handle;

            if (tunnel_info) {
                if(tunnel_info->redundancy_notification_info != NULL) {
                    free(tunnel_info->redundancy_notification_info);
                    tunnel_info->redundancy_notification_info = NULL;
                }
                free((void *)info);
            }
        }
#endif
        
        TSC_DEBUG ("tsc_notification_disable: notification type %d disabled [%p]", 
                   info->notification_data.type, info->notification_data.handle);

        return tsc_error_code_ok;
    } else {
        TSC_ERROR ("tsc_notification_disable: failed to disable notifications [%p]", info);
    }

    return tsc_error_code_error;
}
#ifdef TSC_ODD
tsc_bool tsc_csm_set_odd(tsc_csm_info * info, tsc_socket_info * socket,
			 tsc_ip_port_address * address,
			 tsc_bool set)
{
    
    if (info) {
        if (info->state_info.state != tsc_state_established && info->state_info.state != tsc_state_established_slow_poll) {
            TSC_ERROR ("tsc_csm_set_odd : wrong state [state %d] [%p]", 
                       info->state_info.state, info);   

            return tsc_bool_false;
        }

	
	tsc_csm_msg data;
        memset(&data, 0, sizeof(tsc_csm_msg));
        data.msg_type = tsc_csm_msg_type_service_request;
        data.timestamp = tsc_get_clock();
        data.opaque = (void *)socket;

        if (set == tsc_bool_true)  {
            data.info.service_request.
		service_type = TSC_TLV_SERVICE_TYPE_ENABLE_ODD;
        } else {
            data.info.service_request.
		service_type = TSC_TLV_SERVICE_TYPE_DISABLE_ODD;
        }

	tsc_transport transport = info->tunnel_params.connection_params[info->connection_index].transport;
	
        data.info.service_request.service_request.
	    odd.connection_info_use = tsc_tlv_connection_info_use_ipv4;

        if ((transport == tsc_transport_udp) || (transport == tsc_transport_dtls)) {
          
	    TSC_ERROR ("tsc_csm_set_odd:service not available for transport type [%d] [%p]",transport,
                       info);
	    return tsc_bool_false;
	    
	}
	
	memcpy(&(data.info.service_request.service_request.
		 odd.connection_info.connection_info_ipv4.
		 src_address), address, sizeof(tsc_ip_port_address));

        if (tsc_csm_write_in_msg (info, &data) == tsc_error_code_ok) {
            TSC_DEBUG ("tsc_csm_set_odd:odd request queued ok [%p]",
                       info);

            return tsc_bool_true;
        }
           
    }
    
    TSC_ERROR ("tsc_csm_set_odd: failed to send request [%p]", info);
    
    return tsc_bool_false;
}
#endif

