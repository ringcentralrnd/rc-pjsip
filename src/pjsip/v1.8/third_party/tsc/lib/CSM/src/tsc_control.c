/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#define TSC_LOG_SUBSYSTEM TSC_LOG_SUBSYSTEM_STATE_MACHINE

#include "tsc_control_api.h"
#include "tsc_data.h"


tsc_error_code
tsc_ctrl_init ()
{
    if (tsc_socket_init () == tsc_error_code_ok) {
        return tsc_init ();
    }

    TSC_ERROR ("tsc_ctrl_init: failed to init sockets");

    return tsc_error_code_error;
}

tsc_error_code
tsc_ctrl_delete_tunnel (tsc_handle handle)
{
    return tsc_delete_tunnel (handle);
}

tsc_handle
tsc_ctrl_new_tunnel (tsc_tunnel_params * tunnel_params,
                     tsc_requested_config * requested_config)
{
    tsc_handle handle = tsc_new_tunnel (tunnel_params, requested_config);

    time_t start = tsc_time ();

    if (handle) {
        for (;;) {
            tsc_state_info state_info;
            tsc_get_state (handle, &state_info);

            if (state_info.state == tsc_state_established
                || state_info.state == tsc_state_established_slow_poll) {
                TSC_DEBUG ("tsc_ctrl_new_tunnel: tunnel was setup [%p]",
                           handle);

                break;
            }
            else if (tsc_time () - start > TSC_NEW_TUNNEL_CONNECT_TIMEOUT) {
                TSC_ERROR ("tsc_ctrl_new_tunnel: failed to setup tunnel [%p]",
                           handle);

                tsc_delete_tunnel (handle);

                handle = NULL;

                break;
            }

            /* let's wait for 10 ms before polling again */
            tsc_sleep (10);
        }
    }

    return handle;
}

tsc_error_code
tsc_ctrl_get_config (tsc_handle handle, tsc_config * config)
{
    return tsc_get_config (handle, config);
}

tsc_error_code
tsc_ctrl_get_state (tsc_handle handle, tsc_state_info * state_info)
{
    return tsc_get_state (handle, state_info);
}
