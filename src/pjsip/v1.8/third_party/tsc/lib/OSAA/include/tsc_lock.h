/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#ifndef __TSC_LOCK_H__
#define __TSC_LOCK_H__

#include "tsc_common.h"
#include "tsc_utils.h"

#define TSC_LOCK_TIMEOUT 100

typedef enum
{
    tsc_lock_response_ok = 0,
    tsc_lock_response_already_taken,
    tsc_lock_response_error
} tsc_lock_response;

typedef struct
{
#ifdef TSC_LINUX
    pthread_mutex_t mutex;
    pthread_t taker_thread;
#elif TSC_WINDOWS
    HANDLE mutex;
    DWORD taker_thread;
#endif

    tsc_bool taken;
    void *opaque;
} tsc_lock;

tsc_lock *tsc_lock_new ();
tsc_lock_response tsc_lock_get (tsc_lock * lock);
tsc_lock_response tsc_lock_release (tsc_lock * lock);
tsc_lock_response tsc_lock_delete (tsc_lock * lock);

void *tsc_lock_get_taker_thread (tsc_lock *lock);

#endif /* __TSC_LOCK_H__ */
