/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#include "tsc_thread.h"

tsc_thread *
tsc_thread_new (void *start_address, void *opaque)
{
    tsc_thread *thread = (tsc_thread *) malloc (sizeof (tsc_thread));


    if (thread) {
        memset (thread, 0, sizeof (tsc_thread));

        thread->start_address = start_address;
        thread->opaque = opaque;

#ifdef TSC_LINUX
        pthread_attr_t attr;
        if (!pthread_attr_init (&attr)) {
            struct sched_param param;

            if (!pthread_attr_getschedparam (&attr, &param)) {
                param.sched_priority = sched_get_priority_max (SCHED_FIFO);

                pthread_attr_setschedparam (&attr, &param);
            }
        }

        thread->handle = (pthread_t) NULL;

        pthread_create (&(thread->handle), &attr,
                        (void *(*)(void *)) start_address, opaque);

        pthread_attr_destroy (&attr);
#elif TSC_WINDOWS
        thread->handle =
            CreateThread (NULL, TSC_THREAD_STACK,
                          (unsigned long (__stdcall *) (void *))
                          start_address, opaque, 0, &(thread->thread_id));
#endif
    }

    return thread;
}

tsc_thread_response
tsc_thread_finish (tsc_thread * thread)
{
    if (thread) {
#ifdef TSC_LINUX
        pthread_join (thread->handle, NULL);
#elif TSC_WINDOWS
        WaitForSingleObject (thread->handle, INFINITE);
#endif

        return tsc_thread_response_ok;
    }

    return tsc_thread_response_error;
}

tsc_thread_response
tsc_thread_delete (tsc_thread * thread)
{
    if (thread) {
        free ((void *) thread);

        return tsc_thread_response_ok;
    }

    return tsc_thread_response_error;
}
