/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*
*/

/** @file
 * This file is part of TSC Client SDK
 *
 */

#ifdef TSC_FTCP

#include "tsc_raw.h"

size_t
tsc_raw_if_size ()
{
    pcap_if_t *all_devs = NULL;

    char err_buf[PCAP_ERRBUF_SIZE];

    size_t i = 0;

    if (!pcap_findalldevs (&all_devs, err_buf)) {
        pcap_if_t *tmp = all_devs;

        while (tmp) {
            tmp = tmp->next;

            i++;
        }

        if (all_devs) {
            pcap_freealldevs (all_devs);
        }
    }

    return i;
}

tsc_raw_response
tsc_raw_if_device (size_t idx, char *device)
{
    if (device) {
        pcap_if_t *all_devs = NULL;

        char err_buf[PCAP_ERRBUF_SIZE];

        size_t i = 0;

        if (!pcap_findalldevs (&all_devs, err_buf)) {
            pcap_if_t *tmp = all_devs;

            tsc_raw_response response = tsc_raw_response_not_found;

            while (tmp) {
                if (idx == i) {
                    strcpy (device, tmp->name);

                    response = tsc_raw_response_ok;

                    break;
                }

                tmp = tmp->next;

                i++;
            }

            if (all_devs) {
                pcap_freealldevs (all_devs);
            }

            return response;
        }
    }

    return tsc_raw_response_error;
}

void
tsc_pcap_callback (u_char * arg, const struct pcap_pkthdr *header,
                   const u_char * packet)
{
    tsc_raw *raw = (tsc_raw *) arg;

    if (tsc_lock_get (raw->in_lock) == tsc_lock_response_ok) {
        tsc_buffer msg;

        msg.len = header->len;

        if (msg.len < TSC_MAX_FRAME_SIZE) {
            memcpy (msg.data, packet, msg.len);

            if (!raw->filter || (raw->filter (&msg) == tsc_bool_true)) {
                tsc_queue_write (raw->in_queue, &msg);
            }
        }

        tsc_lock_release (raw->in_lock);
    }
}

tsc_raw_response
tsc_raw_read (tsc_raw * raw_if, tsc_buffer * buffer)
{
    if (raw_if && buffer) {
        if (tsc_lock_get (raw_if->in_lock) == tsc_lock_response_ok) {
            tsc_buffer msg;

            if (tsc_queue_read (raw_if->in_queue, &msg) ==
                tsc_queue_response_ok) {
                memcpy (buffer, &msg, sizeof (tsc_buffer));

                tsc_lock_release (raw_if->in_lock);

                return tsc_raw_response_ok;
            }
            else {
                tsc_lock_release (raw_if->in_lock);

                return tsc_raw_response_no_data;
            }
        }
    }

    return tsc_raw_response_error;
}

void *
tsc_raw_thread (void *arg)
{
    tsc_raw *raw = (tsc_raw *) arg;

    pcap_loop (raw->handler, -1, tsc_pcap_callback, (u_char *) arg);

    return 0L;
}

tsc_raw *
tsc_raw_open (char *device, tsc_bool (*filter) (tsc_buffer *))
{
    tsc_raw *raw = (tsc_raw *) malloc (sizeof (tsc_raw));

    if (raw) {
        memset (raw, 0, sizeof (tsc_raw));

        char err_buf[PCAP_ERRBUF_SIZE];

        raw->handler = pcap_open_live (device, BUFSIZ, 1, 1, err_buf);

        if (!raw->handler) {
            free (raw);

            raw = NULL;
        }
        else {
            raw->processing_thread =
                tsc_thread_new ((void *) tsc_raw_thread, raw);

            if (!raw->processing_thread) {
                pcap_breakloop (raw->handler);

                pcap_close (raw->handler);

                free (raw);

                raw = NULL;
            }
            else {
                raw->in_lock = tsc_lock_new ();

                if (!raw->in_lock) {
                    tsc_thread_finish (raw->processing_thread);

                    pcap_breakloop (raw->handler);

                    pcap_close (raw->handler);

                    free (raw);

                    raw = NULL;
                }
                else {
                    raw->in_queue =
                        tsc_queue_new (TSC_RAW_QUEUE_SIZE,
                                       sizeof (tsc_buffer));

                    if (!raw->in_queue) {
                        tsc_lock_delete (raw->in_lock);

                        tsc_thread_finish (raw->processing_thread);

                        pcap_breakloop (raw->handler);

                        pcap_close (raw->handler);

                        free (raw);

                        raw = NULL;
                    }

                    raw->filter = filter;
                }
            }
        }
    }

    return raw;
}

tsc_raw_response
tsc_raw_close (tsc_raw * raw_if)
{
    if (raw_if) {
        tsc_queue_delete (raw_if->in_queue);

        tsc_lock_delete (raw_if->in_lock);

        pcap_breakloop (raw_if->handler);

        pcap_close (raw_if->handler);

        tsc_thread_finish (raw_if->processing_thread);

        free (raw_if);

        return tsc_raw_response_ok;
    }

    return tsc_raw_response_error;
}

tsc_raw_response
tsc_raw_write (tsc_raw * raw_if, tsc_buffer * buffer)
{
    if (raw_if && buffer) {
        if (!pcap_sendpacket (raw_if->handler, buffer->data, buffer->len)) {
            return tsc_raw_response_ok;
        }
    }

    return tsc_raw_response_error;
}

tsc_raw_response
tsc_raw_ftcp_send (tsc_raw * raw_if, tsc_ip_port_address * src_addr,
                   tsc_ip_port_address * dst_addr, tsc_buffer * buffer)
{
    if (raw_if) {
        uint8_t data[TSC_MAX_FRAME_SIZE];
        memset (data, 0, TSC_MAX_FRAME_SIZE);

        uint32_t size = buffer->len;

        memcpy ((char *) data + TSC_ETHER_HEADER_SIZE + TSC_IP_HEADER_SIZE +
                TSC_TCP_HEADER_SIZE + TSC_UDP_HEADER_SIZE, buffer->data,
                size);

        uint16_t src_port = src_addr->port;
        uint16_t dst_port = dst_addr->port;

        /* extended payload now includes UDP header */
        if (tsc_udp_make
            (src_port, dst_port,
             data + TSC_ETHER_HEADER_SIZE + TSC_IP_HEADER_SIZE +
             TSC_TCP_HEADER_SIZE,
             data + TSC_ETHER_HEADER_SIZE + TSC_IP_HEADER_SIZE +
             TSC_TCP_HEADER_SIZE, size) == tsc_error_code_ok) {
            size += TSC_UDP_HEADER_SIZE;

            if (tsc_tcp_make
                (src_addr, dst_addr,
                 data + TSC_ETHER_HEADER_SIZE + TSC_IP_HEADER_SIZE,
                 data + TSC_ETHER_HEADER_SIZE + TSC_IP_HEADER_SIZE +
                 TSC_TCP_HEADER_SIZE, size) == tsc_error_code_ok) {
                tsc_ip_address src;
                tsc_ip_address dst;

                src.address = src_addr->address;
                dst.address = dst_addr->address;

                if (tsc_ip_make
                    (&src, &dst, SOL_TCP, data + TSC_ETHER_HEADER_SIZE,
                     data + TSC_ETHER_HEADER_SIZE + TSC_IP_HEADER_SIZE, 0,
                     size + TSC_TCP_HEADER_SIZE) == tsc_error_code_ok) {
                    if (tsc_ether_make (&src, &dst, ETHERTYPE_IP, data) ==
                        tsc_error_code_ok) {
                        tsc_buffer buffer;
                        buffer.len =
                            TSC_ETHER_HEADER_SIZE + TSC_IP_HEADER_SIZE +
                            TSC_TCP_HEADER_SIZE + size;

                        memcpy (buffer.data, data, buffer.len);

                        tsc_raw_write (raw_if, &buffer);

                        return tsc_raw_response_ok;
                    }
                }
            }
        }
    }

    return tsc_raw_response_error;
}

tsc_raw_response
tsc_raw_ftcp_recv (tsc_raw * raw_if, tsc_ip_port_address * src,
                   tsc_ip_port_address * dst, tsc_buffer * buffer)
{
    if (raw_if) {
        tsc_buffer pkt;
        if (tsc_raw_read (raw_if, &pkt) == tsc_raw_response_ok) {
            /* let's get IP layer */
            tsc_ip_address src_addr;
            tsc_ip_address dst_addr;
            uint8_t protocol;

            if (tsc_ip_parse
                (&src_addr, &dst_addr, &protocol,
                 pkt.data + TSC_ETHER_HEADER_SIZE) == tsc_error_code_ok) {
                if (protocol == SOL_TCP) {
                    uint32_t tcp_src_port;
                    uint32_t tcp_dst_port;

                    if (tsc_tcp_parse
                        (&tcp_src_port, &tcp_dst_port,
                         pkt.data + TSC_ETHER_HEADER_SIZE +
                         TSC_IP_HEADER_SIZE) == tsc_error_code_ok) {
                        uint32_t udp_src_port;
                        uint32_t udp_dst_port;

                        if (tsc_udp_parse
                            (&udp_src_port, &udp_dst_port,
                             pkt.data + TSC_ETHER_HEADER_SIZE +
                             TSC_IP_HEADER_SIZE + TSC_TCP_HEADER_SIZE)
                            == tsc_error_code_ok) {
                            if (udp_src_port == tcp_src_port
                                && udp_dst_port == tcp_dst_port) {

                                src->address = src_addr.address;
                                dst->address = dst_addr.address;
                                src->port = tcp_src_port;
                                dst->port = tcp_dst_port;

                                memset (buffer, 0, sizeof (tsc_buffer));

                                if (pkt.len >=
                                    (TSC_ETHER_HEADER_SIZE +
                                     TSC_IP_HEADER_SIZE +
                                     TSC_TCP_HEADER_SIZE +
                                     TSC_UDP_HEADER_SIZE)) {
                                    buffer->len =
                                        pkt.len - (TSC_ETHER_HEADER_SIZE +
                                                   TSC_IP_HEADER_SIZE +
                                                   TSC_TCP_HEADER_SIZE +
                                                   TSC_UDP_HEADER_SIZE);

                                    memcpy (buffer->data,
                                            pkt.data +
                                            (TSC_ETHER_HEADER_SIZE +
                                             TSC_IP_HEADER_SIZE +
                                             TSC_TCP_HEADER_SIZE +
                                             TSC_UDP_HEADER_SIZE),
                                            buffer->len);

                                    return tsc_raw_response_ok;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return tsc_raw_response_error;
}

tsc_raw_response
tsc_raw_ready_to_read (tsc_raw * raw_if)
{
    if (raw_if) {
        if (tsc_lock_get (raw_if->in_lock) == tsc_lock_response_ok) {
            if (raw_if->in_queue->gap > 0) {
                tsc_lock_release (raw_if->in_lock);

                return tsc_raw_response_ok;
            }
            else {
                tsc_lock_release (raw_if->in_lock);

                return tsc_raw_response_no_data;
            }
        }
    }

    return tsc_raw_response_error;
}

#endif
