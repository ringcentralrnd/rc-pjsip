/** @defgroup TSCSocketAPI
*
* Socket API suited to TSC
*/

/** @defgroup TSCTunnelManagementAPI
*
* Defines functions for controlling and managing low level tunnel 
*/

/** @defgroup TSCServiceAPI
*
* Defines service related functions- logging, utilities etc.
*/
