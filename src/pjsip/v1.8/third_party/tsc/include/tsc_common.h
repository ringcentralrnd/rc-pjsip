/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/
/*! \brief 
Common Definitions
*/


/** @file
 * This file is part of TSC Client API
 * and defines common utilities
 *
 */

#ifndef __TSC_COMMON_H__
#define __TSC_COMMON_H__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <stdarg.h>
#include <sys/types.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include "tsc_config.h"

#ifdef TSC_LINUX
#include <sys/socket.h>
#ifndef TSC_IOS
#ifndef TSC_ANDROID
#include <net/ethernet.h>
#endif
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>
#endif
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <net/if.h>
#ifndef TSC_IOS
#include <net/if_arp.h>
#endif
#ifdef TSC_FTCP
#include <pcap.h>
#endif
#endif

#ifdef TSC_ANDROID
#include <android/log.h>
#include "jni.h"
#endif

#ifdef TSC_WINDOWS
#include <ws2tcpip.h>
#include <iphlpapi.h>
#endif

#define TSC_ETHER_HDR_LEN 14

#ifndef EOK
#define EOK 0
#endif

#define TSC_VERSION 1

#define TSC_MAX_STR_LEN 0x100

#ifndef SOL_ICMP
#define SOL_ICMP 1
#endif

#define TSC_MIN(X,Y) ((X) < (Y)) ? (X) : (Y)

#if defined(TSC_WINDOWS) || defined(TSC_IOS) || defined(TSC_ANDROID)

#if !defined(TSC_MINGW) && !defined(TSC_IOS) && !defined(TSC_ANDROID)
typedef int ssize_t;
#endif

#if defined(TSC_IOS) && !defined(TSC_ANDROID)
#define TCP_NODELAY 1
#endif

#ifndef TSC_ANDROID
#define CPLUSSTART "extern "C" {"
#define CPLUSEND }
#endif

#ifdef TSC_MINGW
#define EWOULDBLOCK WSAEWOULDBLOCK
#define EINPROGRESS WSAEINPROGRESS
#define ETIMEDOUT WSAETIMEDOUT
#define ECONNRESET WSAECONNRESET
#define EADDRINUSE WSAEADDRINUSE
#endif

#define ETH_ALEN 6

#define	ETHERTYPE_IP 0x0800

/*! \struct ether_header
* ethernet header
*/
struct ether_header
{
  uint8_t ether_dhost[ETH_ALEN];	/* destination eth addr	*/
  uint8_t ether_shost[ETH_ALEN];	/* source ether addr	*/
  uint16_t ether_type;		        /* packet type ID field	*/
};

#ifndef TSC_ANDROID
/*! \struct iphdr
* IP header
*/
struct iphdr
{
    uint8_t ihl:4;
    uint8_t version:4;
    uint8_t tos;
    uint16_t tot_len;
    uint16_t id;
    uint16_t frag_off;
    uint8_t ttl;
    uint8_t protocol;
    uint16_t check;
    uint32_t saddr;
    uint32_t daddr;
};

/*! \struct udphdr
* UDP header
*/
struct udphdr
{
    uint16_t source;
    uint16_t dest;
    uint16_t len;
    uint16_t check;
};

#ifdef TSC_IOS
/*! \struct tcphdr
* tcp header
*/
struct tcphdr {
        unsigned short source;
        unsigned short dest;
        unsigned long seq;
        unsigned long ack_seq;       
        #  if __BYTE_ORDER == __LITTLE_ENDIAN
        unsigned short res1:4;
        unsigned short doff:4;
        unsigned short fin:1;
        unsigned short syn:1;
        unsigned short rst:1;
        unsigned short psh:1;
        unsigned short ack:1;
        unsigned short urg:1;
        unsigned short res2:2;
        #  elif __BYTE_ORDER == __BIG_ENDIAN
        unsigned short doff:4;
        unsigned short res1:4;
        unsigned short res2:2;
        unsigned short urg:1;
        unsigned short ack:1;
        unsigned short psh:1;
        unsigned short rst:1;
        unsigned short syn:1;
        unsigned short fin:1;
        #  endif
        unsigned short window;       
        unsigned short check;
        unsigned short urg_ptr;
};
#endif

#ifndef SOL_UDP
#define SOL_UDP 17
#endif

#ifndef SOL_TCP
#define SOL_TCP 6
#endif

#ifndef F_GETFL
#define F_GETFL 3
#endif

#ifndef F_SETFL
#define F_SETFL 4
#endif

#ifndef O_NONBLOCK
#define O_NONBLOCK 0x4000
#endif
#endif
#endif

/* For now simple IPv4 address:port */
/*! \struct tsc_ip_port_address
* generic IP address including port
*/
/*! \brief
* IPv4 address:port
*/
typedef struct
{
    uint32_t address;
    uint16_t port;
} tsc_ip_port_address;

/* For now simple IPv4 address */
/*! \struct tsc_ip_address
* generic IP address
*/
/*! \brief
* IPv4 address 
*/
typedef struct
{
    uint32_t address;
} tsc_ip_address;

/* For now simple IPv4 mask*/
/*! \struct tsc_ip_mask
* generic IP mask
*/
/*! \brief
* IPv4 mask
*/
typedef struct
{
    uint32_t mask;
} tsc_ip_mask;

/*! \struct tsc_error_code
* error codes
*/
/*! \brief
* generic error codes
*/
typedef enum
{
    tsc_error_code_ok = 0,
    tsc_error_code_error,
    tsc_error_code_not_logged,
    tsc_error_code_cannot_connect,
    tsc_error_code_cannot_configure,
    tsc_error_code_keepalive_failure,
    tsc_error_code_service_failure,
    tsc_error_code_cannot_recv_data,
    tsc_error_code_no_data,
    tsc_error_code_cannot_send_data,
    tsc_error_code_cannot_authenticate,
    tsc_error_code_cannot_release,
    tsc_error_code_queue_overflow
} tsc_error_code;

/*! \typedef tsc_bool
*/
typedef enum
{
    tsc_bool_false = 0,
    tsc_bool_true = 1
} tsc_bool;

#define TSC_MAX_FRAME_SIZE 3000
#define HTTP_PROXY_MAX_MSG_SIZE 5000

/*! \struct tsc_buffer
* generic buffer with max frame size =3000
*/
/*! \brief
* generic buffer
*/
typedef struct
{
    uint8_t data[TSC_MAX_FRAME_SIZE];
    uint32_t len;
} tsc_buffer;

/*! \struct tsc_tunnel_id
* low and high bits for 64-bit tunnel id
*/
/*! \brief
* tunnel id
*/
typedef struct
{
    uint32_t hi;
    uint32_t lo;
} tsc_tunnel_id;

/*! \struct http_proxy_buffer
* generic buffer for proxy messages with max frame size =5000
*/
/*! \brief
* generic buffer for proxy messages
*/
typedef struct
{
    uint8_t data[HTTP_PROXY_MAX_MSG_SIZE];
    uint32_t len;
} http_proxy_buffer;

/*! \typedef tsc_transport
* transport options tcp, tls, dtls
*/
typedef enum
{
#ifdef TSC_FTCP
    tsc_transport_ftcp = 0,
#endif
    tsc_transport_udp = 1,
    tsc_transport_tcp = 2,
    tsc_transport_tls = 3,
    tsc_transport_dtls = 4
} tsc_transport;

/* For now simple IPv4 address:port */
/*! \struct tsc_ip_port_address_prot
* generic IP address including port
*/
/*! \brief
* IPv4 address:port and protocol
*/
typedef struct
{
    uint32_t address;
    uint16_t port;
    tsc_transport protocol;
} tsc_ip_port_address_prot;

#endif /* __TSC_COMMON_H__ */

