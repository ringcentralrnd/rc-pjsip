/* 
   Copyright (c) 2011, Acme Packet, Inc.

   All rights reserved.  Redistribution and use in source and binary forms, 
   with or without modification, are permitted provided that the following
   conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Acme Packet, Inc. nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
   * If file revision information is present, it must be preserved.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL ACME PACKET, INC. BE LIABLE FOR ANY 
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/
/*! \brief
low level tunnel API
*/

/** @file
 * This file is part of TSC Client API
 * and defines low level tunnel API
 *
 */
#include "tsc_common.h"
#include "tsc_utils.h"
#include "tsc_log.h"
#include "tsc_ssl_api.h"


#ifndef __TSC_TUNNEL_H__
#define __TSC_TUNNEL_H__

#ifdef __cplusplus
extern "C" {
#endif
#define TSC_PROXY_USERNAME_LEN 0x100
#define TSC_PROXY_PASSWORD_LEN 0x100

#ifdef TSC_REDUNDANCY
typedef enum
{
    tsc_so_redundancy_method_udp_dtls = 0,
    tsc_so_redundancy_method_tcp_tls_fan_out,
    tsc_so_redundancy_method_tcp_tls_load_balance,
} tsc_so_redundancy_method;

typedef struct
{
    uint8_t redundancy_factor;
    tsc_so_redundancy_method redundancy_method;
} tsc_so_redundancy;
#endif

/*! \typedef tsc_handle
* socket handle
*/
typedef void *tsc_handle;

/*! \struct tsc_config
* structure for tunnel configuration
* Filled by server.
*/
/*! \brief
* tunnel config
*/
typedef struct
{
    tsc_ip_address internal_address;
    tsc_ip_mask mask;
    tsc_ip_port_address sip_server;
    uint32_t keepalive_interval;
    uint32_t supported_version_id;
    uint32_t mtu;
    uint32_t sequence;
    tsc_tunnel_id tunnel_id;
} tsc_config;

/*! \typedef tsc_state
* possible socket states
*/
typedef enum
{
    tsc_state_disconnected = 0,
    tsc_state_connecting,
    tsc_state_proxy_connecting, /* state reached only if proxy used */
#ifdef TSC_OPENSSL
    tsc_state_ssl_connecting,
#endif
    tsc_state_negotiating,
    tsc_state_established,
    tsc_state_established_slow_poll,
    tsc_state_fatal_error       /* state reached if anything fails- invalid
                                   message from client, server dead ,
                                   keepalive time exhaussted etc. */
} tsc_state;

typedef void *tsc_notification_handle;

/*! \typedef tsc_notification
* notification types
*/
typedef enum
{
    tsc_notification_network_info = 0,
    tsc_notification_tunnel_socket_info,
    tsc_notification_tunnel_termination_info,
    tsc_notification_socket_received,
#ifdef TSC_ODD
    tsc_notification_odd,
#endif
#ifdef TSC_REDUNDANCY
    tsc_notification_redundancy
#endif
} tsc_notification;

/*! \struct tsc_notification_network_info_data
* network info notification data
*/
/*! \brief
* network info notification data
*/
typedef struct
{
    uint8_t quality;
} tsc_notification_network_info_data;

/*! \struct tsc_notification_termination_info_data
* network info termination data
*/
/*! \brief
* network info termination data
*/
typedef struct
{
    tsc_config config;
} tsc_notification_termination_info_data;
/*! \struct tsc_notification_socket_received_data
*  socket data received data
*/
/*! \brief
*  socket data received data
*/  
typedef struct
{
    tsc_bool available;
    int socket;
    uint32_t size;
    
} tsc_notification_socket_received_data;
#ifdef TSC_REDUNDANCY
/*! \struct tsc_notification_redundancy_info_data
* redundancy info notification data
*/
/*! \brief
* redundancy info notification data
*/
typedef struct
{
    tsc_bool enabled;
    tsc_bool available;
    int socket;    
    tsc_so_redundancy options;    
} tsc_notification_redundancy_info_data;
#endif

#ifdef TSC_ODD
/*! \struct tsc_notification_odd_info_data
* on demand dtls info notification data
*/
/*! \brief
 * on demand dtls notification data*/
typedef struct
{
    tsc_bool enabled;
    tsc_bool available;
    int socket; 
} tsc_notification_odd_info_data;
#endif

/*! \struct tsc_notification_data
* tunnel notification data
*/
/*! \brief
* notification data
*/
typedef struct
{
    tsc_handle handle;
    tsc_notification type;
    void *opaque;
    void *data;
} tsc_notification_data;

/*! \struct tsc_state_info
* tunnel state machine state information
*/
/*! \brief
* machine state info 
*/
typedef struct
{
    tsc_state state;
    tsc_error_code error;
} tsc_state_info;

/* Initially no parameters can be requested by tunnel client */
/*! \struct tsc_requested_config
* tunnel configuration to be requested.
* Initially no paramaters can be requested by tunnel client
* These values are received from the server in a configuration-response to a valid configuration request
*/
/*! \brief
* requested tunnel config 
*/
typedef struct
{
    /* TBD */
    tsc_tunnel_id tunnel_id;
} tsc_requested_config;


/*! \typedef tsc_transport_buffering
* to decide if transport buffering is allowed
*/
typedef enum
{
    tsc_transport_buffering_disabled = 0,
    tsc_transport_buffering_enabled
} tsc_transport_buffering;

/*! \struct tsc_http_proxy_config
* proxy information, if proxy is used to establish tunnel
*/
typedef struct
{
    tsc_bool proxy_enabled;
    tsc_ip_port_address proxy_address;
    char username[TSC_PROXY_USERNAME_LEN];
    char password[TSC_PROXY_PASSWORD_LEN];
} tsc_http_proxy_config;

/*! \struct tsc_output_option
* processing for output packets
*/
typedef struct
{
    tsc_bool realtime;
    uint8_t tos;
#ifdef TSC_REDUNDANCY
    tsc_so_redundancy redundancy;
#endif
#ifdef TSC_ODD
    tsc_bool odd;
#endif
    tsc_bool slow_idle_poll;
    void *private_data;
} tsc_output_option;

/*! \struct tsc_connection_params
* tunnel connection configuration
*/
/*! \brief
* connection config
*/
typedef struct
{
    tsc_ip_port_address server_address;
    tsc_ip_port_address nat_ipport;
    char devname[TSC_ADDR_STR_LEN];
    tsc_bool choose_device;
    tsc_transport transport;
    tsc_security_config sec_config;
    tsc_transport_buffering buffering;
    tsc_http_proxy_config proxy_config;
    uint8_t tos;
#ifdef TSC_IOS
    tsc_bool background_mode;
#endif
} tsc_connection_params;
/*! \struct tsc_connection_params
* tunnel socket local and remote address
*/
/*! \brief
 * tunnel socket information*/
typedef struct
{
    tsc_ip_port_address local_address;
    tsc_ip_port_address remote_address;
    tsc_ip_port_address nat_ipport;
    tsc_transport transport;
    uint32_t connection_index;
} tsc_tunnel_socket_info;
    
    
#ifdef TSC_PCAP_CAPTURE
#define TSC_PCAP_CAPTURE_FILENAME_LEN 0x100

/*! \struct tsc_pcap_capture
* pcap capture parameters
*/
/*! \brief
* pcap capture parameters
*/
    typedef struct
    {
        tsc_bool enabled;
        char filename[TSC_PCAP_CAPTURE_FILENAME_LEN];
    } tsc_pcap_capture;
#endif

/*! struct tsc_statistics
tunnel statistics struct
*/
/*! \brief
 * statistics
 */
typedef struct
{
    uint32_t sent_bytes;
    uint32_t recv_bytes;
    uint32_t socket_count;
    uint32_t sockets_created;
    uint32_t max_queue_gap;        /* incremented whenever the send queue has
				      more than one packet to send */
    uint32_t dropped_in_packets;   /* number of packets dropped due to queue
				      overflow that are destined for the SBC */
    uint32_t dropped_out_packets;   /* number of packets dropped due to queue
				      overflow from the SBC */

    uint32_t reconn_attempts;
    uint32_t keep_alive_count;  /* keep alives sent */
    uint32_t service_request_count;

    uint32_t in_packet_count;
    uint32_t min_in_processing;
    uint32_t avg_in_processing;
    uint32_t max_in_processing;

    uint32_t out_packet_count;
    uint32_t min_out_processing;
    uint32_t avg_out_processing;
    uint32_t max_out_processing;

} tsc_statistics;



    /* ! \struct tsc_tunnel_params * structure to hold connection
       configuration for multiple connections */
typedef struct
{
    tsc_connection_params connection_params[TSC_MAX_CONNECTION_PARAMS];
    uint32_t max_connections;
#ifdef TSC_PCAP_CAPTURE
    tsc_pcap_capture pcap_capture;
#endif
} tsc_tunnel_params;

/*///////////////////////////////////////////////////////////////////////
// low level tunnel API
////////////////////////////////////////////////////////////////////////
*/
/** @addtogroup TSCTunnelManagementAPI **/
/**@{*/

/* Initialize API */
/*!
* Purpose: low level tunnel initialization \n
* this would allocate a socket  in the socket table for this connection
@return \n Type: \ref tsc_error_code. \n Return tsc_error_code_ok if intialization succesful, else return tsc_error_code_error
*/
tsc_error_code tsc_init ();

/* Create a tunnel and return the tunnel handle for later reference*/
/*!
* Purpose: low level tunnel creation \n
* Create a tunnel and return the tunnel handle for later reference. \n
* The requested config for the tunnel is obtained froma configuration-response message sent by the server
@param tunnel_params \n Type: tsc_tunnel_params variable.\n connection config for the new tunnel 
@param requested_config \n Type: tsc_requested_config variable. \n requested tunnel config, value changes on return
@return  \n Type: tsc_handle. \n handle to the established tunnel( with info about connection times/state of tunnel/read, write queues etc.)
*/
tsc_handle tsc_new_tunnel (tsc_tunnel_params * tunnel_params,
                           tsc_requested_config * requested_config);

/* Return state of client tunnel state-machine
could be used to get current state info from the handle info being passed*/
/*!
* Purpose: low level return tunnel state \n
* Return state of client tunnel state-machine \n
* could be used to get current state info from the handle info being passed
@param handle \n Type: \ref tsc_handle. \n handle for which state info is required
@param state_info \n Type: tsc_state_info variable. \n state of tunnel connection(connected/negotiated/established/keepalive), value changes on return.
@return  \n Type: tsc_error_code. \n Return tsc_error_code_ok if state retrieved, else return tsc_error_code_error
*/
tsc_error_code tsc_get_state (tsc_handle handle, tsc_state_info * state_info);

/* Get client tunnel configuration information (after tunnel is established) 
* could be used to get configuration information from the handle info being passed*/
/*!
* Purpose: low level get tunnel config \n
* Get client tunnel configuration information (after tunnel is established) \n
* could be used to get configuration information from the handle info being passed
@param handle \n Type: tsc_handle. \n handle for which config is required
@param config \n Type: tsc_config variable. \n tunnel config, value changes on return
@return  \n Type: tsc_error_code. \n Return tsc_error_code_ok if config retrieved, else return tsc_error_code_error
*/
tsc_error_code tsc_get_config (tsc_handle handle, tsc_config * config);

/*Get client tunnel statistics information. Populates the passed statistics structure.*/
/*!
* Purpose: client statistics \n
* Get statistics on tunnel usage.
@param handle \n Type: tsc_handle. \n handle for tunnel
@param stats \n Type: tsc_statistics. \n struct to be filled
@return  \n Type: tsc_error_code. \n Return tsc_error_code_ok if stats retrieved, else return tsc_error_code_error
*/
tsc_error_code tsc_get_stats (tsc_handle handle, tsc_statistics * stats);

/* Return the wake up interval for this tunnel. This is intended for devices
 * that go to sleep and need to be restarted to continue processing messages.*/
/*!
* Purpose: low level return wakeup interval \n
* Return wakeup interval of tunnel \n
@param handle \n Type: \ref tsc_handle. \n handle for which state info is required
@param wakeup_interval \n Type: uint32_t variable. \n seconds to wakeup application.
@return  \n Type: tsc_error_code. \n Return tsc_error_code_ok if wakeup interval retrieved, else return tsc_error_code_error
*/
tsc_error_code tsc_get_wakeup_interval (tsc_handle handle, uint32_t *wakeup_interval);

/*Gets information about the current tunnel socket. Populates the passed tsc_tunnel_socket_info structure.*/
/*!
* Purpose: Get tunnel infromation \n
@param handle \n Type: tsc_handle. \n handle for tunnel
@param stats \n Type: tsc_tunnel_socket_info. \n struct to be filled
@return  \n Type: tsc_error_code. \n Return tsc_error_code_ok if socket info retrieved,  else return tsc_error_code_error
*/
tsc_error_code 
tsc_get_tunnel_socket_info (tsc_handle handle, tsc_tunnel_socket_info * tunnel_info);
    

/*Writes all stats to the log file*/
/*!
* Purpose: Logging statistics \n
* Saves currently recorded statistics to the log
@param handle \n Type: tsc_handle. \n handle for tunnel
@return  \n Type: tsc_error_code. \n Return tsc_error_code_ok if stats retrieved, else return tsc_error_code_error
*/
tsc_error_code tsc_dump_stats (tsc_handle handle);


/* Send raw data through the tunnel*/
/*!
* Purpose: low level send raw data \n
* this would initiate a send operation to the server through the connected tunnel
@param handle \n Type: tsc_handle. \n
@param buffer \n Type: void pointer. \n buffer to pass data to be sent
@param size \n Type: uint32_t. \n size of buffer in bytes
@param option \n Type: tsc_output_option. \n flags for data to be sent
@return  \n Type: tsc_error_code. \n Return tsc_error_code_ok if send successful, else return tsc_error_code_error
*/
tsc_error_code tsc_send_data (tsc_handle handle, void *buffer, uint32_t size,
                              tsc_output_option * option);

/* Receive raw data from the tunnel*/
/*!
* Purpose: low level receive raw data \n
* this would initiate a receive operation and store the received data in 'buffer'
@param handle \n Type: tsc_handle. \n
@param buffer \n Type: void pointer. \n pointer to buffer to store received data
@param size \n Type: uint32_t. \n pointer to variable to store the size of received data, measured in bytes
@return  \n Type: tsc_error_code. \n Return tsc_error_code_ok if receive successful, else return tsc_error_code_error
*/
tsc_error_code tsc_recv_data (tsc_handle handle, void *buffer,
                              uint32_t * size);

/* Send udp data through the tunnel*/
/*!
* Purpose: low level send UDP data through the tunnel
@param handle \n Type: tsc_handle. \n
@param src \n Type: tsc_ip_port_address variable. \n source IPv4 add with port 
@param dst \n Type: tsc_ip_port_address variable. \n destination IPv4 address with port
@param buffer \n Type: void pointer. \n pointer to buffer data to be sent
@param size \n Type: uint32_t. \n size of buffer in bytes
@param option \n Type: tsc_output_option. \n flags for data to be sent
@return  \n Type: tsc_error_code. \n Return tsc_error_code_ok if send successful, else return tsc_error_code_error
*/
tsc_error_code tsc_send_udp_data (tsc_handle handle,
                                  tsc_ip_port_address * src,
                                  tsc_ip_port_address * dst, void *buffer,
                                  uint32_t size, tsc_output_option * option);

/* Receive udp data from the tunnel*/
/*!
* Purpose: low level TCP receive data over tunnel
@param handle \n Type: tsc_handle. \n
@param src \n Type: tsc_ip_port_address variable. \n source IPv4 add with port
@param dst \n Type: tsc_ip_port_address variable. \n destination IPv4 add with port
@param buffer \n Type: void pointer. \n pointer to buffer to store received data
@param size \n Type: uint32_t. \n pointer to variable to store the size of received data, measured in bytes
@return  \n Type: tsc_error_code. \n Return tsc_error_code_ok if receive successful, else return tsc_error_code_error
*/
tsc_error_code tsc_recv_udp_data (tsc_handle handle,
                                  tsc_ip_port_address * src,
                                  tsc_ip_port_address * dst, void *buffer,
                                  uint32_t * size);

/* Terminates client tunnel, releases all resources*/
/*! 
* Purpose: low level terminate tunnel \n
this would be called when you want to close connection to the server and delete the tunnel
@param handle \n Type: tsc_handle. \n tunnel handle
@return  \n Type: tsc_error_code. \n Return tsc_error_code_ok if tunnel deleted successfully, else return tsc_error_code_error
*/
tsc_error_code tsc_delete_tunnel (tsc_handle handle);

/* Enables tunnel notifications */
/*! 
* Purpose: enables generic tunnel notifications \n
this would be called when you want to receive tunnel notifications
@param handle \n Type: tsc_handle. \n tunnel handle
@param type \n Type: tsc_notification. \n notification type
@param notification \n Type: void *. \n notification callback
@param opaque \n Type: void *. \n opaque parameter
@return  \n Type: tsc_notification_handle. \n Return notification handle
*/
tsc_notification_handle tsc_notification_enable (tsc_handle handle, tsc_notification type,
                                                 void (*notification)(tsc_notification_data *),
                                                 void *opaque);

/* Disables tunnel notifications */
/*! 
* Purpose: disables generic tunnel notifications \n
this would be called when you want to stop receiving tunnel notifications
@param notification_handle \n Type: tsc_notification_handle. \n tunnel handle
@return  \n Type: tsc_error_code. \n Return tsc_error_code_ok if tunnel deleted successfully, else return tsc_error_code_error
*/
tsc_error_code tsc_notification_disable (tsc_notification_handle notification_handle);

/**@}*/

#ifdef __cplusplus
}
#endif
#endif /* __TSC_TUNNEL_H__ */
