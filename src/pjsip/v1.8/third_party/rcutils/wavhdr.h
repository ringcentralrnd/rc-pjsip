/*
 * wavhdr.h
 *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 */

#ifndef WAVHDR_H_
#define WAVHDR_H_

#include <pj/os.h>
#include <pj/types.h>
#include <pj/log.h>
#include <pj/string.h>

int prepare_wav_header( int clock_rate,
                        int channels,
                        int bits_per_sample,
                        int data_size,
                        FILE* f_record );

#endif /* WAVHDR_H_ */
