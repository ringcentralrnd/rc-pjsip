/*
 * rtphdr.c
 *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 */

#include <rtphdr.h>


int save_rtp_info(  const struct rtp_info* irtp,
        const int s_count,
        FILE* f_out )
{
    pj_timestamp prev;
    pj_uint32_t rtp_timestamp_prev = 0, rtp_timestamp = 0;
    pj_uint32_t  rtp_timestamp_prev_diff = 0;
    pj_uint16_t  rtp_oder_prev = 0, rtp_oder_prev_diff = 0, rtp_oder = 0;

    int i;
    char line[120];

    prev.u64 = 0;

    if( !f_out )
        return 0;


    for( i = 0; i < s_count; i++ ){

        rtp_timestamp = pj_ntohl(irtp[i].rtp_timestamp);
        if( rtp_timestamp_prev == 0 )
            rtp_timestamp_prev = rtp_timestamp;

        rtp_timestamp_prev_diff = rtp_timestamp - rtp_timestamp_prev;

        rtp_oder = pj_ntohs(irtp[i].rtp_order);
        if( rtp_oder_prev == 0 )
            rtp_oder_prev = rtp_oder;

        rtp_oder_prev_diff      = rtp_oder - rtp_oder_prev;

        sprintf( line, "Time %llu [%3u] RTP Time %lu [%3u] Seq %5u [%u]\n",
                irtp[i].timestamp.u64,
                (prev.u64 > 0 ? pj_elapsed_msec( &prev , &irtp[i].timestamp ) : 0 ),
                rtp_timestamp,
                rtp_timestamp_prev_diff,
                rtp_oder,
                rtp_oder_prev_diff );

        fwrite( line, pj_ansi_strlen(line), 1, f_out );

        rtp_oder_prev       = rtp_oder;
        rtp_timestamp_prev  = rtp_timestamp;
        prev.u64 = irtp[i].timestamp.u64;
    }

    return 1;
}
