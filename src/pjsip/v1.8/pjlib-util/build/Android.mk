LOCAL_PATH := $(call my-dir)/../
include $(CLEAR_VARS)

LOCAL_MODULE    := pjlib-util

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../pjlib/include/ $(LOCAL_PATH)/include/ $(LOCAL_PATH)../third_party/tsc/include/
LOCAL_CFLAGS := $(RC_PJSIP_LIB_FLAGS)
PJSIP_LIB_SRC_DIR := src/pjlib-util

LOCAL_SRC_FILES := $(PJSIP_LIB_SRC_DIR)/base64.c \
	$(PJSIP_LIB_SRC_DIR)/crc32.c \
	$(PJSIP_LIB_SRC_DIR)/errno.c \
	$(PJSIP_LIB_SRC_DIR)/dns.c \
	$(PJSIP_LIB_SRC_DIR)/dns_dump.c \
	$(PJSIP_LIB_SRC_DIR)/dns_server.c \
	$(PJSIP_LIB_SRC_DIR)/getopt.c \
	$(PJSIP_LIB_SRC_DIR)/hmac_md5.c \
	$(PJSIP_LIB_SRC_DIR)/hmac_sha1.c \
	$(PJSIP_LIB_SRC_DIR)/md5.c \
	$(PJSIP_LIB_SRC_DIR)/pcap.c \
	$(PJSIP_LIB_SRC_DIR)/resolver.c \
	$(PJSIP_LIB_SRC_DIR)/scanner.c \
	$(PJSIP_LIB_SRC_DIR)/sha1.c \
	$(PJSIP_LIB_SRC_DIR)/srv_resolver.c \
	$(PJSIP_LIB_SRC_DIR)/string.c \
	$(PJSIP_LIB_SRC_DIR)/stun_simple.c \
	$(PJSIP_LIB_SRC_DIR)/stun_simple_client.c \
	$(PJSIP_LIB_SRC_DIR)/xml.c

include $(BUILD_STATIC_LIBRARY)

