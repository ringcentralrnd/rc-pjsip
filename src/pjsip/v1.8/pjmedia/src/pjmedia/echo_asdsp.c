/*
 * Wrapper for ASDSP EC
 */

#include <pjmedia/echo.h>
#include <pjmedia/errno.h>
#include <pj/assert.h>
#include <pj/log.h>
#include <pj/pool.h>
#include <SBACP_API.h>
#include <SBACP_Ver.h>

#include "echo_internal.h"

#define THIS_FILE   "echo_asdsp.c"

typedef struct asdsp_ec
{
    unsigned		  samples_per_frame;
    unsigned		  options;

	ShortReal 		  *speaker_buf_in;
	ShortReal 		  *microphone_buf_in[1];
	ShortReal		  *speaker_buf_out;
	ShortReal		  *microphone_buf_out;
	SBACP_Handle      hSBACP;
} asdsp_ec;

/*
 * Create the AEC.
 */
PJ_DEF(pj_status_t) sbacp_aec_create(pj_pool_t *pool,
				     unsigned clock_rate,
				     unsigned channel_count,
				     unsigned samples_per_frame,
				     unsigned tail_ms,
				     unsigned options,
				     void **p_echo )
{
	PJ_LOG(2,(THIS_FILE, "sbacp_aec_create started."));

	asdsp_ec *echo;
    int sampling_rate;

    *p_echo = NULL;

    echo = PJ_POOL_ZALLOC_T(pool, asdsp_ec);
    if( echo == NULL ){
    	PJ_LOG(1,(THIS_FILE, "sbacp_aec_create Could not allocate memory failed."));
    	return PJ_ENOMEM;
    }

    echo->samples_per_frame = samples_per_frame;
    echo->options = options;

    echo->speaker_buf_in = (ShortReal*)pj_pool_zalloc(pool, sizeof(ShortReal) * samples_per_frame );
    PJ_ASSERT_RETURN(echo->buf_in != NULL, PJ_ENOMEM);

    echo->microphone_buf_in[0] = (ShortReal*)pj_pool_zalloc(pool, sizeof(ShortReal) * samples_per_frame );
    PJ_ASSERT_RETURN(echo->buf_out != NULL, PJ_ENOMEM);

    echo->speaker_buf_out = (ShortReal*)pj_pool_zalloc(pool, sizeof(ShortReal) * samples_per_frame );
    PJ_ASSERT_RETURN(echo->buf_in != NULL, PJ_ENOMEM);

    echo->microphone_buf_out = (ShortReal*)pj_pool_zalloc(pool, sizeof(ShortReal) * samples_per_frame );
    PJ_ASSERT_RETURN(echo->buf_out != NULL, PJ_ENOMEM);

    echo->hSBACP = SBACP_Create();
    if( echo->hSBACP == NULL ){
    	PJ_LOG(1,(THIS_FILE, "sbacp_aec_create SBACP_Create failed."));
    	return PJ_ENOMEM;
    }

    /* Done */
    *p_echo = echo;

    PJ_LOG(2,(THIS_FILE, "sbacp_aec_create finished."));
    return PJ_SUCCESS;
}

/*
 * Destroy AEC
 */
PJ_DEF(pj_status_t) sbacp_aec_destroy(void *state )
{
	PJ_LOG(2,(THIS_FILE, "sbacp_aec_destroy started."));

	asdsp_ec *echo = (asdsp_ec*) state;

    PJ_ASSERT_RETURN(echo, PJ_EINVAL);

    if (echo->hSBACP) {
    	SBACP_Delete(echo->hSBACP);
    	echo->hSBACP = NULL;
    }

	PJ_LOG(2,(THIS_FILE, "sbacp_aec_destroy finished."));
    return PJ_SUCCESS;
}

/*
 * Reset AEC
 */
PJ_DEF(void) sbacp_aec_reset(void *state )
{
	PJ_LOG(2,(THIS_FILE, "sbacp_aec_reset started."));

	asdsp_ec *echo = (asdsp_ec*) state;
    if (echo->hSBACP) {
    	SBACP_Restart(echo->hSBACP);
    }

	PJ_LOG(2,(THIS_FILE, "sbacp_aec_reset finished."));
}

/*
 * Perform echo cancellation.
 */
PJ_DEF(pj_status_t) sbacp_aec_cancel_echo( void *state,
					   pj_int16_t *rec_frm,
					   const pj_int16_t *play_frm,
					   unsigned options,
					   void *reserved )
{
	asdsp_ec *echo = (asdsp_ec*) state;
	int i;

    /* Sanity checks */
    PJ_ASSERT_RETURN(echo && rec_frm && play_frm && options==0 &&
		     reserved==NULL, PJ_EINVAL);

    /* Prepare playback(speaker) & microphone samples for EC*/
    for( i = 0; i < echo->samples_per_frame; i++ ){
    	echo->speaker_buf_in[i] 		= (float)play_frm[i];
    	echo->microphone_buf_in[0][i] 	= (float)rec_frm[i];
    }

    /*
     *
		void SBACP_ProcessFrame                 //! Same as SBACP_ProcessSpkFrame() and SBACP_ProcessMicFrame() called sequentially
		(
			ShortReal**     mic,                //!< each mic[] contains FRAME_SIZE samples
			ShortReal*      speaker,            //!< speaker input
			ShortReal*      TxOut,              //!< Tx Ouput (AEC, PostFilter)
			ShortReal*      SpkOut,             //!< Speaker output
			SBACP_Handle    hSBACP              //!< SBACP handle
		);
     *
     */

    SBACP_ProcessFrame( &echo->microphone_buf_in[0],
						echo->speaker_buf_in,
						echo->microphone_buf_out,
						echo->speaker_buf_out,
						echo->hSBACP );

    /* We do not need on output frames for speaker because
     * it will be used for changing microphone signal
     */


    /* Process microphone output */
    for( i = 0; i < echo->samples_per_frame; i++ ){
    	rec_frm[i] = (pj_int16_t)echo->microphone_buf_out[i];
    }

    return PJ_SUCCESS;

}

