LOCAL_PATH := $(call my-dir)/../

$(call __ndk_info, $(LOCAL_PATH) Android.mk )

include $(CLEAR_VARS)
LOCAL_MODULE    := pjlib

LOCAL_C_INCLUDES += $(LOCAL_PATH)/include/
ifeq ($(RC_PJSIP_USE_TLS),1)
LOCAL_C_INCLUDES += $(LOCAL_PATH)../third_party/openssl/include/
endif

LOCAL_C_INCLUDES += $(LOCAL_PATH)../third_party/tsc/include/


LOCAL_CFLAGS := $(RC_PJSIP_LIB_FLAGS)
PJSIP_LIB_SRC_DIR := src/pj

LOCAL_SRC_FILES := $(PJSIP_LIB_SRC_DIR)/addr_resolv_sock.c \
	$(PJSIP_LIB_SRC_DIR)/file_access_unistd.c \
	$(PJSIP_LIB_SRC_DIR)/file_io_ansi.c \
	$(PJSIP_LIB_SRC_DIR)/guid_simple.c \
	$(PJSIP_LIB_SRC_DIR)/log.c \
	$(PJSIP_LIB_SRC_DIR)/log_writer_stdout.c \
	$(PJSIP_LIB_SRC_DIR)/os_core_android.c \
	$(PJSIP_LIB_SRC_DIR)/os_error_unix.c \
	$(PJSIP_LIB_SRC_DIR)/os_time_common.c \
	$(PJSIP_LIB_SRC_DIR)/os_time_unix.c \
	$(PJSIP_LIB_SRC_DIR)/os_timestamp_common.c \
	$(PJSIP_LIB_SRC_DIR)/os_timestamp_posix.c \
	$(PJSIP_LIB_SRC_DIR)/pool_policy_malloc.c \
	$(PJSIP_LIB_SRC_DIR)/sock_common.c \
	$(PJSIP_LIB_SRC_DIR)/sock_qos_common.c \
	$(PJSIP_LIB_SRC_DIR)/sock_qos_bsd.c \
	$(PJSIP_LIB_SRC_DIR)/ssl_sock_common.c \
	$(PJSIP_LIB_SRC_DIR)/ssl_sock_ossl.c \
	$(PJSIP_LIB_SRC_DIR)/sock_bsd.c \
	$(PJSIP_LIB_SRC_DIR)/sock_select.c \
	$(PJSIP_LIB_SRC_DIR)/activesock.c \
	$(PJSIP_LIB_SRC_DIR)/array.c \
	$(PJSIP_LIB_SRC_DIR)/config.c \
	$(PJSIP_LIB_SRC_DIR)/ctype.c \
	$(PJSIP_LIB_SRC_DIR)/errno.c \
	$(PJSIP_LIB_SRC_DIR)/except.c \
	$(PJSIP_LIB_SRC_DIR)/fifobuf.c \
	$(PJSIP_LIB_SRC_DIR)/guid.c \
	$(PJSIP_LIB_SRC_DIR)/hash.c \
	$(PJSIP_LIB_SRC_DIR)/ip_helper_generic.c \
	$(PJSIP_LIB_SRC_DIR)/list.c \
	$(PJSIP_LIB_SRC_DIR)/lock.c \
	$(PJSIP_LIB_SRC_DIR)/pool.c \
	$(PJSIP_LIB_SRC_DIR)/pool_buf.c \
	$(PJSIP_LIB_SRC_DIR)/pool_caching.c \
	$(PJSIP_LIB_SRC_DIR)/pool_dbg.c \
	$(PJSIP_LIB_SRC_DIR)/rbtree.c \
	$(PJSIP_LIB_SRC_DIR)/string.c \
	$(PJSIP_LIB_SRC_DIR)/rand.c \
	$(PJSIP_LIB_SRC_DIR)/timer.c \
	$(PJSIP_LIB_SRC_DIR)/types.c \
	$(PJSIP_LIB_SRC_DIR)/ioqueue_select.c

$(call __ndk_info, $(LOCAL_PATH) Android.mk BUILD_STATIC_LIBRARY $(BUILD_STATIC_LIBRARY) )

include $(BUILD_STATIC_LIBRARY)

