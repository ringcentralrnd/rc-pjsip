/*
 * This file contains several sample settings for Android. This file is called from
 * <pj/config.h> file.
 *
 */
/*
 * Minimum size
 */

#ifdef PJ_CONFIG_MINIMAL_SIZE

#   undef PJ_OS_HAS_CHECK_STACK
#   define PJ_OS_HAS_CHECK_STACK	0
#   define PJ_LOG_MAX_LEVEL		0
#   define PJ_ENABLE_EXTRA_CHECK	0
#   define PJ_HAS_ERROR_STRING		0
#   undef PJ_IOQUEUE_MAX_HANDLES
/* Putting max handles to lower than 32 will make pj_fd_set_t size smaller
 * than native fdset_t and will trigger assertion on sock_select.c.
 */
#   define PJ_IOQUEUE_MAX_HANDLES	32
#   define PJSIP_MAX_TSX_COUNT		15
#   define PJSIP_MAX_DIALOG_COUNT	15
#   define PJSIP_UDP_SO_SNDBUF_SIZE	4000
#   define PJSIP_UDP_SO_RCVBUF_SIZE	4000
#   define PJMEDIA_HAS_LARGE_FILTER	0
#   define PJMEDIA_HAS_SMALL_FILTER	0


#elif defined(PJ_CONFIG_MAXIMUM_SPEED)
#   define PJ_SCANNER_USE_BITWISE	0
#   undef PJ_OS_HAS_CHECK_STACK
#   define PJ_OS_HAS_CHECK_STACK	0
#   define PJ_LOG_MAX_LEVEL		3
#   define PJ_ENABLE_EXTRA_CHECK	0
#   define PJ_IOQUEUE_MAX_HANDLES	5000
#   define PJSIP_MAX_TSX_COUNT		((640*1024)-1)
#   define PJSIP_MAX_DIALOG_COUNT	((640*1024)-1)
#   define PJSIP_UDP_SO_SNDBUF_SIZE	(24*1024*1024)
#   define PJSIP_UDP_SO_RCVBUF_SIZE	(24*1024*1024)
#   define PJ_DEBUG			0
#   define PJSIP_SAFE_MODULE		0
#   define PJ_HAS_STRICMP_ALNUM		0
#   define PJ_HASH_USE_OWN_TOLOWER	1
#   define PJSIP_UNESCAPE_IN_PLACE	1

#   ifdef PJ_WIN32
#     define PJSIP_MAX_NET_EVENTS	10
#   endif

#   define PJSUA_MAX_CALLS		512

#endif

#if defined(PJ_ANDROID) && PJ_ANDROID!=0

	/* Optimizations */
	#define PJ_ENABLE_EXTRA_CHECK   0

	//Conference ports?
	#define PJMEDIA_CONF_USE_SWITCH_BOARD 0

   	#define PJ_OS_HAS_CHECK_STACK		0


	#define PJSIP_HAS_DIGEST_MJMD5_AUTH	1
	#define PJ_HAS_IPV6 1

	#define PJ_QOS_IMPLEMENTATION    PJ_QOS_BSD


	#define PJSIP_CHECK_VIA_SENT_BY 0

	/*
     * PJMEDIA settings
     */
    /* Select codecs to disable */
    #define PJMEDIA_HAS_L16_CODEC               0

    /* Fine tune Speex's default settings for best performance/quality */
    #define PJMEDIA_CODEC_SPEEX_DEFAULT_QUALITY 5

    /* For CPU reason, disable speex AEC and use the echo suppressor. */
    #define PJMEDIA_HAS_SPEEX_AEC               0

    /* Previously, resampling is disabled due to performance reason and
     * this condition prevented some 'light' wideband codecs (e.g: G722.1)
     * to work along with narrowband codecs. Lately, some tests showed
     * that 16kHz <-> 8kHz resampling using libresample small filter was 
     * affordable on ARM9 222 MHz, so here we decided to enable resampling.
     * Note that it is important to make sure that libresample is created
     * using small filter. For example PJSUA_DEFAULT_CODEC_QUALITY must
     * be set to 3 or 4 so pjsua-lib will apply small filter resampling.
     */
    //#define PJMEDIA_RESAMPLE_IMP              PJMEDIA_RESAMPLE_NONE
    #define PJMEDIA_RESAMPLE_IMP                PJMEDIA_RESAMPLE_LIBRESAMPLE

    /* Use the lighter WSOLA implementation */
    #define PJMEDIA_WSOLA_IMP                   PJMEDIA_WSOLA_IMP_WSOLA_LITE

    /* We probably need more buffers especially if MDA audio backend 
     * is used, so increase the limit 
     */
  //  #define PJMEDIA_SOUND_BUFFER_COUNT          32

//	#define PJMEDIA_SND_DEFAULT_REC_LATENCY 150
//	#define PJMEDIA_SND_DEFAULT_PLAY_LATENCY 200

    /*
     * PJSUA settings.
     */

    /* Default codec quality, previously was set to 5, however it is now
     * set to 4 to make sure pjsua instantiates resampler with small filter.
     */
    #define PJSUA_DEFAULT_CODEC_QUALITY         4
	//We need hi availablitly so retry each minute if network fails -
	//all the more so as we unregister if network not available for us
	#define PJSUA_REG_RETRY_INTERVAL	60
	//#define PJSUA_DEFAULT_ILBC_MODE 			20


    /* Set maximum number of dialog/transaction/calls to minimum */
    #define PJSIP_MAX_TSX_COUNT 		31
    #define PJSIP_MAX_DIALOG_COUNT 		31
    #define PJSUA_MAX_CALLS			4

    /* Other pjsua settings */
    #define PJSUA_MAX_ACC			10
    #define PJSUA_MAX_PLAYERS			4
    #define PJSUA_MAX_RECORDERS			4
    #define PJSUA_MAX_CONF_PORTS		(PJSUA_MAX_CALLS+2*PJSUA_MAX_PLAYERS)
    #define PJSUA_MAX_BUDDIES			32

	#define PJ_STUN_KEEP_ALIVE_SEC 40

	/* Fix google voice problem with RTP DTMF events */
	#define PJMEDIA_RTP_PT_TELEPHONE_EVENTS          101
	#define PJMEDIA_RTP_PT_TELEPHONE_EVENTS_STR      "101"
#endif
