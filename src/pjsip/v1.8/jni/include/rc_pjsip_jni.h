/*
 * rc_pjsip_jni.h
 *
 *
 * Copyright (C) 2012, RingCentral, Inc.
 * All Rights Reserved.
 *
 * Author: Denis Kudja
 *
 * Headers of PJSIP JNI interface
 */

#ifndef RC_PJSIP_JNI_H_
#define RC_PJSIP_JNI_H_

#include <jni.h>
#include <android/log.h>
#include <pjsua-lib/pjsua.h>
#include <pjsua-lib/pjsua_internal.h>

#ifdef __cplusplus
extern "C" {
#endif

#define TAG 		"RC JNI"

extern JavaVM *java_vm;

#define RC_PJSUA_DEFAULT_CLOCK_RATE	8000

#define RC_ACCOUNT_SIP_REGISTRATION_TIMEOUT	( 60 * 20 ) //20 min

enum cb_methods_index {
	on_reg_state_idx 		= 0,
	on_message_idx 			= 1,
	on_incoming_call_idx	= 2,
	on_call_media_state_idx = 3,
	on_stream_destroyed_idx	= 4,
	on_call_state_idx		= 5,
	on_call_transfer_status_idx = 6,
	on_media_failed_idx		= 7,

	last_function			= 8,
};

struct callback_methods {
	jclass 		cb_class;
	jmethodID	cb_methodId[last_function];
};

struct ring_config {
	int			slot;
	int			cnt;
	pjmedia_port *port;
};

struct rc_config_struct {
	pj_pool_t		   *pool;

	struct callback_methods rc_callbacks;

	struct ring_config ringback;
	struct ring_config ring;

	jclass 		AudioDriver_class;
};

/**
 * Additional call information.
 * Added on make_call and on_call_incoming
 *
 * Will be through pjsua_call_set_user_data
 * Is available through pjsua_call_get_user_data
 *
 */

#define RC_CALL_STATUS_HOLD			0x00000001

struct rc_call_info {

	/*
	 * Bit mask
	 *
	 * 0 HOLD status
	 * 1 - is on HOLD, 0 - does not
	 *
	 */
	int 	status;

	/*
	 * RC header is added on INVITE for incoming calls.
	 * this field is felt during receiving on_incoming_call callback
	 */
	pj_str_t rc_header;
};

extern struct rc_config_struct rc_cfg;


#define LOG_VerboseLogcat(...) 	__android_log_print(ANDROID_LOG_VERBOSE, TAG, __VA_ARGS__)
#define LOG_DebugLogcat(...) 		__android_log_print(ANDROID_LOG_DEBUG  , TAG, __VA_ARGS__)
#define LOG_InfoLogcat(...) 		__android_log_print(ANDROID_LOG_INFO   , TAG, __VA_ARGS__)
#define LOG_WarnLogcat(...) 		__android_log_print(ANDROID_LOG_WARN   , TAG, __VA_ARGS__)
#define LOG_ErrorLogcat(...) 		__android_log_print(ANDROID_LOG_ERROR  , TAG, __VA_ARGS__)

void pj_android_log_msg(int level, const char *data, int len);

#define ATTACH_CURRENT_THREAD(ERROR_CODE)	JNIEnv *jni_env; \
	int env_status = java_vm->GetEnv((void **)&jni_env, JNI_VERSION_1_6); \
	if( env_status == JNI_EDETACHED ) \
		jint attachResult = java_vm->AttachCurrentThread(&jni_env , NULL );

#define DETACH_CURRENT_THREAD() if( env_status == JNI_EDETACHED ){ java_vm->DetachCurrentThread(); }

/*
 *
 */
pj_status_t ringback_init(const pjsua_media_config *media_cfg);
pj_status_t ring_init(const pjsua_media_config *media_cfg);

void ringback_start(pjsua_call_id call_id);
void ring_start(pjsua_call_id call_id);

void ring_stop(pjsua_call_id call_id);

void ringback_destroy();
void ring_destroy();


#define RC_HEADER_NAME	"P-rc"

/*
 * Callback methods
 */
void rc_jni_on_reg_state(pjsua_acc_id acc_id);

void rc_jni_on_call_media_state(pjsua_call_id call_id);

void rc_jni_on_message(pjsua_call_id call_id,
						const pj_str_t *from,
						const pj_str_t *to,
						const pj_str_t *contact,
						const pj_str_t *mime_type,
						const pj_str_t *body );

void rc_jni_on_incoming_call(pjsua_acc_id acc_id,
			pjsua_call_id call_id,
			pjsip_rx_data *rdata);

void rc_jni_on_stream_destroyed(pjsua_call_id call_id,
								pjmedia_session *sess,
								unsigned stream_idx);

void rc_jni_on_call_state(pjsua_call_id call_id, pjsip_event *e);

void rc_jni_on_call_transfer_status(pjsua_call_id call_id,
			    int st_code,
			    const pj_str_t *st_text,
			    pj_bool_t final,
			    pj_bool_t *p_cont);

/*
 * ******** SIP Stack control functions ***********
 */

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_create
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1create
  (JNIEnv *, jclass);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_destroy
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1destroy
  (JNIEnv *, jclass);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_init
 * Signature: (Ljava/lang/String;I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1init
  (JNIEnv *, jclass, jstring, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_start
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1start
  (JNIEnv *, jclass);

/*
 * ******** SIP Stack Account configuration functions ***********
 */

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_acc_add
 * Signature: ([Ljava/lang/String;[IJ)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1add
  (JNIEnv *, jclass, jobjectArray, jintArray, jlong);



/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_acc_get_default
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1get_1default
  (JNIEnv *, jclass);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_acc_modify
 * Signature: (I[Ljava/lang/String;J)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1modify
  (JNIEnv *, jclass, jint, jobjectArray, jlong);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_acc_del
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1del
  (JNIEnv *, jclass, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_acc_get_status
 * Signature: (I[I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1get_1status
  (JNIEnv *, jclass, jint, jintArray);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_acc_is_valid
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1is_1valid
  (JNIEnv *, jclass, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_acc_get_info
 * Signature: (I[Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1get_1info
  (JNIEnv *, jclass, jint, jobjectArray);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_acc_set_registration
 * Signature: (IZ)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1set_1registration
  (JNIEnv *, jclass, jint, jboolean);

/*
 * ******** SIP Stack Service functions ***********
 */

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_ring_start
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1ring_1start
  (JNIEnv *, jclass, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_ringback_start
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1ringback_1start
  (JNIEnv *, jclass, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_ring_stop
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1ring_1stop
  (JNIEnv *, jclass, jint);

/*
 * ******** SIP Stack Call control functions ***********
 */

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_answer
 * Signature: (II[Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1answer
  (JNIEnv *, jclass, jint, jint, jobjectArray);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_hangup
 * Signature: (II[Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1hangup
  (JNIEnv *, jclass, jint, jint, jobjectArray);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_hold
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1hold
  (JNIEnv *, jclass, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_unhold
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1unhold
  (JNIEnv *, jclass, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_make
 * Signature: (ILjava/lang/String;[Ljava/lang/String;[I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1make
  (JNIEnv *, jclass, jint, jstring, jobjectArray, jintArray);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_send_request
 * Signature: (ILjava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1send_1request
  (JNIEnv *, jclass, jint, jstring, jstring);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_attach_sound
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1attach_1sound
  (JNIEnv *, jclass, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_meia_disconnect
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1meia_1disconnect
  (JNIEnv *, jclass, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_transfer
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1transfer
  (JNIEnv *, jclass, jint, jstring);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_hangup_all
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1hangup_1all
  (JNIEnv *, jclass);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_reinvate_all
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1reinvate_1all
  (JNIEnv *, jclass);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_dial_dtmf
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1dial_1dtmf
  (JNIEnv *, jclass, jint, jstring);

/*
 * ******** SIP Stack Call information functions ***********
 */

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsua_call_has_media
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsua_1call_1has_1media
  (JNIEnv *, jclass, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_is_active
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1is_1active
  (JNIEnv *, jclass, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_get_max_count
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1get_1max_1count
  (JNIEnv *, jclass);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_get_count
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1get_1count
  (JNIEnv *, jclass);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_enum_calls
 * Signature: ([I[I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1enum_1calls
  (JNIEnv *, jclass, jintArray, jintArray);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_get_info
 * Signature: (I[Ljava/lang/String;[I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1get_1info
  (JNIEnv *, jclass, jint, jobjectArray, jintArray);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_get_duration
 * Signature: (I[J)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1get_1duration
  (JNIEnv *, jclass, jint, jlongArray);

/*
 * ************ Message functions ***************
 */

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_message_send
 * Signature: (ILjava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1message_1send
  (JNIEnv *, jclass, jint, jstring, jstring);

/*
 * Codec functions
 */
/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_enum_codecs
 * Signature: ([Ljava/lang/String;[I[I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1enum_1codecs
  (JNIEnv *, jclass, jobjectArray, jintArray, jintArray );

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_codec_set_priority
 * Signature: (Ljava/lang/String;I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1codec_1set_1priority
  (JNIEnv *, jclass, jstring, jint);

/*
 * SIP Stack Conference service functions
 */

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_conf_get_signal_rx_level
 * Signature: (I)F
 */
JNIEXPORT jfloat JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1get_1signal_1rx_1level
  (JNIEnv *, jclass, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_conf_get_signal_tx_level
 * Signature: (I)F
 */
JNIEXPORT jfloat JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1get_1signal_1tx_1level
  (JNIEnv *, jclass, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_conf_adjust_tx_level
 * Signature: (IF)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1adjust_1tx_1level
  (JNIEnv *, jclass, jint, jfloat);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_conf_adjust_rx_level
 * Signature: (IF)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1adjust_1rx_1level
  (JNIEnv *, jclass, jint, jfloat);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_conf_get_adjust_tx_level
 * Signature: (I)F
 */
JNIEXPORT jfloat JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1get_1adjust_1tx_1level
  (JNIEnv *, jclass, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_conf_get_adjust_rx_level
 * Signature: (I)F
 */
JNIEXPORT jfloat JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1get_1adjust_1rx_1level
  (JNIEnv *, jclass, jint);

/*
 * Audio Device functions
 */
/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_set_snd_dev
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1set_1snd_1dev
  (JNIEnv *, jclass, jint, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_get_snd_dev
 * Signature: ([I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1get_1snd_1dev
  (JNIEnv *, jclass, jintArray);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_set_no_snd_dev
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1set_1no_1snd_1dev
  (JNIEnv *, jclass);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_media_transports_create
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1media_1transports_1create
  (JNIEnv *, jclass, jint);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_media_transports_update
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1media_1transports_1update
  (JNIEnv *, jclass, jint);


/*
 * RC Android Audio Driver JAVA Implementation
 */

/*
 * Class:     com_rcbase_android_sip_audio_AudioDevicePJSIP
 * Method:    writeToRTP
 * Signature: ([BIJJ)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_writeToRTP
  (JNIEnv *, jclass, jbyteArray, jint, jlong, jlong);

/*
 * Class:     com_rcbase_android_sip_audio_AudioDevicePJSIP
 * Method:    readFromRTP
 * Signature: ([BIJJ)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_readFromRTP
  (JNIEnv *, jclass, jbyteArray, jint, jlong, jlong);

/*
 * Class:     com_rcbase_android_sip_audio_AudioDevicePJSIP
 * Method:    aec
 * Signature: ([B[BIJ)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aec
  (JNIEnv *, jclass, jbyteArray, jbyteArray, jint, jlong);

/*
 * Class:     com_rcbase_android_sip_audio_AudioDevicePJSIP
 * Method:    aecScheme2
 * Signature: ([B[BIJ)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aecScheme2
  (JNIEnv *, jclass, jbyteArray, jbyteArray, jint, jlong);

/*
 * Class:     com_rcbase_android_sip_audio_AudioDevicePJSIP
 * Method:    aecSetTestMode
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aecSetTestMode
  (JNIEnv *, jclass, jlong);

/*
 * Class:     com_rcbase_android_sip_audio_AudioDevicePJSIP
 * Method:    aecGetCurrentTestMode
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aecGetCurrentTestMode
  (JNIEnv *, jclass, jlong);

/*
 * Class:     com_rcbase_android_sip_audio_AudioDevicePJSIP
 * Method:    aecGetMicDb
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aecGetMicDb
  (JNIEnv *, jclass, jlong);

/*
 * Class:     com_rcbase_android_sip_audio_AudioDevicePJSIP
 * Method:    aecGetMicDb
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aecGetAudioDelay
  (JNIEnv *, jclass, jlong);

/*
 * Class:     com_rcbase_android_sip_audio_AudioDevicePJSIP
 * Method:    getTimestamp
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_getTimestamp
  (JNIEnv *, jclass, jlong);

/*
 * Class:     com_rcbase_android_sip_audio_AudioDevicePJSIP
 * Method:    getElapsedTime
 * Signature: (JJJ)J
 */
JNIEXPORT jlong JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_getElapsedTime
  (JNIEnv *, jclass, jlong, jlong, jlong);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_setup_start
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1stop_1test
  (JNIEnv *, jclass);

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_setup_stop
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1start_1test
  (JNIEnv *, jclass);

JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_readFromWriteToRTP
  (JNIEnv *, jclass, jbyteArray, jbyteArray, jint, jlong, jlong);

//by martin 
/*
 * ******** TSC  configuration setting  ***********
 */

/*
 * Class:     
 * Method:    
 * Signature: 
 */

JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1tsc_1tunnel_1set
	(JNIEnv *, jclass, 		jint enable,jstring ip,jint  port,jint red);

#ifdef __cplusplus
}
#endif

#endif /* RC_PJSIP_JNI_H_ */
