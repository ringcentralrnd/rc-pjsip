/*
 * rc_android_audio_dev.c
 *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 *  Implementation of RingCentral Audio Device for Android
 */

#ifndef PJMEDIA_AUDIO_DEV_HAS_ANDROID
#define PJMEDIA_AUDIO_DEV_HAS_ANDROID	1
#endif

#include <pjsua-lib/pjsua.h>
#include <pjsua-lib/pjsua_internal.h>
#include <sys/resource.h>

#include "rc_pjsip_jni.h"
#include "rc_android_audio_dev.h"


#if defined(PJMEDIA_AUDIO_DEV_HAS_ANDROID)

#ifndef PJMEDIA_HAS_SBACP_EC
#define PJMEDIA_HAS_SBACP_EC 1
#endif

#if defined(PJMEDIA_HAS_SBACP_EC) && PJMEDIA_HAS_SBACP_EC!=0

#define ANDREA_OUTPUT_LOG

#ifdef __cplusplus
extern "C" {
#endif
#include <SBACP_API.h>
#include <SBACP_Ver.h>
#ifdef  __cplusplus
}
#endif
#endif

#define JNI_EXCEPTION_INIT() bool jni_exc_occurred = false;\
		jthrowable jni_exception = 0;

#define JNI_EXCEPTION_CHECK(update_flag)	jni_exception = jni_env->ExceptionOccurred();\
		if( jni_exception ){\
			LOG_Error("JNI exception is detected");\
			jni_env->ExceptionDescribe();\
			jni_env->ExceptionClear();\
			jni_exception = 0;\
			if(update_flag)\
				jni_exc_occurred = true;\
		}\

#define IF_JNI_EXCEPTION() jni_exc_occurred

#define JNI_EXCEPTION_CALLBACK(error_code) 	if( jni_env != 0 &&\
		rc_cfg.rc_callbacks.cb_class != 0 &&\
		rc_cfg.rc_callbacks.cb_methodId[on_media_failed_idx] != 0 ){\
		int error = error_code;\
		jni_env->CallStaticVoidMethod( rc_cfg.rc_callbacks.cb_class,\
				rc_cfg.rc_callbacks.cb_methodId[on_media_failed_idx],\
				error );\
	}\
	else\
		LOG_Warn(  "Callback on_media_failed() failed. Wrong class or method ID" );


// enable or disable using Andrea's AEC
#define PJMEDIA_USE_SBACP_EC

#define DRV_TRACE_INFO 0

#if defined(DRV_TRACE_INFO) && DRV_TRACE_INFO!=0
#define DRV_TRACE_BUFFER_SIZE	(1024*512*1) //0.5 Mb per file( about 0.5 min. on 8KHz) - max for SE Xperia10i 1.6
#endif

#define RC_TRACE 1

#define DETECT_WORKING_INTERVAL 0

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
#include <pj/math.h>
#endif

#define THIS_FILE	"rc_android_audio_dev.cpp"
#define driver_name	"rc_android"

#define SBACP_HAS_HighPassMode	0

#define ERROR_(expr) PJ_LOG(1, (THIS_FILE, expr ))

#if defined(RC_TRACE) && RC_TRACE!=0
	#define TRACE_(expr) PJ_LOG(4, (THIS_FILE, expr ))
	#define TRACE__(expr,prm) PJ_LOG(4, (THIS_FILE, expr, prm ))
#else
	#define TRACE_(expr)
	#define TRACE__(expr,prm)
#endif

#define BITS_PER_SAMPLE		16

/*
 * Constants values from Android documentation
 */
#define ENCODING_PCM_16BIT	2	/**< Audio data format: PCM 16 bit per sample */
#define ENCODING_PCM_8BIT 	3	/**< Audio data format: PCM 8 bit per sample */

#define CHANNEL_OUT_MONO	4
#define CHANNEL_OUT_STEREO	12

#define CHANNEL_IN_MONO		16
#define CHANNEL_IN_STEREO	12

#define STREAM_VOICE_CALL	0

//for older API level 3
#define CHANNEL_CONFIGURATION_MONO		2
#define CHANNEL_CONFIGURATION_STEREO 	3

#define MediaRecorder_AudioSource_MIC			 1	/**< Microphone audio source  */
#define MediaRecorder_AudioSource_VOICE_UPLINK	 2	/**< Voice call uplink (Tx) audio source   */
#define MediaRecorder_AudioSource_VOICE_DOWNLINK 3	/**< Voice call downlink (Rx) audio source  */
#define MediaRecorder_AudioSource_VOICE_CALL	 4	/**< Voice call uplink + downlink audio source  */

#define MODE_STREAM	1

#define AEC_DEFAULT_DELAY 320 //DK: AEC delay on milliseconds

#define STATE_INITIALIZED 	1
#define STATE_UNINITIALIZED 0

/*
 * The size of audio buffer should be not less that 8K bytes.
 * On the other case we lost some packages.
 */
#define MIN_AUDIO_BUFFER_SIZE  8192

#define PLAYBACK_BUFFER_SIZE_FRAMES		10	// 20 ms each
#define RECORD_BUFFER_SIZE_FRAMES		20

#define RECORDSTATE_STOPPED				1
#define RECORDSTATE_RECORDING			3

#define PLAYSTATE_STOPPED 				1
#define PLAYSTATE_PAUSED				2
#define PLAYSTATE_PLAYING				3

//#define DENIS_RECORD_PLAY
#define AUDIO_RATES_PERIODIC_TRACES 0

#define BUFFER_OVERFLOW_PERIODIC_TRACES_TIMEOUT (2000) // ms

enum SDK_VERSIONS {
	SDK_CUR_DEVELOPMENT	= 10000,//Magic version number for a current development build,
							//which has not yet turned into an official release.
	SDK_UNKNOWN 		= 0,//if we cannot detect version by some reason
	SDK_BASE			= 1,//October 2008: The original, first, version of Android.
	SDK_BASE_1_1		= 2,//February 2009: First Android update, officially called 1.1.
	SDK_CUPCAKE			= 3,//May 2009: Android 1.5.
	SDK_DONUT			= 4,//September 2009: Android 1.6.
	SDK_ECLAIR			= 5,//November 2009: Android 2.0
	SDK_ECLAIR_0_1		= 6,//December 2009: Android 2.0.1
	SDK_ECLAIS_MR1		= 7,//January 2010: Android 2.1
	SDK_FROYO			= 8,//June 2010: Android 2.2
	SDK_GINGERBREAD		= 9,//November 2010: Android 2.3
	SDK_GINGERBREAD_MR1 = 10,//Almost newest version of Android, version 2.3.3.
	SDK_HONEYCOMB		= 11//Newest version of Android, version 3.0.
} ;

#if defined(AUDIO_RATES_PERIODIC_TRACES) && AUDIO_RATES_PERIODIC_TRACES!=0
#define AUDIO_RATES_PERIODIC_TRACES_TIMEOUT (5000) //ms
#endif

#if defined(DRV_TRACE_INFO) && DRV_TRACE_INFO!=0

#ifdef __cplusplus
extern "C" {
#endif
#include <wavhdr.h>
#ifdef  __cplusplus
}
#endif

/*
 *
 */
struct streamTrace {

	FILE* f_record_mic_in;
	FILE* f_record_mic_out;
	FILE* f_record_spk_in;
	FILE* f_record_spk_out;

	jbyteArray logBuffer_in_mic_array;
	jbyte* logBuffer_in_mic;

	jbyteArray logBuffer_out_mic_array;
	jbyte* logBuffer_out_mic;

	jbyteArray logBuffer_in_spk_array;
	jbyte* logBuffer_in_spk;

	jbyteArray logBuffer_out_spk_array;
	jbyte* logBuffer_out_spk;
};
#endif


/*
 * Structure is used for creating Audio Driver factory
 */
struct rc_android_audio_factory
{
	pjmedia_aud_dev_factory base;
	pj_pool_factory *pf;
	pj_pool_t *pool;
    pjmedia_aud_dev_info	 dev_info;
};

struct rc_route
{
	jobject			rc_object;
	jclass 			rc_class;
	pj_thread_t * 	rc_thread;
	pj_bool_t		rc_finished;

	pjmedia_circ_buf* circ_buf;

	unsigned        firstOverflow;
	unsigned        overflowsCounter;
	pj_timestamp    lastOverflowTraceTime;


	pj_int16_t*		  frame_buf; //buffer has 1 frame size
	pj_mutex_t*		  buff_mutex;
	pj_sem_t*		  sync_sem;
	pj_timestamp 	  sample_counter;

	char  			  name[30];
	int				  buffer_size; //internal buffer size
	pj_timestamp 	  started;
};

/* Sound stream. */
struct rc_android_audio_stream
{
    pjmedia_aud_stream	 base;		    /**< Base stream	   */
    pjmedia_aud_param	 param;		    /**< Settings	       */
    pj_pool_t           *pool;          /**< Memory pool.      */
    void                *user_data;     /**< Application data. */

    pjmedia_aud_rec_cb   rec_cb;        /**< Capture callback. */
    pjmedia_aud_play_cb  play_cb;       /**< Playback function.*/

    int 				bytes_per_frame;

    struct rc_route	record;
    struct rc_route playback;

	pj_thread_t * 	rc_sync_thread;
	pj_bool_t		rc_sync_finished;


#if defined(PJMEDIA_HAS_SBACP_EC) && PJMEDIA_HAS_SBACP_EC!=0
	ShortReal 		  	*speaker_buf_in;
	ShortReal 		  	*microphone_buf_in[1];
	ShortReal		  	*speaker_buf_out;
	ShortReal		  	*microphone_buf_out;
	SBACP_Handle      	hSBACP;

    unsigned 			aec_samples_per_frame;
    unsigned 			aec_clock_rate;
    int 				aec_cycles;
#endif

#if defined(DRV_TRACE_INFO) && DRV_TRACE_INFO!=0
	struct streamTrace strace;
#endif
};

/* Operations */
static pjmedia_aud_dev_factory_op rc_audio_factory_op =
{
    &rc_audio_factory_init,
    &rc_audio_factory_destroy,
    &rc_audio_factory_get_dev_count,
    &rc_audio_factory_get_dev_info,
    &rc_audio_factory_default_param,
    &rc_audio_factory_create_stream
};

/* Sound stream operations. */
static pjmedia_aud_stream_op rc_audio_stream_op =
{
	&rc_audio_stream_get_param,
	&rc_audio_stream_get_cap,
	&rc_audio_stream_set_cap,
	&rc_audio_stream_start,
	&rc_audio_stream_stop,
	&rc_audio_stream_destroy
};

#if defined(DRV_TRACE_INFO) && DRV_TRACE_INFO!=0

/*
 *
 */

#define allocate_trace_buffer( abuf, buf ) buf = NULL;\
							abuf = jni_env->NewByteArray( DRV_TRACE_BUFFER_SIZE );\
							if( abuf == 0 )\
							{\
								ERROR_("Could not allocate trace buffer [android.media.AudioRecord]");\
								return PJ_ENOMEM;\
							}\
							buf = jni_env->GetByteArrayElements(abuf, 0);\
						    pj_bzero (buf, DRV_TRACE_BUFFER_SIZE);


#define free_trace_buffer( abuf, buf )  if( buf )\
		{\
			jni_env->ReleaseByteArrayElements( abuf, buf, 0 );\
			buf = NULL;\
		}

#define prepare_wave_header( f_file )	prepare_wav_header( rc_strm->param.clock_rate,\
		rc_strm->param.channel_count,\
		rc_strm->param.bits_per_sample,\
		logSize,\
		f_file )

/*
 *
 */
pj_status_t createTrace(struct streamTrace* strace){

	TRACE_("createTrace started");

	ATTACH_CURRENT_THREAD(PJ_EUNKNOWN);

	allocate_trace_buffer( strace->logBuffer_in_mic_array, strace->logBuffer_in_mic )
	allocate_trace_buffer( strace->logBuffer_out_mic_array, strace->logBuffer_out_mic )
	allocate_trace_buffer( strace->logBuffer_in_spk_array, strace->logBuffer_in_spk )
	allocate_trace_buffer( strace->logBuffer_out_spk_array, strace->logBuffer_out_spk )

	return PJ_SUCCESS;
}

/*
 *
 */
pj_status_t destroyTrace(struct streamTrace* strace){
	TRACE_("destroyTrace started");

	ATTACH_CURRENT_THREAD(PJ_EUNKNOWN);

	free_trace_buffer( strace->logBuffer_in_mic_array, strace->logBuffer_in_mic )
	free_trace_buffer( strace->logBuffer_out_mic_array, strace->logBuffer_out_mic )
	free_trace_buffer( strace->logBuffer_in_spk_array, strace->logBuffer_in_spk )
	free_trace_buffer( strace->logBuffer_out_spk_array, strace->logBuffer_out_spk )

	return PJ_SUCCESS;
}

pj_status_t openTrace(struct rc_android_audio_stream* rc_strm){

	TRACE_("openTrace started");

	char file_mic_in[64];
	char file_mic_out[64];
	char file_spk_in[64];
	char file_spk_out[64];

	sprintf( file_mic_in, "/sdcard/mic_in_%dKHz_%d_%dms.wav",
			rc_strm->param.clock_rate,
			rc_strm->param.samples_per_frame,
			rc_strm->param.ec_tail_ms );

	sprintf( file_mic_out, "/sdcard/mic_out_%dKHz_%d_%dms.wav",
			rc_strm->param.clock_rate,
			rc_strm->param.samples_per_frame,
			rc_strm->param.ec_tail_ms );

	sprintf( file_spk_in, "/sdcard/spk_in_%dKHz_%d_%dms.wav",
			rc_strm->param.clock_rate,
			rc_strm->param.samples_per_frame,
			rc_strm->param.ec_tail_ms );

	sprintf( file_spk_out, "/sdcard/spk_out_%dKHz_%d_%dms.wav",
			rc_strm->param.clock_rate,
			rc_strm->param.samples_per_frame,
			rc_strm->param.ec_tail_ms );

	rc_strm->strace.f_record_mic_in  = fopen( file_mic_in,  "wb" );
	rc_strm->strace.f_record_mic_out = fopen( file_mic_out, "wb" );
	rc_strm->strace.f_record_spk_in  = fopen( file_spk_in,  "wb" );
	rc_strm->strace.f_record_spk_out = fopen( file_spk_out, "wb" );

	return PJ_SUCCESS;
}

pj_status_t closeTrace(struct streamTrace* strace){

	TRACE_("closeTrace started");

	if( strace->f_record_mic_in )
		fclose(strace->f_record_mic_in);

	if( strace->f_record_mic_out )
		fclose(strace->f_record_mic_out);

	if( strace->f_record_spk_in )
		fclose(strace->f_record_spk_in);

	if( strace->f_record_spk_out )
		fclose(strace->f_record_spk_out);

	return PJ_SUCCESS;
}

#endif

/*
 *
 */
pjmedia_aud_dev_factory* pjmedia_rc_android_audio_dev_factory(pj_pool_factory *pf)
{
	pj_pool_t *pool;
	struct rc_android_audio_factory *f;

	/* create pool that will be used for audio driver memory allocations */
	pool = pj_pool_create(pf, "rc_android_audio_dev_f", 256, 64, NULL);
    PJ_ASSERT_RETURN(pool, PJ_ENOMEM);

	f = PJ_POOL_ZALLOC_T(pool, struct rc_android_audio_factory);
    PJ_ASSERT_RETURN(f, PJ_ENOMEM);

	f->pf = pf;
	f->pool = pool;
	f->base.op = &rc_audio_factory_op;

    TRACE_("RC Audio factory has been created");

	return &f->base;
}

/*
 * Initialize the audio device factory.
 */
static pj_status_t rc_audio_factory_init(pjmedia_aud_dev_factory *f)
{
	struct rc_android_audio_factory* rc_af = (struct rc_android_audio_factory*)f;

    PJ_ASSERT_RETURN(f, PJ_EINVAL);

	pj_ansi_strcpy(rc_af->dev_info.name, driver_name );

	rc_af->dev_info.default_samples_per_sec = 8000;
	rc_af->dev_info.caps = PJMEDIA_AUD_DEV_CAP_INPUT_LATENCY | PJMEDIA_AUD_DEV_CAP_OUTPUT_LATENCY ;
	rc_af->dev_info.input_count = 1;
	rc_af->dev_info.output_count = 1;

    TRACE_("RC Audio factory is initialized");

	return PJ_SUCCESS;
}

/*
 * Close this audio device factory and release all resources back to the operating system.
 */
static pj_status_t rc_audio_factory_destroy(pjmedia_aud_dev_factory *f)
{
	struct rc_android_audio_factory* rc_af = (struct rc_android_audio_factory*)f;
    PJ_ASSERT_RETURN(f, PJ_EINVAL);

	TRACE_("RC Audio factory is destroyed");

	pj_pool_t *pool = rc_af->pool;
	rc_af->pool = NULL;
    pj_pool_release(pool);

    return PJ_SUCCESS;
}

/*
 * Get the number of audio devices installed in the system.
 */
static unsigned rc_audio_factory_get_dev_count(pjmedia_aud_dev_factory *f)
{
	PJ_UNUSED_ARG(f);
    TRACE_("Get devices count");
	return 1;
}

/*
 * Get the audio device information and capabilities.
 */
static pj_status_t	rc_audio_factory_get_dev_info(pjmedia_aud_dev_factory *f,
											unsigned index,
											pjmedia_aud_dev_info *info)
{
	struct rc_android_audio_factory* rc_af = (struct rc_android_audio_factory*)f;

    PJ_ASSERT_RETURN(f, PJ_EINVAL);
    PJ_ASSERT_RETURN(index == 0, PJMEDIA_EAUD_INVDEV);

	pj_bzero(info, sizeof(*info));

    info->caps 						= rc_af->dev_info.caps;
    info->default_samples_per_sec 	= rc_af->dev_info.default_samples_per_sec;
    info->input_count 				= rc_af->dev_info.input_count;
    info->output_count 				= rc_af->dev_info.output_count;

	pj_ansi_strcpy(info->name, rc_af->dev_info.name );

	PJ_LOG(4,(THIS_FILE, "Get device info: caps:%d sample/sec: %d count In: %d Out:%d",
			info->caps,
			info->default_samples_per_sec,
			info->input_count,
			info->output_count ));


    return PJ_SUCCESS;
}

/*
 * Initialize the specified audio device parameter with the default
 * values for the specified device.
 */
static pj_status_t rc_audio_factory_default_param(pjmedia_aud_dev_factory *f,
											unsigned index,
											pjmedia_aud_param *param)
{
	struct rc_android_audio_factory* rc_af = (struct rc_android_audio_factory*)f;

    PJ_ASSERT_RETURN(f, PJ_EINVAL);
    PJ_ASSERT_RETURN(index == 0, PJMEDIA_EAUD_INVDEV);

    pj_bzero(param, sizeof(*param));

	if (rc_af->dev_info.input_count && rc_af->dev_info.output_count) {
		param->dir = PJMEDIA_DIR_CAPTURE_PLAYBACK;
		param->rec_id = index;
		param->play_id = index;
	} else if (rc_af->dev_info.input_count) {
		param->dir = PJMEDIA_DIR_CAPTURE;
		param->rec_id = index;
		param->play_id = PJMEDIA_AUD_INVALID_DEV;
	} else if (rc_af->dev_info.output_count) {
		param->dir = PJMEDIA_DIR_PLAYBACK;
		param->play_id = index;
		param->rec_id = PJMEDIA_AUD_INVALID_DEV;
	} else {
		return PJMEDIA_EAUD_INVDEV;
	}

	//DK: will use EC tail length as delay for AEC tests
	param->ec_tail_ms = pjsua_get_var()->media_cfg.ec_tail_len;
	param->sbacp_half_duplex = pjsua_get_var()->media_cfg.sbacp_half_duplex;

	param->rec_id 			 = index;
    param->play_id 			 = index;
    param->clock_rate 		 = rc_af->dev_info.default_samples_per_sec;

#if defined(PJMEDIA_HAS_SBACP_EC) && PJMEDIA_HAS_SBACP_EC!=0
    param->clock_rate 		 = SBACP_GetSampleRate();
#endif

    param->channel_count 	 = 1;
    param->samples_per_frame = (param->clock_rate * pjsua_get_var()->media_cfg.audio_frame_ptime )/ 1000;  //DK: 160 samples per frame
    param->bits_per_sample 	 = BITS_PER_SAMPLE;
    param->flags 			 = rc_af->dev_info.caps;
	param->input_latency_ms  = PJMEDIA_SND_DEFAULT_REC_LATENCY;
	param->output_latency_ms = PJMEDIA_SND_DEFAULT_PLAY_LATENCY;

	PJ_LOG(4,(THIS_FILE, "Factory default params: sample/frame %d sample/sec %d ec_tail=%d",
			param->samples_per_frame,
			param->clock_rate,
			param->ec_tail_ms));

	PJ_LOG(4,(THIS_FILE, "Factory default params: rec id: %d play id: %d bits/sample %d latency in: %d ms out: %d ms",
			param->rec_id,
			param->play_id,
			param->bits_per_sample,
			param->input_latency_ms,
			param->output_latency_ms ));

	return PJ_SUCCESS;
}

/*
 *
 */
static pj_status_t rc_initialize( pj_pool_t *pool,
		struct rc_route* rc,
		const char* name,
		const pjmedia_aud_param *param,
		int buffer_size_frames )
{
	pj_status_t status;

	rc->firstOverflow = 1;
	rc->overflowsCounter = 0;

	strncpy(rc->name, name, sizeof(rc->name) );
    status = pjmedia_circ_buf_create( pool,
    		buffer_size_frames * param->samples_per_frame,
    		&rc->circ_buf );

    if( status != PJ_SUCCESS ){
    	ERROR_("Could not create circular buffer");
    	return PJ_ENOMEM;
    }

    rc->frame_buf = (pj_int16_t*)pj_pool_zalloc( pool, sizeof(pj_int16_t) * param->samples_per_frame );
    if( !rc->frame_buf )
    	return PJ_ENOMEM;

    status = pj_mutex_create_simple( pool, name, &rc->buff_mutex );
    if( status != PJ_SUCCESS ){
    	PJ_LOG(1, (THIS_FILE, "Could not create mutex %s for buffer synchronization", name ));
    	return status;
    }

    status = pj_sem_create( pool,
    		name,
    		0,
    		1,
    		&rc->sync_sem );

    if( status != PJ_SUCCESS ){
    	PJ_LOG(1, (THIS_FILE, "Could not create semaphore %s for buffer synchronization", name ));
    	return status;
    }
    rc->sample_counter.u64 = 0;

	TRACE__("Initialized %s structure.", name );

    return PJ_SUCCESS;
}

/*
 * Note: JNI should be available before call
 */

int detectSdkVersion(JNIEnv *jni_env){

	jclass build_version_class;
	int sdk_version = SDK_UNKNOWN;

	build_version_class = jni_env->FindClass("android/os/Build$VERSION");
	if( build_version_class ){
		jfieldID field_SDK_INT = jni_env->GetStaticFieldID( build_version_class, "SDK_INT", "I" );
		if( field_SDK_INT ){
			sdk_version = jni_env->GetStaticIntField( build_version_class, field_SDK_INT );
		}
		else{
			TRACE_("Could not get static field id SDK_INT");

			sdk_version = SDK_CUPCAKE; //SDK_INT function is present from API level 4
		}
	}
	else{
		TRACE_("Could not find class android/os/Build$VERSION");
	}

	TRACE__("Current SDK version is %d", sdk_version );

	return sdk_version;
}

/*
 * Open the audio device and create audio stream.
 */
static pj_status_t rc_audio_factory_create_stream(pjmedia_aud_dev_factory *f,
											 const pjmedia_aud_param *param,
											 pjmedia_aud_rec_cb rec_cb,
											 pjmedia_aud_play_cb play_cb,
											 void *user_data,
											 pjmedia_aud_stream **p_aud_strm)
{
	PJ_LOG(2, (THIS_FILE,"rc_audio_factory_create_stream started."));

	struct rc_android_audio_factory* rc_af = (struct rc_android_audio_factory*)f;
	pj_pool_t *pool;
    struct rc_android_audio_stream* stream;
    pj_status_t status;
    int sdk_version = SDK_UNKNOWN;

    PJ_ASSERT_RETURN(f, PJ_EINVAL);
	PJ_ASSERT_RETURN(play_cb && rec_cb && p_aud_strm && param, PJ_EINVAL);

	PJ_ASSERT_RETURN( param->bits_per_sample == 8 || param->bits_per_sample == 16, PJMEDIA_EAUD_SAMPFORMAT);

	pool = pj_pool_create(rc_af->pf, "rc_android_audio_dev", 1024, 1024, NULL);
    PJ_ASSERT_RETURN(pool, PJ_ENOMEM);

    stream = PJ_POOL_ZALLOC_T(pool, struct rc_android_audio_stream);
    PJ_ASSERT_RETURN(stream, PJ_ENOMEM);

#if defined(DRV_TRACE_INFO) && DRV_TRACE_INFO!=0
    status = createTrace( &stream->strace );
    if( status != PJ_SUCCESS )
    	return status;
#endif

    status = rc_initialize( pool,  &stream->playback, "playback", param, PLAYBACK_BUFFER_SIZE_FRAMES );
    if( status != PJ_SUCCESS )
    	return status;

    status = rc_initialize( pool, &stream->record, "record", param, RECORD_BUFFER_SIZE_FRAMES  );
    if( status != PJ_SUCCESS )
    	return status;

    stream->pool 		= pool;
    stream->param 		= *param;
    stream->user_data 	= user_data;
    stream->rec_cb 		= rec_cb;
    stream->play_cb 	= play_cb;

    stream->record.rc_finished		= PJ_FALSE;
    stream->playback.rc_finished  	= PJ_FALSE;

	PJ_LOG(4, (THIS_FILE, "Create stream : %d samples/sec, %d samples/frame, %d bytes/sample [%d bits/sample] EC tail %d",
			param->clock_rate,
			param->samples_per_frame,
			param->bits_per_sample/8,
			param->bits_per_sample,
			param->ec_tail_ms ));

#if defined(PJMEDIA_HAS_SBACP_EC) && PJMEDIA_HAS_SBACP_EC!=0

	stream->speaker_buf_in = (ShortReal*)pj_pool_zalloc(pool, sizeof(ShortReal) * param->samples_per_frame );
    PJ_ASSERT_RETURN(stream->speaker_buf_in != NULL, PJ_ENOMEM);

    stream->microphone_buf_in[0] = (ShortReal*)pj_pool_zalloc(pool, sizeof(ShortReal) * param->samples_per_frame );
    PJ_ASSERT_RETURN(stream->microphone_buf_in[0] != NULL, PJ_ENOMEM);

    stream->speaker_buf_out = (ShortReal*)pj_pool_zalloc(pool, sizeof(ShortReal) * param->samples_per_frame );
    PJ_ASSERT_RETURN(stream->speaker_buf_out != NULL, PJ_ENOMEM);

    stream->microphone_buf_out = (ShortReal*)pj_pool_zalloc(pool, sizeof(ShortReal) * param->samples_per_frame );
    PJ_ASSERT_RETURN(stream->microphone_buf_out != NULL, PJ_ENOMEM);

    stream->hSBACP = SBACP_Create();
    if( stream->hSBACP == NULL ){
    	PJ_LOG(1,(THIS_FILE, "Create stream failed. SBACP_Create failed."));
    	return PJ_ENOMEM;
    }


    stream->aec_samples_per_frame	= SBACP_GetFrameSize();
    stream->aec_clock_rate			= SBACP_GetSampleRate();
    stream->aec_cycles = ( param->samples_per_frame > stream->aec_samples_per_frame ?
    		(param->samples_per_frame / stream->aec_samples_per_frame ) : 1 );

    stream->bytes_per_frame = param->samples_per_frame * ( param->bits_per_sample / 8 );

    SBACP_SetNumberOfMics( 1, stream->hSBACP );
    SBACP_SetBandLimitMode( POTS_4KHZ, stream->hSBACP );

    if (param->sbacp_half_duplex != 0) {
        PJ_LOG(4,(THIS_FILE, "HALF_DUPLEX mode"));
        SBACP_SetHalfDuplex(1, stream->hSBACP);
        SBACP_SetEnable(AEC_ENABLE, 0, stream->hSBACP );
    } else {
        PJ_LOG(4,(THIS_FILE, "FULL_DUPLEX mode"));
    }

    int delay = ( param->ec_tail_ms > 0 ? param->ec_tail_ms : AEC_DEFAULT_DELAY);
    PJ_LOG(4,(THIS_FILE, "AEC Delay %d [%d]", SBACP_GetAudioDelay( stream->hSBACP ), delay ));
    SBACP_SetAudioDelay( delay, stream->hSBACP );


#ifdef ANDREA_OUTPUT_LOG

    PJ_LOG(4,(THIS_FILE, "SBACP_GetVer   : %s", SBACP_GetVer()));
    PJ_LOG(4,(THIS_FILE, "SBACP_GetDate  : %s", SBACP_GetDate()));
    PJ_LOG(4,(THIS_FILE, "SBACP_GetProdID: %s", SBACP_GetProdID()));
    PJ_LOG(4,(THIS_FILE, "SBACP_GetFrameSize() : %d", SBACP_GetFrameSize() ));
    PJ_LOG(4,(THIS_FILE, "SBACP_GetSampleRate(): %d", SBACP_GetSampleRate() ));
    PJ_LOG(4,(THIS_FILE, "SBACP_GetAudioDelay(): %d", SBACP_GetAudioDelay( stream->hSBACP ) ));
    PJ_LOG(4,(THIS_FILE, "SBACP_GetHalfDuplex(): %d", SBACP_GetHalfDuplex( stream->hSBACP )));
    PJ_LOG(4,(THIS_FILE, "SBACP_GetTestMode()  : %d", SBACP_GetTestMode( stream->hSBACP )));
    PJ_LOG(4,(THIS_FILE, "SBACP_GetBandLimitMode(): %d", SBACP_GetBandLimitMode( stream->hSBACP )));

#if defined(SBACP_HAS_HighPassMode) && SBACP_HAS_HighPassMode!=0
    PJ_LOG(4,(THIS_FILE, "SBACP_GetHighPassMode: %d", SBACP_GetHighPassMode( stream->hSBACP ) ));
#endif


#define AEC_PARAMETER( NPRM, PRM ) PJ_LOG(4,(THIS_FILE, "SBACP_GetEnable(%s): %d", PRM, SBACP_GetEnable( NPRM, stream->hSBACP) ));

    AEC_PARAMETER( AEC_ENABLE,          "AEC_ENABLE" )
    AEC_PARAMETER( RESCUE_ENABLE,       "RESCUE_ENABLE" )
    AEC_PARAMETER( POSTFILTER_ENABLE,   "POSTFILTER_ENABLE" )
    AEC_PARAMETER( NOISE_RED_ENABLE,    "NOISE_RED_ENABLE" )
    AEC_PARAMETER( EQUALIZER_ENABLE,    "EQUALIZER_ENABLE" )
    AEC_PARAMETER( TX_AGC_ENABLE,       "TX_AGC_ENABLE" )
    AEC_PARAMETER( RX_AGC_ENABLE,       "RX_AGC_ENABLE" )
    AEC_PARAMETER( MIC_STEER_ENABLE,    "MIC_STEER_ENABLE" )
    AEC_PARAMETER( SPEECH_DET_ENABLE,   "SPEECH_DET_ENABLE" )
    AEC_PARAMETER( TX_MUTE_ENABLE,      "TX_MUTE_ENABLE" )
    AEC_PARAMETER( RX_MUTE_ENABLE,      "RX_MUTE_ENABLE" )
    AEC_PARAMETER( USER_TEST_ENABLE,    "USER_TEST_ENABLE" )
    AEC_PARAMETER( THD_CHECK_ENABLE,    "THD_CHECK_ENABLE" )
    /*
     * We does not have re-sampling possibility
     * that is why we should have the same clock rate value
     */
    PJ_ASSERT_RETURN( echo->aec_clock_rate == param->clock_rate, PJ_EINVAL);

#endif //ANDREA_OUTPUT_LOG

#endif // defined(PJMEDIA_HAS_SBACP_EC) && PJMEDIA_HAS_SBACP_EC!=0

	ATTACH_CURRENT_THREAD(PJ_EUNKNOWN);

	JNI_EXCEPTION_INIT();

	sdk_version = detectSdkVersion( jni_env );

	stream->record.started.u64 = 0;
	stream->playback.started.u64 = 0;

	int record_min_buffer_size = 0;
	int playback_min_buffer_size = 0;

	if( stream->param.dir & PJMEDIA_DIR_CAPTURE )
	{
		/* Get class android.media.AudioRecord*/
		stream->record.rc_class = jni_env->FindClass("android/media/AudioRecord");
		JNI_EXCEPTION_CHECK(true);

		if( IF_JNI_EXCEPTION() || stream->record.rc_class == 0 ){
			ERROR_("Could not get class for android.media.AudioRecord");
			goto leave;
		}

		//detect record minimal buffer size
		jmethodID record_metod_id_getMinBufferSize = jni_env->GetStaticMethodID( stream->record.rc_class, "getMinBufferSize", "(III)I" );
		JNI_EXCEPTION_CHECK(true);

		if( IF_JNI_EXCEPTION() || record_metod_id_getMinBufferSize == 0 ){
			ERROR_("Could not get method ID of getMinBufferSize [android.media.AudioRecord]");
			goto leave;
		}

		record_min_buffer_size = jni_env->CallStaticIntMethod( stream->record.rc_class,
															record_metod_id_getMinBufferSize,
															param->clock_rate,
															( sdk_version >= SDK_ECLAIR ? CHANNEL_IN_MONO : CHANNEL_CONFIGURATION_MONO ),
															(param->bits_per_sample == 8 ? ENCODING_PCM_8BIT : ENCODING_PCM_16BIT ));

		JNI_EXCEPTION_CHECK(true);
		if( IF_JNI_EXCEPTION() || record_min_buffer_size < 0 ){
			ERROR_("Could not detect record MinBufferSize");
			goto leave;
		}

		TRACE__("Initial minimal buffer size is %d", record_min_buffer_size );

		if( record_min_buffer_size < MIN_AUDIO_BUFFER_SIZE ){
			record_min_buffer_size = MIN_AUDIO_BUFFER_SIZE;
		}

		if( ( record_min_buffer_size % stream->bytes_per_frame ) != 0 ){
		    record_min_buffer_size = (( record_min_buffer_size / stream->bytes_per_frame ) + 1 ) * stream->bytes_per_frame;
		}
		stream->record.buffer_size =  record_min_buffer_size;

		PJ_LOG(4, (THIS_FILE, "Size of record buffer changed to %d bytes; %d frames; %d ms",
		        stream->record.buffer_size,
		        (stream->record.buffer_size/stream->bytes_per_frame),
		        ((stream->record.buffer_size/stream->bytes_per_frame) * 20)));
	}

	if( stream->param.dir & PJMEDIA_DIR_PLAYBACK )
	{
		/* Get class android.media.AudioTrack*/
		stream->playback.rc_class = jni_env->FindClass("android/media/AudioTrack");
		JNI_EXCEPTION_CHECK(true);

		if( IF_JNI_EXCEPTION() || stream->playback.rc_class == 0 ){
			ERROR_("Could not get class for android.media.AudioTrack");
			goto leave;
		}

		//detect playback minimal buffer size
		jmethodID playback_metod_id_getMinBufferSize = jni_env->GetStaticMethodID( stream->playback.rc_class, "getMinBufferSize", "(III)I" );
		JNI_EXCEPTION_CHECK(true);

		if( IF_JNI_EXCEPTION() || playback_metod_id_getMinBufferSize == 0 ){
			ERROR_("Could not get method ID of getMinBufferSize [android.media.AudioTrack]");
			goto leave;
		}

		playback_min_buffer_size = jni_env->CallStaticIntMethod( stream->playback.rc_class,
															playback_metod_id_getMinBufferSize,
															param->clock_rate,
															( sdk_version >= SDK_ECLAIR ? CHANNEL_OUT_MONO : CHANNEL_CONFIGURATION_MONO ),
															(param->bits_per_sample == 8 ? ENCODING_PCM_8BIT : ENCODING_PCM_16BIT ) );

		JNI_EXCEPTION_CHECK(true);

		if( IF_JNI_EXCEPTION() || playback_min_buffer_size < 0 ){
			ERROR_("Could not detect playback MinBufferSize");
			goto leave;
		}

		TRACE__("Initial Playback minimal buffer size is %d", playback_min_buffer_size );

		if( ( playback_min_buffer_size % stream->bytes_per_frame ) != 0 ){
			playback_min_buffer_size = (( playback_min_buffer_size / stream->bytes_per_frame ) + 1 ) * stream->bytes_per_frame;
		}

		stream->playback.buffer_size = playback_min_buffer_size;

		PJ_LOG(4, (THIS_FILE, "Size of Playback buffer changed to %d bytes; %d frames; %d ms", stream->playback.buffer_size,
		        (stream->playback.buffer_size/stream->bytes_per_frame),  ((stream->playback.buffer_size/stream->bytes_per_frame)*20)));
	}

	if( stream->param.dir & PJMEDIA_DIR_PLAYBACK )
	{
		TRACE_("Initialize playback device");

		jmethodID playback_method_id_constructor =  jni_env->GetMethodID( stream->playback.rc_class, "<init>", "(IIIIII)V" );
		JNI_EXCEPTION_CHECK(true);

		if( IF_JNI_EXCEPTION() || playback_method_id_constructor == 0 ){
			ERROR_("Could not detect method ID for playback constructor");
			goto leave;
		}

		jobject playback_object_local = jni_env->NewObject( stream->playback.rc_class,
															playback_method_id_constructor,
															STREAM_VOICE_CALL,
															param->clock_rate,
															( sdk_version >= SDK_ECLAIR ? CHANNEL_OUT_MONO : CHANNEL_CONFIGURATION_MONO ),
															(param->bits_per_sample == 8 ? ENCODING_PCM_8BIT : ENCODING_PCM_16BIT ),
															stream->playback.buffer_size,
															MODE_STREAM
															);

		JNI_EXCEPTION_CHECK(true);

		if( playback_object_local ){
			stream->playback.rc_object = jni_env->NewGlobalRef(playback_object_local);
		}

		if( IF_JNI_EXCEPTION() || !playback_object_local ){
			ERROR_("Could not create playback object");
			goto leave;
		}

		jmethodID playback_method_id_get_state   = jni_env->GetMethodID( stream->playback.rc_class, "getState", "()I" );
	    if( playback_method_id_get_state != 0 )
	    {
	    	int playback_state = jni_env->CallIntMethod(playback_object_local, playback_method_id_get_state );
	    	PJ_LOG(4, (THIS_FILE, "Playback getState %d", playback_state ));

	    	if( playback_state != STATE_INITIALIZED ){
				ERROR_("Playback was not initialized properly.");
				goto leave;
	    	}
	    }

		TRACE_("Playback device has been initialized successfully");
	}

	if( stream->param.dir & PJMEDIA_DIR_CAPTURE )
	{
		TRACE_("Initialize record device");
		jmethodID record_method_id_constructor =  jni_env->GetMethodID( stream->record.rc_class, "<init>", "(IIIII)V" );
		JNI_EXCEPTION_CHECK(true);

		if( IF_JNI_EXCEPTION() || record_method_id_constructor == 0 ){
			ERROR_("Could not detect method ID for record constructor");
			goto leave;
		}

		jobject record_object_local = jni_env->NewObject( stream->record.rc_class,
															record_method_id_constructor,
															MediaRecorder_AudioSource_MIC,
															param->clock_rate,
															( sdk_version >= SDK_ECLAIR ? CHANNEL_IN_MONO : CHANNEL_CONFIGURATION_MONO ),
															(param->bits_per_sample == 8 ? ENCODING_PCM_8BIT : ENCODING_PCM_16BIT ),
															stream->record.buffer_size
															);

		JNI_EXCEPTION_CHECK(true);

		if( record_object_local ){
			stream->record.rc_object = jni_env->NewGlobalRef(record_object_local);
		}

		if( IF_JNI_EXCEPTION() || !record_object_local ){
			ERROR_("Could not create record object");
			goto leave;
		}

		jmethodID record_method_id_get_state   = jni_env->GetMethodID( stream->record.rc_class, "getState", "()I" );
	    if( record_method_id_get_state != 0 )
	    {
	    	int record_state = jni_env->CallIntMethod(record_object_local, record_method_id_get_state );
	    	PJ_LOG(4, (THIS_FILE, "Record getState %d", record_state ));

	    	if( record_state != STATE_INITIALIZED ){
				ERROR_("Record was not initialized properly.");
				goto leave;
	    	}
	    }

		TRACE_("Record device has been initialized successfully");
	}


	*p_aud_strm = &stream->base;
	(*p_aud_strm)->op = &rc_audio_stream_op;

	DETACH_CURRENT_THREAD();

	return PJ_SUCCESS;
leave:

	rc_audio_stream_destroy( (pjmedia_aud_stream*)stream );

	DETACH_CURRENT_THREAD();

 return PJ_EUNKNOWN;
}

/* API: Get the running parameters for the specified audio stream. */
static pj_status_t rc_audio_stream_get_param(pjmedia_aud_stream *stream,
						 pjmedia_aud_param *param)
{
	TRACE_("rc_audio_stream_get_param started.");

	struct rc_android_audio_stream *rc_strm = (struct rc_android_audio_stream*)stream;
    PJ_ASSERT_RETURN(stream && param, PJ_EINVAL);

    pj_memcpy(param, &rc_strm->param, sizeof(*param));

    return PJ_SUCCESS;
}

/* API: Get the value of a specific capability of the audio stream. */
static pj_status_t rc_audio_stream_get_cap(pjmedia_aud_stream *stream,
					       pjmedia_aud_dev_cap cap,
					       void *value)
{
	TRACE_("rc_audio_stream_get_cap started.");

	PJ_UNUSED_ARG(stream);
	PJ_UNUSED_ARG(cap);
	PJ_UNUSED_ARG(value);

	return PJ_ENOTSUP;
}


/* API: Set the value of a specific capability of the audio stream. */
static pj_status_t rc_audio_stream_set_cap(pjmedia_aud_stream *stream,
					       pjmedia_aud_dev_cap cap,
					       const void *value)
{
	TRACE_("rc_audio_stream_set_cap started.");

	PJ_UNUSED_ARG(stream);
	PJ_UNUSED_ARG(cap);
	PJ_UNUSED_ARG(value);

	return PJ_ENOTSUP;
}

/*
 *
 */
int get_buffer_len( struct rc_route* rc ){
	int buf_size = 0;

	pj_mutex_lock( rc->buff_mutex );
	buf_size =  pjmedia_circ_buf_get_len(rc->circ_buf);
	pj_mutex_unlock( rc->buff_mutex );

	return buf_size;
}
/*
 *
 */
pj_status_t get_from_buffer( struct rc_route* rc,
		pj_int16_t* buffer,
		int samples_per_frame,
		unsigned* psize )
{
	pj_status_t status = PJ_ETOOBIG;
	unsigned buf_size = 0;

	pj_mutex_lock( rc->buff_mutex );

	buf_size =  pjmedia_circ_buf_get_len(rc->circ_buf);
	if( psize )
		*psize = buf_size;

	if( buf_size > 0 ){
		status = pjmedia_circ_buf_read( rc->circ_buf, buffer, samples_per_frame );
	}

	pj_mutex_unlock( rc->buff_mutex );

	if( status != PJ_SUCCESS && status != PJ_ETOOBIG ){
		//PJ_LOG(4, (THIS_FILE, "get_from_buffer '%s'error %d size %d", rc->name, status, buf_size ));
	}
	return status;
}

/*
 *
 */
int is_buffer_full(struct rc_route* rc, int buff_capacity )
{
	unsigned buff_len = 0;

	pj_mutex_lock( rc->buff_mutex );

	buff_len  = pjmedia_circ_buf_get_len(rc->circ_buf);

	pj_mutex_unlock( rc->buff_mutex );

	return ( buff_len == buff_capacity ? 1 : 0 );
}
/*
 *
 */
pj_status_t put_to_buffer( struct rc_route* rc,
		pj_int16_t*	buffer,
		int samples_per_frame,
		unsigned* psize )
{
	pj_status_t status;

	pj_mutex_lock( rc->buff_mutex );

	status = pjmedia_circ_buf_write( rc->circ_buf, buffer, samples_per_frame );
	if( status == PJ_SUCCESS && psize ){
		*psize = pjmedia_circ_buf_get_len(rc->circ_buf);
	}

	pj_mutex_unlock( rc->buff_mutex );
	if( status == PJ_SUCCESS ){
		pj_sem_post( rc->sync_sem );
	}
	else{
	    if (status == PJ_ETOOBIG) {
	         if (rc->firstOverflow) {
                pj_get_timestamp(&rc->lastOverflowTraceTime);
                rc->firstOverflow = 0;
                rc->overflowsCounter = 0;
                PJ_LOG(4, (THIS_FILE, "put_to_buffer '%s' error %d (overflow)", rc->name, status ));
            } else {
                pj_timestamp current_time;
                pj_get_timestamp(&current_time);
                int overflow_log_elapsed_time = pj_elapsed_msec(&rc->lastOverflowTraceTime, &current_time);
                if (overflow_log_elapsed_time > BUFFER_OVERFLOW_PERIODIC_TRACES_TIMEOUT) {
                    unsigned errors_num = rc->overflowsCounter + 1;
                    PJ_LOG(4, (THIS_FILE, "put_to_buffer '%s' error %d (overflow) x %d last %d ms",
                                    rc->name, status, errors_num, overflow_log_elapsed_time ));
                    pj_get_timestamp(&rc->lastOverflowTraceTime);
                    rc->overflowsCounter = 0;
                } else {
                    rc->overflowsCounter++;
                }
            }
        } else {
            PJ_LOG(3, (THIS_FILE, "put_to_buffer '%s' error %d", rc->name, status ));
        }
	}

	return status;
}

/*
 *
 */
pj_status_t read_from_rtp( pj_int16_t * buffer,
		int buffer_size,
		pj_timestamp frame_counter,
		struct rc_android_audio_stream *rc_strm )
{
	pjmedia_frame frame;

	pj_bzero (buffer, buffer_size);

	frame.type = PJMEDIA_FRAME_TYPE_AUDIO;
	frame.size = buffer_size;
	frame.buf = (void *) buffer;
	frame.timestamp.u64 = frame_counter.u64;
	frame.bit_info = 0;

	//get data from PJSIP level
	pj_status_t status = (*rc_strm->play_cb)(rc_strm->user_data, &frame);
	if( status != PJ_SUCCESS )
	{
		ERROR_("play_cb failed.");
		return status;
	}

	if ( frame.type != PJMEDIA_FRAME_TYPE_AUDIO )
	{
		TRACE_( "Invalid type of frame or empty buffer" );
		return PJ_FALSE;
	}

	return status;
}
/*
 *
 */
/*
 *
 */
pj_status_t write_to_rtp( pj_int16_t * buffer,
		int buffer_size,
		pj_timestamp frame_counter,
		struct rc_android_audio_stream *rc_strm )
{
	pjmedia_frame frame;

	frame.type = PJMEDIA_FRAME_TYPE_AUDIO;
	frame.buf = (void*) buffer;
	frame.size = buffer_size;
	frame.bit_info = 0;
	frame.timestamp.u64 = frame_counter.u64;

	pj_status_t status = (*rc_strm->rec_cb)(rc_strm->user_data, &frame );

	if( status != PJ_SUCCESS )
	{
		ERROR_("Call rec_cb failed. [android.media.AudioRecord]");
	}

	return status;
}

/*
 *
 */
static int PJ_THREAD_FUNC rc_sync_working_thread(void* param)
{
    struct rc_android_audio_stream *rc_strm = (struct rc_android_audio_stream*)param;

	TRACE_("rc_sync_working_thread started");
	int read_buffer_size = 0;
	pj_timestamp samples_per_cycle;
	struct pjmedia_frame frame;
	pj_status_t status;

	unsigned play_buf_size = 0;
	unsigned rec_buf_size = 0;
	int buffer_full = 0;

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
	/*
	 * [0] - start
	 * [1] - after buffer get
	 * [2] - after RTP get
	 * [3] - after AEC
	 * [4] - after buffer put
	 * [5] - after RTP put
	 */
	pj_timestamp interval_start[6];

	int interval_max = 0;
	int interval_min = 0;
	int interval = 0;
	int frame_count = 0;
	int playback_frames_discarded = 0;

	/*
	 * 0 - get
	 * 1 - RTP get
	 * 2 - AEC
	 * 3 - put
	 * 4 - RTP put
	 */
	int cycle_time[5] 	= {0,0,0,0,0};
	int sum_time[5] 	= {0,0,0,0,0};
#endif

#if defined(DRV_TRACE_INFO) && DRV_TRACE_INFO!=0
	int logSize = 0;
	openTrace( rc_strm );
#endif

	samples_per_cycle.u64 = (rc_strm->param.samples_per_frame / rc_strm->param.channel_count );

	setpriority(PRIO_PROCESS, 0, ANDROID_PRIORITY_URGENT_AUDIO);

	while( rc_strm->rc_sync_finished != PJ_TRUE )
	{
		pj_sem_wait( rc_strm->record.sync_sem );

		if( rc_strm->rc_sync_finished == PJ_TRUE ){
            PJ_LOG(4, (THIS_FILE, "Sync thread. Stop signal has been detected" ));
			break;
		}

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
			pj_get_timestamp(&interval_start[0]);
#endif

			status = get_from_buffer( &rc_strm->record, rc_strm->record.frame_buf, rc_strm->param.samples_per_frame, &rec_buf_size );

			if( status != PJ_SUCCESS ){
	    		continue;
			}

			if( rc_strm->rc_sync_finished == PJ_TRUE ){
	            PJ_LOG(4, (THIS_FILE, "Sync thread. Stop signal has been detected" ));
				break;
			}

			//PJ_LOG(4, (THIS_FILE, "Sync get from REC buffer size=%d", rec_buf_size ));


#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
			pj_get_timestamp(&interval_start[1]);
#endif

			read_buffer_size = rc_strm->param.samples_per_frame * sizeof(pj_int16_t);

			status = read_from_rtp(rc_strm->playback.frame_buf,
					read_buffer_size,
					rc_strm->playback.sample_counter,
					rc_strm );

			if( status != PJ_SUCCESS ){
				PJ_LOG(4, (THIS_FILE, "Sync read_from_rtp error %d", status ));
			}


#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
			pj_get_timestamp(&interval_start[2]);
#endif
			if( status == PJ_SUCCESS ){
				rc_strm->playback.sample_counter.u64 += samples_per_cycle.u64;
			}

			/*
			 * We have both Record & RTP playback frames
			 * Make full cycle included AEC conversion
			 */

			buffer_full = is_buffer_full( &rc_strm->playback, PLAYBACK_BUFFER_SIZE_FRAMES * rc_strm->param.samples_per_frame );

			if( status == PJ_SUCCESS && !buffer_full )
			{

			 #if defined(DRV_TRACE_INFO) && DRV_TRACE_INFO!=0
				if( logSize + read_buffer_size < DRV_TRACE_BUFFER_SIZE ){
					if( rc_strm->strace.logBuffer_in_mic)
						memcpy( &rc_strm->strace.logBuffer_in_mic[logSize], rc_strm->record.frame_buf, read_buffer_size );
					if( rc_strm->strace.logBuffer_in_spk )
						memcpy( &rc_strm->strace.logBuffer_in_spk[logSize], rc_strm->playback.frame_buf, read_buffer_size );
				}
			 #endif

#if defined(PJMEDIA_HAS_SBACP_EC) && PJMEDIA_HAS_SBACP_EC!=0 && defined(PJMEDIA_USE_SBACP_EC)

					/* Prepare microphone samples for EC*/
					for( int i = 0; i < rc_strm->param.samples_per_frame; i++ ){
						rc_strm->microphone_buf_in[0][i] 	= (ShortReal)((pj_int16_t*)rc_strm->record.frame_buf)[i];
						rc_strm->speaker_buf_in[i] 			= (ShortReal)((pj_int16_t*)rc_strm->playback.frame_buf)[i];
					}

					ShortReal* offset[1];
					for( int idx = 0; idx < rc_strm->aec_cycles; idx++ ){

						offset[0] = &rc_strm->microphone_buf_in[0][idx*rc_strm->aec_samples_per_frame];

						SBACP_ProcessFrame( &offset[0],
								&rc_strm->speaker_buf_in[idx*rc_strm->aec_samples_per_frame],
								&rc_strm->microphone_buf_out[idx*rc_strm->aec_samples_per_frame],
								&rc_strm->speaker_buf_out[idx*rc_strm->aec_samples_per_frame],
								rc_strm->hSBACP );
					}

					for( int i = 0; i < rc_strm->param.samples_per_frame; i++ ){
						((pj_int16_t*)rc_strm->record.frame_buf)[i] = (pj_int16_t)rc_strm->microphone_buf_out[i];
						((pj_int16_t*)rc_strm->playback.frame_buf)[i] = (pj_int16_t)rc_strm->speaker_buf_out[i];
					}

#endif


				#if defined(DRV_TRACE_INFO) && DRV_TRACE_INFO!=0
					if( logSize + read_buffer_size < DRV_TRACE_BUFFER_SIZE ){
						if( rc_strm->strace.logBuffer_out_mic )
							memcpy( &rc_strm->strace.logBuffer_out_mic[logSize], rc_strm->record.frame_buf, read_buffer_size );
						if( rc_strm->strace.logBuffer_out_spk )
							memcpy( &rc_strm->strace.logBuffer_out_spk[logSize], rc_strm->playback.frame_buf, read_buffer_size );
						logSize += read_buffer_size;
					}
				#endif

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
					pj_get_timestamp(&interval_start[3]);
#endif

				  status = put_to_buffer( &rc_strm->playback,
						  rc_strm->playback.frame_buf,
						  rc_strm->param.samples_per_frame,
						  &play_buf_size );

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
				  pj_get_timestamp(&interval_start[4]);
#endif
			}
			else{

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
				if( buffer_full )
					playback_frames_discarded++;

				pj_get_timestamp(&interval_start[3]);
				pj_get_timestamp(&interval_start[4]);
#endif
			}

            status = write_to_rtp( rc_strm->record.frame_buf,
                		rc_strm->param.samples_per_frame,
                		rc_strm->record.sample_counter,
                		rc_strm );

            if( status != PJ_SUCCESS ){
				PJ_LOG(4, (THIS_FILE, "Sync write_to_rtp error %d", status ));
            }

            rc_strm->record.sample_counter.u64 += samples_per_cycle.u64;

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
            pj_get_timestamp(&interval_start[5]);
            interval = pj_elapsed_msec( &interval_start[0], &interval_start[5] );
            frame_count++;

            cycle_time[0] = pj_elapsed_msec( &interval_start[0], &interval_start[1] );
            cycle_time[1] = pj_elapsed_msec( &interval_start[1], &interval_start[2] );
            cycle_time[2] = pj_elapsed_msec( &interval_start[2], &interval_start[3] );
            cycle_time[3] = pj_elapsed_msec( &interval_start[3], &interval_start[4] );
            cycle_time[4] = pj_elapsed_msec( &interval_start[4], &interval_start[5] );

            sum_time[0] += cycle_time[0];
            sum_time[1] += cycle_time[1];
            sum_time[2] += cycle_time[2];
            sum_time[3] += cycle_time[3];
            sum_time[4] += cycle_time[4];

            interval_min = (interval_min == 0 ? interval : PJ_MIN(interval_min, interval) );

            if( interval > interval_max  ){
            	interval_max =  interval;
            	PJ_LOG(4, (THIS_FILE, "Sync max updated=%d get_buff=%d get_rtp=%d aec=%d put_buf=%d put_rtp=%d frames=%d dscd=%d",
            			interval,
            			cycle_time[0],
            			cycle_time[1],
            			cycle_time[2],
            			cycle_time[3],
            			cycle_time[4],
            			frame_count,
            			playback_frames_discarded ));
            }

#endif

	}

leave:
#if defined(DRV_TRACE_INFO) && DRV_TRACE_INFO!=0
	if( logSize > 0 ){
		if( prepare_wave_header(rc_strm->strace.f_record_mic_in) ){
			fwrite( rc_strm->strace.logBuffer_in_mic,  logSize, 1, rc_strm->strace.f_record_mic_in );
		}

		if( prepare_wave_header(rc_strm->strace.f_record_mic_out) ){
			fwrite( rc_strm->strace.logBuffer_out_mic, logSize, 1, rc_strm->strace.f_record_mic_out );
		}

		if( prepare_wave_header(rc_strm->strace.f_record_spk_in) ){
			fwrite( rc_strm->strace.logBuffer_in_spk,  logSize, 1, rc_strm->strace.f_record_spk_in );
		}

		if( prepare_wave_header(rc_strm->strace.f_record_spk_out)){
			fwrite( rc_strm->strace.logBuffer_out_spk, logSize, 1, rc_strm->strace.f_record_spk_out );
		}
	}

	closeTrace( &rc_strm->strace );
#endif

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
	PJ_LOG(4, (THIS_FILE, "Sync interval [ms] Min=%d Max=%d Frames sum=%d discard=%d",
			interval_min,
			interval_max,
			frame_count,
			playback_frames_discarded ));

	PJ_LOG(4, (THIS_FILE, "Sync sum time get_buff=%d get_rtp=%d aec=%d [%d] put_buf=%d put_rtp=%d sum=%d [%d] frames=%d dscd=%d",
			sum_time[0],
			sum_time[1],
			sum_time[2],
			sum_time[2]/( frame_count - playback_frames_discarded ),
			sum_time[3],
			sum_time[4],
			sum_time[0] + sum_time[1] + sum_time[2] + sum_time[3] + sum_time[4],
            (sum_time[0] + sum_time[1] + sum_time[2] + sum_time[3] + sum_time[4])/( frame_count - playback_frames_discarded ),
			frame_count,
			playback_frames_discarded ));

#endif

	rc_strm->rc_sync_finished = PJ_TRUE;

	TRACE_("rc_sync_working_thread finished");
	return 0;
}

/*
 *
 */
static int PJ_THREAD_FUNC rc_record_working_thread(void* param)
{
    struct rc_android_audio_stream *rc_strm = (struct rc_android_audio_stream*)param;

    TRACE_("rc_record_working_thread started");

    ATTACH_CURRENT_THREAD(PJ_EUNKNOWN);

    jmethodID record_method_id_startRecording, record_method_id_stop, record_method_id_read;
    int buffer_size;
    jbyteArray readBuffer_array;
    jbyte* readBuffer;
    pj_status_t status;

    int loop_index = 0;

	JNI_EXCEPTION_INIT()

    int frames_by_request = 1;

    int frame_size = rc_strm->param.samples_per_frame * (rc_strm->param.bits_per_sample/8);
    int buffer_load_interval =  (rc_strm->param.samples_per_frame / ( rc_strm->param.clock_rate / 1000 )) * frames_by_request;
    int delay = 0;

#if defined(AUDIO_RATES_PERIODIC_TRACES) && AUDIO_RATES_PERIODIC_TRACES!=0
    pj_timestamp t_record_start;
    pj_timestamp t_record_last_rate_trace;
    int t_record_processed_frames;
#endif

    PJ_LOG(4, (THIS_FILE, "rc_record_working_thread load_buffer_time=%d", buffer_load_interval ));

    int interval = 0;
	unsigned rec_buff_size = 0;

    pj_timestamp samples_per_cycle;
    pj_timestamp interval_start_full;
    pj_timestamp interval_finish_full;
    int frame_count = 0;

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
    int interval_max = 0;
    int interval_min = 0;
    int record_frames_lost=0;

    int frame_for_period = 0;
    int period_duration = 0;
#endif

    PJ_LOG(4, (THIS_FILE, "rc_record_working_thread clock_rate=%d samples_frame=%d bytes_sample=%d bits_sample=%d",
            rc_strm->param.clock_rate,
            rc_strm->param.samples_per_frame,
            rc_strm->param.bits_per_sample/8,
            rc_strm->param.bits_per_sample ));

    buffer_size =  frame_size * frames_by_request;
    samples_per_cycle.u64 = (rc_strm->param.samples_per_frame / rc_strm->param.channel_count );

    record_method_id_startRecording     = jni_env->GetMethodID( rc_strm->record.rc_class, "startRecording", "()V" );
    if( record_method_id_startRecording == 0 )
    {
        ERROR_("Could not get method ID of startRecording [android.media.AudioRecord]");
        goto leave;
    }

    record_method_id_stop   = jni_env->GetMethodID( rc_strm->record.rc_class, "stop", "()V" );
    if( record_method_id_stop == 0 )
    {
        ERROR_("Could not get method ID of stop [android.media.AudioRecord]");
        goto leave;
    }

    record_method_id_read   = jni_env->GetMethodID( rc_strm->record.rc_class, "read", "([BII)I" );
    if( record_method_id_read == 0 )
    {
        ERROR_("Could not get method ID of stop [android.media.AudioRecord]");
        goto leave;
    }


    readBuffer_array = jni_env->NewByteArray( buffer_size );
    if( readBuffer_array == 0 )
    {
        ERROR_("Could not allocate read buffer [android.media.AudioRecord]");
        goto leave;
    }

    readBuffer = jni_env->GetByteArrayElements(readBuffer_array, 0);

    setpriority(PRIO_PROCESS, 0, ANDROID_PRIORITY_AUDIO ) ;

    //start record
    jni_env->CallVoidMethod( rc_strm->record.rc_object, record_method_id_startRecording );
    JNI_EXCEPTION_CHECK(true)

    pj_get_timestamp(&interval_start_full);

#if defined(AUDIO_RATES_PERIODIC_TRACES) && AUDIO_RATES_PERIODIC_TRACES!=0
    pj_get_timestamp(&t_record_start);
    t_record_processed_frames =0;
    pj_get_timestamp(&t_record_last_rate_trace);
#endif

    pj_get_timestamp( &rc_strm->record.started );
    PJ_LOG(4, (THIS_FILE, "Recorder is ready to process frames" ));

    while( !IF_JNI_EXCEPTION() && rc_strm->record.rc_finished != PJ_TRUE )
    {
        int real_read = jni_env->CallIntMethod(rc_strm->record.rc_object,
                                            record_method_id_read,
                                            readBuffer_array,
                                            0,
                                            buffer_size);
        JNI_EXCEPTION_CHECK(true);

        if( IF_JNI_EXCEPTION() || real_read < 0 )
        {
            TRACE__("Read failed. %d [android.media.AudioRecord]", real_read );
            break;
        }

        if( real_read < buffer_size )
        {
            PJ_LOG( 3, (THIS_FILE, "Read small data %d chunk. Should %d", real_read, buffer_size ));
        }

        if( rc_strm->record.rc_finished == PJ_TRUE ){
            PJ_LOG(4, (THIS_FILE, "Recorder thread. Stop signal has been detected" ));
        	break;
        }

        for( loop_index = 0; loop_index < (real_read / frame_size); loop_index++ ){
            //check was player started?
            if( rc_strm->playback.started.u64 != 0 ){ //yes, he did

                status = put_to_buffer( &rc_strm->record,
    				(pj_int16_t *)&readBuffer[loop_index*frame_size],
                    rc_strm->param.samples_per_frame,
    				&rec_buff_size );

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
                if( status == PJ_ETOOBIG ){
                    record_frames_lost++;
                }
#endif
            }
            else{ //not yet - put frames directly to RTP

                status = write_to_rtp( (pj_int16_t *)&readBuffer[loop_index*rc_strm->param.samples_per_frame],
                            rc_strm->param.samples_per_frame,
                            rc_strm->record.sample_counter,
                            rc_strm );

                if( status != PJ_SUCCESS ){
                    PJ_LOG(4, (THIS_FILE, "Record write_to_rtp error %d", status ));
                }

                rc_strm->record.sample_counter.u64 += samples_per_cycle.u64;
            }
        }

        frame_count += (real_read / frame_size);


#if defined(AUDIO_RATES_PERIODIC_TRACES) && AUDIO_RATES_PERIODIC_TRACES!=0
        t_record_processed_frames += (real_read / frame_size);
#endif


#if defined(AUDIO_RATES_PERIODIC_TRACES) && AUDIO_RATES_PERIODIC_TRACES!=0
        {
            pj_timestamp t_record_rate_current_time;
            pj_get_timestamp(&t_record_rate_current_time);
            int t_record_rate_elapsed_time = pj_elapsed_msec(&t_record_last_rate_trace, &t_record_rate_current_time);

            if (t_record_rate_elapsed_time > AUDIO_RATES_PERIODIC_TRACES_TIMEOUT) {
                t_record_rate_elapsed_time = pj_elapsed_msec( &t_record_start, &t_record_rate_current_time);

                int t_record_rate_frame_time = (rc_strm->param.samples_per_frame / (rc_strm->param.clock_rate / 1000));

                int t_record_rate_frames = t_record_rate_elapsed_time / t_record_rate_frame_time;
                int t_record_rate_drift = t_record_rate_frames - t_record_processed_frames;
                if (abs(t_record_rate_drift) > 2) {
                    PJ_LOG(4,
                            (THIS_FILE, "Record (rate): time=%dms, frames=%d(%d), drift=%d; frame_time=%dms, (last_write_at=%d, should_be_at=%dms)",
                                    t_record_rate_elapsed_time, t_record_processed_frames, t_record_rate_frames, t_record_rate_drift,
                                    t_record_rate_frame_time, t_record_rate_elapsed_time + t_record_rate_frame_time,
                                    (t_record_rate_frame_time * (t_record_processed_frames - 1))));
                } else {
                    PJ_LOG(4,
                            (THIS_FILE, "Record (rate): time=%dms, frames=%d(%d), frame_time=%dms, (last_write_at=%d, should_be_at=%dms)",
                                    t_record_rate_elapsed_time, t_record_processed_frames, t_record_rate_frames,
                                    t_record_rate_frame_time, t_record_rate_elapsed_time + t_record_rate_frame_time,
                                    (t_record_rate_frame_time * (t_record_processed_frames - 1))));
                }
                pj_get_timestamp(&t_record_last_rate_trace);
            }

        }
#endif
        pj_get_timestamp(&interval_finish_full);
        interval = pj_elapsed_msec( &interval_start_full, &interval_finish_full );
        delay = ( frame_count * 20 ) - interval;

        if( interval > 1 && delay > 2 )
            delay -= 2;

        if( delay > 0 ){
            pj_thread_sleep( delay );
        }

        delay = ( delay >= 0 ? 0 : delay + buffer_load_interval );

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0

        if( (frame_count % 200) == 0 ){
        	PJ_LOG(4, (THIS_FILE, "After delay interval=%d time=%d frames=%d delay=%d",
                        interval,
                        frame_count * 20,
                        frame_count,
                        delay ));
        }

        interval_min = (interval_min == 0 ? interval : PJ_MIN(interval_min, interval) );

        if( interval > interval_max ){
            interval_max =  interval;
            //PJ_LOG(4, (THIS_FILE, "Record max updated=%d frames=%d delay=%d", interval, frame_count, delay ));
        }
#endif
    }

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0

        pj_get_timestamp(&interval_finish_full);

        PJ_LOG(4, (THIS_FILE, "Time real=%d recorded=%d Frames recorded=%d should=%d",
                pj_elapsed_msec( &interval_start_full, &interval_finish_full ),
                frame_count * 20,
                frame_count,
                pj_elapsed_msec( &interval_start_full, &interval_finish_full )/20));
#endif

    //stop record
    jni_env->CallVoidMethod( rc_strm->record.rc_object, record_method_id_stop );
    JNI_EXCEPTION_CHECK(false);

    //release buffer
    jni_env->ReleaseByteArrayElements( readBuffer_array, readBuffer, 0 );

    /*
     * Throw JNI exception to JAVA level
     */
    if( IF_JNI_EXCEPTION() ){
    	JNI_EXCEPTION_CALLBACK(2);
    }

    leave:

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
    PJ_LOG(4, (THIS_FILE, "Recording interval [ms] Min=%d Max=%d Frames=%d lost=%d",
            interval_min,
            interval_max,
            frame_count,
            record_frames_lost ));
#endif

    DETACH_CURRENT_THREAD();

    rc_strm->record.rc_finished = PJ_TRUE;

    TRACE_("rc_record_working_thread finished");
    return 0;
}


/*
 *
 */
static int PJ_THREAD_FUNC rc_playback_working_thread(void* param)
{
    struct rc_android_audio_stream *rc_strm = (struct rc_android_audio_stream*)param;
    TRACE_("rc_playback_working_thread started");

	ATTACH_CURRENT_THREAD(PJ_EUNKNOWN);

	jmethodID playback_method_id_play = 0, playback_method_id_write = 0;
	jmethodID playback_method_id_stop = 0, playback_method_id_flush = 0;
	jmethodID playback_method_id_getstate = 0;

#if defined(AUDIO_RATES_PERIODIC_TRACES) && AUDIO_RATES_PERIODIC_TRACES!=0
	pj_timestamp t_playback_start;
	pj_timestamp t_playback_last_rate_trace;
	int t_playback_played_frames;
#endif

	pj_status_t status = 0;
	jbyteArray writeArray;
	jbyte* writeBuffer;
	pj_timestamp frames_per_cycle;
	int real_write = 0;
	int buffer_size = 0;
	unsigned play_buf_size =0;

	JNI_EXCEPTION_INIT()

	int frames_by_request_max = 1;
	int frames_by_request = 0;
	int buffer_load_interval =  (rc_strm->param.samples_per_frame / ( rc_strm->param.clock_rate / 1000 ));

	int dummy_frames_count = 0;

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
	pj_timestamp previous_request;
	int interval_previous = 0;

	pj_timestamp interval_start;
	pj_timestamp interval_finish;
	int interval_max = 0;
	int interval_min = 0;
	int interval = 0;
	int frame_count = 0;
	int frame_before_max=0;
#endif


	PJ_LOG(4, (THIS_FILE, "rc_playback_working_thread : %d samples/sec, %d samples/frame, %d bytes/sample [%d bits/sample]",
			rc_strm->param.clock_rate,
			rc_strm->param.samples_per_frame,
			rc_strm->param.bits_per_sample/8,
			rc_strm->param.bits_per_sample ));


	buffer_size 	 =  rc_strm->param.samples_per_frame * (rc_strm->param.bits_per_sample/8);

	PJ_LOG(4, (THIS_FILE, "Playback Maximum frames by request %d", frames_by_request_max ));

	frames_per_cycle.u64 = rc_strm->param.samples_per_frame / rc_strm->param.channel_count;

	playback_method_id_play 	= jni_env->GetMethodID(rc_strm->playback.rc_class,	"play"	, "()V");
	if( playback_method_id_play == 0 ){
		PJ_LOG(4, (THIS_FILE, "Could not get method ID for play [android.media.AudioTrack]"));
		goto leave;
	}

	playback_method_id_write 	= jni_env->GetMethodID(rc_strm->playback.rc_class,	"write"	, "([BII)I");
	if( playback_method_id_write == 0 ){
		PJ_LOG(4, (THIS_FILE, "Could not get method ID for write [android.media.AudioTrack]"));
		goto leave;
	}

	playback_method_id_stop 	= jni_env->GetMethodID(rc_strm->playback.rc_class,	"stop"	, "()V");
	if( playback_method_id_stop == 0 ){
		PJ_LOG(4, (THIS_FILE, "Could not get method ID for stop [android.media.AudioTrack]"));
		goto leave;
	}

	playback_method_id_flush 	= jni_env->GetMethodID(rc_strm->playback.rc_class,	"flush"	, "()V");
	if( playback_method_id_flush == 0 ){
		PJ_LOG(4, (THIS_FILE, "Could not get method ID for flush [android.media.AudioTrack]"));
		goto leave;
	}

	playback_method_id_getstate 	= jni_env->GetMethodID(rc_strm->playback.rc_class,	"getState"	, "()I");
	if( playback_method_id_getstate == 0 ){
		PJ_LOG(4, (THIS_FILE, "Could not get method ID for getState [android.media.AudioTrack]"));
		goto leave;
	}

	writeArray = jni_env->NewByteArray( buffer_size  * frames_by_request_max );
	if( writeArray == 0 )
	{
		PJ_LOG(4, (THIS_FILE, "Could not allocate write buffer for playback process"));
		goto leave;
	}

	writeBuffer = jni_env->GetByteArrayElements( writeArray, 0 );

	setpriority( PRIO_PROCESS, 0,  ANDROID_PRIORITY_AUDIO);

	//start to play
	jni_env->CallVoidMethod(rc_strm->playback.rc_object, playback_method_id_play);
	JNI_EXCEPTION_CHECK(true)

#if defined(AUDIO_RATES_PERIODIC_TRACES) && AUDIO_RATES_PERIODIC_TRACES!=0
    pj_get_timestamp(&t_playback_start);
    t_playback_played_frames =0;
    pj_get_timestamp(&t_playback_last_rate_trace);
#endif

#define PLAYBACK_IDLE_USE_RTP
	dummy_frames_count = ((rc_strm->playback.buffer_size / buffer_size) * 3) + (rc_strm->playback.buffer_size / buffer_size)/2;

#if defined(AUDIO_RATES_PERIODIC_TRACES) && AUDIO_RATES_PERIODIC_TRACES!=0
	pj_get_timestamp(&t_playback_start);
    t_playback_played_frames =0;
    pj_get_timestamp(&t_playback_last_rate_trace);
#endif

	{
	    int t_frames = dummy_frames_count;

#ifdef PLAYBACK_IDLE_TRACES
        int t_wait_frames = dummy_frames_count;
        int t_traces[t_wait_frames];
#endif

        int t_frame_time = (rc_strm->param.samples_per_frame / ( rc_strm->param.clock_rate / 1000 ));
        pj_timestamp t_start, t_current;
        int t_precise_time, t_elapsed_time, t_last_write;
        int t_written_frames = 0;
        int t_delay_time = 0;
        pj_bzero (writeBuffer, buffer_size);
        PJ_LOG(4, (THIS_FILE, "PlayBack Idle: frames=%d, idle_time=%d", dummy_frames_count, (dummy_frames_count * t_frame_time)));
        pj_get_timestamp(&t_start);

        while ( !IF_JNI_EXCEPTION() && rc_strm->playback.rc_finished != PJ_TRUE) {

#ifdef PLAYBACK_IDLE_USE_RTP
            status = read_from_rtp( (pj_int16_t*)writeBuffer,
                    buffer_size,
                    rc_strm->playback.sample_counter,
                    rc_strm );

            if( status != PJ_SUCCESS )
                PJ_LOG(4, (THIS_FILE, "PlayBack Idle: read_from_rtp error %d", status ));
            else
                rc_strm->playback.sample_counter.u64 += (rc_strm->param.samples_per_frame / rc_strm->param.channel_count );
#endif

            real_write = jni_env->CallIntMethod( rc_strm->playback.rc_object,
                    playback_method_id_write,
                    writeArray,
                    0,
                    buffer_size );

        	JNI_EXCEPTION_CHECK(true)

            if( IF_JNI_EXCEPTION() || real_write < 0 ) {
                PJ_LOG(2, (THIS_FILE, "PlayBack Idle: Write failed [android.media.AudioTrack] real_write < 0"));
                break;
            }

            if( rc_strm->playback.rc_finished == PJ_TRUE ) {
                PJ_LOG(4, (THIS_FILE, "Playback thread. Stop signal has been detected" ));
                break;
            }

            if( buffer_size > real_write) {
                PJ_LOG(2, (THIS_FILE, "PlayBack Idle: Write failed [android.media.AudioTrack] buffer_size > real_write"));
                break;
            }

            pj_get_timestamp(&t_current);
            t_precise_time = t_written_frames * t_frame_time;
            t_elapsed_time = pj_elapsed_msec(&t_start, &t_current);
            t_last_write = t_elapsed_time;

#ifdef PLAYBACK_IDLE_TRACES
            if (t_wait_frames > t_written_frames) {
                t_traces[t_written_frames] = t_elapsed_time;
            }
#endif

#if defined(AUDIO_RATES_PERIODIC_TRACES) && AUDIO_RATES_PERIODIC_TRACES!=0
            t_playback_played_frames++;
#endif

            t_written_frames++;
            dummy_frames_count--;
            if (( rc_strm->playback.rc_finished == PJ_TRUE)) {
                break;
            }
            if( dummy_frames_count == 0 ) {
                if( rc_strm->playback.started.u64 == 0 ) {
                    pj_get_timestamp( &rc_strm->playback.started );
                }
                if( rc_strm->record.started.u64 != 0 ) {
                    break;
                }
                dummy_frames_count++;
                t_frames++;
            }

            if (t_precise_time > t_elapsed_time) {
                pj_thread_sleep(t_frame_time + (t_precise_time - t_elapsed_time));
                if (( rc_strm->playback.rc_finished == PJ_TRUE)) {
                    break;
                }
            } else if (t_elapsed_time > t_precise_time) {
                t_delay_time = t_elapsed_time - t_precise_time;
                if (t_delay_time >= t_frame_time) {
                    continue;
                } else {
                    t_delay_time = t_frame_time - t_delay_time;
                    pj_thread_sleep(t_delay_time);
                    if (( rc_strm->playback.rc_finished == PJ_TRUE)) {
                        break;
                   }
                }
            } else {
                pj_thread_sleep(t_frame_time);
                if (( rc_strm->playback.rc_finished == PJ_TRUE)) {
                    break;
                }
            }
        }

        pj_get_timestamp(&t_current);
        t_elapsed_time = pj_elapsed_msec(&t_start, &t_current);
        PJ_LOG(4, (THIS_FILE, "PlayBack Idle: total_time=%d(%d)ms, frame=%dms, frames_lost: %d (last_write_at=%d, should_be_at=%dms), drift=%d",
                t_elapsed_time, (t_elapsed_time + t_frame_time), (t_elapsed_time + t_frame_time)/t_written_frames,
                (t_frames - t_written_frames), t_elapsed_time, (t_frame_time * (t_written_frames - 1)),
                (t_written_frames - (t_elapsed_time + t_frame_time)/t_frame_time)));

#ifdef PLAYBACK_IDLE_TRACES
        if (t_written_frames > t_wait_frames) {
            t_written_frames = t_wait_frames;
        }
        for (int i = 0; i < t_written_frames; i++) {
            PJ_LOG(4, (THIS_FILE, "---PlayBack Idle: frame %d written at %dms", i, t_traces[i]));
        }
#endif

    }
    if (( rc_strm->playback.rc_finished == PJ_TRUE)) {
        PJ_LOG(4, (THIS_FILE, "Player. Started main working loop." ));
    }

#if defined(AUDIO_RATES_PERIODIC_TRACES) && AUDIO_RATES_PERIODIC_TRACES!=0
        pj_get_timestamp(&t_playback_last_rate_trace);
#endif

	while ( !IF_JNI_EXCEPTION() && rc_strm->playback.rc_finished != PJ_TRUE ) {

		pj_sem_wait( rc_strm->playback.sync_sem );

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
		pj_get_timestamp(&interval_start);
#endif
		play_buf_size = get_buffer_len( &rc_strm->playback ) /rc_strm->param.samples_per_frame;

		if( play_buf_size == 0 ){
			continue;
		}

		if( play_buf_size < frames_by_request_max )
			frames_by_request = play_buf_size;
		else
			frames_by_request = frames_by_request_max;

		//PJ_LOG(4, (THIS_FILE, "Playback queue size is %d frames. Frames by request %d frames", play_buf_size, frames_by_request ));

    	status = get_from_buffer( &rc_strm->playback,
    				(pj_int16_t *)writeBuffer,
    				rc_strm->param.samples_per_frame * frames_by_request,
    				&play_buf_size );

	    if( rc_strm->playback.rc_finished == PJ_TRUE ){
	    	PJ_LOG(4, (THIS_FILE, "Playback thread detected stop signal"));
        	break;
	    }

		if( status != PJ_SUCCESS ){
        	continue;
        }

		// write data to playback device
        real_write = jni_env->CallIntMethod(rc_strm->playback.rc_object,
                playback_method_id_write,
                writeArray,
                0,
                buffer_size * frames_by_request);

    	JNI_EXCEPTION_CHECK(true)

        if( IF_JNI_EXCEPTION() || real_write < 0) {
            PJ_LOG(1, (THIS_FILE, "Write failed %d [android.media.AudioTrack]", status));
            break;
        }

        if (buffer_size * frames_by_request > real_write) {
            TRACE_( "Not all date were written. Should I call flush?" );
        }

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
        pj_get_timestamp(&interval_finish);

        interval_previous = pj_elapsed_msec(&previous_request, &interval_finish);
        interval = pj_elapsed_msec(&interval_start, &interval_finish);

        frame_count += frames_by_request;
        frame_before_max += frames_by_request;

    	interval = interval / frames_by_request;

    		interval_min = (interval_min == 0 ? interval : PJ_MIN(interval_min, interval) );

    		if( interval > interval_max ){
    			interval_max =  interval;

    			PJ_LOG( 2, (THIS_FILE, "Playback max=%d frame=%d frame_before=%d",
    					interval,
    					frame_count,
    					frame_before_max ));

    			frame_before_max=0;
    		}

    	pj_get_timestamp(&previous_request);
#endif

#if defined(AUDIO_RATES_PERIODIC_TRACES) && AUDIO_RATES_PERIODIC_TRACES!=0
        {
            pj_timestamp t_playback_rate_current_time;
            t_playback_played_frames++;
            pj_get_timestamp(&t_playback_rate_current_time);
            int t_playback_rate_elapsed_time = pj_elapsed_msec(&t_playback_last_rate_trace, &t_playback_rate_current_time);

            if (t_playback_rate_elapsed_time > AUDIO_RATES_PERIODIC_TRACES_TIMEOUT) {
                t_playback_rate_elapsed_time = pj_elapsed_msec( &t_playback_start, &t_playback_rate_current_time);

                int t_playback_rate_frame_time = (rc_strm->param.samples_per_frame / (rc_strm->param.clock_rate / 1000));

                int t_playback_rate_frames = t_playback_rate_elapsed_time / t_playback_rate_frame_time;
                int t_playback_rate_drift = t_playback_rate_frames - t_playback_played_frames;
                if (abs(t_playback_rate_drift) > 2) {
                    PJ_LOG(4,
                            (THIS_FILE, "PlayBack (rate): time=%dms, frames=%d(%d), drift=%d; frame_time=%dms, (last_write_at=%d, should_be_at=%dms)",
                                    t_playback_rate_elapsed_time, t_playback_played_frames, t_playback_rate_frames, t_playback_rate_drift,
                                    t_playback_rate_frame_time, t_playback_rate_elapsed_time + t_playback_rate_frame_time,
                                    (t_playback_rate_frame_time * (t_playback_played_frames - 1))));
                } else {
                    PJ_LOG(4,
                            (THIS_FILE, "PlayBack (rate): time=%dms, frames=%d(%d), frame_time=%dms, (last_write_at=%d, should_be_at=%dms)",
                                    t_playback_rate_elapsed_time, t_playback_played_frames, t_playback_rate_frames,
                                    t_playback_rate_frame_time, t_playback_rate_elapsed_time + t_playback_rate_frame_time,
                                    (t_playback_rate_frame_time * (t_playback_played_frames - 1))));
                }
                pj_get_timestamp(&t_playback_last_rate_trace);
            }

        }
#endif

	};

	//flush data
	jni_env->CallVoidMethod(rc_strm->playback.rc_object, playback_method_id_flush);
	JNI_EXCEPTION_CHECK(false)
	//stop to play
	jni_env->CallVoidMethod(rc_strm->playback.rc_object, playback_method_id_stop);
	JNI_EXCEPTION_CHECK(false)
	//release buffer
	jni_env->ReleaseByteArrayElements(writeArray, writeBuffer, 0);

    /*
     * Throw JNI exception to JAVA level
     */
    if( IF_JNI_EXCEPTION() ){
    	JNI_EXCEPTION_CALLBACK(1);
    }

leave:

#if defined(DETECT_WORKING_INTERVAL) && DETECT_WORKING_INTERVAL!=0
	PJ_LOG(4, (THIS_FILE, "Playback interval [ms] Min=%d Max=%d Processed frames %d", interval_min, interval_max, frame_count ));
#endif

	DETACH_CURRENT_THREAD();

	rc_strm->playback.rc_finished = PJ_TRUE;

	TRACE_("rc_playback_working_thread finished");
    return 0;
}

/* API: Start the stream. */
static pj_status_t rc_audio_stream_start(pjmedia_aud_stream *stream)
{
    struct rc_android_audio_stream *rc_strm = (struct rc_android_audio_stream*)stream;
    PJ_ASSERT_RETURN( stream , PJ_EINVAL);

    PJ_LOG(2, (THIS_FILE, "rc_audio_stream_start started"));

#if defined(PJMEDIA_HAS_SBACP_EC) && PJMEDIA_HAS_SBACP_EC!=0
    if (rc_strm->hSBACP) {
    	TRACE_("Restart AEC.");
    	SBACP_Restart(rc_strm->hSBACP);
    }
#endif

    pj_status_t status;

	rc_strm->rc_sync_finished = PJ_FALSE;

	status = pj_thread_create( rc_strm->pool,
								"RC sync thread",
								rc_sync_working_thread,
								(void*)stream,
								0,
								0,
								&rc_strm->rc_sync_thread );

	if( status != PJ_SUCCESS ){
		ERROR_("Could not create SYNC working thread");
		goto leave;
	}


	if( rc_strm->playback.rc_object ){

		rc_strm->playback.rc_finished = PJ_FALSE;

		status = pj_thread_create( rc_strm->pool,
									"RC playback thread",
									rc_playback_working_thread,
									(void*)stream,
									0,
									0,
									&rc_strm->playback.rc_thread );

		if( status != PJ_SUCCESS ){
			ERROR_("Could not create playback working thread");
			goto leave;
		}
	}

	if( rc_strm->record.rc_object ){

		rc_strm->record.rc_finished = PJ_FALSE;
		status = pj_thread_create( rc_strm->pool,
									"RC record thread",
									rc_record_working_thread,
									(void*)stream,
									0,
									0,
									&rc_strm->record.rc_thread );

		if( status != PJ_SUCCESS ){
			ERROR_("Could not create record working thread");
			goto leave;
		}
	}

    return PJ_SUCCESS;

leave:
	rc_audio_stream_stop(stream);

	return PJ_EUNKNOWN;
}

/* API: Stop the stream. */
static pj_status_t rc_audio_stream_stop(pjmedia_aud_stream *stream)
{
    struct rc_android_audio_stream *rc_strm = (struct rc_android_audio_stream*)stream;
    PJ_ASSERT_RETURN( stream , PJ_EINVAL);

    PJ_LOG(2, (THIS_FILE,"rc_audio_stream_stop started"));

	ATTACH_CURRENT_THREAD(PJ_EUNKNOWN);

	JNI_EXCEPTION_INIT();

	if( rc_strm->record.rc_thread )
	{
		TRACE_("rc_audio_stream_stop stopping record thread");

		if( rc_strm->record.rc_finished != PJ_TRUE ){
			rc_strm->record.rc_finished = PJ_TRUE;
			TRACE_("rc_audio_stream_stop Set exit flag");

			jmethodID id_rec_state = jni_env->GetMethodID( rc_strm->record.rc_class, "getRecordingState", "()I" );
			if( id_rec_state != 0 ){
				jint recordingState = jni_env->CallIntMethod( rc_strm->record.rc_object, id_rec_state );

				PJ_LOG(3, (THIS_FILE,"rc_audio_stream_stop Record on state %d", recordingState));

				jmethodID id_stop = jni_env->GetMethodID( rc_strm->record.rc_class, "stop", "()V" );
				if( id_stop != 0 ){
					TRACE_("rc_audio_stream_stop Record is on recording state. Call stop()");
					jni_env->CallVoidMethod( rc_strm->record.rc_object, id_stop );
					JNI_EXCEPTION_CHECK(false);
				}
			}

			TRACE_("rc_audio_stream_stop Join to Record thread");
			pj_thread_join(rc_strm->record.rc_thread);
		}

		TRACE_("rc_audio_stream_stop Destroy Record thread");
		pj_thread_destroy(rc_strm->record.rc_thread);
		rc_strm->record.rc_thread = NULL;

		TRACE_("rc_audio_stream_stop record thread is stopped");
	}

	if( rc_strm->rc_sync_thread )
	{
		TRACE_("rc_audio_stream_stop stopping sync thread");

		if( rc_strm->rc_sync_finished != PJ_TRUE ){
			rc_strm->rc_sync_finished = PJ_TRUE;

			pj_sem_post( rc_strm->record.sync_sem );
			pj_thread_join(rc_strm->rc_sync_thread);
		}

		pj_thread_destroy(rc_strm->rc_sync_thread);
		rc_strm->rc_sync_thread = NULL;

		TRACE_("rc_audio_stream_stop sync thread is stopped");
	}

	if( rc_strm->playback.rc_thread )
	{
		TRACE_("rc_audio_stream_stop stopping playback thread");

		if( rc_strm->playback.rc_finished != PJ_TRUE ){
			rc_strm->playback.rc_finished = PJ_TRUE;
			pj_sem_post( rc_strm->playback.sync_sem );
			pj_thread_join(rc_strm->playback.rc_thread);
		}
		pj_thread_destroy(rc_strm->playback.rc_thread);
		rc_strm->playback.rc_thread = NULL;

		TRACE_("rc_audio_stream_stop playback thread is stopped");
	}
    pjmedia_circ_buf_reset(rc_strm->record.circ_buf);
    pjmedia_circ_buf_reset(rc_strm->playback.circ_buf);

	TRACE_("rc_audio_stream_stop finished");

	DETACH_CURRENT_THREAD();

    return PJ_SUCCESS;
}

/* API: Destroy the stream. */
static pj_status_t rc_audio_stream_destroy(pjmedia_aud_stream *stream)
{
    struct rc_android_audio_stream *rc_strm = (struct rc_android_audio_stream*)stream;
    PJ_ASSERT_RETURN( stream , PJ_EINVAL);

    PJ_LOG(2, (THIS_FILE,"rc_audio_stream_destroy started"));

	jmethodID record_method_id_release, playback_method_id_release;

    rc_audio_stream_stop(stream);

	ATTACH_CURRENT_THREAD(PJ_EUNKNOWN);

	if( rc_strm->record.rc_object )
	{
		record_method_id_release 	= jni_env->GetMethodID( rc_strm->record.rc_class, "release", "()V" );
		if( record_method_id_release != 0 )
		{
			TRACE_("rc_audio_stream_destroy Release record");
			jni_env->CallVoidMethod( rc_strm->record.rc_object, record_method_id_release );
		}

		jni_env->DeleteGlobalRef( rc_strm->record.rc_object );
		rc_strm->record.rc_object = 0;
	}

	if( rc_strm->playback.rc_object )
	{

		playback_method_id_release 	= jni_env->GetMethodID( rc_strm->playback.rc_class, "release", "()V" );
		if( playback_method_id_release != 0 )
		{
			TRACE_("rc_audio_stream_destroy Release playback");
			jni_env->CallVoidMethod( rc_strm->playback.rc_object, playback_method_id_release );
		}

		jni_env->DeleteGlobalRef( rc_strm->playback.rc_object );
		rc_strm->playback.rc_object = 0;
	}

#if defined(PJMEDIA_HAS_SBACP_EC) && PJMEDIA_HAS_SBACP_EC!=0
    if (rc_strm->hSBACP) {
    	TRACE_("Destroy AEC.");
    	SBACP_Delete(rc_strm->hSBACP);
    	rc_strm->hSBACP = NULL;
    }
#endif


#if defined(DRV_TRACE_INFO) && DRV_TRACE_INFO!=0
    destroyTrace( &rc_strm->strace );
#endif

    pj_mutex_destroy(rc_strm->record.buff_mutex);
    pj_mutex_destroy(rc_strm->playback.buff_mutex);

    pj_sem_destroy( rc_strm->record.sync_sem );
    pj_sem_destroy( rc_strm->playback.sync_sem );

    pj_pool_t *pool = rc_strm->pool;
    rc_strm->pool = NULL;
	pj_pool_release(pool);

	DETACH_CURRENT_THREAD();

	TRACE_("rc_audio_stream_destroy finished");
    return PJ_SUCCESS;
}
#endif //PJMEDIA_AUDIO_DEV_HAS_ANDROID
