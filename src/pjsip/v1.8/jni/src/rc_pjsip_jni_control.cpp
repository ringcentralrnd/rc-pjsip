/*
 * rc_pjsip_jni_control.cpp
 *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 *  Implementation of PJSIP JNI interface
 *  SIP Stack control functions.
 */


#include <rc_pjsip_jni.h>

#include <rc_android_audio_dev_java.h>

#define THIS_FILE	"rc_pjsip_jni_control.cpp"

#ifdef __cplusplus
extern "C" {
#endif

int createTransport( pjsip_transport_type_e trpType, pjsua_transport_id* p_id, jint SIPTransportLocalPort );

/*
 *	pjsip_create()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1create(JNIEnv *, jclass )
{
  pj_status_t status;

  status = pjsua_create();

  if( status != PJ_SUCCESS ){
	  pjsua_perror(THIS_FILE, "pjsua_create failed ", status );
  }

  PJ_LOG(4, (THIS_FILE, "pjsua_create %d", status ));

  return (jint) status;
}

/*
 * 	pjsip_destroy()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1destroy(JNIEnv *, jclass )
{
  pj_status_t status;

    status = pjmedia_aud_unregister_factory(&pjmedia_rc_android_audio_dev_java_factory);
  PJ_LOG(4, (THIS_FILE, "pjsip_destroy pjmedia_aud_unregister_factory %d", status ));

  PJ_LOG(4, (THIS_FILE, "pjsip_destroy ring_destroy" ));
  ring_destroy();

  PJ_LOG(4, (THIS_FILE, "pjsip_destroy ringback_destroy" ));
  ringback_destroy();

  status = pjsua_destroy();
  PJ_LOG(4, (THIS_FILE, "pjsip_destroy %d", status ));

  return (jint) status;
}

/**
 * Set RC parameters for Media Configuration
 */
void prepareMediaCfg( pjsua_media_config* media_cfg)
{
    media_cfg->ec_tail_len = 0;
    media_cfg->ptime = 20;
    media_cfg->clock_rate 	 = RC_PJSUA_DEFAULT_CLOCK_RATE;
    media_cfg->snd_clock_rate = RC_PJSUA_DEFAULT_CLOCK_RATE;
    media_cfg->ilbc_mode = 20;
    media_cfg->quality = 4;
    media_cfg->no_vad = PJ_TRUE;

    PJ_LOG(4, (THIS_FILE, "pjsip_init MediaCfg Tail=%d PTime=%d Quality=%d Clock=%d iLBC=%d",
    		media_cfg->ec_tail_len,
    		media_cfg->ptime,
    		media_cfg->quality,
    		media_cfg->clock_rate,
    		media_cfg->ilbc_mode));

}

/**
 * Set RC parameters for Transport Config
 */
void prepareTransportCfg( pjsua_transport_config* cfg)
{
	cfg->port = 5060;
}

/*
 * pjsip_init()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1init(JNIEnv *jenv,
		jclass jcls,
		jstring jstrSipOutboundProxy,
		jint SIPTransportLocalPort)
{
    pjsua_config	 ua_cfg;
    pjsua_logging_config log_cfg;
    pjsua_media_config   media_cfg;
	pjsua_transport_id p_id;

	jsize proxyLen = 0;
    pj_status_t status;
    const char* strOutboundProxy = NULL;

#define MAX_PORT_CHECK_ATTEMPTS	5
#define PORT_CHECK_STEP			2

    PJ_LOG(4, (THIS_FILE, "pjsip_init() Starting ...\n" ));
	PJ_LOG(4, (THIS_FILE, "bymartin:check silk pjsip_init() Starting ...\n" ));



    rc_cfg.pool = pjsua_pool_create("rc_pool", 1000, 1000);
    if( rc_cfg.pool == 0 ){
    	status = PJ_ENOMEM;
    	goto error;
    }

    // Initialize configs with default settings.
    pjsua_config_default(&ua_cfg);
    pjsua_logging_config_default(&log_cfg);
    pjsua_media_config_default(&media_cfg);

    prepareMediaCfg( &media_cfg );

    proxyLen = jenv->GetStringUTFLength( jstrSipOutboundProxy );
    if( proxyLen > 0  ){

    	strOutboundProxy = jenv->GetStringUTFChars( jstrSipOutboundProxy, JNI_FALSE );
    	if( strOutboundProxy == NULL  ){
    	    PJ_LOG(2, (THIS_FILE, "pjsip_init failed. GetStringUTFChars returned null."));
        	status = PJ_ENOMEM;
        	goto error;
    	}
    	else{
    	    PJ_LOG(4, (THIS_FILE, "bymartin pjsip_init() SIP Outbound Proxy is %s port=%d", strOutboundProxy,(unsigned int)SIPTransportLocalPort));

            // add SIP outbound proxy
        	pj_strdup2_with_null( rc_cfg.pool, &ua_cfg.outbound_proxy[0], strOutboundProxy );
        	ua_cfg.outbound_proxy_cnt = 1;

        	jenv->ReleaseStringUTFChars( jstrSipOutboundProxy, strOutboundProxy );
        }
    }
    else{
        PJ_LOG(3, (THIS_FILE, "pjsip_init() SIP Outbound Proxy is empty. Ignore." ));
    }


    /*
     * Set callback function
     */
    ua_cfg.cb.on_reg_state 			= &rc_jni_on_reg_state;
    ua_cfg.cb.on_call_media_state 	= &rc_jni_on_call_media_state;
    ua_cfg.cb.on_pager 				= &rc_jni_on_message;
    ua_cfg.cb.on_incoming_call 		= &rc_jni_on_incoming_call;
    ua_cfg.cb.on_stream_destroyed	= &rc_jni_on_stream_destroyed;
    ua_cfg.cb.on_call_state			= &rc_jni_on_call_state;
    ua_cfg.cb.on_call_transfer_status = &rc_jni_on_call_transfer_status;


    //ua_cfg.thread_cnt	= 3;
    PJ_LOG(4, (THIS_FILE, "pjsip_init UACfg ThCnt=%d", ua_cfg.thread_cnt ));

    /*
     * Set log level
     */
    log_cfg.cb = &pj_android_log_msg;
    log_cfg.level = 6;

    // Initialize pjsua
    status = pjsua_init(&ua_cfg, &log_cfg, &media_cfg);
    if (status != PJ_SUCCESS) {
        PJ_LOG(2, (THIS_FILE, "Error initializing pjsua Result=%d", status ));
    	return (jint)status;
    }

    /*
     * Create UDP transport
     */
    status = createTransport( PJSIP_TRANSPORT_UDP, &p_id, SIPTransportLocalPort );

	if (status != PJ_SUCCESS){
    	goto error;
	}


    /*
     * Create TCP transport
     */
    //status = createTransport( PJSIP_TRANSPORT_TCP, &p_id, SIPTransportLocalPort );
    //status = createTransport( PJSIP_TRANSPORT_TCP, &p_id, SIPTransportLocalPort );

	if (status != PJ_SUCCESS){
    	goto error;
	}

	/*
	 * Register RC Android Audio Driver
	 */
	status = pjmedia_aud_register_factory(&pjmedia_rc_android_audio_dev_java_factory);
	if (status != PJ_SUCCESS){
	    PJ_LOG(2, (THIS_FILE, "Error Register RC Android Audio factory Result=%d", status ));
    	goto error;
	}

	/*
	 * Init Ringback
	 */
	status = ringback_init( &media_cfg );
	if (status != PJ_SUCCESS){
    	goto error;
	}

	/*
	 * Init Ring
	 */
	status = ring_init( &media_cfg );
	if (status != PJ_SUCCESS){
    	goto error;
	}

	PJ_LOG(3, (THIS_FILE, "pjsip_init() Finished Result=%d", status ));
	return (jint)status;

  error:

	pjsua_destroy();

	PJ_LOG(2, (THIS_FILE, "pjsip_init() Failed Result=%d", status ));
	return (jint)status;
}

/*
 *
 */
int createTransport( pjsip_transport_type_e trpType, pjsua_transport_id* p_id, jint SIPTransportLocalPort ){
	pjsua_transport_config cfg;
	pj_status_t status = PJ_SUCCESS;
    int port_check_attempts = 0;
    unsigned port;
    PJ_LOG(3, (THIS_FILE, "pjsua_transport_create() Starting Transport %s ",
    		(trpType == PJSIP_TRANSPORT_UDP ? "UDP" : "TCP")));

	pjsua_transport_config_default(&cfg);
	prepareTransportCfg(&cfg);
	if (SIPTransportLocalPort > 0) {
	    cfg.port = SIPTransportLocalPort;
	}
	port = cfg.port;
	while( port_check_attempts < MAX_PORT_CHECK_ATTEMPTS ){
		status = pjsua_transport_create( trpType, &cfg, p_id );

		if (status == PJ_SUCCESS){
		    PJ_LOG(3, (THIS_FILE, "pjsua_transport_create() Transport %s Port:%d Id:%d",
		    		(trpType == PJSIP_TRANSPORT_UDP ? "UDP" : "TCP"),
		    		cfg.port, *p_id ));
		    break;
		}

		PJ_LOG(2, (THIS_FILE, "Error creating transport %s on port: %d Result=%d",
    			(trpType == PJSIP_TRANSPORT_UDP ? "UDP" : "TCP"),
    			cfg.port,
    			status ));

    	port_check_attempts++;
    	cfg.port = port + ( PORT_CHECK_STEP * port_check_attempts );
	}

	return status;
}

/*
 * pjsip_start()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1start(JNIEnv *, jclass )
{
	  pj_status_t status;

	  PJ_LOG(4, (THIS_FILE, "pjsip_start is starting..." ));

	  status = pjsua_start();

	  if( status != PJ_SUCCESS ){
		  pjsua_perror(THIS_FILE, "pjsua_start failed ", status );
	  }

	  PJ_LOG(4, (THIS_FILE, "pjsip_start finished %d", status ));
	  return (jint) status;
}

/*
 * pjsip_media_transports_create
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1media_1transports_1create(JNIEnv *,
		jclass,
		jint port )
{
	pj_status_t status;
	pjsua_transport_config cfg;

	PJ_LOG(4, (THIS_FILE, "pjsip_media_transports_create is starting..." ));

	pjsua_transport_config_default(&cfg);
	prepareTransportCfg(&cfg);
	if( port > 0 ){
		cfg.port = (unsigned)port;
	}

	status = pjsua_media_transports_create( &cfg );
	if( status != PJ_SUCCESS ){
	  pjsua_perror(THIS_FILE, "pjsip_media_transports_create failed ", status );
	}

	return status;
}

static pj_status_t create_transport_adapter(pjmedia_endpt *med_endpt,
		int port,
		pjmedia_transport **p_tp)
{
    pjmedia_transport *udp;
    pj_status_t status;

    /* Create the UDP media transport */
    status = pjmedia_transport_udp_create(med_endpt, NULL, port, 0, &udp);
	if (status != PJ_SUCCESS)
		return status;

	/* Create the adapter */
	status = pjmedia_tp_adapter_create(med_endpt, NULL, udp, p_tp);
	if (status != PJ_SUCCESS) {
		pjmedia_transport_close(udp);
		return status;
	}

    return PJ_SUCCESS;
}

/*
 * Method:    pjsip_media_transports_update
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1media_1transports_1update(JNIEnv *,
		jclass,
		jint jport )
{

	PJ_LOG(4, (THIS_FILE, "pjsip_media_transports_update is starting..." ));

	pj_status_t status;
    pjsua_media_transport tp[PJSUA_MAX_CALLS];
    int port = ( jport == 0 ? 7000 : jport );
    unsigned i;
    unsigned max_calls = pjsua_call_get_max_count();

    for (i = 0; i < max_calls; ++i) {
		status = create_transport_adapter( pjsua_get_pjmedia_endpt(), port + i* 10, &tp[i].transport);

		if (status != PJ_SUCCESS) {
			pjsua_perror(THIS_FILE, "pjsip_media_transports_update [create_transport_adapter] failed ", status);
			return status;
		}
	}

    status = pjsua_media_transports_attach(tp, i, PJ_TRUE);
	if (status != PJ_SUCCESS) {
		pjsua_perror(THIS_FILE, "pjsip_media_transports_update [pjsua_media_transports_attach] failed ", status);
	}

	return status;
}

#ifdef __cplusplus
}
#endif
