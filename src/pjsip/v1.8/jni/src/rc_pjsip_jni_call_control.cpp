/*
 * rc_pjsip_jni_call_control.cpp
 *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 *  Implementation of PJSIP JNI interface
 *  SIP Stack Call control functions.
 */

#include <rc_pjsip_jni.h>
#include <rc_android_audio_dev.h>

#define THIS_FILE	"rc_pjsip_jni_call_control.cpp"

#ifdef __cplusplus
extern "C" {
#endif

void prepareHeaderList( JNIEnv *jenv,
		jint hdr_count,
		jobjectArray headers,
		pjsip_hdr* hdr_list )
{

    pj_str_t hdr_name;
    pj_str_t hdr_value;
    jstring jstrHdr_name;
    jstring jstrHdr_value;

    int i = 0;
	  while( i < hdr_count ){
		  pjsip_hdr *hdr;
		  jstrHdr_name = (jstring)jenv->GetObjectArrayElement( headers, i++ );
		  const char* ptrHdr_name = jenv->GetStringUTFChars( jstrHdr_name, JNI_FALSE );

		  PJ_LOG(4, (THIS_FILE, "pjsip_call_answer Item %d ", i ));

		  jstrHdr_value = (jstring)jenv->GetObjectArrayElement( headers, i++ );
		  const char* ptrHdr_value = jenv->GetStringUTFChars( jstrHdr_value, JNI_FALSE );

		  PJ_LOG(4, (THIS_FILE, "pjsip_call_answer Item %d ", i ));

		  if( ptrHdr_name != NULL && ptrHdr_value != NULL ){

			  PJ_LOG(4, (THIS_FILE, "pjsip_call_answer Duplicate values"));

			  pj_strdup2_with_null( rc_cfg.pool, &hdr_name, ptrHdr_name );
			  pj_strdup2_with_null( rc_cfg.pool, &hdr_value, ptrHdr_value );

			  PJ_LOG(4, (THIS_FILE, "pjsip_call_answer Create header"));
			  hdr = (pjsip_hdr*) pjsip_generic_string_hdr_create(rc_cfg.pool, &hdr_name, &hdr_value);

			  PJ_LOG(4, (THIS_FILE, "pjsip_call_answer Add header"));
			  pj_list_push_back( hdr_list, hdr);
		  }

		  if( ptrHdr_name != NULL)
			  jenv->ReleaseStringUTFChars( jstrHdr_name, ptrHdr_name );

		  if( ptrHdr_value != NULL)
			  jenv->ReleaseStringUTFChars( jstrHdr_value, ptrHdr_value );
	  }
}

/*
 * pjsip_call_answer()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1answer(JNIEnv *jenv,
		jclass,
		jint callId,
		jint code,
		jobjectArray headers )
{
	pj_status_t status;
	struct pjsua_msg_data data;

	PJ_LOG(4, (THIS_FILE, "pjsip_call_answer is starting... %d", callId ));

	jint hdr_count = jenv->GetArrayLength( headers );
	PJ_LOG(4, (THIS_FILE, "pjsip_call_answer detected %d headers", hdr_count ));

	if( hdr_count >= 2 && (hdr_count % 2) == 0 ){

		pjsua_msg_data_init( &data );
	    pj_list_init( &data.hdr_list );

	    prepareHeaderList( jenv, hdr_count, headers, &data.hdr_list );
		status = pjsua_call_answer( callId, code, NULL, &data );
	}
	else
		status = pjsua_call_answer( callId, code, NULL, NULL );

	if( status != PJ_SUCCESS )
		  pjsua_perror(THIS_FILE, "pjsip_call_answer failed ", status );

	PJ_LOG(4, (THIS_FILE, "pjsip_call_answer finished %d", status ));
	return (jint) status;
}

/*
 * pjsip_call_hangup()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1hangup(JNIEnv *jenv,
		jclass,
		jint callId,
		jint code,
		jobjectArray headers )
{
	pj_status_t status;
	struct pjsua_msg_data data;

	PJ_LOG(4, (THIS_FILE, "pjsip_call_hangup is starting... %d", callId ));

	jint hdr_count = jenv->GetArrayLength( headers );
	PJ_LOG(4, (THIS_FILE, "pjsip_call_answer detected %d headers", hdr_count ));

	if( hdr_count >= 2 && (hdr_count % 2) == 0 ){

		pjsua_msg_data_init( &data );
	    pj_list_init( &data.hdr_list );

	    prepareHeaderList( jenv, hdr_count, headers, &data.hdr_list );
		status = pjsua_call_hangup( callId, code, NULL, &data );
	}
	else
		status = pjsua_call_hangup( callId, code, NULL, NULL );

	if( status != PJ_SUCCESS ){
		  pjsua_perror(THIS_FILE, "pjsip_call_hangup failed ", status );
	}

	PJ_LOG(4, (THIS_FILE, "pjsip_call_hangup finished %d", status ));
	return (jint) status;
}

/*
 * pjsip_call_hangup_all
 */
JNIEXPORT void JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1hangup_1all(JNIEnv *, jclass)
{
	pjsua_call_id calls[PJSUA_MAX_CALLS];
	unsigned call_count = PJSUA_MAX_CALLS;

	PJ_LOG(4, (THIS_FILE, "pjsip_call_hangup_all is starting..." ));
	pjsua_call_hangup_all();

	PJ_LOG(4, (THIS_FILE, "pjsip_call_hangup_all finished" ));
}

/*
 * pjsip_call_hold()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1hold(JNIEnv *, jclass, jint callId )
{
	pj_status_t status;

	PJ_LOG(4, (THIS_FILE, "pjsip_call_hold is starting... %d", callId ));

	status = pjsua_call_set_hold( callId, NULL );
	if( status != PJ_SUCCESS ){
		  pjsua_perror(THIS_FILE, "pjsip_call_hold failed ", status );
	}
	else{
		rc_call_info* rcCallInfo = (rc_call_info*)pjsua_call_get_user_data( callId );
		if( rcCallInfo != NULL ){
			rcCallInfo->status = ( rcCallInfo->status | RC_CALL_STATUS_HOLD );
		}
	}

	PJ_LOG(4, (THIS_FILE, "pjsip_call_hold finished %d", status ));
	return (jint) status;
}

/*
 * pjsip_call_unhold()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1unhold(JNIEnv *, jclass, jint callId)
{
	pj_status_t status;

	PJ_LOG(4, (THIS_FILE, "pjsip_call_unhold is starting... %d", callId ));

	status = pjsua_call_reinvite( callId, PJ_TRUE, NULL );
	if( status != PJ_SUCCESS ){
		  pjsua_perror(THIS_FILE, "pjsip_call_unhold failed ", status );
	}
	else{
		rc_call_info* rcCallInfo = (rc_call_info*)pjsua_call_get_user_data( callId );
		if( rcCallInfo != NULL ){
			rcCallInfo->status = ( rcCallInfo->status & ~RC_CALL_STATUS_HOLD );
		}
	}

	PJ_LOG(4, (THIS_FILE, "pjsip_call_unhold finished %d", status ));
	return (jint) status;
}

/*
 * pjsip_call_make()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1make(JNIEnv *jenv,
		jclass,
		jint accountId,
		jstring dstURI,
		jobjectArray headers,
		jintArray callId_array )
{
	pj_status_t status;
	pj_str_t dst_uri;
	const char* pdst_uri;
	pjsua_call_id callId;
	struct pjsua_msg_data data;

	PJ_LOG(4, (THIS_FILE, "pjsip_call_make is starting... %d", accountId ));

	jint size = jenv->GetArrayLength( callId_array );
	if( size == 0 ){
		pjsua_perror(THIS_FILE, "pjsip_call_make failed ", PJ_EINVAL );
		return PJ_EINVAL;
	}

	pdst_uri = jenv->GetStringUTFChars( dstURI, JNI_FALSE );
	pj_strdup2_with_null( rc_cfg.pool, &dst_uri, pdst_uri );
	jenv->ReleaseStringUTFChars( dstURI, pdst_uri );

	jint hdr_count = jenv->GetArrayLength( headers );
	PJ_LOG(4, (THIS_FILE, "pjsip_call_answer detected %d headers", hdr_count ));

	if( hdr_count >= 2 && (hdr_count % 2) == 0 ){

		pjsua_msg_data_init( &data );
	    pj_list_init( &data.hdr_list );

	    prepareHeaderList( jenv, hdr_count, headers, &data.hdr_list );
		status = pjsua_call_make_call( accountId, &dst_uri, 0, NULL, &data, &callId );
	}
	else
		status = pjsua_call_make_call( accountId, &dst_uri, 0, NULL, NULL, &callId );

	if( status == PJ_SUCCESS ){

		jenv->SetIntArrayRegion(callId_array, 0, 1, &callId );

		/*
		 * Add RC Call Information
		 */
		rc_call_info* rcCallInfo = (rc_call_info*)pj_pool_zalloc( rc_cfg.pool, sizeof(rc_call_info));
		rcCallInfo->status = 0;
		pjsua_call_set_user_data( callId, rcCallInfo );
	}
	else{
		pjsua_perror(THIS_FILE, "pjsua_call_make_call failed ", status );

		unsigned call_count = pjsua_call_get_count();
		if( call_count > 0 ){
			pjsua_call_id* calls_array = (pjsua_call_id*)pj_pool_zalloc( rc_cfg.pool, sizeof(pjsua_call_id) * call_count );
			if( calls_array != NULL ){
				status = pjsua_enum_calls( calls_array, &call_count );
				if( status == PJ_SUCCESS ){
					unsigned counter = call_count;
					for( int i = 0; i < counter; i++ ){
						if( !pjsua_call_is_active(calls_array[i]) )
							call_count--;
					}
				}
			}
		}

		PJ_LOG(4, (THIS_FILE, "pjsip_call_make Failed. Detected %d active calls", call_count ));

	    /* Activate sound device auto-close timer if sound device is idle.
	     * It is idle when there is no port connection in the bridge and
	     * there is no active call.
	     */
	    if ((pjsua_var.snd_port != NULL || pjsua_var.null_snd != NULL)
				&& pjsua_var.snd_idle_timer.id == PJ_FALSE
				&& pjmedia_conf_get_connect_count(pjsua_var.mconf) == 0 &&
				call_count == 0 )
	    {
			pj_time_val delay;

			delay.msec = 0;
			delay.sec = (pjsua_var.media_cfg.snd_auto_close_time >= 0 ? pjsua_var.media_cfg.snd_auto_close_time : 1 );
			pjsua_var.snd_idle_timer.id = PJ_TRUE;

			PJ_LOG(4, (THIS_FILE, "pjsip_call_make Failed. Will start auto-close timer for %d sec.", delay.sec ));

			pjsip_endpt_schedule_timer(pjsua_var.endpt, &pjsua_var.snd_idle_timer, &delay);
		}

	}

	PJ_LOG(4, (THIS_FILE, "pjsip_call_make finished %d", status ));
	return (jint) status;
}

/*
 * Method:    pjsip_call_attach_sound
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1attach_1sound(JNIEnv *,
		jclass,
		jint callId )
{
	pj_status_t status;
	pjsua_call_info call_info;

	PJ_LOG(4, (THIS_FILE, "pjsip_call_attach_sound is starting... %d", callId ));

	status = pjsua_call_get_info( callId, &call_info );
	if( status != PJ_SUCCESS ){
		pjsua_perror(THIS_FILE, "pjsip_call_attach_sound. pjsua_call_get_info failed ", status );
		return (jint)status;
	}

	status = pjsua_conf_connect(call_info.conf_slot, 0);
	if( status != PJ_SUCCESS ){
		pjsua_perror(THIS_FILE, "pjsip_call_attach_sound. pjsua_conf_connect [1] failed ", status );
		return (jint)status;
	}

	status = pjsua_conf_connect(0, call_info.conf_slot);
	if( status != PJ_SUCCESS ){
		pjsua_perror(THIS_FILE, "pjsip_call_attach_sound. pjsua_conf_connect [2] failed ", status );
		return (jint)status;
	}

	PJ_LOG(4, (THIS_FILE, "pjsip_call_attach_sound finished %d", status ));
	return (jint) status;
}

/*
 * Class:     com_rcbase_android_sip_service_PJSIP
 * Method:    pjsip_call_meia_disconnect
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1meia_1disconnect(JNIEnv *,
		jclass,
		jint callId)
{
	pj_status_t status;
	pjsua_call_info call_info;

	PJ_LOG(4, (THIS_FILE, "pjsip_call_meia_disconnect is starting... %d", callId ));

	int hasMedia = pjsua_call_has_media( callId );

	if( !hasMedia ){
		PJ_LOG(4, (THIS_FILE, "pjsip_call_meia_disconnect finished %d. No media.", callId ));
		return PJ_SUCCESS;
	}

	status = pjsua_call_get_info( callId, &call_info );
	if( status != PJ_SUCCESS ){
		pjsua_perror(THIS_FILE, "pjsip_call_meia_disconnect. pjsua_call_get_info failed ", status );
		return (jint)status;
	}

	status = pjsua_conf_disconnect(call_info.conf_slot, 0);
	if( status != PJ_SUCCESS ){
		pjsua_perror(THIS_FILE, "pjsip_call_meia_disconnect. pjsua_conf_disconnect [1] failed ", status );
	}

	status = pjsua_conf_disconnect(0, call_info.conf_slot);
	if( status != PJ_SUCCESS ){
		pjsua_perror(THIS_FILE, "pjsip_call_meia_disconnect. pjsua_conf_disconnect [2] failed ", status );
	}

	return (jint) status;
}


/*
 * Method:    pjsip_call_send_request
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1send_1request(JNIEnv *jenv,
		jclass,
		jint callId,
		jstring content,
		jstring clientId)
{
	pj_status_t status;
    pjsua_msg_data msg_data;
    const char* pcontent;
	pj_str_t SIP_INFO;

	PJ_LOG(4, (THIS_FILE, "pjsip_call_send_request started %d", callId ));

	pj_strdup2_with_null( rc_cfg.pool, &SIP_INFO, "INFO" );

	pjsua_msg_data_init(&msg_data);
	pj_strdup2_with_null( rc_cfg.pool, &msg_data.content_type, "x-rc/agent" );

	pcontent = jenv->GetStringUTFChars( content, JNI_FALSE );
	pj_strdup2_with_null( rc_cfg.pool, &msg_data.msg_body, pcontent );
	jenv->ReleaseStringUTFChars( content, pcontent );

	status = pjsua_call_send_request( callId, &SIP_INFO, &msg_data );
	if( status != PJ_SUCCESS ){
		pjsua_perror(THIS_FILE, "pjsip_call_send_request failed ", status );
		return status;
	}

	PJ_LOG(4, (THIS_FILE, "pjsip_call_send_request finished %d", status ));
	return (jint) status;
}

/*
 * Method:    pjsip_call_transfer
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1transfer(JNIEnv *jenv,
		jclass,
		jint callId,
		jstring dstURI)
{
	pj_status_t status;
    pjsua_msg_data msg_data;
    const char* pdstURI;
    pj_str_t pj_dstURI;

	PJ_LOG(4, (THIS_FILE, "pjsip_call_transfer started %d", callId ));

	pdstURI = jenv->GetStringUTFChars( dstURI, JNI_FALSE );
	pj_strdup2_with_null( rc_cfg.pool, &pj_dstURI, pdstURI );
	jenv->ReleaseStringUTFChars( dstURI, pdstURI );

	pjsua_msg_data_init(&msg_data);
	status = pjsua_call_xfer( callId, &pj_dstURI, &msg_data);

	PJ_LOG(4, (THIS_FILE, "pjsip_call_transfer finished %d", status ));
	return (jint) status;
}

/*
 * pjsip_call_reinvate_all
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1reinvate_1all(JNIEnv *, jclass)
{
	pj_status_t status = PJ_SUCCESS;
	pjsip_tx_data *tdata;
	pj_str_t new_contact;

	PJ_LOG(3, (THIS_FILE, "pjsip_call_reinvate_all started" ));

	unsigned call_count = pjsua_call_get_max_count();
	if( call_count == 0 ){
		return PJ_ENOTFOUND;
	}

	pjsua_call_id* calls_array = (pjsua_call_id*)pj_pool_zalloc( rc_cfg.pool, sizeof(pjsua_call_id) * call_count );
	if( calls_array == NULL ){
		pjsua_perror(THIS_FILE, "pjsip_call_reinvate_all failed ", PJ_ENOMEM );
		return PJ_ENOMEM;
	}

	status = pjsua_enum_calls( calls_array, &call_count );
	if( status != PJ_SUCCESS ){
		pjsua_perror(THIS_FILE, "pjsip_call_reinvate_all failed ", status );
		return status;
	}

	PJ_LOG(3, (THIS_FILE, "pjsip_call_reinvate_all Detected %d calls", call_count ));

	for( int i = 0; i < call_count; i++ ){
		pjsua_call_id call_id = calls_array[i];
		pjsip_inv_session *inv = pjsua_var.calls[call_id].inv;

		new_contact = pjsua_var.acc[0].contact;

		PJ_LOG(3, (THIS_FILE, "pjsip_call_reinvate_all New contact %s", ( new_contact.ptr != NULL ? new_contact.ptr : "NULL") ));

		status = pjsip_inv_reinvite(inv, &new_contact, NULL, &tdata);
		if (status == PJ_SUCCESS){
			PJ_LOG(3, (THIS_FILE, "pjsip_call_reinvate_all pjsip_inv_send_msg callId: %d ", call_id ));
		    pjsip_inv_send_msg(inv, tdata);
		}
		else{
			PJ_LOG(3, (THIS_FILE, "pjsip_call_reinvate_all pjsip_inv_reinvite failed %d", status ));
		}
	}

	return status;
}

/*
 * pjsip_call_dial_dtmf
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1dial_1dtmf(JNIEnv *jenv,
		jclass,
		jint callId,
		jstring digits)
{
	pj_status_t status;
    const char* pdstDigits;
    pj_str_t pj_dstDigits;

	PJ_LOG(4, (THIS_FILE, "pjsip_call_dial_dtmf started %d", callId ));

	pdstDigits = jenv->GetStringUTFChars( digits, JNI_FALSE );
	pj_strdup2_with_null( rc_cfg.pool, &pj_dstDigits, pdstDigits );
	jenv->ReleaseStringUTFChars( digits, pdstDigits );

	status = pjsua_call_dial_dtmf( callId, &pj_dstDigits );

	PJ_LOG(4, (THIS_FILE, "pjsip_call_dial_dtmf finished %d", status ));
	return (jint) status;
}


#ifdef __cplusplus
}
#endif
