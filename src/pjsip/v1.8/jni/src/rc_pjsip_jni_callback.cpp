/*
 * rc_pjsip_jni_callback.cpp
 *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 *  Implementation of PJSIP JNI interface
 *  SIP Stack Callback functions.
 */

#include "rc_pjsip_jni.h"

#define THIS_FILE	"rc_pjsip_jni_callback.cpp"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Notify application when invite state has changed.
 * Application may then query the call info to get the
 * detail call states by calling  pjsua_call_get_info() function.
 *
 * @param call_id	The call index.
 * @param e		Event which causes the call state to change.
 */
#define LOG_DUMP_BUF_SIZE (1024 * 3)
void rc_jni_on_call_state(pjsua_call_id call_id, pjsip_event *e)
{
    pjsua_call_info call_info;
    pj_status_t status;
    char some_buf[1024 * 3];
    unsigned call_dump_len = 0;

    PJ_UNUSED_ARG(e);

    status = pjsua_call_get_info(call_id, &call_info);

    if( status != PJ_SUCCESS ){
  	  pjsua_perror(THIS_FILE, "rc_jni_on_call_state failed ", status );
  	  return;
    }

    PJ_LOG(4, (THIS_FILE,  "rc_jni_on_call_state call ID: %d state: %d last status: %d",
			call_id, (int)call_info.state, (int)call_info.last_status ));

    if (call_info.state == PJSIP_INV_STATE_DISCONNECTED) {
    	/* Stop all ringback for this call */
		ring_stop(call_id);
	    status = pjsua_call_dump(call_id, PJ_TRUE, some_buf, sizeof(some_buf), "  ");
	    if( status != PJ_SUCCESS ){
	        pjsua_perror(THIS_FILE, "rc_jni_on_call_state getting dump failed ", status );
	    } else {
	        call_dump_len = strlen(some_buf);
	        if (!(call_dump_len > 0 && call_dump_len < LOG_DUMP_BUF_SIZE)) {
	            pjsua_perror(THIS_FILE, "rc_jni_on_call_state getting dump failed len is %d ", call_dump_len );
	            call_dump_len = 0;
	        }
	    }
	}

	ATTACH_CURRENT_THREAD();

	if( jni_env != 0 && rc_cfg.rc_callbacks.cb_class != 0 && rc_cfg.rc_callbacks.cb_methodId[on_call_state_idx] != 0 ){

		int state = call_info.state;
		int last_status = call_info.last_status;
		int has_dump = 0;
		jobjectArray statusText = NULL;
		if (call_dump_len > 0) {
		    has_dump = 1;
		    statusText = (jobjectArray)jni_env->NewObjectArray( 1,
		            jni_env->FindClass("java/lang/String"),
		            jni_env->NewStringUTF(""));

		    jni_env->SetObjectArrayElement( statusText, 0, jni_env->NewStringUTF(&some_buf[0]) );
		}

		jni_env->CallStaticVoidMethod( rc_cfg.rc_callbacks.cb_class,
				rc_cfg.rc_callbacks.cb_methodId[on_call_state_idx],
				call_id,
				state,
				last_status,
				has_dump,
				statusText);
	}
	else
	    PJ_LOG(3, (THIS_FILE, "rc_jni_on_call_state() Failed. Wrong class or method ID" ));

	DETACH_CURRENT_THREAD();


}

/**
 * Notify application when registration status has changed.
 * Application may then query the account info to get the
 * registration details.
 *
 * @param acc_id	    The account ID.
 */
void rc_jni_on_reg_state(pjsua_acc_id acc_id){
    PJ_LOG(4, (THIS_FILE,  "rc_jni_on_reg_state() Starting AccID %d", acc_id ));

	ATTACH_CURRENT_THREAD();

	if( jni_env != 0 && rc_cfg.rc_callbacks.cb_class != 0 && rc_cfg.rc_callbacks.cb_methodId[on_reg_state_idx] != 0 ){

		jni_env->CallStaticVoidMethod( rc_cfg.rc_callbacks.cb_class, rc_cfg.rc_callbacks.cb_methodId[on_reg_state_idx], acc_id );
	}
	else
	    PJ_LOG(3, (THIS_FILE, "rc_jni_on_reg_state() Failed. Wrong class or method ID" ));

	DETACH_CURRENT_THREAD();

	PJ_LOG(4, (THIS_FILE, "rc_jni_on_reg_state() Finished" ));
}

/**
 * Notify application when media state in the call has changed.
 * Normal application would need to implement this callback, e.g.
 * to connect the call's media to sound device. When ICE is used,
 * this callback will also be called to report ICE negotiation
 * failure.
 *
 * @param call_id	The call index.
 */
void rc_jni_on_call_media_state(pjsua_call_id call_id)
{
    PJ_LOG(4, (THIS_FILE, "rc_jni_on_call_media_state call ID %d", call_id ));

    pjsua_call_info call_info;
    pj_status_t status;

    status = pjsua_call_get_info(call_id, &call_info);
    if( status != PJ_SUCCESS ){
  	  pjsua_perror(THIS_FILE, "rc_jni_on_call_media_state failed ", status );
  	  return;
    }

    /* Stop ringback */
    ring_stop(call_id);

    /* Connect ports appropriately when media status is ACTIVE or REMOTE HOLD,
     * otherwise we should NOT connect the ports.
     */
    if (call_info.media_status == PJSUA_CALL_MEDIA_ACTIVE ||
    		call_info.media_status == PJSUA_CALL_MEDIA_REMOTE_HOLD)
    {
        PJ_LOG(4, (THIS_FILE, "Connect to Player. call ID: %d Media state: %d Slot: %d", call_id, call_info.media_status, call_info.conf_slot ));

    	pjsua_conf_connect(call_info.conf_slot, 0);
	    pjsua_conf_connect(0, call_info.conf_slot);
    }

    /* Handle media status */
	switch (call_info.media_status) {
	case PJSUA_CALL_MEDIA_ACTIVE:
	    PJ_LOG(4, (THIS_FILE, "rc_jni_on_call_media_state Media for call %d is active", call_id ));
		break;

	case PJSUA_CALL_MEDIA_LOCAL_HOLD:
	    PJ_LOG(4, (THIS_FILE, "rc_jni_on_call_media_state Media for call %d is suspended (hold) by local", call_id));
		break;

	case PJSUA_CALL_MEDIA_REMOTE_HOLD:
	    PJ_LOG(4, (THIS_FILE, "rc_jni_on_call_media_state Media for call %d is suspended (hold) by remote", call_id ));
		break;

	case PJSUA_CALL_MEDIA_ERROR:
	    PJ_LOG(3, (THIS_FILE, "rc_jni_on_call_media_state Media has reported error, disconnecting call" ));
		{
			pj_str_t reason;
			pj_strdup2_with_null( pjsua_get_var()->pool, &reason, "ICE negotiation failed" );
			pjsua_call_hangup(call_id, 500, &reason, NULL);
		}
		break;

	case PJSUA_CALL_MEDIA_NONE:
	    PJ_LOG(3, (THIS_FILE, "rc_jni_on_call_media_state Media for call %d is inactive", call_id ));
		break;

	default:
		pj_assert(!"Unhandled media status");
		break;
	}

	ATTACH_CURRENT_THREAD();

	if( jni_env != 0 && rc_cfg.rc_callbacks.cb_class != 0 && rc_cfg.rc_callbacks.cb_methodId[on_call_media_state_idx] != 0 ){

		jni_env->CallStaticVoidMethod( rc_cfg.rc_callbacks.cb_class,
				rc_cfg.rc_callbacks.cb_methodId[on_call_media_state_idx],
				call_id,
				call_info.media_status );
	}
	else
	    PJ_LOG(3, (THIS_FILE, "rc_jni_on_call_media_state() Failed. Wrong class or method ID" ));

	DETACH_CURRENT_THREAD();

}

/**
 * Notify application on incoming pager (i.e. MESSAGE request).
 * Argument call_id will be -1 if MESSAGE request is not related to an
 * existing call.
 *
 * See also \a on_pager2() callback for the version with \a pjsip_rx_data
 * passed as one of the argument.
 *
 * @param call_id	    Containts the ID of the call where the IM was
 *			    sent, or PJSUA_INVALID_ID if the IM was sent
 *			    outside call context.
 * @param from	    URI of the sender.
 * @param to	    URI of the destination message.
 * @param contact	    The Contact URI of the sender, if present.
 * @param mime_type	    MIME type of the message.
 * @param body	    The message content.
 */
void rc_jni_on_message(pjsua_call_id call_id,
						const pj_str_t *from,
						const pj_str_t *to,
						const pj_str_t *contact,
						const pj_str_t *mime_type,
						const pj_str_t *body )
{
    PJ_LOG(4, (THIS_FILE,  "rc_jni_on_message() Starting Call ID %d", call_id ));
	ATTACH_CURRENT_THREAD();

	if( jni_env != 0 && rc_cfg.rc_callbacks.cb_class != 0 && rc_cfg.rc_callbacks.cb_methodId[on_message_idx] != 0 ){

		jobjectArray parameters = (jobjectArray)jni_env->NewObjectArray( 5,
				jni_env->FindClass("java/lang/String"),
				jni_env->NewStringUTF(""));

		jni_env->SetObjectArrayElement( parameters, 0, jni_env->NewStringUTF(from->ptr) );
		jni_env->SetObjectArrayElement( parameters, 1, jni_env->NewStringUTF(to->ptr) );
		jni_env->SetObjectArrayElement( parameters, 2, jni_env->NewStringUTF(contact->ptr) );
		jni_env->SetObjectArrayElement( parameters, 3, jni_env->NewStringUTF(mime_type->ptr) );
		jni_env->SetObjectArrayElement( parameters, 4, jni_env->NewStringUTF(body->ptr) );

		jni_env->CallStaticVoidMethod( rc_cfg.rc_callbacks.cb_class,
					rc_cfg.rc_callbacks.cb_methodId[on_message_idx],
					call_id,
					parameters );
	}
	else
	    PJ_LOG(3, (THIS_FILE, "rc_jni_on_message() Failed. Wrong class or method ID" ));


	DETACH_CURRENT_THREAD();
	PJ_LOG(4, (THIS_FILE,  "rc_jni_on_message() Finished" ));
}

/**
 * Notify application on incoming call.
 *
 * @param acc_id	The account which match the incoming call.
 * @param call_id	The call id that has just been created for
 *			the call.
 * @param rdata	The incoming INVITE request.
 */
void rc_jni_on_incoming_call(pjsua_acc_id acc_id,
			pjsua_call_id call_id,
			pjsip_rx_data *rdata)
{
	char uri[PJSIP_MAX_URL_SIZE];
	pj_str_t rc_hdr_name;
	PJ_LOG(4, (THIS_FILE, "rc_jni_on_incoming_call() Starting Account ID %d Call ID %d ", acc_id, call_id ));

	/*
	 * Add RC Call Information
	 */
	rc_call_info* rcCallInfo = (rc_call_info*)pj_pool_zalloc( rc_cfg.pool, sizeof(rc_call_info));
	rcCallInfo->status = 0;
	pjsua_call_set_user_data( call_id, rcCallInfo );

	ATTACH_CURRENT_THREAD();

	if( jni_env != 0 && rc_cfg.rc_callbacks.cb_class != 0 && rc_cfg.rc_callbacks.cb_methodId[on_incoming_call_idx] != 0 ){

		/*
		 * headers [0] - From
		 * headers [1] - To
		 * headers [2] - P-rc, if present
		 * headers [3] - Call-ID
		 */
		jobjectArray headers = (jobjectArray)jni_env->NewObjectArray( 4,
				jni_env->FindClass("java/lang/String"),
				jni_env->NewStringUTF(""));

		/*
		 * From header
		 */
		pjsip_uri_print(PJSIP_URI_IN_FROMTO_HDR,
						rdata->msg_info.from->uri,
						uri,
						PJSIP_MAX_URL_SIZE);

		jni_env->SetObjectArrayElement( headers, 0, jni_env->NewStringUTF(uri) );

		/*
		 * To header
		 */
		pjsip_uri_print(PJSIP_URI_IN_FROMTO_HDR,
						rdata->msg_info.to->uri,
						uri,
						PJSIP_MAX_URL_SIZE);

		jni_env->SetObjectArrayElement( headers, 1, jni_env->NewStringUTF(uri) );

		/*
		 * Call-ID
		 */
		jni_env->SetObjectArrayElement( headers, 3, jni_env->NewStringUTF(rdata->msg_info.cid->id.ptr) );

		/*
		 * P-rc header, if present
		 */
		pj_strdup2_with_null( rc_cfg.pool, &rc_hdr_name,  RC_HEADER_NAME );
		pjsip_generic_string_hdr* hdr = (pjsip_generic_string_hdr*)pjsip_msg_find_hdr_by_name( rdata->msg_info.msg, &rc_hdr_name, NULL );
		if( hdr != NULL ){
		    PJ_LOG(4, (THIS_FILE,  "rc_jni_on_incoming_call() Found RC custom headers "));
			if( hdr->hvalue.ptr != NULL ){

				/*
				 * Add header to the RC additional info
				 */
				pj_strdup2_with_null( rc_cfg.pool, &rcCallInfo->rc_header, hdr->hvalue.ptr );

				jni_env->SetObjectArrayElement( headers, 2, jni_env->NewStringUTF(hdr->hvalue.ptr) );
			}
		}

		jni_env->CallStaticVoidMethod( rc_cfg.rc_callbacks.cb_class,
					rc_cfg.rc_callbacks.cb_methodId[on_incoming_call_idx],
					acc_id,
					call_id,
					headers );

	}
	else
	    PJ_LOG(3, (THIS_FILE, "rc_jni_on_incoming_call() Failed. Wrong class or method ID" ));


	DETACH_CURRENT_THREAD();
	PJ_LOG(4, (THIS_FILE,  "rc_jni_on_incoming_call() Finished" ));
}

/**
 * Notify application when media session has been unregistered from the
 * conference bridge and about to be destroyed.
 *
 * @param call_id	    Call identification.
 * @param sess	    Media session for the call.
 * @param stream_idx    Stream index in the media session.
 */
void rc_jni_on_stream_destroyed(pjsua_call_id call_id,
								pjmedia_session *sess,
								unsigned stream_idx)
{
    PJ_LOG(4, (THIS_FILE,  "rc_jni_on_stream_destroyed() callId %d", call_id ));

    ATTACH_CURRENT_THREAD();

	if( jni_env != 0 && rc_cfg.rc_callbacks.cb_class != 0 && rc_cfg.rc_callbacks.cb_methodId[on_stream_destroyed_idx] != 0 ){

		jni_env->CallStaticVoidMethod( rc_cfg.rc_callbacks.cb_class,
				rc_cfg.rc_callbacks.cb_methodId[on_stream_destroyed_idx],
				call_id );
	}
	else
	    PJ_LOG(3, (THIS_FILE, "rc_jni_on_stream_destroyed() Failed. Wrong class or method ID" ));

	DETACH_CURRENT_THREAD();

}

/**
 * Notify application of the status of previously sent call
 * transfer request. Application can monitor the status of the
 * call transfer request, for example to decide whether to
 * terminate existing call.
 *
 * @param call_id	    Call ID.
 * @param st_code	    Status progress of the transfer request.
 * @param st_text	    Status progress text.
 * @param final	    If non-zero, no further notification will
 *			    be reported. The st_code specified in
 *			    this callback is the final status.
 * @param p_cont	    Initially will be set to non-zero, application
 *			    can set this to FALSE if it no longer wants
 *			    to receive further notification (for example,
 *			    after it hangs up the call).
 */
void rc_jni_on_call_transfer_status(pjsua_call_id call_id,
			    int st_code,
			    const pj_str_t *st_text,
			    pj_bool_t final,
			    pj_bool_t *p_cont)
{
    PJ_LOG(4, (THIS_FILE, "rc_jni_on_call_transfer_status() callId %d status %d", call_id, st_code ));

    ATTACH_CURRENT_THREAD();

	if( jni_env != 0 && rc_cfg.rc_callbacks.cb_class != 0 &&
			rc_cfg.rc_callbacks.cb_methodId[on_call_transfer_status_idx] != 0 ){

		jobjectArray statusText = (jobjectArray)jni_env->NewObjectArray( 1,
				jni_env->FindClass("java/lang/String"),
				jni_env->NewStringUTF(""));

		jni_env->SetObjectArrayElement( statusText, 0, jni_env->NewStringUTF(st_text->ptr) );

		jni_env->CallStaticVoidMethod( rc_cfg.rc_callbacks.cb_class,
				rc_cfg.rc_callbacks.cb_methodId[on_call_transfer_status_idx],
				call_id,
				st_code,
				statusText );
	}
	else
	    PJ_LOG(3, (THIS_FILE, "rc_jni_on_call_transfer_status() Failed. Wrong class or method ID" ));

	DETACH_CURRENT_THREAD();

}

#ifdef __cplusplus
}
#endif
