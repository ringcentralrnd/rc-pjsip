/*
 * rc_pjsip_jni.cpp
 *
 * Copyright (C) 2011-2012, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 *  Implementation of PJSIP JNI interface
 */

#include <rc_pjsip_jni.h>

#define THIS_FILE	"rc_pjsip_jni.cpp"



struct rc_config_struct rc_cfg;

#ifdef __cplusplus
extern "C" {
#endif


/*******************************************************************************************************************************************/
#ifndef SMART_LOGGING
#define SMART_LOGGING 1
#endif

#if(1 == SMART_LOGGING)
jclass		LoggerLoggingClass = NULL;
jmethodID	POSTLogMSGMethod = NULL;

jclass		getClass();
jmethodID	getMethod();
/*******************************************************************************************************************************************/

jmethodID getMethod() {
	jclass tempLnkToMKTClass = NULL;
	ATTACH_CURRENT_THREAD(PJ_EUNKNOWN);

	if (NULL != (tempLnkToMKTClass = getClass())) {
		if (NULL == POSTLogMSGMethod) {
            // Param 1 Type:
	        //    0 MARKET
            //    1 QA
		    // Param 2 LogLevel:
		    //    0 INFORMATION
		    //    1 VERBOSE
		    //    2 DEBUG
		    //    3 WARNING
		    //    4 ERROR
			POSTLogMSGMethod  = jni_env->GetStaticMethodID(tempLnkToMKTClass, "postLog", "(IILjava/lang/String;)V");
		}

		if (NULL == POSTLogMSGMethod) {
			LOG_WarnLogcat("NtvLog.postLog There is no such method");
		}
	}
	DETACH_CURRENT_THREAD();

	return POSTLogMSGMethod;
}

jclass getClass() {
	jclass tmpClassLnk = NULL;
	ATTACH_CURRENT_THREAD(PJ_EUNKNOWN);

	if (NULL == LoggerLoggingClass) {
	    LOG_WarnLogcat("Loading NtvLog class");
		LoggerLoggingClass = jni_env->FindClass("com/rcbase/android/logging/NtvLog");
	}

	if (NULL == LoggerLoggingClass) {
	    LOG_WarnLogcat("NtvLog class not found");
	}
	tmpClassLnk = LoggerLoggingClass;

	DETACH_CURRENT_THREAD();

	return tmpClassLnk;
}
#endif

void pj_android_log_msg(int level, const char *data, int len) {
#if(1 == SMART_LOGGING)
	jmethodID callingMethod = NULL;
	jclass usingClass = NULL;
	ATTACH_CURRENT_THREAD(PJ_EUNKNOWN);

	callingMethod = getMethod();
	usingClass = getClass();
	jstring Trace = jni_env->NewStringUTF(data);
#endif

	switch (level) {
	case 1:
#if(1 == SMART_LOGGING)
		if( NULL != callingMethod && NULL != usingClass) {
            // Param 1 Type:
            //    0 MARKET
            //    1 QA
            // Param 2 LogLevel:
            //    0 INFORMATION
            //    1 VERBOSE
            //    2 DEBUG
            //    3 WARNING
            //    4 ERROR
			jni_env->CallStaticVoidMethod(usingClass, callingMethod, 0, 4, Trace);
		}
#else
		LOG_ErrorLogcat("%s", data );
#endif
		break;
	case 2:
#if(1 == SMART_LOGGING)
		if( NULL != callingMethod && NULL != usingClass) {
            // Param 1 Type:
            //    0 MARKET
            //    1 QA
            // Param 2 LogLevel:
            //    0 INFORMATION
            //    1 VERBOSE
            //    2 DEBUG
            //    3 WARNING
            //    4 ERROR
			jni_env->CallStaticVoidMethod(usingClass, callingMethod, 0, 4, Trace);
		}
#else
		LOG_WarnLogcat("%s", data );
#endif
		break;
	case 3:
#if(1 == SMART_LOGGING)
		if( NULL != callingMethod && NULL != usingClass) {
            // Param 1 Type:
            //    0 MARKET
            //    1 QA
            // Param 2 LogLevel:
            //    0 INFORMATION
            //    1 VERBOSE
            //    2 DEBUG
            //    3 WARNING
            //    4 ERROR
			jni_env->CallStaticVoidMethod(usingClass, callingMethod, 0, 3, Trace);
		}
#else
		LOG_InfoLogcat("%s", data );
#endif
		break;
	case 4:
#if(1 == SMART_LOGGING)
		if( NULL != callingMethod && NULL != usingClass) {
            // Param 1 Type:
            //    0 MARKET
            //    1 QA
            // Param 2 LogLevel:
            //    0 INFORMATION
            //    1 VERBOSE
            //    2 DEBUG
            //    3 WARNING
            //    4 ERROR
			jni_env->CallStaticVoidMethod(usingClass, callingMethod, 0, 0, Trace);
		}
#else
		LOG_DebugLogcat("%s", data );
#endif
		break;
	case 5:
#if(1 == SMART_LOGGING)
		if( NULL != callingMethod && NULL != usingClass) {
            // Param 1 Type:
            //    0 MARKET
            //    1 QA
            // Param 2 LogLevel:
            //    0 INFORMATION
            //    1 VERBOSE
            //    2 DEBUG
            //    3 WARNING
            //    4 ERROR
			jni_env->CallStaticVoidMethod(usingClass, callingMethod, 0, 2, Trace);
		}
#else
		LOG_VerboseLogcat("%s", data );
#endif
		break;
	default:
		LOG_VerboseLogcat("%s", data );
		break;
	}

#if(1 == SMART_LOGGING)
	jni_env->DeleteLocalRef(Trace);
	DETACH_CURRENT_THREAD();
#endif
}

/*
 *	List of methods
 */
static JNINativeMethod pjsip_methods[] = {
		{ "pjsip_create", 	"()I", 						(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1create },
		{ "pjsip_destroy", 	"()I", 						(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1destroy },
		{ "pjsip_init", 	"(Ljava/lang/String;I)I",   (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1init },
		{ "pjsip_start", 	"()I", 						(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1start },

		{ "pjsip_acc_add",	"([Ljava/lang/String;[IJ)I",	(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1add },
//by martin 
		{ "pjsip_tsc_tunnel_set","(ILjava/lang/String;II)I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1tsc_1tunnel_1set},

		{ "pjsip_acc_get_default", 	"()I", 				(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1get_1default },
		{ "pjsip_acc_modify", "(I[Ljava/lang/String;J)I",(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1modify },
		{ "pjsip_acc_del",			"(I)I", 			(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1del },
		{ "pjsip_acc_get_status", 	"(I[I)I",			(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1get_1status },
		{ "pjsip_acc_is_valid",		"(I)I",				(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1is_1valid },
		{ "pjsip_acc_get_info",		"(I[Ljava/lang/String;)I",	(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1get_1info },
		{ "pjsip_acc_set_registration", "(IZ)I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1set_1registration},

		{ "pjsip_ring_start",		"(I)V",				(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1ring_1start },
		{ "pjsip_ringback_start",	"(I)V",				(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1ringback_1start },
		{ "pjsip_ring_stop",		"(I)V",				(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1ring_1stop },

		{ "pjsip_enum_codecs", 		"([Ljava/lang/String;[I[I)I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1enum_1codecs },
		{ "pjsip_codec_set_priority", "(Ljava/lang/String;I)I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1codec_1set_1priority },

		{ "pjsip_call_answer", 		"(II[Ljava/lang/String;)I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1answer },
		{ "pjsip_call_hangup", 		"(II[Ljava/lang/String;)I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1hangup },
		{ "pjsip_call_hold", 		"(I)I",  (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1hold },
		{ "pjsip_call_unhold", 		"(I)I",  (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1unhold },
		{ "pjsip_call_make", 		"(ILjava/lang/String;[Ljava/lang/String;[I)I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1make },
		{ "pjsip_call_send_request","(ILjava/lang/String;Ljava/lang/String;)I",  (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1send_1request },
		{ "pjsip_call_attach_sound","(I)I",  (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1attach_1sound },
		{ "pjsip_call_transfer", 	"(ILjava/lang/String;)I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1transfer },
		{ "pjsip_call_hangup_all",	"()V", 	 (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1hangup_1all },
		{ "pjsip_call_reinvate_all","()I", 	 (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1reinvate_1all},
		{ "pjsip_call_dial_dtmf",	"(ILjava/lang/String;)I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1dial_1dtmf},
		{ "pjsip_call_meia_disconnect", "(I)I",  (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1meia_1disconnect },

		{ "pjsip_call_is_active", 	"(I)I",  (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1is_1active },
		{ "pjsua_call_has_media", 	"(I)I",  (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsua_1call_1has_1media },
		{ "pjsip_call_get_max_count","()I",  (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1get_1max_1count },
		{ "pjsip_call_get_count", 	"()I", 	 (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1get_1count },
		{ "pjsip_call_enum_calls", 	"([I[I)I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1enum_1calls },
		{ "pjsip_call_get_info", 	"(I[Ljava/lang/String;[I)I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1get_1info },
		{ "pjsip_call_get_duration","(I[J)I",(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1get_1duration },

		{ "pjsip_conf_get_signal_rx_level", "(I)F",(void*)&  Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1get_1signal_1rx_1level },
		{ "pjsip_conf_get_signal_tx_level", "(I)F",(void*)&  Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1get_1signal_1tx_1level },
		{ "pjsip_conf_adjust_tx_level", 	"(IF)I",(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1adjust_1tx_1level },
		{ "pjsip_conf_adjust_rx_level", 	"(IF)I",(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1adjust_1rx_1level},
		{ "pjsip_conf_get_adjust_tx_level", "(I)F",(void*)&  Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1get_1adjust_1tx_1level },
		{ "pjsip_conf_get_adjust_rx_level", "(I)F",(void*)&  Java_com_rcbase_android_sip_service_PJSIP_pjsip_1conf_1get_1adjust_1rx_1level},

		{ "pjsip_set_snd_dev", 	"(II)I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1set_1snd_1dev},
		{ "pjsip_set_no_snd_dev", "()I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1set_1no_1snd_1dev},
		{ "pjsip_get_snd_dev",	"([I)I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1get_1snd_1dev},
		{ "pjsip_media_transports_create", "(I)I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1media_1transports_1create },
		{ "pjsip_media_transports_update", "(I)I", (void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1media_1transports_1update },


		// setup wizard functionality
		{ "pjsip_start_test",		"()I",				(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1start_1test },
		{ "pjsip_stop_test",		"()I",				(void*)& Java_com_rcbase_android_sip_service_PJSIP_pjsip_1stop_1test },
};

static JNINativeMethod pjsip_aodiodevice_methods[] = {
		{ "writeToRTP",  "([BIJJ)I", (void*)& Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_writeToRTP},
		{ "readFromRTP", "([BIJJ)I", (void*)& Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_readFromRTP},
		{ "aec", 		 "([B[BIJ)I", (void*)& Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aec},

		// setup wizard functionality
		{ "aecSetTestMode", 		"(J)I", (void*)& Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aecSetTestMode},
		{ "aecGetCurrentTestMode", 	"(J)I", (void*)& Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aecGetCurrentTestMode},
		{ "aecGetMicDb", 	 		"(J)I", (void*)& Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aecGetMicDb},
		{ "aecGetAudioDelay", 		"(J)I", (void*)& Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aecGetAudioDelay},
		{ "getTimestamp",           "(J)J", (void*)& Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_getTimestamp},
		{ "getElapsedTime",       "(JJJ)J", (void*)& Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_getElapsedTime},
		{ "readFromWriteToRTP","([B[BIJJ)I", (void*)& Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_readFromWriteToRTP},
		{ "aecScheme2",         "([B[BIJ)I", (void*)& Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aecScheme2},

};

/*
 *	JNI_OnLoad
 */
JNIEXPORT jint JNI_OnLoad(JavaVM *jvm, void *reserved) {
	JNIEnv *env;
	jclass cls;

	java_vm = jvm;

	LOG_InfoLogcat( "JNI_OnLoad Started" );

	jint result = jvm->GetEnv ((void **) &env, JNI_VERSION_1_4);

	/*
	 * Register native methods
	 */
	cls = env->FindClass("com/rcbase/android/sip/service/PJSIP");
	result = env->RegisterNatives (cls, pjsip_methods, (int) (sizeof(pjsip_methods) / sizeof(pjsip_methods[0])));

	cls = env->FindClass("com/rcbase/android/sip/audio/AudioDevicePJSIP");
	result = env->RegisterNatives (cls, pjsip_aodiodevice_methods, (int) (sizeof(pjsip_aodiodevice_methods) / sizeof(pjsip_aodiodevice_methods[0])));
	LOG_InfoLogcat( "JNI_OnLoad Floating Point flag %d", PJ_HAS_FLOATING_POINT );
	LOG_InfoLogcat( "JNI_OnLoad Finished %d", result );


	/*
	 * Detect method ID for callback functions
	 */
	rc_cfg.rc_callbacks.cb_class = env->FindClass("com/rcbase/android/sip/service/PJSIPCallback");

	rc_cfg.rc_callbacks.cb_methodId[on_reg_state_idx] 		= env->GetStaticMethodID( rc_cfg.rc_callbacks.cb_class,
																"on_reg_state",
																"(I)V");

	rc_cfg.rc_callbacks.cb_methodId[on_message_idx] 		= env->GetStaticMethodID( rc_cfg.rc_callbacks.cb_class,
																"on_message",
																"(I[Ljava/lang/String;)V");

	rc_cfg.rc_callbacks.cb_methodId[on_incoming_call_idx] 	= env->GetStaticMethodID( rc_cfg.rc_callbacks.cb_class,
																"on_incoming_call",
																"(II[Ljava/lang/String;)V");

	rc_cfg.rc_callbacks.cb_methodId[on_call_media_state_idx]= env->GetStaticMethodID( rc_cfg.rc_callbacks.cb_class,
																"on_call_media_state",
																"(II)V");

	rc_cfg.rc_callbacks.cb_methodId[on_stream_destroyed_idx]= env->GetStaticMethodID( rc_cfg.rc_callbacks.cb_class,
																"on_stream_destroyed",
																"(I)V");

	rc_cfg.rc_callbacks.cb_methodId[on_call_state_idx]= env->GetStaticMethodID( rc_cfg.rc_callbacks.cb_class,
																"on_call_state",
																"(IIII[Ljava/lang/String;)V");

	rc_cfg.rc_callbacks.cb_methodId[on_call_transfer_status_idx] 	= env->GetStaticMethodID( rc_cfg.rc_callbacks.cb_class,
																"on_call_transfer_status",
																"(II[Ljava/lang/String;)V");

	rc_cfg.rc_callbacks.cb_methodId[on_media_failed_idx] 		= env->GetStaticMethodID( rc_cfg.rc_callbacks.cb_class,
																"on_media_failed",
																"(I)V");

	rc_cfg.AudioDriver_class = env->FindClass("com/rcbase/android/sip/audio/AudioDevice");

	return JNI_VERSION_1_4;
}

/*
 * JNI_OnUnload
 */
JNIEXPORT void JNI_OnUnload(JavaVM* vm, void* reserved){

}

#ifdef __cplusplus
}
#endif
