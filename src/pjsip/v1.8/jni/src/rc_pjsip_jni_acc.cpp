/*
 * rc_pjsip_jni_acc.cpp
 *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 *  Implementation of PJSIP JNI interface
 *  Account configuration functions
 */

#include <rc_pjsip_jni.h>

#define THIS_FILE	"rc_pjsip_jni_acc.cpp"

#ifdef __cplusplus
extern "C" {
#endif

/*
 *
 */
void fillInAccountConfig(JNIEnv *jenv,
		pjsua_acc_config* acc_conf,
		pj_pool_t *pool,
		jint size,
		jobjectArray accountParameters,
		jlong forcedExpires)
{
	  acc_conf->transport_id = 0;
	  acc_conf->forcedExpiration = forcedExpires;
	  for( int i = 0; i < size; i++ ){
		  jstring jstrPrm = (jstring)jenv->GetObjectArrayElement( accountParameters, i );
		  jsize prmLen = jenv->GetStringUTFLength( jstrPrm );

		  if( prmLen == 0 )
			  continue;

		  const char* ptrPrm = jenv->GetStringUTFChars( jstrPrm, JNI_FALSE );
		  if( ptrPrm == NULL  )
			  continue;

		  switch( i ){
			  case 0: //account
				  PJ_LOG(3, (THIS_FILE, "pjsip_acc_add Set account to %s", ptrPrm ));
				  pj_strdup2_with_null( pool, &acc_conf->id, ptrPrm );
				  break;
			  case 1: //service provider
				  PJ_LOG(3, (THIS_FILE, "pjsip_acc_add Set service provider to %s", ptrPrm ));
				  pj_strdup2_with_null( pool, &acc_conf->reg_uri, ptrPrm );
				  break;
			  case 2:
				  PJ_LOG(3, (THIS_FILE, "pjsip_acc_add Set user name to %s", ptrPrm ));
				  acc_conf->cred_count = 1;
				  acc_conf->cred_info[0].data_type = 0;
				  pj_strdup2_with_null( pool, &acc_conf->cred_info[0].realm, "*" );
				  pj_strdup2_with_null( pool, &acc_conf->cred_info[0].scheme, "Digest" );
				  pj_strdup2_with_null( pool, &acc_conf->cred_info[0].username, ptrPrm );
				  break;
			  case 3:
				  pj_strdup2_with_null( pool, &acc_conf->cred_info[0].data, ptrPrm );
				  break;
			  case 4:
				  PJ_LOG(3, (THIS_FILE, "pjsip_acc_add Set SIP outbound proxy to %s", ptrPrm ));
				  acc_conf->proxy_cnt = 1;
				  pj_strdup2_with_null( pool, &acc_conf->proxy[0], ptrPrm );
				  break;
		  }

		  jenv->ReleaseStringUTFChars( jstrPrm, ptrPrm );
	  }

}

/**
 * Detect transport protocol is used for this account
 */
pjsua_transport_id getAccountProtocol(){
	pjsua_transport_id id = PJSUA_INVALID_ID;
	pjsua_transport_info trInfo;
	pjsua_transport_id trId[2];
	unsigned counter = 2;

	pj_status_t status = pjsua_enum_transports( trId, &counter );
	if( status == PJ_SUCCESS ){
		for( int i = 0; i < counter; i++ ){
			status = pjsua_transport_get_info( trId[i], &trInfo );
			if( status == PJ_SUCCESS ){
				  PJ_LOG(3, (THIS_FILE, "getAccountProtocol Detected transport type:%d Id:%d", trInfo.type, trInfo.id ));

				if( trInfo.type == PJSIP_TRANSPORT_TCP ){
					id = trInfo.id;
					break;
				}
			}
		}
	}

	return id;
}

/*
 * SIP Stack account configuration functions
 * pjsip_acc_add()
 */
#define RC_ACCOUNT_PARAMETERS_COUNT	 5
#define RC_ACCOUNT_ID_COUNT			 1
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1add(JNIEnv *jenv,
		jclass,
		jobjectArray accountParameters,
		jintArray accountId,
		jlong forcedExpires)
{
	  pj_status_t status;
	  pjsua_acc_config acc_conf;
	  pjsua_acc_id acc_id = 0;

	  PJ_LOG(4, (THIS_FILE, "pjsip_acc_add is starting: forcedExpires=%d", forcedExpires ));

	  jint size = jenv->GetArrayLength( accountParameters );

	  /* check size of account parameters array */
	  if( size != RC_ACCOUNT_PARAMETERS_COUNT ){
		  PJ_LOG(3, (THIS_FILE, "pjsip_acc_add Wrong number of account parameters %d", size ));
		  return PJ_EINVAL;
	  }

	  /* check size of account IDs array */
	  if( RC_ACCOUNT_ID_COUNT != jenv->GetArrayLength( accountId ) ){
		  PJ_LOG(3, (THIS_FILE, "pjsip_acc_add Wrong number of account ID" ));
		  return PJ_EINVAL;
	  }

	  /* set default values for a new account */
	  pjsua_acc_config_default( &acc_conf );

	  fillInAccountConfig(jenv, &acc_conf, rc_cfg.pool, size, accountParameters, forcedExpires );

	  acc_conf.transport_id = PJSUA_INVALID_ID;
	  acc_conf.reg_timeout = RC_ACCOUNT_SIP_REGISTRATION_TIMEOUT;

	  status = pjsua_acc_add( &acc_conf, PJ_TRUE, &acc_id );
	  if( status == PJ_SUCCESS ){
		  PJ_LOG(4, (THIS_FILE, "pjsua_acc_add Successful Account ID %d", acc_id ));
		  jenv->SetIntArrayRegion(accountId, 0, 1, &acc_id );
	  }
	  else
		  pjsua_perror(THIS_FILE, "pjsua_acc_add failed ", status );

	  PJ_LOG(4, (THIS_FILE, "pjsip_acc_add finished %d", status ));
	  return (jint) status;
}

JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1tsc_1tunnel_1set(JNIEnv *jenv,
		jclass,
		jint enable,
		jstring ip,
		jint  port,
		jint red)
{

extern int tsc_enable;
//extern char tsc_tunnel_addr[6*4];
extern unsigned int tsc_tunnel_port;
extern int redundancy_factor;

const char* pdstURI;
//extern pj_str_t tsc_tunnel_addr;
extern char tsc_tunnel_addr[6*4];

//PJ_LOG(4, (THIS_FILE, "bymartin enter in tsc_tunnel_set"));
LOG_InfoLogcat("bymartin enter in tsc_tunnel_set\n");



tsc_enable=enable;
LOG_InfoLogcat("bymartin enter in tsc_tunnel_set step2\n");


tsc_tunnel_port=port;
LOG_InfoLogcat("bymartin enter in tsc_tunnel_set step3\n");

redundancy_factor=red;
LOG_InfoLogcat("bymartin enter in tsc_tunnel_set step4 tsc_enable=%d,tsc_tunnel_port=%d,red=%d\n",tsc_enable,tsc_tunnel_port,redundancy_factor);

pdstURI = jenv->GetStringUTFChars( ip, JNI_FALSE );
LOG_InfoLogcat("bymartin in tsc_tunnel_set step5 pdstURI=%s\n",pdstURI);

//PJ_LOG(4, (THIS_FILE, "bymartin in tsc_tunnel_set,we get pdstURI=%s\n",pdstURI));
memset(tsc_tunnel_addr,0,sizeof(tsc_tunnel_addr));
memcpy(tsc_tunnel_addr,pdstURI,strlen(pdstURI));

//pj_strdup2_with_null( rc_cfg.pool, &tsc_tunnel_addr, pdstURI );
LOG_InfoLogcat("bymartin enter in tsc_tunnel_set step6 tsc_tunnel_addr=%s\n",tsc_tunnel_addr);
jenv->ReleaseStringUTFChars( ip, pdstURI );


//PJ_LOG(4, (THIS_FILE, "bymartin in tsc_tunnel_set,we get tsc_tunnel_addr=%s \n",tsc_tunnel_addr.ptr));

//ok, all para were passed to JNI.

LOG_InfoLogcat("bymartin leaving tsc_tunnel_set step7\n");

return 0;
}


/*
 *	pjsip_acc_get_default()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1get_1default(JNIEnv *, jclass)
{
	pjsua_acc_id acc_def =  pjsua_acc_get_default();
	return (jint)acc_def;
}

/*
 * 	pjsip_acc_modify()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1modify(JNIEnv *jenv,
		jclass,
		jint accountId,
		jobjectArray accountParameters,
		jlong forcedExpires)
{
  pj_status_t status;
  pjsua_acc_id acc_id = 0;
  pjsua_acc_config acc_conf;

  PJ_LOG(4, (THIS_FILE, "pjsua_acc_modify is starting..." ));

  jint size = jenv->GetArrayLength( accountParameters );

  /* check size of account parameters array */
  if( size != RC_ACCOUNT_PARAMETERS_COUNT ){
	  PJ_LOG(3, (THIS_FILE, "pjsua_acc_modify Wrong number of account parameters %d", size ));
	  return PJ_EINVAL;
  }

  /* set default values for a new account */
  pjsua_acc_config_default( &acc_conf );

  fillInAccountConfig(jenv, &acc_conf, rc_cfg.pool, size, accountParameters,  forcedExpires);

  status = pjsua_acc_modify( accountId, &acc_conf );
  if( status != PJ_SUCCESS ){
	  pjsua_perror(THIS_FILE, "pjsua_acc_modify failed ", status );
  }

  PJ_LOG(4, (THIS_FILE, "pjsua_acc_modify finished %d", status ));
  return (jint)status;
}

/*
 *	pjsip_acc_del()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1del(JNIEnv *,
		jclass,
		jint accountId )
{
	  pj_status_t status;
	  pjsua_acc_id acc_id = accountId;

	  PJ_LOG(4, (THIS_FILE, "pjsip_acc_del is starting..." ));

	  status = pjsua_acc_del( acc_id );
	  if( status != PJ_SUCCESS ){
		  pjsua_perror(THIS_FILE, "pjsip_acc_del failed ", status );
	  }

	  PJ_LOG(4, (THIS_FILE, "pjsip_acc_del finished %d", status ));
	  return (jint)status;
}

/*
 * 	pjsip_acc_get_status()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1get_1status(JNIEnv *jenv,
		jclass,
		jint accountId,
		jintArray regStatus )
{
	  pjsua_acc_info acc_info;
	  pjsua_acc_id acc_id = accountId;
	  pj_status_t status;

	  PJ_LOG(4, (THIS_FILE, "pjsip_acc_get_status is starting..." ));

	  status = pjsua_acc_get_info( acc_id, &acc_info );
	  if( status != PJ_SUCCESS ){
		  pjsua_perror(THIS_FILE, "pjsip_acc_get_status failed ", status );
		  return (jint)status;
	  }

	  if( jenv->GetArrayLength( regStatus ) > 1 ){
		  PJ_LOG(4, (THIS_FILE, "pjsip_acc_get_status Successful Registration status %d", acc_id ));
		  jint rstatus = acc_info.status;
		  jenv->SetIntArrayRegion(regStatus, 0, 1, &rstatus );
		  rstatus = acc_info.expires;
		  jenv->SetIntArrayRegion(regStatus, 1, 1, &rstatus );
	  } else {
	      status = 70019;
	      pjsua_perror(THIS_FILE, "pjsip_acc_get_status failed (array is too small) ", status );
	  }

	  PJ_LOG(4, (THIS_FILE, "pjsip_acc_get_status finished %d", status ));
	  return (jint)status;
}

/*
 * 	pjsip_acc_is_valid
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1is_1valid(JNIEnv *,
		jclass,
		jint accountId)
{
	pjsua_acc_id acc_id = accountId;

	PJ_LOG(4, (THIS_FILE, "pjsip_acc_is_valid is starting..." ));

	return (jint)pjsua_acc_is_valid(accountId);
}

/*
 *  pjsip_acc_get_info()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1get_1info(JNIEnv *jenv,
		jclass,
		jint accountId,
		jobjectArray accountInfo )
{
	  pjsua_acc_info acc_info;
	  pjsua_acc_id acc_id = accountId;
	  pj_status_t status;

	  PJ_LOG(4, (THIS_FILE, "pjsip_acc_get_info is starting..." ));

	  jint size = jenv->GetArrayLength( accountInfo );
	  if( size == 0 ){
		  pjsua_perror(THIS_FILE, "pjsip_acc_get_info Wrong number of account parameters", PJ_EINVAL );
		  return PJ_EINVAL;
	  }

	  status = pjsua_acc_get_info( acc_id, &acc_info );
	  if( status != PJ_SUCCESS ){
		  pjsua_perror(THIS_FILE, "pjsip_acc_get_info failed ", status );
		  return (jint)status;
	  }

	  jenv->SetObjectArrayElement( accountInfo, 0, jenv->NewStringUTF(acc_info.acc_uri.ptr));

	  PJ_LOG(4, (THIS_FILE, "pjsip_acc_get_info finished %d", status ));
	  return (jint)status;
}

/*
 * pjsip_acc_set_registration
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1acc_1set_1registration(JNIEnv *,
	  jclass,
	  jint accountId,
	  jboolean renew )
{
	pjsua_acc_id acc_id = accountId;
	PJ_LOG(4, (THIS_FILE, "pjsip_acc_set_registration is starting..." ));

	pj_status_t status = pjsua_acc_set_registration( accountId, ( renew == JNI_TRUE ? PJ_TRUE : PJ_FALSE ));
	if( status != PJ_SUCCESS ){
		  pjsua_perror(THIS_FILE, "pjsip_acc_set_registration failed ", status );
	}

	return (jint)status;
}

#ifdef __cplusplus
}
#endif
