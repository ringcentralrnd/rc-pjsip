/*
 * rc_pjsip_jni_call_info.cpp
 *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 *  Implementation of PJSIP JNI interface
 *  SIP Stack Call information functions.
 */

#include <rc_pjsip_jni.h>
#include <rc_android_audio_dev.h>

#define THIS_FILE	"rc_pjsip_jni_call_info.cpp"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * pjsip_call_is_active()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1is_1active(JNIEnv *, jclass, jint callId)
{
	jint result = pjsua_call_is_active( callId );
	return result;
}

/*
 * Method:    pjsua_call_has_media
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsua_1call_1has_1media(JNIEnv *, jclass, jint callId)
{
	return(jint)pjsua_call_has_media( callId );
}

/*
 * Method:    pjsip_call_get_max_count
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1get_1max_1count(JNIEnv *, jclass)
{
	return (jint)pjsua_call_get_max_count();
}

/*
 * Method:    pjsip_call_get_count
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1get_1count(JNIEnv *, jclass)
{
	pjsua_call_id calls[PJSUA_MAX_CALLS];
	unsigned call_count = PJSUA_MAX_CALLS;

	jint counter = pjsua_call_get_count();

	PJ_LOG(3, (THIS_FILE, "pjsip_call_is_active Active calls %d", counter ));
	return counter;
}

/*
 * Method:    pjsip_call_enum_calls
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1enum_1calls(JNIEnv *jenv,
	jclass,
	jintArray callId,
	jintArray count )
{
	pj_status_t status;

	jint size = jenv->GetArrayLength( callId );

	unsigned call_count = pjsua_call_get_max_count();
	if( call_count == 0 ){
		return PJ_ENOTFOUND;
	}

	pjsua_call_id* calls_array = (pjsua_call_id*)pj_pool_zalloc( rc_cfg.pool, sizeof(pjsua_call_id) * call_count );
	if( calls_array == NULL ){
		pjsua_perror(THIS_FILE, "pjsip_call_enum_calls failed ", PJ_ENOMEM );
		return PJ_ENOMEM;
	}

	status = pjsua_enum_calls( calls_array, &call_count );
	if( status != PJ_SUCCESS ){
		pjsua_perror(THIS_FILE, "pjsip_call_enum_calls failed ", status );
		return status;
	}

	jint ji_call_count = (jint)call_count;
	jenv->SetIntArrayRegion(count, 0, 1, &ji_call_count );

	if( size < call_count ){
		pjsua_perror(THIS_FILE, "pjsip_call_enum_calls failed ", PJ_ETOOSMALL );
		return PJ_ETOOSMALL;
	}

	for( int i = 0; i < call_count; i++ ){
		  jenv->SetIntArrayRegion(callId, i, 1, &calls_array[i] );
	}

	return PJ_SUCCESS;
}

/*
 * Method:    pjsip_call_get_info
 */
#define RC_CALL_INFO_VALUES_COUNT	 6
#define RC_CALL_INFO_STATUS_COUNT	 5
/*
 * Call values are:
 *
 * [0]	Local URI
 * [1]	Local Contact
 * [2]	Remote URI
 * [3]	Remote contact
 * [4]	Dialog Call-ID string
 * [5]  RC Header (if present)
 *
 * Status are:
	 * [0]  state - Call state
	 * [1]  status code - Last status code heard, which can be used as cause code
	 * [2]  media status - Call media status
	 * [3]  account ID
	 * [4]  call status (Hold, Finished)
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1get_1info(JNIEnv *jenv,
		  jclass,
		  jint callId,
		  jobjectArray values_array,
		  jintArray status_array )
{
	pj_status_t status;
	pjsua_call_info call_info;
	jint status_code;

	PJ_LOG(3, (THIS_FILE, "pjsip_call_get_info is starting... %d", callId ));

	jint values_size = jenv->GetArrayLength( values_array );
	jint status_size = jenv->GetArrayLength( status_array );

	if( values_size < RC_CALL_INFO_VALUES_COUNT ||
			status_size < RC_CALL_INFO_STATUS_COUNT ){

		pjsua_perror(THIS_FILE, "pjsip_call_enum_calls failed ", PJ_EINVAL );
		return PJ_EINVAL;
	}

	status = pjsua_call_get_info( callId, &call_info );
	if( status != PJ_SUCCESS ){
		pjsua_perror(THIS_FILE, "pjsip_call_enum_calls failed ", status );
		return status;
	}

	jenv->SetObjectArrayElement( values_array, 0, jenv->NewStringUTF(call_info.local_info.ptr));
	jenv->SetObjectArrayElement( values_array, 1, jenv->NewStringUTF(call_info.local_contact.ptr));
	jenv->SetObjectArrayElement( values_array, 2, jenv->NewStringUTF(call_info.remote_info.ptr));
	jenv->SetObjectArrayElement( values_array, 3, jenv->NewStringUTF(call_info.remote_contact.ptr));
	jenv->SetObjectArrayElement( values_array, 4, jenv->NewStringUTF(call_info.call_id.ptr));

	status_code = call_info.state;
	jenv->SetIntArrayRegion(status_array, 0, 1, &status_code );
	status_code = call_info.last_status;
	jenv->SetIntArrayRegion(status_array, 1, 1, &status_code );
	status_code = call_info.media_status;
	jenv->SetIntArrayRegion(status_array, 2, 1, &status_code );

	jenv->SetIntArrayRegion(status_array, 3, 1, &call_info.acc_id );

	pjsua_call_id call_id = callId;
	rc_call_info* rcCallInfo = (rc_call_info*)pjsua_call_get_user_data( call_id );
	if( rcCallInfo == NULL ){
		/*
		 * Strange situation. RC Call Info is absent
		 */
		status_code = -1;
		jenv->SetIntArrayRegion(status_array, 4, 1, &status_code );
	}
	else{
		status_code = rcCallInfo->status;
		jenv->SetIntArrayRegion(status_array, 4, 1, &status_code );

		jenv->SetObjectArrayElement( values_array, 5, jenv->NewStringUTF(rcCallInfo->rc_header.ptr));
	}

	PJ_LOG(3, (THIS_FILE, "pjsip_call_get_info finished %d", callId ));
	return PJ_SUCCESS;
}

/*
 * Method:    pjsip_call_get_duration
 */
#define RC_CALL_INFO_DURATION_COUNT	 2
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1call_1get_1duration(JNIEnv *jenv,
		jclass,
		jint callId,
		jlongArray duration)
{
	pj_status_t status;
	pjsua_call_info call_info;
	jlong dur;

	jint size = jenv->GetArrayLength( duration );
	if( size < RC_CALL_INFO_DURATION_COUNT ){

		pjsua_perror(THIS_FILE, "pjsip_call_get_duration failed ", PJ_EINVAL );
		return PJ_EINVAL;
	}

	status = pjsua_call_get_info( callId, &call_info );
	if( status != PJ_SUCCESS ){
		pjsua_perror(THIS_FILE, "pjsip_call_get_duration failed ", status );
		return status;
	}

	dur = (jlong)call_info.connect_duration.sec;
	jenv->SetLongArrayRegion(duration, 0, 1, &dur );
	dur = (jlong)call_info.total_duration.sec;
	jenv->SetLongArrayRegion(duration, 1, 1, &dur );

	return PJ_SUCCESS;
}


#ifdef __cplusplus
}
#endif

