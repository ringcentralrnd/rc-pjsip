/*
 * rc_pjsip_jni_audio_dev.cpp
  *
 * Copyright (C) 2011, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 *  Implementation of PJSIP JNI interface
 *  PJSIP calls for RC Android Audio Device
 */

#include <rc_pjsip_jni.h>
#include "rc_android_audio_dev_java.h"

#define THIS_FILE	"rc_pjsip_jni_audio_dev.cpp"

#ifndef INT16_MIN
#define INT16_MIN       (-32768)            // minimum Int16 value
#define INT16_MAX       (32767)             // maximum Int16 value
#endif

#define F_MAX(A,B)          (((A) > (B)) ? (A) : (B))
#define F_MIN(A,B)          (((A) < (B)) ? (A) : (B))
#define F_CLIP(x,Max,Min)   F_MIN(Max, F_MAX(x,Min))

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_readFromWriteToRTP(JNIEnv *jenv,
          jclass,
          jbyteArray jbBufferArrayTo,
          jbyteArray jbBufferArrayFrom,
          jint buffer_size,
          jlong frameCounter,
          jlong params )
{

    pjmedia_frame frame, frame_from;
    pj_status_t status;

    PJ_ASSERT_RETURN(params, PJ_EINVAL);

    struct rc_android_audio_stream_java *rc_strm = (struct rc_android_audio_stream_java *)params;
    jbyte* bufferto = jenv->GetByteArrayElements( jbBufferArrayTo, 0 );
    jbyte* bufferfrom = jenv->GetByteArrayElements( jbBufferArrayFrom, 0 );

    frame.type = PJMEDIA_FRAME_TYPE_AUDIO;
    frame.buf = (void*) bufferto;
    frame.size = buffer_size;
    frame.bit_info = 0;
    frame.timestamp.u64 = frameCounter;

    status = (*rc_strm->rec_cb)(rc_strm->user_data, &frame );
    if( status != PJ_SUCCESS )
    {
         ERROR_("Call rec_cb failed. [android.media.AudioRecord]");
    }

    frame_from.type = PJMEDIA_FRAME_TYPE_AUDIO;
    frame_from.buf = (void*) bufferfrom;
    frame_from.size = buffer_size;
    frame_from.bit_info = 0;
    frame_from.timestamp.u64 = frameCounter;
    pj_bzero (bufferfrom, buffer_size);
    status = (*rc_strm->play_cb)(rc_strm->user_data, &frame_from );
    if( status != PJ_SUCCESS )
    {
        ERROR_("Call play_cb failed. [android.media.AudioRecord]");
    }

    jenv->ReleaseByteArrayElements( jbBufferArrayTo, bufferto, 0 );
    jenv->ReleaseByteArrayElements( jbBufferArrayFrom, bufferfrom, 0 );

    return PJ_SUCCESS;
}

/*
 * Method:    writeToRTP
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_writeToRTP(JNIEnv *jenv,
		  jclass,
		  jbyteArray jbBufferArray,
		  jint buffer_size,
		  jlong frameCounter,
		  jlong params )
{

	pjmedia_frame frame;
	pj_status_t status;

	PJ_ASSERT_RETURN(params, PJ_EINVAL);

	struct rc_android_audio_stream_java *rc_strm = (struct rc_android_audio_stream_java *)params;
	jbyte* buffer = jenv->GetByteArrayElements( jbBufferArray, 0 );

	frame.type = PJMEDIA_FRAME_TYPE_AUDIO;
	frame.buf = (void*) buffer;
	frame.size = buffer_size;
	frame.bit_info = 0;
	frame.timestamp.u64 = frameCounter;

	status = (*rc_strm->rec_cb)(rc_strm->user_data, &frame );

	jenv->ReleaseByteArrayElements( jbBufferArray, buffer, 0 );

	if( status != PJ_SUCCESS )
	{
		ERROR_("Call rec_cb failed. [android.media.AudioRecord]");
	}

	return status;
}

/*
 * Method:    readFromRTP
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_readFromRTP(JNIEnv *jenv,
		  jclass,
		  jbyteArray jbBufferArray,
		  jint bufferSize,
		  jlong frameCounter,
		  jlong params )
{

	pjmedia_frame frame;
	pj_status_t status;

	PJ_ASSERT_RETURN(params, PJ_EINVAL);

	struct rc_android_audio_stream_java *rc_strm = (struct rc_android_audio_stream_java *)params;

	if( bufferSize != sizeof(pj_int16_t) * rc_strm->param.samples_per_frame ){
		ERROR_("Call readFromRTP failed. Invalid buffer size");
		return PJ_FALSE;
	}

	jbyte* buf = jenv->GetByteArrayElements( jbBufferArray, 0 );
	pj_bzero (buf, bufferSize);
	frame.type = PJMEDIA_FRAME_TYPE_AUDIO;
	frame.size = bufferSize;
	frame.buf = (void *) buf;
	frame.timestamp.u64 = frameCounter;
	frame.bit_info = 0;

	status = (*rc_strm->play_cb)(rc_strm->user_data, &frame);
	jenv->ReleaseByteArrayElements( jbBufferArray, buf, 0 );

	if( status == PJ_SUCCESS ) {
		if ( frame.type != PJMEDIA_FRAME_TYPE_AUDIO )
		{
			TRACE_( "Invalid type of frame or empty buffer" );
			status = PJ_TRUE;
		}
	}

	if( status != PJ_SUCCESS )
	{
		ERROR_("Call play_cb failed. [android.media.AudioRecord]");
	}

	return status;
}

/*
 * Method:    aec
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aec(JNIEnv *jenv,
		jclass,
		jbyteArray jbMicBufferArray,
		jbyteArray jbSpkBufferArray,
		jint bufferSize,
		jlong params )
{

	PJ_ASSERT_RETURN(params, PJ_EINVAL);

	struct rc_android_audio_stream_java *rc_strm = (struct rc_android_audio_stream_java *)params;

	if( bufferSize != sizeof(pj_int16_t) * rc_strm->param.samples_per_frame ){
		ERROR_("Call AEC failed. Invalid buffer size");
		return PJ_FALSE;
	}

	jbyte* micBuffer = jenv->GetByteArrayElements( jbMicBufferArray, 0 );
	jbyte* spkBuffer = jenv->GetByteArrayElements( jbSpkBufferArray, 0 );

	/* Prepare microphone & speaker samples for EC*/
	for( int i = 0; i < rc_strm->param.samples_per_frame; i++ ){
		rc_strm->microphone_buf_in[0][i] 	= (ShortReal)((pj_int16_t*)micBuffer)[i];
		rc_strm->speaker_buf_in[i] 			= (ShortReal)((pj_int16_t*)spkBuffer)[i];
	}

	jenv->ReleaseByteArrayElements( jbSpkBufferArray, spkBuffer, 0 );
	jenv->ReleaseByteArrayElements( jbMicBufferArray, micBuffer, 0 );

	ShortReal* offset[1];
	for( int idx = 0; idx < rc_strm->aec_cycles; idx++ ){

		offset[0] = &rc_strm->microphone_buf_in[0][idx*rc_strm->aec_samples_per_frame];

		rc_strm->pSBACP->ProcessSpkFrame( &rc_strm->speaker_buf_in[idx*rc_strm->aec_samples_per_frame],
				&rc_strm->speaker_buf_out[idx*rc_strm->aec_samples_per_frame] );

		rc_strm->pSBACP->ProcessMicFrame( &offset[0],
				&rc_strm->microphone_buf_out[idx*rc_strm->aec_samples_per_frame] );
	}

	for( int i = 0; i < rc_strm->param.samples_per_frame; i++ ){
		((pj_int16_t*)rc_strm->aecFrameBuffMic)[i] = (pj_int16_t)F_CLIP(rc_strm->microphone_buf_out[i], (float)INT16_MAX, (float)INT16_MIN);
		((pj_int16_t*)rc_strm->aecFrameBuffSpk)[i] = (pj_int16_t)F_CLIP(rc_strm->speaker_buf_out[i], (float)INT16_MAX, (float)INT16_MIN);
	}

	jenv->SetByteArrayRegion( jbMicBufferArray, 0, bufferSize, (jbyte*)rc_strm->aecFrameBuffMic );
	jenv->SetByteArrayRegion( jbSpkBufferArray, 0, bufferSize, (jbyte*)rc_strm->aecFrameBuffSpk );


	return PJ_SUCCESS;
}

/*
 * Method:    aec for scheme 2
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_aecScheme2(JNIEnv *jenv,
        jclass,
        jbyteArray jbMicBufferArray,
        jbyteArray jbSpkBufferArray,
        jint bufferSize,
        jlong params )
{

    PJ_ASSERT_RETURN(params, PJ_EINVAL);

    struct rc_android_audio_stream_java *rc_strm = (struct rc_android_audio_stream_java *)params;

    if( bufferSize != sizeof(pj_int16_t) * rc_strm->param.samples_per_frame ){
        ERROR_("Call AEC (Scheme 2) failed. Invalid buffer size");
        return PJ_FALSE;
    }

    jbyte* micBuffer = jenv->GetByteArrayElements( jbMicBufferArray, 0 );
    jbyte* spkBuffer = jenv->GetByteArrayElements( jbSpkBufferArray, 0 );

    /* Prepare microphone & speaker samples for EC*/
    for( int i = 0; i < rc_strm->param.samples_per_frame; i++ ){
        rc_strm->microphone_buf_in[0][i]    = (ShortReal)((pj_int16_t*)micBuffer)[i];
        rc_strm->speaker_buf_in[i]          = (ShortReal)((pj_int16_t*)spkBuffer)[i];
    }

    jenv->ReleaseByteArrayElements( jbSpkBufferArray, spkBuffer, 0 );
    jenv->ReleaseByteArrayElements( jbMicBufferArray, micBuffer, 0 );

    ShortReal* offset[1];
    for( int idx = 0; idx < rc_strm->aec_cycles; idx++ ){

        offset[0] = &rc_strm->microphone_buf_in[0][idx*rc_strm->aec_samples_per_frame];

        rc_strm->pSBACP->ProcessSpkFrame( &rc_strm->speaker_buf_in[idx*rc_strm->aec_samples_per_frame],
                &rc_strm->speaker_buf_out[idx*rc_strm->aec_samples_per_frame] );

        rc_strm->pSBACP->ProcessMicFrame( &offset[0],
                &rc_strm->microphone_buf_out[idx*rc_strm->aec_samples_per_frame] );
    }

    for( int i = 0; i < rc_strm->param.samples_per_frame; i++ ){
        ((pj_int16_t*)rc_strm->aecFrameBuffMic)[i] = (pj_int16_t)F_CLIP(rc_strm->microphone_buf_out[i], (float)INT16_MAX, (float)INT16_MIN);
    }

    jenv->SetByteArrayRegion( jbMicBufferArray, 0, bufferSize, (jbyte*)rc_strm->aecFrameBuffMic );
    jenv->SetByteArrayRegion( jbSpkBufferArray, 0, bufferSize, (jbyte*)rc_strm->aecFrameBuffSpk );


    return PJ_SUCCESS;
}



JNIEXPORT jlong JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_getTimestamp(JNIEnv *jenv,
        jclass,
        jlong params )
{
    PJ_ASSERT_RETURN(params, PJ_EINVAL);
    pj_timestamp tstamp;
    pj_get_timestamp(&tstamp);
    return tstamp.u64;
}

JNIEXPORT jlong JNICALL Java_com_rcbase_android_sip_audio_AudioDevicePJSIP_getElapsedTime
  (JNIEnv *, jclass, jlong params, jlong start, jlong stop)
{
    PJ_ASSERT_RETURN(params, PJ_EINVAL);
    pj_timestamp strt;
    pj_timestamp stp;
    strt.u64 = start;
    stp.u64 = stop;
    return pj_elapsed_msec64(&strt, &stp);
}

/*
 * starts beep just to make the sound
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1start_1test(JNIEnv *, jclass)
{
    TRACE_("setup_start");
	ring_start(PJSUA_INVALID_ID);

	return 1;
}


/*
 *  pjsip_setup_init()
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1stop_1test(JNIEnv *, jclass)
{
    TRACE_("setup_start");
	ring_stop(PJSUA_INVALID_ID);

	return 1;
}


#ifdef __cplusplus
}
#endif


