/*
 * rc_pjsip_jni_codec.cpp
 *
 *
 * Copyright (C) 2011-2012, RingCentral, Inc.
 * All Rights Reserved.
 *
 *  Author: Denis Kudja
 *
 *  Implementation of PJSIP JNI interface
 *  SIP Stack Codec functions.
 */

#include <rc_pjsip_jni.h>

#define THIS_FILE	"rc_pjsip_jni_codec.cpp"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Method:    pjsip_enum_codecs
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1enum_1codecs(JNIEnv *jni_env,
	  jclass,
	  jobjectArray codecId,
	  jintArray codecPrio,
	  jintArray codecCount )
{
	pj_status_t status;
	pjsua_codec_info info[32];
	unsigned count = PJ_ARRAY_SIZE(info);

	PJ_LOG(5, (THIS_FILE, "pjsip_enum_codecs is starting... " ));

	jsize id_len = jni_env->GetArrayLength(codecId);
	jsize prio_len = jni_env->GetArrayLength(codecPrio);

	status = pjsua_enum_codecs( info, &count );
	if( status != PJ_SUCCESS ){
		pjsua_perror(THIS_FILE, "pjsip_enum_codecs failed ", status );
		return status;
	}

	PJ_LOG(4, (THIS_FILE, "pjsip_enum_codecs detected %d codecs", count ));
	jint ji_count = count;
	jni_env->SetIntArrayRegion(codecCount, 0, 1, &ji_count );

	if( count > id_len || count > prio_len ){
		pjsua_perror(THIS_FILE, "pjsip_enum_codecs failed. Buffer too small.", PJ_ETOOSMALL );
		return PJ_ETOOSMALL;
	}

	for( int i = 0; i < count; i++ ){
		jint priority = info[i].priority;
		jni_env->SetObjectArrayElement( codecId, i, jni_env->NewStringUTF(info[i].codec_id.ptr) );
		jni_env->SetIntArrayRegion( codecPrio, i, 1, &priority );
	}

	PJ_LOG(5, (THIS_FILE, "pjsip_enum_codecs finished %d", status ));
	return status;
}

/*
 * Method:    pjsip_codec_set_priority
 */
JNIEXPORT jint JNICALL Java_com_rcbase_android_sip_service_PJSIP_pjsip_1codec_1set_1priority(JNIEnv *jni_env,
		jclass,
		jstring codecId,
		jint priority)
{
	pj_status_t status;
	pj_str_t pj_codec_id;
	const char* pcodec_id;
	pj_uint8_t pj_priority= (pj_uint8_t)priority;

	PJ_LOG(4, (THIS_FILE, "pjsip_codec_set_priority is starting... priority: %d", pj_priority ));

	pcodec_id = jni_env->GetStringUTFChars( codecId, JNI_FALSE );
	pj_strdup2_with_null( rc_cfg.pool, &pj_codec_id, pcodec_id );
	jni_env->ReleaseStringUTFChars( codecId, pcodec_id );

	status = pjsua_codec_set_priority( &pj_codec_id, pj_priority );
	if( status != PJ_SUCCESS ){
		pjsua_perror(THIS_FILE, "pjsip_codec_set_priority failed ", status );
		return status;
	}

	PJ_LOG(4, (THIS_FILE, "pjsip_codec_set_priority finished %d", status ));
	return status;
}

#ifdef __cplusplus
}
#endif

