
LOCAL_PATH := $(call my-dir)/../

include $(CLEAR_VARS)
LOCAL_MODULE := rcpjsip

LOCAL_C_INCLUDES += 	$(LOCAL_PATH)/../pjsip/include \
		    	$(LOCAL_PATH)/../pjlib-util/include/ \
				$(LOCAL_PATH)/../pjlib/include/ \
				$(LOCAL_PATH)/../pjmedia/include \
				$(LOCAL_PATH)/../pjnath/include \
				$(LOCAL_PATH)/../pjlib/include \
				$(LOCAL_PATH)/../third_party/sbacp \
				$(LOCAL_PATH)/../third_party/rcutils \
				$(LOCAL_PATH)/include/ \
				$(LOCAL_PATH)../third_party/tsc/include/
			
LOCAL_CFLAGS := $(RC_PJSIP_LIB_FLAGS)

JNI_SRC_DIR := src/

LOCAL_SRC_FILES := $(JNI_SRC_DIR)/rc_pjsip_jni.cpp \
		$(JNI_SRC_DIR)/rc_pjsip_jni_control.cpp \
		$(JNI_SRC_DIR)/rc_pjsip_jni_acc.cpp \
		$(JNI_SRC_DIR)/rc_pjsip_jni_callback.cpp \
		$(JNI_SRC_DIR)/rc_pjsip_jni_ring.cpp \
		$(JNI_SRC_DIR)/rc_pjsip_jni_call_control.cpp \
		$(JNI_SRC_DIR)/rc_pjsip_jni_call_info.cpp \
		$(JNI_SRC_DIR)/rc_pjsip_jni_message.cpp \
		$(JNI_SRC_DIR)/rc_pjsip_jni_codec.cpp \
		$(JNI_SRC_DIR)/rc_pjsip_jni_conf.cpp \
		$(JNI_SRC_DIR)/rc_pjsip_jni_audio_dev.cpp \
		$(JNI_SRC_DIR)/rc_android_audio_dev_java.cpp
		

LOCAL_LDLIBS := -llog

ifeq ($(RC_PJSIP_USE_TLS),1)
LOCAL_LDLIBS += -ldl 
endif

LOCAL_STATIC_LIBRARIES := pjsip pjmedia pjnath pjlib-util pjlib resample srtp rcutils

ifeq ($(RC_PJSIP_USE_ILBC),1)
	LOCAL_STATIC_LIBRARIES += ilbc
endif

ifeq ($(RC_PJSIP_USE_GSM),1)
	LOCAL_STATIC_LIBRARIES += gsm
endif

ifeq ($(RC_PJSIP_USE_SPEEX),1)
	LOCAL_STATIC_LIBRARIES += speex
endif

ifeq ($(RC_PJSIP_USE_G729),1)
	LOCAL_STATIC_LIBRARIES += g729
endif

ifeq ($(RC_PJSIP_USE_SILK),1)
	LOCAL_STATIC_LIBRARIES += silk
endif

ifeq ($(RC_PJSIP_USE_TLS),1)
	LOCAL_STATIC_LIBRARIES += ssl crypto
endif

ifeq ($(RC_PJSIP_USE_SBACP),1)
  SBACP_LIB := $(LOCAL_PATH)/../third_party/sbacp/libs/$(TARGET_ARCH_ABI)/
  LOCAL_LDLIBS += -L$(SBACP_LIB) -lSBACP -lASDSP -lPFA_FFT
endif

#TSC_LIB := $(LOCAL_PATH)/../third_party/tsc/lib/
#LOCAL_LDLIBS += -L$(TSC_LIB) -ltsm
ifeq ($(RC_PJSIP_USE_TSC),1)
	LOCAL_STATIC_LIBRARIES += tsm_s
endif

include $(BUILD_SHARED_LIBRARY)

